<?php
/*******************************************************************************
**   FILE: howitworks.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Handles the display of the how it works page and header/footer files
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   
**
**   ADAPTED BY: 
**
*********************************************************************************/
?>
<?php 

include("wrapper/header.php"); 

include(ACTIVE_THEME_PATH ."/howitworks.php"); 

include("wrapper/footer.php"); 

?>
