<?php
	ini_set("include_path", ".:/home/3250/users/.home/domains/sendairtime.com/html/lib/phpmailer");
	require_once("/home/3250/domains/sendairtime.com/html".'/lib/phpmailer/class.phpmailer.php');
	include_once $_SERVER['DOCUMENT_ROOT'].'/src/constants.php';
	include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
	
	function sendBasicEmail($from,$to,$subject,$body){
		//$recordsEmail ='test@sendairtime.com';
		$from = "Sendairtime dot Com Web <".$from.">";
		$header = "From: " . $from . "\r\n";
		mail($to, $subject, $body,$header);
	}
	
	function sendEmail($fromName,$fromAddress,$toAddress,$bcc,$subject,$body){
		$mail = new PHPMailer();
		if($bcc==''){$bcc=RECORDS_EMAIL;}
		if($fromAddress==''){$fromAddress=FROM_EMAIL;}
		$mail->IsSMTP();                                   // send via SMTP
		$mail->Host     = "mail.sendairtime.com"; // SMTP servers
		$mail->SMTPAuth = true;     // turn on SMTP authentication
		$mail->Username = SUPPORT_EMAIL;  // SMTP username
		$mail->Password = SUPPORT_PASSWD; // SMTP password

		$mail->From     = $fromAddress;
		if($fromName==''){$fromName=FROM_NAME;}
		$mail->FromName = $fromName;
		$mail->AddAddress($toAddress);               // optional name
		//$mail->AddReplyTo($fromAddress);
		$mail->AddBCC($bcc);

		//$mail->WordWrap = 50;                              // set word wrap
		$mail->IsHTML(false);                               // send as HTML

		$mail->Subject  =  $subject;
		$mail->Body     =  $body;
		//$mail->AltBody  =  "This is the text-only body";
		//$mail->ClearCustomHeaders();
		
		if(!$mail->Send())
		{
 		  	//echo "Message was not sent <p>";
   			//echo "Mailer Error: " . $mail->ErrorInfo;
  		 	//exit;
			return false;
		}

		//echo "Message has been sent<br>";
		return true;
	}
	
	function sendEmail2($fromName,$fromAddress,$toAddress,$bcc,$subject,$body,$countryAbbreviation){
		global $supportEmailArray;
		$outgoingFromName=$supportEmailArray[$countryAbbreviation]["name"];
		$outgoingFromEmail=$supportEmailArray[$countryAbbreviation]["email"];
		$outgoingBCC=$supportEmailArray[$countryAbbreviation]["bcc"];
		$passwd=$supportEmailArray[$countryAbbreviation]["password"];
		
		
		//make sure that there are values for these emails
		if($outgoingFromName!=''&& $outgoingFromEmail!=''&& $outgoingBCC!=''){
			$mail = new PHPMailer();
			if($bcc==''){$bcc=$outgoingBCC;}
			if($fromAddress==''){$fromAddress=$outgoingFromEmail;}
			$mail->IsSMTP();                                   // send via SMTP
			$mail->Host     = SMTP_SERVER; // SMTP servers
			$mail->SMTPAuth = true;     // turn on SMTP authentication
			$mail->Username = $outgoingFromEmail;  // SMTP username
			$mail->Password = $passwd; // SMTP password
	
			$mail->From     = $fromAddress;
			if($fromName==''){$fromName=$outgoingFromName;}
			$mail->FromName = $fromName;
			$mail->AddAddress($toAddress);               // optional name
			//$mail->AddReplyTo($fromAddress);
			$mail->AddBCC($bcc);
	
			//$mail->WordWrap = 50;                              // set word wrap
			$mail->IsHTML(false);                               // send as HTML
	
			$mail->Subject  =  $subject;
			$mail->Body     =  $body;
			//$mail->AltBody  =  "This is the text-only body";
			//$mail->ClearCustomHeaders();
			
			if(!$mail->Send())
			{
				
				$errorBody="Message was not sent to $toAddress \n";
				$errorBody.="outgoingFromEmail $outgoingFromEmail \n";
				$errorBody.="fromAddress: $fromAddress \n";
				$errorBody.="fromName: $fromName \n";
				$errorBody.="toAddress: $toAddress \n";
				$errorBody.="bcc: $bcc \n";
				
				$errorSubject="Mailer Error: " . $mail->ErrorInfo;
				//echo $errorBody."<br>";
				//echo $errorSubject."<br>";
				//sendEmail(ERROR_NAME,"noreply@sendairtime.com",TECH_ADMIN,$bcc,$errorSubject,$errorBody);
				sendBasicEmail($fromAddress,TECH_ADMIN,$errorSubject,$errorBody);
				//exit;
				return false;
			}
	
			//echo "Message has been sent<br>";
			return true;
		}
		else{//since the emails have a missing value then we should not send out anything.
			//echo "Error, most likely country code not in List";
			return false;
		}
	}
	
	function getValueInUGSh($itemID){
		return substr($itemID,9);
	}
	
	function sendAdminErrorEmail($errorSubject,$errorBody){
		sendEmail(ERROR_NAME,"noreply@sendairtime.com",TECH_ADMIN,"",$errorSubject,$errorBody);
	}
	
	//function to inform client that their PIN will be sent to them when we restock
	function sendZeroCardsEmail2Client($toAddress,$receiverFirstName,$firstName,$itemID,$transID){
		//$fromAddress="sms@sendairtime.com";
		
		
		$amount=getValueInUGSh($itemID);
		
		$networkName=getNetworkName($itemID);
		$usAmount=getPriceInUSD($itemID);
		
		//determine the countries currency from the array
		global $countryInfo;
		global $supportEmailArray;
		global $cardAdminEmailArray;
		
		
		$countryAbbreviation=substr($itemID,0,2);
		$cardAdminEmail=$cardAdminEmailArray[$countryAbbreviation];
		//echo $countryAbbreviation."<br>";
		
		$currency=$countryInfo[$countryAbbreviation]["currency"];
		//echo $currency."<br>";
		
		//also obtain the countries support email from the array
		$support=$supportEmailArray[$countryAbbreviation]["email"];
		//echo $support."<br>";
		if($support==''){$support=SUPPORT_EMAIL;}
		
		//extract homepage from array
		$homePage=$countryInfo[$countryAbbreviation]["homePage"];
		//echo $homePage."<br>";
		if($homePage==''){$homePage="www.sendairtime.com";}//default
		
		//$toAddress="client@sendairtime.com";
		//dispatch the no cards email to the client
		$subject = "$homePage: Airtime Card Details";
		$message = "Dear $firstName, \n"; 
		$message.= "We wish to inform you that we have received your order however \r\n";
		$message.= "we are sorry that there will be a delay in delivery of your card and we apologize for the inconvinience\r\n";
		$message.= "Below are the order details, you will receive another email confirming the dispatch of your card\r\n\n";		
		$message.= "-------ORDER DETAILS-------------------\n";
		$message.= "Network: $networkName\r\n";
		$message.= "Airtime Code: to be emailed in a few hours\r\n";
		$message.= "Amount in ".$currency." : $amount \r\n";
		$message.= "Amount in USD: $usAmount \r\n";
		$message.= "Order Number: $transID \r\n";
		$message.= "--------------------------------------\n\n";
		$message.= "You can contact us via email at ".$support. "\n";
		$message.= "Please have the order number ready when contacting customer service \n\n";
		$message.= "Cheers -- Buy airtime online at http://".$homePage."\n";
		
		//sendEmail2($fromName,$fromAddress,$toAddress,$bcc,$subject,$body,$countryAbbreviation);
		sendEmail2('','',$toAddress,$cardAdminEmail,$subject,$message,$countryAbbreviation);	
	}
	
	//modified for modularity
	function sendZeroCardsEmail2Receiver($toAddress,$receiverFirstName,$firstName,$itemID,$transID){
		
		$amount=getValueInUGSh($itemID);
		$networkName=getNetworkName($itemID);
		$usAmount=getPriceInUSD($itemID);
		
		//determine the countries currency from the array
		global $countryInfo;
		global $supportEmailArray;
		global $cardAdminEmailArray;
		
		$countryAbbreviation=substr($itemID,0,2);
		$cardAdminEmail=$cardAdminEmailArray[$countryAbbreviation];
		//echo $countryAbbreviation."<br>";
		
		$currency=$countryInfo[$countryAbbreviation]["currency"];
		//echo $currency."<br>";
		
		//also obtain the countries support email from the array
		$support=$supportEmailArray[$countryAbbreviation]["email"];
		//echo $support."<br>";
		if($support==''){$support=SUPPORT_EMAIL;}
		
		//extract homepage from array
		$homePage=$countryInfo[$countryAbbreviation]["homePage"];
		//echo $homePage."<br>";
		if($homePage==''){$homePage="www.sendairtime.com";}//default
		
		
		if($receiverFirstName==''){
			$receiverFirstName='Receiver';
		}
		
		//$toAddress="receiver@sendairtime.com";
		//dispatch the no cards email to the client
		$subject = "$homePage: Airtime Card Details";
		$message = "Dear $receiverFirstName, \n"; 
		$message.= "This is to inform you that $firstName has bought $amount ".$currency." airtime for you \r\n";
		$message.= "however due to being unexpectedly out of stock we have delayed the order. We will \r\n";
		$message.= "dispatch the order the moment we restock (takes at most 2 hours).\r\n";		
		$message.= "Meanwhile you can sit, relax and wait.\n\n";
		$message.= "Incase you have any questions, you can contact us at ".$support." with order number $transID\n\n";
		$message.= "Cheers -- Buy airtime online at http://".$homePage."\n";
		sendEmail2($fromName,$fromAddress,$toAddress,$cardAdminEmailArray,$subject,$message,$countryAbbreviation);
	}
	
	//modified for modularity
	function sendEmail2Client($toAddress,$receiverFirstName,$firstName,$pin2Send,$itemID,$transid){
				
		if($receiverFirstName==''){
			$receiverFirstName='the receiver';
		}
		
		//get amount, networkName, and Price in USD
		$amount=getValueInUGSh($itemID);
		$networkName=getNetworkName($itemID);
		$usAmount=getPriceInUSD($itemID);
		
		//determine the countries specifics from the array
		global $countryInfo;
		global $supportEmailArray;
		global $networkArray;
		global $cardAdminEmailArray;
		global $countrySignatureArray;
		
		$countryAbbreviation=substr($itemID,0,2);
		$cardAdminEmail=$cardAdminEmailArray[$countryAbbreviation];
		$currency=$countryInfo[$countryAbbreviation]["currency"]; //echo $currency."<br>";
		//also obtain the countries support email from the array
		$support=$supportEmailArray[$countryAbbreviation]["email"]; //echo $support."<br>";
		if($support==''){$support=SUPPORT_EMAIL;}
		//extract homepage from array
		$homePage=$countryInfo[$countryAbbreviation]["homePage"]; //echo $homePage."<br>";
		if($homePage==''){$homePage="www.sendairtime.com";}//default
		//extract load instruction code
		$networkCode=substr($itemID,0,5);
		$loadCode=$networkArray[$countryAbbreviation][$networkCode]["loadCode"];
		$countrySignature=$countrySignatureArray[$countryAbbreviation];
		
		
		//$toAddress="client@sendairtime.com";
		//dispatch the card to the client and the receiver
		$subject = "$homePage: Airtime Card Details";
		$message = "Dear $firstName, \n"; 
        $message.= "YOUR AIRTIME ACCESS NUMBER FOR $networkName $amount is $pin2Send \n";
		$message.= "Your order to $receiverFirstName has been processed and is complete.";
		$message.= "The airtime code was automatically sent to $receiverFirstName.\n";
		//$message.= "We wish to inform you that we have received the order you placed for $receiverFirstName and processed it \r\n";
		$message.= "Below is the Airtime Access Number and the order details for your records. \r\n\n";	
		$message.= "ORDER DETAILS--------------------------\n";
		$message.= "Airtime Access Number: $pin2Send\r\n";
		$message.= "Network: $networkName\r\n";
		$message.= "Amount in ".$currency.": $amount \r\n";
		$message.= "Amount in USD: $usAmount \r\n";
		$message.= "Order Number: $transid \r\n";
		$message.= "------------------------------------------------\n\n";
		$message.= "Instructions for the Receiver are: \n";
		$message.= "To load dial ".$loadCode.$pin2Send."# on a $networkName phone. \n\n";
		$message.= "Use the order number above when contacting customer service \n";
		$message.= "Please do provide feedback of what you would like us to improve at ".$support."\n\n";
		$message.= "Incase of a bad airtime code contact us at ".$support."\n\n";
		$message.= "Cheers -- Buy airtime online at ".$homePage."\n";
		$message.= $countrySignature;		
		
		sendEmail2($fromName,$fromAddress,$toAddress,$cardAdminEmail,$subject,$message,$countryAbbreviation);
	}
	
	//modified and tested for modularity
	function sendEmail2Receiver($toAddress,$receiverFirstName,$firstName,$pin2Send,$itemID,$transid){
		
		if($receiverFirstName==''){
			$receiverFirstName='Receiver';
		}
		
		//get amount, networkName, and Price in USD
		$amount=getValueInUGSh($itemID);
		$networkName=getNetworkName($itemID);
		$usAmount=getPriceInUSD($itemID);
		
		//determine the countries specifics from the array
		global $countryInfo;
		global $supportEmailArray;
		global $networkArray;
		global $cardAdminEmailArray;
		
		$countryAbbreviation=substr($itemID,0,2);
		$cardAdminEmail=$cardAdminEmailArray[$countryAbbreviation];
		$currency=$countryInfo[$countryAbbreviation]["currency"]; //echo $currency."<br>";
		//also obtain the countries support email from the array
		$support=$supportEmailArray[$countryAbbreviation]["email"]; //echo $support."<br>";
		if($support==''){$support=SUPPORT_EMAIL;}
		//extract homepage from array
		$homePage=$countryInfo[$countryAbbreviation]["homePage"]; //echo $homePage."<br>";
		if($homePage==''){$homePage="www.sendairtime.com";}//default
		//extract load instruction code
		$networkCode=substr($itemID,0,5);
		$loadCode=$networkArray[$countryAbbreviation][$networkCode]["loadCode"];
		
		//$toAddress="receiver@sendairtime.com";
		//receiver Message
		$subject="$homePage: Order Details";
		$receiverMessage.= "Dear $receiverFirstName, \n";
		$receiverMessage.= "You have received airtime! \n";
		$receiverMessage.= "$firstName has bought you $amount ".$currency." worth of airtime. ";
		$receiverMessage.= "Below is your Airtime Code and order details:\n\n";
		$receiverMessage.= "ORDER DETAILS--------------------------\n";
		$receiverMessage.= "Airtime Access Number: $pin2Send\r\n";
		$receiverMessage.= "Network: $networkName\r\n";
		$receiverMessage.= "Amount in ".$currency.": $amount \r\n";
		$receiverMessage.= "Order Number: $transid \r\n";
		$receiverMessage.= "-----------------------------------------------------\n\n";
		$receiverMessage.="To load dial ".$loadCode.$pin2Send."# on a $networkName phone. \n\n";
		$receiverMessage.= "Use order number above when contacting customer service \n";
		$receiverMessage.= "Incase of a bad airtime code contact us at ".$support."\n\n";
		$receiverMessage.= "Cheers -- Buy airtime online at ".$homePage."\n";
		if($toAddress!=''){
			sendEmail2($fromName,$fromAddress,$toAddress,$cardAdminEmail,$subject,$receiverMessage,$countryAbbreviation);
		}
		else{
			//since the receiver's address is blank, we do nothing
		}
	}
	

	
	//modified for modularity
	function sendEmail2ClientAuto($toAddress,$firstName,$itemID,$transid){
		if($receiverFirstName==''){
			$receiverFirstName='Receiver';
		}
		
		//get amount, networkName, and Price in USD
		$amount=getValueInUGSh($itemID);
		$networkName=getNetworkName($itemID);
		$usAmount=getPriceInUSD($itemID);
		
		//determine the countries specifics from the array
		global $countryInfo;
		global $supportEmailArray;
		global $networkArray;
		global $cardAdminEmailArray;
		
		$countryAbbreviation=substr($itemID,0,2);
		$cardAdminEmail=$cardAdminEmailArray[$countryAbbreviation];
		$currency=$countryInfo[$countryAbbreviation]["currency"]; //echo $currency."<br>";
		//also obtain the countries support email from the array
		$support=$supportEmailArray[$countryAbbreviation]["email"]; //echo $support."<br>";
		if($support==''){$support=SUPPORT_EMAIL;}
		//extract homepage from array
		$homePage=$countryInfo[$countryAbbreviation]["homePage"]; //echo $homePage."<br>";
		if($homePage==''){$homePage="www.sendairtime.com";}//default
		//extract load instruction code
		$networkCode=substr($itemID,0,5);
		$loadCode=$networkArray[$countryAbbreviation][$networkCode]["loadCode"];
		
		//$toAddress="client@sendairtime.com";
		//dispatch the card to the client and the receiver
		$subject = "$homePage: Airtime Card Details";
		$message = "Dear $firstName, \n"; 
		$message.= "We wish to inform you that we have received your order. \r\n";
		$message.= "This is only an initial order notification and the airtime will be loaded once you complete the form \r\n";
		$message.= "Below you will find the details of the product you purchased: \r\n";
		$message.= "-------ORDER DETAILS-------------------\n";
		$message.= "Network: $networkName\r\n";
		$message.= "Airtime Code: AUTOMATIC\r\n";
		$message.= "Amount in ".$currency.": $amount \r\n";
		$message.= "Amount in USD: $usAmount \r\n";
		$message.= "Order Number: $transid \r\n";
		$message.= "--------------------------------------\n\n";
		$message.= "Incase you have any questions you can contact customer service at ".$support." \r\n";
		$message.= "Cheers -- Buy airtime online at ".$homePage."\n";
		sendEmail2($fromName,$fromAddress,$toAddress,$cardAdminEmail,$subject,$message,$countryAbbreviation);					
	}

	//modified for modularity
	//function to inform client that their the airtime will be directly loaded onto the receiver's phone
	function sendSecondAutoEmail2Client($toAddress,$receiverFirstName,$firstName,$itemID,$transID,$receiverEmail,$receiverPhone){

		//get amount, networkName, and Price in USD
		$amount=getValueInUGSh($itemID);
		$networkName=getNetworkName($itemID);
		$usAmount=getPriceInUSD($itemID);
		
		//determine the countries specifics from the array
		global $countryInfo;
		global $supportEmailArray;
		global $networkArray;
		global $cardAdminEmailArray;
		
		$countryAbbreviation=substr($itemID,0,2);
		$cardAdminEmail=$cardAdminEmailArray[$countryAbbreviation];
		$currency=$countryInfo[$countryAbbreviation]["currency"]; //echo $currency."<br>";
		//also obtain the countries support email from the array
		$support=$supportEmailArray[$countryAbbreviation]["email"]; //echo $support."<br>";
		if($support==''){$support=SUPPORT_EMAIL;}
		//extract homepage from array
		$homePage=$countryInfo[$countryAbbreviation]["homePage"]; //echo $homePage."<br>";
		if($homePage==''){$homePage="www.sendairtime.com";}//default
		//extract load instruction code
		$networkCode=substr($itemID,0,5);
		$loadCode=$networkArray[$countryAbbreviation][$networkCode]["loadCode"];
		
		//$toAddress="client@sendairtime.com";
		//dispatch the no cards email to the client
		$subject = "$homePage: Airtime Card Details";
		$message = "Dear $firstName, \n"; 
		$message.= "Thank you for completing the form we now have the details of \r\n";
		$message.= "the receiver and the airtime will be automatically loaded\r\n";
		$message.= "Below are the order details:\r\n";		
		$message.= "-------ORDER DETAILS-------------------\n";
		$message.= "Network: $networkName\r\n";
		$message.= "Airtime Code: AUTOMATIC\r\n";
		$message.= "Amount in ".$currency.": $amount \r\n";
		$message.= "Amount in USD: $usAmount \r\n";
		$message.= "Order Number: $transID \r\n";
		$message.= "Receiver's Name: $receiverFirstName \r\n";
		$message.= "Receiver's Email: $receiverEmail \r\n";
		$message.= "Receiver's Phone: $receiverPhone \r\n";
		$message.= "--------------------------------------\n\n";
		$message.= "You can contact us at ".$support." incase of anything \r\n";
		$message.= "please have the Order Number above ready.\n\n";
		$message.= "Cheers -- Buy airtime online at ".$homePage."\n";
		sendEmail2($fromName,$fromAddress,$toAddress,'',$subject,$message,$countryAbbreviation);
	}
	
	

	function sendReferralEmail($referredName,$senderName,$senderEmail,$referredEmail1,$body,$countryAbbreviation){
		//determine the countries specifics from the array
		global $countryInfo;
		global $supportEmailArray;
		global $networkArray;

		//extract homepage from array
		$homePage=$countryInfo[$countryAbbreviation]["homePage"]; //echo $homePage."<br>";
		if($homePage==''){$homePage="www.sendairtime.com";}//default
		$country=$countryInfo[$countryAbbreviation]["country"];
		
		$subject="$homePage referral from $senderName";
		if($body=='Type personalized message to your friend here or simply leave blank and default message will be sent'){
			$body="Hi $referredName, \n";
			$body.="Did you know that you can buy airtime or sendairtime to ".$country." instantly at ".$homePage."\n";
			$body.="Take advantage of this great service today.";
			$body.="--$senderName";
		}
		else{
			$body.="\n Send airtime to ".$country." or Buy airtime online at ".$homePage;
		}
		sendEmail($senderName,$senderEmail,$referredEmail1,'',$subject,$body);
		
	}
	
	function sendReferralEmail2($referredName,$senderName,$senderEmail,$referredEmail1,$body,$countryAbbreviation){
		//determine the countries specifics from the array
		global $countryInfo;
		global $supportEmailArray;
		global $networkArray;
		//$countryAbbreviation=substr($itemID,0,2); //echo $countryAbbreviation."<br>";
		//also obtain the countries support email from the array
		$support=$supportEmailArray[$countryAbbreviation]["email"]; //echo $support."<br>";
		if($support==''){$support=SUPPORT_EMAIL;}
		//extract homepage from array
		$homePage=$countryInfo[$countryAbbreviation]["homePage"]; //echo $homePage."<br>";
		if($homePage==''){$homePage="www.sendairtime.com";}//default
		$country=$countryInfo[$countryAbbreviation]["country"];
		
		$subject="$homePage referral from $senderName";
		if($body=='Type personalized message to your friend here or simply leave blank and default message will be sent'){
			$body="Hi $referredName, \n";
			$body.="Did you know that you can buy airtime or sendairtime to ".$country." instantly at ".$homePage."\n";
			$body.="Take advantage of this great service today.";
			$body.="--$senderName";
		}
		else{
			$body.="\n Send airtime to ".$country." or Buy airtime online at ".$homePage;
		}
		sendEmail($senderName,$senderEmail,$referredEmail1,'',$subject,$body);
		
	}
	
	function sendTextUsingClick($phone,$textMessage){
		// Turn off all error reporting
		error_reporting(0);
		
		$user = CLICKATELL_USER;
		$password = CLICKATELL_PASSWD;
		$baseurl ="http://api.clickatell.com";
		$text = urlencode("$textMessage");
		$from = urlencode("3nitylabs");
		$api_id = "2600844";
		$confirmationMsg="";
		$numberPrefix = substr($phone,0,5);
		
		global $clickAPIID;
		$api_id = $clickAPIID[$numberPrefix];
		if($api_id==''){$api_id = "2737070";$phone=substr($phone,2);}
		
		$to = $phone;
		// auth call
		$url = "$baseurl/http/auth?user=$user&password=$password&api_id=$api_id";
		// do auth call
		$ret = file($url);
		// split our response. return string is on first line of the data returned
		$sess = split(":",$ret[0]);
		if ($sess[0] == "OK") {
			$sess_id = trim($sess[1]); // remove any whitespace
			$url = "$baseurl/http/sendmsg?session_id=$sess_id&to=$to&text=$text&from=$from";
			// do sendmsg call
			$ret = file($url);
			$send = split(":",$ret[0]);
			if ($send[0] == "ID"){
				//$confirmationMsg.="success<br>message ID: ". $send[1];
				$_SESSION['clickMessageID']="".$send[1];
				if($to=="+15149631699"){
					//do nothing
					//no print out due to live stuff
					$confirmationMsg.="SMS sent to $to ";
				}
				else{
					$confirmationMsg.="SMS sent to $to ";
					//sendEmail("sendTextUsingClick","",$toAddress,$bcc,$subject,$body)
				}
			}
			else{
				$confirmationMsg.="<li>SMS Unavailable try again l8r </li>";
				//echo "<li>Send message failed, please contact support@sendairtime.com </li>";
			}
		} 
		else {
			$confirmationMsg.="Authentication failure: ". $ret[0];
			exit();
		}
		return $confirmationMsg;
	}
	
	function sendTextUsingClick2($phone,$textMessage){
		// Turn off all error reporting
		error_reporting(0);		
		$user = CLICKATELL_USER;
		$password = CLICKATELL_PASSWD;
		$baseurl ="http://api.clickatell.com";
		$text = urlencode("$textMessage");
		$from = urlencode("3nitylabs");
		$api_id = "2600844";
		$confirmationMsg="";
		$numberPrefix = substr($phone,0,5);
		
		global $clickAPIID;
		$api_id = $clickAPIID[$numberPrefix];
		if($api_id==''){$api_id = "2737070";}
		
		$to = $phone;
		//echo "$phone";
		// auth call
		$url = "$baseurl/http/auth?user=$user&password=$password&api_id=$api_id";
		// do auth call
		$ret = file($url);
		// split our response. return string is on first line of the data returned
		$sess = split(":",$ret[0]);
		if ($sess[0] == "OK") {
			$sess_id = trim($sess[1]); // remove any whitespace
			$url = "$baseurl/http/sendmsg?session_id=$sess_id&to=$to&text=$text&from=$from";
			// do sendmsg call
			$ret = file($url);
			$send = split(":",$ret[0]);
			if ($send[0] == "ID"){
				//$confirmationMsg.="success<br>message ID: ". $send[1];
				$_SESSION['clickMessageID']="".$send[1];
				if($to=="+15149631699"){
					//do nothing
					//no print out due to live stuff
					$confirmationMsg.="SMS sent to $to ";
				}
				else{
					$confirmationMsg.="SMS sent to $to ";
					//sendEmail("sendTextUsingClick","",$toAddress,$bcc,$subject,$body)
				}
			}
			else{
				$confirmationMsg.="<li>SMS Unavailable try again l8r </li>";
				//echo "<li>Send message failed, please contact support@sendairtime.com </li>";
			}
		} 
		else {
			$confirmationMsg.="Authentication failure: ". $ret[0];
			exit();
		}
		return $confirmationMsg;
	}
	
	
	
	function sendText2Phone($receiverPhone,$textMessage){
		//1. here we assuming that the sms form has already checked the following:
		//that the number entered is either 077,078 or 071
		//am not sure if celtel supports Email2SMS
		//mtn is 0782555555@mtnconnect.co.ug
		//utl is 0712555555@sms.ugandatelecom.com
		
		//set this flags accordingly based on whether mtn utl or ctl have issues with their email
		$mtnPhoneEmailWorking=false;
		$utlPhoneEmailWorking=false;
		$ctlPhoneEmailWorking=false;
		$fidoPhoneEmailWorking=false;

		$debug='false';
		if($debug == 'true'){echo("<span class=\"style7\"> phoneNumber: $phoneNumber </span> <br>");}
		$numberPrefix = substr($receiverPhone,0,6);
		$shortNumber = substr($receiverPhone,4);
		$twelveDigitPhoneFormat=substr($receiverPhone,1);
		$countryCode=substr($receiverPhone,1,3);
		$confirmationMsg="";
		//comment out when testing
		//$twelveDigitPhoneFormat="+15149631699";
		if($debug == 'true'){echo"twelve digit phone format = $twelveDigitPhoneFormat<br>";}

		
		if($debug == 'true'){echo("<span class=\"style7\"> numberPrefix: $numberPrefix </span>  <br>");}
		if($debug == 'true'){echo("<span class=\"style7\"> shortNumber: $shortNumber </span>  <br>");}
		
		
		$textMessage="Sendairtime.com:".$textMessage;
		
		$receiverPhoneEmail='';
		
		if($numberPrefix=='+25677' || $numberPrefix=='+25678'){
			if($mtnPhoneEmailWorking){
				$receiverPhoneEmail=sprintf("0%d@mtnconnect.co.ug",$shortNumber);
				if($debug == 'true'){echo"mtn receiverPhone = $receiverPhoneEmail";}
				$receiverPhoneEmail="phone@sendairtime.com";
				sendEmail('','sms@sendairtime.com',$receiverPhoneEmail,'','',$textMessage);				
			}
			else{
				//if mtn's email system is down like it was in July 2006
				$confirmationMsg=sendTextUsingClick2($twelveDigitPhoneFormat,$textMessage);
			}
			//echo"Your SMS is on its way to $twelveDigitPhoneFormat<br>";
		}
		else if ($numberPrefix=='+25671'){//071234567@sms.ugandatelecom.com
			if($utlPhoneEmailWorking){//071234567@utmobile.com 
				$receiverPhoneEmail=sprintf("0%d@utmobile.ug",$shortNumber);
				if($debug == 'true'){echo"utl receiverPhone = $receiverPhoneEmail";}
				$receiverPhoneEmail="phone@sendairtime.com";				
				sendEmail('','sms@sendairtime.com',$receiverPhoneEmail,'','',$textMessage);
			}
			else{//email to phone system is down
				$confirmationMsg=sendTextUsingClick2($twelveDigitPhoneFormat,$textMessage);
			}
			//echo"Your SMS is on its way to $twelveDigitPhoneFormat<br>";
		}
		else if ($numberPrefix=='+25675'){
			if($ctlPhoneEmailWorking){
				$receiverPhoneEmail=sprintf("0%d@ug.celtel.com",$shortNumber);
				if($debug == 'true'){echo"ctl receiverPhone = $receiverPhoneEmail";}
				$receiverPhoneEmail="phone@sendairtime.com";
				sendEmail('','sms@sendairtime.com',$receiverPhoneEmail,'','',$textMessage);
			}
			else{//email to phone system is down at celtel
				$confirmationMsg=sendTextUsingClick2($twelveDigitPhoneFormat,$textMessage);
			}
			//echo"Your SMS is on its way to $twelveDigitPhoneFormat<br>";
		}
		else if ($numberPrefix=='+15149'){
			if($fidoPhoneEmailWorking){
				$receiverPhoneEmail=sprintf("%s@fido.ca",$receiverPhone);
				echo"fido receiverPhone = $receiverPhoneEmail";
				sendEmail('','sms@sendairtime.com',$receiverPhoneEmail,'','',$textMessage);
				//echo"Your SMS is on its way to $twelveDigitPhoneFormat<br>";
			}
			else{
				$confirmationMsg=sendTextUsingClick2($twelveDigitPhoneFormat,$textMessage);
			}
		}
		else {
			//this is another cellphone network, for now we do not know their email2Phone syntax
			$confirmationMsg=sendTextUsingClick2($twelveDigitPhoneFormat,$textMessage);
			//echo("<span class=\"style7\"> You entered a phone number that is not supported </span>");
		}
		return $confirmationMsg;
	}
	
	
	
	function sendEmail2Phone($receiverPhone,$receiverFirstName,$sendersName,$pin2Send,$amount,$itemID){
		
		//get amount, networkName, and Price in USD
		$amount=getValueInUGSh($itemID);
		$networkName=getNetworkName($itemID);
		$usAmount=getPriceInUSD($itemID);
		
		//determine the countries specifics from the array
		global $countryInfo;
		global $supportEmailArray;
		global $networkArray;
		global $cardAdminEmailArray;
		
		$countryAbbreviation=substr($itemID,0,2);

		$currency=$countryInfo[$countryAbbreviation]["currency"]; //echo $currency."<br>";
		//also obtain the countries support email from the array
		$support=$supportEmailArray[$countryAbbreviation]["email"]; //echo $support."<br>";
		if($support==''){$support=SUPPORT_EMAIL;}
		//extract homepage from array

		//extract load instruction code
		$networkCode=substr($itemID,0,5);
		$loadCode=$networkArray[$countryAbbreviation][$networkCode]["loadCode"];
		
		
		
		$fromAddress=$support;
		//$toAddress="phone@sendairtime.com";
		$phoneSubject = 'sendairtime.com';
		if($pin2Send==''){
			$receiverPhoneMessage = sprintf("Hi $receiverFirstName, %s just bought airtime 4 u worth %s $currency, however there is a delay we'll mail u the PIN in a few hours sorry 4 the delay.", $sendersName, $amount, $pin2Send);	
		}
		else{
		//Hi Arturo, u rcvd 20000UGX airtime from Paul, the code is:122 , to load dial *111*122#.  [sendairtime.com]
			if(substr($receiverPhone,0,6)=='+25677'||substr($receiverPhone,0,6)=='+25678'){//access number
				$receiverPhoneMessage= sprintf("Hi $receiverFirstName, u have received %sUGX airtime from %s, the code is:%s , to load dial *155*$pin2Send#.",$amount,$sendersName, $pin2Send);	
			}
			elseif(substr($receiverPhone,0,6)=='+25671'){//recharge code
				$receiverPhoneMessage= sprintf("Hi $receiverFirstName, u have received %sUGX airtime from %s, the code is:%s , to load dial *111*$pin2Send#.",$amount,$sendersName,$pin2Send);	
			}
			elseif(substr($receiverPhone,0,6)=='+25675'){//access number
				$receiverPhoneMessage= sprintf("Hi $receiverFirstName, u have received %sUGX airtime from %s, the code is:%s , to load dial *130*$pin2Send#.",$amount,$sendersName,$pin2Send);	
			}
			else{
				$receiverPhoneMessage= "Hi ".$receiverFirstName.", u have received ".$amount.$currency." airtime from ".$sendersName.", the code is:".$pin2Send." , to load dial ".$loadCode.$pin2Send."#.";	
			}
			//$receiverPhoneMessage = sprintf("Hi $receiverFirstName, I just bought u airtime worth %sUGX, the airtime code is:$pin2Send , to load dial *155#$pin2Send. - $sendersName [www.sendairtime.com]",$amount);	
		}
		//sendEmail($fromName,$fromAddress,$toAddress,'',$phoneSubject,$receiverPhoneMessage);
		$confirmationMsg=sendAT2Phone($receiverPhone,$receiverPhoneMessage);
		return $confirmationMsg;
	}//end sendEmail2Phone function
	
	//sends airtime to a phone
	function sendAT2Phone($receiverPhone,$textMessage){
		//1. here we assuming that the sms form has already checked the following:
		//that the number entered is either 077,078 or 071
		//am not sure if celtel supports Email2SMS
		//mtn is 0782555555@mtnconnect.co.ug
		//utl is 0712555555@sms.ugandatelecom.com
		
		//set this flags accordingly based on whether mtn utl or ctl have issues with their email
		$mtnPhoneEmailWorking=false;
		$utlPhoneEmailWorking=false;
		$ctlPhoneEmailWorking=false;
		$fidoPhoneEmailWorking=false;

		$debug='false';
		if($debug == 'true'){echo("<span class=\"style7\"> phoneNumber: $phoneNumber </span> <br>");}
		$numberPrefix = substr($receiverPhone,0,6);
		$shortNumber = substr($receiverPhone,4);
		$twelveDigitPhoneFormat=substr($receiverPhone,1);
		$countryCode=substr($receiverPhone,1,3);
		$confirmationMsg="";
		//comment out when testing
		//$twelveDigitPhoneFormat="+15149631699";
		if($debug == 'true'){echo"twelve digit phone format = $twelveDigitPhoneFormat<br>";}

		
		if($debug == 'true'){echo("<span class=\"style7\"> numberPrefix: $numberPrefix </span>  <br>");}
		if($debug == 'true'){echo("<span class=\"style7\"> shortNumber: $shortNumber </span>  <br>");}
		
		
		$textMessage.=":This was sent using Ghanaairtime.com:";
		
		$receiverPhoneEmail='';
		
		if($numberPrefix=='+25677' || $numberPrefix=='+25678'){
			if($mtnPhoneEmailWorking){
				$receiverPhoneEmail=sprintf("0%d@mtnconnect.co.ug",$shortNumber);
				if($debug == 'true'){echo"mtn receiverPhone = $receiverPhoneEmail";}
				$receiverPhoneEmail="phone@sendairtime.com";
				sendEmail('','sms@sendairtime.com',$receiverPhoneEmail,'','',$textMessage);				
			}
			else{
				//if mtn's email system is down like it was in July 2006
				$confirmationMsg=sendATUsingClick($twelveDigitPhoneFormat,$textMessage);
			}
			//echo"Your SMS is on its way to $twelveDigitPhoneFormat<br>";
		}
		else if ($numberPrefix=='+25671'){//071234567@sms.ugandatelecom.com
			if($utlPhoneEmailWorking){//071234567@utmobile.com 
				$receiverPhoneEmail=sprintf("0%d@utmobile.ug",$shortNumber);
				if($debug == 'true'){echo"utl receiverPhone = $receiverPhoneEmail";}
				$receiverPhoneEmail="phone@sendairtime.com";				
				sendEmail('','sms@sendairtime.com',$receiverPhoneEmail,'','',$textMessage);
			}
			else{//email to phone system is down
				$confirmationMsg=sendATUsingClick($twelveDigitPhoneFormat,$textMessage);
			}
			//echo"Your SMS is on its way to $twelveDigitPhoneFormat<br>";
		}
		else if ($numberPrefix=='+25675'){
			if($ctlPhoneEmailWorking){
				$receiverPhoneEmail=sprintf("0%d@ug.celtel.com",$shortNumber);
				if($debug == 'true'){echo"ctl receiverPhone = $receiverPhoneEmail";}
				$receiverPhoneEmail="phone@sendairtime.com";
				sendEmail('','sms@sendairtime.com',$receiverPhoneEmail,'','',$textMessage);
			}
			else{//email to phone system is down at celtel
				$confirmationMsg=sendATUsingClick($twelveDigitPhoneFormat,$textMessage);
			}
			//echo"Your SMS is on its way to $twelveDigitPhoneFormat<br>";
		}
		else if ($numberPrefix=='+15149'){
			if($fidoPhoneEmailWorking){
				$receiverPhoneEmail=sprintf("%s@fido.ca",$receiverPhone);
				echo"fido receiverPhone = $receiverPhoneEmail";
				sendEmail('','sms@sendairtime.com',$receiverPhoneEmail,'','',$textMessage);
				//echo"Your SMS is on its way to $twelveDigitPhoneFormat<br>";
			}
			else{
				$confirmationMsg=sendATUsingClick($twelveDigitPhoneFormat,$textMessage);
			}
		}
		else if ($numberPrefix=='+23320'){//send onetouch airtime using clickatel
			$confirmationMsg=sendATUsingRouteSMS($twelveDigitPhoneFormat,$textMessage);
		}
		else if ($numberPrefix=='+23324'){//send areeba airtime using clickatel
			$confirmationMsg=sendATUsingRouteSMS($twelveDigitPhoneFormat,$textMessage);
		}
		else {
			//this is another cellphone network, for now we do not know their email2Phone syntax
			$confirmationMsg=sendATUsingRouteSMS($twelveDigitPhoneFormat,$textMessage);
			//echo("<span class=\"style7\"> You entered a phone number that is not supported </span>");
		}
		return $confirmationMsg;
	}//end sendAT2Phone($receiverPhone,$textMessage)
	
	function sendATUsingClick($phone,$textMessage){
		// Turn off all error reporting
		error_reporting(0);
		$user = CLICKATELL_USER;
		$password = CLICKATELL_PASSWD;
		$baseurl ="http://api.clickatell.com";
		$text = urlencode("$textMessage");
		$from = urlencode("gotairtime");
		$api_id = "2600844";
		$confirmationMsg="";
		$numberPrefix = substr($phone,0,5);
		$clickMessageID="";
		
		//by default we will send the airtime through another api
		//different from the ones used for text messaging.
		//global $clickAPIID;
		//$api_id = $clickAPIID[$numberPrefix];
		//if($api_id==''){$api_id = "2737070";}
		
		$to = $phone;
		//echo "$phone";
		// auth call
		$url = "$baseurl/http/auth?user=$user&password=$password&api_id=$api_id";
		// do auth call
		$ret = file($url);
		// split our response. return string is on first line of the data returned
		$sess = split(":",$ret[0]);
		if ($sess[0] == "OK") {
			$sess_id = trim($sess[1]); // remove any whitespace
			$url = "$baseurl/http/sendmsg?session_id=$sess_id&to=$to&text=$text&from=$from";
			// do sendmsg call
			$ret = file($url);
			$send = split(":",$ret[0]);
			if ($send[0] == "ID"){
				//$confirmationMsg.="success<br>message ID: ". $send[1];
				$clickMessageID="".$send[1];
				if($to=="+15149631699"){
					//do nothing
					//no print out due to live stuff
					$confirmationMsg.="SMS sent to $to ";
				}
				else{
					$confirmationMsg.="SMS sent to $to ";
					//sendEmail("sendTextUsingClick","",$toAddress,$bcc,$subject,$body)
				}
			}
			else{
				$confirmationMsg.="<li>SMS Unavailable try again l8r </li>";
				//echo "<li>Send message failed, please contact support@sendairtime.com </li>";
			}
		} 
		else {
			$confirmationMsg.="Authentication failure: ". $ret[0];
			exit();
		}
		return $clickMessageID;
	}
	function sendATUsingIts4SMS($phone,$textMessage){
		// Turn off all error reporting
		error_reporting(0);
		$user = antozi;
		$password = "62738";
		$baseurl ="http://Its4sms.com/sendsms.asp";
		$text = urlencode("$textMessage");
		$from = urlencode("gotairtime");
		$api_id = "1630";
		$confirmationMsg="";
		$numberPrefix = substr($phone,0,5);
		$clickMessageID="";
		
		//by default we will send the airtime through another api
		//different from the ones used for text messaging.
		//global $clickAPIID;
		//$api_id = $clickAPIID[$numberPrefix];
		//if($api_id==''){$api_id = "2737070";}
		
		//http://its4sms.com/sendsms.asp?user=antozi&password=62738&sid=1630&text=Test&PhoneNumber=15149631699
		
		$to = $phone;
		//echo "$phone";
		// auth call
		$url = "$baseurl?user=$user&password=$password&sid=$api_id&text=$text&PhoneNumber=$to";
		// do auth call
		$ret = file($url);
		// split our response. return string is on first line of the data returned
		$sess = split(":",$ret[0]);
		$clickMessageID = $sess[0];		
		echo "test: $clickMessageID";
		return $clickMessageID;
	}
	function sendATUsingRouteSMS($phone,$textMessage){
		// Turn off all error reporting
		error_reporting(0);
		$user = "ghanaairtime";
		$password = "ghana21";
		
		//http://202.63.160.78/bulksms/bulksms.asp?username=john&password=john&messag
		//e=Text test mssage&mobile=447987342234&sender=jeni&type=0&route=1
		
		$baseurl ="http://203.199.162.43/bulksms/bulksms.asp";
		$text = urlencode("$textMessage");
		$from = urlencode("Ghanaairtime");
		$api_id = "1630";
		$confirmationMsg="";
		$numberPrefix = substr($phone,0,5);
		$clickMessageID="";
		
		//by default we will send the airtime through another api
		//different from the ones used for text messaging.
		//global $clickAPIID;
		//$api_id = $clickAPIID[$numberPrefix];
		//if($api_id==''){$api_id = "2737070";}
		
		$to = $phone;
		//echo "$phone";
		// auth call
		$url = "$baseurl?username=$user&password=$password&message=$text&mobile=$to&sender=$from&type=0&route=18";
		// do auth call
		$ret = file($url);
		// split our response. return string is on first line of the data returned
		$sess = split(":",$ret[0]);
		$confirmationID = $sess[0];		
		//echo "$url <br>";
		//echo "test: $clickMessageID";
		return $confirmationID;
	}
	
	function sendTextUsingRouteSMS($phone,$textMessage){
		// Turn off all error reporting
		error_reporting(0);
		$user = "ghanaairtime";
		$password = "ghana21";
		
		//http://202.63.160.78/bulksms/bulksms.asp?username=john&password=john&messag
		//e=Text test mssage&mobile=447987342234&sender=jeni&type=0&route=1
		
		$baseurl ="http://203.199.162.43/bulksms/bulksms.asp";
		$text = urlencode("$textMessage");
		$from = urlencode("Ghanaairtime");
		$api_id = "1630";
		$confirmationMsg="";
		$numberPrefix = substr($phone,0,5);
		$clickMessageID="";
		
		//by default we will send the airtime through another api
		//different from the ones used for text messaging.
		//global $clickAPIID;
		//$api_id = $clickAPIID[$numberPrefix];
		//if($api_id==''){$api_id = "2737070";}
		
		$to = $phone;
		//echo "$phone";
		// auth call
		$url = "$baseurl?username=$user&password=$password&message=$text&mobile=$to&sender=$from&type=0&route=18";
		// do auth call
		$ret = file($url);
		// split our response. return string is on first line of the data returned
		$sess = split(":",$ret[0]);
		$confirmationID = $sess[0];		
		//echo "$url <br>";
		//echo "test: $clickMessageID";
		return $confirmationID;
	}
?>