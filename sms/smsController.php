<?php

	if($_POST['Submit']){
		// we must never forget to start the session
		session_start();
		
		include_once"Messenger.php";
		include_once"../src/SessionFunctions.php";
		include_once"../src/GeneralFunctions.php";
		include_once"../src/constants.php";
		//SMSAdvertising
		$smsAdOn=false;
		$SMSAdMessage="Did u know that people living abroad can send you airtime ";
		$SMSAdMessage.="instantly from the internet at www.sendairtime.com to MTN,UTL,CelTel?";
		
		
		$phoneNumber = trim($_POST['to_phone']);
		$smsContent = trim($_POST['message']);
		$clientEmailIN = trim($_POST['your_email']); //client email only has value at log in
		$clientEmail ="";//variable to keep track of client email
		$clientPassword =trim($_POST['your_password']);
		$authenticated=false;
		$debug=false;
		$ip=getIP();
		$confirmationMsg="Sorry your SMS was not sent";
		$month=date("F");
		
		//by this point the javascript should have already verified the format of the email
		
		//PERFORM AUTHENTICATION
		//$clientFirstName="";//variable to be passed by reference use session instead of pass 
		//by reference
		if(!$_SESSION['smsLoggedIn']){
			//this function sets $_SESSION['smsLoggedIn']
			$authenticated=authenticateSMS($clientEmailIN,$clientPassword);//,& $clientFirstName);
			//SMSAdvertising
			$_SESSION['adAlreadySent']=false;
		}
		
		if($_SESSION['smsLoggedIn']){//user is authenticated - Auth. Success
			
			//if($debug){echo"Authentication Successful <br>";}
			
			//CHECK USERS COUNTRY
			//$clientsCountry = geoip_country_name_by_name($ip);
			//$handle = fopen("http://ip-to-country.directi.com/country/name/".$ip, 'r');
			//echo fgets($handle, 4096);
			//fclose($handle);
			//since 99% of our clients leave out of Uga
			//if($country!="UGA"){/*do not check smsCreditLimit*/}
			//else{/*Country is UGA hence check smsCreditLimit*/}
								
			//CHECK USERS CREDITS
			$smsCreditsLeft=$_SESSION['smsCreditsLeft'];
			//$smsCreditsLeft=0;
			if($smsCreditsLeft>0){//FSM: authsuccess && creditsLeft
				
				//$smsContent.='-'.$sendersName;
				
				//SEND SMS Msg
				if(!$debug){$confirmationMsg=sendText2Phone($phoneNumber,$smsContent);}
				//get the smsGatewayID
				$smsGatewayID=$_SESSION['clickMessageID'];
				
				//$smsGatewayID='asasfas';
				//echo"<br>Message ID is $smsGatewayID <br>";
					
				//UPDATE CREDITS: decrement credits by 1
				$clientEmail=$_SESSION['emailAddress'];
				if(!$debug){decrementCredits($clientEmail);}

				//LOG SMS SENT
				if(!$debug){logSMS($clientEmail, $smsGatewayID, $ip);}
				
				//creditsLeft minus 1
				$smsCreditsLeft-=1;
				
				//Messages to be parsed
				$_SESSION['smsCreditsLeft']-=1;//$smsCreditsLeft;
				$longMsg="You have $smsCreditsLeft free SMS credits left for $month";
				$_SESSION['confirmation']=$confirmationMsg;
				$_SESSION['longMsg']=$longMsg;				
				
				//SMSAdvertising: SEND SMS Advert Msg
				if(!$debug&&$smsAdOn&&!$_SESSION['adAlreadySent']){
					$confirmationMsg=sendText2Phone($phoneNumber,$SMSAdMessage);
					$_SESSION['adAlreadySent']=true;
				}
				
				//redirect to page with login
				if(!$debug){header("Location: ".SUCCESS_SMS_RETURN_PAGE);}
			}
			else{//REDIRECT TO PURCHASE CREDITS
				if($debug){echo"User has $smsCreditsLeft credits <br>";}
				
				//for now redirect to inform user he/she has run out of credits
				$outOfCreditsMsg="your monthly credits are over";
				$longMsg="Sorry you have exceeded your ".SMS_MAX_CREDITS." free credits for $month, please try again next month";
				$_SESSION['confirmation']=$outOfCreditsMsg;
				$_SESSION['longMsg']=$longMsg;
				if(!$debug){header("Location: ".PURCHASE_SMS_RETURN_PAGE);}
			}
		}
		else{//users credentials do not check out.
			//REDIRECT TO SIGN UP FORM
			$signupMsg="wrong email and password";
			$longMsg="Try again or sign up for free";
			//header("Location: ../signup/signup.php?msg=$signupMsg");
			if(!$debug){header("Location: ".SUCCESS_SMS_RETURN_PAGE."?confirmation=$signupMsg&longMsg=$longMsg");}
		}
		
		// if the user is logged in, unset the session for now till we implement the log in
		//this is only temporary
		//if (isset($_SESSION['smsLoggedIn'])) {
			//unset($_SESSION['smsLoggedIn']);
		//}
	}//end of POST
	else{//its not a POST
		//it could be a GET telling the controller to log the user out
		$smsCommand=trim($_GET['smsCommand']);
		if($smsCommand=='logout'){
			session_start();
			// if the user is logged in, unset the session for now till we implement the log in
			//this is only temporary
			if (isset($_SESSION['smsLoggedIn'])) {
				unset($_SESSION['smsLoggedIn']);
				
				$logoutMsg="You have logged out";
				//$longMsg="Try again or sign up for free";
				//header("Location: ../signup/signup.php?msg=$signupMsg");
				if(!$debug){header("Location: ".SUCCESS_SMS_RETURN_PAGE."?confirmation=$logoutMsg");}
			}
		}
		else{
		}
	}
?>