<?
    //test php mailer
    ini_set("include_path", ".:/var/www/vhosts/sendsente.com/httpdocs/lib/phpmailer");
    require_once("/var/www/vhosts/sendsente.com/httpdocs/lib/phpmailer/class.phpmailer.php");
    
    function send_email($input_array)
    {
        $from_name = $input_array['from_name'];
        $from_address = $input_array['from_address'];
        $to_address = $input_array['to_address'];
        $bcc = $input_array['bcc'];
        $subject = $input_array['subject'];
        $body = $input_array['body'];
    
		$mail = new PHPMailer();
		if($bcc==''){$bcc=RECORDS_EMAIL;}
		if($from_address==''){$from_address=FROM_EMAIL;}
		$mail->IsSMTP();                                   // send via SMTP
		$mail->Host     = SMTP_SERVER; // SMTP servers
		$mail->SMTPAuth = true;     // turn on SMTP authentication
		$mail->Username = SUPPORT_EMAIL;  // SMTP username
		$mail->Password = SUPPORT_PASSWD; // SMTP password

		$mail->From     = $from_address;
		if($from_name==''){$from_name=FROM_NAME;}
		$mail->FromName = $from_name;
		$mail->AddAddress($to_address);               // optional name
		//$mail->AddReplyTo($from_address);
		$mail->AddBCC($bcc);

		//$mail->WordWrap = 50;                              // set word wrap
		$mail->IsHTML(false);                               // send as HTML

		$mail->Subject  =  $subject;
		$mail->Body     =  $body;
		//$mail->AltBody  =  "This is the text-only body";
		//$mail->ClearCustomHeaders();
		
		if(!$mail->Send())
		{
 		  	//echo "Message was not sent <p>";
   			//echo "Mailer Error: " . $mail->ErrorInfo;
  		 	//exit;
			return false;
		}

		//echo "Message has been sent<br>";
		return true;
	}
?>