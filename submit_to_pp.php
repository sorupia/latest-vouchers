<?php
/*******************************************************************************
**   FILE: submit_to_pp.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Handles display/positioning of the submit loading 
**   screen and calling the papyal form processing script
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   
**
**   ADAPTED BY: Philip Mbonye (PBT-AVMM, Kampala) DATE: 30.Nov.2015
**
*********************************************************************************/
?>
<? 
	include("wrapper/header.php");

    if(isset($_POST['network_id']))
    {
        session_start();

    
       include $_SERVER['DOCUMENT_ROOT']."/src/Order/process_submit_order_form_vchr.php" ;
    }
	else
    {
		header("location:receiver_vchr.php?msg=no_selection#show");
	}
	
	include( ACTIVE_THEME_PATH . '/submit_to_pp.php');
	
	include("wrapper/footer.php"); 
?>
