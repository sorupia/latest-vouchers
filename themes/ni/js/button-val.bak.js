
function buyItem(name,row,td,id,shop,price,usprice){
	$("#"+td).hover(function(){
		$(this).addClass("col-md-8 btn btn-green btn-block").css({"background-color":"#00B800", "color":"#fff","padding": "0px 2px","font-size": "15px","text-align":"center"});
		$("#"+id).css({"padding": "0px 5px"});
		$("#network_id").val(shop);
		$("#price").val(price);
		$("#usprice").val(usprice);
		$("#network_name").val(name);
	}, function(){
		$(this).css({"background-color":"#fff", "color":"#747474"}).removeClass("col-md-8 btn btn-green btn-block");
		$("#"+id+",#"+td).removeAttr('style');
	});
}

function loadVouchers(id){
	$.ajax({
		
		url: 'vouchers-form.php',
		type: 'GET',
		data: {category_id:id},
		success: function(reponse) {
		//called when successful
		$('#fill').html(reponse);
		},
		error: function(error) {
		//called when there is an error
		$('#fill').html(error);
		}
	});
}
