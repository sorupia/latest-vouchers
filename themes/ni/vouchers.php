<?php
/*******************************************************************************
**   FILE: vouchers.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Part of index.php and used to display the vouchers section of the site
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 14.Jul.2015
**
**   MODIFIED BY: Arthur Ntozi (3nitylabs-AVMM, Kampala) DATE: 28.Nov.2015
**   Changed the way we check availability of voucher denominations since they
**   will be a purchase on demand item unlike airtime cards
**
**   MODIFIED BY:Arthur Ntozi (3nitylabs-AVMM, Kampala) DATE: 2015.12.01
**   Fixed a bug with the wrong hosted_button_id being passed
**
*********************************************************************************/

    //See wrapper/header.php for includes
    //Also see wrapper/header.php for siteTurnedON
    //Also see wrapper/header.php for country_is_blocked

    global $networkMap;
    global $networkArray;
    global $cardMap;
    $quantityOfCardsArray = array();
    $numberOfNetworks = count($networkMap[$countryAbbreviation]);
    $networkNames = array();

    //Check if IP is staff blocked
    $ipaddress     = $ipLocationArray['ip_address'];
    $ip_is_blocked = IPIsBlocked($ipaddress, $countryAbbreviation);

    //Check if IP belongs to a New Client
    //$ip_is_new_clients = isNewClientIP($ipaddress);

    //Check if Country is staff blocked
    $ipCountry          = $ipLocationArray['ip_country'];
    $country_is_blocked = countryIsBlocked($ipCountry,$countryAbbreviation);

    //Block Staff Blocked IP or Country
    if($ip_is_blocked || $country_is_blocked)
    {
        $siteTurnedON = false;
    }

    if($siteTurnedON)
    {
        //Loop through all the cards to see if they are still available
        //store the denomination quantities in the $quantityOfCardsArray array
        for($networkIndex = 0 ; $networkIndex < $numberOfNetworks ; $networkIndex++ )
        {
            $thisNetwork           = $networkMap[$countryAbbreviation][$networkIndex];
            $numberOfDenominations = count($cardMap[$countryAbbreviation][$thisNetwork]);
            $networkNames[]        = $networkArray[$countryAbbreviation]["$thisNetwork"]["networkName"];
            $categoryArray         = $networkArray[$countryAbbreviation]["$thisNetwork"]["category"];
            $thisCard              = $cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];

            // If this category is selected and there is no category match
            // skip this entry
            if (isset($_REQUEST['category']))
            {
                // Check if this category is contained in this category array
                if (!in_array( $_REQUEST['category'], $categoryArray  ))
                {
                    continue;
                }
            }

            if(CARD_LOAD_ENABLED)
            {
                //Search the DB only if we have airtime/recharge/topup/scratch cards enabled.
                $connection=connect2DB();
                    for( $j=0 ; $j < $numberOfDenominations ; $j++ )
                    {
                        $oneQuery = "SELECT itemID ";
                        $oneQuery.= "FROM NewCards ";
                        $oneQuery.= "WHERE ";
                        $oneQuery.= "NewCards.itemID = '$thisCard' ";
                        $oneQuery.= "AND ";
                        $oneQuery.= "NewCards.cardStatus != 'USED'";
                        $oneResult = mysql_query($oneQuery) or handleDatabaseError('' . mysql_error(),$oneQuery);
                        $quantityOfCardsArray["$thisCard"] = mysql_num_rows($oneResult);
                        mysql_free_result($oneResult);
                    }
                disconnectDB($connection);
            }
            else
            {
                $quantityOfCardsArray["$thisCard"] = 0;
            }
        }
    }
    else
    {
    }
    
    if($siteTurnedON)
    {
        //display regular prices
        //BEGIN FOR LOOPS
        echo '<form action="receiver_vchr.php" method="post">';
        echo '<input type="hidden" name="network_id" id="network_id" />';
        echo '<input type="hidden" name="network_name" id="network_name" />';
        echo '<input type="hidden" name="hosted_button_id" id="hosted_button_id" />';
        echo '<input type="hidden" name="receiver_amount_in_local" id="receiver_amount_in_local" />';
        echo '<input type="hidden" name="receiver_amount_in_foreign" id="receiver_amount_in_foreign" />';
        echo '<input type="hidden" name="item_id" id="item_id"  /> ';
        echo '<input type="hidden" name="item_name"  id="item_name" /> ';   
        echo '<input type="hidden" name="ip_address"  id="ip_address"   value="'.$ipLocationArray['ip_address'].'"/> ';
        echo '<input type="hidden" name="ip_country"  id="ip_country"   value="'.$ipLocationArray['ip_country'].'"/> ';
        echo '<input type="hidden" name="ip_state"    id="ip_state"     value="'.$ipLocationArray['ip_state'].'"/> ';
        echo '<input type="hidden" name="ip_city"     id="ip_city"      value="'.$ipLocationArray['ip_city'].'"/> ';                 

        echo '<div class="col-md-12  free">';

        $displayIndex = 0;
        for($networkIndex=0; $networkIndex < $numberOfNetworks; $networkIndex++)
        {

            //get the number of denominations
            $thisNetwork = $networkMap[$countryAbbreviation][$networkIndex];
            $numberOfDenominations = count($cardMap[$countryAbbreviation][$thisNetwork]);
            $networkName  = $networkArray[$countryAbbreviation]["$thisNetwork"]["networkName"];
            $categoryArray  = $networkArray[$countryAbbreviation]["$thisNetwork"]["category"];

            // If this category is selected and there is no category match
            // skip this entry
            if (isset($_REQUEST['category'])) {         
                // Check if this category is contained in this category array
                if (!in_array( $_REQUEST['category'], $categoryArray  )) {
                    continue;
                }
            }

            // Place 3 networks/supermarkets per row
            if ($displayIndex == 0 || ($displayIndex % 3) == 0 ) {
                echo '<div class="row special-pad">';       
            }

            echo '<div class="col-xs-4 col-sm-4 col-md-4 slim">';
                echo '<div class="panel panel-default">';
                    echo '<a href="merchant-profile.php?merchant='.$thisNetwork.'">';
                    echo '<img src="images/logos/'.$thisNetwork.'.jpg" class="img-responsive" style="width: 100%;" alt="'.$networkName.'" title="'.$networkName.' Profile"/>';
                    echo '</a>';
                     echo '<table class="table" id="denominations-table">';
                        echo '<tr></tr>';

                        for($denomination=0; $denomination < $numberOfDenominations; $denomination++)
                        {
                            $cardIndex = $cardMap[$countryAbbreviation]["$thisNetwork"]["$denomination"];
                            $priceInLocal = $networkArray[$countryAbbreviation]["$thisNetwork"][0]["$cardIndex"]["PriceInUGX"];
                            $priceInUSD = $networkArray[$countryAbbreviation]["$thisNetwork"][0]["$cardIndex"]["PriceInUSD"];
                            $item = $networkArray[$countryAbbreviation]["$thisNetwork"][0]["$cardIndex"]["Item"];
                            $item_name = "$priceInLocal $item";
                            $input_array['item_id'] = $cardIndex;

                            $shs = str_replace(LOCAL_CURRENCY ." ","",$priceInLocal);
                            $shs = str_replace(",","",$shs);
                            $td_id = str_replace(".","",$priceInUSD);

                            if($quantityOfCardsArray["$cardIndex"]>0)
                            {
                                $hosted_button_id = $networkArray[$countryAbbreviation]["$thisNetwork"][0]["$cardIndex"]["hosted_button_id"];
                                
                                echo '<tr id="'.$thisNetwork.$denomination.'" >';
                                echo '    <td align="center" id="'.$thisNetwork.$td_id.'">';
                                echo '        <input type="submit" 
                                                     id="'.$cardIndex.'" 
                                                     name="buy"  onmouseover="buyItem(\''.$hosted_button_id.'\',\''.$cardIndex.'\',\''.$item_name.'\',\''.$networkName.'\',\''.$thisNetwork.'\',\''.$shs.'\',\''.$priceInUSD.'\')" 
                                                     class="denomination" 
                                                     value="'.$priceInLocal.' &nbsp; '.$priceInUSD.' '. SALE_CURRENCY .'"/>
                                          </td>';
                                echo '</tr>';
                            }
                            else if(this_network_has_auto_load($input_array) && AUTO_LOAD_ENABLED)
                            {
                                $hosted_button_id = "";

                                //Convert Item ID to autoload
                                $converted_item_id = str_replace("card", "auto", $input_array['item_id']);
                                
                                echo '<tr id="'.$thisNetwork.$denomination.'" >';
                                echo '    <td align="center" id="'.$thisNetwork.$td_id.'">';
                                echo '        <input type="submit"
                                                     id="'.$cardIndex.'"
                                                     name="buy"  
                                                     onmouseover="buyItem(\''.$hosted_button_id.'\',\''.$cardIndex.'\',\''.$item_name.'\',\''.$networkName.'\',\''.$thisNetwork.'\',\''.$shs.'\',\''.$priceInUSD.'\')" 
                                                     class="denomination" 
                                                     value="'.$priceInLocal.' &nbsp; '.$priceInUSD.' '. SALE_CURRENCY .'"/></td>';
                                echo '</tr>';
                            }
                            else if(is_a_voucher($input_array) && VOUCHER_LOAD_ENABLED)
                            {
                                $hosted_button_id = "";

                                //Convert Item ID to voucherload if required
                                //Still investigating on the effect of this
                                $load_options = array("card","auto");
                                $converted_item_id = str_replace($load_options, "vchr", $input_array['item_id']);

                                echo '<tr id="'.$thisNetwork.$denomination.'" >';
                                echo '    <td align="center" id="'.$thisNetwork.$td_id.'">';
                                echo '        <input type="submit" 
                                                     id="'.$cardIndex.'" 
                                                     name="buy"  
                                                     onmouseover="buyItem(\''.$hosted_button_id.'\',\''.$cardIndex.'\',\''.$item_name.'\',\''.$networkName.'\',\''.$thisNetwork.'\',\''.$shs.'\',\''.$priceInUSD.'\')" 
                                                     class="denomination" 
                                                     value="'.$priceInLocal.' &nbsp; '.$priceInUSD.' '. SALE_CURRENCY .'"/>
                                          </td>';
                                echo '</tr>';
                            }
                            else
                            {
                                echo '<tr id="">';
                                echo '  <td align="center" id="">';
                                echo '      <a style="font-size:13px;">'.$priceInLocal.' &nbsp; SOLD OUT</a></td>';
                                echo '</tr>';
                            }
                        }

                    echo '</table>';
                echo '</div>';
            echo '</div>';

            // Place 3 networks/supermarkets per row
            //if (($displayIndex == 2) || ($displayIndex == ($numberOfNetworks - 1)) || (($displayIndex % 3) == 2) ) {
            if (($displayIndex % 3) == 2)  {
                echo '</div>';
            }
            
            // Increment the display index after a successful display
            $displayIndex++;
        }

        // We need to close the row div if we didnt have a multiple of 3 elements to display
        if ( ($displayIndex % 3) != 0) {
            echo '</div>';
        }
            
        echo '</div>';
        echo '</form>';
    
    }
    else if($ip_is_blocked || $country_is_blocked)
    {
        echo '<div class="alert alert-danger alert-dismissable">';
        echo '  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
        echo '      <h4><i class="icon fa fa-ban"></i> Alert!</h4>';
                echo 'Sorry voucher purchase is not available in ';
                echo 'your country or your first purchase has not been verified.<br/> ';
                echo 'Ask your family and friends abroad to buy '.PRODUCT.' for you at this website. ';
                echo 'In case you just made your first purchase please wait until it is verified<br/>';
                echo 'For more assistance in '.COUNTRY_NAME.' call<br/> ';
                echo SUPPORT_PHONE_1;
                echo '<br> ';
                echo '--'.SERVICED_BY.'--<br><br> ';
        echo '</div>';
    }
    else
    {
        echo '<div class="alert alert-danger alert-dismissable">';
        echo '  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
        echo '      <h4><i class="icon fa fa-ban"></i> Alert!</h4>';
                echo "<strong>Voucher Unavailable</strong><br/>";
                echo "We are currently experiencing problems with our server. ";
                echo "As a result the ".PRODUCT." System is unavailable until this issue is resolved. ";
                echo "Sorry about any inconvenience please try again later. <br/>";
                echo "--".SERVICED_BY."--<br/><br/>";
        echo '</div>';
    }
    
    echo '<script type="text/javascript" src="'.ACTIVE_THEME_PATH . '/js/button-val.js"></script>';
?>
