<div class="banner-bottom">
<div class="container">
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="col-sm-8 col-md-8 col-lg-8">
				<div class="col-sm-12 col-md-12 col-lg-12">
					<!--h3>
					UgandaVouchers.com is a service offered by 3nitylabs Inc., an Edmonton based 
					ICT company formed by a team of East African business and ICT professionals.
					</h3-->
					<h3><img src="images/email.png" id="contact-img" /> Email</h3>
					<p align="left" id="spacing">support@ugandavouchers.com</p>

					<h3><img src="images/Skype-icon.png" id="contact-img" /> Skype</h3>
					<p  align="left" id="spacing">ugandavouchers</p></br>

					<h3><img src="images/phone-icon.png" id="contact-img" /> Phone</h3>
					<p align="left" id="spacing">+1.617.340.2408</p>
					</br>

					<!--h3>North America</h3>
					<p align="left" id="spacing">
						3nitylabs Inc. 202-1945 104 Street NW,</br> Edmonton, AB, T6J 5M2, Canada 
					</p></br-->
					<h3><img src="images/home-icon.png" id="contact-img" /> Address</h3>
					<p align="left" id="spacing">
						Samburu Solutions Ltd Second Level,</br>
						Jofra House, 6 Kataza Close,</br>
						Kiswa, Kampala,</br>
						P.O. Box 29213, Uganda
					</p></br>
					<!--div class="row">
						<div class="col-sm-4 col-md-4 col-lg-4" style="padding-left:30px;">
							<a href="#"  class="btn btn-black btn-block">Contact Us</a>
						</div>
					</div-->
					<br/><br/>
				</div>
			</div>
			<div class="row">
				<?php include(ACTIVE_THEME_PATH ."/left-denominations-bar-others.php"); ?>			
			</div>
		</div>
	</div>
</div>