<div class="col-sm-4 col-md-4 col-lg-4">
	<div class="row">
		<div class="panel panel-default">				 			
			<div class="panel-body">
				<h4>Keep in touch with Relatives and friends in Uganda</h4></br>
				<p>A Convenient service from which you can buy a voucher for your relatives and friends in Uganda to use at <br><center><b><?php echo "$networkName"; ?></b></center></p>
				<form action="receiver_vchr.php" method="post" role="form">
					</br>							
					<!--<label for="name"><p>Select a Denomination</p></label>-->		
					<div class="form-group">					
						<?php 
						
						// this variable will tell subsequent logic to hide
						// the send now button if items are sold out
						$itemsAvailable = 0;
			
						include(ACTIVE_THEME_PATH ."/left-denominations-bar-select.php");						
						?>	 
					</div>
					<div class="form-group"><br/>
						<? 
						// Hide the SEND NOW button if all are sold out
						if ($itemsAvailable > 0) {
						?>						
							<input type="submit" class="col-md-12 btn btn-green  btn-block" name="buy" value="SEND NOW"/>
						<?
						} else {
						?>
							<p><center><h3>SOLD OUT</h3></center></p>
						<?
						}
						?>
						
					</div>
				</form>
			</div>
		</div>
		<div class="panel panel-default">
			<?php include(ACTIVE_THEME_PATH ."/share.php"); ?>
		</div>
	</div>
</div>