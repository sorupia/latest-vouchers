<div class="col-md-12 free pull-back">
	<!-- Static navbar -->
	<div class="navbar" role="navigation">
		<div class="navbar-header" id="category-header">
		  <button type="button" class="navbar-toggle" id="category-navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
			<span class="caret"></span>
		  </button>
			<a class="category-lead" id="category-leader" href="#">Choose a category</a>
		</div>
		<div class="navbar-collapse collapse" id="navbar-collapse">
		  <!-- Left nav -->
		  <ul class="nav navbar-nav">
			<?php
			$marchant = urldecode($_REQUEST['category']);
			$categories = count($categoryMap[$countryAbbreviation]);
			
			$active = "";
			$others = "";
			$dropdown = "";
			$page = "";
			$Txt = "More";
			for($count=0; $count < $categories; $count++){
				
				$category = $categoryMap[$countryAbbreviation]["$count"];
				$page = "index.php?category=". urlencode($category);
				
				if($marchant == $category){
					$active = "active";
				}
								
				if($count < 4){										
					echo'<li><a href="'.$page.'" class="category '.$active.'">'.$category.'</a></li>';				
				}
				elseif($count == 4){
					$dropdown = "dropdown";
					echo '<li '.$dropdown.'>';
					echo '<a href="#" data-toggle="dropdown" class="dropdown-toggle category '.$others.'">'.$Txt.'<span class="caret"></span></a>';
					echo '<ul class="dropdown-menu">';
						echo'<li><a href="'.$page.'" class="sub_category '.$active.'">'.$category.'</a></li>';	
				}
				
				if($count > 4){
					echo'<li><a href="'.$page.'" class="sub_category '.$active.'">'.$category.'</a></li>';	
				}
				if($count == ($categories - 1)){
						echo '</ul>';
					echo '</li>';
				}
				//clear off the category active flag before going to the next category on the list
				$active = "";
			}
			?>
		  </ul>

		</div><!--/.nav-collapse -->
	</div>

</div>
