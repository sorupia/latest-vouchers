<?
/*******************************************************************************
**   FILE: header.php
**
**   FUNCTION: header
**
**   PURPOSE: Themes header
**
**   WRITTEN BY: Simon Orupia (PBT-AVMM, Kampala)   DATE: Sep.2015
**
**   MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 2015.01.12
**   Put appropriate search engine keywords
**
*********************************************************************************/
?>

<!--
Author:PBT
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
	<title>Send Vouchers To Uganda</title>
	<!--web-font-->
	<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
	<!--//web-font-->
	<link rel="stylesheet" href="<? echo ACTIVE_THEME_PATH . '/' ?>css/bootstrap.min.css" media="all">
	<!--<link rel="stylesheet" href="css/bootstrap-theme.min.css">-->
	<link href="<? echo ACTIVE_THEME_PATH . '/' ?>css/style.css" type="text/css" rel="stylesheet" media="all">
	<!-- SmartMenus jQuery Bootstrap Addon CSS -->
    <link href="<? echo ACTIVE_THEME_PATH . '/' ?>categories-libs/addons/bootstrap/jquery.smartmenus.bootstrap.css" rel="stylesheet">
	<!-- SmartMenus jQuery Bootstrap Addon CSS -->
	<link rel="stylesheet" href="<? echo ACTIVE_THEME_PATH . '/' ?>css/denomination.css"/>
	<link rel="icon" type="image/PNG" href="images/favicon.png">	
	<!-- Custom Theme files -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Uganda vouchers, send a voucher to Uganda, send voucher, supermarket vouchers, Nakumatt, Capital Shoppers, Quality supermarket,
    Shoprite, Uchumi, Tuskys, Game" />

	<!--//end-smoth-scrolling-->
	<!-- added for modal popoup-->
	<script src="<? echo ACTIVE_THEME_PATH . '/' ?>js/bootstrap.min.js"></script>
</head>
<body>
	<!--navigation-->
	<div class="top-nav">
		<nav class="navbar navbar-fixed-top navbar-default">
			<!--a href="index.php">
			<img src="images/new-voucher-logo.png" style="height:60px; padding-left:20px; padding-top:5px; padding-bottom:5px;">
			</a-->
			<div class="container free">
				<div class="col-md-3 free pull-back">
					<div class="navbar-header">
					<a href="index.php">
				    	<img src="images/new-voucher-logo.png" style="height:60px; float: left; padding-top:5px; padding-bottom:5px;">
					</a>
						<div style="padding-left: 3px; border-left: 1px solid #9b979d;">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
					</div>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="col-md-9 free">
					<div class="row">
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<div>
								<ul class="nav navbar-nav navbar-right" id="navi-menu"><!-- pull-right -->
									<li><a href="index.php" class="active">Home</a></li>
									<li><a href="howitworks.php?k=hiw">How it works</a></li>
									<li><a href="faq.php">FAQS</a></li>
									<li><a href="contactus.php">Contact Us</a></li>
									<li><a href="aboutus.php">About Us</a></li>
								</ul>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
			</div>	
		</nav>		
	</div>	
	<!--div class="banner" -->
		<!-- banner-text Slider starts Here -->
				
			<!--//End-slider-script -->		
	<!--/div -->
<div id="ban">
	<img src="images/4.jpg" alt="banner" />
</div>	
