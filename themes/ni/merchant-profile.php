<div class="banner-bottom">
<div class="container">
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="col-sm-8 col-md-8 col-lg-8">
				<div class="row">
				<?php 
				
				include(ACTIVE_THEME_PATH ."/category-bar.php");
				$thisNetwork = $_REQUEST['merchant'];
				$networkName  = $networkArray[$countryAbbreviation]["$thisNetwork"]["networkName"];
				$branchesArray  = $descriptionArray[$countryAbbreviation]["$thisNetwork"]["branches"];
				$phonesArray = $descriptionArray[$countryAbbreviation]["$thisNetwork"]["phone"];
				$email = $descriptionArray[$countryAbbreviation]["$thisNetwork"]["email"];
				$website = $descriptionArray[$countryAbbreviation]["$thisNetwork"]["website"];
				
				?>
				</div>
				<div class="row">					
					<div class="col-md-12" id="rows">
						<h4><?php echo "$networkName"; ?></h4>						
					</div>
					<div class="col-md-12" id="rows">
						<img src="images/merchants/<?php echo $thisNetwork; ?>.jpg" class="merchant-image" alt="<?php echo "$networkName"; ?>" />
					</div>
					<div class="col-md-12" id="rows">
						<h4>Branches</h4>
						</br>
						<p><i class="fa fa-home"></i>
						<?php 
							for($count=0; $count < count($branchesArray); $count++){
								if ($count > 0) {
									echo ", ";
								}
								echo ucwords(strtolower($branchesArray[$count]));
							}
						?>
						</p>
						<br>
						<h4>Contact Information</h4>
						</br>
						<p><i class="fa fa-phone-square"></i>Phone: <b><?php echo $phonesArray[0].", ".$phonesArray[1]; ?></b></p>
						<p><i class="fa fa-envelope-o"></i>Email: <b><?php echo $email; ?></b></p>
						<p><i class="fa fa-globe"></i>Website: <b><?php echo $website; ?></b></p>						
					</div>
				</div>
			</div>			
				<?php include(ACTIVE_THEME_PATH ."/left-denominations-bar.php"); ?>											
		</div>
	</div>
</div>
<script>
function voucherWorth(value){
	
	var vals = value.split("::");
	
	$("#hosted_button_id").val(vals[0]);
	$("#item_id").val(vals[1]);
	$("#item_name").val(vals[2]);
	$("#network_name").val(vals[3]);
	$("#network_id").val(vals[4]);
	$("#receiver_amount_in_local").val(vals[5]);
	$("#receiver_amount_in_foreign").val(vals[6]);
	
}
</script>