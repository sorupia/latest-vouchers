<?
/*******************************************************************************
**   FILE: themes/ni/return_page_footer.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Footer files for web application
**
**   ADAPTED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2015.12.01
**   Adapted from themes/ni/footer.php because JavaScript may be causing return_page 
**   to be called twice
**
*********************************************************************************/
?>
<div class="footer">
	<div class="container">
		<div class="address">
			<div class="col-md-12" id="separate">
				<div class="col-md-3">
					<h5>GET TO KNOW US</h5>
					<ul>
						<li><a href="">About Us</a></li>
						<li><a href="">How it works</a></li>
						<li><a href="">Privacy policy</a></li>
						<li><a href="">Why our website is safe</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<h5>DO BUSINESS WITH US</h5>
					<ul>
						<li><a href="">Services </a></li>
						<li><a href="">Partners</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<h5>LET US HELP YOU</h5>
					<ul>
						<li><a href="">Free SMS</a></li>
						<li><a href="">FAQ</a></li>
						<li><a href="">Refund policy</a></li>
						<li><a href="">Contact Us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12" style="background-color: darkgrey; padding-bottom:20px; color:#f2f2f2;">
		<div class="container">
			<h5 align="" style="margin-top:30px; padding-left:20px;">Copyright 2015 &copy;  UgandaVouchers.com. All Rights Reserved</h5>
		</div>
	</div>
	<?php 
	error_reporting(0);
	$k = $_REQUEST['k'];
	?>
</div>

</body>
</html>