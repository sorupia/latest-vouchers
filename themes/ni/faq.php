<div class="banner-bottom">
<div class="container">
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="col-sm-8 col-md-8 col-lg-8">
				<h3>Frequently Asked Questions</h3>
				<div class="row">
					<div class="col-md-12" id="rows">
						<b>What does UgandaVouchers.com do?</b>
						<p>
						We sell Shopping Vouchers to clients.
						The transaction number are automatically delivered to the receiver's mobile phone.			
						</p>
					</div>
					<div class="col-md-12" id="rows">
						<b>How does the beneficiary receive the Voucher I have sent?</b>
						<p>
						1. As soon as the Voucher is sent, the beneficiary will receive an SMS containing the transaction number and 
						then we will make a call to the reciever informing him or her on the time and supermarket where 
						our agent will hand over the voucher to him / her.
						</p>	
					</div>
					<div class="col-md-12" id="rows">
						<b>Is the transaction number received immediately? </b>
						<p>The transaction number is generally received within 5 - 30 seconds on average.</p>
					</div>
					<div class="col-md-12" id="rows">
						<b>If I am in Uganda and I do not own a credit card can I use the service?  </b>
						<p>
						Yes. We accept payments by mobile money. however at the moment, the mobile money system payments provided 
						by the telecom networks are not instantly reconciled and because of this we are unable to provide an 
						instant service to mobile money users. Mobile money users receive delayed Vouchers.
						We believe this will become instant in the future.		
						</p>
					</div>
					<div class="col-md-12" id="rows">
						<b>Can I get a refund? </b>
						<p>
						If you experience a problem during the transaction, please send us an email at support@ugandavouchers.com along 
						with the transaction number that was provided to you. The problem will be investigated and a refund effected if 
						we find that you were charged for incomplete services. However, we do not provide refunds for transactions 
						completed without any problems.
						</p>
					</div>
					<div class="col-md-12" id="rows">
						<b>Do you store my credit card or bank information? </b>
						<p>
						We do not receive or store your payment information. When you get to the payment part of the transaction,
						you will be redirected to PayPal where you will complete the transaction. PayPal is a world leader in 
						transaction brokerage and is completely secure. For more information,
						please visit the PayPal website at http://www.paypal.com
						</p>
					</div>
					<div class="col-md-12" id="rows">
						<b>Do you share any of my information with other companies?  </b>
						<p>
						No, our policy is to keep all your information confidential and only use it for refunds and customer service.
						</p>
					</div>
					<div class="col-md-12" id="rows">
						<b>What happened to my stored receivers?  </b>
						<p>
						To access your stored receiver numbers you will need to first login into the account section.
						</p><br />
					</div>
				</div>
			</div>
			<div class="row">
				<?php include(ACTIVE_THEME_PATH ."/left-denominations-bar-others.php"); ?>								
			</div>
		</div>
	</div>
</div>