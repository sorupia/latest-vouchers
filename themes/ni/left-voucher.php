<?

$network_id = '';
$network_name = '';
if (isset($input_array)) {
	$network_name = $input_array['network_name'];
	$network_id = $input_array['network_id'];
} elseif ( isset($transactionArray)) {
	$network_name = $input_array['network_name'];
	$network_id = $input_array['network_id'];
}

// Skip this panel if no id exists
if (isset($network_id)) {
?>

<div panel header></div>
<div class="panel-body">
	<h3><?=$network_name?></h3>
	<img src='images/logos/<?=$network_id?>.png' style='width: 100%;' />
	<h4 id="v-logos">Recipient will be hand delivered a voucher like this..</h4>
	<img id="voucherselect1" style="width: 100%;" src="images/vouchers/<?=$network_id?>.jpg"/>
</div>

<? } ?>