<div class="banner-bottom">
	<div class="container">
		<div class="row">
			<div class="col-md-12 free">
				<div class="col-md-8 col-lg-8 free">
					<?php 
					include(ACTIVE_THEME_PATH ."/category-bar.php");						
					?>
					<div class="row">
						<div class="col-md-12 free">
							<p class="right-alitle">
							Just click on any denomination to get started.
							</p>
							</br>
						</div>
					</div>
					<?php 
					include(ACTIVE_THEME_PATH ."/vouchers.php");						
					?>					
				</div>
				<div class="col-sm-4 col-md-4 col-lg-4 free special-left">
					<div class="panel panel-default">
						<div class="panel-heading gn">
							<h4 align="center">How to Buy a Voucher</h4>
						</div>
						<div class="panel-body">
							<p>1. Select Voucher denomination</p>
							<p>2. Enter receiver's details</p>
							<p>3. Pay via PayPal</p>
							<p>4. Transaction number displayed and sent to the receiver</p>
						</div>	
					</div>
					<div class="panel panel-default">
						<?php include(ACTIVE_THEME_PATH ."/moreinfo.php"); ?>
					</div>
					<div class="panel panel-default">
						<?php include(ACTIVE_THEME_PATH ."/share.php"); ?>
					</div>
					<div class="panel panel-default">
						<?php include(ACTIVE_THEME_PATH ."/paypal.php"); ?>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
	