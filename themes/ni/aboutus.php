<?
/*******************************************************************************
**   FILE: aboutus.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Displays information about UgandaVouchers.com
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   
**
**   ADAPTED BY: 
**
*********************************************************************************/
?>
<div class="banner-bottom">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12"> <!--id="bgimgs"-->
                <div class="col-sm-8 col-md-8 col-lg-8">
                    <h3>About Us</h3>
                    <div class="row">
                        <div class="col-md-12" id="rows">
                            <p>
                            UgandaVouchers.com is a service offered by 3nitylabs Inc., an Edmonton based ICT company formed by a team of 
                            East African business and ICT professionals. It is the first of a number of innovative services meant to 
                            foster East African cooperation, contribute to ICT development and technology transfer to the area, 
                            as well as link the East Africans living abroad to their homelands.         
                            </p>
                        </div>
                        <div class="col-md-12" id="rows">
                            <p>
                            UgandaVouchers.com is a platform that we have created to provide a convenient mechanism for East Africans 
                            living abroad to send Vouchers to their friends, relatives and business partners back home. It is also 
                            convenient for expatriates in East Africa as well as East Africans who are visiting from abroad and have 
                            credit cards or PayPal account. The platform provides for a ubiquitous (anytime anywhere) Voucher sending 
                            to friends or relatives in Kampala, Uganda. The service shall soon be extended to other parts of the country.
                            </p>
                        </div>
                        <div id="rows" class="col-md-12">
                            <p>
                            With accumulated skills in Software Development, Systems Integration, Telecommunications and Business 
                            processes, we are dedicated to providing the best services through a customer-centric approach with 
                            utmost professionalism. Our mission is to relentlessly pursue technology advancement in East Africa.
                            The main offices are located in Edmonton, Alberta, Canada. We have partners and/or representatives in 
                            the USA, UK and East Africa.            
                            </p>
                        </div><br/><br/>
                    </div>
                </div>
                <div class="row">
                    <?php include(ACTIVE_THEME_PATH ."/left-denominations-bar-others.php"); ?>
                </div>
            </div>
        </div>
    </div>
    