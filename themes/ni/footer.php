<div class="footer">
	<div class="container">
		<div class="address">
			<div class="col-md-12" id="separate">
				<div class="col-md-3">
					<h5>GET TO KNOW US</h5>
					<ul>
						<li><a href="">About Us</a></li>
						<li><a href="">How it works</a></li>
						<li><a href="">Privacy policy</a></li>
						<li><a href="">Why our website is safe</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<h5>DO BUSINESS WITH US</h5>
					<ul>
						<li><a href="">Services </a></li>
						<li><a href="">Partners</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<h5>LET US HELP YOU</h5>
					<ul>
						<li><a href="">Free SMS</a></li>
						<li><a href="">FAQ</a></li>
						<li><a href="">Refund policy</a></li>
						<li><a href="">Contact Us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12" style="background-color: darkgrey; padding-bottom:20px; color:#f2f2f2;">
		<div class="container">
			<h5 align="" style="margin-top:30px; padding-left:20px;">Copyright 2015 &copy;  UgandaVouchers.com. All Rights Reserved</h5>
		</div>
	</div>
	<?php 
	error_reporting(0);
	$k = $_REQUEST['k'];
	?>
</div>
	<script src="<? echo ACTIVE_THEME_PATH . '/'?>js/jquery-1.9.1.min.js"></script>
	<!--smooth-scrolling-of-move-up-->
	<script type="text/javascript">
	
		function changeImage(){
			//chage the banner image for smaller devices
			var w = $(window).width();
			var key = '<?php echo $k; ?>';
			if( key == 'hiw' ){
				if( w <= 768 && w >= 600){
					$("#ban").html("<img src='images/1.jpg' width='100%' alt='banner' />").css("margin-bottom","-6%");
				}else if( w < 600 && w >= 550){
					$("#ban").html("<img src='images/1.jpg' width='100%' alt='banner' />").css("margin-bottom","-5.5%");
				}else if( w < 550 && w > 300){
					$("#ban").html("<img src='images/1.jpg' width='100%' alt='banner' />").css("margin-bottom","-11%");
				}else if( w < 300){
					$("#ban").html("<img src='images/1.jpg' width='100%' alt='banner' />").css("margin-bottom","-20%");
				}else{
					$("#ban").html("<img src='images/7.jpg' alt='banner' />").css("margin-bottom","-4.5%");
				}
			}else{
				if( w <= 768 && w >= 600){
					$("#ban").html("<img src='images/2.jpg' width='100%' alt='banner' />").css("margin-bottom","-7%");
				}else if( w < 600 && w >= 550){
					$("#ban").html("<img src='images/2.jpg' width='100%' alt='banner' />").css("margin-bottom","-6.5%");
				}else if( w < 550 && w > 300){
					$("#ban").html("<img src='images/2.jpg' width='100%' alt='banner' />").css("margin-bottom","-11%");
				}else if( w < 300){
					$("#ban").html("<img src='images/2.jpg' width='100%' alt='banner' />").css("margin-bottom","-20%");
				}else{
					$("#ban").html("<img src='images/4.jpg' alt='banner' />").css("margin-bottom","-4.5%");
				}
			}
		}
		
		function categoryBar(){
			
			var w = $(window).width();
			if( w <= 760 ){
				$("#category-header,.category-lead").show(0);
			}else{
				$("#category-header,.category-lead").hide(0);
			}			
		}
				
		$(document).ready(function() {
		
			changeImage();
			categoryBar();
			
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/
			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
		
		$(window).resize(function(){
				changeImage();
				categoryBar();
		});
		
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<!--//smooth-scrolling-of-move-up-->
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- //Custom Theme files -->
	<!-- js -->
	<!-- //js -->	
	<!-- start-smoth-scrolling-->
	<script type="text/javascript" src="<? echo ACTIVE_THEME_PATH . '/'?>js/move-top.js"></script>
	<script type="text/javascript" src="<? echo ACTIVE_THEME_PATH . '/'?>js/easing.js"></script>	
	<!-- added for modal popoup-->
	<script type="text/javascript">
		$(document).ready(function($) { 
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
			//call the modal popup
			//$("#myModal").modal('show');
		});
	</script>
	<!-- Popup Modal -->
	<div id="myModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Confirmation</h4>
				</div>
				<div class="modal-body">
					<p>Do you want to save changes you made to document before closing?</p>
					<p class="text-warning"><small>If you don't save, your changes will be lost.</small></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Popup Modal -->
	<!-- Placed at the end of the document so the pages load faster -->
    <script src="<? echo ACTIVE_THEME_PATH . '/'?>js/bootstrap.js"></script>	
	<!-- SmartMenus jQuery plugin -->
    <script type="text/javascript" src="<? echo ACTIVE_THEME_PATH . '/'?>categories-libs/jquery.smartmenus.js"></script>
    <!-- SmartMenus jQuery Bootstrap Addon -->
    <script type="text/javascript" src="<? echo ACTIVE_THEME_PATH . '/'?>categories-libs/addons/bootstrap/jquery.smartmenus.bootstrap.js"></script>

</body>
</html>