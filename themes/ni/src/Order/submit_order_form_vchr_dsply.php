<?php
/*******************************************************************************
**   FILE: submit_order_form_vchr.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Displays information entered into the receiver form
**   for confirmation. Handles form data when navigating to
**   the previous recepient form.
**
**   WRITTEN BY: Philip Mbonye (PBT-AVMM, Kampala) DATE: 28.Nov.2015  
**
**   MODIFIED BY: Arthur Ntozi (3nitylabs-AVMM, Kampala) DATE: 30.Nov.2015
**   Formatted a few items
**
*********************************************************************************/
?>
<div class="col-md-8 free" id="form-md">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body free">
				<div class="form-group"> 
					<label class="col-sm-6 control-label">Voucher value in UGX</label>
					<div class="col-sm-6"> 
					<?=LOCAL_CURRENCY?> <?=number_format($input_array['receiver_amount_in_local'], LOCAL_DECIMAL_PLACES)?>
					</div>
                </div> 
				
				<div class="form-group"> 
				  <label class="col-sm-6 control-label">Price in USD</label>
					<div class="col-sm-6"> 
					<?=SALE_CURRENCY." ".number_format($input_array["receiver_amount_in_foreign"],FOREIGN_DECIMAL_PLACES)?>
					</div>
                </div>
				
				<div class="form-group"> 
				  <label class="col-sm-6 control-label">Receiver's phone</label>
					<div class="col-sm-6"> 
					<?=$input_array["receiver_phone"]?>
					</div>
                </div>
				
				<div class="form-group"> 
				  <label class="col-sm-6 control-label">Receiver's first name</label>
					<div class="col-sm-6"> 
					<?=$input_array["receiver_first_name"]?>
					</div>
                </div>
				
				<div class="form-group"> 
				  <label class="col-sm-6 control-label">Receiver's last name </label>
					<div class="col-sm-6"> 
					<?=$input_array["receiver_last_name"]?>
					</div>
                </div>
				
				<div class="form-group"> 
				  <label class="col-sm-6 control-label">Receiver's region </label>
					<div class="col-sm-6"> 
					<?=$input_array["receiver_region"]?>
					</div>
                </div>
				
				<div class="form-group"> 
				  <label class="col-sm-6 control-label">Pay By</label>
					<div class="col-sm-6"> 
					<?=$input_array["payment_option"]?>
					</div>
                </div>
				
				<div class="col-sm-3"> 
					
				</div>
				<div class="col-sm-8 align"> 
					<form action="receiver_vchr.php" method="post" name="receiver_form" >
						<input type="hidden" name="supermarket"                 value="<?=$_POST['supermarket']?>" id="supermarket">
						<input type="hidden" name="payment_option"              value="<?=PAYMENT_OPTION_PAYPAL?>" id="payment_option">
						<input type="hidden" name="receiver_amount_in_local"    value="<?=$input_array['receiver_amount_in_local']?>">
						<input type="hidden" name="receiver_amount_in_foreign"  value="<?=$input_array['receiver_amount_in_foreign']?>">
						<input type="hidden" name="receiver_phone"              value="<?=$input_array['receiver_phone']?>">
						<input type="hidden" name="phoneConfirm"                value="<?=$input_array['phoneConfirm']?>">
						<input type="hidden" name="receiver_first_name"         value="<?=$input_array['receiver_first_name']?>">
						<input type="hidden" name="receiver_last_name"          value="<?=$input_array['receiver_last_name']?>">
						<input type="hidden" name="receiver_region"             value="<?=$input_array['receiver_region']?>">
						<input type="hidden" name="network_name"                value="<?=$input_array['network_name']?>" id="network_name">
						<input type="hidden" name="item_id"                     value="<?=$input_array['item_id']?>">
						<input type="hidden" name="airtime_load_option"         value="<?=$input_array['airtime_load_option']?>">
						<input type="hidden" name="ip_address"                  value="<?=$input_array['ip_address']?>">
						<input type="hidden" name="ip_country"                  value="<?=$input_array['ip_country']?>">
						<input type="hidden" name="ip_state"                    value="<?=$input_array['ip_state']?>">
						<input type="hidden" name="ip_city"                     value="<?=$input_array['ip_city']?>">
						<input type="hidden" name="network_id"                  value=<?=$input_array['network_id']?>>
						<input type="hidden" name="item_name"                   value="<?=$input_array['item_name']?>">
						<input type="hidden" name="hosted_button_id"            value="<?=$input_array['hosted_button_id']?>">
						<input type="submit" value="BACK" class="col-sm-4 btn btn-green sm-btn" />
					</form>
					<form action="submit_to_pp.php" method="post" name="receiver_form">
					   <input  type="hidden" name="supermarket"                 value="<?=$_POST['supermarket']?>" id="supermarket">
						<input type="hidden" name="payment_option"              value="<?=PAYMENT_OPTION_PAYPAL?>" id="payment_option"      >
						<input type="hidden" name="receiver_amount_in_local"    value="<?=$input_array['receiver_amount_in_local']?>">
						<input type="hidden" name="receiver_amount_in_foreign"  value="<?=$input_array['receiver_amount_in_foreign']?>">
						<input type="hidden" name="receiver_phone"              value="<?=$input_array['receiver_phone']?>">
						<input type="hidden" name="phoneConfirm"                value="<?=$input_array['phoneConfirm']?>">
						<input type="hidden" name="receiver_first_name"         value="<?=$input_array['receiver_first_name']?>">
						<input type="hidden" name="receiver_last_name"          value="<?=$input_array['receiver_last_name']?>">
						<input type="hidden" name="receiver_region"             value="<?=$input_array['receiver_region']?>">
						<input type="hidden" name="item_id"                     value="<?=$input_array['item_id']?>">
						<input type="hidden" name="network_name"                value="<?=$input_array['network_name']?>" id="network_name">
						<input type="hidden" name="airtime_load_option"         value="<?=$input_array['airtime_load_option']?>">
						<input type="hidden" name="ip_address"                  value="<?=$input_array['ip_address']?>">
						<input type="hidden" name="ip_country"                  value="<?=$input_array['ip_country']?>">
						<input type="hidden" name="ip_state"                    value="<?=$input_array['ip_state']?>">
						<input type="hidden" name="ip_city"                     value="<?=$input_array['ip_city']?>">
						<input type="hidden" name="network_id"                  value=<?=$input_array['network_id']?>>
						<input type="hidden" name="item_name"                   value="<?=$input_array['item_name']?>">
						<input type="hidden" name="hosted_button_id"            value="<?=$input_array['hosted_button_id']?>">
						<button type="submit" name="send" class="col-sm-offset-1 col-sm-4 btn btn-green sm-btn">SEND NOW</button>
					</form>
				</div>              
            </div>
        </div>
    </div>
</div>
