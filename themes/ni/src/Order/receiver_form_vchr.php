<?php
/*******************************************************************************
**   FILE: receiver_form_vchr.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Handles display/positioning of the reciever information 
**   form, vendor logo , voucher image and paypal section
**
**   WRITTEN BY: Philip Mbonye (PBT-AVMM, Kampala) DATE: 28.Nov.2015  
**
**   MODIFIED BY: 
**
*********************************************************************************/
?>
<div class="banner-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-12 free">
                <?php include(ACTIVE_THEME_PATH ."/src/Order/receiver_form_vchr_dsply.php"); ?>
                <div class="col-md-4 free special-left">
                    <div class="panel panel-default">
                        <?php include(ACTIVE_THEME_PATH ."/left-voucher.php"); ?>
                    </div>
                    <div class="panel panel-default">
                        <?php include(ACTIVE_THEME_PATH ."/paypal.php"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>