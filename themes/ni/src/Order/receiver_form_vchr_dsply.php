<?
/*******************************************************************************
**   FILE: receiver_form_vchr_dsply.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Displays form that captures voucher recepient information
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   
**
**   ADAPTED BY: Philip Mbonye (PBT-AVMM, Kampala) DATE: 28.Nov.2015
**
**   MODIFIED BY: Arthur Ntozi (3nitylabs-AVMM, Kampala) DATE: 1.Dec.2015
**
*********************************************************************************/
?>

<?php 


    if( substr($input_array['receiver_phone'], 0, 4) == "+".COUNTRY_CODE)
    {
        $receiver_phone = substr($input_array['receiver_phone'], 4);
    }
    if( substr($input_array['phoneConfirm'], 0, 4) == "+".COUNTRY_CODE)
    {
        $phoneConfirm = substr($input_array['phoneConfirm'], 4);
    }
    
 ?>
<div class="col-md-8 free" id="form-md">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body free">
                <!-- form -->
                <form class="form-horizontal" name="receiver" method="post" action="process_vchr.php" role="form">          
                    <div class="form-group"> 
                      <label for="item_id" class="col-sm-5 control-label">Voucher's Value</label>
                        <div class="col-sm-7 align"> 
							 <?php
                                    include_once $_SERVER['DOCUMENT_ROOT']."/adm2/itemIDDropDown.php";
                                    if(isset($_REQUEST['item_id']))
                                    {
                                        getDropDownSelected($countryAbbreviation, $_REQUEST['item_id']);
                                    }
                                    else
                                    {
                                        getDropDownSelected($countryAbbreviation, $_POST['item_id']); 
                                    }
                                ?>
                        </div>
                    </div>  
                    
                    <div class="form-group">
                         <label for="receiver_phone" class="col-md-5 control-label">Receiver's Phone Number <span id="phone1Star">*</span></label> 
                         <div class="col-sm-7">
                             <section id="phone1Error">Incomplete Phone Number</section>
                             <?php if(isset($_REQUEST['error'])){ ?>
                                <section style=" color: red; " id="incomplete"><?=$_REQUEST['error']?></section>
                             <?php } else if(isset($_REQUEST['mismatch'])){?>
                             <section style=" color: red; " id="incomplete">Please make sure Numbers entered are the same</section>
                             <?php } ?>
                             <!--code below is from the old site -->
                             <div id="err1" style="color: red; display: none;"></div>
                            <?
                                //Number of digits in recipient phone after the country code
                                $country_code_length = strlen(COUNTRY_CODE);
                                $rx_phone_length_no_country_code = RX_PHONE_LENGTH - $country_code_length - 1;
                                $rx_phone_pattern = "[\+]{1}[".COUNTRY_CODE."]{".$country_code_length."}[\d]{".$rx_phone_length_no_country_code."}";
                            ?>
                            <input  name="receiver_phone"
                                    class="form-control"
                                    pattern="<?=$rx_phone_pattern?>"
                                    maxlength="<?=RX_PHONE_LENGTH?>"
                                    id="receiver_phone"
                                    value="+<?=COUNTRY_CODE.$receiver_phone?>"
                                    onblur="mobile1_onblur();"
                                    required=""
                                    type="text"
                                    title="Phone should start with +<?=COUNTRY_CODE?> and be <?=RX_PHONE_LENGTH?> characters long with digits">
                             <!--code above is from the old site -->
                         </div>
                     </div>

                     <div class="form-group">
                        <label for="phoneConfirm" class="col-md-5 control-label">Re-enter Phone Number <span id="phoneStar">*</span></label> 
                        <div class="col-sm-7"> 
                            <section id="phoneError">Phone Number Mismatch</section>
                            <!--code below is from the old site -->
                            <div id="err2" style="color: red; display: none;"></div>
                            <?//Todo: Configurable items in pattern and value ?>
                            <input  name="phoneConfirm"
                                    class="form-control"
                                    pattern="<?=$rx_phone_pattern?>"
                                    maxlength="<?=RX_PHONE_LENGTH?>" 
                                    id="phoneConfirm"
                                    value="+<?=COUNTRY_CODE.$phoneConfirm?>"
                                    onblur="mobile2_onblur();"
                                    required=""
                                    type="text"
                                    title="Phone should start with +<?=COUNTRY_CODE?> and be <?=RX_PHONE_LENGTH?> characters long with digits">
                            <!--code above is from the old site -->
                        </div>
                     </div>
                         
                     <div class="form-group">
                         <label for="receiver_first_name" class="col-md-5 control-label">Receiver's First Name</label> 
                         <div class="col-sm-7"> 
                         <?php if(isset($_REQUEST['missing'])){ ?>
                            <section style=" color: red; " id="nameMissing">Please ensure both names are entered</section>
                         <?php }else if (isset($_REQUEST['samename'])){  ?>
                            <section style=" color: red; " id="nameMissing">Please ensure the names are different</section>
                         <?php } ?>
                         <input type="text" class="form-control" name="receiver_first_name" id="receiver_first_name" onblur="nameValidation();" placeholder="Enter First Name" value="<?=$input_array['receiver_first_name']?>" required/> </div>
                     </div>
                     
                    <div class="form-group">
                        <label for="receiver_last_name" class="col-md-5 control-label">Receiver's Last Name</label> 
                        <div class="col-sm-7">  
                        <section id="nameError">The names are not different</section>                  
                            <input type="text" class="form-control" name="receiver_last_name" id="receiver_last_name" onblur="nameValidation();" placeholder="Enter Second Name" value="<?=$input_array['receiver_last_name']?>" required/> 
                        </div>
                    </div>
                     
                    <div class="form-group"> 
                        <label for="receiver_region" class="col-md-5 control-label">Receiver's Region</label>
                        <div class="col-sm-7"> 
                            <select class="form-control" name="receiver_region" row="1" required> 
                                <option>Kampala</option> 
                            </select> 
                        </div>
                    </div>
                    <div class="form-group" id="confirmTable">   
                        <div class="col-sm-offset-3 col-sm-6" id="confirmTable">
                            <?//Todo: incoming post for supermarket ?>
                            <input type="hidden" name="receiver_amount_in_foreign"    id="receiver_amount_in_foreign"   value="<?=$input_array['receiver_amount_in_foreign']?>"/>
                            <input type="hidden" name="receiver_amount_in_local"      id="receiver_amount_in_local"     value="<?=$input_array['receiver_amount_in_local']?>"/>
                            <input type="hidden" name="payment_option"                id="payment_option"               value="<?=PAYMENT_OPTION_PAYPAL?>"/>
                            <input type="hidden" name="network_id"                    id="network_id"                   value="<?=$input_array['network_id']?>"/>
                            <input type="hidden" name="item_id"                       id="item_id"                      value="<?=$input_array['item_id']?>"/>
                            <input type="hidden" name="network_name"                  id="network_name"                 value="<?=$input_array['network_name']?>"/>
                            <input type="hidden" name="airtime_load_option"           id="airtime_load_option"          value="<?=$input_array['airtime_load_option']?>"/>
                            <input type="hidden" name="ip_address"                    id="ip_address"                   value="<?=$input_array['ip_address']?>"/>
                            <input type="hidden" name="ip_country"                    id="ip_country"                   value="<?=$input_array['ip_country']?>"/>
                            <input type="hidden" name="ip_state"                      id="ip_state"                     value="<?=$input_array['ip_state']?>"/>
                            <input type="hidden" name="ip_city"                       id="ip_city"                      value="<?=$input_array['ip_city']?>"/>
                            <input type="hidden" name="item_name"                     id="item_name"                    value="<?=$input_array['item_name']?>"/>
                            <input type="hidden" name="hosted_button_id"              id="hosted_button_id"             value="<?=$input_array['hosted_button_id']?>"/>
                            <div class="row pull-left-alittle">
                                <input type="button" value="BACK" class="col-sm-offset-4 col-sm-3 btn btn-green sm-btn" 
                                onclick="window.location.href='../index.php';" />
                                <button type="submit" name="send" class="col-sm-offset-1 col-sm-4 btn btn-green sm-btn"> CONFIRM</button> 
                            </div>
                        </div> 
                    </div> 
                </form>
                <!-- form -->
            </div>
        </div>
    </div>
</div>
<script>
    function validatePhone(){
        var Mobile1 = mobile1.value;
        var Mobile2 = mobile2.value;
        var mobile1Length = Mobile1.length - 1;
        var mobile2Length = Mobile2.length - 1;
        
        if(isNaN(Mobile1)){
            Mobile1 = Mobile1.substring(0, mobile1Length);
            mobile1.value = Mobile1;
        }
        if(isNaN(Mobile2)){
            Mobile2 = Mobile2.substring(0, mobile2Length);
            mobile2.value = Mobile2;
        }
    }
    
    function matchPhone(){
        var Mobile1 = mobile1.value;
        var Mobile2 = mobile2.value;
        
        if(Mobile1 != Mobile2){
            mobile2.value = "<?=COUNTRY_CODE?>";
            mobile2.select();
            $("#phoneError, #phoneStar").css("display","block");
        }else{
            $("#phoneError, #phoneStar").css("display","none");
        }
    }
    
    function incompletePhone(){
        var Mobile1 = mobile1.value;
        var mobile1Length = Mobile1.length;
        
        if(mobile1Length < 12){
            mobile1.select();
            $("#phone1Error, #phone1Star").css("display","block");
        }else{
            $("#phone1Error, #phone1Star").css("display","none");
            $("#incomplete").css("display","none");
        }
    }
    //Validation for the first and second name not to be the same.
    function nameValidation(){
        var FName = document.forms["receiver"]["receiver_first_name"].value;
        var LName = document.forms["receiver"]["receiver_last_name"].value;
        
        if(FName==null || FName=="",LName==null || LName==""){
            $("#nameMissing").css({"display":"block"});
            lastname.select();
        }
        else if(FName.toUpperCase() == LName.toUpperCase()){
            lastname.select();
            $("#nameError").css({"display":"block"});
        }else{
            $("#nameError").css({"display":"none"});
        }
    }
    
</script>
