<?php
/*******************************************************************************
**   FILE: DisplayManualProcNewClient.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Displays voucher purchase information of new buyers/clients (MANUAL-LOAD)
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   
**
**   ADAPTED BY: 
**
*********************************************************************************/
?>
<?
    $networkCode   = substr($itemIDNumber,0,5);
    $valueInUGSh   = $transactionArray['valueInLocal'];
    $localCurrency = LOCAL_CURRENCY;
    $networkName   = $transactionArray['networkName'];
?>
<div class="banner-bottom">
<div class="container">
<div class="row">
	<div class="col-md-12">
	
		<div class="col-md-8" id="form-md">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-heading gn">	
						<h4 align="center">Thank you for your purchase!</h4>
					</div>		
				
					<div class="panel-body">
						<div style="width: 100%; ">
							<p>
								<? // DisplayManualProcNewClient ?>
								<table width="100%">
									<tr>
										<b>&nbsp;&nbsp; <?=$transactionArray['first_name']?>, this is your first transaction with us. This transaction will require manual processing. The details of your purchase are below:</b>
									</tr>
									<tr>
										<td align="right" width="200px"><b>Receiver's Name:</b> </td><td>&nbsp;&nbsp; <?=$transactionArray['order_data']['receiver_first_name']?> <?=$transactionArray['order_data']['receiver_last_name']?></td>
									</tr>
									<tr>
										<td align="right" ><b>Receiver's Phone: </b> </td><td>&nbsp;&nbsp; <?=$transactionArray['order_data']['receiver_phone']?></td>
									</tr>
									<tr>
										<td align="right" ><b>Voucher Value: </b> </td><td>&nbsp;&nbsp; <?=LOCAL_CURRENCY." ".number_format($valueInUGSh, LOCAL_DECIMAL_PLACES)." $networkName ".PRODUCT?></td>
									</tr>
									<tr>
										<td align="right" ><b>Cost: </b> </td><td>&nbsp;&nbsp; <?=LOCAL_CURRENCY." ".number_format($transactionArray['mc_gross'],FOREIGN_DECIMAL_PLACES)?></td>
									</tr>
									<tr>
										<td align="right" ><b>Transaction ID:</b>  </td><td>&nbsp;&nbsp; <?=$transactionArray['txn_id']?></td>
									</tr>
									<tr>
										<td align="right" ><b>Order ID: </b> </td><td>&nbsp;&nbsp; <?=$transactionArray['invoice']?></td>
									</tr>
									<tr>
										<td align="right" valign="top"><b>Receiver Instructions:</b></td>
										<td>&nbsp;&nbsp; The recipient will be called by one of our agents and the voucher delivered to him/her.
					In case of any issues please email us at <b><?=SUPPORT_EMAIL?></b> or call <b><?=SUPPORT_PHONE?></b>.</td>
									</tr>
								</table>
					
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-md-4">
			<div class="panel panel-default">			
				<?php include(ACTIVE_THEME_PATH ."/voucherdeliveryinfo.php"); ?>				
			</div>
			<div class="panel panel-default">
				<?php include(ACTIVE_THEME_PATH ."/share.php"); ?>
			</div>
		</div>
		
	</div>
</div>
</div>
</div>
