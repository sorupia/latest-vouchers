<?php
/*******************************************************************************
**   FILE: DisplayOutOfStock.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Displays airtime purchase information
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   
**
**   ADAPTED BY: 
**
*********************************************************************************/
?>
<?
    $networkCode   = substr($itemIDNumber,0,5);
    $valueInUGSh   = $transactionArray['valueInLocal'];
    $localCurrency = LOCAL_CURRENCY;
    $networkName   = $transactionArray['networkName'];
?>
<div class="banner-bottom">
<div class="container">
<div class="row">
	<div class="col-md-12">
	
		<div class="col-md-8" id="form-md">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-heading gn">	
						<h4 align="center">Thank you for your purchase!</h4>
					</div>		
				
					<div class="panel-body">
						<div style="width: 100%; ">
							<p>
								<? // DisplayOutOfStock ?>
								<table width="100%">
									<tr>
										<b>&nbsp;&nbsp; <?=$transactionArray['first_name']?>, the details of your purchase are listed below:</b>
									</tr>
									<tr>
										<td align="right" width="200px"><b>Airtime Access Number: </b></td><td>&nbsp;&nbsp; -TRANSFER DELAYED-</td>
									</tr>
									<tr>
										<td align="right" width="200px"><b>Card Type: </b></td><td>&nbsp;&nbsp; <? echo LOCAL_CURRENCY." ".number_format($valueInUGSh, LOCAL_DECIMAL_PLACES)." $networkName ".PRODUCT;?></td>
									</tr>
									<tr>
										<td align="right" width="200px">Receiver's Name: </td><td>&nbsp;&nbsp; <?=$transactionArray['order_data']['receiver_first_name']?> <?=$transactionArray['order_data']['receiver_last_name']?></td>
									</tr>
									<tr>
										<td align="right" >Receiver's Phone: </td><td>&nbsp;&nbsp; <?=$transactionArray['order_data']['receiver_phone']?></td>
									</tr>
									<tr>
										<td align="right" >Cost: </td><td>&nbsp;&nbsp; <?=LOCAL_CURRENCY." ".number_format($transactionArray['mc_gross'],FOREIGN_DECIMAL_PLACES)?></td>
									</tr>
									<tr>
										<td align="right" ><b>Date & Time:</b></td><td>&nbsp;&nbsp; <? echo "$timeCardSent";?> (<?=TIME_ZONE?> Time)</td>
									</tr>
									<tr>
										<td align="right" >Transaction ID: </td><td>&nbsp;&nbsp; <?=$transactionArray['txn_id']?></td>
									</tr>
									<tr>
										<td align="right" >Order ID: </td><td>&nbsp;&nbsp; <?=$transactionArray['invoice']?></td>
									</tr>
									<tr>
										<td align="right" valign="top">Receiver Instructions:</td>
										<td>&nbsp;&nbsp; Airtime will be automatically loaded onto the receiver's phone.
					In case of any issues please email us at <b><?=SUPPORT_EMAIL?></b> or call <b><?=SUPPORT_PHONE?></b>.</td>
									</tr>
								</table>
					
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-md-4">
			<div class="panel panel-default">
				<?php include(ACTIVE_THEME_PATH ."/moreinfo.php"); ?>
			</div>
			<div class="panel panel-default">
				<?php include(ACTIVE_THEME_PATH ."/share.php"); ?>
			</div>
		</div>
		
	</div>
</div>
</div>
</div>
