<?php
/*******************************************************************************
**   FILE: DuplicateID.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Displayed error message on detected duplicate transasction
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   
**
**   ADAPTED BY: 
**
*********************************************************************************/
?>
<div class="banner-bottom">
<div class="container">
<div class="row">
	<div class="col-md-12">
	
		<div class="col-md-8" id="form-md">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-heading gn">	
						<h4 align="center">Thank you for your purchase!</h4>
					</div>		
				
					<div class="panel-body">
						<div style="width: 100%; ">
							<ul>
							<?=$transactionArray['first_name']?> it appears that we have already received this transaction. Kindly check your email at <b><?=$transactionArray['payer_email']?></b> for the details.
					In case of any issues email us at <b><?=SUPPORT_EMAIL?></b> or call <b><?=SUPPORT_PHONE?></b>.<br>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-md-4">
			<div class="panel panel-default">			
				<?php include(ACTIVE_THEME_PATH ."/voucherdeliveryinfo.php"); ?>				
			</div>
			<div class="panel panel-default">
				<?php include(ACTIVE_THEME_PATH ."/share.php"); ?>
			</div>
		</div>
		
	</div>
</div>
</div>
</div>
