<?php
/*******************************************************************************
**   FILE: PPAuthenticateElse.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Displayed error message on paypal payment failure
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   
**
**   ADAPTED BY: 
**
*********************************************************************************/
?>
<?
    $networkCode   = substr($itemIDNumber,0,5);
    $valueInUGSh   = $transactionArray['valueInLocal'];
    $localCurrency = LOCAL_CURRENCY;
    $networkName   = $transactionArray['networkName'];
?>
<div class="banner-bottom">
<div class="container">
<div class="row">
	<div class="col-md-12">
	
		<div class="col-md-8" id="form-md">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-heading">	
						<h4 align="center">Oops, something's wrong!</h4>
					</div>		
				
					<div class="panel-body">
						<div style="width: 100%; ">
							<p>
								<? // PPAuthenticateElse ?>
								<table width="100%">
									<tr>
										<b>&nbsp;&nbsp; <?=$transactionArray['first_name']?>,thank you for your purchase. Unfortunately there was a temporary error with your PayPal payment. Please email us at <b><?=SUPPORT_EMAIL?></b>.</b>
									</tr>
									
								</table>
					
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-md-4">
			<div class="panel panel-default">			
				<?php include(ACTIVE_THEME_PATH ."/moreinfo.php"); ?>				
			</div>
			<div class="panel panel-default">
				<?php include(ACTIVE_THEME_PATH ."/share.php"); ?>
			</div>
		</div>
		
	</div>
</div>
</div>
</div>

<?php
	// This file caters for 
	// the infamous paypal glitch in which if a user pays by credit card and then
	// signs up for a new account, they loose the auth. details.
	// log for manual investigation
	//TODO: Insert this into an include
	$clientIP=getIP();
	$myHostName=gethostbyaddr($clientIP);
	$timeOfAccess = date("Y-m-d H:i:s");

	//log ip address over here to Failed IPs.
	$connection=connect2DB2($countryAbbreviation);
		$logQuery="INSERT INTO FailedIPs(dateOfAccess,ipAddress,notes) VALUES('$timeOfAccess','$clientIP','Failed Auth.')";
		$logResult=mysql_query($logQuery) or handleDatabaseError(''. mysql_error(),$logQuery);
	disconnectDB($connection);
	
	//25.Oct.2007: Send admin an email to find out what is causing this
	//inform the admin of failed authentication at PPRP
	$body="Hi Admin, a user tried to access the PayPalReturnPage and had the following problem.\n";
	$body.="Check the Orders Queue under Queues in the admin section and compare the Order ID with the PayPal payment email.\n";
	$body.="PayPal returned:\n ";
	
	for($i=0;$i<count($lines);$i++){
		$body.="$i: $lines[$i] \n";
	}
	
	$body.="Time of access is: $timeOfAccess \n";
	sendEmail('PayPal Return Page',$fromAddress,TECH_ADMIN,'',"PPAuthenticateElse",$body);
	sendEmail('PayPal Return Page',$fromAddress,SUPER_TECH_ADMIN,'',"PPAuthenticateElse",$body);
	sendEmail('PayPal Return Page',$fromAddress,SUPPORT_EMAIL,'',"PPAuthenticateElse",$body);
?>