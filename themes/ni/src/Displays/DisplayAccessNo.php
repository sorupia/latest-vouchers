<?
/*******************************************************************************
**   FILE: DisplayAccessNo.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Displays return information on a successful purchase
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   
**
**   ADAPTED BY: 
**
*********************************************************************************/
?>
<div class="banner-bottom">
<div class="container">
<div class="row">
    <div class="col-md-12">
    
        <div class="col-md-8" id="form-md">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading gn">  
                        <h4 align="center">Thank you for your purchase!</h4>
                    </div>      
                
                    <div class="panel-body">
                        <div style="width: 100%; ">
                            <p>
                                <? // DisplayAccessNo ?>
                                
                                <ul>
                                    <li><b><?=$transactionArray['first_name']?>, the details of your purchase are listed below:</b></li>
                                    <li><b>Airtime Access No.:</b> <?=$transactionArray['cardPIN']?></li>
                                    <li><b>Card Type:</b> <? echo "$valueInUGSh $LocalCurrency $networkName";?></li>
                                    <li><b>Receiver's First Name:</b> <?=$transactionArray['order_data']['receiver_first_name']?></li>                
                                    <li><b>Receiver's Phone:</b> <?=$transactionArray['order_data']['receiver_phone']?></li>
                                    <li><b>Date & Time:</b> <? echo "$timeCardSent";?> (<?=TIME_ZONE?> Time)</li>
                                    <li><b>Receiver Instructions:</b> To load dial <? echo $loadCode.$cardPIN."#";?></li>

                                    <? if($transactionArray['payer_email'] != DEFAULT_CLIENT_EMAIL){ ?>
                                    <li>The access number has also been sent to <b><?=$transactionArray['payer_email']?></b></li>
                                    <? } ?>
                                    <li>In case of any issues please email us at <b><?=SUPPORT_EMAIL?></b> or call <b><?=FOREIGN_SUPPORT_PHONE?></b>.</li>
                                </ul>
                    
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-4">
            <div class="panel panel-default">           
                <?php include(ACTIVE_THEME_PATH ."/voucherdeliveryinfo.php"); ?>                
            </div>
            <div class="panel panel-default">
                <?php include(ACTIVE_THEME_PATH ."/paypal.php"); ?>
            </div>
        </div>
        
    </div>
</div>
</div>
</div>
