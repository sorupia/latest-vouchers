<?php
/*******************************************************************************
**   FILE: aboutus.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Handles the display of about us content and header/footer files
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   
**
**   ADAPTED BY: 
**
*********************************************************************************/
?>
<?php 

include("wrapper/header.php"); 

include(ACTIVE_THEME_PATH ."/aboutus.php"); 

include("wrapper/footer.php"); 

?>
