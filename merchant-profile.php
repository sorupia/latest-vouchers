<?php
/*******************************************************************************
**   FILE: merchant-profile.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Handles the display of the merchant/vendor information and header/footer files
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   
**
**   ADAPTED BY: 
**
*********************************************************************************/
?>
<?php 

include("wrapper/header.php"); 

include(ACTIVE_THEME_PATH ."/merchant-profile.php"); 

include("wrapper/footer.php"); 

?>
