<?
    include "wrapper/header.php";
?>

<table cellpadding="0" cellspacing="0" class="listing">
    <tr>
        <th colspan="6"><h1>Reports - Stock, Revenue & Delivery</h1></th>
    </tr>
    
<?
    include "reports_header_nav.php";
?>
    
</table>

<form name="reports" id="reports" method="post" enctype="multipart/form-data" action="reports.php">
                <p>
                Employee Username 
                <input name="employee_username" type="text" class="field_small" id="employee_username" value="" /> 
                &nbsp;
                Employee Password 
                <input name="employee_password" type="password" class="field_small" id="employee_password" value="" />
                </p>
                <p>Start Date   
                  <input name="start_date" id="start_date" class="datefield" onfocus="this.blur();" />
                            <a href="#" title="Add Date" onclick="callDate('1', 'reports', 'start_date', this, event);" 
                            onfocus="this.blur();"><img src="images/icons/calendar.gif" alt="Add Date" class="datefieldicon" 
                            onmouseover='tooltip(this);' name='Open calendar tool' /></a>

                End Date 
                <input name="end_date" id="end_date" class="datefield" onfocus="this.blur();" />
                         <a href="#" title="Add Date" onclick="callEndDate('1', 'reports', 'end_date', this, event);" 
                         onfocus="this.blur();"><img src="images/icons/calendar.gif" alt="Add Date" class="datefieldicon" 
                         onmouseover='tooltip(this);' name='Open calendar tool' /></a>
            
                Ex. Rate 
                <input name="ex_rate" type="text" class="field_small" id="ex_rate" value="<?=EXCHANGE_RATE?>" />
  </p>
                <p>
                  <select name="report_type" accesskey="t">
                    <option value="stock_report">Stock Report</option>
                    <option value="revenue_report">Revenue Report</option>
                    <option value="sold_delivery_report">Delivery Report (Sold Date Full)</option>
                    <option value="purchase_delivery_report">Delivery Report (Purchase Date)</option>
                    <option value="sold_delivery_report_no_jacob">Delivery Report (Sold Date No Jacob)</option>
                    <option value="sold_delivery_report_no_jacob_detail">Delivery Report (Sold Date No Jacob Detail)</option>
                  </select>
                </p>
                <p>
                  <input type="submit" name="Generate" id="submit" 
                         value="Preview" class="submit" onfocus="this.blur();" />
                  <input type="button" name="cancel" id="cancel" value="Cancel" 
                         class="submit" onfocus="this.blur();" onclick="openPage('reports.php');" />
                </p>
</form>

    <?
        if(array_key_exists("report_type",$_POST))
        {
            $report_type = $_POST['report_type'];
        }
        else
        {
            $report_type = "";
        }

        $authenticated = false;

        if(array_key_exists("Generate",$_POST))
        {
            $username = $_POST['employee_username'];
            $password = $_POST['employee_password'];

            $authenticated = authenticateEmployee($username, $password, $countryAbbreviation);
        }

        if(array_key_exists("Generate",$_POST) && $authenticated)
        {
            $debug = false;
            
            $exchange_rate = $_POST['ex_rate']; 
            $start_date    = $_POST['start_date'].':01';
            $end_date      = $_POST['end_date'].':59';

            if($report_type=='stock_report')
            {
                echo"<br> Stock Report: $start_date to $end_date @ $exchange_rate <br><br>";

                $return_type = "return_array";

                $stock_array = purchase_report($start_date, 
                                                $end_date, 
                                                $exchange_rate,  
                                                $countryAbbreviation,
                                                $debug,
                                                $return_type);

                echo"<table cellpadding=\"0\" cellspacing=\"0\" class=\"listing\">";
                    displayStockReportNew($stock_array);
                echo"</table>";
            }
            else if($report_type=='revenue_report')
            {
                $cost_per_sms = PRICE_PER_SMS;
                echo"Revenue Report: $start_date to $end_date <br>";
                echo"Exchange Rate: $exchange_rate <br>";
                echo"Cost per SMS: $cost_per_sms <br>";

                $return_type = "return_array";

                $revenue_array = preciseRevenueReport($start_date, 
                                                      $end_date, 
                                                      $exchange_rate,
                                                      $cost_per_sms,  
                                                      $countryAbbreviation,
                                                      $debug,
                                                      $return_type);

                echo"<table cellpadding=\"0\" cellspacing=\"0\" class=\"listing\">";
                    displayRevenueReportNew($revenue_array);
                echo"</table>";

            }
            else if($report_type=="sold_delivery_report")
            {

                echo"Delivery Report by Date We Sold: <br> $start_date to $end_date <br>";

                echo"<form name=\"form1\" id=\"form1\" method=\"post\" action=\"export_delivery_report.php\">";
                    echo"<input name=\"start_date\" type=\"hidden\" value=\"$start_date\" />";
                    echo"<input name=\"end_date\" type=\"hidden\" value=\"$end_date\" />";
                    echo"<p> <input name=\"csv_file\" type=\"submit\" value=\"export as csv\" />";
                    echo" <input name=\"xls_file\" type=\"submit\" value=\"export as xls\" />                  </p>";
                echo"</form>";

                $usedCardsResult = queryByDateSold($countryAbbreviation,
                                                   $start_date,
                                                   $end_date);
                echo"<table cellpadding=\"0\" cellspacing=\"0\" class=\"listing\">";
                    displayTableNew($usedCardsResult);
                echo"</table>";

                //log the view to email
                if(NOTIFY_VIEW_OF_REPORTS_ENABLED)
                {
                    notify_view_of_delivery_report($_POST);
                }
            }
            else if($report_type=="purchase_delivery_report")
            {
                echo"Delivery Report by Date We Purchased: <br> $start_date to $end_date";

                $usedCardsResult = queryByDateWePurchased($countryAbbreviation,
                                                          $start_date,
                                                          $end_date);
                echo"<table cellpadding=\"0\" cellspacing=\"0\" class=\"listing\">";    
                    displayTableNew($usedCardsResult);
                echo"</table>";

                //log the view to email
                if(NOTIFY_VIEW_OF_REPORTS_ENABLED)
                {
                    notify_view_of_delivery_report($_POST);
                }
            }
            else if($report_type=="sold_delivery_report_no_jacob")
            {

                echo"Delivery Report without transaction id jacob by Date We Sold: <br> $start_date to $end_date <br>";

                echo"<form name=\"form1\" id=\"form1\" method=\"post\" action=\"export_delivery_report_no_jacob.php\">";
                    echo"<input name=\"start_date\" type=\"hidden\" value=\"$start_date\" />";
                    echo"<input name=\"end_date\" type=\"hidden\" value=\"$end_date\" />";
                    echo"<p> <input name=\"csv_file\" type=\"submit\" value=\"export as csv\" />";
                    echo" <input name=\"xls_file\" type=\"submit\" value=\"export as xls\" />                  </p>";
                echo"</form>";

                $usedCardsResult = queryByDateSold4AsReport($countryAbbreviation,
                                                            $start_date,
                                                            $end_date);
                echo"<table cellpadding=\"0\" cellspacing=\"0\" class=\"listing\">";
                    displayTableNew($usedCardsResult);
                echo"</table>";

                //log the view to email
                if(NOTIFY_VIEW_OF_REPORTS_ENABLED)
                {
                    notify_view_of_delivery_report($_POST);
                }
            }
            else if($report_type=="sold_delivery_report_no_jacob_detail")
            {

                echo"Delivery Report without transaction id jacob and more client detail by Date We Sold: <br> $start_date to $end_date <br>";

                echo"<form name=\"form1\" id=\"form1\" method=\"post\" action=\"export_delivery_report_no_jacob_detail.php\">";
                    echo"<input name=\"start_date\" type=\"hidden\" value=\"$start_date\" />";
                    echo"<input name=\"end_date\" type=\"hidden\" value=\"$end_date\" />";
                    echo"<p> <input name=\"csv_file\" type=\"submit\" value=\"export as csv\" />";
                    echo" <input name=\"xls_file\" type=\"submit\" value=\"export as xls\" />                  </p>";
                echo"</form>";

                $input_array['countryAbbreviation'] = $countryAbbreviation;
                $input_array['start_date']          = $start_date;
                $input_array['end_date']            = $end_date;
                $input_array['connection']          = connect2DB2($countryAbbreviation);
                $report_detail_data = sold_delivery_report_no_jacob_detail_2($input_array);

                echo "<table cellpadding=\"0\" cellspacing=\"0\" class=\"listing\">";
                    displayTableFromArray($report_detail_data);
                echo "</table>";

                disconnectDB($input_array['connection']);

                //log the view to email
                if(NOTIFY_VIEW_OF_REPORTS_ENABLED)
                {
                    notify_view_of_delivery_report($_POST);
                }
            }
        }
        elseif(!$authenticated && $_POST['Generate'])
        {
            echo "<br><br> Invalid username and password";
        }
    ?>
<?
    include "wrapper/footer.php";
?>