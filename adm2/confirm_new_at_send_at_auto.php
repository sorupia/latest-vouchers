<?
	include "wrapper/header.php";
?>

<table cellpadding="0" cellspacing="0" class="listing">
	<tr>
		<th colspan="6"><h1>Send Airtime as Auto Load</h1></th>
	</tr>

<?
	include "send_at_header_nav.php";
?>

</table>

<table cellpadding="0" cellspacing="0" class="listing">

<tr>
    <td align="left">
        <div class ="left">
            <div>
                <div>
                    <div>
                        <h2>Confirm airtime receiver details</h2><br/>
                        <b>Receiver Name:</b> <?=$_POST["receiverFirstName"]?><br/>
                        <b>Receiver Phone:</b> <?=$_POST["receiverPhone"]?><br/>
                        <b>Airtime To Be Received:</b> <?=LOCAL_CURRENCY?> <?=number_format($_POST['receiver_amount_in_local'], LOCAL_DECIMAL_PLACES)?><br/>
                        <b>Airtime To Be Received As:</b> <?=$_POST["airtime_load_option"]?><br/><br/>
                        <b>Transaction ID:</b> <?=$_POST["transID"]?><br/>
                        <b>Client's Email:</b> <?=$_POST["clientEmail"]?><br/>
                        <b>Client's First Name:</b> <?=$_POST["clientFirstName"]?><br/>
                        <b>Receiver's Email:</b> <?=$_POST["receiverEmail"]?><br/>
                    </div>
                    <div class="clear"></div>
                    <form action="sendAT.php" method="post" name="submit_order_form_auto" id="confirm_new_at_send_at_auto">
                        <div align="right" class="row" style="padding-right:20px;">
                            <input type="hidden" name="employeeUsername"         value="<?=$_POST['employeeUsername']?>">
                            <input type="hidden" name="employeePassword"         value="<?=$_POST['employeePassword']?>">
                            <input type="hidden" name="transID"                  value="<?=$_POST['transID']?>">
                            <input type="hidden" name="clientEmail"              value="<?=$_POST['clientEmail']?>">
                            <input type="hidden" name="clientFirstName"          value="<?=$_POST['clientFirstName']?>">
                            <input type="hidden" name="receiverFirstName"        value="<?=$_POST['receiverFirstName']?>">
                            <input type="hidden" name="receiverPhone"            value="<?=$_POST['receiverPhone']?>">
                            <input type="hidden" name="phoneConfirm"             value="<?=$_POST['phoneConfirm']?>">
                            <input type="hidden" name="receiver_amount_in_local" value="<?=$_POST['receiver_amount_in_local']?>">
                            <input type="hidden" name="receiverEmail"            value="<?=$_POST['receiverEmail']?>">
                            <input type="hidden" name="type"                     value="<?=$_POST['manual_send_auto_at']?>">
                            <input type="hidden" name="airtime_load_option"      value="<?=AIRTIME_LOAD_OPTION_AUTO?>">
                            <input type="Submit" name="Submit" id="Submit"       value="Submit" class="submit" onfocus="this.blur();" />
                            <input type="button" name="cancel" id="cancel"       value="Cancel" class="submit" style="margin-top: 5px;" onclick="history.go(-1);" onfocus="this.blur();" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </td>
</tr>


</table>

<?
	include "wrapper/footer.php";
?>