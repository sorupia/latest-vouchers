<?
	include "wrapper/header.php";
	
	$connection = connect2DB2($countryAbbreviation);
	$query		= "SELECT * FROM Employees WHERE username = '$_GET[id]' ";
    $query     .= "AND type <> 'superadmin' ";
	$result		= mysql_query($query);
	$data		= mysql_fetch_array($result);
	disconnectDB($connection);
?>
<table cellpadding="0" cellspacing="0" class="listing">

<tr>
	<th><h1><a href="javascript:history.go(-1);" class="sub_menu" title="back" onfocus="this.blur();">&laquo; Back</a> Edit Employee</h1></th>
</tr>

<?
	include "employees_nav.php";
?>

</table>

<br />

<table cellpadding="0" cellspacing="0" class="listing">

<tr>
    <td align="left">
         <form name="editEmployee" id="editEmployee" method="post" action="controllers/cases.php">
            <div class="left">
                <div class="row">
                    <label for="first_name">First Name</label>
					<input type="text" name="first_name" id="first_name" class="field" value="<?=$data['first_name']?>" />
                </div>
				<div class="row">
                    <label for="last_name">Last Name</label>
					<input type="text" name="last_name" id="last_name" class="field" value="<?=$data['last_name']?>" />
                </div>
				<div class="row">
                    <label for="username">Username</label>
					<input type="text" name="username" id="username" class="field" value="<?=$data['username']?>" />
                </div>
				<div class="row">
                    <label for="password">Password</label>
					<input type="password" name="password" id="password" class="field" value="" />
                 LEAVE EMPTY IF NOT CHANGING PASSWORD </div>
                <div class="clear"></div>
				<div class="row">
                    <label for="employee_type">Employee Type</label>
					<select name="employee_type" id="employee_type" >
					<?
						global $access_level_map;
						$statusValues = array_keys($access_level_map);
					    for($i = 0 ; $i < count($statusValues) - 1 ; $i++ )
						{
							$status = $statusValues[$i];
							if ( $status == $data['type'] )
							{
								echo"<option value=\"$status\" selected=\"selected\">$status</option>";
							}
							else
							{
								echo"<option value=\"$status\">$status</option>";
							}
						}
					?>
                    </select>
                </div>
            <div class="clear"></div>
            <div class="row">
				<label><input type="submit" name="submit" id="submit" value="Edit" class="submit" onfocus="this.blur();" /></label>
                <input type="button" name="cancel" id="cancel" value="Cancel" class="submit" style="margin-top: 5px;" onclick="history.go(-1);"onfocus="this.blur();" />
            </div>
            <div class="clear">&nbsp;</div>
            <input type="hidden" name="type" id="type" value="editEmployee" />
			<input type="hidden" name="page" id="page" value="<?=$_GET['page']?>" />
			<input type="hidden" name="employee_username" id="employee_username" value="<?=$_SESSION['uid']?>" />
        </form>
    </td>
</tr>

</table>

<? include "wrapper/footer.php"; ?>