<?
	include "wrapper/header.php";
?>
<table cellpadding="0" cellspacing="0" class="listing">

<tr>
	<th colspan="6"><h1>Clients - Search Results</h1></th>
</tr>

<tr>
	<td colspan="4" class="highlighted">
    	<form name="search" id="search" class="search" method="get" action="search_clients.php">        	
            <div class="right"><a href="#" onfocus="this.blur();" title="Search" onclick="submitSearch('search');">search</a></div>
            <div class="right">
            	<select name="searchType" id="searchType" >
                    <option value="name">Name</option>
                    <option value="email">Email Address</option>
                </select>
            </div>
        	<div class="right"><input type="text" name="keywords" id="keywords" /></div>
            <div class="right" style="margin:5px 0px 0px 0px;">Keywords to search for Client</div>            
        </form>
    </td>
</tr>
<?
	
	$type 		= $_GET['searchType'];
	$keywords	= $_GET['keywords'];	
	
	$values		= array("smsUserID", "lastLogin", "firstName","lastName","emailAddress","cellphone","subscriberStatus","airtimeStatus");
	
	switch($type)
	{
		case "name":
			$query = render_pager2($values, "SMSUsers WHERE firstName LIKE '%$keywords%' OR lastName LIKE '%$keywords%' ", 
			                       "smsUserID", 
								   40, 
								   'type='.$type.'&keywords='.$keywords,
								   $countryAbbreviation, 
								   'sms');			
			break;
			
		case "email":
			$query = render_pager2($values, 
			                       "SMSUsers WHERE emailAddress LIKE '%$keywords%' ",
								   "smsUserID",
								   40,
								   '?type='.$type.'&keywords='.$keywords,
								   $countryAbbreviation,
								   'sms');
			break;
	}
	
	$exist		= mysql_num_rows($query);
	$u			= ($_GET['r']) ? $_GET['r'] + 1 : 1 ;
	
	if($exist)
	{					
		while($data = mysql_fetch_array($query))
		{
			
			echo	'<tr>
						<td width="60" rowspan="2">'.
							"<a href='clients_edit_client?id=".$data['smsUserID']."' onfocus='this.blur();' 
							title='Edit User'><img src='images/icons/edit.gif' ald='Edit User' class='icon' /></a>".
							"<a href='used_at_dest_number.php?searchType=client_email&email=".$data['emailAddress']."' 
							onfocus='this.blur();' title='View Sent Airtime'><img src='images/icons/view.gif' 
							ald='View Sent Airtime' class='icon' /></a>".
							"<a href='clients_delete_client.php?id=".$data['smsUserID']."' onfocus='this.blur();' 
							title='Delete User'><img src='images/icons/delete.gif' ald='Delete User' class='icon' /></a>".
						'</td>
						<th width="200">Name</th>
						<th>Email Address</th>
						<th>Last Login</th>
						<th>Cell Phone</th>
						<th>SMS Status</th>
						<th>Airtime Status</th>
						
					</tr>';			
			
			echo	'<tr>'.
						'<td>'.$data['firstName'].' '.$data['lastName'].'</td>'.
						'<td><a href="'.$data['emailAddress'].'" title="'.$data['emailAddress'].'">'.$data['emailAddress'].'</a></td>'.
						'<td>'.$data['lastLogin'].' '.$data['lastLogin'].'</td>'.
						'<td>'.$data['cellphone'].'</td>'.
						'<td>'.$data['subscriberStatus'].'</td>'.
						'<td>'.$data['airtimeStatus'].'</td>'.
					'</tr>';
			
			$u++;
		}
	} else
	{
		echo '<tr><td align="center">Sorry, no results found.</td></tr>';
	}
	
	$values	= array("clientID", "clientFirstName", "clientLastName", "clientEmail");
	include "wrapper/footer.php";
?>