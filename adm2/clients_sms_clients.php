<?
	include "wrapper/header.php";
?>

<table cellpadding="0" cellspacing="0" class="listing">
	
	<tr>
		<th colspan="6"><h1>Clients - SMS Clients</h1></th>
	</tr>

	<?
		include "clients_header_nav.php";
	?>

</table>

<br />

<table cellpadding="0" cellspacing="0" class="listing">

<?
	$values   = array("smsUserID","lastLogin","firstName","lastName","emailAddress","creditsLeft","cellphone");
	
	$result = render_pager($values, "SMSUsers", "lastLogin DESC", 15, $countryAbbreviation, 'sms', subscriberStatus, "'VERIFIED'");
	$exist = mysql_num_rows($result);
	
	if($exist)
	{
		echo'<thead>
			<tr>
			<th>Option</th>
			<th>Last Login</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Email</th>
			<th>SMS Credits</th>
			<th>Phone</th>
			</tr>
		</thead>';
						
		while($data = mysql_fetch_array($result))
		{
			
			echo'<tbody><tr>
				<td>'.
				"<a href='clients_edit_client.php?id=".$data['smsUserID'].
				"' onfocus='this.blur();' title='Edit User' onmouseover='tooltip(this);' name='Edit user'>".
				"Edit</a> | ".
				"<a href='clients_delete_client.php?id=".$data['smsUserID']."' onfocus='this.blur();' 
				title='Delete User' onmouseover='tooltip(this);' name='Delete User'>Delete</a>".'</td>
			<td>'.$data['lastLogin'].'</td>
			<td>'.$data['firstName'].'</td>
			<td>'.$data['lastName'].'</td>
			<td><a href="mailto:'.$data['emailAddress'].
			    '" title="'.$data['emailAddress'].
			    '">'.$data['emailAddress'].'</a></td>
			<td>'.$data['creditsLeft'].'</td>
			<td>'.$data['cellphone'].'</td>
			</tr></tbody>';
			
			$u++;
		}
	}
	else
	{
		echo '<tr><td align="center" colspan="4">No users in database</td></tr>';
	}
?>

</table>

<?
	include "wrapper/footer.php";
?>