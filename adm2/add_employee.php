<?
	include "wrapper/header.php";
?>
<table cellpadding="0" cellspacing="0" class="listing">

<tr>
	<th><h1><a href="javascript:history.go(-1);" class="sub_menu" title="back" onfocus="this.blur();">&laquo; Back</a> Add Employee</h1></th>
</tr>

<?
	include "employees_nav.php";
?>

</table>

<br />

<table cellpadding="0" cellspacing="0" class="listing">

<tr>
    <td align="left">
         <form name="addEmployee" id="addEmployee" method="post" action="controllers/cases.php">
            <div class="left">
                <div class="row">
                    <label for="first_name">First Name</label>
					<input type="text" name="first_name" id="first_name" class="field" value="" />
                </div>
				<div class="row">
                    <label for="last_name">Last Name</label>
					<input type="text" name="last_name" id="last_name" class="field" value="" />
                </div>
				<div class="row">
                    <label for="username">Username</label>
					<input type="text" name="username" id="username" class="field" value="" />
                </div>
				<div class="row">
                    <label for="password">Password</label>
					<input type="password" name="password" id="password" class="field" />
                </div>
                <div class="clear"></div>
				<div class="row">
                    <label for="employee_type">Employee Type</label>
					<select name="employee_type" id="employee_type" >
					<?
						global $access_level_map;
						$statusValues = array_keys($access_level_map);				
					    for($i = 0 ; $i < count($statusValues) - 1 ; $i++ )
						{
							$status = $statusValues[$i];
							echo"<option value=\"$status\">$status</option>";
						}
					?>
                    </select>
                </div>
            <div class="clear"></div>
            <div class="row">
				<label><input type="submit" name="submit" id="submit" value="Add Employee" class="submit" onfocus="this.blur();" /></label>
                <input type="button" name="cancel" id="cancel" value="Cancel" class="submit" style="margin-top: 5px;" onclick="history.go(-1);"onfocus="this.blur();" />
            </div>
            <div class="clear">&nbsp;</div>
            <input type="hidden" name="type" id="type" value="addEmployee" />
			<input type="hidden" name="id" id="id" value="<?=$_GET['id']?>" />
			<input type="hidden" name="page" id="page" value="employees" />
			<input type="hidden" name="employee_username" id="employee_username" value="<?=$_SESSION['uid']?>" />
        </form>
    </td>
</tr>

</table>

<? include "wrapper/footer.php"; ?>