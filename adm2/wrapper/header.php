<?
	session_start();
	(!$_SESSION['uid']) ? header("Location: index.php") : null ;

	include_once "controllers/functions.php";
	include_once "controllers/paginator.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/src/General/GeneralFunctions.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/src/SecurityFunctions.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/src/ReportFunctions.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/UsedCardsTable.php";
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/Objects/AllCardsTable.php');
	require_once $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayResults.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/UsersTable.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/sms/Messenger.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/src/SessionFunctions.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/ObjectFunctions.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayTableFromArray.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/src/Reports/sold_delivery_report_no_jacob_detail.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/src/Reports/sold_delivery_report_no_jacob_detail_2.php";
	
	$countryAbbreviation = COUNTRY_ABBREVIATION;

	$page = check_me();
	$thisPage = substr($page,0,-4);

	checkPageAccess($page);
	
	//used to reset the logins for new airtime query by type and by purchase date
	if($page != "new_at_query_by_type.php" && $page != "delete_card.php" && $page != "edit_card.php") 
		$_SESSION['ViewNewAirtimeType'] = false;

	if($page != "new_at_purchase_date.php" && $page != "delete_card.php" && $page != "edit_card.php")
		$_SESSION['ViewNewAirtime'] = false;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<? if($page == "used_at.php") echo '<meta http-equiv="refresh" content="180"/>'; ?>
	
	<title><?=DOMAIN_NAME?> Admin</title>
	
	<link rel="stylesheet" href="admin.css" />
	
	<script language="javascript" type="text/javascript" src="controllers/tooltip.js"></script>
	<script language="javascript" type="text/javascript" src="controllers/getObjectOffset.js"></script>
	<script language="javascript" type="text/javascript" src="controllers/func.js"></script>
	<script language="javascript" type="text/javascript" src="controllers/addDateTime.js"></script>
	<script language="javascript" type="text/javascript" src="controllers/addSponsor.js"></script>
	<script language="javascript" type="text/javascript" src="../src/javascript.js"></script>
	<script language="javascript" type="text/javascript" src="controllers/assignItemIDs.js"></script>	    
</head>

<body>

<?
	
	if($_SESSION['user_access_level'] == $access_level_map['superadmin'])
		include "admin_header.php";
	else if($_SESSION['user_access_level'] == $access_level_map['admin'])
		include "admin_header.php";
	else if($_SESSION['user_access_level'] == $access_level_map['support'])
		include "support_header.php";
	else
		include "loader_header.php";
?>