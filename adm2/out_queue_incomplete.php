<?
	include "wrapper/header.php";
?>

<table cellpadding="0" cellspacing="0" class="listing">
	<tr>
		<th colspan="6"><h1>Outgoing Queue - Incomplete Payments Queue</h1></th>
	</tr>
<?
	include "out_queue_nav.php";
?>

</table>

<table cellpadding="0" cellspacing="0" class="listing">
<?
	
	$thisPage = substr($page,0,-4);

	$values   = array("id","timeOfPayment","transactionID","clientEmail",
	                  "receiverPhone","receiverFirstName",
	                  "receiverEmail","clientIP",
					  "ipLatitude","ipLongitude",
					  "ipCity","ipState","ipCountry","comments");
	
	$result = render_pager($values, "IncompletePaymentsQueue", "id DESC", 15, $countryAbbreviation, 'airtime');
	$exist = mysql_num_rows($result);
	
	if($exist)
	{
		echo'<thead>
			<tr>
			<th>Option</th>
			<th>ID</th>
			<th>Time of Payment</th>
			<th>Transaction ID</th>
			<th>Client Email</th>
			<th>Receiver Phone</th>
			<th>Receiver First Name</th>
			<th>Receiver Email</th>
			<th>Client IP</th>
			<th>IP Latitude</th>
			<th>IP Longitude</th>
			<th>IP City</th>
			<th>IP State</th>
			<th>IP Country</th>
			<th>Comments</th>
			</tr>
		</thead>';
						
		while($data = mysql_fetch_array($result))
		{
			
			echo'<tbody><tr>
				<td>'.
				"<a href='send_incomplete_payment.php?id=".$data['id']."&page=$thisPage' onfocus='this.blur();' title='Send Airtime'>".
				"Send Airtime</a> | ".
				"<a href='edit_incomplete_payment.php?id=".$data['id']."&page=$thisPage' onfocus='this.blur();' title='Edit Transaction'>".
				"Edit</a> | ".
				"<a href='#' onfocus='this.blur();' 
				title='Delete Transaction' onclick=delEntry('".$data['id']."','IncompletePaymentsQueue','id','$thisPage');>
				Delete</a>".'</td>
			<td>'.$data['id'].'</td>
			<td>'.$data['timeOfPayment'].'</td>
			<td>'.$data['transactionID'].'</td>
			<td>'.$data['clientEmail'].'</td>
			<td>'.$data['receiverPhone'].'</td>
			<td>'.$data['receiverFirstName'].'</td>
			<td>'.$data['receiverEmail'].'</td>
			<td>'.$data['clientIP'].'</td>
			<td>'.$data['ipLatitude'].'</td>
			<td>'.$data['ipLongitude'].'</td>
			<td>'.$data['ipCity'].'</td>
			<td>'.$data['ipState'].'</td>
			<td>'.$data['ipCountry'].'</td>
			<td>'.$data['comments'].'</td>
			</tr></tbody>';
			
			$u++;
		}
	}
	else
	{
		echo '<tr><td align="center" colspan="4">No users in Incomplete Payments Queue</td></tr>';
	}
?>

</table>

<?
	include "wrapper/footer.php";
?>