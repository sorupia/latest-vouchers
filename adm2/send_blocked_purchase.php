<?
	include "wrapper/header.php";
	
	$connection = connect2DB2($countryAbbreviation);
	$query		= "SELECT * FROM BlockedClientsQueue WHERE id = '$_GET[id]'";
	$result		= mysql_query($query);
	$data		= mysql_fetch_array($result);
	disconnectDB($connection);
?>
<table cellpadding="0" cellspacing="0" class="listing">

<tr>
	<th><h1><a href="javascript:history.go(-1);" class="sub_menu" title="back" onfocus="this.blur();">&laquo; Back</a> Send Blocked Payment</h1></th>
</tr>

<?
	include "out_queue_nav.php";
?>

</table>

<br />

<table cellpadding="0" cellspacing="0" class="listing">

<tr>
    <td align="left">
         <form name="sendBlockedPurchase" id="sendBlockedPurchase" method="post" action="controllers/cases.php" >
            <div class="left">
				<div class="row">
                    <label for="receiverPhone">Receiver Phone</label>
                    <input type="text" name="receiverPhone" id="receiverPhone" class="field" value="<?=$data['receiverPhone']?>" 
					 disabled="disabled" />
                </div>			
			    <div class="row">
                    <label for="employeeUsername">Employee Username</label>
                    <input type="text" name="employeeUsername" id="employeeUsername" class="field" value="<?=$_SESSION['username']?>" />
				</div>
				<div class="row">
                    <label for="employeePassword">Employee Password</label>
                    <input type="password" name="employeePassword" id="employeePassword" class="field" value="" />
				</div>
				<div class="row">
                    <label for="transID">Transaction ID</label>
                    <input type="text" name="transID" id="transID" class="field" value="<?=$data['transID']?>" 
                    disabled="disabled" />
				</div>
				<div class="row">
                    <label for="clientEmail">Client Email</label>
                    <input type="text" name="clientEmail" id="clientEmail" class="field" value="<?=$data['emailAddress']?>" 
                    disabled="disabled" />
                </div>
			    <div class="row">
                    <label for="clientFirstName">Client Name</label>
                    <input type="text" name="clientFirstName" id="clientFirstName" class="field" value="<?=$data['firstName']?>" 
                    disabled="disabled" />
				</div>
			    <div class="row">
                    <label for="clientPhone">Client Phone</label>
                    <input type="text" name="clientPhone" id="clientPhone" class="field" value="<?=$data['clientPhone']?>" 
                    disabled="disabled" />
				</div>
				<div class="row">
                    <label for="itemIDNumber">Item ID</label>
                    <input type="text" name="itemIDNumber" id="itemIDNumber" class="field" value="<?=$data['itemIDNumber']?>" 
                    disabled="disabled" />
                </div>
				<div class="row">
                    <label for="timeStamp">Time Stamp</label>
                    <input type="text" name="timeStamp" disabled="disabled" id="timeStamp" class="field" value="<?=$data['timeStamp']?>" 
                    disabled="disabled" />
				</div>
				<div class="row">
                    <label for="receiverFirstName">Receiver First Name</label>
                    <input type="text" name="receiverFirstName" disabled="disabled" id="receiverFirstName" class="field" 
					 value="<?=$data['receiverFirstName']?>" disabled="disabled" />
                </div>
				<div class="row">
                    <label for="receiverEmail">Receiver Email</label>
                    <input type="text" name="receiverEmail"  id="receiverEmail" class="field" value="<?=$data['receiverEmail']?>" 
					 disabled="disabled" />
                </div>
                <div class="row">
                    <label for="comments">Comments</label>
                    <input type="text" name="comments" id="comments" class="field" value="<?=$data['comments']?>" disabled="disabled" />
                </div>
				<div class="row">
                    <label for="clientIP">IP Address</label>
                    <input type="text" name="clientIP"  id="clientIP" class="field" value="<?=$data['clientIP']?>" 
					 disabled="disabled" />
                </div>
				<div class="row">
                    <label for="addressCity">Address City</label>
                    <input type="text" name="addressCity"  id="addressCity" class="field" value="<?=$data['addressCity']?>" 
					 disabled="disabled" />
                </div>
				<div class="row">
                    <label for="ipCity">IP City</label>
                    <input type="text" name="ipCity"  id="ipCity" class="field" value="<?=$data['ipCity']?>" 
					 disabled="disabled" />
                </div>
				<div class="row">
                    <label for="addressState">Address State</label>
                    <input type="text" name="addressState"  id="addressState" class="field" value="<?=$data['addressState']?>" 
					 disabled="disabled" />
                </div>
				<div class="row">
                    <label for="ipState">IP State</label>
                    <input type="text" name="ipState"  id="ipState" class="field" value="<?=$data['ipState']?>" 
					 disabled="disabled" />
                </div>
				<div class="row">
                    <label for="addressCountry">Address Country</label>
                    <input type="text" name="addressCountry"  id="addressCountry" class="field" value="<?=$data['addressCountry']?>" 
					 disabled="disabled" />
                </div>
				<div class="row">
                    <label for="ipCountry">IP Country</label>
                    <input type="text" name="ipCountry"  id="ipCountry" class="field" value="<?=$data['ipCountry']?>" 
					 disabled="disabled" />
                </div>
				<div class="row">
                    <label for="paymentStatus">Payment Status</label>
                    <input type="text" name="paymentStatus"  id="paymentStatus" class="field" value="<?=$data['paymentStatus']?>" 
					 disabled="disabled" />
                </div>
				<div class="row">
                    <label for="ourPayPalEmail">Our PayPal Email</label>
                    <input type="text" name="ourPayPalEmail"  id="ourPayPalEmail" class="field" value="<?=$data['ourPayPalEmail']?>" 
					 disabled="disabled" />
                </div>
				<div class="row">
                    <label for="paymentAmount">Payment Amount</label>
                    <input type="text" name="paymentAmount"  id="paymentAmount" class="field" value="<?=$data['paymentAmount']?>" 
					 disabled="disabled" />
                </div>
            </div>
            <div class="clear"></div>
            <div class="row">
				<label>
                    <input type="submit" name="submit" id="submit" value="Send" class="submit" onfocus="this.blur();"   />
			    </label>
                <input type="button" name="cancel" id="cancel" value="Cancel" class="submit" style="margin-top: 5px;" onclick="history.go(-1);"onfocus="this.blur();" />
            </div>
            <div class="clear">&nbsp;</div>
            <input type="hidden" name="type" id="type" value="sendBlockedPurchase" />
            <input type="hidden" name="id" id="id" value="<?=$data['id']?>" />
			<input type="hidden" name="transID" id="transID" value="<?=$data['transID']?>" />
            <input type="hidden" name="clientEmail" id="clientEmail" value="<?=$data['emailAddress']?>" />
            <input type="hidden" name="clientFirstName" id="clientFirstName" value="<?=$data['firstName']?>" />
			<input type="hidden" name="receiverFirstName" id="receiverFirstName" value="<?=$data['receiverFirstName']?>" />
            <input type="hidden" name="receiverPhone" id="receiverPhone" value="<?=$data['receiverPhone']?>" />
            <input type="hidden" name="receiverEmail" id="receiverEmail" value="<?=$data['receiverEmail']?>" />
            <input type="hidden" name="itemIDNumber" id="itemIDNumber" value="<?=$data['itemIDNumber']?>" />
			<input type="hidden" name="page" id="page" value="<?=$_GET['page']?>" />
        </form>
    </td>
</tr>

</table>

<? include "wrapper/footer.php"; ?>