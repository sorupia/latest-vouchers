<?
	include "wrapper/header.php";
?>
<table cellpadding="0" cellspacing="0" class="listing">
	
	<tr>
		<th colspan="6"><h1>Clients - PayPal Info</h1></th>
	</tr>

	<?
		include "clients_header_nav.php";
	?>

</table>

<table cellpadding="0" cellspacing="0" class="listing">

<?
	$values   = array("clientID",
	                  "clientFirstName",
					  "clientLastName",
					  "dateOfFirstPurchase",
					  "phoneNumber",
					  "clientEmail",
					  "addressCity",
					  "addressState",
					  "addressCountry");
	
	$query = render_pager($values, "Clients", "dateOfFirstPurchase ASC", 15);
	$exist = mysql_num_rows($query);
	
	if($exist)
	{		
		echo'<thead>
			<tr>
			<th class="noresize" width="57"><div style="width: 57px;">&nbsp;</div></th>
			<th>ClientID</th>
			<th>Name</th>
			<th>First Purchase</th>
			<th>Phone</th>
			<th>Email</th>
			<th>Street</th>
			<th>City</th>
			<th>Zip</th>
			<th>State</th>
			<th>Country</th>
			</tr>
		</thead>';
						
		while($data = mysql_fetch_array($query))
		{
			
			echo'<tbody><tr>
				<td width="60">'.
				"<a href='#' onfocus='this.blur();' title='Edit User'><img src='images/icons/edit.gif' ald='Edit User' class='icon' onmouseover='tooltip(this);' name='Edit user' /></a>".
				"<a href='#' onfocus='this.blur();' title='Delete User' onclick=delEntry('".$data['clientEmail']."','users','uid');><img src='images/icons/delete.gif' ald='Delete User' class='icon' onmouseover='tooltip(this);' name='Delete user' /></a>".'</td>
			<td>'.$data['clientID'].'</td>
			<td>'.$data['clientFirstName'].' '.$data['clientLastName'].'</td>
			<td>'.$data['dateOfFirstPurchase'].'</td>
			<td>'.$data['phoneNumber'].'</td>
			<td><a href="mailto:'.$data['clientEmail'].'" title="'.$data['clientEmail'].'">'.$data['clientEmail'].'</a></td>
			<td>'.$data['addressStreet'].'</td>
			<td>'.$data['addressCity'].'</td>
			<td>'.$data['addressZip'].'</td>			
			<td>'.ucwords($data['addressState']).'</td>
			<td>'.ucwords($data['addressCountry']).'</td>
			</tr></tbody>';
			
			$u++;
		}
	} 
	else
	{
		echo '<tr><td align="center" colspan="4">No users in database</td></tr>';
	}
?>
</table>
<?
	include "wrapper/footer.php";
?>