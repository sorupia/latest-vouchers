<?
	include "wrapper/header.php";
	
	$connection = connect2DB2($countryAbbreviation);
	$query		= "SELECT * FROM IncompletePaymentsQueue WHERE id = '$_GET[id]'";
	$result		= mysql_query($query);
	$data		= mysql_fetch_array($result);
	disconnectDB($connection);
?>
<table cellpadding="0" cellspacing="0" class="listing">

<tr>
	<th><h1><a href="javascript:history.go(-1);" class="sub_menu" title="back" onfocus="this.blur();">&laquo; Back</a> Send Incomplete Payment</h1></th>
</tr>

<?
	include "out_queue_nav.php";
?>

</table>

<br />

<table cellpadding="0" cellspacing="0" class="listing">

<tr>
    <td align="left">
         <form name="sendIncompletePayment" id="sendIncompletePayment" method="post" action="controllers/cases.php"
		  onsubmit="return IncompletePaymentQChecker(receiverPhone,receiverPhone,itemIDNumber,clientEmail,transactionID,clientFirstName)">
            <div class="left">
				<div class="row">
                    <label for="receiverPhone">Receiver Phone</label>
                    <input type="text" name="receiverPhone" id="receiverPhone" class="field" value="<?=$data['receiverPhone']?>" 
					 disabled="disabled" />
                </div>			
			    <div class="row">
                    <label for="employeeUsername">Employee Username</label>
                    <input type="text" name="employeeUsername" id="employeeUsername" class="field" value="<?=$_SESSION['username']?>" />
				</div>
				<div class="row">
                    <label for="employeePassword">Employee Password</label>
                    <input type="password" name="employeePassword" id="employeePassword" class="field" value="" />
				</div>
				<div class="row">
                    <label for="transactionID">Transaction ID</label>
                    <input type="text" name="transactionID" id="transactionID" class="field" value="<?=$data['transactionID']?>" />
				</div>
				<div class="row">
                    <label for="clientEmail">Client Email</label>
                    <input type="text" name="clientEmail" id="clientEmail" class="field" value="<?=$data['clientEmail']?>" />
                </div>
			    <div class="row">
                    <label for="clientFirstName">Client Name</label>
                    <input type="text" name="clientFirstName" id="clientFirstName" class="field" value="" />
				</div>				
				<div class="row">
                    <label for="itemID">Item ID</label>
                    <select name="itemIDNumber" size="1" id="itemIDNumber" >
						<?php
							global $cardMap;
							global $networkMap;

							$numberOfNetworks=count($networkMap[$countryAbbreviation]);
								
							echo'<option value="" selected>none selected</option>';
															
							for($networkIndex=0;$networkIndex<$numberOfNetworks;$networkIndex++){
								$thisNetwork=$networkMap[$countryAbbreviation][$networkIndex];
								$numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);
								
								for($j=0;$j<$numberOfDenominations;$j++){
									$thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
									if($j==0 && $networkIndex==0){
										echo"<option value=\"".$thisCard."\">".$thisCard."</option>";
									}
									else{
										echo"<option value=\"".$thisCard."\">".$thisCard."</option>";
									}
								}
							}
						?>
           			</select>
                </div>
				<div class="row">
                    <label for="receiverFirstName">Receiver First Name</label>
                    <input type="text" name="receiverFirstName" disabled="disabled" id="receiverFirstName" class="field" 
					 value="<?=$data['receiverFirstName']?>"/>
                </div>
				<div class="row">
                    <label for="timeOfPayment">Time Of Payment</label>
                    <input type="text" name="timeOfPayment" disabled="disabled" id="timeOfPayment" class="field" value="<?=$data['timeOfPayment']?>" />
				</div>
				<div class="row">
                    <label for="receiverEmail">Receiver Email</label>
                    <input type="text" name="receiverEmail"  id="receiverEmail" class="field" value="<?=$data['receiverEmail']?>" 
					 disabled="disabled" />
                </div>
                <div class="row">
                    <label for="comments">Comments</label>
                    <input type="text" name="comments" id="comments" class="field" value="<?=$data['comments']?>" />
                </div>
				<div class="row">
                    <label for="clientIP">IP Address</label>
                    <input type="text" name="clientIP"  id="clientIP" class="field" value="<?=$data['clientIP']?>" 
					 disabled="disabled" />
                </div>
				<div class="row">
                    <label for="ipCity">IP City</label>
                    <input type="text" name="ipCity"  id="ipCity" class="field" value="<?=$data['ipCity']?>" 
					 disabled="disabled" />
                </div>
				<div class="row">
                    <label for="ipState">IP State</label>
                    <input type="text" name="ipState"  id="ipState" class="field" value="<?=$data['ipState']?>" 
					 disabled="disabled" />
                </div>
				<div class="row">
                    <label for="ipCountry">IP Country</label>
                    <input type="text" name="ipCountry"  id="ipCountry" class="field" value="<?=$data['ipCountry']?>" 
					 disabled="disabled" />
                </div>
            </div>
            <div class="clear"></div>
            <div class="row">
				<label>
                    <input type="submit" name="submit" id="submit" value="Send" class="submit" onfocus="this.blur();"   />
			    </label>
                <input type="button" name="cancel" id="cancel" value="Cancel" class="submit" style="margin-top: 5px;" onclick="history.go(-1);"onfocus="this.blur();" />
            </div>
            <div class="clear">&nbsp;</div>
            <input type="hidden" name="type" id="type" value="sendIncompletePayment" />
            <input type="hidden" name="id" id="id" value="<?=$data['id']?>" />
			<input type="hidden" name="receiverPhone" id="receiverPhone" value="<?=$data['receiverPhone']?>" />
			<input type="hidden" name="receiverFirstName" id="receiverFirstName" value="<?=$data['receiverFirstName']?>" />
			<input type="hidden" name="clientIP" id="clientIP" value="<?=$data['clientIP']?>" />
			<input type="hidden" name="ipCity" id="ipCity" value="<?=$data['ipCity']?>" />
			<input type="hidden" name="ipState" id="ipState" value="<?=$data['ipState']?>" />
			<input type="hidden" name="ipCountry" id="ipCountry" value="<?=$data['ipCountry']?>" />
			<input type="hidden" name="page" id="page" value="<?=$_GET['page']?>" />
        </form>
    </td>
</tr>

</table>

<? include "wrapper/footer.php"; ?>