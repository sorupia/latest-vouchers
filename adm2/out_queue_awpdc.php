<?
	include "wrapper/header.php";
?>

<table cellpadding="0" cellspacing="0" class="listing">
	<tr>
		<th colspan="6"><h1>Outgoing Queue - AWPDC New Client Purchases</h1></th>
	</tr>
	
	<tr>
		<td colspan="4" class="highlighted">
			<form name="search" id="search" class="search" method="get" action="out_queue_awpdc.php">        	
				<div class="right">
				<a href="#" onfocus="this.blur();" title="Search" onclick="submitSearch('search');">search
				</a>
				</div>
				<div class="right">
					<select name="searchType" id="searchType" >
						<option value="null" selected="selected">Search type...</option>
						<option value="null">---</option>
						<option value="transaction_id">Transaction ID</option>
						<option value="email">Email Address</option>
					</select>
				</div>
				<div class="right"><input type="text" name="keywords" id="keywords" /></div>
				<div class="right" style="margin:5px 0px 0px 0px;">Keywords to search for AWPDC Client</div>            
			</form>
		</td>
	</tr>

<?
	include "out_queue_nav.php";
?>

</table>

<table cellpadding="0" cellspacing="0" class="listing">
<?
	$type 		= $_GET['searchType'];
	$keywords	= $_GET['keywords'];		
	
	$values   = array("record_id",
					  "transID",
					  "firstname",
					  "lastname",
					  "itemIDNumber",
					  "clientEmail",
					  "clientCountry",
					  "clientIP",
					  "status",
					  "comments",
					  "rmobile");
	
	switch($type)
	{
		case "transaction_id":
			$result = render_pager_awpdc($values, "cust_table", "record_id DESC", 15, 'gh','transID', "'$keywords'");
			break;
			
		case "email":
			$result = render_pager_awpdc($values, "cust_table", "record_id DESC", 15, 'gh','clientEmail', "'$keywords'");
			break;

		default:
			$result = render_pager_awpdc($values, "cust_table", "record_id DESC", 15, 'gh');
			break;
	}
	
	
	$exist = mysql_num_rows($result);
	
	if($exist)
	{
		echo'<thead>
			<tr>
			<th class="noresize" width="47"><div style="width: 47px;">&nbsp;</div></th>
			<th>Record ID</th>
			<th>Transaction ID</th>
			<th>Name</th>
			<th>Client Email</th>
			<th>Client Country</th>
			<th>Client IP</th>
			<th>Status</th>
			<th>Item ID</th>
			<th>Receiver Phone</th>
			<th>Comments</th>
			</tr>
		</thead>';
						
		while($data = mysql_fetch_array($result))
		{
			
			echo'<tbody><tr>
				<td width="47">'.
				"<a href='process_awpdc_client.php?id=".$data['record_id']."&page=$thisPage' onfocus='this.blur();' title='Process User'>".
				"<img src='images/icons/edit.gif' ald='Process User' class='icon' onmouseover='tooltip(this);' name='Process user' /></a>".
				"<a href='#' onfocus='this.blur();' title='Delete User' onclick=delEntry('".$data['record_id']."','cust_table','record_id');><img src='images/icons/delete.gif' ald='Delete User' class='icon' onmouseover='tooltip(this);' name='Delete user' /></a>".'</td>
			<td>'.$data['record_id'].'</td>
			<td>'.$data['transID'].'</td>
			<td>'.$data['firstname'].' '.$data['lastname'].'</td>
			<td>'.$data['clientEmail'].'</td>
			<td>'.$data['clientCountry'].'</td>
			<td>'.$data['clientIP'].'</td>
			<td>'.$data['status'].'</td>
			<td>'.$data['itemIDNumber'].'</td>
			<td>'.$data['rmobile'].'</td>
			<td>'.$data['comments'].'</td>
			</tr></tbody>';
			
			$u++;
		}
	}
	else
	{
		echo '<tr><td align="center" colspan="4">No users in New client purchases queue</td></tr>';
	}
?>

</table>

<?
	include "wrapper/footer.php";
?>