<?
/////////////////////////////////////////////////////////////// CONSTANTS /////
//MYSQL
define("SQL_SERVER", "internal-db.s3250.gridserver.com");
define("SQL_USER", "db3250_uganda");
define("SQL_PASS", "c3ntr3b311");
define("SQL_DB", "sendairtime_com_-_live");
// FTP
define("FTP_SERVER", "");
define("FTP_USER", "");
define("FTP_PASS", "");
// OTHER
define("SALT_LENGTH", 9);

/////////////////////////////////////////////////////////////// MISC Functions /////
///Generate Hash
function hashit($text, $salt)
{
	$salt = !($salt) ? substr(md5(random()), 0, SALT_LENGTH) : substr($salt, 0, SALT_LENGTH);
	return $salt.md5($text);
}
//db connection
function db_connect()
{
	$mysql_conn = mysql_connect(SQL_SERVER,SQL_USER,SQL_PASS) or die;
	$db = mysql_select_db(SQL_DB);
	return $mysql_conn;
}
function mysql_next_id($table)
{
    $mysql_conn = db_connect();
	$result 	= mysql_query('SHOW TABLE STATUS LIKE "'.$table.'"');
    $rows 		= mysql_fetch_array($result);
    return $rows['Auto_increment'];
	db_close($mysql_conn);
}
//db close
function db_close($mysql_conn)
{
	mysql_close($mysql_conn);
}

///Limit text function
function ltext($text, $limit)
{
  // figure out the total length of the string
  if( strlen($text) > $limit )
  {
    # cut the text
    $text = substr($text, 0, $limit );
    # lose any incomplete word at the end
    $text = substr($text, 0, -(strlen(strrchr($text,' '))) ) . ' ...';
  }
  // return the processed string
  return $text;
}

//check page
//function check_me()
//{
//	preg_match("/([^\\/]*\\.(php|htm))$/", $_SERVER['PHP_SELF'], $res);
//	return $res[0];
//}

//get url
function get_url($link)
{
	preg_match("/http:\/\/([^\/]+)\//", $link, $res);
	return $res[0];
}
///Function to check for valid email address
function chk_eml($useremail)
{
	error_reporting(0);
   if(eregi("^[a-zA-Z0-9_]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$]", $useremail)){
      return FALSE;
   }

   list($Username, $Domain) = split("@",$useremail);

   if(getmxrr($Domain, $MXHost)){
      return TRUE;
   } else {

      if(fsockopen($Domain, 25, $errno, $errstr, 30)){
         return TRUE; 
      } else {
         return FALSE; 
      }
	  
   }
}
//random
function random()
{
	$possible = '23456789bcdfghjkmnpqrstvwxyz';
	$code = '';
	
	for($r = 0; $r < SALT_LENGTH; $r++)
	{
		$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
	}
	
	return $code;
}
/////////////////////////////////////////////////////////////// PICS Functions /////
function resizer($image, $wMax, $hMax, $name, $path)
{
	//create new thumbnail image
	$mWidth = $wMax;
	$mHeight = $hMax;
	
	//get new dimensions
	list($width_orig, $height_orig) = getimagesize($image);
	//image ratio
	$ratio_orig = $width_orig/$height_orig;
	
		if ($mWidth / $mHeight > $ratio_orig) {
			$mWidth = $mHeight * $ratio_orig;
		} else {
			$mHeight = $mWidth / $ratio_orig;
		}
		
	//resample
	$image_p = imagecreatetruecolor($mWidth, $mHeight);
	$image_n = imagecreatefromjpeg($image);
	imagecopyresampled($image_p, $image_n, 0, 0, 0, 0, $mWidth, $mHeight, $width_orig, $height_orig);
	
	//output
	imagejpeg($image_p, $path."/".$name, 100);
	imagedestroy($image_p);
}

function thumb($image, $canvas, $wMax, $hMax, $name, $path)
{
	//create new thumbnail image
	$mWidth = $wMax;
	$mHeight = $hMax;
	
	// Content type
	//get new dimensions
	list($width_orig, $height_orig) = getimagesize($image);
	
	//create canvas
	$image_p = imagecreatetruecolor($canvas, $canvas);
	$image_n = imagecreatefromjpeg($image);
	//prepare image resizing and crop -- Center crop location
	$newwidth = $width_orig / 2;
	$newheight = $height_orig / 2;
	$cropLeft = ($newwidth / 2) - ($mWidth / 2);
	$cropHeight = ($newheight / 2) - ($mHeight / 2);
	
	//image ratio
	$ratio_orig = $width_orig/$height_orig;
	
		if ($mWidth / $mHeight > $ratio_orig) {
			$mWidth = $mHeight * $ratio_orig;
		} else {
			$mHeight = $mWidth / $ratio_orig;
		}
	$yPos = ($canvas - $mHeight)/2;
	//create new image
	imagecopyresampled($image_p, $image_n, 0, 0, $cropLeft, $cropHeight, $mWidth, $mHeight, $newwidth, $newheight);
	
	//output
	imagejpeg($image_p, $path."/".$name, 100);
	imagedestroy($image_p);
}

/////////////////////////////////////////////////////////////// FTP Functions //////
//Remove a directory with or without files
function rm_dir($dir_name)
{
	$ftp_server = FTP_SERVER;
	$conn_id = ftp_connect($ftp_server);
	ftp_login($conn_id, FTP_USER, FTP_PASS);
	ftp_pasv( $conn_id, true );
	$contents = ftp_nlist($conn_id, $dir_name);
	$total_files = count($contents);
	if(!empty($contents)){
		for($x = 0; $x < $total_files; $x++){
			ftp_delete($conn_id, $dir_name."/".$contents[$x]);
		}
	}
	ftp_rmdir($conn_id, $dir_name);
	ftp_close($conn_id);
}

//Delete file
function delete_file($path)
{
	$ftp_server = FTP_SERVER;
	$conn_id = ftp_connect($ftp_server);
	ftp_login($conn_id, FTP_USER, FTP_PASS);
	ftp_delete($conn_id, $path);
	ftp_close($conn_id);
}
//Make directory
function make_dir($dir_name, $path)
{
	$ftp_server = FTP_SERVER;
	$conn_id = ftp_connect($ftp_server);
	ftp_login($conn_id, FTP_USER, FTP_PASS);
	ftp_chdir($conn_id, $path);
	ftp_mkdir($conn_id, $dir_name);
	ftp_close($conn_id);
}

//Upload files
function upload($destination_file, $file, $path)
{
	$ftp_server = FTP_SERVER;
	$conn_id = ftp_connect($ftp_server);
	ftp_login($conn_id, FTP_USER, FTP_PASS);
	ftp_chdir($conn_id, $path);
	ftp_put($conn_id, $destination_file, $file, FTP_BINARY);
	ftp_close($conn_id);
}

//Rename files
function renameFile($old_file, $new_file)
{
	$ftp_server = FTP_SERVER;
	$conn_id = ftp_connect($ftp_server);
	ftp_login($conn_id, FTP_USER, FTP_PASS);
	ftp_chdir($conn_id, $path);
	ftp_rename($conn_id, $old_file, $new_file);
	ftp_close($conn_id);
}


//Get filesize
function get_size($path)
{
	$size = (filesize($path)/1024);
	return round($size, "2");	
}
?>
