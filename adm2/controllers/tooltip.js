// JavaScript Document
function tooltip(obj)
{
	
	var box			= document.createElement('div');
	var text		= document.createElement('span')
	var offsetX 	= getOffsetX(obj);
	var offsetY 	= getOffsetY(obj);
	
	box.className	= 'tooltip';
	text.innerHTML 	= obj.name;		
	
    obj.onmouseout = function()
	{
		document.body.removeChild(box);	
	}
	
	box.appendChild(text);
	document.body.appendChild(box);
	
	if( navigator.appName == "Microsoft Internet Explorer")
	{
	    box.style.left 	= offsetX - 11 + 'px';
	    box.style.top 	= (offsetY - (box.offsetHeight + 2)) + 'px';
	}
	else
	{
	    box.style.left 	= offsetX + 8 + 'px';
	    box.style.top 	= (offsetY - (box.offsetHeight + 2)) + 'px';
	}
}