<?
	session_start();
	include "functions.php";
	
	switch($_POST['type'])
    {
		/*[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ REMINDERS ]]*/
		case "addReminder":
			
			$html .= "<form name='addReminder' id='addReminder'>
						<div class='left'>
							<div class='formRow'>
								<label for='title' id='titleLabel'>Title</label>
								<input name='title' id='title' maxlength='255' />
							</div>
							<div class='formRow'>
								<label for='prid' id='pridLabel'>Priority</label>
								<select name='prid' id='prid'>
									<option value='null'>Choose</option>
									<option value='null'>---</option>";
									
									$mysql_conn = db_connect();
									$query		= mysql_query('SELECT * FROM priority ORDER BY prid ASC');
									
									while($p = mysql_fetch_array($query))
									{
										$html .= '<option value="'.$p['prid'].'">'.ucwords($p['type']).'</option>';
									}
									
									mysql_close($mysql_conn);
										
				$html .=		"</select>
							</div>
							<div class='formRow'>
								<label for='note' id='noteLabel'>Note</label>
								<textarea name='note' id='note' class='notrequired' /></textarea>
							</div>
						</div>
						<div class='clear'></div>
						<div class='buttonRow'><input type='button' name='submit' id='submit' value='Add Reminder' class='submit' onFocus='this.blur();' onClick=chkFrm('addReminder','controllers/cases.php'); /></div>
						<input type='hidden' name='uid' id='uid' value='".$_SESSION['uid']."' />
						<input type='hidden' name='type' id='type' value='addReminder' />
					</form>";
			
			echo $html;
			
			break;
			
		case "editReminder":
			
			$id			= $_POST['id'];
			$mysql_conn = db_connect();
			$query		= mysql_query("SELECT title, prid, note FROM reminders WHERE rid = '$id'");
			$data		= mysql_fetch_array($query);
			
			$html .= "<form name='editReminder' id='editReminder'>
						<div class='left'>
							<div class='formRow'>
								<label for='title' id='titleLabel'>Title</label>
								<input name='title' id='title' maxlength='255' value='".$data['title']."' />
							</div>
							<div class='formRow'>
								<label for='prid' id='pridLabel'>Priority</label>
								<select name='prid' id='prid'>
									<option value='null'>Choose</option>
									<option value='null'>---</option>";
									
									$query	= mysql_query('SELECT * FROM priority ORDER BY prid ASC');
									
									while($p = mysql_fetch_array($query))
									{
										if($p['prid'] == $data['prid'])
										{
											$html .= '<option value="'.$p['prid'].'" selected="selected">'.ucwords($p['type']).'</option>';
										} else
										{
											$html .= '<option value="'.$p['prid'].'">'.ucwords($p['type']).'</option>';
										}
									}
										
				$html .=		"</select>
							</div>
							<div class='formRow'>
								<label for='note' id='noteLabel'>Note</label>
								<textarea name='note' id='note' class='notrequired' />".$data['note']."</textarea>
							</div>
						</div>
						<div class='clear'></div>
						<div class='buttonRow'><input type='button' name='submit' id='submit' value='Save Changes' class='submit' onFocus='this.blur();' onClick=chkFrm('editReminder','controllers/cases.php'); /></div>
						<input type='hidden' name='type' id='type' value='editReminder' />
						<input type='hidden' name='id' id='id' value='".$id."' />
					</form>";
			
			echo $html;
			mysql_close($mysql_conn);
			
			break;
		/*[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ CLIENTS ]]*/
		case "addClient":
			
			$html .= "<form name='addClient' id='addClient'>
						<div class='left'>
							<div class='formRow'>
								<label for='contact' id='contactLabel'>Contact</label>
								<input name='contact' id='contact' maxlength='60' />
							</div>
							<div class='formRow'>
								<label for='url' id='urlLabel'>Url</label>
								<input name='url' id='url' maxlength='255' class='notrequired' />
							</div>
							<div class='formRow'>
								<label for='city' id='cityLabel'>City</label>
								<input name='city' id='city' maxlength='255' class='notrequired' />
							</div>
							<div class='formRow'>
								<label for='pcode_zip' id='pcode_zipLabel'>Postal/Zip Code</label>
								<input name='pcode_zip' id='pcode_zip' maxlength='7' class='notrequired' />
							</div>
							<div class='formRow'>
								<label for='phone01' id='phone01Label'>Phone 01</label>
								<input name='phone01' id='phone01' maxlength='20' />
							</div>
							<div class='formRow'>
								<label for='email' id='emailLabel'>Email</label>
								<input name='email' id='email' maxlength='40' />
							</div>
						</div>
						<div class='right'>        	
							<div class='formRow'>
								<label for='company' id='companyLabel'>Company</label>
								<input name='company' id='company' maxlength='255' class='notrequired' />
							</div>
							<div class='formRow'>
								<label for='address' id='addressLabel'>Address</label>
								<input name='address' id='address' maxlength='255' class='notrequired' />
							</div>
							<div class='formRow'>
								<label for='prov_state' id='prov_stateLabel'>Province/State</label>
								<input name='prov_state' id='prov_state' maxlength='40' class='notrequired' />
							</div>
							<div class='formRow'>
								<label for='country' id='countryLabel'>Country</label>
								<select name='country' id='country'>
									<option value='null'>Choose</option>
									<option value='null'>---</option>";
									
									$mysql_conn = db_connect();
									$query		= mysql_query('SELECT * FROM country ORDER BY country ASC');
									
									while($cntry = mysql_fetch_array($query))
									{
										$html .= '<option value="'.$cntry['coid'].'">'.$cntry['country'].'</option>';
									}
									
									mysql_close($mysql_conn);
										
				$html .=		"</select>
							</div>
							<div class='formRow'>
								<label for='phone02' id='phone02Label'>Phone 02</label>
								<input name='phone02' id='phone02' maxlength='20' class='notrequired' />
							</div>
							<div class='formRow'>
								<label for='fax' id='faxLabel'>Fax</label>
								<input name='fax' id='fax' maxlength='20' class='notrequired' />
							</div>
						</div>
						<div class='clear'></div>
						<div class='buttonRow'><input type='button' name='submit' id='submit' value='Add Client' class='submit' onFocus='this.blur();' onClick=chkFrm('addClient','controllers/cases.php'); /></div>
						<input type='hidden' name='uid' id='uid' value='".$_SESSION['uid']."' />
						<input type='hidden' name='type' id='type' value='addC' />
					</form>";
			
			echo $html;
			
			break;
			
		case "editClient":
			
			$clientEmail = $_POST['clientEmail'];
			echo $clientEmail.'<br>';
			$mysql_conn = db_connect();
			$query = mysql_query("SELECT clientFirstName, clientLastName, dateOfFirstPurchase, phoneNumber, clientEmail, addressCity, addressState, addressCountry FROM Clients WHERE clientEmail = '$clientEmail'");
			$data = mysql_fetch_array($query);
			$html = '';
			
			$html .= "<form name='editClient' id='editClient'>
			<div class='left'>
				<div class='formRow'>
					<label for='clientFirstName' id='clientFirstNameLabel'>First Name</label>
					<input name='clientFirstName' id='clientFirstName' maxlength='60' value='".
					$data['clientFirstName']."' />
				</div>
				<div class='formRow'>
					<label for='clientLastName' id='clientLastNameLabel'>Last Name</label>
				<input name='clientLastName' id='clientLastName' maxlength='20' value='".
				$data['clientLastName']."' class='notrequired' />
				</div>
				
				<div class='formRow'>
					<label for='phoneNumber' id='phoneNumberLabel'>Phone Number</label>
					<input name='phoneNumber' id='phoneNumber' maxlength='20' value='".
					$data['phoneNumber']."' class='notrequired' />
				</div>
				<div class='formRow'>
					<label for='clientEmail' id='clientEmailLabel'>Email</label>
					<input name='clientEmail' id='clientEmail' maxlength='60' value='".
					$data['clientEmail']."' class='notrequired' />
				</div>
				<div class='formRow'>
					<label for='addressCity' id='addressCityLabel'>City</label>
					<input name='addressCity' id='addressCity' maxlength='20' value='".
					$data['addressCity']."' />
				</div>
				<div class='formRow'>
					<label for='addressState' id='addressStateLabel'>State</label>
					<input name='addressState' id='addressState' maxlength='40' value='".
					$data['addressState']."' />
				</div>
			</div>
			<div class='right'>        	
				<div class='formRow'>
					<label for='addressCountry' id='addressCountryLabel'>Country</label>
					<input name='addressCountry' id='addressCountry' maxlength='25' value='".
					$data['addressCountry']."' class='notrequired' />
				</div>
			</div>
			<div class='clear'></div>
			<div class='buttonRow'><input type='button' name='submit' id='submit' value='Save Changes' class='submit' onFocus='this.blur();' onClick=chkFrm('editClient','controllers/cases.php'); /></div>
				<input type='hidden' name='clientEmail' id='clientEmail' value='".$clientEmail."' />
				<input type='hidden' name='type' id='type' value='editClient' />
			</form>";
			
			echo $html;
			mysql_close($mysql_conn);
			
			break;
		/*[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ RESOURCES ]]*/
		case "addResource":
			
			$html .= "<form name='addResource' id='addResource'>
						<div class='left'>
							<div class='formRow'>
								<label for='name' id='nameLabel'>Name</label>
								<input name='name' id='name' maxlength='60' />
							</div>
							<div class='formRow'>
								<label for='url' id='urlLabel'>Url</label>
								<input name='url' id='url' maxlength='255' class='notrequired' />
							</div>
							<div class='formRow'>
								<label for='role' id='roleLabel'>Role</label>
								<input name='role' id='role' maxlength='40' />
							</div>
						</div>
						<div class='right'>        	
							<div class='formRow'>
								<label for='email' id='emailLabel'>Email</label>
								<input name='email' id='email' maxlength='40' />
							</div>
							<div class='formRow'>
								<label for='phone' id='phoneLabel'>Phone</label>
								<input name='phone' id='phone' maxlength='20' class='notrequired' />
							</div>
							<div class='formRow'>
								<label for='rate' id='rateLabel'>Rate</label>
								<input name='rate' id='rate' maxlength='4' class='notrequired' />
							</div>
						</div>
						<div class='clear'></div>
						<div class='formRow'>
							<label for='note' id='noteLabel'>Note</label>
							<textarea name='note' id='note' class='notrequired' style='width:472px;' /></textarea>
						</div>
						<div class='buttonRow'><input type='button' name='submit' id='submit' value='Add Resource' class='submit' onFocus='this.blur();' onClick=chkFrm('addResource','controllers/cases.php'); /></div>
						<input type='hidden' name='uid' id='uid' value='".$_SESSION['uid']."' />
						<input type='hidden' name='type' id='type' value='addR' />
					</form>";
			
			echo $html;
			
			break;
			
		case "editResource":
			
			$id			= $_POST['id'];
			$mysql_conn = db_connect();
			$query		= mysql_query("SELECT name, email, phone, role, url, rate, note FROM resources WHERE rid = '$id'");
			$data		= mysql_fetch_array($query);
			
			$html .= "<form name='editResource' id='editResource'>
						<div class='left'>
							<div class='formRow'>
								<label for='name' id='nameLabel'>Name</label>
								<input name='name' id='name' maxlength='60' value='".$data['name']."' />
							</div>
							<div class='formRow'>
								<label for='url' id='urlLabel'>Url</label>
								<input name='url' id='url' maxlength='255' value='".$data['url']."' class='notrequired' />
							</div>
							<div class='formRow'>
								<label for='role' id='roleLabel'>Role</label>
								<input name='role' id='role' maxlength='40' value='".$data['role']."' />
							</div>
						</div>
						<div class='right'>        	
							<div class='formRow'>
								<label for='email' id='emailLabel'>Email</label>
								<input name='email' id='email' maxlength='40' value='".$data['email']."' />
							</div>
							<div class='formRow'>
								<label for='phone' id='phoneLabel'>Phone</label>
								<input name='phone' id='phone' maxlength='20' value='".$data['phone']."' class='notrequired' />
							</div>
							<div class='formRow'>
								<label for='rate' id='rateLabel'>Rate</label>
								<input name='rate' id='rate' maxlength='4' value='".$data['rate']."' class='notrequired' />
							</div>
						</div>
						<div class='clear'></div>
						<div class='formRow'>
							<label for='note' id='noteLabel'>Note</label>
							<textarea name='note' id='note' class='notrequired' style='width:472px;' />".$data['note']."</textarea>
						</div>
						<div class='buttonRow'><input type='button' name='submit' id='submit' value='Save Resource' class='submit' onFocus='this.blur();' onClick=chkFrm('editResource','controllers/cases.php'); /></div>
						<input type='hidden' name='id' id='id' value='".$id."' />
						<input type='hidden' name='type' id='type' value='editR' />
					</form>";
			
			echo $html;
			
			break;
	}
?>
