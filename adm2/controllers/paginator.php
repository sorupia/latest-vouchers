<?
	function render_pager($array, $table, $order_by, $limit, $country = 'ug', 
	                      $db_type = 'airtime', $field = 0, $value = 0, $join = 0)
	{
		(!isset($_GET['r'])) ? $r = 0 : $r = $_GET['r'];
		(!isset($_GET['p'])) ? $p = 1 : $p = $_GET['p'];
		
		$page			= $_SERVER['PHP_SELF'];
		$columns		= implode($array, ",");
		$where			= (($field) && ($value)) ? "WHERE $field = $value" : null ;
		$table2			= ($join) ? "INNER JOIN $join" : null ;
		
		$connection     = ($db_type == 'airtime') ? connect2DB2($country) : connect2SMSDB2($country);
		$query	 		= "SELECT SQL_CALC_FOUND_ROWS $columns FROM $table $table2 $where ORDER BY $order_by LIMIT $r, $limit";
		
		$result         = mysql_query($query);
		$total_records 	= mysql_fetch_row(mysql_query("SELECT FOUND_ROWS()"));
		
		$total_pages	= ceil($total_records[0] / $limit);
		$next			= $r + $limit;
		$prev			= $r - $limit;
		$ntpnum			= $p + 1;
		$prpnum			= $p - 1;
		
		$u 	= (array_key_exists("u",$_GET)) ? '&u='.$_GET['u'] : null ;		
		
		$prevlink = ($p == 1) ?  null : '&nbsp;<a href="'.$page.'?r='.$prev.
		             '&p='.$prpnum.$u.'" onfocus="this.blur();">prev</a>';
					 
		$nextlink = ($p == $total_pages) ?  null : '<a href="'.$page.'?r='.$next.
		             '&p='.$ntpnum.$u.'" onfocus="this.blur();">next</a>';
		
		if(($total_records) && ($total_records[0] > $limit))
		{
			$row = 0;
			
			echo 	'<div>'.$prevlink.'&nbsp;'.
				$p.' of '.$total_pages.' . '.$total_records[0].' result(s)'.
				'&nbsp;';
		
			for($i = 1; $i <= $total_pages; $i++)
			{
				//echo ($i == $p) ? $i.'&nbsp;' : '<a href="'.$page.'?r='.$row.'&p='.$i.$u.
				//					 '" onfocus="this.blur();">'.$i.'</a>&nbsp;';
				$row = $limit * $i;
			}
		
			echo $nextlink.'</div>';
		} 
		else
		{
			//Do Nothing
		}
		
		disconnectDB($connection);
		return $result;		
	}
	
	function render_pager2($array, $table, $order_by, $limit, $append = 0, $country = 'ug',
	                      $db_type = 'airtime')
	{
		(!isset($_GET['r'])) ? $r = 0 : $r = $_GET['r'];
		(!isset($_GET['p'])) ? $p = 1 : $p = $_GET['p'];
		
		$page			= $_SERVER['PHP_SELF'];
		$columns		= implode($array, ",");
		$connection     = ($db_type == 'airtime') ? connect2DB2($country) : connect2SMSDB2($country);
		$query	 		= mysql_query("SELECT SQL_CALC_FOUND_ROWS $columns FROM $table ORDER BY $order_by LIMIT $r, $limit");
		$total_records 	= mysql_fetch_row(mysql_query("SELECT FOUND_ROWS()"));
		$total_pages	= ceil($total_records[0] / $limit);
		$next			= $r + $limit;
		$prev			= $r - $limit;
		$ntpnum			= $p + 1;
		$prpnum			= $p - 1;
		
		//$u 	= ($_GET['u']) ? '&u='.$_GET['u'] :'';		
		$prevlink = ($p == 1) ?  null : '&nbsp;<a href="'.$page.'?r='.$prev.'&p='.$prpnum.'&'.$append.'" onfocus="this.blur();">prev</a>';
		$nextlink = ($p == $total_pages) ?  null : '<a href="'.$page.'?r='.$next.'&p='.$ntpnum.'&'.$append.'" onfocus="this.blur();">next</a>';
		
		if(($total_records) && ($total_records[0] > $limit))
		{
			$row = 0;
			
			echo 	'<div>'.$prevlink.'&nbsp;'.
				$p.' of '.$total_pages.' . '.$total_records[0].' result(s)'.
				'&nbsp;';
		
			for($i = 1; $i <= $total_pages; $i++)
			{
				//echo ($i == $p) ? $i.'&nbsp;' : '<a href="'.$page.'?r='.$row.'&p='.$i.$u.
				//					 '" onfocus="this.blur();">'.$i.'</a>&nbsp;';
				$row = $limit * $i;
			}
		
			echo $nextlink.'</div>';
		} 
		else
		{
			//Do Nothing
		}
		
		disconnectDB($connection);
		return $query;		
		
	}
	
	function render_pager_awpdc($array, $table, $order_by, $limit, $country = 'ug',
	                            $field = 0, $value = 0, $join = 0)
	{
		(!isset($_GET['r'])) ? $r = 0 : $r = $_GET['r'];
		(!isset($_GET['p'])) ? $p = 1 : $p = $_GET['p'];
		
		$page			= $_SERVER['PHP_SELF'];
		$columns		= implode($array, ",");
		$where			= (($field) && ($value)) ? "WHERE $field = $value" : null ;
		$table2			= ($join) ? "INNER JOIN $join" : null ;
		
		$connection     = connect2_awpc_db($country);
		$query	 		= "SELECT SQL_CALC_FOUND_ROWS $columns FROM $table $table2 $where ORDER BY $order_by LIMIT $r, $limit";
		
		$result         = mysql_query($query);
		$total_records 	= mysql_fetch_row(mysql_query("SELECT FOUND_ROWS()"));
		
		$total_pages	= ceil($total_records[0] / $limit);
		$next			= $r + $limit;
		$prev			= $r - $limit;
		$ntpnum			= $p + 1;
		$prpnum			= $p - 1;
		
		$u 	= ($_GET['u']) ? '&u='.$_GET['u'] : null ;		
		
		$prevlink = ($p == 1) ?  null : '&nbsp;<a href="'.$page.'?r='.$prev.
		             '&p='.$prpnum.$u.'" onfocus="this.blur();">prev</a>';
					 
		$nextlink = ($p == $total_pages) ?  null : '<a href="'.$page.'?r='.$next.
		             '&p='.$ntpnum.$u.'" onfocus="this.blur();">next</a>';
		
		if(($total_records) && ($total_records[0] > $limit))
		{
			$row = 0;
			
			echo 	'<div>'.
				$p.' of '.$total_pages.' . '.$total_records[0].' result(s)'.
				$prevlink.'&nbsp;';
		
			for($i = 1; $i <= $total_pages; $i++)
			{
				//echo ($i == $p) ? $i.'&nbsp;' : '<a href="'.$page.'?r='.$row.'&p='.$i.$u.
				//					 '" onfocus="this.blur();">'.$i.'</a>&nbsp;';
				$row = $limit * $i;
			}
		
			echo $nextlink.'</div>';
		}
		else
		{
			//Do Nothing
		}
		
		disconnectDB($connection);
		return $result;		
	}
	
	function render_pager_custom($query, $limit, $country = 'ug', $db_type = 'airtime')
	{
		(!isset($_GET['r'])) ? $r = 0 : $r = $_GET['r'];
		(!isset($_GET['p'])) ? $p = 1 : $p = $_GET['p'];
		
		$page			= $_SERVER['PHP_SELF'];
        $query          = $query." LIMIT $r, $limit";
		$connection     = ($db_type == 'airtime') ? connect2DB2($country) : connect2SMSDB2($country);

		$result         = mysql_query($query);
		$total_records 	= mysql_fetch_row(mysql_query("SELECT FOUND_ROWS()"));
		
		$total_pages	= ceil($total_records[0] / $limit);
		$next			= $r + $limit;
		$prev			= $r - $limit;
		$ntpnum			= $p + 1;
		$prpnum			= $p - 1;
		
		$u 	= isset($_GET['u']) ? '&u='.$_GET['u'] : null ;		
		
		$prevlink = ($p == 1) ?  null : '&nbsp;<a href="'.$page.'?r='.$prev.
		             '&p='.$prpnum.$u.'" onfocus="this.blur();">prev</a>';
					 
		$nextlink = ($p == $total_pages) ?  null : '<a href="'.$page.'?r='.$next.
		             '&p='.$ntpnum.$u.'" onfocus="this.blur();">next</a>';
		
		if(($total_records) && ($total_records[0] > $limit))
		{
			$row = 0;
			
			echo 	'<div>'.$prevlink.'&nbsp;'.
				$p.' of '.$total_pages.' . '.$total_records[0].' result(s)'.
				'&nbsp;';
		
			for($i = 1; $i <= $total_pages; $i++)
			{
				//echo ($i == $p) ? $i.'&nbsp;' : '<a href="'.$page.'?r='.$row.'&p='.$i.$u.
				//					 '" onfocus="this.blur();">'.$i.'</a>&nbsp;';
				$row = $limit * $i;
			}
		
			echo $nextlink.'</div>';
		} 
		else
		{
			//Do Nothing
		}
		
		disconnectDB($connection);
		return $result;		
	}
?>
