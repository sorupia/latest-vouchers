// JavaScript Document
/*******************************************************************************
**  FILE: addDateTime.js
**
**  FUNCTION: date popup functions
**
**  PURPOSE: Functionality to popup date input box
**
**  WRITTEN BY: Anthony Cholmondeley (3nitylabs, Montreal)   DATE: 2007
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Montreal)   DATE: 2008
**               Modified callDate and removed toRow
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.10.18
**               Added callEndDate so as to provide a default end time
**
*********************************************************************************/

//////////////////////////////////////////// CALL POPUP
var openCalendar 	= '';
var hidden1			= '';
var hidden2			= '';
var selectedDate	= '';
var selectedDay		= '';
var count			= 1;

function callDate(id, frm, input, obj, e, edit)
{	
	var months 		= ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	var inputField 	= document.getElementById(input);
	var form		= document.getElementById(frm);
	var div			= document.getElementById('date' + id);
	
	if(openCalendar == id)
	{
		var calendar = document.getElementById('datepopup' + id);
		document.body.removeChild(calendar);
		openCalendar = '';
	} else
	{
		hideCalendar();
		var container 	= document.createElement('div');
		var outerFrame 	= document.createElement('div');
		var spacing 	= document.createElement('div');
		var fromLabel 	= document.createElement('span');
		var fromRow 	= document.createElement('div');
		var fromHours 	= document.createElement('select');
		var fromMinutes	= document.createElement('select');
		var fromAmPm	= document.createElement('select');
		var toLabel 	= document.createElement('span');
		var toRow 		= document.createElement('div');
		var toHours 	= document.createElement('select');
		var toMinutes	= document.createElement('select');
		var toAmPm		= document.createElement('select');
		var insertRow	= document.createElement('div');
		var insert		= document.createElement('input');
		
		outerFrame.className = "outerframe";
		spacing.className 	 = "frame";
		spacing.id			 = "spacing";
		container.id		 = "datepopup" + id;
		container.className  = "datepopup";
		fromRow.className	 = 'row';
		toRow.className	 	 = 'row';
		
		fromLabel.innerHTML = "Time";
		fromLabel.className = 'label';
		toLabel.innerHTML	= "End";
		toLabel.className   = 'label';
		
		////////////////////////////////////////////// MONTH, DAY YEAR
		
		////////////////////////////////////////////// FROM
		var frHR 	= document.createElement('option');
		var frMM 	= document.createElement('option');
		var frAMPM 	= document.createElement('option');
		
		frHR.value 		= '00';
		frHR.innerHTML 	= '00';
		frHR.selected 	= true;
		
		frMM.value 		= '00';
		frMM.innerHTML 	= '00';
		frMM.selected 	= true;
		
		frAMPM.value 	 = 'null';
		frAMPM.innerHTML = 'Am / Pm';
		frAMPM.selected  = true;
		
		fromHours.appendChild(frHR);
		fromMinutes.appendChild(frMM);
		fromAmPm.appendChild(frAMPM);
		
		for(h=1; h<=23; h++)
		{
			var o		= document.createElement('option');
			o.value 	= (h < 10) ? '0' + h : h;
			o.innerHTML = (h < 10) ? '0' + h : h;
			fromHours.appendChild(o);
		}
		
		for(m=1; m<=59; m++)
		{
			var o		= document.createElement('option');
			o.value 	= (m < 10) ? '0' + m : m ;
			o.innerHTML = (m < 10) ? '0' + m : m ;
			fromMinutes.appendChild(o);
		}
		
		var fam 	= document.createElement('option');
		var fpm 	= document.createElement('option');
		
		fam.value = 'am';
		fam.innerHTML = 'am';
		
		fpm.value = 'pm';
		fpm.innerHTML = 'pm';
		
		fromAmPm.appendChild(fam);
		fromAmPm.appendChild(fpm);
		
		////////////////////////////////////////////// TO
		var toHR 	= document.createElement('option');
		var toMM 	= document.createElement('option');
		var toAMPM 	= document.createElement('option');
		
		toHR.value 		= 'null';
		toHR.innerHTML 	= 'hh';
		toHR.selected 	= true;
		
		toMM.value 		= 'null';
		toMM.innerHTML 	= 'mm';
		toMM.selected 	= true;
		
		toAMPM.value 	 = 'null';
		toAMPM.innerHTML = 'Am / Pm';
		toAMPM.selected 	 = true;
		
		toHours.appendChild(toHR);
		toMinutes.appendChild(toMM);
		toAmPm.appendChild(toAMPM);
		
		for(h=0; h<=23; h++)
		{
			var o		= document.createElement('option');
			o.value 	= (h < 10) ? '0' + h : h;
			o.innerHTML = (h < 10) ? '0' + h : h;
			toHours.appendChild(o);
		}
		
		for(m=0; m<=59; m++)
		{
			var o		= document.createElement('option');
			o.value 	= (m < 10) ? '0' + m : m ;
			o.innerHTML = (m < 10) ? '0' + m : m ;
			toMinutes.appendChild(o);
		}
		
		var tam 	= document.createElement('option');
		var tpm 	= document.createElement('option');
		
		tam.value = 'am';
		tam.innerHTML = 'am';
		
		tpm.value = 'pm';
		tpm.innerHTML = 'pm';
		
		toAmPm.appendChild(tam);
		toAmPm.appendChild(tpm);
		
		////////////////////////////////////////////// ADD BUTTON
		insert.type 	 	= 'button'
		insert.value 	 	= 'Add Date';
		insert.className 	= 'submit';
		insert.onfocus = function(){this.blur()};
		insertRow.className = 'insert';
		
		insert.onclick = function(e)
		{
			var errors = '';
			
			if(selectedDate == 'null' || selectedDate == '')
			{
				errors += '- Please select a date\n'
			}
			
			if(fromHours.value == 'null' || fromMinutes.value == 'null' )
			{
				errors += '- Please select a start time\n'
			}
			
			if(errors)
			{
				alert('The following error(s) occured:\n' + errors);
			} 
			else
			{
				inputField.value = selectedDate + ' ' + fromHours.value + ':' + fromMinutes.value;
				selectedDate = '';
				hideCalendar();
			}
		}
		
		////////////////////////////////////////////// APPEND TO DOM
		spacing.appendChild(generateCalendar());
		spacing.appendChild(fromRow);
		fromRow.appendChild(fromLabel);
		fromRow.appendChild(fromHours);
		fromRow.appendChild(fromMinutes);
		//fromRow.appendChild(fromAmPm);
		
		//commenting out the to row
		//spacing.appendChild(toRow);
		toRow.appendChild(toLabel);		
		toRow.appendChild(toHours);
		toRow.appendChild(toMinutes);
		toRow.appendChild(toAmPm);
		
		insertRow.appendChild(insert);
		spacing.appendChild(insertRow);
		outerFrame.appendChild(spacing);
		container.appendChild(outerFrame);
		document.body.appendChild(container);
		
		var offsetX = (getOffsetX(obj)) + obj.offsetWidth;
		var offsetY = (getOffsetY(obj)) - (container.offsetHeight / 2) + 10;
		
		container.onclick = function(e)
		{
			if(!e)
			{
				var e = window.event;
				e.cancelBubble = true;
			} else
			{
				e.cancelBubble = true;
			}
		}
		
		container.style.left 	= offsetX + 'px';
		container.style.top 	= offsetY + 'px';
		
		openCalendar 	= id;
		e.cancelBubble 	= true;
	}
}


//////////////////////////////////////////// callEndDate
function callEndDate(id, frm, input, obj, e, edit)
{	
	var months 		= ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	var inputField 	= document.getElementById(input);
	var form		= document.getElementById(frm);
	var div			= document.getElementById('date' + id);
	
	if(openCalendar == id)
	{
		var calendar = document.getElementById('datepopup' + id);
		document.body.removeChild(calendar);
		openCalendar = '';
	} else
	{
		hideCalendar();
		var container 	= document.createElement('div');
		var outerFrame 	= document.createElement('div');
		var spacing 	= document.createElement('div');
		var fromLabel 	= document.createElement('span');
		var fromRow 	= document.createElement('div');
		var fromHours 	= document.createElement('select');
		var fromMinutes	= document.createElement('select');
		var fromAmPm	= document.createElement('select');
		var toLabel 	= document.createElement('span');
		var toRow 		= document.createElement('div');
		var toHours 	= document.createElement('select');
		var toMinutes	= document.createElement('select');
		var toAmPm		= document.createElement('select');
		var insertRow	= document.createElement('div');
		var insert		= document.createElement('input');
		
		outerFrame.className = "outerframe";
		spacing.className 	 = "frame";
		spacing.id			 = "spacing";
		container.id		 = "datepopup" + id;
		container.className  = "datepopup";
		fromRow.className	 = 'row';
		toRow.className	 	 = 'row';
		
		fromLabel.innerHTML = "Time";
		fromLabel.className = 'label';
		toLabel.innerHTML	= "End";
		toLabel.className   = 'label';
		
		////////////////////////////////////////////// MONTH, DAY YEAR
		
		////////////////////////////////////////////// FROM
		var frHR 	= document.createElement('option');
		var frMM 	= document.createElement('option');
		var frAMPM 	= document.createElement('option');
		
		frHR.value 		= '23';
		frHR.innerHTML 	= '23';
		frHR.selected 	= true;
		
		frMM.value 		= '59';
		frMM.innerHTML 	= '59';
		frMM.selected 	= true;
		
		frAMPM.value 	 = 'null';
		frAMPM.innerHTML = 'Am / Pm';
		frAMPM.selected  = true;
		
		fromHours.appendChild(frHR);
		fromMinutes.appendChild(frMM);
		fromAmPm.appendChild(frAMPM);
		
		for(h=1; h<=23; h++)
		{
			var o		= document.createElement('option');
			o.value 	= (h < 10) ? '0' + h : h;
			o.innerHTML = (h < 10) ? '0' + h : h;
			fromHours.appendChild(o);
		}
		
		for(m=1; m<=59; m++)
		{
			var o		= document.createElement('option');
			o.value 	= (m < 10) ? '0' + m : m ;
			o.innerHTML = (m < 10) ? '0' + m : m ;
			fromMinutes.appendChild(o);
		}
		
		var fam 	= document.createElement('option');
		var fpm 	= document.createElement('option');
		
		fam.value = 'am';
		fam.innerHTML = 'am';
		
		fpm.value = 'pm';
		fpm.innerHTML = 'pm';
		
		fromAmPm.appendChild(fam);
		fromAmPm.appendChild(fpm);
		
		////////////////////////////////////////////// TO
		var toHR 	= document.createElement('option');
		var toMM 	= document.createElement('option');
		var toAMPM 	= document.createElement('option');
		
		toHR.value 		= 'null';
		toHR.innerHTML 	= 'hh';
		toHR.selected 	= true;
		
		toMM.value 		= 'null';
		toMM.innerHTML 	= 'mm';
		toMM.selected 	= true;
		
		toAMPM.value 	 = 'null';
		toAMPM.innerHTML = 'Am / Pm';
		toAMPM.selected 	 = true;
		
		toHours.appendChild(toHR);
		toMinutes.appendChild(toMM);
		toAmPm.appendChild(toAMPM);
		
		for(h=0; h<=23; h++)
		{
			var o		= document.createElement('option');
			o.value 	= (h < 10) ? '0' + h : h;
			o.innerHTML = (h < 10) ? '0' + h : h;
			toHours.appendChild(o);
		}
		
		for(m=0; m<=59; m++)
		{
			var o		= document.createElement('option');
			o.value 	= (m < 10) ? '0' + m : m ;
			o.innerHTML = (m < 10) ? '0' + m : m ;
			toMinutes.appendChild(o);
		}
		
		var tam 	= document.createElement('option');
		var tpm 	= document.createElement('option');
		
		tam.value = 'am';
		tam.innerHTML = 'am';
		
		tpm.value = 'pm';
		tpm.innerHTML = 'pm';
		
		toAmPm.appendChild(tam);
		toAmPm.appendChild(tpm);
		
		////////////////////////////////////////////// ADD BUTTON
		insert.type 	 	= 'button'
		insert.value 	 	= 'Add Date';
		insert.className 	= 'submit';
		insert.onfocus = function(){this.blur()};
		insertRow.className = 'insert';
		
		insert.onclick = function(e)
		{
			var errors = '';
			
			if(selectedDate == 'null' || selectedDate == '')
			{
				errors += '- Please select a date\n'
			}
			
			if(fromHours.value == 'null' || fromMinutes.value == 'null' )
			{
				errors += '- Please select a start time\n'
			}
			
			if(errors)
			{
				alert('The following error(s) occured:\n' + errors);
			} 
			else
			{
				inputField.value = selectedDate + ' ' + fromHours.value + ':' + fromMinutes.value;
				selectedDate = '';
				hideCalendar();
			}
		}
		
		////////////////////////////////////////////// APPEND TO DOM
		spacing.appendChild(generateCalendar());
		spacing.appendChild(fromRow);
		fromRow.appendChild(fromLabel);
		fromRow.appendChild(fromHours);
		fromRow.appendChild(fromMinutes);
		//fromRow.appendChild(fromAmPm);
		
		//commenting out the to row
		//spacing.appendChild(toRow);
		toRow.appendChild(toLabel);		
		toRow.appendChild(toHours);
		toRow.appendChild(toMinutes);
		toRow.appendChild(toAmPm);
		
		insertRow.appendChild(insert);
		spacing.appendChild(insertRow);
		outerFrame.appendChild(spacing);
		container.appendChild(outerFrame);
		document.body.appendChild(container);
		
		var offsetX = (getOffsetX(obj)) + obj.offsetWidth;
		var offsetY = (getOffsetY(obj)) - (container.offsetHeight / 2) + 10;
		
		container.onclick = function(e)
		{
			if(!e)
			{
				var e = window.event;
				e.cancelBubble = true;
			} else
			{
				e.cancelBubble = true;
			}
		}
		
		container.style.left 	= offsetX + 'px';
		container.style.top 	= offsetY + 'px';
		
		openCalendar 	= id;
		e.cancelBubble 	= true;
	}
}

//////////////////////////////////////////// GENERATE CALENDAR
function generateCalendar(month, year, div)
{
	selectedDate 	= '';
	selectedDay 	= '';
	
	var parent		= document.getElementById('spacing');
	var currDate	= new Date();
	var months 		= ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	var days 		= ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];
	var totalDays	= [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	
	var month		= (isNaN(month)) ? currDate.getMonth() : month ;
	var year		= (isNaN(year)) ? currDate.getFullYear() : year ;
	
	if(div)
	{
		var placeholder = document.getElementById(div);
	} else
	{
		var placeholder	= document.createElement('div');
		placeholder.id	= 'calendarBox';
	}

	var container	= document.createElement('div');
	var mthControls = document.createElement('div');
	var mthText		= document.createElement('div');
	var clear01		= document.createElement('div');
	var clear02		= document.createElement('div');
	var left		= document.createElement('div');
	var right		= document.createElement('div');
	var first_day	= new Date(year, month, 1);
	var start_day	= first_day.getDay();
	var monthTotal 	= totalDays[month];
	
	if (month == 1) { // February only!
	  if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0){
		monthTotal = 29;
	  }
	}
	
	container.id  	= 'calendar';
	mthControls.id	= 'mthControls';
	clear01.className = 'clear';
	clear02.className = 'clear';
	
	left.className	= 'leftArrow';
	left.onfocus	= function(){this.blur();};
	left.onclick	= function()
	{
		placeholder.removeChild(container);
		
		if(month == 0)
		{
			month	= 12; 
			year	= year - 1;
		}

		generateCalendar(month - 1, year, 'calendarBox');
	}

	right.className	= 'rightArrow';
	right.onfocus	= function(){this.blur();};
	right.onclick	= function()
	{
		placeholder.removeChild(container);

		if(month == 11)
		{
			month	= -1; 
			year	= year + 1;
		}

		generateCalendar(month + 1, year, 'calendarBox');
	}

	var label = document.createTextNode(months[month] + ' ' + year);
	mthText.className = 'month';
	
	mthText.appendChild(label);
	mthControls.appendChild(left);
	mthControls.appendChild(mthText);
	mthControls.appendChild(right);
	mthControls.appendChild(clear02);
	container.appendChild(mthControls);
	
	//days of the week
	for(var d=0; d<days.length; d++)
	{
		var day 		= document.createElement('div');
		day.className 	= 'days';
		day.innerHTML 	= '<span>' + days[d] + '</span>';
		container.appendChild(day);
	}
	
	//days
	var dd = 1;
	
	// this loop is for is weeks (rows)
	for (var i = 0; i < 6; i++) {
		
		// this loop is for weekdays (cells)
		for (var j = 0; j <= 6; j++) {
			
			var cell = document.createElement('div');
			
			if (dd <= monthTotal && (i > 0 || j >= start_day))
			{
				cell.day			= dd;
				cell.id				= 'cell' + dd;
				cell.innerHTML 		= '<span>' + dd + '</span>';
				cell.className 		= 'cell';
				
				cell.onclick = function(e)
				{
					this.day = (this.day < 10) ? '0' + this.day : this.day ;
					//date 	 = year + '-' + months[month] + '-' + this.day;
					var enum_month = month + 1;

					if(enum_month < 10)	date = year + '-0' + enum_month + '-' + this.day;
					else date = year + '-' + enum_month + '-' + this.day;

					ddiv 	 = document.getElementById(this.id);
					selected = (selectedDay) ? document.getElementById(selectedDay) : null ;
					
					if(this.id != selectedDay)
					{
						ddiv.style.backgroundColor = '#A82428';
						ddiv.onmouseover = function(){this.style.backgroundColor = '#A82428';};
						ddiv.onmouseout = function(){this.style.backgroundColor = '#A82428';};
						
						if(selectedDay)
						{
							selected.style.backgroundColor = '#FFF';	
							selected.onmouseover = function(){this.style.backgroundColor = '#ceff9c';};
							selected.onmouseout	 = function(){this.style.backgroundColor = '#FFF';};
						}						
					} else
					{
						//do nothing
					}
					
					selectedDay	 = this.id;
					selectedDate = date;
				}

				cell.onmouseover	= function(){this.style.backgroundColor = '#ceff9c';};
				cell.onmouseout		= function(){this.style.backgroundColor = '#FFF';};
				
				container.appendChild(cell);
				dd++;
			} else
			{
				cell.innerHTML 	= '<span>&nbsp;</span>';
				cell.className 	= 'cellEmpty';
				container.appendChild(cell);
			}
		}
		
		// stop making rows if we've run out of days
		if (dd == 42) {
			break;
		}
	}
	
	container.appendChild(clear01);
	placeholder.appendChild(container);

	/*if(!div)
	{
		parent.appendChild(placeholder);
	}*/
	
	return placeholder;
}
//////////////////////////////////////////// CLOSE CALENDAR
function hideCalendar()
{	
	var calendar = document.getElementById('datepopup' + openCalendar);
	selectedDate = '';
	selectedDay  = '';
	
	if(calendar)
	{
		document.body.removeChild(calendar);
		openCalendar = '';
	}
}
//////////////////////////////////////////// ADD DATE
function addDate(holder)
{
	count++;
	var div 		= document.getElementById(holder);
	var container 	= document.createElement('div');
	var input 		= document.createElement('input');
	var addTag		= document.createElement('a');
	var add			= document.createElement('img');
	var delTag		= document.createElement('a');
	var del			= document.createElement('img');
	var calTag		= document.createElement('a');
	var cal			= document.createElement('img');
	
	container.id 	= 'date' + count;
	addTag.href 	= "#";
	addTag.onclick 	= function(){addDate('bin');};
	addTag.onfocus	= function(){this.blur();};
	addTag.title	= 'Add Date';
	
	add.src 		= 'images/icons/add.gif';
	add.alt 		= 'Add Date';
	add.border 		= 0;
	add.className 	= 'datefieldicon';
	addTag.appendChild(add);
	
	delTag.href 	= "#";
	delTag.onclick 	= function(){div.removeChild(dateBox);};
	delTag.onfocus	= function(){this.blur();};
	delTag.title	= 'Remove Date';
	
	del.src 		= 'images/icons/delete.gif';
	del.alt 		= 'Delete Date';
	del.border 		= '0';
	del.className 	= 'datefieldicon';
	delTag.appendChild(del);
	
	input.type 		= 'text';
	input.name		= 'd' + count;
	input.id		= 'd' + count;
	input.onfocus	= function(){this.blur();};
	input.className	= 'datefield';
	
	calTag.href 	= "#";
	
	calTag.onclick 	= function(e){
		
		if(!e)
		{
			var e = window.event;
		}
		
		callDate(count, 'addCourse', 'd' + count, this, e);
	}
	
	calTag.onfocus	= function(){this.blur();};
	calTag.title	= 'Add Date';
	
	cal.src			= 'images/icons/calendar.gif';
	cal.alt 		= 'Add Date';
	cal.border 		= '0';
	cal.className	= 'datefieldicon';
	calTag.appendChild(cal);
	
	container.appendChild(input);
	container.appendChild(calTag);
	container.appendChild(addTag);
	container.appendChild(delTag);
	div.appendChild(container);
	
	var dateBox		= document.getElementById('date' + count);
}

document.onclick = hideCalendar;