<?
    session_start();
    include_once "functions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/SecurityFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/SessionFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/NewCardsTable.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/UsedCardsTable.php";
    include_once '../src/insertATArray.php';
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/Country.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/Employee.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/IncompletePaymentsQueue.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/NewClientsQueueTable.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/OutgoingQueueTable.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/BlockNewClient.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/ProcessOrderQueue.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/ProcessBlockedClientsQ.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/DeleteBlockedClientsQ.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Security/get_location_by_ip.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/AllCardsTable.php";

    $countryAbbreviation = COUNTRY_ABBREVIATION;

if(isset($_POST['type']))
{
    switch($_POST['type'])
    {
        /*[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ CLIENTS ]]*/
        case "editUser":
            $smsUserID       = $_POST['smsUserID'];
            $emailAddress    = $_POST['emailAddress'];
            $firstName       = $_POST['firstName'];
            $lastName        = $_POST['lastName'];
            $cellphone       = $_POST['cellphone'];
            $addressCity     = $_POST['addressCity'];
            $addressState    = $_POST['addressState'];
            $addressCountry  = $_POST['addressCountry'];
            $airtimeStatus   = $_POST['airtimeStatus'];
            $subscriberStatus= $_POST['subscriberStatus'];
            $creditsLeft     = SMS_MAX_CREDITS; //$_POST['creditsLeft'];
            $comments        = $_POST['comments'];
            $originalSubscriberStatus = $_POST['originalSubscriberStatus'];
            $originalAirtimeStatus = $_POST['originalAirtimeStatus'];

            $connection = connect2SMSDB2($countryAbbreviation);
            $query      = "UPDATE SMSUsers SET firstName = '$firstName', lastName = '$lastName', cellphone = '$cellphone', ";
            $query     .= "addressCity = '$addressCity', addressState = '$addressState', addressCountry = '$addressCountry', ";
            $query     .= "airtimeStatus = '$airtimeStatus', subscriberStatus = '$subscriberStatus', creditsLeft = $creditsLeft ";
            $query     .= "WHERE smsUserID = '$smsUserID'";
            mysql_query($query);
            disconnectDB($connection);
            
            if( $originalAirtimeStatus != 'VERIFIED' && $originalSubscriberStatus != 'VERIFIED' &&
                $airtimeStatus == 'VERIFIED' && $subscriberStatus == 'VERIFIED' )
            {
                if(substr($comments,0,14) == "MAX_TX_REACHED")
                {
                    $body="Hi $firstName, \n";
                    $body.="Your Airtime & SMS account at ".DOMAIN_PREFIX." has been reactivated \n";
                    $body.="You can send airtime and free SMS anytime to ".COUNTRY_NAME.". \n";
                    $body.="--Thanks, ".SERVICED_BY." [".SUPPORT_EMAIL."]";
                    $subject="Account reactivated";
                    sendEmail('','',$emailAddress,SIGNUP_EMAIL,$subject,$body);
                }
                else
                {
                    //echo "email Airtime & SMS registration confirmation to $emailAddress";
                    $body="Hi $firstName, \n";
                    $body.="The verification of your Airtime & SMS account at ".DOMAIN_PREFIX." is complete \n";
                    $body.="Now you have access to send airtime and free SMS anytime to ".COUNTRY_NAME.". \n";
                    $body.="--Thanks, ".SERVICED_BY." [".SUPPORT_EMAIL."]";
                    $subject="Account verification complete";
                    sendEmail('','',$emailAddress,SIGNUP_EMAIL,$subject,$body);
                }
            }
            else
            {
                if( $originalSubscriberStatus != 'VERIFIED' && $subscriberStatus == 'VERIFIED' ){
                    //echo "email SMS registration confirmation to $emailAddress";
                    $body="Hi $firstName, \n";
                    $body.="The verification of your SMS account at ".DOMAIN_PREFIX." is complete \n";
                    $body.="Now you have access to send free SMS anytime to ".COUNTRY_NAME.". \n";
                    $body.="--Thanks, ".SERVICED_BY." [".SUPPORT_EMAIL."]";
                    $subject="SMS account verification complete";
                    sendEmail('','',$emailAddress,SIGNUP_EMAIL,$subject,$body);
                }
        
                if($originalAirtimeStatus != 'VERIFIED' && $airtimeStatus == 'VERIFIED'){
                    //echo "email Airtime registration confirmation";
                    $body="Hi $firstName, \n";
                    $body.="The verification of your airtime account at ".DOMAIN_PREFIX." is complete \n";
                    $body.="Now you have access to send airtime anytime to ".COUNTRY_NAME.". \n";
                    $body.="--Thanks, ".SERVICED_BY." [".SUPPORT_EMAIL."]";
                    $subject="Airtime account verification complete";
                    sendEmail('','',$emailAddress,SIGNUP_EMAIL,$subject,$body);
                }
            }
            
            header("Location: ../clients.php");
            exit;
            break;
        case "deleteUser":
            $smsUserID       = $_POST['smsUserID'];
            $emailAddress    = $_POST['emailAddress'];
            
            $connection = connect2SMSDB2($countryAbbreviation);
                $query  = "DELETE FROM SMSUsers WHERE smsUserID = '$smsUserID'";
                mysql_query($query);
            disconnectDB($connection);
            
            $_SESSION['message'] = "Client $emailAddress has been deleted";
            header("Location: ../clients.php");
            exit;
        break;
        case "deleteCard":
            $cardID       = $_POST['cardID'];
            $cardPIN      = $_POST['cardPIN'];
            $page         = $_POST['page'];
            $username     = $_POST['employee_username'];

            deleteNewCard($countryAbbreviation,$cardPIN,$username);
            
            $_SESSION['message'] = "CardPIN $cardPIN has been deleted";
            header("Location: ../".$page.".php");
            exit;
            break;
        case "editCard":
            $cardID       = $_POST['cardID'];
            $oldCardPIN   = $_POST['oldCardPIN'];
            $newCardPIN   = $_POST['cardPIN'];
            $itemID       = $_POST['itemID'];
            $serialNumber = $_POST['serialNumber'];
            $page         = $_POST['page'];
            $username     = $_POST['employee_username'];

            $result = modifyNewCardsInfo($countryAbbreviation,
                                         $oldCardPIN,
                                         $newCardPIN,
                                         $itemID,
                                         $serialNumber,
                                         $username);

            if($result)
            {
                $_SESSION['message'] = "CardPIN $newCardPIN has been modified";
            }
            else{
                $_SESSION['message'] = "CardPIN $newCardPIN has not been modified";
            }
            header("Location: ../".$page.".php");
            exit;
            break;
        case "completeUsedCard":
            $cardID       = $_POST['cardID'];
            $cardPIN      = $_POST['cardPIN'];
            $page         = $_POST['page'];
            $itemID       = $_POST['itemID'];
            $serialNumber = $_POST['serialNumber'];
            $receiverPhone = $_POST['receiverPhone'];
            $receiverFirstName = $_POST['receiverFirstName'];
            $receiverEmail = $_POST['receiverEmail'];
            $firstName   = $_POST['firstName'];
            $valueInUGSh   = $_POST['valueInUGSh'];
            $clientEmail   = $_POST['clientEmail'];
            $transactionID = $_POST['transactionID'];
            $clientFirstName = $_POST['clientFirstName'];   

            //SMS the cardPIN to receiverPhone
            //$receiverPhone= "+15149631699";
            $smsGWID = sendEmail2Phone($receiverPhone,$receiverFirstName,$firstName,$cardPIN,$valueInUGSh,$itemID);

            //Email the Client a confirmation
            sendEmail2Client($clientEmail,$receiverPhone,$firstName,$cardPIN,$itemID,$transactionID);

            //Email the Receiver a confirmation
            sendEmail2Receiver($receiverEmail,$receiverFirstName,$firstName,$cardPIN,$itemID,$transactionID);

            //Update the UsedCards Table
            updateUsedCard($countryAbbreviation,
                           $cardID,
                           $receiverPhone,
                           $receiverFirstName,
                           $receiverEmail,
                           $smsGWID);

            $_SESSION['message'] = "Used card $cardPIN has been SMSed";
            header("Location: ../".$page.".php");
            exit;

            break;
        case "restockUsedCard":
            $cardID       = $_POST['cardID'];
            $cardPIN      = $_POST['cardPIN'];
            $page         = $_POST['page'];
            $itemID       = $_POST['itemID'];
            $serialNumber = $_POST['serialNumber'];
            $receiverPhone = $_POST['receiverPhone'];
            $receiverFirstName = $_POST['receiverFirstName'];
            $receiverEmail = $_POST['receiverEmail'];
            $firstName   = $_POST['firstName'];
            $valueInUGSh   = $_POST['valueInUGSh'];
            $clientEmail   = $_POST['clientEmail'];
            $transactionID = $_POST['transactionID'];
            $clientFirstName = $_POST['clientFirstName'];
            $username     = $_POST['employee_username'];
            $networkName = $_POST['networkName'];
            $ourPriceInUSD = $_POST['ourPriceInUSD'];
            $dateWePurchased = $_POST['dateWePurchased'];
            
            //Email the restock action to administrator
            $fromName = "Restock Page";
            $subject = "Card Restocked";
            $body    = "Hi, \n";
            $body   .= "The airtime card with ItemID: $itemID & cardPIN: $cardPIN has been restocked \n";
            $body   .= "by $username ";
            sendEmail($fromName,$fromAddress,CARD_ADMIN,$bcc,$subject,$body);
            
            //Delete from UsedCards Table
            deleteUsedCardByCardID($countryAbbreviation, $cardID);
            
            
            $pinNumbers[0][0] = $cardPIN;
            $pinNumbers[0][1] = $serialNumber;
            $myIP = getIP();

            //Insert into NewCards Table
            insertATArray($testing,
                       $countryAbbreviation,
                       $pinNumbers,
                       $error_detected,
                       $error_report,
                       $load_report,
                       $networkName,
                       $valueInUGSh,
                       $ourPriceInUSD,
                       $dateWePurchased,
                       $itemID,
                       $username,
                       $myIP);
            
            $_SESSION['message'] = "Used card $cardPIN has been restocked";
            header("Location: ../".$page.".php");
            exit;
            break;
        case "addCountry":
            $countryToAdd = $_POST['countryAbbreviation'];
            $countryName = $iso_country_codes[$countryToAdd];
            $status = $_POST['countryStatus'];
            $username     = $_POST['employee_username'];
            $page         = $_POST['page'];
            
            $result = addCountryToList($countryAbbreviation,
                                       $countryToAdd,
                                       $countryName,
                                       $status,
                                       $username);
                             
            if($result){
                
                $_SESSION['message'] = "Country: $countryName has been added";
                header("Location: ../".$page.".php");
                exit;
            }
            else{
                //echo"not updated";
                $_SESSION['message'] = "Country: $countryName has not been added";
                header("Location: ../".$page.".php");
                exit;
            }
            break;
        case "editCountry":
            $countryToEdit = $_POST['countryAbbreviation'];
            $status = $_POST['countryStatus'];
            $username     = $_POST['employee_username'];
            $page         = $_POST['page'];
            
            editCountry($countryAbbreviation,
                        $countryToEdit,
                        $status,
                        $username);
                             
            $_SESSION['message'] = "Country: $countryName has been modified to $status";
            header("Location: ../".$page.".php");
            exit;
            break;
        case "editIP":
            $ipToEdit = $_POST['IPAddress'];
            $oldIPAddress = $_POST['oldIPAddress'];
            $ipStatus = $_POST['ipStatus'];
            $id       = $_POST['id'];
            $comments = $_POST['comments'];
            $username     = $_POST['employee_username'];
            $page         = $_POST['page'];
            
            editIP($countryAbbreviation,$_POST);

            $_SESSION['message'] = "IP: $oldIPAddress has been modified";
            header("Location: ../".$page.".php");
            exit;
            break;
        case "addIP":
            $ipToEdit = $_POST['IPAddress'];
            $ipStatus = $_POST['ipStatus'];
            $comments = $_POST['comments'];
            $username     = $_POST['employee_username'];
            $page         = $_POST['page'];

            $result = addIPToList($countryAbbreviation,
                                  $ipToEdit,
                                  $ipStatus,
                                  $comments,
                                  $username);
            
            if($result)
            {
                $_SESSION['message'] = "IP: $ipToEdit has been added as $ipStatus";
                header("Location: ../".$page.".php");
                exit;
            }
            else
            {
                //echo"not updated";
                $_SESSION['message'] = "IP: $ipToEdit has not been added";
                header("Location: ../".$page.".php");
                exit;
            }
            break;
        case "IPLookup":
            $ipToLookup = $_POST['IPAddress'];
            $username     = $_POST['employee_username'];
            $page         = $_POST['page'];

            $ipLocationArray = get_location_by_ip($ipToLookup);

            $_SESSION['message'] = "IP Address: ".$ipLocationArray['ip_address']."<br>";
            $_SESSION['message'].= "IP Country: ".$ipLocationArray['ip_country']."<br>";
            $_SESSION['message'].= "IP State: ".$ipLocationArray['ip_state']."<br>";
            $_SESSION['message'].= "IP City: ".$ipLocationArray['ip_city']."<br>";
            header("Location: ../".$page.".php");
            exit;
            break;
        case "editEmployee":
            $firstName        = $_POST['first_name'];
            $lastName         = $_POST['last_name'];
            $username         = $_POST['username'];
            $password         = $_POST['password'];
            $employeeType     = $_POST['employee_type'];
            $employeeUsername = $_POST['employee_username'];
            
            $page         = $_POST['page'];
            
            $result = editEmployee($countryAbbreviation,
                                   $firstName,
                                   $lastName,
                                   $username,
                                   $password,
                                   $employeeType,
                                   $employeeUsername);

            //echo"not updated";
            $_SESSION['message'] = "Employee: $username has been modified";
            header("Location: ../".$page.".php");
            exit;
            break;
        case "editIncompletePayment":
            $modifications['comments']      = $_POST['comments'];
            $modifications['transactionID'] = $_POST['transactionID'];
            $id           = $_POST['id'];
            $page         = $_POST['page'];
            $username     = $_POST['employee_username'];

            $result = modifyIncompletePayment($countryAbbreviation,
                                              $id,
                                              $modifications);
            
            if($result){
                $_SESSION['message'] = "Incomplete Payment record has been modified";
            }
            else{
                $_SESSION['message'] = "Incomplete Payment record has not been modified";
            }
            header("Location: ../".$page.".php");
            exit;
            break;
        case "sendIncompletePayment":
            
            //provided at the time of transaction
            $incompleteQueryArray['id'] = $_POST['id'];
            $id = $incompleteQueryArray['id'];
            $incompleteQueryArray['clientEmail'] = $_POST['clientEmail'];
            $incompleteQueryArray['receiverPhone'] = $_POST['receiverPhone'];
            $incompleteQueryArray['receiverFirstName'] = $_POST['receiverFirstName'];
            $incompleteQueryArray['receiverEmail'] = $_POST['receiverEmail'];
            $incompleteQueryArray['clientIP'] = $_POST['clientIP'];
        
            //provided by individual processing this 
            $incompleteQueryArray['transID'] = $_POST['transactionID'];
            $incompleteQueryArray['itemIDNumber'] = $_POST['itemIDNumber'];
            $incompleteQueryArray['clientFirstName'] = $_POST['clientFirstName'];
            $incompleteQueryArray['comments'] = $_POST['comments'];
            $incompleteQueryArray['employeeUsername'] = $_POST['employeeUsername'];
            $incompleteQueryArray['employeePassword'] = $_POST['employeePassword'];

            $page         = $_POST['page'];
            $username     = $_POST['employeeUsername'];
            $password     = $_POST['employeePassword'];

            $resultArray = 
            processIncompletePaymentsQ($countryAbbreviation,
                                       $incompleteQueryArray);
            
            if($resultArray['result']){
                $_SESSION['message'] = "Incomplete Payment $id has been dispatched with ".$resultArray['card'];
            }
            else{
                $_SESSION['message'] = "Incomplete Payment $id has not been modified, error: ".$resultArray['message'];
            }
            header("Location: ../".$page.".php");
            exit;
            break;          
        case "completeIncompletePayment":
            $modifications['comments']      = $_POST['comments'];
            $modifications['transactionID'] = $_POST['transactionID'];
            $id           = $_POST['id'];
            $page         = $_POST['page'];
            $username     = $_POST['employee_username'];

            $result = modifyIncompletePayment($countryAbbreviation,
                                              $id,
                                              $modifications);
                
            if($result){
                $_SESSION['message'] = "Incomplete Payment record has been modified";
            }
            else{
                $_SESSION['message'] = "Incomplete Payment record has not been modified";
            }
            header("Location: ../".$page.".php");
            exit;
            break;
        case "verifyNewClientTransaction":
            $page = $_POST['page'];
            $username = $_POST['employeeUsername'];
            $clientEmail = $_POST['clientEmail'];
            $userStatus = "VERIFIED";
            $receiverPhone = $_POST['receiverPhone']; 

            if($clientEmail != ""){
                $resultArray = processNewClientsQ($countryAbbreviation,
                                                  $_POST,
                                                  $userStatus,
                                                  $username);
                
                if($resultArray['result']){
                    $_SESSION['message'] = "New user $clientEmail has been approved ";
                    $_SESSION['message'].= "and ".PRODUCT." to $receiverPhone dispatched with ".$resultArray['card'];
                }
                else{
                    $_SESSION['message'] = "New user $clientEmail has not been approved, ";
                    $_SESSION['message'].= "error: ".$resultArray['message'];
                }
                header("Location: ../".$page.".php");
                exit;
            }
            else{
                //echo"not updated";
                $_SESSION['message'] = "New user $clientEmail has not been deleted";
                header("Location: ../".$page.".php");
                exit;
            }
            break;
        case "verifyOldClientTransaction":
            $page = $_POST['page'];
            $username = $_POST['employeeUsername'];
            $clientEmail = $_POST['clientEmail'];
            $receiverPhone = $_POST['receiverPhone'];
            $userStatus = "VERIFIED";

            $resultArray = processOutgoingQ($countryAbbreviation,
                                            $_POST,
                                            $userStatus,
                                            $username);
            
            if($resultArray['result']){
                $_SESSION['message'] = "Old user $clientEmail transaction verified ";
                $_SESSION['message'].= "and airtime to $receiverPhone dispatched with ".$resultArray['card'];
                $_SESSION['message'].= "<br><br>".$resultArray['message'];
            }
            else{
                $_SESSION['message'] = "Old user $clientEmail transaction has not ";
                $_SESSION['message'].= "been approved, error: ".$resultArray['message'];
            }
            header("Location: ../".$page.".php");
            exit;
            break;
        case "verifyOrder":
            $page          = $_POST['page'];
            $username      = $_POST['employeeUsername'];
            $clientEmail   = $_POST['clientEmail'];
            $receiverPhone = $_POST['receiverPhone'];
            $order_id      = $_POST['order_id'];
            $userStatus    = "VERIFIED";

            $resultArray = process_order_queue($countryAbbreviation,
                                               $_POST,
                                               $userStatus,
                                               $username);
            
            if($resultArray['result']){
                $_SESSION['message'] = "$clientEmail order verified ";
                $_SESSION['message'].= "and airtime to $receiverPhone dispatched with ".$resultArray['card'];
            }
            else{
                $_SESSION['message'] = "$clientEmail order has not ";
                $_SESSION['message'].= "been approved, error: ".$resultArray['message'];
            }
            header("Location: ../".$page.".php");
            exit;
            break;
        case "blockNewClientTransaction":
            $page = $_POST['page'];
            $username = $_POST['employeeUsername'];
            $clientEmail = $_POST['clientEmail'];
            $receiverPhone = $_POST['receiverPhone'];
            $_POST['userStatus'] = "BLOCKED";

            if($clientEmail != "")
            {
                $resultArray = blockNewClient($_POST);

                if($resultArray['result'])
                {
                    $_SESSION['message'] = $resultArray['message'];
                }
                else
                {
                    $_SESSION['message'] = "New user $clientEmail has not been blocked, ";
                    $_SESSION['message'].= "error: ".$resultArray['message'];
                }
                header("Location: ../".$page.".php");
                exit;
            }
            else
            {
                $_SESSION['message'] = "No client email provided";
                header("Location: ../".$page.".php");
                exit;
            }
            
            break;
        case "addEmployee":
            $firstName        = $_POST['first_name'];
            $lastName         = $_POST['last_name'];
            $username         = $_POST['username'];
            $password         = $_POST['password'];
            $employeeType     = $_POST['employee_type'];
            $employeeUsername = $_POST['employee_username'];
            
            $page         = $_POST['page'];
            
            $result = addEmployee($countryAbbreviation,
                                  $firstName,
                                  $lastName,
                                  $username,
                                  $password,
                                  $employeeType,
                                  $employeeUsername);

            $_SESSION['message'] = "Employee: $username has been added";
            header("Location: ../".$page.".php");
            exit;
            break;
        /*[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ LOGIN ]]*/  
        case "login":
            
            $username   = $_POST['username'];
            $password   = $_POST['password'];
            $redirect   = ($_POST['redirect'] =="") ? "dashboard.php": $_POST['redirect'];

            //$mysql_conn   = db_connect();
            $connection = connect2DB2($countryAbbreviation);
            $result  = mysql_query("SELECT username, type FROM Employees WHERE username = '$username' AND password = PASSWORD('$password')");
            $success = mysql_num_rows($result);

            $last_login_ip = getIP();
            $last_login_time = date("Y-m-d H:i:s");

            if($success)
            {
                $updateQuery = "UPDATE Employees ";
                $updateQuery.= "SET last_login_time = '$last_login_time', last_login_ip = '$last_login_ip' ";
                $updateQuery.= "WHERE username = '$username'";
                mysql_query($updateQuery);

                session_start();
                $data            = mysql_fetch_array($result);
                $_SESSION['uid'] = $data['username'];
                $access_level = $data['type'];
                $_SESSION['user_access_level'] = $access_level_map[$access_level];
                $_SESSION['ip_address'] = $last_login_ip;

                //keep track of who got in
                $insertQuery = "INSERT INTO ";
                $insertQuery.= "EmployeeLoginLogs(username, login_ip, login_time) ";
                $insertQuery.= "VALUES('$username','$last_login_ip','$last_login_time') ";
                mysql_query($insertQuery);

                disconnectDB($connection);
                header("Location: ../".$redirect);
                exit;
            } else
            {
                disconnectDB($connection);
                header("Location: ../index.php");
                exit;
            }
            
            break;
        /*[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[ Receivers ]]*/  
        case "editReceiver":
            $receiverID             = $_POST['receiverID'];
            $lastContacted          = $_POST['lastContacted'];
            $comments               = $_POST['comments'];
            $receiverPhone          = $_POST['receiverPhone'];
            $receiverStatus         = $_POST['receiverStatus'];
            $originalReceiverStatus = $_POST['originalReceiverStatus'];
            $page                   = $_POST['page'];

            $connection = connect2DB2($countryAbbreviation);
                $query      = "UPDATE Receivers ";
                $query     .= "SET ";
                $query     .= "lastContacted = '$lastContacted', ";

                if($receiverStatus != $originalReceiverStatus)
                {
                    $query .= "receiverStatus = '$receiverStatus', ";
                }

                $query     .= "comments = '$comments' ";
                $query     .= "WHERE receiverID = '$receiverID'";
                $result     = mysql_query($query);
            disconnectDB($connection);
            
            $_SESSION['message'] = "Receiver $receiverPhone updated";
            header("Location: ../".$page.".php");
            exit;
            break;
        case "editNewReceiver":
            $receiverID      = $_POST['receiverID'];
            $lastContacted   = $_POST['lastContacted'];
            $comments        = $_POST['comments'];
            $receiverPhone   = $_POST['receiverPhone'];
            $page            = $_POST['page'];

            $connection = connect2DB2($countryAbbreviation);
                $query      = "UPDATE NewReceivers ";
                $query     .= "SET ";
                $query     .= "lastContacted = '$lastContacted', ";
                $query     .= "comments = '$comments' ";
                $query     .= "WHERE receiverID = '$receiverID'";
                $result     = mysql_query($query);
            disconnectDB($connection);
            
            $_SESSION['message'] = "Receiver $receiverPhone updated";
            header("Location: ../".$page.".php");
            exit;
            break;
        case "sendBlockedPurchase":
            //provided at the time of transaction
            $page         = $_POST['page'];
            $username     = $_POST['employeeUsername'];
            $password     = $_POST['employeePassword'];

            $resultArray = processBlockedClientsQ($countryAbbreviation,$_POST);
            
            if($resultArray['result']){
                $_SESSION['message'] = "Blocked Client ".$_POST['clientEmail']." purchase has been dispatched with ".$resultArray['card'];
            }
            else{
                $_SESSION['message'] = "Blocked Client ".$_POST['clientEmail']." purchase has not been modified, error: ".$resultArray['message'];
            }
            header("Location: ../".$page.".php");
            exit;
            break;
    }
}

if(isset($_GET['type']))
{
    switch($_GET['type'])
    {
        case "delete":
            $redirect   = $_GET['redirect'];
            $hasDir     = $_GET['dir'];
            $tid        = $_GET['tid'];
            $id         = $_GET['id'];
            $table      = $_GET['table'];
            $page       = $_GET['page'];
            
            ($hasDir) ? rm_dir("html/".$table."/".$id."/") : null;
            
            $connection = connect2DB2($countryAbbreviation);
                $query  = "DELETE FROM $table WHERE $tid = '$id'";
                $result = mysql_query($query);
                $_SESSION['message'] = "Table ID $id has been deleted from $table";
                ($redirect) ? header("Location: ../".$redirect) : header("Location: ../".$page.".php");
            disconnectDB($connection);
            exit;
            break;
            
        case "logout":
            
            session_unset();
            session_destroy();
            header("Location: ../index.php");
            exit;
            break;
    }
}
    
?>
