<?
    session_start();
    include_once "functions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/SecurityFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/SessionFunctions.php";

    $countryAbbreviation = COUNTRY_ABBREVIATION;

    if($_POST['type'] == "resendUsedCard")
    {
        $cardID       = $_POST['cardID'];
        $cardPIN      = $_POST['cardPIN'];
        $page         = $_POST['page'];
        $itemID       = $_POST['itemID'];
        $serialNumber = $_POST['serialNumber'];
        $receiverPhone = $_POST['receiverPhone'];
        $receiverFirstName = $_POST['receiverFirstName'];
        $receiverEmail = $_POST['receiverEmail'];
        $firstName   = $_POST['firstName'];
        $valueInUGSh   = $_POST['valueInUGSh'];
        $clientEmail   = $_POST['clientEmail'];
        $transactionID = $_POST['transactionID'];
        $clientFirstName = $_POST['clientFirstName'];   

        //SMS the cardPIN to receiverPhone
        $smsGWID = sendEmail2Phone($receiverPhone,$receiverFirstName,$firstName,$cardPIN,$valueInUGSh,$itemID);

        //Email the Client a confirmation
        sendEmail2Client($clientEmail,$receiverPhone,$firstName,$cardPIN,$itemID,$transactionID);

        //Email the Receiver a confirmation
        sendEmail2Receiver($receiverEmail,$receiverFirstName,$firstName,$cardPIN,$itemID,$transactionID);

        $_SESSION['message'] = "Used card $cardPIN has been resent to $receiverPhone and confirmation is $smsGWID";
        header("Location: ../".$page.".php");
    }
    else
    {
        header("Location: ../dashboard.php");
    }
?>