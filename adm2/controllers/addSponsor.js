// JavaScript Document
//////////////////////////////////////////// ADD SPONSOR
var sponsorField = 1;

function addS(bin)
{
	var div		 	= document.getElementById(bin);
	sponsorField++;
	
	if(sponsorField < 16)
	{
		var holder		= document.createElement('div');
		var file		= document.createElement('input');
		var addTag		= document.createElement('a');
		var add			= document.createElement('img');
		var delTag		= document.createElement('a');
		var del			= document.createElement('img');
		var calTag		= document.createElement('a');
		var cal			= document.createElement('img');
		
		holder.id		= 'sponsor' + sponsorField;
		addTag.href 	= "#";
		addTag.onclick 	= function(){addS('sponsorbin');};
		addTag.onfocus	= function(){this.blur();};
		addTag.title	= 'Add Sponsor';
		
		add.src 		= 'images/icons/add.gif';
		add.alt 		= 'Add Sponsor';
		add.border 		= 0;
		add.className 	= 'datefieldicon';
		addTag.appendChild(add);
		
		delTag.href 	= "#";
		delTag.onclick 	= function(){div.removeChild(document.getElementById(holder.id)); sponsorField = sponsorField - 1;};
		delTag.onfocus	= function(){this.blur();};
		delTag.title	= 'Remove Sponsor';
		
		del.src 		= 'images/icons/delete.gif';
		del.alt 		= 'Remove Sponsor';
		del.border 		= '0';
		del.className 	= 'datefieldicon';
		delTag.appendChild(del);
		
		file.className			= 'upload';
		file.type				= 'file';
		file.name				= 'sponsor' + sponsorField;
		file.id					= 'sponsor' + sponsorField;
		
		holder.appendChild(file);
		holder.appendChild(addTag);
		holder.appendChild(delTag);
		div.appendChild(holder);
	} else
	{
		alert("You have reached the maximum amount of sponsors");
	}
}