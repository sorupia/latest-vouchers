function generateCalendar(month, year, div)
{
	var currDate	= new Date();
	var months 		= ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	var days 		= ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];
	var totalDays	= [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	
	var month		= (isNaN(month)) ? currDate.getMonth() : month ;
	var year		= (isNaN(year)) ? currDate.getFullYear() : year ;
	
	if(div)
	{
		var placeholder = document.getElementById(div);
	} else
	{
		var placeholder	= document.createElement('div');
		placeholder.id	= 'calendarBox';
	}

	var container	= document.createElement('div');
	var mthControls = document.createElement('div');
	var mthText		= document.createElement('div');
	var clear01		= document.createElement('div');
	var clear02		= document.createElement('div');
	var left		= document.createElement('div');
	var right		= document.createElement('div');
	var first_day	= new Date(year, month, 1);
	var start_day	= first_day.getDay();
	var monthTotal 	= totalDays[month];
	
	if (month == 1) { // February only!
	  if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0){
		monthTotal = 29;
	  }
	}
	
	container.id  	= 'calendar';
	mthControls.id	= 'mthControls';
	clear01.className = 'clear';
	clear02.className = 'clear';
	
	left.className	= 'leftArrow';
	left.onfocus	= function(){this.blur();};
	left.onclick	= function()
	{
		placeholder.removeChild(container);
		
		if(month == 0)
		{
			month	= 12; 
			year	= year - 1;
		}

		generateCalendar(month - 1, year, 'calendarBox');
	}

	right.className	= 'rightArrow';
	right.onfocus	= function(){this.blur();};
	right.onclick	= function()
	{
		placeholder.removeChild(container);

		if(month == 11)
		{
			month	= -1; 
			year	= year + 1;
		}

		generateCalendar(month + 1, year, 'calendarBox');
	}

	var label = document.createTextNode(months[month] + ' ' + year);
	mthText.className = 'month';
	
	mthText.appendChild(label);
	mthControls.appendChild(left);
	mthControls.appendChild(mthText);
	mthControls.appendChild(right);
	mthControls.appendChild(clear02);
	container.appendChild(mthControls);
	
	//days of the week
	for(var d=0; d<days.length; d++)
	{
		var day 		= document.createElement('div');
		day.className 	= 'days';
		day.innerHTML 	= '<span>' + days[d] + '</span>';
		container.appendChild(day);
	}
	
	//days
	var dd = 1;
	
	// this loop is for is weeks (rows)
	for (var i = 0; i < 6; i++) {
		
		// this loop is for weekdays (cells)
		for (var j = 0; j <= 6; j++) {
			
			var cell = document.createElement('div');
			
			if (dd <= monthTotal && (i > 0 || j >= start_day)) {
				cell.innerHTML 		= '<span>' + dd + '</span>';
				cell.className 		= 'cell';
				cell.onmouseover	= function(){this.style.backgroundColor = '#ceff9c';};
				cell.onmouseout	= function(){this.style.backgroundColor = '#FFF';};
				container.appendChild(cell);
				dd++;
			} else
			{
				cell.innerHTML 	= '<span>&nbsp;</span>';
				cell.className 	= 'cellEmpty';
				container.appendChild(cell);
			}
		}
		
		// stop making rows if we've run out of days
		if (dd == 42) {
			break;
		}
	}
	
	container.appendChild(clear01);
	placeholder.appendChild(container);

	if(!div)
	{
		document.body.appendChild(placeholder);
	}
}