// JavaScript Document
//////////////////////////////////////////// ADD COURSE
function addCourse()
{
	var frm		= document.getElementById('addCourse');
	var errors 	= '';
	alert(frm);
	if(frm.name.value == '')
	{
		errors += '- Please enter a Course name\n';
	}
	
	if(frm.d1.value == '')
	{
		errors += '- Please enter a Date and Time\n';
	}
	
	if(errors)
	{
		alert('The following error(s) occured:\n'+errors);
		return false;
	} else
	{
		frm.submit();	
	}
	
}
//////////////////////////////////////////// SEARCH
function submitSearch(form)
{
	var frm		= document.getElementById(form);
	var errors 	= '';
	
	if(frm.searchType.value == 'null')
	{
		errors += '- Please enter a search type\n';
	}
	
	if(frm.keywords.value == '')
	{
		errors += '- Please enter a keyword\n';
	}
	
	if(errors)
	{
		alert('The following error(s) occured:\n'+errors);
		return false;
	} else
	{
		frm.submit();	
	}
	
}
//////////////////////////////////////////// CONFIRM PASSWORD
function confirmPassword()
{
	var frm		= document.getElementById('settings');
	var errors 	= '';
	
	if(frm.npassword.value)
	{
		if(frm.npassword.value !== frm.rpassword.value)
		{
			errors += '- New Password and Repeat New Password fields do not match.\n';
		}
	}
	
	if(errors)
	{
		alert('The following error(s) occured:\n'+errors);
		return false;
	} else
	{
		frm.submit();	
	}
	
}
//////////////////////////////////////////// OPEN PAGE
function openPage(page)
{
	location.href = page;
}
//////////////////////////////////////////// JUMP MENU
function jump(form){
	var index = form.view.selectedIndex;
	
	if(index == 0){
		//meh!
	} else {
		url = form.view.options[index].value;
		window.location.assign(url);	
	}
}
//////////////////////////////////////////// DELETE ENTRY
function delEntry(obj,table,tid,page)
{
	var answer = confirm('Are you sure you would like to delete this entry? ');
	
	if(answer)
	{
		location.href = 'controllers/cases.php?type=delete&page='+page+'&id='+obj+'&table='+table+'&tid='+tid;
	} else
	{
		//Do Nothing	
	}	
}
//////////////////////////////////////////// CHANGE ACCESS
function changeAccess(course,user,frm)
{
	if(frm.duration.value != '0')
	{
		var answer = confirm('Are you sure you would like to activate '+course+' for '+user+'');
		
		if(answer)
		{
			frm.submit();
		} else
		{
			//Do Nothing
			return false;
		}
	} else
	{
		var answer = confirm('Are you sure you would like to de-activate '+course+' for '+user+'');
		
		if(answer)
		{
			frm.submit();
		} else
		{
			//Do Nothing
			return false;
		}
	}
}