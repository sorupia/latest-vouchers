<?
	session_start();
	include "functions.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/src/SecurityFunctions.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/src/SessionFunctions.php";
    require_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/NewCardsTable.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/UsedCardsTable.php";
	require_once('../src/insertATArray.php');
	require_once($_SERVER['DOCUMENT_ROOT']."/src/Objects/Country.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/src/Objects/Employee.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/src/Objects/IncompletePaymentsQueue.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/src/Objects/NewClientsQueueTable.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/src/Objects/awpdcClient.php");
    require_once($_SERVER['DOCUMENT_ROOT']."/src/Objects/OutgoingQueueTable.php");

	switch($_POST['type'])
    {
		case "processAwpdcClient":
			$page = $_POST['page'];
            $username = $_POST['employeeUsername'];
            $clientEmail = $_POST['clientEmail'];
            $userStatus = "VERIFIED";
            $receiverPhone = $_POST['receiverPhone'];
			$countryAbbreviation = $_POST['countryAbbreviation']; 

			$_SESSION['errors']	= false;
			
			if(!$_SESSION['errors']){

				if($_POST['unblockandsend'])
				{
                	$resultArray = processUnblockAndSend($countryAbbreviation,
								                      	 $_POST,
								                      	 $userStatus,
								                      	 $username);
				}
				else
				{
					//$_POST['unblock']
					$resultArray = processUnblock($countryAbbreviation,
								                  $_POST,
								                  $userStatus,
								                  $username);
				}

				if($resultArray['result'] && $_POST['submit'] == "unblockandsend"){
                    $_SESSION['message'] = "New client $clientEmail has been UnBlocked and ";
                    $_SESSION['message'].= "airtime sent to $receiverPhone dispatched with ".$resultArray['card'];
                }
				else if($resultArray['result'] && $_POST['submit'] == "unblock"){
					$_SESSION['message'] = "New client $clientEmail has been UnBlocked ";
				}
                else{
                    $_SESSION['message'] = "New client $clientEmail has not been UnBlocked, ";
                    $_SESSION['message'].= "error: ".$resultArray['message'];
                }

                header("Location: ../".$page.".php");
			}
			else{
				//echo"not updated";
				$_SESSION['message'] = "New user $clientEmail has not been deleted";
				header("Location: ../".$page.".php");
			}
			
			break;
	}
	
?>
