<?
    include "wrapper/header.php";

    $connection = connect2DB2($countryAbbreviation);
    $query      = "SELECT * FROM NewClientsQueue WHERE transID = '$_GET[id]'";
    $result     = mysql_query($query);
    $data       = mysql_fetch_array($result);
    disconnectDB($connection);
?>
<table cellpadding="0" cellspacing="0" class="listing">

<tr>
	<th><h1><a href="javascript:history.go(-1);" class="sub_menu" title="back" onfocus="this.blur();">&laquo; Back</a> Verify User Transaction</h1></th>
</tr>

<?
	include "out_queue_nav.php";
?>

</table>

<br />

<table cellpadding="0" cellspacing="0" class="listing">

<tr>
    <td align="left">
         <form name="verifyNewClientTransaction" id="verifyNewClientTransaction" method="post" action="controllers/cases.php" onsubmit="">
            <div class="left">
            <div class="row">
                    <label for="receiverPhone">Receiver Phone</label>
                    <input type="text" name="receiverPhone" id="receiverPhone" class="field" value="<?=$data['receiverPhone']?>" 
                    <? if($data['receiverPhone']) echo"disabled='disabled'"?> />
                </div>
                <div class="row">
                    <label for="itemID">Item ID</label>
                    <input type="text" name="itemID" id="itemID" class="field" value="<?=$data['itemIDNumber']?>"  disabled="disabled" />
                </div>
                <div class="row">
                    <label for="employeeUsername">Employee Username</label>
                    <input type="text" name="employeeUsername" id="employeeUsername" class="field" 
                    value="<?=$_SESSION['username']?>" />
                </div>
                <div class="row">
                    <label for="employeePassword">Employee Password</label>
                    <input type="password" name="employeePassword" id="employeePassword" class="field" value="" />
                </div>
                <div class="row">
                    <label for="dateOfFirstPurchase">Time Stamp</label>
                    <input type="text" name="dateOfFirstPurchase" id="dateOfFirstPurchase" class="field" 
                    value="<?=$data['dateOfFirstPurchase']?>" disabled="disabled" />
                </div>
                <div class="row">
                    <label for="transID">Transaction ID</label>
                    <input type="text" name="transID" id="transID" class="field" value="<?=$data['transID']?>" 
                    disabled="disabled" />
                </div>
                <div class="row">
                    <label for="clientEmail">Client Email</label>
                    <input type="text" name="clientEmail" id="clientEmail" class="field" value="<?=$data['emailAddress']?>" 
                    disabled="disabled" />
                </div>
                <div class="row">
                    <label for="clientFirstName">Client First Name</label>
                    <input type="text" name="clientFirstName" id="clientFirstName" class="field" value="<?=$data['firstName']?>" 
                    disabled="disabled" />
                </div>
                <div class="row">
                    <label for="clientLastName">Client Last Name</label>
                    <input type="text" name="clientLastName" id="clientLastName" class="field" value="<?=$data['lastName']?>" 
                    disabled="disabled" />
                </div>
                <div class="row">
                    <label for="clientPhone">Client Phone</label>
                    <input type="text" name="clientPhone" id="clientPhone" class="field" value="<?=$data['cellphone']?>" 
                    disabled="disabled" />
                </div>
                <div class="row">
                    <label for="paymentStatus">Payment Status</label>
                    <input type="text" name="paymentStatus" disabled="disabled" id="paymentStatus" class="field" 
                     value="<?=$data['paymentStatus']?>"/>
                </div>
                <div class="row">
                    <label for="clientIP">IP Address</label>
                    <input type="text" name="clientIP"  id="clientIP" class="field" value="<?=$data['clientIP']?>" 
                     disabled="disabled" />
                </div>
                <div class="row">
                    <label for="addressStreet">Address Street</label>
                    <input type="text" name="addressStreet"  id="addressStreet" class="field" value="<?=$data['addressStreet']?>" 
                     disabled="disabled" />
                </div>
                <div class="row">
                    <label for="addressCity">Address City</label>
                    <input type="text" name="addressCity"  id="addressCity" class="field" value="<?=$data['addressCity']?>" 
                     disabled="disabled" />
                </div>
                <div class="row">
                    <label for="ipCity">IP City</label>
                    <input type="text" name="ipCity"  id="ipCity" class="field" value="<?=$data['ipCity']?>" 
                     disabled="disabled" />
                </div>
                <div class="row">
                    <label for="addressState">Address State</label>
                    <input type="text" name="addressState"  id="addressState" class="field" value="<?=$data['addressState']?>" 
                     disabled="disabled" />
                </div>
                <div class="row">
                    <label for="ipState">IP State</label>
                    <input type="text" name="ipState"  id="ipState" class="field" value="<?=$data['ipState']?>" 
                     disabled="disabled" />
                </div>
                <div class="row">
                    <label for="addressCountry">Address Country</label>
                    <input type="text" name="addressCountry"  id="addressCountry" class="field" 
                    value="<?=$data['addressCountry']?>" disabled="disabled" />
                </div>
                <div class="row">
                    <label for="ipCountry">IP Country</label>
                    <input type="text" name="ipCountry"  id="ipCountry" class="field" value="<?=$data['ipCountry']?>" 
                     disabled="disabled" />
                </div>
                <div class="row">
                    <label for="ourPayPalEmail">Our Paypal Email</label>
                    <input type="text" name="ourPayPalEmail" disabled="disabled" id="ourPayPalEmail" class="field" 
                     value="<?=$data['ourPayPalEmail']?>"/>
                </div>
                <div class="row">
                    <label for="receiverFirstName">Receiver First Name</label>
                    <input type="text" name="receiverFirstName" disabled="disabled" id="receiverFirstName" class="field" 
                     value="<?=$data['receiverFirstName']?>"/>
                </div>
                <div class="row">
                    <label for="receiverLastName">Receiver Last Name</label>
                    <input type="text" name="receiverLastName" disabled="disabled" id="receiverLastName" class="field" 
                     value="<?=$data['receiverLastName']?>"/>
                </div>
                <div class="row">
                    <label for="receiverEmail">Receiver Email</label>
                    <input type="text" name="receiverEmail"  id="receiverEmail" class="field" value="<?=$data['receiverEmail']?>" 
                     disabled="disabled" />
                </div>
                <div class="row">
                    <label for="receiverRegion">Receiver Region</label>
                    <input type="text" name="receiverRegion"  id="receiverRegion" class="field" value="<?=$data['receiverRegion']?>" 
                     disabled="disabled" />
                </div>
                <div class="row">
                    <label for="comments">Comments</label>
                    <input type="text" name="comments" id="comments" class="field" value="<?=$data['comments']?>" />
                </div>
                
            </div>
            <div class="clear"></div>
            <div class="row">
                <label>
                    <input type="submit" name="submit" id="submit" value="Send" class="submit" onfocus="this.blur();"   />
                </label>
                <input type="button" name="cancel" id="cancel" value="Cancel" class="submit" 
                style="margin-top: 5px;" onclick="history.go(-1);"onfocus="this.blur();" />
            </div>
            <div class="clear">&nbsp;</div>
            <input type="hidden" name="type" id="type" value="verifyNewClientTransaction" />
            <input type="hidden" name="id" id="id" value="<?=$data['transID']?>" />
            <input type="hidden" name="receiverPhone" id="receiverPhone" value="<?=$data['receiverPhone']?>" 
            <? if(!$data['receiverPhone']) echo"disabled='disabled'"?>/>
            <input type="hidden" name="itemIDNumber" id="itemIDNumber" value="<?=$data['itemIDNumber']?>" />
            <input type="hidden" name="dateOfFirstPurchase" id="dateOfFirstPurchase" value="<?=$data['dateOfFirstPurchase']?>" />
            <input type="hidden" name="registrationDate" id="registrationDate" value="<?=$data['registrationDate']?>" />
            <input type="hidden" name="transID" id="transID" value="<?=$data['transID']?>" />
            <input type="hidden" name="clientEmail" id="clientEmail" value="<?=$data['emailAddress']?>" />
            <input type="hidden" name="clientFirstName" id="clientFirstName" value="<?=$data['firstName']?>" />
            <input type="hidden" name="clientLastName" id="clientLastName" value="<?=$data['lastName']?>" />
            <input type="hidden" name="clientPhone" id="clientPhone" value="<?=$data['cellphone']?>" />
            <input type="hidden" name="paymentStatus" id="paymentStatus" value="<?=$data['paymentStatus']?>"/>
            <input type="hidden" name="clientIP"  id="clientIP" value="<?=$data['clientIP']?>" />
            <input type="hidden" name="addressStreet"  id="addressStreet" value="<?=$data['addressStreet']?>" />
            <input type="hidden" name="addressCity"  id="addressCity" value="<?=$data['addressCity']?>" />
            <input type="hidden" name="ipCity"  id="ipCity" value="<?=$data['ipCity']?>" />
            <input type="hidden" name="addressZip"  id="addressZip" value="<?=$data['addressZip']?>" />
            <input type="hidden" name="addressState"  id="addressState" value="<?=$data['addressState']?>" />
            <input type="hidden" name="ipState"  id="ipState" value="<?=$data['ipState']?>" />
            <input type="hidden" name="addressCountry"  id="addressCountry" value="<?=$data['addressCountry']?>" />
            <input type="hidden" name="ipCountry"  id="ipCountry" value="<?=$data['ipCountry']?>" />
            <input type="hidden" name="ourPayPalEmail" id="ourPayPalEmail" value="<?=$data['ourPayPalEmail']?>"/>
            <input type="hidden" name="receiverFirstName" id="receiverFirstName" value="<?=$data['receiverFirstName']?>"/>
            <input type="hidden" name="receiverLastName" id="receiverLastName" value="<?=$data['receiverLastName']?>"/>
            <input type="hidden" name="receiverEmail"  id="receiverEmail" value="<?=$data['receiverEmail']?>" />
            <input type="hidden" name="receiverRegion"  id="receiverRegion" value="<?=$data['receiverRegion']?>" />
            <input type="hidden" name="order_id"  id="order_id" value="<?=$data['order_id']?>" />
            <input type="hidden" name="page" id="page" value="<?=$_GET['page']?>" />
        </form>
    </td>
</tr>

</table>

<? include "wrapper/footer.php"; ?>
