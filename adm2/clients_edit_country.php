<?
	include "wrapper/header.php";
	
	$connection = connect2DB2($countryAbbreviation);
	$query		= "SELECT * FROM CountryList WHERE countryAbbreviation = '$_GET[id]'";
	$result		= mysql_query($query);
	$data		= mysql_fetch_array($result);
	disconnectDB($connection);
?>
<table cellpadding="0" cellspacing="0" class="listing">

<tr>
	<th><h1><a href="javascript:history.go(-1);" class="sub_menu" title="back" onfocus="this.blur();">&laquo; Back</a> Edit Country</h1></th>
</tr>

<?
	include "clients_header_nav.php";
?>

</table>

<br />

<table cellpadding="0" cellspacing="0" class="listing">

<tr>
    <td align="left">
         <form name="editCountry" id="editCountry" method="post" action="controllers/cases.php">
            <div class="left">
                <div class="row">
                    <label for="countryAbbreviation">Country</label>
					<select name="countryAbbreviation" id="countryAbbreviation">
					<?
						include_once $_SERVER['DOCUMENT_ROOT']."/src/country_codes.php";
						getCountryDropDownSelected($data['countryAbbreviation']);
					?>
					</select>
                </div>
                <div class="clear"></div>
				<div class="row">
                    <label for="countryStatus">Country Status</label>
					<input type="hidden" name="originalcountryStatus" id="originalcountryStatus" value="<?=$data['status']?>" />
					<select name="countryStatus" id="countryStatus" >
					<?
						$statusValues = array("BLOCKED","UNBLOCKED");				
					    for($i = 0 ; $i < count($statusValues) ; $i ++ )
						{
							$status = $statusValues[$i];
							if ( $status == $data['status'] )
							{
								echo"<option value=\"$status\" selected=\"selected\">$status</option>";
							}
							else
							{
								echo"<option value=\"$status\">$status</option>";
							}
						}
					?>
                    </select>
                </div>
            <div class="clear"></div>
            <div class="row">
				<label><input type="submit" name="submit" id="submit" value="Modify" class="submit" onfocus="this.blur();" /></label>
                <input type="button" name="cancel" id="cancel" value="Cancel" class="submit" style="margin-top: 5px;" onclick="history.go(-1);"onfocus="this.blur();" />
            </div>
            <div class="clear">&nbsp;</div>
            <input type="hidden" name="type" id="type" value="editCountry" />
			<input type="hidden" name="page" id="page" value="<?=$_GET['page']?>" />
			<input type="hidden" name="employee_username" id="employee_username" value="<?=$_SESSION['uid']?>" />
        </form>
    </td>
</tr>

</table>

<? include "wrapper/footer.php"; ?>