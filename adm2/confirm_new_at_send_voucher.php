<?
	include "wrapper/header.php";
        $receiver_amount_in_local = getValueInLocal($_POST['itemIDNumber']);
?>

<table cellpadding="0" cellspacing="0" class="listing">
	<tr>
		<th colspan="6"><h1>Send <?=PRODUCT?></h1></th>
	</tr>

<?
	include "send_at_header_nav.php";
?>

</table>

<table cellpadding="0" cellspacing="0" class="listing">

<tr>
    <td align="left">
        <div class ="left">
            <div>
                <div>
                    <div>
                        <h2>Confirm <?=PRODUCT?> receiver details</h2>
                        <b>Receiver Name:</b> <?=$_POST["receiverFirstName"]?> <?=$_POST["receiverLastName"]?><br/>
                        <b>Receiver Phone:</b> <?=$_POST["receiverPhone"]?><br/>
                        <b><?=PRODUCT?> To Be Received:</b> <?=LOCAL_CURRENCY?> <?=number_format($receiver_amount_in_local, LOCAL_DECIMAL_PLACES)?><br/>
                        <b><?=PRODUCT?> To Be Received As:</b> <?=$_POST["airtime_load_option"]?><br/>
                        <b>Receiver's Region:</b> <?=$_POST["receiverRegion"]?><br/><br/>
                        <b>Transaction ID:</b> <?=$_POST["transID"]?><br/>
                        <b>Client's Email:</b> <?=$_POST["clientEmail"]?><br/>
                        <b>Client's Name:</b> <?=$_POST["clientFirstName"]?> <?=$_POST["clientLastName"]?><br/>
                    </div>
                    <div class="clear"></div>
                    <form action="sendAT.php" method="post" name="submit_order_form_auto" id="confirm_new_at_send_voucher">
                        <div align="right" class="row" style="padding-right:20px;">
                            <input type="hidden" name="employeeUsername"         value="<?=$_POST['employeeUsername']?>">
                            <input type="hidden" name="employeePassword"         value="<?=$_POST['employeePassword']?>">
                            <input type="hidden" name="transID"                  value="<?=$_POST['transID']?>">
                            <input type="hidden" name="clientEmail"              value="<?=$_POST['clientEmail']?>">
                            <input type="hidden" name="clientFirstName"          value="<?=$_POST['clientFirstName']?>">
                            <input type="hidden" name="clientLastName"           value="<?=$_POST['clientLastName']?>">
                            <input type="hidden" name="receiverFirstName"        value="<?=$_POST['receiverFirstName']?>">
                            <input type="hidden" name="receiverLastName"         value="<?=$_POST['receiverLastName']?>">
                            <input type="hidden" name="receiverRegion"           value="<?=$_POST['receiverRegion']?>">
                            <input type="hidden" name="receiverPhone"            value="<?=$_POST['receiverPhone']?>">
                            <input type="hidden" name="phoneConfirm"             value="<?=$_POST['phoneConfirm']?>">
                            <input type="hidden" name="itemIDNumber"             value="<?=$_POST['itemIDNumber']?>">
                            <input type="hidden" name="receiver_amount_in_local" value="<?=$receiver_amount_in_local?>">
                            <input type="hidden" name="type"                     value="<?=$_POST['type']?>">
                            <input type="hidden" name="airtime_load_option"      value="<?=$_POST['airtime_load_option']?>">
                            <input type="Submit" name="Submit" id="Submit"       value="Submit" class="submit" onfocus="this.blur();" />
                            <input type="button" name="cancel" id="cancel"       value="Cancel" class="submit" style="margin-top: 5px;" onclick="history.go(-1);" onfocus="this.blur();" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </td>
</tr>


</table>

<?
	include "wrapper/footer.php";
?>
