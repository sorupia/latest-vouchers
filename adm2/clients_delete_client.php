<?
	include "wrapper/header.php";
	
	$connection = connect2SMSDB2($countryAbbreviation);
	$query		= mysql_query("SELECT * FROM SMSUsers WHERE smsUserID = '$_GET[id]'");
	$data		= mysql_fetch_array($query);
	disconnectDB($connection);
?>
<table cellpadding="0" cellspacing="0" class="listing">

<tr>
	<th><h1><a href="javascript:history.go(-1);" class="sub_menu" title="back" onfocus="this.blur();">&laquo; Back</a> Delete User</h1></th>
</tr>

<?
	include "clients_header_nav.php";
?>

</table>

<br />

<table cellpadding="0" cellspacing="0" class="listing">

<tr>
    <td align="left">
         <form name="deleteUser" id="deleteUser" method="post" action="controllers/cases.php">
              <div class="left">
              <div class="row">
                    <label for="fname">First Name</label>
                    <input type="text" name="firstName" id="firstName" disabled="disabled" class="field" value="<?=$data['firstName']?>" />
                </div>
                <div class="clear"></div>
                <div class="row">
                    <label for="lname">Last Name</label>
                    <input type="text" name="lastName" id="lastName" disabled="disabled" class="field" value="<?=$data['lastName']?>" />
                </div>
                <div class="row">
                    <label for="email">Email</label>
                    <input type="text" name="emailAddress" id="emailAddress" disabled="disabled" class="field" value="<?=$data['emailAddress']?>" />
                </div>
				<div class="row">
                    <label for="country">Airtime Status</label>
					<input type="text" name="originalAirtimeStatus" id="originalAirtimeStatus" disabled="disabled" class="field" value="<?=$data['airtimeStatus']?>" />
                </div>
				<div class="row">
                    <label for="country">SMS Status</label>
					<input type="text" name="originalSubscriberStatus" id="originalSubscriberStatus" disabled="disabled" class="field" value="<?=$data['subscriberStatus']?>" />
                </div>				
                <div class="row">
                    <label for="cellphone">Phone</label>
                    <input type="text" name="cellphone" id="cellphone" disabled="disabled" class="field" value="<?=$data['cellphone']?>" />
                </div>
                <div class="row">
                    <label for="addressStreet">Address</label>
                    <input type="text" name="addressStreet" id="addressStreet" disabled="disabled" class="field" value="<?=$data['addressStreet']?>" />
                </div>
                <div class="row">
                    <label for="addressCity">City</label>
                    <input type="text" name="addressCity" id="addressCity" disabled="disabled" class="field" value="<?=$data['addressCity']?>" />
                </div>
				<div class="row">
                    <label for="addressZip">Postal Code</label>
                    <input type="text" name="addressZip" id="addressZip" disabled="disabled" class="field" value="<?=$data['addressZip']?>" />
                </div>
                <div class="row">
                    <label for="addressState">Province / State</label>
                    <input type="text" name="addressState" id="addressState" disabled="disabled" class="field" value="<?=$data['addressState']?>" />
                </div>
                <div class="row">
                    <label for="addressCountry">Country</label>
                    <input type="text" name="addressCountry" id="addressCountry" disabled="disabled" class="field" value="<?=$data['addressCountry']?>" />
                </div>
				<div class="row">
                    <label for="creditsLeft">Credits Left</label>
                    <input type="text" name="creditsLeft" id="creditsLeft" disabled="disabled" class="field" value="<?=$data['creditsLeft']?>" />
                </div>
				<div class="row">
                    <label for="airtimeCreditsLeft">Airtime Credits</label>
                    <input type="text" name="airtimeCreditsLeft" id="airtimeCreditsLeft" disabled="disabled" class="field" value="<?=$data['airtimeCreditsLeft']?>" />
                </div>
				<div class="row">
                    <label for="registrationDate">Registration Date</label>
                    <input type="text" name="registrationDate" id="registrationDate" disabled="disabled"  class="field" value="<?=$data['registrationDate']?>" />
                </div>
				<div class="row">
                    <label for="lastLogin">Last Login</label>
                    <input type="text" name="lastLogin" id="lastLogin" disabled="disabled" class="field" value="<?=$data['lastLogin']?>" />
                </div>
            </div>
            <div class="clear"></div>
            <div class="row">
				<label><input type="submit" name="submit" id="submit" value="Delete" class="submit" onfocus="this.blur();" /></label>
                <input type="button" name="cancel" id="cancel" value="Cancel" class="submit" style="margin-top: 5px;" onclick="history.go(-1);"onfocus="this.blur();" />
            </div>
            <div class="clear">&nbsp;</div>
            <input type="hidden" name="type" id="type" value="deleteUser" />
            <input type="hidden" name="smsUserID" id="smsUserID" value="<?=$_GET['id']?>" />
            <input type="hidden" name="emailAddress" id="emailAddress" value="<?=$data['emailAddress']?>" />
        </form>
    </td>
</tr>

</table>

<? include "wrapper/footer.php"; ?>