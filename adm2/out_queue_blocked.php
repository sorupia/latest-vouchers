<?
	include "wrapper/header.php";
?>

<table cellpadding="0" cellspacing="0" class="listing">
	<tr>
		<th colspan="6"><h1>Outgoing Queue - Blocked Purchases Queue</h1></th>
	</tr>
<?
	include "out_queue_nav.php";
?>

<?
	include "src/messages.php";
?>

</table>

<table cellpadding="0" cellspacing="0" class="listing">
<?
	
	$thisPage = substr($page,0,-4);

	$values   = array("id","timeStamp","transID","firstName","lastName","emailAddress",
	                  "receiverPhone","clientIP","itemIDNumber",
					  "ipCity","ipState","ipCountry","comments");
	
	$result = render_pager($values, "BlockedClientsQueue", "id DESC", 15, $countryAbbreviation, 'airtime');
	$exist = mysql_num_rows($result);
	
	if($exist)
	{
		echo'<thead>
			<tr>
			<th>Options</th>
			<th>Time Stamp</th>
			<th>Transaction ID</th>
			<th>Client Name</th>
			<th>Client Email</th>
			<th>Receiver Phone</th>
			<th>Item ID</th>
			<th>IP City</th>
			<th>IP State</th>
			<th>IP Country</th>
			<th>Comments</th>
			</tr>
		</thead>';
						
		while($data = mysql_fetch_array($result))
		{
			
			echo'<tbody><tr>
				<td>'.
				"<a href='send_blocked_purchase.php?id=".$data['id'].
                "&page=$thisPage' onfocus='this.blur();' title='Send Airtime'>Send Airtime</a> | ".
				"<a href='#' onfocus='this.blur();' title='Delete Transaction' onclick=delEntry('".$data['id'].
                "','BlockedClientsQueue','id','$thisPage');>Delete</a>".'</td>
			<td>'.$data['timeStamp'].'</td>
            <td>'.$data['transID'].'</td>
			<td>'.$data['firstName'].' '.$data['lastName'].'</td>
			<td>'.$data['emailAddress'].'</td>
			<td>'.$data['receiverPhone'].'</td>
			<td>'.$data['itemIDNumber'].'</td>
			<td>'.$data['ipCity'].'</td>
			<td>'.$data['ipState'].'</td>
			<td>'.$data['ipCountry'].'</td>
			<td>'.$data['comments'].'</td>
			</tr></tbody>';
			
			$u++;
		}
	}
	else
	{
		echo '<tr><td align="center" colspan="4">No records in Blocked Purchases Queue</td></tr>';
	}
?>

</table>

<?
	include "wrapper/footer.php";
?>