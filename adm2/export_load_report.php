<?
/*******************************************************************************
**  FILE: export_load_report.php
**
**  FUNCTION: export_load_report
**
**  PURPOSE: A load report requested by GhanaAirTime to list cards loaded on a 
**           particular day. This report will also be exportable to csv and xls formats.
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.10.29
*********************************************************************************/

if($_POST['xls_file'] || $_POST['csv_file'])
{
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/GeneralFunctions.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Objects/ObjectFunctions.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Objects/AllCardsTable.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/General/convert_transformed_array_to_CSV_string.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/General/convert_transformed_array_to_xls.php';

    $countryAbbreviation = COUNTRY_ABBREVIATION;
    $debug = false;

    $start_date  = $_POST['start_date'];
    $end_date    = $_POST['end_date'];
    $item_id     = $_POST['itemID'];
    
    $input_array = $_POST;
    $input_array['use_echo'] = 0;

    $file_name = "noname.xls";

    if($_POST['csv_file'])
    {
        $connection = connect2DB2($countryAbbreviation);
        $input_array['connection'] = $connection;
            $allCardsArray = query_cards_for_load_report($input_array);
        disconnectDB($connection);

        $column_names = array("Date We Purchased",
                              "Item ID",
                              "Card PIN",
                              "Status Date",
                              "Card Status",
                              "Transaction ID",
                              "Client Email",
                              "Reason");

        /* process the db results into string */
        $file_text = convert_transformed_array_to_CSV_string($column_names, $allCardsArray);

        /* write to temporary file */
        $file_name = "load_report.csv";
        $file_handler = @fopen($file_name,'w') or die("failed to open file: $file_name for writing ");			
        $num_of_bytes = @fwrite($file_handler, $file_text) or die("failed to write to $file_name");						
        @fclose($file_handler);
    }
    else if($_POST['xls_file'])
    {
        $connection = connect2DB2($countryAbbreviation);
        $input_array['connection'] = $connection;
            $allCardsArray = query_cards_for_load_report($input_array);
        disconnectDB($connection);

        $column_names = array("Date We Purchased",
                              "Item ID",
                              "Card PIN",
                              "Status Date",
                              "Card Status",
                              "Transaction ID",
                              "Client Email",
                              "Reason");

        /* process the db results into string */
        $file_text = convert_transformed_array_to_xls($column_names, $allCardsArray);

        /* write to temporary file */
        $file_name = "load_report.xls";
        $file_handler = @fopen($file_name,'w') or die("failed to open file: $file_name for writing ");			
        $num_of_bytes = @fwrite($file_handler, $file_text) or die("failed to write to $file_name");						
        @fclose($file_handler);
    }

    /* initiate save as local file option */
    $ext = strtolower(substr(strrchr($file_name,"."),1));
    
    // required for IE, otherwise Content-disposition is ignored
    if(ini_get('zlib.output_compression'))
    ini_set('zlib.output_compression', 'Off');
    
    if($ext == "php"){

    } 
    else {
        switch($ext)
        {
            case "pdf": $ctype="application/pdf"; break;
            case "exe": $ctype="application/octet-stream"; break;
            case "zip": $ctype="application/zip"; break;
            case "doc": $ctype="application/msword"; break;
            case "xls": $ctype="application/vnd.ms-excel"; break;
            case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
            case "gif": $ctype="image/gif"; break;
            case "png": $ctype="image/png"; break;
            case "jpeg":
            case "jpg": $ctype="image/jpg"; break;
            default: $ctype="application/force-download";
        }
        
        header("Pragma: public"); // required
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false); // required for certain browsers 
        header("Content-Type: $ctype");
        // change, added quotes to allow spaces in filenames, by Rajkumar Singh
        header("Content-Disposition: attachment; filename=\"".basename($file_name)."\";" );
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: ".filesize($file_name));
        readfile("$file_name");
        exit();
    }		
    
}

?>