 <?php 
	include_once $_SERVER['DOCUMENT_ROOT'].'/src/constants.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/cards.php';
	function getDropDown($countryAbbreviation){
		global $cardMap;
		global $networkMap;
		$numberOfNetworks=count($networkMap[$countryAbbreviation]);
		
		for($networkIndex=0;$networkIndex<$numberOfNetworks;$networkIndex++){
			$thisNetwork=$networkMap[$countryAbbreviation][$networkIndex];
			$numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);
			
			for($j=0;$j<$numberOfDenominations;$j++){
				$thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
				if($j==0 && $networkIndex==0){
					echo"<option value=\"".$thisCard."\" selected>".$thisCard."</option>";
				}
				else{
					echo"<option value=\"".$thisCard."\">".$thisCard."</option>";
				}
			}
		}
	}
	
	function getDropDownSelected($countryAbbreviation, $selectedItem){
		global $cardMap;
		global $networkMap;
        global $networkArray;
        
        $selected_vendor_id = substr($selectedItem, 0, 5);
		$numberOfNetworks=count($networkMap[$countryAbbreviation]);
		
    for($networkIndex=0;$networkIndex<$numberOfNetworks;$networkIndex++){
                $thisNetwork=$networkMap[$countryAbbreviation][$networkIndex];
                $numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);
                
                for($j=0;$j<$numberOfDenominations;$j++){
                    $thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
                     $priceInUSD = $networkArray[$countryAbbreviation]["$thisNetwork"][0]["$thisCard"]["PriceInUSD"];
                    $vendor_ids = substr($thisCard, 0, 5);
                    $amount = substr($thisCard, 9);
                    if( $selectedItem == $thisCard && $vendor_ids ==  $selected_vendor_id){
                        
                        echo LOCAL_CURRENCY ." ".number_format($amount)." for ".$priceInUSD." ". SALE_CURRENCY;
                    }
                }
            }
	}
    
    
?>