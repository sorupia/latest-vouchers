<?
	include "wrapper/header.php";
    include_once "itemIDDropDown.php";
?>

<table cellpadding="0" cellspacing="0" class="listing">
	<tr>
		<th colspan="6"><h1>Reports - Load Report</h1></th>
	</tr>
	
<?
	include "reports_header_nav.php";
?>
	
</table>

<form name="reports" id="reports" method="post" enctype="multipart/form-data" action="reports_load_report.php">

	<p>
    Employee Username 
	<input name="employee_username" type="text" class="field_small" id="employee_username" value="" /> 
	&nbsp;
	Employee Password 
	<input name="employee_password" type="password" class="field_small" id="employee_password" value="" />
	</p>

	<p>
    Start Date
	<input name="start_date" id="start_date" class="datefield" onfocus="this.blur();" />
				<a href="#" title="Add Date" onclick="callDate('1', 'reports', 'start_date', this, event);" 
				onfocus="this.blur();"><img src="images/icons/calendar.gif" alt="Add Date" class="datefieldicon" 
				onmouseover='tooltip(this);' name='Open calendar tool' /></a>

	End Date
	<input name="end_date" id="end_date" class="datefield" onfocus="this.blur();" />
			 <a href="#" title="Add Date" onclick="callEndDate('1', 'addCourse', 'end_date', this, event);" 
			 onfocus="this.blur();"><img src="images/icons/calendar.gif" alt="Add Date" class="datefieldicon" 
			 onmouseover='tooltip(this);' name='Open calendar tool' /></a>

	Ex. Rate
	<input name="ex_rate" type="text" class="field_small" id="ex_rate" value="<?=EXCHANGE_RATE?>" />
</p>
	<p>
      Item ID
      <select name="itemID" id="itemID">
		<?getDropDown($countryAbbreviation);?>
      </select>
    </p>
    <p>
      Report Type
	  <select name="report_type" accesskey="t">
		<option value="load_report">Load Report</option>
	  </select>
	</p>
	<p>
	  <input type="submit" name="Generate" id="submit" 
			 value="Preview" class="submit" onfocus="this.blur();" />
	  <input type="button" name="cancel" id="cancel" value="Cancel" 
			 class="submit" onfocus="this.blur();" onclick="openPage('reports.php');" />
	</p>
</form>

<table cellpadding="0" cellspacing="0" class="listing">
<?
	include "src/reports_load_report_code.php"
?>
</table>

<?
	include "wrapper/footer.php";
?>