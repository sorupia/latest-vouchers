<? require_once $_SERVER['DOCUMENT_ROOT']."/src/constants.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=SERVICED_BY?> Admin</title>
<link rel="stylesheet" href="login.css" />
</head>

<? $page = ""; ?>

<body>
<form id="login" action="controllers/cases.php" method="post">
    <table cellpadding="5" cellspacing="2" class="tbl">
        <tr>
            <th><label for="username">Username</label></th>
            <th><label for="password">Password</label></th>
        </tr>
        <tr>
            <td><input type="text" name="username" id="username" class="field" /></td>
            <td><input type="password" name="password" id="password" class="field" /></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" name="submit" id="submit" value="Login" class="submit" onfocus="this.blur();" /></td>
        </tr>
    </table>
    <input type="hidden" name="type" id="type" value="login" />
    <input type="hidden" name="redirect" id="redirect" value="<?=$_GET['redirect']?>" />
</form>
<? include "wrapper/footer.php"; ?>