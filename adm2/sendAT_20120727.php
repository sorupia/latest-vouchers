<?php
	session_start();
    include_once $_SERVER['DOCUMENT_ROOT']."/src/SessionFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/ObjectFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Messaging/MessagingFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Yo/yo_functions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/OrderFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Constants/Constants.php";

    if($_POST['Submit'])
    {
        $employeeUsername    = $_POST['employeeUsername'];
        $employeePassword    = $_POST['employeePassword']; 
        $transID             = $_POST['transactionID']; 
        $clientEmail         = $_POST['clientEmail']; 
        $clientFirstName     = $_POST['clientFirstName']; 
        $receiverFirstName   = $_POST['receiverFirstName']; 
        $receiverPhone       = $_POST['receiverMobilePhone'];
        $receiverPhone2      = $_POST['receiverMobilePhone2'];
        $receiverEmail       = $_POST['receiverEmail']; 
        $itemIDNumber        = $_POST['itemIDNumber'];
        $countryAbbreviation = COUNTRY_ABBREVIATION;
        $timeCardSent        = date("Y-m-d H:i:s");
        $ipLocationArray     = getIPLocation();
        $clientIP            = $ipLocationArray['ip_address'];
        $paymentAmount       = getPriceInUSD($itemIDNumber);
        $paymentFee          = 0;

        $transactionArray                 = $_POST;
        $transactionArray['txn_id']       = $transID;
        $transactionArray['item_number']  = $itemIDNumber;
        $transactionArray['payer_email']  = $clientEmail;
        $transactionArray['mc_fee']       = $paymentFee;
        $transactionArray['timeCardSent'] = $timeCardSent;
        $transactionArray['mc_gross']     = $paymentAmount;
        $transactionArray['first_name']   = $clientFirstName;
        $transactionArray['sessionUser']  = $employeeUsername;
        $transactionArray["clientIP"]     = $ipLocationArray['ip_address'];
        $transactionArray['order_data']['ip_city']                  = $ipLocationArray['ip_city'];
        $transactionArray['order_data']['ip_state']                 = $ipLocationArray['ip_state'];
        $transactionArray['order_data']['ip_country']               = $ipLocationArray['ip_country'];
        $transactionArray['order_data']['receiver_phone']           = $receiverPhone;
        $transactionArray['order_data']['receiver_email']           = $receiverEmail;
        $transactionArray['order_data']['receiver_first_name']      = $receiverFirstName;
        $transactionArray['order_data']['receiver_network_id']      = substr($itemIDNumber,0,5);
        $transactionArray['order_data']['receiver_amount_in_local'] = getValueInLocal($itemIDNumber);

        $transactionArray['receiver_phone']      = $receiverPhone;
        $transactionArray['receiver_email']      = $receiverEmail;
        $transactionArray['receiver_first_name'] = $receiverFirstName;
        $transactionArray['phoneConfirm']        = $receiverPhone2;
        $transactionArray['network_id']          = substr($itemIDNumber,0,5);

        //Validate receiver form and return network id
        $transactionArray = validate_receiver_form_auto($transactionArray);
        $success = $transactionArray['success'];

        if($itemIDNumber == "")
        {
            $transactionArray = create_item_id($transactionArray);
            $itemIDNumber                                               = $transactionArray['created_item_id'];
            $transactionArray['item_number']                            = $itemIDNumber;
            $transactionArray['itemIDNumber']                           = $itemIDNumber;
            $transactionArray['order_data']['receiver_network_id']      = substr($itemIDNumber,0,5);
            $transactionArray['order_data']['receiver_amount_in_local'] = getValueInLocal($itemIDNumber);
        }

        if(array_key_exists('receiver_amount_in_local', $transactionArray))
        {
            $transactionArray['order_data']['receiver_amount_in_local'] = $transactionArray['receiver_amount_in_local'];
        }
        else
        {
            $transactionArray['receiver_amount_in_local'] = getValueInLocal($itemIDNumber);
        }

        $message = "";

        if($success)
        {
            if(authenticateEmployee($employeeUsername, $employeePassword, $countryAbbreviation))
            {
                $connection=connect2DB2($countryAbbreviation);
                $transactionArray['connection'] = $connection;

                $transactionArray = checkDuplicateTransID($transactionArray);
                $success = !$transactionArray['transaction_id_exists'];

                if($clientEmail==""||$clientFirstName==""||$receiverPhone==""||$transID=="")
                {
                    $message.= 'The transaction ID, client email, client first name and receiver phone cannot be blank<br>';
                    disconnectDB($connection);
                    $_SESSION['message'] = $message;
                    $success = false;
                }
                else if( $success == false )
                {
                    $message.= "The transaction id you entered is a duplicate<br>";
                    disconnectDB($connection);
                    $_SESSION['message'] = $message;
                }
                else
                {
                    if(isAutoDispatch($itemIDNumber))
                    {
                        //AUTO_LOAD
                        $transactionArray['autoDispatch'] = 1;
                        $transactionArray = process_cart_auto_item($transactionArray);
                        
                        if($transactionArray['invoke_success'])
                        {
                            $transactionArray = recordSale($transactionArray);

                            updateReceiverInfo($transactionArray);

                            $cardPIN = $transactionArray['cardPIN'];
                            $message.= "Airtime Card with ItemID: $itemIDNumber Sent <br>";
                            $message.= "Access No.: $cardPIN <br>";
                            $message.= "Receiver Phone: $receiverPhone <br>";
                            $message.= "Receiver Email: $receiverEmail <br>";
                            $message.= "Client Email: $clientEmail <br>";
                        }
                        else
                        {
                            $message.= "Unable to send airtime. Item has been saved to outgoing queue. ";
                            $message.= $transactionArray['api_response']['StatusMessage'];
                        }
                    }
                    else
                    {
                        //CARD_LOAD
                        $transactionArray['autoDispatch'] = 0;
                        $transactionArray = useCard($transactionArray);

                        if($transactionArray['cardsArePresent'] == "true")
                        {
                            $transactionArray = recordSale($transactionArray);
                            emailAirtime($transactionArray);

                            updateReceiverInfo($transactionArray);

                            $cardPIN = $transactionArray['cardPIN'];
                            $message.= "Airtime Card with ItemID: $itemIDNumber Sent <br>";
                            $message.= "Access No.: $cardPIN <br>";
                            $message.= "Receiver Phone: $receiverPhone <br>";
                            $message.= "Receiver Email: $receiverEmail <br>";
                            $message.= "Client Email: $clientEmail <br>";
                        }
                        else
                        {
                            $message.= "There are no more $itemIDNumber cards left";
                            $success = false;
                        }
                    }

                    disconnectDB($connection);
                    $_SESSION['message'] = $message;
                }
            }
            else
            {
                $message.= 'The username and password are incorrect <br>';
                $_SESSION['message'] = $message;
                $success = false;
            }
        }
        else
        {
            $message = $transactionArray['error'];
            $_SESSION['message'] = $transactionArray['error'];
        }
        
        if($success)
        {
            header("Location: new_at_send_at.php");
        }
        else
        {
            if($transactionArray['airtime_load_option'] == AIRTIME_LOAD_OPTION_AUTO)
            {
                $form_action = "new_at_send_at_auto.php";
            }
            else
            {
                $form_action = "new_at_send_at.php";
            }
        
            //REPOST information back to form
            echo'<form action="'.$form_action.'" method="post" name="manual_send_auto_at_repost" id="manual_send_auto_at_repost">
                 <input type="hidden" name="transactionID"            value="'.$_POST['transactionID'].'">
                 <input type="hidden" name="clientEmail"              value="'.$_POST['clientEmail'].'" />
                 <input type="hidden" name="clientFirstName"          value="'.$_POST['clientFirstName'].'" />
                 <input type="hidden" name="receiverFirstName"        value="'.$_POST['receiverFirstName'].'" />
                 <input type="hidden" name="receiverMobilePhone"      value="'.$_POST['receiverMobilePhone'].'" />
                 <input type="hidden" name="receiverMobilePhone2"     value="'.$_POST['receiverMobilePhone2'].'" />
                 <input type="hidden" name="receiver_amount_in_local" value="'.$_POST['receiver_amount_in_local'].'">
                 <input type="hidden" name="receiverEmail"            value="'.$_POST['receiverEmail'].'">
                 <input type="hidden" name="itemIDNumber"             value="'.$_POST['itemIDNumber'].'">
                 <input type="hidden" name="error"                    value="'.$message.'">
                 </form>';

            echo'<script language="JavaScript" type="text/javascript">
                    window.onload=function(){ window.document.manual_send_auto_at_repost.submit(); }
                 </script>';
        }
    }
    else
    {
        print_r($_POST);
    }
?>