<?php
    include_once $_SERVER['DOCUMENT_ROOT']."/src/sendATSimpleVchr.php";

    session_start();

    if($_POST['Submit'])
    {
        $transactionArray = $_POST;
        $transactionArray = sendATSimpleVchr(COUNTRY_ABBREVIATION,$transactionArray);
        $success = $transactionArray['success'];
        $message = $transactionArray['message'];

        if($transactionArray['airtime_load_option'] == AIRTIME_LOAD_OPTION_AUTO)
        {
            $form_action = "new_at_send_at_auto.php";
        }
        else if($transactionArray['airtime_load_option'] == AIRTIME_LOAD_OPTION_VCHR)
        {
            $form_action = "new_at_send_voucher.php";
        }
        else
        {
            $form_action = "new_at_send_at.php";
        }

        if($success)
        {
            header("Location: ".$form_action);
        }
        else
        {
            //REPOST information back to form
            echo'<form action="'.$form_action.'" method="post" name="manual_send_auto_at_repost" id="manual_send_auto_at_repost">
                 <input type="hidden" name="transactionID"            value="'.$_POST['transactionID'].'">
                 <input type="hidden" name="clientEmail"              value="'.$_POST['clientEmail'].'" />
                 <input type="hidden" name="clientFirstName"          value="'.$_POST['clientFirstName'].'" />
                 <input type="hidden" name="receiverFirstName"        value="'.$_POST['receiverFirstName'].'" />
                 <input type="hidden" name="receiverPhone"            value="'.$_POST['receiverPhone'].'" />
                 <input type="hidden" name="phoneConfirm"             value="'.$_POST['phoneConfirm'].'" />
                 <input type="hidden" name="receiver_amount_in_local" value="'.$_POST['receiver_amount_in_local'].'">
                 <input type="hidden" name="receiverEmail"            value="'.$_POST['receiverEmail'].'">
                 <input type="hidden" name="itemIDNumber"             value="'.$_POST['itemIDNumber'].'">
                 <input type="hidden" name="error"                    value="'.$message.'">
                 </form>';

            echo'<script language="JavaScript" type="text/javascript">
                    window.onload=function(){ window.document.manual_send_auto_at_repost.submit(); }
                 </script>';
        }
    }
    else
    {
        print_r($_POST);
    }
?>