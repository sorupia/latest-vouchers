<?

    /* Render page */
    $values = array("receiverID","lastUpdated","receiverFirstName","receiverPhone","clientEmail","lastContacted","receiverStatus","comments");
    
    $result = render_pager($values, "Receivers", "lastUpdated DESC", 15, $countryAbbreviation);
    $exist = mysql_num_rows($result);
    
    /* Display page */
    if($exist)
    {
        echo'<thead>
            <tr>
            <th>Option</th>
            <th>Last Updated</th>
            <th>First Name</th>
            <th>Receiver Phone</th>
            <th>Client Email</th>
            <th>Last Contacted</th>
            <th>Receiver Status</th>
            <th>Comments</th>
            </tr>
        </thead>';

        while($data = mysql_fetch_array($result))
        {

            echo'<tbody><tr>
                <td>'.
                "<a href='receivers_edit_receiver.php?id=".$data['receiverID'].
                "&page=$thisPage' onfocus='this.blur();' title='Edit Receiver' onmouseover='tooltip(this);' name='Edit Receiver'>".
                "Edit</a> | "."<a href='#' onfocus='this.blur();' title='Delete Receiver' onclick=delEntry('".$data['receiverID'].
                "','Receivers','receiverID','$thisPage');>Delete</a>".
                '</td>
            <td>'.$data['lastUpdated'].'</td>
            <td>'.$data['receiverFirstName'].'</td>
            <td>'.$data['receiverPhone'].'</td>
            <td>'.$data['clientEmail'].'</td>
            <td>'.$data['lastContacted'].'</td>
            <td>'.$data['receiverStatus'].'</td>
            <td>'.$data['comments'].'</td>
            </tr></tbody>';
            
            $u++;
        }
    } 
    else
    {
        echo '<tr><td align="center" colspan="4">No receivers in database</td></tr>';
    }
?>
