<?

    //20101130: Created a function to enable the processing of PayPal reversal reports
    //and calculate the amount lost
    include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/SaveFile.php";

    function processReversalReport($fileArray, $paramsArray)
    {
        //Begin
        $start_time = time();
        
        $curr_line=0;
        $countryAbbreviation = $paramsArray['countryAbbreviation'];
        $debug = $paramsArray['debug'];
        $success = false;
        $num_with_airtime = 0;
        $num_no_airtime = 0;
        $duplicates = 0;
        $non_duplicates = 0;
        $num_chargebacks = 0;
        $totalAmountLost = 0;
        $cases_w_airtime_output = "";
        $cases_no_airtime_output = "";
        $duplicate_output = "";
        
        //indices
        $type_index = 0;
        $counter_party_index =1;
        $tx_id_index = 2;
        $date_index = 3;
        $amount_index = 4;
        $reason_index = 5;
        $filed_on_index = 6;
        $status_index = 7;
        $details_index = 8;
        $email_index = 9;
        
        //PayPal constant
        $pp_tx_fee_2 = 0.30;

        //Clickatel costs
        $cost_per_sms = PRICE_PER_SMS;
        
        //exchange rate
        $exchange_rate = $paramsArray['transfer_rate'];

        $casesWithAirtimeArray = array();
        $casesNoAirtimeArray = array();
        $resultArray = array();
        $data = array();
        $reversalReportArray=array();//stores the quantities for each denomination available
        $transactionIDArray = array();
        $fee_1 = array();
        $fee_1_populated = array();
        
        global $networkMap;
		global $cardMap;//help determine what index value is for a particular card
        $numberOfNetworks=count($networkMap[$countryAbbreviation]);
        
        //initialize array values to 0
        for($networkIndex=0;$networkIndex<$numberOfNetworks;$networkIndex++){
            $thisNetwork=$networkMap[$countryAbbreviation][$networkIndex];
            $numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);

            for($j=0;$j<$numberOfDenominations;$j++)
            {
                $thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
                $fee_1["$thisCard"] = 0;
                $fee_1_populated["$thisCard"] = false;
                
                //records for which we do not have a payment_fee in UsedCards
                $thisCard.="_0";
                $reversalReportArray["$thisCard"]['quantity'] = 0;
                $reversalReportArray["$thisCard"]['priceInLocal'] = 0;
                $reversalReportArray["$thisCard"]['totalPriceInLocal'] = 0;
                $reversalReportArray["$thisCard"]['profitInUSD'] = 0;
                $reversalReportArray["$thisCard"]['totalProfitInUSD'] = 0;
                $reversalReportArray["$thisCard"]['totalCardLoss'] = 0;

                //records for one of the payment fees in UsedCards
                $thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
                $thisCard.="_1";
                $reversalReportArray["$thisCard"]['quantity'] = 0;
                $reversalReportArray["$thisCard"]['priceInLocal'] = 0;
                $reversalReportArray["$thisCard"]['totalPriceInLocal'] = 0;
                $reversalReportArray["$thisCard"]['profitInUSD'] = 0;
                $reversalReportArray["$thisCard"]['totalProfitInUSD'] = 0;
                $reversalReportArray["$thisCard"]['totalCardLoss'] = 0;

                //records for the other payment fee in UsedCards
                $thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
                $thisCard.="_2";
                $reversalReportArray["$thisCard"]['quantity'] = 0;
                $reversalReportArray["$thisCard"]['priceInLocal'] = 0;
                $reversalReportArray["$thisCard"]['totalPriceInLocal'] = 0;
                $reversalReportArray["$thisCard"]['profitInUSD'] = 0;
                $reversalReportArray["$thisCard"]['totalProfitInUSD'] = 0;
                $reversalReportArray["$thisCard"]['totalCardLoss'] = 0;
            }
		}

        //read the first line
        $first_line_txt = $fileArray[$curr_line];
        $first_line     = explode(',',$fileArray[$curr_line++]);
        $column_0_txt   = trim($first_line[$type_index]);
		$column_1_txt   = trim($first_line[$counter_party_index]);
        $column_2_txt   = trim($first_line[$tx_id_index]);
		$column_3_txt   = trim($first_line[$date_index]);
        $column_4_txt   = trim($first_line[$amount_index]);
		$column_5_txt   = trim($first_line[$reason_index]);
        $column_6_txt   = trim($first_line[$filed_on_index]);
		$column_7_txt   = trim($first_line[$status_index]);
        $column_8_txt   = trim($first_line[$details_index]);
		$column_9_txt   = trim($first_line[$email_index]);
        
        //validate first line
        if($column_0_txt == "Type" &&
           $column_1_txt == "Counterparty" &&
           $column_2_txt == "Transaction ID" &&
           $column_3_txt == "Date" &&
           $column_4_txt == "Amount" &&
           $column_5_txt == "Reason" &&
           $column_6_txt == "Filed On" &&
           $column_7_txt == "Status" &&
           $column_8_txt == "Details" &&
           $column_9_txt == "Email")
        {
            $success = true;
            $output = "first line is valid hence rest of file in good format";
            if($debug) echo $output."<br>";
        }
        else
        {
            $success = false;
            $output.="first line is not valid hence not good file format<br>";
            $output.="Should be Type,Counterparty,Transaction ID,Date,Amount<br>";
            $output.="Instead is $first_line_txt <br>";
            if($debug) echo $output;
            $resultArray['success'] = 0;
            return $resultArray;
        }
        
        //count number of cards with airtime, count number of cards with no airtime
        if($success)
        {

            $connection=connect2DB($countryAbbreviation);

                //multiple queries as opposed to a single query mainly due to simplicity
                for( $i=0 ; 
			        ($i<(count($fileArray)-1)) && ($fileArray[$curr_line] != "") ; 
				     $i++)
                {
                    $current_csv_row = $fileArray[$curr_line++];
                    $current_row_array = explode(',',$current_csv_row);
                    if($debug){ print_r($current_row_array);echo "<br>";}

                    $transaction_id = trim($current_row_array[$tx_id_index]);
                    
                    //Count the number of chargebacks
                    $reversal_type = trim($current_row_array[$type_index]);
                    if($reversal_type == "Chargeback") $num_chargebacks++;
                
                    if(array_search($transaction_id, $transactionIDArray) === FALSE)
                    {
                        $transactionIDArray[$non_duplicates++] = $transaction_id;
                    
                        //does this transaction id exist in used cards?
                        $query ="SELECT * ";
                        $query.="FROM UsedCards ";
                        $query.="WHERE transactionID LIKE '%$transaction_id%'";

                        $result = mysql_query($query) or handleDatabaseError(''. mysql_error(),$query);
                        $data = @mysql_fetch_array($result);
                        $numberOfRecords = mysql_num_rows($result);
                        
                        
                        if($numberOfRecords == 1)
                        {
                            $itemID = $data['itemID'];
                            $ourPriceInUSD = $data['ourPriceInUSD'];
                            $priceInLocal = $data['valueInUGSh'];
                            $pp_tx_fee = $data['paymentFee'];

                            //echo"$itemID, $ourPriceInUSD, $priceInLocal <br>";                        

                            //calculate profit on international purchases of this card
                            $actual_price = $priceInLocal / $exchange_rate;
                            $pp_tx_fee_US = (PP_TX_PCT_US * $ourPriceInUSD);
                            $pp_tx_fee_int = (PP_TX_PCT_INT * $ourPriceInUSD);
                            
                            if($pp_tx_fee == 0)
                            {
                                if($debug) echo "No PayPal fee in table therefore calculate<br>";
                                $pp_tx_fee = $pp_tx_fee_2 + (($pp_tx_fee_US + $pp_tx_fee_int)/2);
                                $itemID.="_0";
                            }
                            else
                            {
                                if($fee_1["$itemID"] == 0 && !$fee_1_populated["$itemID"])
                                {
                                    $fee_1["$itemID"] = $pp_tx_fee;
                                    $fee_1_populated["$itemID"] = true;
                                    $itemID.="_1";
                                }
                                else if($fee_1["$itemID"] == $pp_tx_fee)
                                {
                                    $itemID.="_1";
                                }
                                else
                                {
                                    $itemID.="_2";
                                }
                            }

                            $cost_b4_markup_int = $actual_price + $pp_tx_fee + $cost_per_sms;
                            $gross_per_card_int = $ourPriceInUSD - $cost_b4_markup_int;

                            //amount in USD lost due to the loss of this card
                            $totalCardLoss = ($priceInLocal/$exchange_rate) + $gross_per_card_int;

                            if($debug) echo "$itemID : $pp_tx_fee <br>";

                            //populate results table
                            $reversalReportArray["$itemID"]['quantity']++;
                            $reversalReportArray["$itemID"]['priceInLocal'] = $priceInLocal;
                            $reversalReportArray["$itemID"]['totalPriceInLocal']+= $priceInLocal;
                            $reversalReportArray["$itemID"]['profitInUSD'] = $gross_per_card_int;
                            $reversalReportArray["$itemID"]['totalProfitInUSD']+= $gross_per_card_int;
                            $reversalReportArray["$itemID"]['totalCardLoss']+= $totalCardLoss;

                            $totalAmountLost+= $totalCardLoss;

                            $casesWithAirtimeArray[$num_with_airtime++] = $current_csv_row;
                            $output = "Transaction exists";
                            if($debug) echo $output."<br>";
                        }
                        else
                        {
                            $casesNoAirtimeArray[$num_no_airtime++] = $current_csv_row;
                            $output = "Transaction does not exist";
                            if($debug) echo $output."<br>";
                        }
                    }
                    else
                    {
                        //duplicate reversal records
                        $duplicateReversalsArray[$duplicates++] = $current_csv_row;
                        $output = "Transaction is a duplicate";
                        if($debug) echo $output."<br>";
                    }
                }

                mysql_free_result($result);
            disconnectDB($connection);

            if($debug){
                $output = "Number of reversed transactions with airtime: ".$num_with_airtime;
                echo "<br>".$output."<br>";
                echo "Cases with airtime<br>";
            }

            for($i = 0; $i < count($casesWithAirtimeArray); $i++)
            {
                $cases_w_airtime_output.= $casesWithAirtimeArray[$i];
                
                if($debug) echo $casesWithAirtimeArray[$i]."<br>";
            }

            if($debug)
            {
                $output = "Number of reversed transactions without airtime: ".$num_no_airtime;
                echo "<br>".$output."<br>";
            }

            for($i = 0; $i < count($casesNoAirtimeArray); $i++)
            {
                $cases_no_airtime_output.= $casesNoAirtimeArray[$i];
                
                if($debug) echo $casesNoAirtimeArray[$i]."<br>";
            }

            if($debug)
            {
                $output = "Number of chargebacks: ".$num_chargebacks;
                echo "<br>".$output."<br>";
                
                $output = "Report Array <br>";

                echo $output;
                print_r($reversalReportArray);
                echo "<br>";
            }

            if($debug)
            {
                $output = "Number of duplicate entries: ".$duplicates;
                echo "<br>".$output."<br>";
            }

            for($i = 0; $i < count($duplicateReversalsArray); $i++)
            {
                $duplicate_output.= $duplicateReversalsArray[$i];
                
                if($debug) echo $duplicateReversalsArray[$i]."<br>";
            }            
        }

        //print report
        if($success)
        {
            $reversals_start_date = $paramsArray['reversals_start_date'];
            $reversals_stop_date = $paramsArray['reversals_stop_date'];
            $profit_start_date = $paramsArray['profit_start_date'];
            $profit_end_date = $paramsArray['profit_end_date'];
            $week_profit = $paramsArray['week_profit'];
            $week_expenses = $paramsArray['week_expenses'];
            
            //print heading
            $report_output = "Transfer Rate:,".$exchange_rate.",";
            $report_output.= "Effect of reversals filed ";
            $report_output.= "$reversals_start_date to $reversals_stop_date ";
            $report_output.= "on sales $profit_start_date to $profit_end_date\n";
            
            $report_output.="Type,Quantity Lost,Card Price in GHC,Total Price of Cards GHC,";
            $report_output.="USD Profit Lost per card,Total Profit Lost,Total Loss on Cards\n";
            
            //print table
            for($networkIndex=0;$networkIndex<$numberOfNetworks;$networkIndex++)
            {
                $thisNetwork=$networkMap[$countryAbbreviation][$networkIndex];
                $numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);

                for($j=0;$j<$numberOfDenominations;$j++)
                {
                    //print each row
                    $thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
                    $itemID=$thisCard."_0";
                    $report_output.=$itemID.",";
                    $report_output.=$reversalReportArray["$itemID"]['quantity'].",";
                    $report_output.=$reversalReportArray["$itemID"]['priceInLocal'].",";
                    $report_output.=$reversalReportArray["$itemID"]['totalPriceInLocal'].",";
                    $report_output.=round($reversalReportArray["$itemID"]['profitInUSD'],2).",";
                    $report_output.=round($reversalReportArray["$itemID"]['totalProfitInUSD'],2).",";
                    $report_output.=round($reversalReportArray["$itemID"]['totalCardLoss'],2)."\n";

                    $itemID=$thisCard."_1";
                    $report_output.=$itemID.",";
                    $report_output.=$reversalReportArray["$itemID"]['quantity'].",";
                    $report_output.=$reversalReportArray["$itemID"]['priceInLocal'].",";
                    $report_output.=$reversalReportArray["$itemID"]['totalPriceInLocal'].",";
                    $report_output.=round($reversalReportArray["$itemID"]['profitInUSD'],2).",";
                    $report_output.=round($reversalReportArray["$itemID"]['totalProfitInUSD'],2).",";
                    $report_output.=round($reversalReportArray["$itemID"]['totalCardLoss'],2)."\n";

                    $itemID=$thisCard."_2";
                    $report_output.=$itemID.",";
                    $report_output.=$reversalReportArray["$itemID"]['quantity'].",";
                    $report_output.=$reversalReportArray["$itemID"]['priceInLocal'].",";
                    $report_output.=$reversalReportArray["$itemID"]['totalPriceInLocal'].",";
                    $report_output.=round($reversalReportArray["$itemID"]['profitInUSD'],2).",";
                    $report_output.=round($reversalReportArray["$itemID"]['totalProfitInUSD'],2).",";
                    $report_output.=round($reversalReportArray["$itemID"]['totalCardLoss'],2)."\n";
                }
            }

            //print total number
            $report_output.= "Total Number,$num_with_airtime,,,,Total Amount Lost,$totalAmountLost\n";
            $report_output.= "Number of Chargebacks,$num_chargebacks\n";
            $report_output.= "Number without airtime,$num_no_airtime\n";
            $report_output.= "Duplicate records,$duplicates\n";
            
            //calculate amount payable to 3nitylabs
            $report_output.= "\n";
            $report_output.= "Weeks Profit ";
            $report_output.= "$profit_start_date to $profit_end_date,";
            $report_output.= "$week_profit\n";
            
            $report_output.= "Weeks Expenses,";
            $report_output.= "$week_expenses\n";
            
            $report_output.= "Total Amount Lost From Cards,";
            $report_output.= round($totalAmountLost,2)."\n";
            
            $totalChargeback = $num_chargebacks * 10;
            $report_output.= "Total Chargeback,";
            $report_output.= "$totalChargeback\n";
            
            $newNetProfit = $week_profit - $week_expenses - $totalAmountLost - $totalChargeback;
            $report_output.= "New Net Profit,";
            $report_output.= round($newNetProfit,2)."\n";
            
            $payable_amount = $newNetProfit/4.0;
            $report_output.= "3nitylabs amount,";
            $report_output.= round($payable_amount,2)."\n";
            
            if($debug) echo "File Output: ".$report_output."<br>";
            
            $report_output.= "\n\nCases With Airtime\n";
            $report_output.= $fileArray[0];
            $report_output.= $cases_w_airtime_output;
            $report_output.= "\n\nCases With No Airtime\n";
            $report_output.= $fileArray[0];
            $report_output.= $cases_no_airtime_output;
            $report_output.= "\n\nDuplicate Records\n";
            $report_output.= $fileArray[0];
            $report_output.= $duplicate_output;
            
            if(!$debug)
            {
                $inputParams['file_name'] = "reversal_report.csv";
                $inputParams['file_text'] = $report_output;
                saveFile($inputParams);
            }
        }
        
        $end_time = time();
        $execution_time = $end_time - $start_time;
        if($debug) echo "Function took ".$execution_time." seconds<br>";
        
        //exiting processReversalReport
    }
?>