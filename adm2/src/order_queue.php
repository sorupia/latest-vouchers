<?
	include "wrapper/header.php";
?>

<table cellpadding="0" cellspacing="0" class="listing">
	<tr>
		<th colspan="6"><h1>Order Queue - Purchases for which payment has not been verified</h1></th>
	</tr>
<?
	include "out_queue_nav.php";
?>
</table>

<table cellpadding="0" cellspacing="0" class="listing">
<?
	$values   = array("order_id",
                      "session_id",
					  "date_added",
					  "receiver_first_name",
					  "receiver_phone",
					  "receiver_email",
					  "receiver_network_id",
					  "item_id",
                      "button_id",
					  "ip_country",
					  "ip_state",
					  "ip_city",
					  "ip_address");
	
	$result = render_pager($values, "Ordered_Cart_Items", "date_added DESC", 15, $countryAbbreviation, 'airtime');
	$exist = mysql_num_rows($result);
	
	if($exist)
	{
		echo'<thead>
			<tr>
			<th>Option</th>
			<th>Date Added</th>
			<th>Order ID</th>
			<th>Session ID</th>
			<th>Receiver Phone</th>
			<th>Item ID</th>
			<th>Receiver Network</th>
			<th>Receiver</th>
			<th>Receiver Email</th>
			<th>IP Country</th>
			<th>IP State</th>
			<th>IP City</th>
			<th>IP Address</th>
			</tr>
		</thead>';
						
		while($data = mysql_fetch_array($result))
		{
			
			echo'<tbody><tr>
				<td>'.
				"<a href='verify_order.php?id=".$data['order_id'].
				"&page=$thisPage' onfocus='this.blur();' title='Verify Order'>".
				"verify</a> | ".
				"<a href='#' onfocus='this.blur();' title='Delete Order' onclick=delEntry('".$data['order_id'].
				"','Ordered_Cart_Items','order_id','$thisPage');>".
                "delete</a>".
			'</td>
			<td>'.$data['date_added'].'</td>
			<td>'.$data['order_id'].'</td>
			<td>'.$data['session_id'].'</td>
			<td>'.$data['receiver_phone'].'</td>
			<td>'.$data['item_id'].'</td>
			<td>'.$data['receiver_network_id'].'</td>
			<td>'.$data['receiver_first_name'].'</td>
			<td>'.$data['receiver_email'].'</td>
			<td>'.$data['ip_country'].'</td>
			<td>'.$data['ip_state'].'</td>
			<td>'.$data['ip_city'].'</td>
			<td>'.$data['ip_address'].'</td>
			</tr></tbody>';
			
			$u++;
		}
	}
	else
	{
		echo '<tr><td align="center" colspan="4">No orders in queue</td></tr>';
	}
?>

</table>

<?
	include "wrapper/footer.php";
?>