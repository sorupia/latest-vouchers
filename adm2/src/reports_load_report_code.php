<?
/*******************************************************************************
**  FILE: reports_load_report.php
**
**  FUNCTION: reports_load_report
**
**  PURPOSE: A load report requested by GhanaAirTime to list cards loaded on a 
**           particular day. This report will also be exportable to csv and xls formats.
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.10.24
*********************************************************************************/

	$report_type = $_POST['report_type'];
	
	$authenticated = false;
    
    if($_POST['Generate'])
    {
        $username = $_POST['employee_username'];
        $password = $_POST['employee_password'];

        $authenticated = authenticateEmployee($username, $password, $countryAbbreviation);

		$_SESSION['ViewLoadReportUsername'] = $username;
    }

	if($_POST['Generate'] && $authenticated)
	{
		$debug = false;
		
		$exchange_rate = $_POST['ex_rate'] * 0.947; 
		$start_date    = $_POST['start_date'].':01';
		$end_date      = $_POST['end_date'].':59';
        $item_id       = $_POST['itemID'];

        $input_array['country_abbreviation'] = $countryAbbreviation;
        $input_array['start_date']           = $start_date;
        $input_array['end_date']             = $end_date;
        $input_array['itemID']               = $item_id;
        $input_array['username']             = $username;

        $connection                = connect2DB2($countryAbbreviation);
        $input_array['connection'] = $connection;

		if($report_type=='load_report')
		{
			echo"Load Report by Date of Purchase: <br><b> From $start_date to $end_date </b><br>";
            echo"Item ID: $item_id<br>";

            echo'<form name="load_report_form" id="form1" method="post" action="export_load_report.php">';
                echo'<input name="start_date" type="hidden" value="'.$start_date.'" />';
                echo'<input name="end_date" type="hidden" value="'.$end_date.'" />';
                echo'<input name="itemID" type="hidden" value="'.$item_id.'" />';
                echo'<input name="username" type="hidden" value="'.$username.'" />';
                echo'<p> ';
                    echo'<input name="csv_file" type="submit" value="export as csv" />';
                    echo'<input name="xls_file" type="submit" value="export as xls" />';
                echo'</p>';
            echo'</form>';

            $input_array['use_echo'] = 1;
            $allCardsArray = query_cards_for_load_report($input_array);

            if( count($allCardsArray) > 0 )
            {
                /* Display results remembering that the rows are columns and vice versa*/
                displayAllCardsReport($allCardsArray);
            }
            else
            {
                echo"<br><br> No records please check dates entered";
            }

            //log the view to email
            if(NOTIFY_VIEW_OF_REPORTS_ENABLED)
            {
                notify_view_of_load_report($_POST);
            }
		}
        disconnectDB($connection);
	}
	elseif(!$authenticated && $_POST['Generate'])
	{
		echo "<br><br> Invalid username and password";
	}
?>