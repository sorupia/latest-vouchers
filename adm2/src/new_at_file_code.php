<td align="left">
    <p>Please note that MTN has a special format for the airtime that they provide in files. For the rest of the networks simply create a text file that starts
    with the first line as 'airtime' or 'airtime,serial'. In the lines that follow insert either airtime access number only or airtime and serial numbers separated 
    with commas.</p>
		<form action="new_at_file.php" method="post" enctype="multipart/form-data" name="loadFileForm" id="loadFileForm">
		  <h3>1. Select file type</h3>
		  <p>File Type:
			  <select name="fileType" size="1" id="fileType">
                    <option value="ugmtncard-CRYPT"     selected="selected">ugmtncard-CRYPT</option>
                    <option value="ugmtncard-CRYPT-5k"  selected="selected">ugmtncard-CRYPT-5k</option>
                    <option value="ugmtncard-CRYPT-10k" selected="selected">ugmtncard-CRYPT-10k</option>
                    <option value="ugmtncard-CRYPT-20k" selected="selected">ugmtncard-CRYPT-20k</option>
				<?php
					include_once "itemIDDropDown.php";
					getDropDown($countryAbbreviation);
				?>
                <option value="none" selected="selected">none</option>
			  </select>
		  </p>
		  <h3> 2. Enter number of digits of this card type</h3>
		  <p>
			Card Length:
			  <input name="cardLengthField" type="text" id="cardLengthField" size="3" maxlength="3" />
		  </p>
		  <h3> 3. Select the  airtime file below</h3>
		  <p>
			<input type="hidden" name="MAX_FILE_SIZE" value="100000" />
		    Airtime File:
		    <input name="airtime_file" type="file" id="airtime_file">
		  </p>
		  <h3>4. Enter your credentials </h3>
		  <p>
			  Username:
			  <input name="usernameField" type="text" id="usernameField" size="12" maxlength="15" />
		  </p>
		  <p>
		      Password:
			  <input name="passwordField" type="password" id="passwordField" size="12" maxlength="15" />
		  </p>
		  <!--
		  <p>
			<textarea name="fileContents" cols="61" rows="25" id="fileContents"></textarea>
		  </p>
		  -->

		  <p>
		    <input type="submit" name="Submit" value="Load Airtime" />
	      </p>
	  </form>
</td>