<?
    // Modified: Jan.10.2010 by antozi, modified file format to be airtime,serial

	require_once($_SERVER['DOCUMENT_ROOT'].'/src/GeneralFunctions.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/SecurityFunctions.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/SessionFunctions.php');
	require_once('insertATArray.php');
	require_once('checkCardLength.php');
	
	function loadCSVATFromFile($file, $username, $password, $itemIDNumber, $cardLength)
	{
		//BEGIN
		$testing = false; //IMPORTANT
		
		$network_pin_length=13;
		$curr_line=0;
		$success = false;
		$report_problem = true;
		$error_report = "ERROR REPORT: \n <br>";
		$error_report.= "============= \n <br>";
		$report_success = true;
		$load_report = "LOAD REPORT: \n";
		$load_report.= "============= \n";
		$debug = false;
		$error_detected = false;
		$number_of_columns = 0;
		
		$countryAbbreviation = substr($itemIDNumber,0,2);

		$networkName = getNetworkName($itemIDNumber);
		echo "<br><br>";
		echo "Network Name: ".$networkName."<br>";
		echo "Item ID Number: ".$itemIDNumber."<br>";

		$pinNumbers = array(); //stores pin number
		
		//Authenticate Airtime Loader
		$success=authenticateEmployee($username, $password, $countryAbbreviation);
	
	
		//verify item_id_number	
		if($success && isItemIDValid($itemIDNumber,$countryAbbreviation))
		{
			$ourPriceInUSD = getPriceInUSD($itemIDNumber);
			$valueInUGSh = getValueInLocal($itemIDNumber);
			$output = "Item ID: $itemIDNumber \n";
			$load_report.= $output;
			if($debug) echo $output."<br>";
		}
		else
		{
			$success = false;
			$output = "ERROR: Invalid Item ID \n";
			$error_report.= $output;
			$error_detected = true;
			echo $output."<br>";
		}

		//Begin file extraction
		//$file[0] = "airtime,serial";
		//$file[1] = "40000300501698,21255287184173";
		
		//verify the first line i.e. airtime,serial
		$first_line = explode(',',$file[$curr_line++]);
		$column_one_txt = trim($first_line[0]);
		$column_two_txt = trim($first_line[1]);
		$output = "first_line[0]: ".$column_one_txt."<br>";
		$output.= "first_line[1]: ".$column_two_txt."<br>";
		$error_report.= $output;
		
		if( $success && 
		   $column_one_txt == "airtime" && 
		   $column_two_txt == "serial")
		{
			$output = "Read first line successfully: ";			
			$output.= $first_line[0].",";
			$output.= $first_line[1]."\n";
			$number_of_columns = 2;
			
			$load_report.= $output;
			echo $output."<br>";
			
		}
		elseif( $success && 
		        $column_one_txt == "airtime")
		{
			$output = "Read first line successfully and it only contains the format: ";			
			$output.= $first_line[0];
			$number_of_columns = 1;
			
			$load_report.= $output;
			echo $output."<br>";
		}
		else{
			$success = false;
			echo "<br>";
			$output = "***ERROR: First line should contain the text: airtime,serial <br> \n";
			$output.= "***ERROR: or should contain the text: airtime <br> \n";
			$output.= "***ERROR: instead it contains ".$first_line[0].",".$first_line[1]." <br> \n";
			$error_report.= $output;
			$error_detected = true;
			echo $output."<br>";
		}
		
		//read serial number and airtime from file line
		//40000300501698,21255287184173
		if( $success && 
		    $number_of_columns == 2)
		{
			for( $i=0 ; 
			     ( $i<(count($file)-1) ) && ( $file[$curr_line] != "" ) ; 
				 $i++)
			{
				$airtime_card = explode(',', $file[$curr_line++]);
				$pinNumbers[$i][0] = trim($airtime_card[0]);
				$pinNumbers[$i][1] = trim($airtime_card[1]);
				$output = "Airtime Access#: ".$pinNumbers[$i][0]."\n";
				echo "<br>";
				$output.= "Airtime Serial#: ".$pinNumbers[$i][1]."\n";
				$load_report.= $output;
				echo $output."<br><br>";
			}
		}
		elseif( $success && 
		        $number_of_columns == 1)
		{
			for( $i=0 ; 
			     ( $i<(count($file)-1) ) && ( $file[$curr_line] != "" ) ; 
				 $i++)
			{
				//since its a single column file, there is no need to explode the line
				//set the serial number to 0
				$pinNumbers[$i][0] = trim($file[$curr_line++]);
				$pinNumbers[$i][1] = 0;
				$output = "Airtime Access#: ".$pinNumbers[$i][0]."\n";
				echo "<br>";
				$output.= "Airtime Serial#: ".$pinNumbers[$i][1]."\n";
				$load_report.= $output;
				echo $output."<br><br>";
			}
		}

        //call function to validate the card length
		$resultArray = checkCardLength($pinNumbers, $cardLength);
		$success = $resultArray[0];
		$load_report.= $resultArray[1];
		
		echo "<br>";	
		
		//Read "End of file"
		if( $success )
		{
			$output = "End of File successfully reached --- <br>\n";
			$load_report.= $output;
			echo $output."<br>";
			
			//todo: Send email to cardadmin about new load		
		}
		else
		{
			$output = "FAILURE: Did not reach end of file, possible incomplete file <br>\n";
			$error_report.= $output;
			$error_detected = true;
			echo $output."<br>";
		}
		//END FILE DECODE

        //TODO: Put duplicate checker here
        for( $i = 0; $i < count ($pinNumbers) ; $i++ )
        {
           $key1 = $pinNumbers[$i][0];

           for( $j = $i + 1 ; $j < count ($pinNumbers) ; $j++ )
           {
                $key2 = $pinNumbers[$j][0];

                if( $key1 == $key2 )
                {
                    $is_unique = false;
                    $success = false;
                    echo $key2."has duplicates<br>";
                }
           }
        }

		//$success = true; //todo:remove 
		//Create and Execute SQL queries
		if($success)
		{
			$myIP=getIP();
			$dateWePurchased = date("Y-m-d H:i:s");
			
			$result = insertATArray($testing,
                          			$countryAbbreviation,
								  	$pinNumbers,
								  	$error_detected,
								  	$error_report,
								  	$load_report,
								  	$networkName,
								  	$valueInUGSh,
								  	$ourPriceInUSD,
								  	$dateWePurchased,
								  	$itemIDNumber,
								  	$username,
								  	$myIP);
			
			$error_detected = $result['error_occured'];
			$error_report = $result['error_info'];
			$load_report  = $result['load_info'];
			$db_stats = $result['db_stats'];
			
			//report duplicates to screen & admin email
			if($report_problem && $error_detected)
			{
				$error_report = "Loaded By: ".$username."\n".$load_report."\n".$error_report;
				reportError($error_report,"loadCSVATFromFile()");
				echo $error_report."<br>";
				echo "Error report sent to admin<br>";
			}
			else if($report_success)
			{
				$load_report = "Loaded By: ".$username."\n".$load_report;
				sendEmail("Sendairtime Web",FROM_EMAIL,SUPER_TECH_ADMIN,LOADER_EMAIL,"-loadCSVATFromFile Report-",$load_report);			
				echo $db_stats."<br>";
                if($debug) echo "Load report sent to admin<br>";
			}
			
		}
		else{
		    $output = "NO RECORD INSERTED INTO DATABASE DUE TO ERRORS! <br>";
			$load_report.= $output;
			echo "NO RECORD INSERTED INTO DATABASE DUE TO ERRORS! <br>";
		    
			if($report_problem)
			{
				$load_report = "Loaded By: ".$username."\n".$load_report;
				sendEmail("Sendairtime Web",FROM_EMAIL,LOADER_EMAIL,SUPER_TECH_ADMIN,"-loadCSVATFromFile Report-",$load_report);			
				if($debug) echo "Load report sent to admin<br>";
			}
		}
			
		//Write to file and save on server
		if($debug) echo "End of Function <br>";
	}
	
	
?>