<?
    include_once $_SERVER['DOCUMENT_ROOT']."/adm2/src/processReversalReport.php";
    $debug = false;
    $keep_file_record = false;
    
    $reversals_file = $_POST['reversals_file'];
	$transfer_rate = $_POST['transfer_rate'];
	$reversals_start_date = $_POST['reversals_start_date'];
	$reversals_stop_date = $_POST['reversals_stop_date'];
	$profit_start_date = $_POST['profit_start_date'];
	$profit_end_date = $_POST['profit_end_date'];
	$week_profit = $_POST['week_profit'];
	$week_expenses = $_POST['week_expenses'];
	
    $temp_file_path = $_FILES["reversals_file"]["tmp_name"];
    $fileArray = explode("\n",$fileContents);
    
	/* Display our input params */
    if($debug)
    {
        echo "Transfer rate: ".$transfer_rate."<br>";
        echo "Reversal start date: ".$reversals_start_date."<br>";
        echo "Reversal stop date: ".$reversals_stop_date."<br>";
        echo "Profit start date: ".$profit_start_date."<br>";
        echo "Profit end date: ".$profit_end_date."<br>";
        echo "Week's profit: ".$week_profit."<br>";
        echo "Week's expenses: ".$week_expenses."<br>";
    }
	
    /* Read file contents into an array */
	$fileArray = file($temp_file_path,FILE_SKIP_EMPTY_LINES);
    
    /* DEBUG */
    if($debug)
    {
        echo "temp file path: $temp_file_path <br>";
        echo "Array Contents:";	
    }
    //if($debug) print_r($fileArray);
    
    /* Move file to a location for record storage */
	if($keep_file_record)
	{
		$target_path = "records/";
		$target_path = $target_path.basename($_FILES["reversals_file"]["name"]);
	
		if(move_uploaded_file($_FILES['reversals_file']['tmp_name'], $target_path)) {
			echo "The file ".  basename( $_FILES['reversals_file']['name'])." has been uploaded";
		}
		else{
			echo "There was an error uploading the file, please try again!";
		}
	}
    
    $paramsArray['debug'] = $debug;
	$paramsArray['transfer_rate'] = $transfer_rate;
	$paramsArray['reversals_start_date'] = $reversals_start_date;
	$paramsArray['reversals_stop_date'] = $reversals_stop_date;
	$paramsArray['profit_start_date'] = $profit_start_date;
	$paramsArray['profit_end_date'] = $profit_end_date;
	$paramsArray['week_profit'] = $week_profit;
	$paramsArray['week_expenses'] = $week_expenses;
    $paramsArray['countryAbbreviation'] = "gh";
	
    processReversalReport($fileArray, $paramsArray);
?>