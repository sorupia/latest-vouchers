<?
	$values   = array("smsUserID",
	                  "registrationDate",
					  "firstName",
					  "lastName",
					  "emailAddress",
					  "cellphone",
					  "addressStreet",
					  "addressCity",
					  "addressState",
					  "addressCountry",
					  "signUpIP");
	
	$result = render_pager($values,
	                       "SMSUsers",
	                       "registrationDate DESC", 
						   15, 
						   $countryAbbreviation, 
						   'sms', 
						   subscriberStatus, 
						   "'UNVERIFIED'");

	$exist = mysql_num_rows($result);
	
	if($exist)
	{
		echo'<thead>
			<tr>
			<th>Option</th>
			<th>Registration Date</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Street</th>
			<th>City</th>
			<th>State</th>
			<th>Country</th>
			<th>Sign Up IP</th>
			</tr>
		</thead>';
						
		while($data = mysql_fetch_array($result))
		{
			
			echo'<tbody><tr>
				<td>'.
				"<a href='clients_edit_client.php?id=".$data['smsUserID'].
				"' onfocus='this.blur();' title='Edit User' onmouseover='tooltip(this);' name='Edit Client' >".
				"Edit</a> | ".
				"<a href='clients_delete_client.php?id=".$data['smsUserID']."' onfocus='this.blur();' 
				title='Delete User' onmouseover='tooltip(this);' name='Delete User'>Delete</a>".'</td>
			<td>'.$data['registrationDate'].'</td>
			<td>'.$data['firstName'].'</td>
			<td>'.$data['lastName'].'</td>
			<td><a href="mailto:'.$data['emailAddress'].
			    '" title="'.$data['emailAddress'].
			    '">'.$data['emailAddress'].'</a></td>
			<td>'.$data['cellphone'].'</td>
			<td>'.$data['addressStreet'].'</td>
			<td>'.$data['addressCity'].'</td>
			<td>'.$data['addressState'].'</td>
			<td>'.$data['addressCountry'].'</td>
			<td>'.$data['signUpIP'].'</td>
			</tr></tbody>';
			
			$u++;
		}
	}
	else
	{
		echo '<tr><td align="center" colspan="4">No users in database</td></tr>';
	}
?>