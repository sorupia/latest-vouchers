<?
	require_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/NewCardsTable.php";
	include_once "itemIDDropDown.php";
	
    $authenticated = false;
    $u = isset($u) ? $u : 0;
    
    if(array_key_exists("submit",$_POST) && $_POST['submit'] == "view")
    {
        $username = $_POST['employee_username'];
        $password = $_POST['employee_password'];
		$itemID = $_POST['itemID'];

        $authenticated = authenticateEmployee($username, $password, $countryAbbreviation);

		$_SESSION['ViewNewAirtimeType'] = true;
		$_SESSION['ViewNewATUsername'] = $username;
		$_SESSION['ViewNewATItemID'] = $itemID;
    }
    
    if($authenticated && array_key_exists("submit",$_POST) &&  $_POST['submit'] == "view" || $_SESSION['ViewNewAirtimeType'])
    {
   		$username = $_SESSION['ViewNewATUsername'];
		$itemID = $_SESSION['ViewNewATItemID'];
		logView($countryAbbreviation,$username);
		$thisPage = substr($page,0,-4);
		
    
        $values   = array("cardID","dateWePurchased","cardPIN","itemID","ourPriceInUSD","cardStatus","loadedBy","loadersIP","serialNumber");
        
        $result = render_pager($values, "NewCards", "dateWePurchased DESC", 15, $countryAbbreviation, 'airtime','itemID',"'$itemID'");
        $exist = mysql_num_rows($result);
        
        if($exist)
        {
            echo'<thead>
                <tr>
                <th>Option</th>
                <th>Purchase Date</th>
                <th>Card PIN</th>
                <th>Item ID</th>
                <th>Price In USD</th>
                <th>Card Status</th>
                <th>Loaded By</th>
                <th>Loaders IP</th>
                <th>Serial Number</th>
                </tr>
            </thead>';
                            
            while($data = mysql_fetch_array($result))
            {
                
                echo'<tbody><tr>
                    <td>'.
                    "<a href='edit_card.php?id=".$data['cardID']."&page=$thisPage' onfocus='this.blur();' title='Edit Card'>".
                    "Edit</a> | ".
                    "<a href='delete_card.php?id=".$data['cardID']."&page=$thisPage' onfocus='this.blur();' title='Delete Card'>".
                    "Delete</a>".'					
					</td>
                <td>'.$data['dateWePurchased'].'</td>
                <td>'.$data['cardPIN'].'</td>
                <td>'.$data['itemID'].'</td>
                <td>'.$data['ourPriceInUSD'].'</td>
                <td>'.$data['cardStatus'].'</td>
                <td>'.$data['loadedBy'].'</td>
                <td>'.$data['loadersIP'].'</td>
                <td>'.$data['serialNumber'].'</td>
                </tr></tbody>';
                
                $u++;
            }
        }
        else
        {
            echo '<tr><td align="center" colspan="4">No records found</td></tr>';
        }
    }
    else if(array_key_exists("submit",$_POST) && $_POST['submit'] == "view")
    {
        echo "Wrong username and Password";
    }
	else
	{
		//print_r($_POST);
		echo'<form name="new_at_login" id="new_at_login" method="post" enctype="multipart/form-data" action="">
		  <p>';
		echo'<select name="itemID" id="itemID">';
		getDropDown($countryAbbreviation);
		echo'</select> <br/><br/>';
		echo'	Employee Username 
				<input name="employee_username" type="text" class="field_small" id="employee_username" value="" /> 
				&nbsp;
				Employee Password 
				<input name="employee_password" type="password" class="field_small" id="employee_password" value="" /> 
				<input type="submit" name="submit" id="submit" value="view" class="submit" onfocus="this.blur();" />
		  </p>
		</form>';
	}
?>