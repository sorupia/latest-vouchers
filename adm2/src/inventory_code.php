<?php

	global $networkMap;
	global $networkArray;
	global $cardMap;//help determine what index value is for a particular card
	$quantityOfCardsArray=array();//stores the quantities for each denomination available
	$numberOfNetworks=count($networkMap[$countryAbbreviation]);
    $totalPriceInLocal = 0;

	//draw table
	echo"<table class=\"tbl\" width=\"55%\" align=\"center\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" bordercolor=\"#000000\">";
  	echo"<tr bgcolor=\"#F0F0F0\">
    	<th width=\"25%\" scope=\"col\">Card</th>
    	<th width=\"25%\" scope=\"col\">Cards Left </th>
		<th width=\"25%\" scope=\"col\">Cards Sold Todate </th>
		<th width=\"25%\" scope=\"col\">Value of Cards Left </th>
  		</tr>";	
	
	//connect to DB
	$connection=connect2DB($countryAbbreviation);
			
			$totalNumber=0;
			$totalSold=0;
			$thisTotalPriceInLocal = 0;
			for($networkIndex=0;$networkIndex<$numberOfNetworks;$networkIndex++){
				
				$thisNetwork=$networkMap[$countryAbbreviation][$networkIndex];
				$numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);
				
				for($j=0;$j<$numberOfDenominations;$j++){
					$thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
					$oneQuery = sprintf ("SELECT itemID FROM NewCards WHERE NewCards.itemID = '$thisCard' AND NewCards.cardStatus != 'USED'");
					$soldQuery = sprintf ("SELECT itemID FROM UsedCards WHERE UsedCards.itemID = '$thisCard'");
					$oneResult = mysql_query($oneQuery) or handleDatabaseError('' . mysql_error(),$oneQuery);
					$soldResult = mysql_query($soldQuery) or handleDatabaseError('' . mysql_error(),$soldQuery);
					$quantityOfCardsArray["$thisCard"] = mysql_num_rows($oneResult);
					$thisSoldCards = mysql_num_rows($soldResult);
					$totalNumber+=$quantityOfCardsArray["$thisCard"];
					
					$totalSold+= $thisSoldCards;
					$cardValue = getValueInLocal($thisCard);
					$thisTotalPriceInLocal = $cardValue*$quantityOfCardsArray["$thisCard"];
					$totalPriceInLocal+= $thisTotalPriceInLocal;
					
					echo"<tr><td>".$thisCard."</td><td>".$quantityOfCardsArray["$thisCard"]."</td><td>$thisSoldCards</td><td>$thisTotalPriceInLocal</td></tr>";
					
					mysql_free_result($oneResult);
					mysql_free_result($soldResult);
				}
				//echo"<tr><td></td><td></td><td></td><td></td></tr>";
			}
			echo"<tr><td><strong>TOTAL</strong></td><td><strong>$totalNumber</strong></td><td><strong>$totalSold</strong></td><td><strong>$totalPriceInLocal</strong></td></tr>";
			echo"</table>";
			
	//disconnect from DB
	disconnectDB($connection);
	
?>