<? include_once $_SERVER['DOCUMENT_ROOT']."/src/constants.php"; ?>

<form action="src/reports_reversal_effect_action_code.php" method="post" enctype="multipart/form-data" name="reversal_report_form" id="loadFileForm">
    <h3>1. Enter money transfer rate </h3>
    <p>Money transfer rate 
      <input name="transfer_rate" type="text" id="transfer_rate" value="<?=MONEY_TRANSFER_RATE?>" />
    </p>
    <h3>2. Date range for reversals filings </h3>
    <p>Start date 
      <input name="reversals_start_date" type="text" id="reversals_start_date" />
	  <a href="#" title="Add Date" onclick="callDate('1', 'reversal_report_form', 'reversals_start_date', this, event);" 
							onfocus="this.blur();"><img src="images/icons/calendar.gif" alt="Add Date" class="datefieldicon" 
							onmouseover='tooltip(this);' name='Open calendar tool' /></a>
    </p>
    <p>End date 
      <input name="reversals_stop_date" type="text" id="reversals_stop_date" />
	  <a href="#" title="Add Date" onclick="callDate('1', 'reversal_report_form', 'reversals_stop_date', this, event);" 
							onfocus="this.blur();"><img src="images/icons/calendar.gif" alt="Add Date" class="datefieldicon" 
							onmouseover='tooltip(this);' name='Open calendar tool' /></a>
    </p>
    <h3>3. These reversals affect the profits of week </h3>
    <p>Start date
      <input type="text" name="profit_start_date" id="profit_start_date"/>
	  <a href="#" title="Add Date" onclick="callDate('1', 'reversal_report_form', 'profit_start_date', this, event);" 
							onfocus="this.blur();"><img src="images/icons/calendar.gif" alt="Add Date" class="datefieldicon" 
							onmouseover='tooltip(this);' name='Open calendar tool' /></a>
    </p>
    <p>End date
	  <input type="text" name="profit_end_date" id="profit_end_date"/>
	  <a href="#" title="Add Date" onclick="callDate('1', 'reversal_report_form', 'profit_end_date', this, event);" 
							onfocus="this.blur();"><img src="images/icons/calendar.gif" alt="Add Date" class="datefieldicon" 
							onmouseover='tooltip(this);' name='Open calendar tool' /></a>
    </p>
    <h3>4. Enter week's profit</h3>
    <p>Week's profit 
      <input type="text" name="week_profit" />
    </p>
    <h3>5. Enter week's expenses </h3>
    <p>Week's expenses 
      <input type="text" name="week_expenses" />
    </p>
    <h3>6. Select the  PayPal reversals file below</h3>
    <p>
        <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
        PayPal reversals file:
        <input name="reversals_file" type="file" id="reversals_file">
    </p>
    <p>
        <input type="submit" name="Submit" value="Reversal Report" />
    </p>
</form>