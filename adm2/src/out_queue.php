<?
    include "wrapper/header.php";
?>

<table cellpadding="0" cellspacing="0" class="listing">
    <tr>
        <th colspan="6"><h1>Outgoing Queue - Old Client Purchases</h1></th>
    </tr>
<?
    include "out_queue_nav.php";
?>
</table>

<table cellpadding="0" cellspacing="0" class="listing">
<?
    $values   = array("transactionID",
                      "order_id",
                      "clientEmail",
                      "receiverEmail",
                      "receiverPhone",
                      "itemID",
                      "timeStamp",
                      "dispatchStatus",
                      "clientFirstName",
                      "receiverFirstName");
    
    $result = render_pager($values, "OutgoingQueue", "timeStamp DESC", 15, $countryAbbreviation, 'airtime');
    $exist = mysql_num_rows($result);
    
    if($exist)
    {
        echo'<thead>
            <tr>
            <th>Option</th>
            <th>Time Stamp</th>
            <th>Client Email</th>
            <th>Transaction ID</th>
            <th>Order ID</th>
            <th>Receiver Phone</th>
            <th>Item ID</th>
            <th>Dispatch Status</th>
            <th>Client</th>
            <th>Receiver</th>
            <th>Receiver Email</th>
            </tr>
        </thead>';
                        
        while($data = mysql_fetch_array($result))
        {
            
            echo'<tbody><tr>
                <td>'.
                "<a href='verify_old_client_transaction.php?id=".$data['transactionID'].
                "&page=$thisPage' onfocus='this.blur();' title='Verify Old Client'>".
                "Verify</a> | ".
                "<a href='#' onfocus='this.blur();' title='Delete Transaction' onclick=delEntry('".$data['transactionID'].
                "','OutgoingQueue','transactionID','$thisPage');>Delete</a>".
            '</td>
            <td>'.$data['timeStamp'].'</td>
            <td><a href="mailto:'.$data['clientEmail'].
                '" title="'.$data['clientEmail'].
             '">'.$data['clientEmail'].'</a></td>
            <td>'.$data['transactionID'].'</td>
            <td>'.$data['order_id'].'</td>
            <td>'.$data['receiverPhone'].'</td>
            <td>'.$data['itemID'].'</td>
            <td>'.$data['dispatchStatus'].'</td>
            <td>'.$data['clientFirstName'].'</td>
            <td>'.$data['receiverFirstName'].'</td>
            <td>'.$data['receiverEmail'].'</td>
            </tr></tbody>';
            
            $u++;
        }
    }
    else
    {
        echo '<tr><td align="center" colspan="4">No users in outgoing queue</td></tr>';
    }
?>

</table>

<?
    include "wrapper/footer.php";
?>