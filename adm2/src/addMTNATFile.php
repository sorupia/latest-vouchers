<?
/*******************************************************************************
**  FILE: addMTNATFile.php
**
**  FUNCTION: loadMTNATFromFile.php
**
**  PURPOSE: This function is to read one of the many MTN EVD formats and load to
**  the database
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 7th.Feb.2008
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 1st.Aug.2012
**               MTN format changed again
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 27th.May.2015
**               MTN format changed again,this time trying to make it generic
**               -Removed the need to validate first_card_info_field last 2 digits
**               -Removed the need to validate card_info_line_1
**               -Removed the need to validate the first 9 digits in the card_info_line_2
*********************************************************************************/

    require_once($_SERVER['DOCUMENT_ROOT'].'/src/GeneralFunctions.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/src/SecurityFunctions.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/src/SessionFunctions.php');

    function loadMTNATFromFile($file, $username, $password)
    {
        //BEGIN
        $testing = false; //IMPORTANT

        $mtn_pin_length=13;
        $curr_line=0;
        $success = false;
        $report_problem = true;
        $error_report = "ERROR REPORT: \n";
        $error_report.= "============= \n";
        $report_success = true;
        $load_report = "LOAD REPORT: \n";
        $load_report.= "============= \n";
        $debug = false;
        $error_detected = false;
        $networkName = "MTN Uganda";
        $countryAbbreviation = "ug";

        $pinNumbers[0][0] = 0; //stores pin number
        $pinNumbers[0][1] = 0; //stores serial number

        //Authenticate Airtime Loader
        //authenticate the username and password of this employee
        $success=authenticateEmployee($username, $password, $countryAbbreviation);

        //Read file name line
        // Start of file CRYPT_VOPROD.000000000000252.20081001.1327
        // ugmtn_load_id, ugmtn_load_date, ugmtn_load_time
        if($success)
        {
            $file_name_line = preg_split("/[\s,\x2e]+/",$file[$curr_line++]);
            if( $file_name_line )
            {
                $file_name_line_prefix = $file_name_line[3];
                $output = "file name line prefix: $file_name_line_prefix \n";
                $load_report.= $output;
                echo $output."<br>";
            }
            else
            {
                $success = false;
                $output = "could not extract file name line \n";
                $error_report.= $output;
                $error_detected = true;
                echo $output."<br>";
            }
        }
        else{
            echo "Wrong Username and Password <br>";
            return;
        }

        //ugmtn_load id, date, time
        if( $success && $file_name_line_prefix == "CRYPT_VOPROD" )
        {
            $file_name_line_ugmtn_load_id   = $file_name_line[4];
            $file_name_line_ugmtn_load_date = $file_name_line[5];
            $file_name_line_ugmtn_load_time = $file_name_line[6];
            $output = "ugmtn load id: $file_name_line_ugmtn_load_id \n";
            $output.= "ugmtn load date: $file_name_line_ugmtn_load_date \n";
            $output.= "ugmtn load time: $file_name_line_ugmtn_load_time \n";
            $load_report.= $output;
            echo $output."<br>";
            //Y-m-d H:i:s
            $dateWePurchased = substr($file_name_line_ugmtn_load_date,0,4)."-".//Y-
                               substr($file_name_line_ugmtn_load_date,4,2)."-".//m-
                               substr($file_name_line_ugmtn_load_date,6,2)." ".//d
                               substr($file_name_line_ugmtn_load_time,0,2).":".//H:
                               substr($file_name_line_ugmtn_load_time,2,2).":".//i:
                               "00";//s
            if($debug) echo "Date we purchased: $dateWePurchased <br>";                        
        }
        else
        {
            $success = false;
            $output = "File Invalid: CRYPT_VOPROD missing from file name line \n";
            $error_report.= $output;
            $error_detected = true;
            echo $output."<br>";
        }

        echo "<br>";

        //Read file information line
        //$file[1]="5k-20pcs";
        //card_value-num_cards_in_file
        //some_number, 50900...num_of_cards
        //00000000007,509000000000000050
        if( $success )
        {
            $file_info_line = preg_split("/[[:punct:]-]+/",$file[$curr_line++]);//match any number of '-' put
            echo "file info line 0: ".$file_info_line[0]."<br>";
            echo "file info line 1: ".$file_info_line[1]."<br>";
            $output = "Reading file information line ---\n";
            $load_report.= $output;
            echo $output."<br>";
        }

        if( $file_info_line )
        {
            $file_info_card_value = (int) $file_info_line[0];
            $num_cards_in_file = substr($file_info_line[1],-5,5);
            $output = "Cards in this file are of type: $file_info_card_value&#75 UGX\n"; //&#75 = K
            $load_report.= $output;
            echo $output."<br>";
            $output = "Number of cards in this file: $num_cards_in_file\n";
            $load_report.= $output;
            echo $output."<br>";
        }
        else
        {
            $success = false;
            $output = "Unable to extract file information line\n";
            $error_report.= $output;
            $error_detected = true;
            echo $output."<br>";
        }

        echo "<br>";

        //reading type of cards in the file
        //$file[2]="040K00031.12.2999000006888250823803   385175714";
        if( $num_cards_in_file )
        {
            $card_info_line = preg_split("/[\s,\x2e]+/",$file[$curr_line]);
            $first_card_info_field = $card_info_line[0];

            //extract 040 from 040K00031
            $card_value = (int) (substr($first_card_info_field,0,3));

            $output = "Cards in this file are of type: $card_value&#75 UGX\n"; //&#75 = K
            $load_report.= $output;
            echo $output."<br>";

            $itemIDNumber = "ugmtncard".$card_value."000";      
            $ourPriceInUSD = getPriceInUSD($itemIDNumber);
            $valueInUGSh = getValueInLocal($itemIDNumber);
            if($debug) echo"Item ID: $itemIDNumber \n";
        }
        else
        {
            $success = false;
            $output = "Unable to extract file information line\n";
            $error_report.= $output;
            $error_detected = true;
            echo $output."<br>";
        }

        //match the two occurrences of card type
        if( $file_info_card_value == $card_value )
        {
            $output ="File info line card type matches card type in rows\n";
            $load_report.= $output;
            echo $output."<br>";
        }
        else
        {
            //$success = false; //ignore this warning
            $output ="WARNING: File info line card type does not match card type in rows\n";
            $error_report.= $output;
            //$error_detected = true;
            echo $output."<br>";
        }

        //checking if the K exists in this first card info field 040K00031
        if( (substr($first_card_info_field,3,1) == 'K'||substr($first_card_info_field,3,1) == 'k') )
        {
            $success = true;
            $output = "First field in card info field contains K character i.e. $first_card_info_field\n";
            echo $output."<br>";
        }
        else
        {
            $success = false;
            $output = "first field in card info field does not contain K i.e. $first_card_info_field \n";
            $error_report.= $output;
            $error_detected = true;
            echo $output."<br>";
        }

        echo "<br>";

        //Read next (number of cards) lines
        //  old     000000001.01.1900000003418623034334   383118533
        //$file[2]="040K00031.12.2999000006888250823803   385175714";
        //card_pin, card_serial_num
        for($j=0; 
            ($num_cards_in_file>0) && $j<$num_cards_in_file && $success; 
            $j++)
        {
            $card_info_line = preg_split("/[\s,\x2e]+/",$file[$curr_line++]);

            if( $success && $card_info_line )
            {
                $card_info_line_0 = $card_info_line[0];
                $output = "Reading card information line: $j ---\n";
                $output.= "Read $card_info_line_0\n";
                $load_report.= $output;
                if($debug) echo $output."<br>";
            }
            else
            {
                $success = false;
                $output = "Unable to read card information line\n";
                $error_report.= $output;
                $error_detected = true;
                echo $output."<br>";
            }

            if( $success && ($card_info_line_0 == $first_card_info_field) )
            {
                $card_info_line_1 = $card_info_line[1];
                $output = "Read $card_info_line_1\n";
                $load_report.= $output;
                if($debug) echo $output."<br>";
            }
            else
            {
                $success = false;
                $output = "Unable to read card_info_line_0: $first_card_info_field\n";
                $error_report.= $output;
                $error_detected = true;
                echo $output."<br>";
            }

            //if( $success && $card_info_line_1 == "12" )
            //The card_info_line_1 value keeps being changed by MTN hence no need to check
            if( $success )
            {
                $card_info_line_2 = substr($card_info_line[2],0,9);
                $output = "Read $card_info_line_2\n";
                $load_report.= $output;
                if($debug)  echo $output."<br>";
            }
            else
            {
                $success = false;
                $output = "Unable to read card_info_line_2: 12\n";
                $error_report.= $output;
                $error_detected = true;
                echo $output."<br>";
            }

            //if( $success && $card_info_line_2=="299900000" )
            //The first 9 digits in the card_info_line_2 keeps being changed by MTN hence no need to check
            if( $success )
            {
                $card_info_card_pin = substr($card_info_line[2],9);
                $card_info_card_serial = $card_info_line[3];

                $pinNumbers[$j][0] = trim($card_info_card_pin);
                $pinNumbers[$j][1] = trim($card_info_card_serial);

                $output1 = "Read card pin: $card_info_card_pin\n";
                $output2 = "Read card serial: $card_info_card_serial\n";
                $load_report.= $output1;
                $load_report.= $output2;
                echo $output1."<br>";
                if($debug) echo $output2."<br>";
            }
            else{
                $success = false;
                $output = "Unable to read card_info_line_2: 299900000\n";
                $error_report.= $output;
                $error_detected = true;
                echo $output."<br>";
            }

            echo "<br>";
        }

        echo "<br>";

        //Read "End of file"
        if( ( substr($file[$curr_line],0,11) == "End of file" ) && $success )
        {
            $output = "End of File successfully reached --- <br>\n";
            $load_report.= $output;
            echo $output."<br>";

            //todo: Send email to cardadmin about new load      
        }
        else
        {
            $output = "FAILURE: End of file line missing possible incomplete file <br>\n";
            $error_report.= $output;
            $error_detected = true;
            echo $output."<br>";

            //todo: Send email to admin with error report for debug
        }
        //END FILE DECODE
        
        //Create and Execute SQL queries

        if($success)
        {
            $myIP=getIP();
            $dateWeLoaded = date("Y-m-d H:i:s");
            $cardStatus = "NEW";    

            //connect, todo:change this to the live database
            if ($testing) { $connection=connect2SMSDB2($countryAbbreviation);   }
            else { $connection=connect2DB2($countryAbbreviation); }
                //search usedcards
                $firstElement = true;
                for( $j=0 ; $j<count($pinNumbers) ; $j++)
                {
                    $currentPIN = $pinNumbers[$j][0];

                    if($currentPIN!=0) 
                    {
                        if($firstElement){ 
                            $duplicateUsedCardsQuery = "SELECT cardPIN FROM UsedCards WHERE "; 
                            
                        }
                        else
                        {
                            $duplicateUsedCardsQuery.=" OR "; 
                        }

                        $duplicateUsedCardsQuery.= "cardPIN='$currentPIN'";
                        $firstElement = false;
                    }
                }

                $output = "Duplicate Used Card Query: ".$duplicateUsedCardsQuery."\n";
                //$load_report.= $output;
                if($debug) echo $output."<br>";


                $duplicateUsedCards = array();
                if($duplicateUsedCardsQuery != "")
                {
                    $duplicateUsedResult=mysql_query($duplicateUsedCardsQuery,$connection);
                    if(!$duplicateUsedResult){
                        $errorMsg = "Query: ".$duplicateUsedCardsQuery."\n";
                        $errorMsg.= "Error: ".mysql_error()."\n";
                        $error_report.= $errorMsg;
                        $error_detected = true;
                        //reportError($errorMsg,"addMTNATFile.php");
                    }               

                    $i = 0;
                    while ($usedCards = mysql_fetch_array($duplicateUsedResult, MYSQL_BOTH)) {
                        $duplicateUsedCards[$i] = $usedCards[0];
                        $duplicate = $duplicateUsedCards[$i];
                        $output = "duplicate used card: ".$duplicate."\n";
                        $error_report.= $output;                
                        if($debug) echo $output."<br>";
                        $i++;
                    }
                }

                echo "<br>";

                //search newcards
                $firstElement = true;
                for( $j=0 ; $j<count($pinNumbers) ; $j++)
                {
                    $currentPIN = $pinNumbers[$j][0];;

                    if($currentPIN!=0)
                    {
                        if($firstElement)
                        { 
                            $duplicateNewCardsQuery = "SELECT cardPIN FROM NewCards WHERE "; 
                        }
                        else{ 
                            $duplicateNewCardsQuery.=" OR "; 
                        }
                        
                        $duplicateNewCardsQuery.= "cardPIN='$currentPIN'";
                        $firstElement = false;
                    }
                }

                if($debug) echo"Duplicate New Card Query: ".$duplicateNewCardsQuery."\n";

                $duplicateNewCards = array();
                if( $duplicateNewCardsQuery != "" ){
                    $duplicateNewResult=mysql_query($duplicateNewCardsQuery,$connection);
                    if(!$duplicateNewResult){
                        $errorMsg = "Query: ".$duplicateNewCardsQuery."\n";
                        $errorMsg.= "Error: ".mysql_error()."\n";
                        $error_report.= $errorMsg;
                        $error_detected = true;
                        //reportError($errorMsg,"addMTNATFile.php");
                    }


                    $i = 0;
                    while ($newCards = mysql_fetch_array($duplicateNewResult, MYSQL_BOTH)) {
                        $duplicateNewCards[$i] = $newCards[0];
                        $duplicate = $duplicateNewCards[$i];
                        $output = "duplicate new card: ".$duplicate."\n";                   
                        $error_report.= $output;
                        if($debug) echo $output."<br>";
                        $i++;
                    }
                    echo "<br>";
                }
                            
                //insert non duplicates
                $firstElement = true;
                for( $j=0 ; $j<count($pinNumbers) ; $j++)
                {

                    $pin = $pinNumbers[$j][0];
                    if( in_array($pin,$duplicateUsedCards) )
                    {
                        $output = "Duplicate in UsedCards: ".$pin."\n";
                        $error_report.= $output;
                        $error_detected = true;
                        echo $output."<br>";                        
                    } 
                    elseif( in_array($pin,$duplicateNewCards) )
                    {
                        $output = "Duplicate in NewCards: ".$pin."\n";
                        $error_report.= $output;
                        $error_detected = true;
                        echo $output."<br>";                        
                    }
                    else
                    {
                        if($firstElement){ $insertionQuery = "INSERT INTO NewCards (cardPIN, networkName, valueInUGSh, ourPriceInUSD, dateWePurchased, cardStatus, itemID, loadedBy, loadersIP, serialNumber) VALUES"; }
                        else { $insertionQuery.=","; }
                        
                        $serial = $pinNumbers[$j][1];
                        $insertionQuery.= "('$pin','$networkName',$valueInUGSh,$ourPriceInUSD,'$dateWePurchased','$cardStatus','$itemIDNumber','$username','$myIP','$serial')";
                        $firstElement = false;
                    }
                }

                echo "<br>";

                if($insertionQuery != "")
                {
                    if($debug) echo "Insertion Query: ".$insertionQuery."\n";

                    $insertionResult=mysql_query($insertionQuery,$connection);
                    if(!$insertionResult){
                        $errorMsg = "Query: ".$insertionQuery."\n";
                        $errorMsg.= "Error: ".mysql_error()."\n";
                        reportError($errorMsg,"addMTNATFile.php");
                    }
                    else
                    {
                        $output = "SUCCESS <br>-----------<br>".mysql_info();
                        $load_report.= $output;
                        echo $output."<br>";
                    }
                }
                else
                {
                    $output = "ERROR: NOTHING TO ADD TO DATABASE ";
                    $error_report.=$output;
                    $error_detected = true;
                    echo $output."<br>";
                }       
            //disconnect
            disconnectDB($connection);


            //report duplicates to screen & admin email
            if($report_problem && $error_detected)
            {
                $error_report = $load_report."\n".$error_report;
                reportError($error_report,"loadMTNATFromFile()");
                echo "Error report sent to admin<br>";
            }
            else if($report_success)
            {
                $load_report = "Loaded By: ".$username."\n".$load_report;           
                sendEmail("Sendairtime Web",FROM_EMAIL,LOADER_EMAIL,SUPER_TECH_ADMIN,"-loadMTNATFromFile Report-",$load_report);            
                if($debug) echo "Load report sent to admin<br>";
            }
            
        }
            
        //Write to file and save on server
        if($debug) echo "End of Function <br>";
    }

    //Start of file CRYPT_VOPROD.000000000007430.20120718.1125
    //00000000007,430000000000000010
    //040K00031.12.2999000001670743733500   385309074
    //040K00031.12.2999000005855104321971   385309073
    //040K00031.12.2999000003112416991383   385309072
    //040K00031.12.2999000003862851929867   385309071
    //040K00031.12.2999000007844100582254   385309070
    //040K00031.12.2999000001118713026161   385309069
    //040K00031.12.2999000002097680218880   385309068
    //040K00031.12.2999000002701791134253   385309067
    //040K00031.12.2999000003912117594379   385309066
    //040K00031.12.2999000001150417418157   385309065
    //End of file

    //old file format before 20120404
    //$file[0]="Start of file CRYPT_VOPROD.000000000000252.20081001.1327";
    //$file[1]="5k-20pcs";
    //$file[2]="040K00031.12.2999000006888250823803   385175714";
    //$file[3]="040K00031.12.2999000001030590868418   385177550";
    //$file[4]="040K00031.12.2999000001545234637196   385179951";
    //$file[5]="040K00031.12.2999000002295553596197   385181694";
    //$file[6]="040K00031.12.2999000004010058943475   385180638";
    //$file[7]="040K00031.12.2999000003601126147216   385192360";
    //$file[8]="040K00031.12.2999000003851626463380   385171746";
    //$file[9]="040K00031.12.2999000002227880338273   385178398";
    //$file[10]="040K00031.12.2999000003099764143732   385171487";
    //$file[11]="040K00031.12.2999000002980956027189   385177653";
    //$file[12]="040K00031.12.2999000006420111123133   385174139";
    //$file[13]="040K00031.12.2999000003743805804110   385186514";
    //$file[14]="040K00031.12.2999000002530198164154   385185917";
    //$file[15]="040K00031.12.2999000008879613371626   247269114";
    //$file[16]="040K00031.12.2999000009238343452593   385173610";
    //$file[17]="End of file";

    //old file format
    //Begin file extraction
    //$file[0] = "Start of file CRYPT_VOPROD.000000000003213.20080207.0950";
    //$file[1] = "5K-07PCS";
    //$file[2] = "000000001.01.1900000003418623034334   383118533";
    //$file[3] = "000000001.01.1900000003877411175254   383118534";
    //$file[4] = "000000001.01.1900000006999902853050   383118535";
    //$file[5] = "000000001.01.1900000001008909891107   383118536";
    //$file[6] = "000000001.01.1900000003743853500187   383118537";
    //$file[7] = "000000001.01.1900000004258704201336   383118538";
    //$file[8] = "000000001.01.1900000001912694235187   383118539";
    //$file[9] = "End of file";

?>