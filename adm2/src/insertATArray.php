<?
function insertATArray($testing,
                       $countryAbbreviation,
					   $pinNumbers,
					   $error_detected,
					   $error_report,
					   $load_report,
					   $networkName,
					   $valueInUGSh,
					   $ourPriceInUSD,
					   $dateWePurchased,
					   $itemIDNumber,
					   $username,
					   $myIP)
{

	$debug = $testing;
    $duplicate_count = 0;
    $total_submitted = count($pinNumbers);
	$output = "Number of Cards to be Loaded: ".$total_submitted."\n <br>";
	$error_report.= $output;
    if($debug) echo $output."<br>";

			//connect, todo:change this to the live database
			if ($testing) { $connection=connect2SMSDB2($countryAbbreviation);	}
			else { $connection=connect2DB2($countryAbbreviation); }

                //search usedcards
				$firstElement = true;
				for( $j=0 ; $j<$total_submitted ; $j++)
				{
					$currentPIN = $pinNumbers[$j][0];

					if($currentPIN!=0)
					{
						if($firstElement){
							$duplicateUsedCardsQuery = "SELECT cardPIN FROM UsedCards WHERE "; 
							
						}
						else
						{
							$duplicateUsedCardsQuery.=" OR "; 
						}

						$duplicateUsedCardsQuery.= "cardPIN='$currentPIN'";
						$firstElement = false;
					}
				}

				$duplicateUsedCards = array();	
				if($duplicateUsedCardsQuery != "")
				{
					$duplicateUsedResult=mysql_query($duplicateUsedCardsQuery,$connection);
					if(!$duplicateUsedResult){
						$errorMsg = "Query: ".$duplicateUsedCardsQuery."\n";
						$errorMsg.= "Error: ".mysql_error()."\n";
						$error_report.= $errorMsg;
						$error_detected = true;
						//reportError($errorMsg,"addMTNATFile.php");
					}				

					$i = 0;
					while ($usedCards = mysql_fetch_array($duplicateUsedResult, MYSQL_BOTH)) {
						$duplicateUsedCards[$i] = $usedCards[0];
						$duplicate = $duplicateUsedCards[$i];
						$output = "duplicate used card: ".$duplicate."\n";
						$error_report.= $output;
						if($debug) echo $output."<br>";
						$i++;
					}

					$output = "Duplicate Used Card Query: <br>".$duplicateUsedCardsQuery."\n";
					//$load_report.= $output;
					if($debug) echo $output."<br>";
				}
				else{
					$output = "ERROR: duplicateUsedCardsQuery: $duplicateUsedCardsQuery \n";
					$error_report.= $output;
					if($debug) echo $output."<br>";
				}

				if($debug) echo "<br>";

				//search newcards		
				$firstElement = true;	
				for( $j=0 ; $j<$total_submitted ; $j++)
				{
					$currentPIN = $pinNumbers[$j][0];;

					if($currentPIN!=0)
					{
						if($firstElement)
						{ 
							$duplicateNewCardsQuery = "SELECT cardPIN FROM NewCards WHERE "; 
						}
						else{ 
							$duplicateNewCardsQuery.=" OR "; 
						}
						
						$duplicateNewCardsQuery.= "cardPIN='$currentPIN'";
						$firstElement = false;
					}
				}
				
				$output = "Duplicate New Card Query: <br>".$duplicateNewCardsQuery."\n";
				if($debug) echo $output."<br>";
				
				$duplicateNewCards = array();
				if( $duplicateNewCardsQuery != "" ){
					$duplicateNewResult=mysql_query($duplicateNewCardsQuery,$connection);
					if(!$duplicateNewResult){
						$errorMsg = "Query: ".$duplicateNewCardsQuery."\n";
						$errorMsg.= "Error: ".mysql_error()."\n";
						$error_report.= $errorMsg;
						$error_detected = true;
						//reportError($errorMsg,"addMTNATFile.php");
					}
				

					$i = 0;
					while ($newCards = mysql_fetch_array($duplicateNewResult, MYSQL_BOTH)) {
						$duplicateNewCards[$i] = $newCards[0];
						$duplicate = $duplicateNewCards[$i];
						$output = "duplicate new card: ".$duplicate."\n";					
					    $error_report.= $output;
						if($debug) echo $output."<br>";
						$i++;
					}
					if($debug) echo "<br>";
				}
				else{
					$output = "ERROR: duplicateNewCardsQuery: $duplicateNewCardsQuery \n";
					$error_report.= $output;
					if($debug) echo $output."<br>";
				}				
							
				//insert non duplicates
				$firstElement = true;
				for( $j=0 ; $j<$total_submitted ; $j++)
				{
					
					$pin = $pinNumbers[$j][0];
					if( in_array($pin,$duplicateUsedCards) )
					{
                        $duplicate_count++;
						$output = "Duplicate in UsedCards: ".$pin."\n <br>";
					    $error_report.= $output;
						$error_detected = true;
						if($debug) echo $output."<br>";						
					} 
					elseif( in_array($pin,$duplicateNewCards) )
					{
                        $duplicate_count++;
						$output = "Duplicate in NewCards: ".$pin."\n <br>";
					    $error_report.= $output;
						$error_detected = true;
						if($debug) echo $output."<br>";						
					}
					else
					{
						if($firstElement){ $insertionQuery = "INSERT INTO NewCards (cardPIN, networkName, valueInUGSh, ourPriceInUSD, dateWePurchased, cardStatus, itemID, loadedBy, loadersIP, serialNumber) VALUES"; }
						else { $insertionQuery.=","; }
						
						$serial = $pinNumbers[$j][1];
						$insertionQuery.= "('$pin','$networkName',$valueInUGSh,$ourPriceInUSD,'$dateWePurchased','NEW','$itemIDNumber','$username','$myIP','$serial')";
						$firstElement = false;
					}
				}
				
				if($debug) echo "<br>";
				
				if($insertionQuery != "")
				{
					if($debug) echo "Insertion Query: ".$insertionQuery."\n";
			
					$insertionResult=mysql_query($insertionQuery,$connection);
					if(!$insertionResult){
						$errorMsg = "Query: ".$insertionQuery."\n";
						$errorMsg.= "Error: ".mysql_error()."\n";
						reportError($errorMsg,"insertATArray.php");
					}
					else
					{
						$output = "<br><br> SUCCESS \n <br>-----------\n <br>";
						$output.= "Total: ".$total_submitted." \n <br>";
                        $output.= "Duplicates: ".$duplicate_count."\n <br>";
                        $output.= "Inserted ".mysql_info();
                        $load_report.= $output;
						$db_stats = $output;
						if($debug) echo $output."<br>";
					}
				}
				else
				{
					$output = "ERROR: NOTHING TO ADD TO DATABASE ";
					$error_report.=$output;
					$error_detected = true;
					if($debug) echo $output."<br>";
				}		
			//disconnect
			disconnectDB($connection);
			
			$result = array("error_occured"=>$error_detected, "error_info"=>$error_report, "load_info"=>$load_report, "db_stats"=>$db_stats);
			return $result;
}
?>