<?
    include_once "src/addMTNATFile_5k_20120904.php";
    include_once "src/addMTNATFile_10k_20120904.php";
    include_once "src/addMTNATFile_20k_20150520.php";
    include_once "src/addMTNATFile.php";
    include_once "src/addCSVATFile.php";
    $debug = false;
	$keep_file_record = false;
	
    $username = trim($_POST['usernameField']);
    $password = trim($_POST['passwordField']);	
    $fileType = $_POST['fileType'];
	$cardLength = trim($_POST['cardLengthField']);
    $airtime_file = $_POST['airtime_file'];
    $temp_file_path = $_FILES["airtime_file"]["tmp_name"];
    $fileArray = explode("\n",$fileContents);
	
	/* Read file contents into an array */
	$fileArray = file($temp_file_path,FILE_SKIP_EMPTY_LINES);
    
	/* DEBUG */
    if($debug) echo "username: $username <br>";
    if($debug) echo "password: $password <br>";
    if($debug) echo "fileType: $fileType <br>";
    if($debug) echo "temp file path: $temp_file_path <br>";
    if($debug) echo "Array Contents:";	
    if($debug) print_r($fileArray);
    

	/* Move file to a location for record storage */
	if($keep_file_record)
	{
		$target_path = "records/";
		$target_path = $target_path.basename($_FILES["airtime_file"]["name"]);
	
		if(move_uploaded_file($_FILES['airtime_file']['tmp_name'], $target_path)) {
			echo "The file ".  basename( $_FILES['airtime_file']['name'])." has been uploaded";
		}
		else{
			echo "There was an error uploading the file, please try again!";
		}
	}

    if($fileType == "ugmtncard-CRYPT" ||
       $fileType == "ugmtncard-CRYPT-5k" ||
       $fileType == "ugmtncard-CRYPT-10k" ||
       $fileType == "ugmtncard-CRYPT-20k")
    {
        if($fileType == "ugmtncard-CRYPT-5k")
        {
            loadMTNATFromFile_5k_20120904($fileArray, $username, $password);
        }
        else if($fileType == "ugmtncard-CRYPT-10k")
        {
            loadMTNATFromFile_10k_20120904($fileArray, $username, $password);
        }
        else if($fileType == "ugmtncard-CRYPT-20k")
        {
            loadMTNATFromFile_20k_20150520($fileArray, $username, $password);
        }
        else
        {
            loadMTNATFromFile($fileArray, $username, $password);
        }
    }
	else if($fileType == "none")
	{
	    echo 'No airtime type selected, please retry <a href="new_at_file.php">here</a>';
	}
    else
    {
        $itemIDNumber = $fileType;
        loadCSVATFromFile($fileArray, $username, $password, $itemIDNumber, $cardLength);
    }
?>