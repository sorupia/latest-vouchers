<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/GeneralFunctions.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/SecurityFunctions.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/SessionFunctions.php');
	require_once('insertATArray.php');
	
	function loadCSVATFromFile($file, $username, $password, $itemIDNumber)
	{
		//BEGIN
		$testing = false; //IMPORTANT
		
		$network_pin_length=13;
		$curr_line=0;
		$success = false;
		$report_problem = true;
		$error_report = "ERROR REPORT: \n";
		$error_report.= "============= \n";
		$report_success = true;
		$load_report = "LOAD REPORT: \n";
		$load_report.= "============= \n";
		$debug = false;
		$error_detected = false;
		
		$countryAbbreviation = substr($itemIDNumber,0,2);

		$networkName = getNetworkName($itemIDNumber);
		echo $networkName."<br>";
		echo $itemIDNumber."<br>";

		$pinNumbers = array(); //stores pin number
		
		//Authenticate Airtime Loader
		$success=authenticateEmployee($username, $password, $countryAbbreviation);
	
	
		//verify item_id_number	
		if($success && isItemIDValid($itemIDNumber,$countryAbbreviation))
		{
			$ourPriceInUSD = getPriceInUSD($itemIDNumber);
			$valueInUGSh = getValueInLocal($itemIDNumber);
			$output = "Item ID: $itemIDNumber \n";
			$load_report.= $output;
			if($debug) echo $output."<br>";
		}
		else
		{
			$success = false;
			$output = "ERROR: Invalid Item ID \n";
			$error_report.= $output;
			$error_detected = true;
			echo $output."<br>";
			
		}

		//Begin file extraction
		//$file[0] = "Serial,Code";
		//$file[1] = "40000300501698,21255287184173";
		
		//verify the first line i.e. Serial,Code
		$first_line = explode(',',$file[$curr_line++]);
		$serial_txt = trim($first_line[0]);
		$code_txt = trim($first_line[1]);
		$output = "first_line[0]: ".$serial_txt."<br>";
		$output.= "first_line[1]: ".$code_txt."<br>";
		$error_report.= $output;
		
		if( $success && 
		   $serial_txt == "Serial" && 
		   $code_txt == "Code")
		{
			$output = "Read first line successfully: ";			
			$output.= $first_line[0].",";
			$output.= $first_line[1]."\n";
			$load_report.= $output;
			echo $output."<br>";
		}
		else{
			$success = false;
			$output = "ERROR: First line should contain the text: Serial,Code \n";
			$error_report.= $output;
			$error_detected = true;
			echo $output."<br>";
		}
		
		//read serial number and airtime from file line
		//40000300501698,21255287184173
		if($success)
		{
			for( $i=0 ; 
			     ( $i<(count($file)-1) ) && ( $file[$curr_line] != "" ) ; 
				 $i++)
			{
				$airtime_card = explode(',', $file[$curr_line++]);
				$pinNumbers[$i][0] = trim($airtime_card[1]);
				$pinNumbers[$i][1] = trim($airtime_card[0]);
				$output = "Airtime Code: ".$pinNumbers[$i][0]."\n";
				echo "<br>";
				$output.= "Airtime Serial#: ".$pinNumbers[$i][1]."\n";
				$load_report.= $output;
				echo $output."<br>";
			}
		}



		
		echo "<br>";	
		
		//Read "End of file"
		if( $success )
		{
			$output = "End of File successfully reached --- <br>\n";
			$load_report.= $output;
			echo $output."<br>";
			
			//todo: Send email to cardadmin about new load		
		}
		else
		{
			$output = "FAILURE: Did not reach end of file, possible incomplete file <br>\n";
			$error_report.= $output;
			$error_detected = true;
			echo $output."<br>";
		}
		//END FILE DECODE
		
		//$success = true; //todo:remove 
		//Create and Execute SQL queries
		if($success)
		{
			$myIP=getIP();
			$dateWePurchased = date("Y-m-d H:i:s");
			
			$result = insertATArray($testing,
                          			$countryAbbreviation,
								  	$pinNumbers,
								  	$error_detected,
								  	$error_report,
								  	$load_report,
								  	$networkName,
								  	$valueInUGSh,
								  	$ourPriceInUSD,
								  	$dateWePurchased,
								  	$itemIDNumber,
								  	$username,
								  	$myIP);
			
			$error_detected = $result['error_occured'];
			$error_report = $result['error_info'];
			$load_report  = $result['load_info'];
			
			//report duplicates to screen & admin email
			if($report_problem && $error_detected)
			{
				$error_report = "Loaded By: ".$username."\n".$load_report."\n".$error_report;
				reportError($error_report,"loadCSVATFromFile()");
				echo "Error report sent to admin<br>";
			}
			else if($report_success)
			{
				$load_report = "Loaded By: ".$username."\n".$load_report;
				sendEmail("Sendairtime Web",FROM_EMAIL,TECH_ADMIN,$bcc,"-loadCSVATFromFile Report-",$load_report);			
				if($debug) echo "Load report sent to admin<br>";
			}
			
		}
		else{
			echo "NO RECORD INSERTED INTO DATABASE DUE TO ERRORS! <br>";
		}
			
		//Write to file and save on server
		if($debug) echo "End of Function <br>";
	}
	
	
?>