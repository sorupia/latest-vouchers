<?

	$report_type = isset($_POST['report_type']) ? $_POST['report_type'] : "";
	
	$authenticated = false;
    
    if(isset($_POST['Generate']))
    {
        $username = $_POST['employee_username'];
        $password = $_POST['employee_password'];

        $authenticated = authenticateEmployee($username, $password, $countryAbbreviation);

		$_SESSION['ViewAllPINsUsername'] = $username;
    }
	
	
	if($authenticated && isset($_POST['Generate']))
	{
		$debug = false;
		
		$exchange_rate = $_POST['ex_rate'] * 0.947; 
		$start_date    = $_POST['start_date'].':01';
		$end_date      = $_POST['end_date'].':59';
		
		
		
		
		if($report_type=='all_pins_report')
		{
			echo"All Cards Report by Date of Deletion(Deleted Cards) or Date of Purchase: <br><b> $start_date to $end_date </b>";

			/* Initialize array that will be used to contain merged card tables rows will be columns and vice versa*/
			$allCardsArray = array();																	

			/* Used Cards */
			$usedCardsResult = queryUsedCardsForAllPins($countryAbbreviation,
														$start_date,
														$end_date);
			$allCardsArray = organizeUsedCardsResults($usedCardsResult,$allCardsArray);
			$numUsedCards = count($allCardsArray[0]);
			echo "<br>Used cards: ".$numUsedCards;

			/* New Cards */
			$newCardsResult = queryNewCardsForAllPins($countryAbbreviation,
													  $start_date,
													  $end_date,
													  $username);
			$allCardsArray = organizeNewCardsResults($newCardsResult,$allCardsArray);
			$numNewCards = count($allCardsArray[0]) - $numUsedCards;
			echo "<br>New cards: ".$numNewCards;						
			
			/*  Deleted Cards */
			$deletedCardsResult = queryDeletedForAllPins($countryAbbreviation,
														 $start_date,
														 $end_date);
			$allCardsArray = organizeDeletedCardsResults($deletedCardsResult,
														 $allCardsArray);
			$numDeletedCards = count($allCardsArray[0]) - $numUsedCards - $numNewCards;
			echo "<br>Deleted cards: ".$numDeletedCards;

			if(($numUsedCards != 0) || ($numNewCards != 0) || ($numDeletedCards != 0))
			{
				/* Sort the table with a php4+ provided function*/
				array_multisort($allCardsArray[0],SORT_ASC,SORT_STRING,
								$allCardsArray[1],SORT_ASC,SORT_STRING,
								$allCardsArray[2],SORT_ASC,SORT_STRING,
								$allCardsArray[3],SORT_ASC,SORT_STRING,
								$allCardsArray[4],SORT_ASC,SORT_STRING,
								$allCardsArray[5],SORT_ASC,SORT_STRING,
								$allCardsArray[6],SORT_ASC,SORT_STRING,
								$allCardsArray[7],SORT_ASC,SORT_STRING);

				/* Display results remembering that the rows are columns and vice versa*/
				displayAllCardsReport($allCardsArray);
			}
			else
			{
				echo"<br><br> No records please check dates entered";
			}
		}
	}
	elseif(!$authenticated && isset($_POST['Generate']))
	{
		echo "<br><br> Invalid username and password";
	}
?>