<?

    $u = isset($u) ? $u : 0;

	$values   = array("deleteID",
	                  "username",
					  "cardPINDeleted",
					  "itemID",
					  "dateWePurchased",
					  "timeDeleted",
					  "loadedBy",
					  "deleteIP");
	
	$result = render_pager($values, "DeleteLog", "timeDeleted DESC", 15, $countryAbbreviation, 'airtime');
	$exist = mysql_num_rows($result);
	
	if($exist)
	{
		echo'<thead>
			<tr>
			<th>Time Deleted</th>
			<th>Card PIN</th>
			<th>Item ID</th>
			<th>Purchase Date</th>
			<th>Loaded By</th>
			<th>Deleted By</th>
			<th>IP</th>
			</tr>
		</thead>';
						
		while($data = mysql_fetch_array($result))
		{
			
			echo'<tbody><tr>
				<td>'.$data['timeDeleted'].'</td>
				<td>'.$data['cardPINDeleted'].'</td>
				<td>'.$data['itemID'].'</td>
				<td>'.$data['dateWePurchased'].'</td>
				<td>'.$data['loadedBy'].'</td>
				<td>'.$data['username'].'</td>
				<td>'.$data['deleteIP'].'</td>
				</tr></tbody>';
			
			$u++;
		}
	}
	else
	{
		echo '<tr><td align="center" colspan="4">No deleted cards in database</td></tr>';
	}
?>