<?php
//11.May.2009: Added duplicate check within loaded batch of 11 cards
//12.May.2009: Added email confirmation of loading cards

if($_POST['Submit']){


	function createQuery($pinNo,$selection,$isFirstField,$insertQuery,$loadedBy){
		$networkName2=getNetworkName($selection);
		if($debug){echo"networkName2 = $networkName2 <br>";}
		$valueInUGSh2=getValueInUGSh($selection);
		$ourPriceInUSD2=getPriceInUSD($selection);
		if($debug){echo"ourPriceInUSD2 = $ourPriceInUSD2 <br>";}
		
		$myIP=getIP();
		echo"Your IP address is $myIP<br>";
		//$myHostName=gethostbyaddr($myIP);
		//echo"Your host name is $myHostName ";
		
		//YYYY-MM-DD HH:MM:SS
		//date_default_timezone_set('America/Montreal'); in PHP5.2
		$dateWePurchased2=date("Y-m-d H:i:s");
		echo" date = $dateWePurchased2 <br>";
		$cardStatus2="NEW";
		//create string to add to query
		$pinQuery2="('$pinNo','$networkName2',$valueInUGSh2,$ourPriceInUSD2,'$dateWePurchased2','$cardStatus2','$selection','$loadedBy','$myIP')";
	
		if($isFirstField){
			$insertQuery=$insertQuery.$pinQuery2;
		}
		else{
			$insertQuery=$insertQuery.','.$pinQuery2;
		}
		return $insertQuery;
	}
	
	function createUsedCardsQuery($pinNo,$selection,$isFirstField,$transactionID,$clientEmail,$receiverEmail, $insertQuery, $username){
		$networkName2=getNetworkName($selection);
		if($debug){echo"networkName2 = $networkName2 <br>";}
		$valueInUGSh2=getValueInUGSh($selection);
		$ourPriceInUSD2=getPriceInUSD($selection);
		if($debug){echo"ourPriceInUSD2 = $ourPriceInUSD2 <br>";}
		//YYYY-MM-DD HH:MM:SS
		//date_default_timezone_set('America/Montreal'); in PHP5.2
		$dateWePurchased2=date("Y-m-d H:i:s");
		if($debug){echo" date = $dateWePurchased2 <br>";}
		$cardStatus2="NEW";
		//create string to add to query
		//VALUES('$cardPIN',networkName,valueInUGSh,ourPriceInUSD,dateWePurchased,dateWeSoldIt,transactionID,itemID,clientEmail,receiverEmail)
		$pinQuery2="('$pinNo','$networkName2',$valueInUGSh2,$ourPriceInUSD2,'$dateWePurchased2','$dateWePurchased2','$transactionID','$selection','$clientEmail', '$receiverEmail', '$username')";
	
		if($isFirstField){
			$insertQuery=$insertQuery.' VALUES'.$pinQuery2;
		}
		else{
			$insertQuery=$insertQuery.','.$pinQuery2;
		}
		return $insertQuery;
	}

	
	$debug=false;
	$input_contains_duplicates = false;

	$message = "Loading Report:\n";
	$message.= "IP: $myIP\n";

	$number_of_cards_input = 0;

	//$action=$_REQUEST['action'];
	$pin1=trim($_POST['textfield']); $select1=trim($_POST['select']);
	$pin2=trim($_POST['textfield2']); $select2=trim($_POST['select2']);
	$pin3=trim($_POST['textfield3']); $select3=trim($_POST['select3']);
	$pin4=trim($_POST['textfield4']); $select4=trim($_POST['select4']);
	$pin5=trim($_POST['textfield5']); $select5=trim($_POST['select5']);
	$pin6=trim($_POST['textfield6']); $select6=trim($_POST['select6']);
	$pin7=trim($_POST['textfield7']); $select7=trim($_POST['select7']);
	$pin8=trim($_POST['textfield8']); $select8=trim($_POST['select8']);
	$pin9=trim($_POST['textfield9']); $select9=trim($_POST['select9']);
	$pin10=trim($_POST['textfield10']); $select10=trim($_POST['select10']);
	$pin11=trim($_POST['textfield11']); $select11=trim($_POST['select11']);
	
	//code to make sure duplicates do not exist within the loading batch  
	$filterArray = array();
    $airtimeTypeArray = array();
	if($pin1!=''){  $filterArray[0]  = $pin1; $airtimeTypeArray[0] = $select1;}
	if($pin2!=''){  $filterArray[1]  = $pin2; $airtimeTypeArray[1] = $select2;}
	if($pin3!=''){  $filterArray[2]  = $pin3; $airtimeTypeArray[2] = $select3;}
	if($pin4!=''){  $filterArray[3]  = $pin4; $airtimeTypeArray[3] = $select4;}
	if($pin5!=''){  $filterArray[4]  = $pin5; $airtimeTypeArray[4] = $select5;}
	if($pin6!=''){  $filterArray[5]  = $pin6; $airtimeTypeArray[5] = $select6;}
	if($pin7!=''){  $filterArray[6]  = $pin7; $airtimeTypeArray[6] = $select7;}
	if($pin8!=''){  $filterArray[7]  = $pin8; $airtimeTypeArray[7] = $select8;}
	if($pin9!=''){  $filterArray[8]  = $pin9; $airtimeTypeArray[8] = $select9;}
	if($pin10!=''){ $filterArray[9]  = $pin10; $airtimeTypeArray[9] = $select10;}
	if($pin11!=''){ $filterArray[10] = $pin11; $airtimeTypeArray[10] = $select11;}
	
	$size_before_removal = count($filterArray);
	$unique_result = array_unique($filterArray);
	$size_after_removal = count($unique_result);
	
	if( $size_before_removal != $size_after_removal )
	{
		$input_contains_duplicates = true;
		$message.= "Duplicates within batch: TRUE\n";
	}
	else
	{
		$message.= "Duplicates within batch: FALSE\n";
	}
	
	
	
	$username = trim($_POST['usernameField']);
	$password = trim($_POST['passwordField']);
	
	//$verifyUserQuery="SELECT password FROM Employees WHERE username='$username'";
	//$verifyUserResult=mysql_query($verifyUserQuery) or die("$verifyUserQuery failed");
	//$verifyUserArray=mysql_fetch_array($verifyUserResult);
	//$passwordFromDB=$verifyUserArray['password'];
	
	//authenticate the username and password of this employee
	$authenticated=authenticateEmployee($username, $password, $countryAbbreviation);

	/* Connecting to MYSQL server */        
	$connection = connect2DB2($countryAbbreviation);   
	
	if( $authenticated && !$input_contains_duplicates ){
		$message.= "Authenticated user: $username\n";                

		$pin1Available=false;
		$pin2Available=false;
		$pin3Available=false;
		$pin4Available=false;
		$pin5Available=false;
		$pin6Available=false;
		$pin7Available=false;
		$pin8Available=false;	
		$pin9Available=false;
		$pin10Available=false;
		$pin11Available=false;
		
	
		if($debug){
			echo"pin1 = $pin1 select1 = $select1 <br>";
			echo"pin2 = $pin2 select2 = $select2 <br>";
			echo"pin3 = $pin3 select3 = $select3 <br>";
			echo"pin4 = $pin4 select4 = $select4 <br>";
			echo"pin5 = $pin5 select5 = $select5 <br>";
			echo"pin6 = $pin6 select6 = $select6 <br>";
			echo"pin7 = $pin7 select7 = $select7 <br>";
			echo"pin8 = $pin8 select8 = $select8 <br>";
			echo"pin9 = $pin9 select9 = $select9 <br>";
			echo"pin10 = $pin10 select10 = $select10 <br>";
			echo"pin11 = $pin11 select11 = $select11 <br><br>";
		
			$valueInUGSh1=getValueInUGSh($select2);
			//echo"valueInUGSh1 = $valueInUGSh1 <br>";
		}
	
		//we should ensure that someone does not input a pin thats already in our database
		$checkIncomingCardQuery="SELECT * FROM NewCards WHERE ";
		$checkUsedCards4Duplicate="SELECT * FROM UsedCards WHERE ";
		$add2Query="";
		$isFirst=true;

		//BEGIN Counting number of cards---------------------
		$numberOfCardsInserted=0;
		if($pin1!=''){
			$numberOfCardsInserted=$numberOfCardsInserted+1;
			$pin1Available=true;
			if($isFirst){
				$add2Query="(cardPIN = '$pin1')";
				$isFirst=false;
			}
			else{
				$add2Query=$add2Query."OR"." (cardPIN = '$pin1')";
			}
		}
		if($pin2!=''){
			$numberOfCardsInserted=$numberOfCardsInserted+1;
			$pin2Available=true;
		
			if($isFirst){
				$add2Query="(cardPIN = '$pin2')";
				$isFirst=false;
			}
			else{
				$add2Query=$add2Query." OR"." (cardPIN = '$pin2')";
			}
		}	
		if($pin3!=''){
			$numberOfCardsInserted=$numberOfCardsInserted+1;
			$pin3Available=true;
			if($isFirst){
				$add2Query="(cardPIN = '$pin3')";
				$isFirst=false;
			}
			else{
				$add2Query=$add2Query." OR"." (cardPIN = '$pin3')";
			}
		}	
		if($pin4!=''){
			$numberOfCardsInserted=$numberOfCardsInserted+1;
			$pin4Available=true;
			if($isFirst){
				$add2Query="(cardPIN = '$pin4')";
				$isFirst=false;
			}
			else{
				$add2Query=$add2Query." OR"." (cardPIN = '$pin4')";
			}
		}	
		if($pin5!=''){
			$numberOfCardsInserted=$numberOfCardsInserted+1;
			$pin5Available=true;
			if($isFirst){
				$add2Query="(cardPIN = '$pin5')";
				$isFirst=false;
			}
			else{
				$add2Query=$add2Query." OR"." (cardPIN = '$pin5')";
			}		
		}	
		if($pin6!=''){
			$numberOfCardsInserted=$numberOfCardsInserted+1;
			$pin6Available=true;
			if($isFirst){
				$add2Query="(cardPIN = '$pin6')";
				$isFirst=false;
			}
			else{
				$add2Query=$add2Query." OR"." (cardPIN = '$pin6')";
			}		
		}	
		if($pin7!=''){
			$numberOfCardsInserted=$numberOfCardsInserted+1;
			$pin7Available=true;
			if($isFirst){
				$add2Query="(cardPIN = '$pin7')";
				$isFirst=false;
			}
			else{
				$add2Query=$add2Query." OR"." (cardPIN = '$pin7')";
			}		
		}	
		if($pin8!=''){
			$numberOfCardsInserted=$numberOfCardsInserted+1;
			$pin8Available=true;
			if($isFirst){
				$add2Query="(cardPIN = '$pin8')";
				$isFirst=false;
			}
			else{
				$add2Query=$add2Query." OR"." (cardPIN = '$pin8')";
			}		
		}	
		if($pin9!=''){
			$numberOfCardsInserted=$numberOfCardsInserted+1;
			$pin9Available=true;
			if($isFirst){
				$add2Query="(cardPIN = '$pin9')";
				$isFirst=false;
			}
			else{
				$add2Query=$add2Query." OR"." (cardPIN = '$pin9')";
			}		
		}	
		if($pin10!=''){
			$numberOfCardsInserted=$numberOfCardsInserted+1;
			$pin10Available=true;
			if($isFirst){
				$add2Query="(cardPIN = '$pin10')";
				$isFirst=false;
			}
			else{
				$add2Query=$add2Query." OR"." (cardPIN = '$pin10')";
			}		
		}	
		if($pin11!=''){
			$numberOfCardsInserted=$numberOfCardsInserted+1;
			$pin11Available=true;
			if($isFirst){
				$add2Query="(cardPIN = '$pin11')";
				$isFirst=false;
			}
			else{
				$add2Query=$add2Query." OR"." (cardPIN = '$pin11')";
			}		
		}
		
		$number_of_cards_input = $numberOfCardsInserted;
		
		if($numberOfCardsInserted!=0){
			$checkIncomingCardQuery.=$add2Query;
			$checkIncomingCardResult=mysql_query($checkIncomingCardQuery) or handleDatabaseError(''. mysql_error(),$checkIncomingCardQuery);
			$numberOfOccurences = mysql_num_rows($checkIncomingCardResult);
			//echo" query to check if cards are not already in NewCards DB $checkIncomingCardQuery<br>";
			echo"number of Occurences in NewCards = $numberOfOccurences<br>";
			
			$checkUsedCards4Duplicate.=$add2Query;
			$checkUsedCardsResult=mysql_query($checkUsedCards4Duplicate) or handleDatabaseError(''. mysql_error(),$checkUsedCards4Duplicate);
			$numberOfUsedCards = mysql_num_rows($checkUsedCardsResult);
			//echo" query to check if cards are not already in UsedCards DB $checkUsedCards4Duplicate<br>";
			echo"number of Occurences in UsedCards = $numberOfUsedCards<br>";
		
			//END Counting number of cards---------------------
		
			if($numberOfOccurences==0 && $numberOfUsedCards==0){//if this is not a duplicate
				//LOCK
				$lockQuery="LOCK TABLES OutgoingQueue WRITE";
				$lockResult=mysql_query($lockQuery) or handleDatabaseError(''. mysql_error(),$lockQuery);
									
				//we have PINs, now check the outgoing queue and send the guys the pins first b4 inserting any in db
				//todo: here we can query for all those in the queue instead of limiting it
				$outgoingQueueQuery="SELECT * FROM OutgoingQueue WHERE dispatchStatus='pinready' LIMIT $numberOfCardsInserted FOR UPDATE";
				//echo"out query = $outgoingQueueQuery <br>";
				$outgoingQueueResult=mysql_query($outgoingQueueQuery) or handleDatabaseError(''. mysql_error(),$outgoingQueueQuery);
				$numberOfOutgoingUsers = mysql_num_rows($outgoingQueueResult);
				echo"number of people in the outgoing queue: $numberOfOutgoingUsers";
				$counter=1;
				$isFirstField=true;
				$usedCardsQuery="INSERT INTO UsedCards(cardPIN,networkName,valueInUGSh,ourPriceInUSD,dateWePurchased,dateWeSoldIt,transactionID,itemID,clientEmail,receiverEmail,loadedBy)";
				$usedCardExists=false;
				$updateQuery="";
				//while there are cards and people left to send to
				while(($numberOfCardsInserted!=0)&&($outgoingQueueData = mysql_fetch_array($outgoingQueueResult))){
					echo"Sending to $numberOfOutgoingUsers <br>";
					$transactionID = $outgoingQueueData['transactionID'];
					$clientEmail = $outgoingQueueData['clientEmail'];
					$receiverEmail = $outgoingQueueData['receiverEmail'];
					$receiverPhone = $outgoingQueueData['receiverPhone'];
					$itemID = $outgoingQueueData['itemID'];
					$timeStamp = $outgoingQueueData['timeStamp'];
					$dispatchStatus = $outgoingQueueData['dispatchStatus'];
					$clientFirstName = $outgoingQueueData['clientFirstName'];
					$receiverFirstName=$outgoingQueueData['receiverFirstName'];
					
					if($dispatchStatus=='pinready'){
						//send out an email with confirmation
						//prepareEmail
						//insert cards into UsedCards Table
						echo"Delivery to $clientFirstName is set to pin<br>";
						
						if(($pin1Available)&&($select1==$itemID)){
							$usedCardsQuery=createUsedCardsQuery($pin1,$select1,$isFirstField,$transactionID,$clientEmail,$receiverEmail, $usedCardsQuery, $username);
							$numberOfCardsInserted=$numberOfCardsInserted-1;
							$usedCardExists=true;
							$pin1Available=false;
							echo"pin1 is available and the selected itemID is available";
						
							if($isFirstField){
								$updateQuery=" (transactionID = '$transactionID')";
							}
							else{
								$updateQuery=$updateQuery." OR "."(transactionID = '$transactionID')";
							}
							$valueInUGSh=getValueInUGSh($select1);
							sendEmail2Client($clientEmail,$receiverFirstName,$clientFirstName,$pin1,$itemID,$transactionID);
							sendEmail2Receiver($receiverEmail,$receiverFirstName,$clientFirstName,$pin1,$itemID,$transactionID);
							sendEmail2Phone($receiverPhone,$receiverFirstName,$clientFirstName,$pin1,$valueInUGSh,$itemID);
						}
						else if($pin2Available&&($select2==$itemID)){
							$usedCardsQuery=createUsedCardsQuery($pin2,$select2,$isFirstField,$transactionID,$clientEmail,$receiverEmail, $usedCardsQuery, $username);
							$usedCardExists=true;
							$pin2Available=false;
							$numberOfCardsInserted=$numberOfCardsInserted-1;
						
							if($isFirstField){
								$updateQuery=" (transactionID = '$transactionID')";
							}	
							else{
								$updateQuery=$updateQuery." OR "."(transactionID = '$transactionID')";
							}
						
							$valueInUGSh=getValueInUGSh($select2);
							sendEmail2Client($clientEmail,$receiverFirstName,$clientFirstName,$pin2,$itemID,$transactionID);
							sendEmail2Receiver($receiverEmail,$receiverFirstName,$clientFirstName,$pin2,$itemID,$transactionID);
							sendEmail2Phone($receiverPhone,$receiverFirstName,$clientFirstName,$pin2,$valueInUGSh,$itemID);
		
						}
						else if($pin3Available&&($select3==$itemID)){
							$usedCardsQuery=createUsedCardsQuery($pin3,$select3,$isFirstField,$transactionID,$clientEmail,$receiverEmail, $usedCardsQuery, $username);
							$usedCardExists=true;
							$pin3Available=false;
							$numberOfCardsInserted=$numberOfCardsInserted-1;
						
							if($isFirstField){
								$updateQuery=" (transactionID = '$transactionID')";
							}
							else{
								$updateQuery=$updateQuery." OR "."(transactionID = '$transactionID')";
							}
							$valueInUGSh=getValueInUGSh($select3);
							sendEmail2Client($clientEmail,$receiverFirstName,$clientFirstName,$pin3,$itemID,$transactionID);
							sendEmail2Receiver($receiverEmail,$receiverFirstName,$clientFirstName,$pin3,$itemID,$transactionID);
							sendEmail2Phone($receiverPhone,$receiverFirstName,$clientFirstName,$pin3,$valueInUGSh,$itemID);			
						}
						else if($pin4Available&&($select4==$itemID)){
							$usedCardsQuery=createUsedCardsQuery($pin4,$select4,$isFirstField,$transactionID,$clientEmail,$receiverEmail, $usedCardsQuery, $username);
							$usedCardExists=true;
							$pin4Available=false;
							$numberOfCardsInserted=$numberOfCardsInserted-1;
						
							if($isFirstField){
								$updateQuery=" (transactionID = '$transactionID')";
							}
							else{
								$updateQuery=$updateQuery." OR "."(transactionID = '$transactionID')";
							}
							$valueInUGSh=getValueInUGSh($select4);
							sendEmail2Client($clientEmail,$receiverFirstName,$clientFirstName,$pin4,$itemID,$transactionID);
							sendEmail2Receiver($receiverEmail,$receiverFirstName,$clientFirstName,$pin4,$itemID,$transactionID);
							sendEmail2Phone($receiverPhone,$receiverFirstName,$clientFirstName,$pin4,$valueInUGSh,$itemID);
						
						}
						else if($pin5Available&&($select5==$itemID)){
							$usedCardsQuery=createUsedCardsQuery($pin5,$select5,$isFirstField,$transactionID,$clientEmail,$receiverEmail, $usedCardsQuery, $username);
							$usedCardExists=true;
							$pin5Available=false;
							$numberOfCardsInserted=$numberOfCardsInserted-1;
						
							if($isFirstField){
								$updateQuery=" (transactionID = '$transactionID')";
							}
							else{
								$updateQuery=$updateQuery." OR "."(transactionID = '$transactionID')";
							}
							$valueInUGSh=getValueInUGSh($select5);
							sendEmail2Client($clientEmail,$receiverFirstName,$clientFirstName,$pin5,$itemID,$transactionID);
							sendEmail2Receiver($receiverEmail,$receiverFirstName,$clientFirstName,$pin5,$itemID,$transactionID);

							sendEmail2Phone($receiverPhone,$receiverFirstName,$clientFirstName,$pin5,$valueInUGSh,$itemID);
							
						}
						else if($pin6Available&&($select6==$itemID)){
							$usedCardsQuery=createUsedCardsQuery($pin6,$select6,$isFirstField,$transactionID,$clientEmail,$receiverEmail, $usedCardsQuery, $username);
							$usedCardExists=true;
							$pin6Available=false;
							$numberOfCardsInserted=$numberOfCardsInserted-1;
							
							if($isFirstField){
								$updateQuery=" (transactionID = '$transactionID')";
							}
							else{
								$updateQuery=$updateQuery." OR "."(transactionID = '$transactionID')";
							}
							$valueInUGSh=getValueInUGSh($select6);
							sendEmail2Client($clientEmail,$receiverFirstName,$clientFirstName,$pin6,$itemID,$transactionID);
							sendEmail2Receiver($receiverEmail,$receiverFirstName,$clientFirstName,$pin6,$itemID,$transactionID);
							sendEmail2Phone($receiverPhone,$receiverFirstName,$clientFirstName,$pin6,$valueInUGSh,$itemID);
							
						}
						else if($pin7Available&&($select7==$itemID)){
							$usedCardsQuery=createUsedCardsQuery($pin7,$select7,$isFirstField,$transactionID,$clientEmail,$receiverEmail, $usedCardsQuery, $username);
							$usedCardExists=true;
							$pin7Available=false;
							$numberOfCardsInserted=$numberOfCardsInserted-1;
							
							if($isFirstField){
								$updateQuery=" (transactionID = '$transactionID')";
							}
							else{
								$updateQuery=$updateQuery." OR "."(transactionID = '$transactionID')";
							}
							$valueInUGSh=getValueInUGSh($select7);
							sendEmail2Client($clientEmail,$receiverFirstName,$clientFirstName,$pin7,$itemID,$transactionID);
							sendEmail2Receiver($receiverEmail,$receiverFirstName,$clientFirstName,$pin7,$itemID,$transactionID);
							sendEmail2Phone($receiverPhone,$receiverFirstName,$clientFirstName,$pin7,$valueInUGSh,$itemID);
						}
						else if($pin8Available&&($select8==$itemID)){
							$usedCardsQuery=createUsedCardsQuery($pin8,$select8,$isFirstField,$transactionID,$clientEmail,$receiverEmail, $usedCardsQuery, $username);
							$usedCardExists=true;
							$pin8Available=false;
							$numberOfCardsInserted=$numberOfCardsInserted-1;
							
							if($isFirstField){
								$updateQuery=" (transactionID = '$transactionID')";
							}
							else{
								$updateQuery=$updateQuery." OR "."(transactionID = '$transactionID')";
							}
										$valueInUGSh=getValueInUGSh($select8);
							sendEmail2Client($clientEmail,$receiverFirstName,$clientFirstName,$pin8,$itemID,$transactionID);
							sendEmail2Receiver($receiverEmail,$receiverFirstName,$clientFirstName,$pin8,$itemID,$transactionID);
							sendEmail2Phone($receiverPhone,$receiverFirstName,$clientFirstName,$pin8,$valueInUGSh,$itemID);
				
						}
						else if($pin9Available&&($select9==$itemID)){
							$usedCardsQuery=createUsedCardsQuery($pin9,$select9,$isFirstField,$transactionID,$clientEmail,$receiverEmail, $usedCardsQuery, $username);
							$usedCardExists=true;
							$pin9Available=false;
							$numberOfCardsInserted=$numberOfCardsInserted-1;
							
							if($isFirstField){
								$updateQuery=" (transactionID = '$transactionID')";
							}
							else{
								$updateQuery=$updateQuery." OR "."(transactionID = '$transactionID')";
							}
							$valueInUGSh=getValueInUGSh($select9);
							sendEmail2Client($clientEmail,$receiverFirstName,$clientFirstName,$pin9,$itemIDh,$transactionID);
							sendEmail2Receiver($receiverEmail,$receiverFirstName,$clientFirstName,$pin9,$itemID,$transactionID);
							sendEmail2Phone($receiverPhone,$receiverFirstName,$clientFirstName,$pin9,$valueInUGSh,$itemID);
							
						}
						else if($pin10Available&&($select10==$itemID)){
							$usedCardsQuery=createUsedCardsQuery($pin10,$select10,$isFirstField,$transactionID,$clientEmail,$receiverEmail, $usedCardsQuery, $username);
							$usedCardExists=true;
							$pin10Available=false;
							$numberOfCardsInserted=$numberOfCardsInserted-1;
							
							if($isFirstField){
								$updateQuery=" (transactionID = '$transactionID')";
							}
							else{
								$updateQuery=$updateQuery." OR "."(transactionID = '$transactionID')";
							}
							$valueInUGSh=getValueInUGSh($select10);
							sendEmail2Client($clientEmail,$receiverFirstName,$clientFirstName,$pin10,$itemID,$transactionID);
							sendEmail2Receiver($receiverEmail,$receiverFirstName,$clientFirstName,$pin10,$itemID,$transactionID);
							sendEmail2Phone($receiverPhone,$receiverFirstName,$clientFirstName,$pin10,$valueInUGSh,$itemID);
				
						}
						else if($pin11Available&&($select11==$itemID)){
							$usedCardsQuery=createUsedCardsQuery($pin11,$select11,$isFirstField,$transactionID,$clientEmail,$receiverEmail, $usedCardsQuery, $username);
							$usedCardExists=true;
							$pin11Available=false;
							$numberOfCardsInserted=$numberOfCardsInserted-1;
							
							if($isFirstField){
								$updateQuery=" (transactionID = '$transactionID')";
							}
							else{
								$updateQuery=$updateQuery." OR "."(transactionID = '$transactionID')";
							}
							$valueInUGSh=getValueInUGSh($select11);
							sendEmail2Client($clientEmail,$receiverFirstName,$clientFirstName,$pin11,$itemID,$transactionID);
							sendEmail2Receiver($receiverEmail,$receiverFirstName,$clientFirstName,$pin11,$itemID,$transactionID);
							sendEmail2Phone($receiverPhone,$receiverFirstName,$clientFirstName,$pin11,$valueInUGSh,$itemID);
							
						}
						//may need to insert this into the if ($isFirstField)
						//seems like bug
						$isFirstField=false;
								
						$counter=$counter+1;
					}//end if $dispatchStatus=='pinready'
					else{//dispatchStatus is set to autoready and hence only the loader for auto airtime can send it
						echo"Delivery of this user is set to automatic<br>";
						//this users airtime will be delivered automatically
						$numberOfOutgoingUsers=$numberOfOutgoingUsers-1;
					}
					
					
				}//end while loop (($numberOfCardsInserted!=0)&&($outgoingQueueData = mysql_fetch_array($outgoingQueueResult)))
		
				
				if($numberOfOutgoingUsers>0){//if we have people in the outgoing queue
					//mark outgoing queue rows for deletion 
					if($updateQuery==''){
					}
					else{//$updateQuery is not empty
						$updateQuery="UPDATE OutgoingQueue SET dispatchStatus='delete' WHERE".$updateQuery;
						$updateQueryResult = mysql_query($updateQuery) or handleDatabaseError(''. mysql_error(),$updateQuery);
						//echo"update query = $updateQuery updateQueryResult=$updateQueryResult <br>";
					
						//delete outgoing queue rows
						$deleteQuery="DELETE FROM OutgoingQueue WHERE dispatchStatus='delete'";
						$deleteQueryResult = mysql_query($deleteQuery) or handleDatabaseError(''. mysql_error(),$deleteQuery);
						echo"delete Query = $deleteQuery <br>";
					}
					
					//unlocking outgoingqueue
					$lockQuery="UNLOCK TABLES";
					$lockResult=mysql_query($lockQuery) or handleDatabaseError(''. mysql_error(),$lockQuery);		
			
					//insert into used cards
					if($usedCardExists){//if there are cards we used to send to people
						echo"used cards query = $usedCardsQuery <br>";
						$usedCardsResult = mysql_query($usedCardsQuery) or handleDatabaseError(''. mysql_error(),$usedCardsQuery);
						echo"UsedCards DB has been updated";
					}
				}//end if ($numberOfOutgoingUsers>0)
				else{
					//unlocking outgoingqueue
					$lockQuery="UNLOCK TABLES";
					$lockResult=mysql_query($lockQuery) or handleDatabaseError(''. mysql_error(),$lockQuery);		
				}
					
				//insert the remaining cards into the DB
				//for database we need 
				//cardPIN, networkName, valueInUGSh, ourPriceInUSD, dateWePurchased, cardStatus, itemID
				//CREATING QUERY BEGIN---------------------
				$insertQuery="INSERT INTO NewCards (cardPIN, networkName, valueInUGSh, ourPriceInUSD, dateWePurchased, cardStatus, itemID, loadedBy, loadersIP)
								VALUES";
				$isFirstField=true;
				$numberOfNewCardsToInsert=0;
				
				//textfield1
				//$cardPIN=$pin1
				//networkName
				if($pin1Available){//the only way to know if the card has been used is by checking if its set to ''
					//create string to add to query
					$insertQuery=createQuery($pin1,$select1,$isFirstField,$insertQuery,$username);
					$isFirstField=false;
					$numberOfNewCardsToInsert=$numberOfNewCardsToInsert+1;
				}
				if($pin2Available){
					//create string to add to query
					$insertQuery=createQuery($pin2,$select2,$isFirstField,$insertQuery,$username);
					$isFirstField=false;
					$numberOfNewCardsToInsert=$numberOfNewCardsToInsert+1;
				}
				if($pin3Available){
					//create string to add to query
					$insertQuery=createQuery($pin3,$select3,$isFirstField,$insertQuery,$username);
					$isFirstField=false;
					$numberOfNewCardsToInsert=$numberOfNewCardsToInsert+1;
				}
				if($pin4Available){
					//create string to add to query
					$insertQuery=createQuery($pin4,$select4,$isFirstField,$insertQuery,$username);
					$isFirstField=false;
					$numberOfNewCardsToInsert=$numberOfNewCardsToInsert+1;		
				}
				if($pin5Available){
					//create string to add to query
					$insertQuery=createQuery($pin5,$select5,$isFirstField,$insertQuery,$username);
					$isFirstField=false;
					$numberOfNewCardsToInsert=$numberOfNewCardsToInsert+1;		
				}
				if($pin6Available){
					//create string to add to query
					$insertQuery=createQuery($pin6,$select6,$isFirstField,$insertQuery,$username);
					$isFirstField=false;
					$numberOfNewCardsToInsert=$numberOfNewCardsToInsert+1;		
				}
				if($pin7Available){
					//create string to add to query
					$insertQuery=createQuery($pin7,$select7,$isFirstField,$insertQuery,$username);
					$isFirstField=false;
					$numberOfNewCardsToInsert=$numberOfNewCardsToInsert+1;		
				}
				if($pin8Available){
					//create string to add to query
					$insertQuery=createQuery($pin8,$select8,$isFirstField,$insertQuery,$username);
					$isFirstField=false;
					$numberOfNewCardsToInsert=$numberOfNewCardsToInsert+1;		
				}
				if($pin9Available){
					//create string to add to query
					$insertQuery=createQuery($pin9,$select9,$isFirstField,$insertQuery,$username);
					$isFirstField=false;
					$numberOfNewCardsToInsert=$numberOfNewCardsToInsert+1;		
				}
				if($pin10Available){
					//create string to add to query
					$insertQuery=createQuery($pin10,$select10,$isFirstField,$insertQuery,$username);
					$isFirstField=false;
					$numberOfNewCardsToInsert=$numberOfNewCardsToInsert+1;		
				}
				if($pin11Available){
					//create string to add to query
					$insertQuery=createQuery($pin11,$select11,$isFirstField,$insertQuery,$username);
					$isFirstField=false;
					$numberOfNewCardsToInsert=$numberOfNewCardsToInsert+1;		
				}
				
					
				//CREATING QUERY END---------------------
				if($numberOfNewCardsToInsert>0){//if there are new cards left after giving people in outgoing queue
					//now that we have the query, all we have to do is execute it
					$insertresult = mysql_query($insertQuery) or handleDatabaseError(''. mysql_error(),$insertQuery);
					//echo"Database has been loaded resultset = $insertresult";
					if($debug){ echo"insertQuery = $insertQuery";}
					

					$message.= "Cards input into input fields: $number_of_cards_input\n";
					$message.= "Cards loaded into DB: $numberOfNewCardsToInsert\n";
					
					if($usedCardExists)
					{
						$numberSentToOutQ = $number_of_cards_input-$numberOfNewCardsToInsert;
						$message.= "Cards sent to users in outgoing queue: $numberSentToOutQ\n";
					}
					
					for( $j=0 ; $j<count($filterArray) ; $j++ )
					{
						$message.= $filterArray[$j]." : ".$airtimeTypeArray[$j]."\n";
					}
				}
				else{
					$cards_dispatched = "All cards were dispatched to people in the outgoing queue<br>";
					echo $cards_dispatched;
					
					$message.= $cards_dispatched."\n";
				}
			
			}//end if ($numberOfOccurences==0 && $numberOfUsedCards==0)
			else{
				$duplicates_msg = "You entered a card that already exists in DB please verify and enter only those that do not exist<br>";
				echo $duplicates_msg;

				$message.= "Cards Loaded: 0\n";
				$message.= "Occurences in NewCards = $numberOfOccurences\n";
				$message.= "Occurences in UsedCards = $numberOfUsedCards\n";
				$message.= $duplicates_msg."\n";
				
				for( $j=0 ; $j<count($filterArray) ; $j++ )
				{
					echo $filterArray[$j]." : ".$airtimeTypeArray[$j]."<br>";
                    $message.= $filterArray[$j]." : ".$airtimeTypeArray[$j]."\n";
				}
			}
		}//end if number of cards to be inserted is zero
		else{
			$fields_blank_msg = "All fields were blank<br>";
			echo $fields_blank_msg;
			
			$message.= $fields_blank_msg."\n";
		}
		
		/* Closing connection */
	}//end if password correct
	else
	{
		if($input_contains_duplicates)
		{
			$duplicate_message = "INPUT CONTAINS DUPLICATES WITHIN BATCH PLEASE RE-ENTER ALL<br>";
			echo $duplicate_message;

			$message.= "$duplicate_message\n";

			for( $j=0 ; $j<count($filterArray) ; $j++ )
			{
				echo $filterArray[$j]." : ".$airtimeTypeArray[$j]."<br>";
                $message.= $filterArray[$j]." : ".$airtimeTypeArray[$j]."\n";
			}
		}
		else
		{
			$password_incorrect = "INCORRECT USERNAME AND PASSWORD<br>";
			echo $password_incorrect;
			$message.= "$password_incorrect\n";
		}
	}
	disconnectDB($connection);


	$subject = "Cards Load Report: ".date("Y-m-d H:i:s");
	$toAddress = TECH_ADMIN;
	$message.= "\n\n---3nitylabs Software Inc.\n";
	sendEmail2($fromName,$fromAddress,$toAddress,$bcc,$subject,$message,$countryAbbreviation);
    $toAddress = LOADER_EMAIL;
    sendEmail2($fromName,$fromAddress,$toAddress,$bcc,$subject,$message,$countryAbbreviation);

}//end if GET(POST)
?>