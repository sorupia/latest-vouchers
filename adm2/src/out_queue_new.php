<?
	include "wrapper/header.php";
?>

<table cellpadding="0" cellspacing="0" class="listing">
	<tr>
		<th colspan="6"><h1>Outgoing Queue - New Client Purchases</h1></th>
	</tr>
<?
	include "out_queue_nav.php";
?>
</table>

<table cellpadding="0" cellspacing="0" class="listing">
<?

	$values   = array("transID","emailAddress",
	                  "receiverEmail","receiverPhone",
	                  "itemIDNumber","dateOfFirstPurchase",
					  "paymentStatus","firstName","addressCountry",
					  "ipCountry","clientIP","cellphone",
					  "receiverFirstName");
	
	$result = render_pager($values, "NewClientsQueue", "dateOfFirstPurchase DESC", 15, $countryAbbreviation, 'airtime');
	$exist = mysql_num_rows($result);
	
	if($exist)
	{
		echo'<thead>
			<tr>
			<th>Option</th>
			<th>Time Stamp</th>
			<th>Client Email</th>
			<th>Transaction ID</th>
			<th>Item ID</th>
			<th>Receiver Phone</th>
			<th>Payment Status</th>
			<th>Client</th>
			<th>Client Phone</th>
			<th>aCountry</th>
			<th>iCountry</th>
			<th>Client IP</th>
			<th>Receiver</th>			
			<th>Receiver Email</th>
			</tr>
		</thead>';
						
		while($data = mysql_fetch_array($result))
		{
			
			echo'<tbody><tr>				 
				<td>'.
				"<a href='verify_new_client_transaction.php?id=".$data['transID'].
				"&page=$thisPage' onfocus='this.blur();' title='Verify Transaction'>".
				"Verify</a> | ".
				"<a href='block_new_client_transaction.php?id=".$data['transID'].
				"&page=$thisPage' onfocus='this.blur();' title='Blacklist Client'>".
				"Blacklist</a> | ".
				"<a href='#' onfocus='this.blur();' 
				title='Delete Transaction' onclick=delEntry('".$data['transID']."','NewClientsQueue','transID','$thisPage');>
				Delete</a>".
			'</td>
			<td>'.$data['dateOfFirstPurchase'].'</td>
			<td>'.$data['emailAddress'].'</td>
			<td>'.$data['transID'].'</td>
			<td>'.$data['itemIDNumber'].'</td>
			<td>'.$data['receiverPhone'].'</td>
			<td>'.$data['paymentStatus'].'</td>
			<td>'.$data['firstName'].'</td>
			<td>'.$data['cellphone'].'</td>
			<td>'.$data['addressCountry'].'</td>
			<td>'.$data['ipCountry'].'</td>
			<td>'.$data['clientIP'].'</td>
			<td>'.$data['receiverFirstName'].'</td>
			<td>'.$data['receiverEmail'].'</td>
			</tr></tbody>';
			
			$u++;
		}
	}
	else
	{
		echo '<tr><td align="center" colspan="4">No users in New client purchases queue</td></tr>';
	}
?>

</table>

<?
	include "wrapper/footer.php";
?>