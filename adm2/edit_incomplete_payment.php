<?
	include "wrapper/header.php";
	
	$connection = connect2DB2($countryAbbreviation);
	$query		= "SELECT * FROM IncompletePaymentsQueue WHERE id = '$_GET[id]'";
	$result		= mysql_query($query);
	$data		= mysql_fetch_array($result);
	disconnectDB($connection);
?>
<table cellpadding="0" cellspacing="0" class="listing">

<tr>
	<th><h1><a href="javascript:history.go(-1);" class="sub_menu" title="back" onfocus="this.blur();">&laquo; Back</a> Edit Incomplete Payment</h1></th>
</tr>

<?
	include "out_queue_nav.php";
?>

</table>

<br />

<table cellpadding="0" cellspacing="0" class="listing">

<tr>
    <td align="left">
         <form name="editIncompletePayment" id="editIncompletePayment" method="post" action="controllers/cases.php">
            <div class="left">
			    <div class="row">
                    <label for="transactionID">Transaction ID</label>
                    <input type="text" name="transactionID" id="transactionID" class="field" value="<?=$data['transactionID']?>" />
				</div>
				<div class="row">
                    <label for="timeOfPayment">Time Of Payment</label>
                    <input type="text" name="timeOfPayment" id="timeOfPayment" class="field" value="<?=$data['timeOfPayment']?>" />
				</div>
				<div class="row">
                    <label for="clientEmail">Client Email</label>
                    <input type="text" name="clientEmail" disabled="disabled" id="clientEmail" class="field" value="<?=$data['clientEmail']?>" />
                </div>
				<div class="row">
                    <label for="receiverPhone">Receiver Phone</label>
                    <input type="text" name="receiverPhone" id="receiverPhone" class="field" value="<?=$data['receiverPhone']?>" 
					 disabled="disabled" />
                </div>
				<div class="row">
                    <label for="receiverFirstName">Receiver First Name</label>
                    <input type="text" name="receiverFirstName" disabled="disabled" id="receiverFirstName" class="field" 
					 value="<?=$data['receiverFirstName']?>"/>
                </div>
				<div class="row">
                    <label for="receiverEmail">Receiver Email</label>
                    <input type="text" name="receiverEmail"  id="receiverEmail" class="field" value="<?=$data['receiverEmail']?>" 
					 disabled="disabled" />
                </div>
                <div class="row">
                    <label for="comments">Comments</label>
                    <input type="text" name="comments" id="comments" class="field" value="<?=$data['comments']?>" />
                </div>
            </div>
            <div class="clear"></div>
            <div class="row">
				<label>
                    <input type="submit" name="submit" id="submit" value="Modify" class="submit" onfocus="this.blur();"   />
			    </label>
                <input type="button" name="cancel" id="cancel" value="Cancel" class="submit" style="margin-top: 5px;" onclick="history.go(-1);"onfocus="this.blur();" />
            </div>
            <div class="clear">&nbsp;</div>
            <input type="hidden" name="type" id="type" value="editIncompletePayment" />
            <input type="hidden" name="id" id="id" value="<?=$data['id']?>" />
			<input type="hidden" name="page" id="page" value="<?=$_GET['page']?>" />
			<input type="hidden" name="employee_username" id="employee_username" value="<?=$_SESSION['username']?>" />
        </form>
    </td>
</tr>

</table>

<? include "wrapper/footer.php"; ?>