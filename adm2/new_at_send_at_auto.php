<?
	include "wrapper/header.php";
?>

<?
    //calculate the transaction id
    $trans_id_date_part = date("YmdHis");
    $session_id = session_id();
    $trans_id_sess_id_part = substr($session_id,-4);

    if($_POST['transID'])
    {
        $transaction_id = $_POST['transID'];
    }
    else if(TRANSACTION_ID_PREFIX == "")
    {
        $transaction_id = $trans_id_date_part.$trans_id_sess_id_part;
    }
    else
    {
        $transaction_id = TRANSACTION_ID_PREFIX."_".$trans_id_date_part.$trans_id_sess_id_part;
    }

    $clientEmail              = (array_key_exists('clientEmail',$_POST)) ? $_POST['clientEmail'] : "".DEFAULT_CLIENT_EMAIL;
    $clientFirstName          = (array_key_exists('clientFirstName',$_POST)) ? $_POST['clientFirstName'] : "";
    $receiverFirstName        = (array_key_exists('receiverFirstName',$_POST)) ? $_POST['receiverFirstName'] : "";
    $receiverPhone            = (array_key_exists('receiverPhone',$_POST)) ? $_POST['receiverPhone'] : "+".COUNTRY_CODE;
    $phoneConfirm             = (array_key_exists('phoneConfirm',$_POST)) ? $_POST['phoneConfirm'] : "+".COUNTRY_CODE;
    $receiver_amount_in_local = (array_key_exists('receiver_amount_in_local',$_POST)) ? $_POST['receiver_amount_in_local'] : "";
    $receiverEmail            = (array_key_exists('receiverEmail',$_POST)) ? $_POST['receiverEmail'] :"";
?>

<table cellpadding="0" cellspacing="0" class="listing">
	<tr>
		<th colspan="6"><h1>Send Airtime as Auto Load</h1></th>
	</tr>

<?
	include "send_at_header_nav.php";
?>

</table>

<table cellpadding="0" cellspacing="0" class="listing">

<tr>
    <td align="left">
	<p>Use this form to manually send auto load airtime to clients.</p>
      <? if(array_key_exists("error",$_POST)) echo $_POST['error']; ?>
         <form name="manual_send_auto_at" id="manual_send_auto_at" method="post" action="confirm_new_at_send_at_auto.php">
            <div class="left">
              <div class="row">
                    <label for="employeeUsername">employee username</label>
                    <input type="text" name="employeeUsername" id="employeeUsername" class="field" value="<?=$_SESSION['uid']?>" />
                </div>
                <div class="clear"></div>
                <div class="row">
                    <label for="employeePassword">employee password</label>
                    <input type="password" name="employeePassword" id="employeePassword" class="field" value="" />
                </div>
                <div class="row">
                    <label for="transID">transaction id</label>
                    <input type="text" name="transID" id="transID" class="field" value="<?=$transaction_id?>" />
                </div>				
                <div class="row">
                    <label for="clientEmail">clients email</label>
                    <input type="text" name="clientEmail" id="clientEmail" class="field" value="<?=$clientEmail?>" />
                </div>
                <div class="row">
                    <label for="clientFirstName">clients first name</label>
                    <input type="text" name="clientFirstName" id="clientFirstName" class="field" value="<?=$clientFirstName?>" />
                </div>
                <div class="row">
                    <label for="receiverFirstName">receiver first name</label>
                    <input type="text" name="receiverFirstName" id="receiverFirstName" class="field" value="<?=$receiverFirstName?>" />
                    optional
                </div>
				<div class="row">
                    <label for="receiverPhone">receiver mobile phone</label>
                    <input type="text" name="receiverPhone" id="receiverPhone" class="field" value="<?=$receiverPhone?>" />
                </div>
				<div class="row">
                    <label for="phoneConfirm">receiver mobile phone</label>
                    <input type="text" name="phoneConfirm" id="phoneConfirm" class="field" value="<?=$phoneConfirm?>" />
                </div>
                <div class="row">
                    <label for="itemID">Receiver Amount in Local</label>
                    <input type="text" name="receiver_amount_in_local" id="receiver_amount_in_local" class="field" value="<?=$receiver_amount_in_local?>" />
                </div>
                <div class="row">
                    <label for="receiverEmail">receiver email</label>
                    <input type="text" name="receiverEmail" id="receiverEmail" class="field" value="<?=$receiverEmail?>" />
                optional</div>
            </div>
            <div class="clear"></div>
            <div class="row">
            	<label><input type="Submit" name="Submit" id="Submit" value="Submit" class="submit" onfocus="this.blur();" /></label>
                <input type="button" name="cancel" id="cancel" value="Cancel" class="submit" style="margin-top: 5px;" onclick="history.go(-1);" onfocus="this.blur();" />
            </div>
            <div class="clear">&nbsp;</div>
            <input type="hidden" name="type" id="type" value="manual_send_auto_at" />
            <input type="hidden" name="airtime_load_option" id="type" value="<?=AIRTIME_LOAD_OPTION_AUTO?>">
        </form>
    </td>
</tr>


</table>

<?
	include "wrapper/footer.php";
?>