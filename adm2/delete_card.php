<?
	include "wrapper/header.php";
	
	$connection = connect2DB2($countryAbbreviation);
	$query		= "SELECT * FROM NewCards WHERE cardID = '$_GET[id]'";
	$result		= mysql_query($query);
	$data		= mysql_fetch_array($result);
	disconnectDB($connection);
?>
<table cellpadding="0" cellspacing="0" class="listing">

<tr>
	<th><h1><a href="javascript:history.go(-1);" class="sub_menu" title="back" onfocus="this.blur();">&laquo; Back</a> Delete New Card</h1></th>
</tr>

<?
	include "new_at_header_nav.php";
?>

</table>

<br />

<table cellpadding="0" cellspacing="0" class="listing">

<tr>
    <td align="left">
         <form name="deleteCard" id="deleteCard" method="post" action="controllers/cases.php">
            <div class="left">
              <div class="row">
                    <label for="dateWePurchased">Date of Purchase</label>
                    <input type="text" name="dateWePurchased" disabled="disabled" id="dateWePurchased" class="field" value="<?=$data['dateWePurchased']?>" />
                </div>
                <div class="clear"></div>
                <div class="row">
                    <label for="itemID">Item ID</label>
					<select name="itemID" disabled="disabled" id="itemID">
					<?
						include_once "itemIDDropDown.php";
						getDropDownSelected($countryAbbreviation, $data['itemID']);
					?>
					</select>
                </div>
                <div class="row">
                    <label for="ourPriceInUSD">Our Price in USD</label>
                    <input type="text" name="ourPriceInUSD" id="ourPriceInUSD" class="field" value="<?=$data['ourPriceInUSD']?>" />
                </div>
				<div class="row">
                    <label for="cardStatus">Card Status</label>
					<input type="hidden" name="originalCardStatus" id="originalCardStatus" value="<?=$data['cardStatus']?>" />
					<select name="cardStatus" disabled="disabled" id="cardStatus" >
					<?
						$statusValues = array("USED","NEW");				
					    for($i = 0 ; $i < count($statusValues) ; $i ++ )
						{
							$status = $statusValues[$i];
							if ( $status == $data['cardStatus'] )
							{
								echo"<option value=\"$status\" selected=\"selected\">$status</option>";
							}
							else
							{
								echo"<option value=\"$status\">$status</option>";
							}
						}
					?>
                    </select>
                </div>
				<div class="row">
                    <label for="loadedBy">Loaded By</label>
					<input type="text" name="loadedBy" disabled="disabled" id="loadedBy" class="field" value="<?=$data['loadedBy']?>" />
                </div>				
                <div class="row">
                    <label for="loadersIP">Loaders IP</label>
                    <input type="text" name="loadersIP" disabled="disabled" id="loadersIP" class="field" value="<?=$data['loadersIP']?>" />
                </div>
                <div class="row">
                    <label for="serialNumber">Serial Number</label>
                    <input type="text" name="serialNumber" disabled="disabled" id="serialNumber" class="field" value="<?=$data['serialNumber']?>" />
                </div>
				<div class="row">
                    <label for="cardPIN">Card PIN</label>
                    <input type="text" name="cardPIN" disabled="disabled" id="cardPIN" class="field" value="<?=$data['cardPIN']?>" />
                </div>
            </div>
            <div class="clear"></div>
            <div class="row">
				<label><input type="submit" name="submit" id="submit" value="Delete" class="submit" onfocus="this.blur();" /></label>
                <input type="button" name="cancel" id="cancel" value="Cancel" class="submit" style="margin-top: 5px;" onclick="history.go(-1);"onfocus="this.blur();" />
            </div>
            <div class="clear">&nbsp;</div>
            <input type="hidden" name="type" id="type" value="deleteCard" />
            <input type="hidden" name="cardID" id="cardID" value="<?=$_GET['id']?>" />
			<input type="hidden" name="cardPIN" id="cardPIN" value="<?=$data['cardPIN']?>" />
			<input type="hidden" name="page" id="page" value="<?=$_GET['page']?>" />
			<input type="hidden" name="employee_username" id="employee_username" value="<?=$_SESSION['uid']?>" />
        </form>
    </td>
</tr>

</table>

<? include "wrapper/footer.php"; ?>