<?php
/*******************************************************************************
**   FILE: contactus.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Handles the display of UgandaVouchers contact information and header/footer files
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   
**
**   ADAPTED BY: 
**
*********************************************************************************/
?>
<?php 

include("wrapper/header.php"); 

include(ACTIVE_THEME_PATH ."/contactus.php"); 

include("wrapper/footer.php");

?>
