<?php
/*******************************************************************************
**   FILE: contactus.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Handles the display of the frequently asked questions content and header/footer files
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   
**
**   ADAPTED BY: 
**
*********************************************************************************/
?>
<?php 

include("wrapper/header.php"); 

include(ACTIVE_THEME_PATH .'/faq.php');

include("wrapper/footer.php")
?>
