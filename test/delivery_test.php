<?php
/*******************************************************************************
**   FILE: delivery_test.php
**
**   FUNCTION: Test successful delivery
**
**   WRITTEN BY: Daniel Agaba
*********************************************************************************/

include_once $_SERVER['DOCUMENT_ROOT']."/src/Delivery/DeliveryFunctions.php"; 

$xml_location = trim(file_get_contents('php://input'));
$xml_in = simplexml_load_string($xml_location);

$xml_out = delivery_process_notification($xml_in);



?>