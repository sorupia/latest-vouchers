<?php

$url = "http://vouchers-ni.3nitylabs.com/test/delivery_test.php";

if(isset($_POST['saveForm']))
{
	$network_transaction_id_pending = $_POST['network_transaction_id_pending'];
	$network_transaction_id_delivered = $_POST['network_transaction_id_delivered'];
	$cardPIN = $_POST['cardPIN'];
	$serialNumber = $_POST['serialNumber'];	

	$success_delivery_xml  = "<?xml version='1.0' encoding='utf-8'?>";
    $success_delivery_xml .= "<Delivery>";
    $success_delivery_xml .= "<network_transaction_id_pending>".$network_transaction_id_pending."</network_transaction_id_pending>";
    $success_delivery_xml .= "<network_transaction_id_delivered>".$network_transaction_id_delivered."</network_transaction_id_delivered>";
    $success_delivery_xml .= "<cardPIN>".$cardPIN."</cardPIN>";
    $success_delivery_xml .= "<serialNumber>".$serialNumber."</serialNumber>";
    $success_delivery_xml .= "</Delivery>";
   
	$ch = curl_init();
	curl_setopt( $ch, CURLOPT_URL, $url );
	curl_setopt( $ch, CURLOPT_POST, true );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $success_delivery_xml);
	
	$data = curl_exec($ch); 

	if(curl_errno($ch)) {
		print curl_error($ch);
	}

	curl_close($ch);

	//header("Content-type: text/xml");
	echo $data;

} else {

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Successful Delivery Test Form</title>
<link rel="stylesheet" type="text/css" href="form_css/view.css" media="all">
<script type="text/javascript" src="form_js/view.js"></script>
<script type="text/javascript" src="form_js/calendar.js"></script>
</head>
<body id="main_body" >
	
	<img id="top" src="form_img/top.png" alt="">
	<div id="form_container">

	<h1><a>Successful Delivery Request Form</a></h1>

	<form id="form_1072263" class="appnitro"  method="post">
		<div class="form_description">
		<h2>Successful Delivery Request Form</h2>
		<p></p>
		</div>						
		<ul >
			<li id="li_1" >
			<label class="description" for="element_1">network transaction id pending </label>	
			<div>
			<input id="element_1" name="network_transaction_id_pending" class="element text medium" type="text" maxlength="255" value=""/> 
			</div> 	
			</li>	

			<li id="li_2" >
			<label class="description" for="element_2">network transaction id delivery </label>
			<div>
			<input id="element_2" name="network_transaction_id_delivered" class="element text medium" type="text" maxlength="255" value=""/> 
			</div> 
			</li>

			<li id="li_3" >
			<label class="description" for="element_3">Card PIN</label>
			<div>
			<input id="element_3" name="cardPIN" class="element text medium" type="text" maxlength="255" value=""/> 
			</div> 
			</li>		
			
			<li id="li_4" >
			<label class="description" for="element_4">serial Number </label>
			<div>
			<input id="element_4" name="serialNumber" class="element text medium" type="text" maxlength="255" value=""/> 
			</div> 
			</li>	
                
             <li class="buttons">
			<input type="hidden" name="form_id" value="1072263" />
			<input id="saveForm" class="button_text" type="submit" name="saveForm" value="Submit" />
			</li>
		</ul>

	</form>	
	<div id="footer">
	</div>
	</div>
	<img id="bottom" src="form_img/bottom.png" alt="">
</body>
</html>

<?php

} 

?>