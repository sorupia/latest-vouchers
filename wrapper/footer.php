<?
/*******************************************************************************
**   FILE: wrapper/footer.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Footer files for web application
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 03.Jul.2012
**
**   ADAPTED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 22.May.2015
**   Adapted for UgandaVouchers.com
**
*********************************************************************************/

include_once(ACTIVE_THEME_PATH . '/footer.php');
?>
        