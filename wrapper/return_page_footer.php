<?
/*******************************************************************************
**   FILE: wrapper/footer.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Footer files for web application
**
**   ADAPTED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2015.12.01
**   Adapted from footer.php because JavaScript may be causing return_page 
**   to be called twice
**
*********************************************************************************/

include_once(ACTIVE_THEME_PATH . '/return_page_footer.php');
?>
        