<?
/*******************************************************************************
**   FILE: wrapper/header.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Header files for web application
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 03.Jul.2012
**
**   MODIFIED BY: Arthur Ntozi (3nitylabs-AVMM, Kampala)   DATE: 30.Nov.2015
**   Code review: replaced tabs for whitespaces
**
*********************************************************************************/

    session_start();
    include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/cards.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/Country.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Security/IPStatusHardCoded.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Security/CountryStatusHardCoded.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Constants/Constants.php";

    $countryAbbreviation=COUNTRY_ABBREVIATION;

    $page = check_me();
    $thisPage = substr($page,0,-4);

    if(!$_SESSION['ATSubscriberLoggedIn'] &&
       ($thisPage == 'user_account' || 
        $thisPage == 'edit_receiver' || 
        $thisPage == 'all_receivers' ||
        $thisPage == 'specific_index'))
    {
        header("Location: index.php");
    }

    $siteTurnedON       = true;
    $country_is_blocked = false;

    //Check if IP is hard code unblocked
    if(IP_HARD_CODE_BLOCK_ENABLED)
    {
        $input_array['ip_address'] = getIP();
        $input_array = IPStatusHardCoded($input_array);
        $ip_hard_coded_unblocked = $input_array['ip_hard_coded_unblocked'];

        //Block hard code blocked IPs
        if($ip_hard_coded_unblocked == 0)
        {
            //echo $input_array['ip_address']."<br>";
            echo "SORRY THE WEBSITE IS TEMPORARILY UNAVAILABLE, ";
            echo "PLEASE TRY AGAIN LATER OR FOR ALTERNATIVE CALL ".FOREIGN_SUPPORT_PHONE." ";
            echo "OR EMAIL ".SUPPORT_EMAIL." ";
            $siteTurnedON = false;
            exit();
        }
    }

    //Query MaxMind
    $ipLocationArray = getIPLocation();

    //Check if Country is hard code unblocked
    if(COUNTRY_HARD_CODE_BLOCK_ENABLED)
    {
        $input_array['ip_address'] = $ipLocationArray['ip_address'];
        $input_array['ip_country'] = $ipLocationArray['ip_country'];
        $input_array = CountryStatusHardCoded($input_array);
        $country_hard_coded_unblocked = $input_array['country_hard_coded_unblocked'];

        //Block hard code blocked countries
        if($country_hard_coded_unblocked == 0)
        {
            //echo $input_array['ip_country']."<br>";
            echo "SORRY THE WEBSITE IS TEMPORARILY UNAVAILABLE, ";
            echo "PLEASE TRY AGAIN LATER OR FOR ALTERNATIVE CALL ".SUPPORT_PHONE." ";
            echo "OR EMAIL ".SUPPORT_EMAIL." ";
            $siteTurnedON = false;
            $country_is_blocked = true;
            exit();
        }
    }

    include_once(ACTIVE_THEME_PATH . "/return_page_header.php");

?>
