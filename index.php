<?php
/*******************************************************************************
**   FILE: index.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Handles the display of the index page and header/footer files
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   
**
**   ADAPTED BY: 
**
*********************************************************************************/
?>
<?
    include "wrapper/header.php";
        include $_SERVER['DOCUMENT_ROOT']. '/'. ACTIVE_THEME_PATH ."/index.php";
    include "wrapper/footer.php";
?>