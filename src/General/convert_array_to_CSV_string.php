<?
/*******************************************************************************
**  FILE: convert_array_to_CSV_string.php
**
**  FUNCTION: convert_array_to_CSV_string
**
**  PURPOSE: Convert an array to a comma separated string 
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2013.03.20
**
*********************************************************************************/

function convert_array_to_CSV_string($input_array)
{
    $title_string = "";
    $content_string = "";

    $numOfColumns = count($input_array[0]);
    $column_names = array_keys($input_array[0]);

    for($i=0 ; $i<$numOfColumns ; $i++)
    {
        $columnName = $column_names[$i];
        if($i!=0){ $title_string.=','; }
        $title_string.=$columnName;
    }
    $title_string.="\n";

    $is_first_column = true;

    for( $i = 0 ; $i < count($input_array) ; $i++ )
    {
        for( $j=0 ; $j < $numOfColumns ; $j++ )
        {
            if(!$is_first_column) $content_string.=',';

            $column_name = $column_names[$j];
            $array_element = $input_array[$i]["$column_name"];
            $content_string.=$array_element;
            $is_first_column = false;
        }
        $content_string.="\n";
        $is_first_column = true;
    }

    return $title_string.$content_string;
}

?>