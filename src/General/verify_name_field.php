<?
    function verify_name_field($input_array)
    {
        $receiver_first_name = $input_array['receiver_first_name'];
        $input_array['valid_name_field'] = 1;
        
        if(strlen($receiver_first_name) > FIRST_NAME_MAX_LENGTH)
        {
            $input_array['error'].= "Receiver first name field ";
            $input_array['error'].= "should be less than ".FIRST_NAME_MAX_LENGTH." characters <br>";
            $input_array['valid_name_field'] = 0;
        }
        
        return $input_array;
    }
?>