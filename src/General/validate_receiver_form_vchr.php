<?
/*******************************************************************************
**   FILE: validate_receiver_form_vchr.php
**
**   FUNCTION: validate_receiver_form_vchr
**
**   PURPOSE: Validates user input data.
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 03.Jul.2012
**
**   ADAPTED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 25.May.2015
**   Adapted for UgandaVouchers.com from src/General/validate_receiver_form_auto.php
**
*********************************************************************************/

function validate_receiver_form_vchr($input_array)
{
    $input_array['error'] = "";
    
    
    //verify phone number length
    $input_array = verify_receiver_phone_length($input_array);
    $success = $input_array['phone_length_ok'];
    

    //verify phone number prefix
    if($success == 1)
    {
        $input_array = verify_receiver_phone_prefix($input_array);
        $success = $input_array['phone_prefix_ok'];
    }
    
    //verify that phone number consists of numeric digits only
    if($success == 1)
    {
        $input_array = verify_receiver_phone_digits($input_array);
        $success = $input_array['phone_digits_ok'];
    }
   
    //match phone number to network
    //No need to match phone number to supermarket

    //verify receiver amount in case of auto load
    /**if($success && ($input_array['airtime_load_option'] == AIRTIME_LOAD_OPTION_AUTO))
    {
        $input_array['currency_code_type'] = 'airtime_currency_code';
        $input_array = verify_receiver_amount_vchr($input_array);
        $success = $input_array['receiver_amount_ok'];
    } **/

    //match phone numbers
    if($success == 1)
    {
        $input_array = match_phone_numbers($input_array);
        $success = $input_array['phone_numbers_match'];
    }
    
    //verify first name field
    //verify last name field
    if($success == 1)
    {
        $input_array = verify_first_last_name_fields($input_array);
        $success = $input_array['valid_name_field'];
    }

    //verify email address
    //No recipient email address required for supermarket vouchers
    

    $input_array['success'] = $success;

    return $input_array;
}
?>