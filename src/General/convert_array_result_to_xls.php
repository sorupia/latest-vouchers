<?
/*******************************************************************************
**  FILE: convert_array_result_to_xls.php
**
**  FUNCTION: convert_array_result_to_xls
**
**  PURPOSE: Convert an array to an xls file 
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.10.31
*********************************************************************************/

function convert_array_result_to_xls($column_names, $input_array)
{
    $file_text.= "<html \n";
    $file_text.= "xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\" \n";
    $file_text.= "xmlns:o=\"urn:schemas-microsoft-com:office:office\" \n";
    $file_text.= "xmlns:x=\"urn:schemas-microsoft-com:office:excel\" \n";
    $file_text.= "xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" \n";
    $file_text.= "xmlns:html=\"http://www.w3.org/TR/REC-html40\"> \n";
    $file_text.= "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"> \n";
    $file_text.= "<html> \n";
    $file_text.= "<head> \n";
    $file_text.= "<meta http-equiv=\"Content-type\" content=\"text/html;charset=utf-8\" /> \n";
    $file_text.= "<style id=\"Classeur1_16681_Styles\"> \n";
    $file_text.= "</style> \n";
    $file_text.= "</head> \n";
    $file_text.= "<body> \n";
    $file_text.= "<div id=\"Classeur1_16681\" align=center x:publishsource=\"Excel\"> \n";
    $file_text.= "<table x:str border=0 cellpadding=0 cellspacing=0 width=100% style=\"border-collapse: collapse\"> \n";

    /* process the db results into string */
    $file_text.= "<tr>";

    $numOfColumns = count($column_names);

    for($i=0 ; $i<$numOfColumns ; $i++)
    {
        $columnName = $column_names[$i];
        $file_text.="<td class=xl2216681 nowrap><b>".$columnName."</b></td>";
    }

    $file_text.= "</tr>";
    $file_text.= "\n";

    for( $i = 0 ; $i < count($input_array[0]) ; $i++ )
    {
        $file_text.= "<tr>";
        for( $j=0 ; $j < count($input_array) ; $j++ )
        {
            $col_value = $input_array[$j][$i];
            $file_text.="<td class=xl2216681 nowrap>".$col_value."</td>";
        }
        $file_text.= "</tr>";
        $file_text.= "\n";
    }

    $file_text.= "</table> \n";
    $file_text.= "</div> \n";
    $file_text.= "</body> \n";
    $file_text.= "</html> \n";
    $file_text.= "</Workbook> \n";

    return $file_text;
}
?>