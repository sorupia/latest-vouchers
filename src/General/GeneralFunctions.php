<?
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/validate_receiver_form.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/verify_receiver_phone_length.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/verify_receiver_phone_prefix.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/verify_receiver_phone_digits.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/verify_receiver_phone_network.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/match_phone_numbers.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/verify_name_field.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/verify_email_address_format.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/verify_email.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/email_ipn_post.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/check_me.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/validate_receiver_form_auto.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/verify_receiver_amount.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/create_item_id.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/verify_receiver_phone_network_auto.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/record_sale_to_google_analytics.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/this_network_has_auto_load.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/compare_shorter_string.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/verify_first_last_name_fields.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/validate_receiver_form_vchr.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/is_a_voucher.php";
?>