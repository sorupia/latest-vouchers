<?

    function verify_email($input_array)
    {
        $receiver_email = $input_array['receiver_email'];

        if($receiver_email =='' || filter_var($receiver_email, FILTER_VALIDATE_EMAIL))
        {
            $input_array['valid_email'] = 1;
        }
        else
        {
            $input_array['valid_email'] = 0;
            $input_array['error'].= "Invalid email address<br>";
        }

        return $input_array;
    }
?>