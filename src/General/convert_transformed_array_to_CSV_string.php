<?
/*******************************************************************************
**  FILE: convert_transformed_array_to_CSV_string.php
**
**  FUNCTION: convert_transformed_array_to_CSV_string
**
**  PURPOSE: Convert a transformed array to a comma separated string 
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.10.24
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2013.03.20
**  Changed the function name from convert_array_to_CSV_string
**
*********************************************************************************/

function convert_transformed_array_to_CSV_string($column_names, $input_array)
{
    $title_string = "";
    $content_string = "";

    $numOfColumns = count($column_names);

    for($i=0 ; $i<$numOfColumns ; $i++)
    {
        $columnName = $column_names[$i];
        if($i!=0){ $title_string.=','; }
        $title_string.=$columnName;
    }
    $title_string.="\n";

    $is_first_column = true;

    for( $i = 0 ; $i < count($input_array[0]) ; $i++ )
    {
        for( $j=0 ; $j < count($input_array) ; $j++ )
        {
            if(!$is_first_column){ $content_string.=','; }
            $array_element = $input_array[$j][$i];
            $content_string.=$array_element;
            $is_first_column = false;
        }
        $content_string.="\n";
        $is_first_column = true;
    }

    return $title_string.$content_string;
}

?>
