<?

/*******************************************************************************
**   FILE: obtain_mysqli_object.php
**
**   FUNCTION: obtain_mysqli_object
**
**   PURPOSE: Function to connect to the DB and return a mysqli object
**   
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 13.Aug.2012
*********************************************************************************/

function obtain_mysqli_object($input_array)
{
    $input_array['mysqli'] = new mysqli(LIVE_DB_ADDRESS, LIVE_DB_USERNAME, LIVE_DB_PASSWORD, LIVE_DB_NAME);
    return $input_array;
}

?>