<?
/*******************************************************************************
**  FILE: verify_receiver_amount.php
**
**  FUNCTION: verify_receiver_amount
**
**  PURPOSE: Verify the amount to be sent to the receiver
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, New York)   DATE: 2011.10
**  Originally built for SendSente.com transfer of mobile money
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.08.10
**  Modified to be used for SendAirTime.com transfer of airtime
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.12.13
**  Modified to include a warning email and text when airtime credit is low
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2014.08.01
**  Modified warning email and text to specify network when client inputs value
**  that is greater than what we have in the system.
**
*********************************************************************************/

    function verify_receiver_amount($input_array)
    {
        $receiver_amount_in_local = $input_array['receiver_amount_in_local'];
        $input_array['receiver_amount_ok'] = 1;

        if(ROUND_RECEIVER_AMOUNT_IN_LOCAL)
        {
            $input_array['receiver_amount_in_local'] = round($input_array['receiver_amount_in_local'], LOCAL_DECIMAL_PLACES);
        }

        //Check receiver amount against lower limit
        if($receiver_amount_in_local < RECEIVER_LOWER_LIMIT)
        {
            $input_array['error'].= "Receiver amount less than the minimum of ".
            LOCAL_CURRENCY." ".number_format(RECEIVER_LOWER_LIMIT, LOCAL_DECIMAL_PLACES)."<br>";
            $input_array['receiver_amount_ok'] = 0;
        }

        //Check receiver amount against upper limit
        if($receiver_amount_in_local > RECEIVER_UPPER_LIMIT)
        {
            $input_array['error'].= "Receiver amount greater than the maximum of ".
            LOCAL_CURRENCY." ".number_format(RECEIVER_UPPER_LIMIT, LOCAL_DECIMAL_PLACES)."<br>";
            $input_array['receiver_amount_ok'] = 0;
        }

        //Check our YO! balance
        $account_balance = 0;
        if((CMP_RCVR_AMT_WITH_YO_BAL || NOTIFY_ON_LOW_YO_BALANCE) && ($input_array['receiver_amount_ok'] == 1))
        {
            $input_array = yo_account_balance($input_array);

            $input_array = find_yo_account_balance($input_array);

            $account_balance   = $input_array['found_yo_account_balance'];
        }

        //Compare receiver amount with Yo! balance
        if(($input_array['receiver_amount_ok'] == 1) && CMP_RCVR_AMT_WITH_YO_BAL && ($receiver_amount_in_local >= $account_balance))
        {
            $input_array['error'].= "Receiver amount ".number_format($receiver_amount_in_local, LOCAL_DECIMAL_PLACES)." greater than what system has at the moment. ";

            if($account_balance >= RECEIVER_LOWER_LIMIT)
            {
                $input_array['error'].= "Please reduce amount to less than ".number_format($account_balance, LOCAL_DECIMAL_PLACES)." or try again later<br>";
            }
            else
            {
                $input_array['error'].= "Please try again later.<br>";
            }

            $input_array['receiver_amount_ok'] = 0;

            if(NFY_CMP_RCVR_AMT_WITH_YO_BAL)
            {
                $input_array['subject']            = "Receiver amount greater than balance";
                $input_array['msg']                = $input_array['error']." Network ID: ".$input_array['network_id'].", Client IP: ".$input_array['ip_address'].".";
                $input_array['admin_text_message'] = $input_array['error']." Network ID: ".$input_array['network_id'].", Client IP: ".$input_array['ip_address'].".";
                notify_admin_email($input_array);
                notify_admin_sms($input_array);
            }
        }

        if(($input_array['receiver_amount_ok'] == 1) && NOTIFY_ON_LOW_YO_BALANCE)
        {
            //Only notify once the Yo! balance goes below the threshold until a certain point so as not to overwhelm admins with msgs
            if((($account_balance < LOW_YO_BALANCE_THRESHOLD) && ($account_balance > (LOW_YO_BALANCE_THRESHOLD - RECEIVER_UPPER_LIMIT))) ||
               ($account_balance < RECEIVER_UPPER_LIMIT))
            {
                $input_array['warning'].= DOMAIN_NAME."\nYo! ".$input_array['network_id']." ".$input_array['currency_code_type']." balance of ".
                                        number_format($account_balance, LOCAL_DECIMAL_PLACES).
                                        " lower than threshold of ".number_format(LOW_YO_BALANCE_THRESHOLD, LOCAL_DECIMAL_PLACES).".\nIP: ".$input_array['ip_address'].".";

                $input_array['subject']            = "YO! Low ".$input_array['currency_code_type']." Balance: ".number_format($account_balance, LOCAL_DECIMAL_PLACES);
                $input_array['msg']                = $input_array['warning'];
                $input_array['admin_text_message'] = $input_array['warning'];
                notify_admin_email($input_array);
                //notify_admin_sms($input_array);
            }
        }

        return $input_array;
    }
?>