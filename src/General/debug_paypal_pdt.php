<?

function debug_paypal_pdt($input_array)
{
    $header          = $input_array['paypal_pdt_header'];
    $req             = $input_array['paypal_pdt_request'];
    $paypal_web      = $input_array['paypal_pdt_web_address'];
    $paypal_web_port = $input_array['paypal_pdt_web_port'];
    $lines           = $input_array['lines'];
    $time_stamp      = date("Y-m-d H:i:s");


    $subject = "PayPal debug";
    $body    = "A user tried to access the PayPalReturnPage and had a problem.\n";
    $body   .= "PayPal Web Address: ".$paypal_web."\n";
    $body   .= "PayPal Port: ".$paypal_web_port."\n";
    $body   .= "Header and Body Sent to PayPal: ".$header.$req."\n";
    $body   .= "Time of access is: ".$time_stamp."\n";

    $body   .= "PayPal responded with:\n ";

    for( $i=0 ; $i<count($lines) ; $i++ )
    {
        $body.="$lines[$i]\n";
    }

    sendEmail('PayPal Return Page',$fromAddress,TECH_ADMIN,'',$subject,$body);
    sendEmail('PayPal Return Page',$fromAddress,SUPER_TECH_ADMIN,'',$subject,$body);
}



?>