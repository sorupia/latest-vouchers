<?
/*******************************************************************************
**  FILE: verify_receiver_amount_vchr.php
**
**  FUNCTION: verify_receiver_amount_vchr
**
**  PURPOSE: Verify the amount to be sent to the receiver
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, New York)   DATE: 2011.10
**  Originally built for SendSente.com transfer of mobile money
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.08.10
**  Modified to be used for SendAirTime.com transfer of airtime
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.12.13
**  Modified to include a warning email and text when airtime credit is low
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2014.08.01
**  Modified warning email and text to specify network when client inputs value
**  that is greater than what we have in the system.
**
**   ADAPTED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 25.May.2015
**   Adapted for UgandaVouchers.com from src/General/validate_receiver_form_auto.php
**
*********************************************************************************/

    function verify_receiver_amount_vchr($input_array)
    {
        $receiver_amount_in_local = $input_array['receiver_amount_in_local'];
        $input_array['receiver_amount_ok'] = 1;

        if(ROUND_RECEIVER_AMOUNT_IN_LOCAL)
        {
            $input_array['receiver_amount_in_local'] = round($input_array['receiver_amount_in_local'], LOCAL_DECIMAL_PLACES);
        }

        //Check receiver amount against lower limit
        if($receiver_amount_in_local < RECEIVER_LOWER_LIMIT)
        {
            $input_array['error'].= "Receiver amount less than the minimum of ".
            LOCAL_CURRENCY." ".number_format(RECEIVER_LOWER_LIMIT, LOCAL_DECIMAL_PLACES)."<br>";
            $input_array['receiver_amount_ok'] = 0;
        }

        //Check receiver amount against upper limit
        if($receiver_amount_in_local > RECEIVER_UPPER_LIMIT)
        {
            $input_array['error'].= "Receiver amount greater than the maximum of ".
            LOCAL_CURRENCY." ".number_format(RECEIVER_UPPER_LIMIT, LOCAL_DECIMAL_PLACES)."<br>";
            $input_array['receiver_amount_ok'] = 0;
        }

        return $input_array;
    }
?>