<?
/*******************************************************************************
**  FILE: verify_first_last_name_fields.php
**
**  FUNCTION: verify_first_last_name_fields
**
**  PURPOSE: Verify the amount to be sent to the receiver
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, New York)   DATE: 2011.10
**  Originally built for SendSente.com transfer of mobile money
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.08.10
**  Modified to be used for SendAirTime.com transfer of airtime
**
**   ADAPTED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 25.May.2015
**   Adapted for UgandaVouchers.com from src/General/verify_name_field.php
**
*********************************************************************************/

    function verify_first_last_name_fields($input_array)
    {
        $receiver_first_name = $input_array['receiver_first_name'];
        $receiver_last_name  = $input_array['receiver_last_name'];

        $input_array['valid_name_field'] = 1;

        if(strlen($receiver_first_name) > FIRST_NAME_MAX_LENGTH)
        {
            $input_array['error'].= "Receiver first name field ";
            $input_array['error'].= "should be less than ".FIRST_NAME_MAX_LENGTH." characters <br>";
            $input_array['valid_name_field'] = 0;
        }

        if(strlen($receiver_last_name) > LAST_NAME_MAX_LENGTH)
        {
            $input_array['error'].= "Receiver last name field ";
            $input_array['error'].= "should be less than ".LAST_NAME_MAX_LENGTH." characters <br>";
            $input_array['valid_name_field'] = 0;
        }

        if(!(ctype_alpha($receiver_first_name) && ctype_alpha($receiver_last_name)))
        {
            $input_array['error'].= "First and last names should contain only alphabetic characters<br>";
            $input_array['valid_name_field'] = 0;
        }

        return $input_array;
    }
?>