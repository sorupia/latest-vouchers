<?
/*******************************************************************************
**  FILE: compare_shorter_string.php
**
**  FUNCTION: compare_shorter_string
**
**  PURPOSE: Compare two strings using the shorter to find a matching pattern in the longer
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Montreal)   DATE: 2012.11.29
**
*********************************************************************************/

function compare_shorter_string($string_1, $string_2)
{
    if(strlen($string_1) > strlen($string_2))
    {
        $longer_string  = $string_1;
        $shorter_string = $string_2;
    }
    else if(strlen($string_1) < strlen($string_2))
    {
        $longer_string  = $string_2;
        $shorter_string = $string_1;
    }
    else
    {
        return strcasecmp($string_1, $string_2) == 0 ? true : false;
    }

    if (preg_match("/\b$shorter_string\b/i", $longer_string))
    {
        return true;
    }
    else
    {
        return false;
    }
}

//Test code
/*
$string_1 = "Fren";
$string_2 = "D. French";

if(compare_shorter_string($string_1, $string_2))
{
    echo "A match was found.";
}
else
{
    echo "A match was not found.";
}
*/

?>