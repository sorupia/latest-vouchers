<?
    function verify_receiver_phone_length($input_array)
    {
        $receiver_phone = $input_array['receiver_phone'];
        $input_array['phone_length_ok'] = 1;

        $phone_length = strlen($receiver_phone);
        
        if($phone_length != RX_PHONE_LENGTH)
        {
            $input_array['error'].= "Phone length should be ".RX_PHONE_LENGTH." including +<br>";
            $input_array['phone_length_ok'] = 0;
        }
        
        return $input_array;
    }
?>