<?
    function verify_receiver_phone_digits($input_array)
    {
        global $country_info_array;

        $receiver_phone = $input_array['receiver_phone'];

        $input_array['phone_digits_ok'] = 1;
        $phone_digits = substr($receiver_phone,1);

        if(!ctype_digit($phone_digits))
        {
            $input_array['error'].= "Phone number should be numeric except for the +<br>";
            $input_array['phone_digits_ok'] = 0;
        }

        return $input_array;
    }
?>