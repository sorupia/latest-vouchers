<?
function validate_receiver_form($input_array)
{
    $input_array['error'] = "";

    //verify phone number length
    $input_array = verify_receiver_phone_length($input_array);
    $success = $input_array['phone_length_ok'];

    //verify phone number prefix
    if($success)
    {
        $input_array = verify_receiver_phone_prefix($input_array);
        $success = $input_array['phone_prefix_ok'];
    }

    //verify that phone number consists of numeric digits only
    if($success)
    {
        $input_array = verify_receiver_phone_digits($input_array);
        $success = $input_array['phone_digits_ok'];
    }

    //match phone number to network
    if($success && !MNP_IN_USE)
    {
        $input_array = verify_receiver_phone_network($input_array);
        $success = $input_array['phone_network_ok'];
    }

    //match phone numbers
    if($success)
    {
        $input_array = match_phone_numbers($input_array);
        $success = $input_array['phone_numbers_match'];
    }

    //verify first name field
    if($success)
    {
        $input_array = verify_name_field($input_array);
        $success = $input_array['valid_name_field'];
    }

    //verify email address
    if($success)
    {
        $input_array = verify_email($input_array);
        $success = $input_array['valid_email'];
    }

    $input_array['success'] = $success;

    return $input_array;
}
?>