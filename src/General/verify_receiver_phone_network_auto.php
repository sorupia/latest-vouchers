<?
/*******************************************************************************
**  FILE: verify_receiver_phone_network_auto.php
**
**  FUNCTION: verify_receiver_phone_network_auto
**
**  PURPOSE: Verify receiver phone network for auto loading and return the network id
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, New York)   DATE: 2011.10
**  Originally built for SendSente.com transfer of mobile money
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.08.10
**  Modified to be used for SendAirTime.com transfer of airtime
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.12.14
**  Modified to fix small bug with indexing auto_load_network_array
**
*********************************************************************************/

    function verify_receiver_phone_network_auto($input_array)
    {
        $receiver_phone        = $input_array['receiver_phone'];
        $receiver_phone_prefix = substr($receiver_phone,0,RX_PHONE_PREFIX_LENGTH);

        $input_array['phone_network_ok'] = 0;
        $supported_prefixes = "";

        global $auto_load_network_array;

        if($receiver_country_code=="") $receiver_country_code = COUNTRY_ABBREVIATION;

        $network_id_array = array_keys($auto_load_network_array["$receiver_country_code"]);
        for($i = 0; $i< count($network_id_array); $i++)
        {
            $this_network_id       = $network_id_array[$i];
            $this_network_prefixes = $auto_load_network_array["$receiver_country_code"]["$this_network_id"]["prefixes"];

            for ($j = 0; $j < count($this_network_prefixes); $j++)
            {
                $this_prefix = $this_network_prefixes[$j];
                $supported_prefixes.= $this_prefix." ";

                if($receiver_phone_prefix == $this_prefix)
                {
                    $input_array['phone_network_ok'] = 1;
                    $network_name = $auto_load_network_array["$receiver_country_code"]["$this_network_id"]["networkName"];
                    $network_id = $this_network_id;
                }
            }
        }

        if($input_array['phone_network_ok'] == 0)
        {
            $input_array['error'].= "The phone number should start with either of ";
            $input_array['error'].= "the following prefixes: ".$supported_prefixes."<br>";
        }
        else
        {
            $input_array['network_id']       = $network_id;
            $input_array['network_name']     = $network_name;
            $input_array['network_prefixes'] = $this_network_prefixes;
        }

        return $input_array;
    }
?>