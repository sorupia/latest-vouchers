<?
/*******************************************************************************
**   FILE: is_a_voucher.php
**
**   FUNCTION: is_a_voucher
**
**   PURPOSE: Check to see if item_id provided belongs to a network
**            that does vouchers. So far we have 3 delivery methods:
**            1. card (scratch card with a loading procedure)
**            2. auto (automatic top-up of airtime)
**            3. vchr (hand delivery of a voucher)
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs-AVMM, Kampala)   DATE: 30.Nov.2015
*********************************************************************************/

function is_a_voucher($input_array)
{
    $item_id = $input_array['item_id'];
    if( substr($item_id,5,4) == AIRTIME_LOAD_OPTION_VCHR )
    {
        return true;
    }
    else
    {
        return false;
    }

    return false;
}

?>