<?php

function record_sale_to_google_analytics($input_array)
{
    $google_analytics_acct = array_key_exists('google_analytics_acct',$input_array) ? $input_array['google_analytics_acct']:'';
    $txn_id                = $input_array['txn_id'];
    $payment_amount        = $input_array['mc_gross'];
    $address_city          = $input_array['address_city'];
    $address_state         = $input_array['address_state'];
    $address_country       = $input_array['address_country'];
    $item_id               = $input_array['item_number'];
    $item_name             = $input_array['item_name'];
    
    if($google_analytics_acct == '')
    {
        $google_analytics_acct = GOOGLE_ANALYTICS_ACCT;
    }

	//track the sales with google analytics
	echo'
		<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
		</script>
		<script type="text/javascript">
		  _uacct="'.$google_analytics_acct.'";
		  urchinTracker();
		</script>';
		
	echo"<form style=\"display:none;\" name=\"utmform\">
		<textarea id=\"utmtrans\">
		UTM:T|".$txn_id."|Sendairtime ".COUNTRY_NAME."|".$payment_amount."|0| 0|".$address_city."|".$address_state."|".$address_country." 
		UTM:I|".$txn_id."|".$item_id."|".$item_name."|Airtime|".$payment_amount."|1 
		</textarea>
		</form>";		
		
	echo'<script type="text/javascript">
		__utmSetTrans();
		</script>';
}

?>