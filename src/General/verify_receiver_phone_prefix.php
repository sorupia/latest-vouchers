<?
    function verify_receiver_phone_prefix($input_array)
    {
        $receiver_phone = $input_array['receiver_phone'];
        $input_array['phone_prefix_ok'] = 1;

        $phone_prefix = substr($receiver_phone,0,strlen(COUNTRY_MOBILE_PREFIX));

        if($phone_prefix != COUNTRY_MOBILE_PREFIX)
        {
            $input_array['error'].= "The phone number should start with ".COUNTRY_MOBILE_PREFIX."<br>";
            $input_array['phone_prefix_ok'] = 0;
        }

        return $input_array;
    }
?>