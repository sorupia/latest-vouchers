<?
/*******************************************************************************
**   FILE: this_network_has_auto_load.php
**
**   FUNCTION: this_network_has_auto_load
**
**   PURPOSE: Check to see if item_id provided belongs to a network
**            of which we support auto load.
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 05.Aug.2012
*********************************************************************************/

function this_network_has_auto_load($input_array)
{
    global $auto_load_network_array;

    $this_network_id = substr($input_array['item_id'],0,5);
    
    $this_network_id_has_auto_load = array_key_exists($this_network_id,$auto_load_network_array[COUNTRY_ABBREVIATION]);
    
    return $this_network_id_has_auto_load;
}

?>