<?
    function match_phone_numbers($input_array)
    {
        $receiver_phone  = $input_array['receiver_phone'];
        $phoneConfirm    = $input_array['phoneConfirm'];
        $input_array['phone_numbers_match'] = 1;

        if($receiver_phone != $phoneConfirm)
        {
            $input_array['error'].= "Please enter matching phone numbers<br>";
            $input_array['phone_numbers_match'] = 0;
        }

        return $input_array;
    }
?>