<?
    //validate_receiver_phone_network and get network id
    function verify_receiver_phone_network($input_array)
    {
        $receiver_phone        = $input_array['receiver_phone'];
        $receiver_phone_prefix = substr($receiver_phone,0,RX_PHONE_PREFIX_LENGTH);

        $input_array['phone_network_ok'] = 0;
        $supported_prefixes = "";

        global $networkArray;

        if($receiver_country_code=="") $receiver_country_code = COUNTRY_ABBREVIATION;

        $this_network_id       = $input_array['network_id'];
        $this_network_name     = $networkArray["$receiver_country_code"]["$this_network_id"]["networkName"];
        $this_network_prefixes = $networkArray["$receiver_country_code"]["$this_network_id"]["prefixes"];

        for ($j = 0; $j < count($this_network_prefixes); $j++)
        {
            $this_prefix = $this_network_prefixes[$j];
            $supported_prefixes.= $this_prefix." ";

            if($receiver_phone_prefix == $this_prefix)
            {
                $input_array['phone_network_ok'] = 1;
            }
        }


        if($input_array['phone_network_ok'] == 0)
        {
            $input_array['error'].= "For $this_network_name the phone number should start with ";
            $input_array['error'].= "the following prefixes: ".$supported_prefixes."<br>";
        }

        return $input_array;
    }
?>