<?
/*******************************************************************************
**  FILE: verify_receiver_region.php
**
**  FUNCTION: verify_receiver_region
**
**  PURPOSE: Verify the region of the recipient
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2015.11.04
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: YYYY.MM.DD
**
*********************************************************************************/

    function verify_receiver_region($input_array)
    {
        $receiver_region = $input_array['receiver_region'];

        $input_array['valid_region_field'] = 1;

        if(strlen($receiver_region) > REGION_MAX_LENGTH)
        {
            $input_array['error'].= "Receiver region field ";
            $input_array['error'].= "should be less than ".REGION_MAX_LENGTH." characters <br>";
            $input_array['valid_region_field'] = 0;
        }

        if(!(ctype_alpha($receiver_region)))
        {
            $input_array['error'].= "Receiver region should contain only alphabetic characters<br>";
            $input_array['valid_region_field'] = 0;
        }

        return $input_array;
    }
?>