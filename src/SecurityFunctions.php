<?
/*
2010-11-26: Modified checkUserTransactionLimit
2010-11-26: Added limitUserTransactions
2010-11-26: Modified warnUserOnTransactionLimit
20110113: Moved checkUserTransactionLimit, limitUserTransactions, warnUserOnTransactionLimit
          to individual files
*/
    require_once$_SERVER['DOCUMENT_ROOT']."/src/constants.php";
    require_once$_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";

    require_once$_SERVER['DOCUMENT_ROOT']."/src/Security/blockSMSUser.php";
    require_once$_SERVER['DOCUMENT_ROOT']."/src/Security/checkUserTransactionLimit.php";
    require_once$_SERVER['DOCUMENT_ROOT']."/src/Security/limitUserTransactions.php";
    require_once$_SERVER['DOCUMENT_ROOT']."/src/Security/warnUserOnTransactionLimit.php";

    function updateMonthlySMS($countryAbbreviation)
    {
        //perform SMS credits update for all users
        $connection=connect2SMSDB2($countryAbbreviation);
            $smsCreditsQuery = "UPDATE SMSUsers SET creditsLeft = ".SMS_MAX_CREDITS;
            $smsCreditsResult = mysql_query($smsCreditsQuery,$connection);
            $month=date("F");
            $resultMsg = "SMS Monthly Credits reset to ".SMS_MAX_CREDITS." for $month\n Result: ".mysql_info()."\n";
            $resultMsg.= "---3nitylabs Software Inc. \n";
            if($debug) echo "Query: ".$smsCreditsQuery."<br>";
            echo "$resultMsg <br>";
        disconnectDB($connection);
        sendEmail("Sendairtime Web",FROM_EMAIL,TECH_ADMIN,$bcc,"Monthly SMS Update",$resultMsg);
    }

    function isItemIDValid($itemIDNumber,$countryAbbreviation){
        $debug = false;
        $success = false;
        if($debug){echo "BEGIN isItemIDValid --<br>"; }
            
        global $cardMap;
        
        if(strlen($itemIDNumber) < 5) 
        {
            if($debug){echo "itemIDNumber: $itemIDNumber length < 5 <br><br>"; }
            return false;
        }
        
        $network = substr($itemIDNumber,0,5);
        if($debug){echo "Network: $network <br>"; }
        
        $networkItemIDArray = $cardMap[$countryAbbreviation][$network];
        if($debug){echo "networkItemIDArray: "; }
        if($debug){print_r($networkItemIDArray); }
        
        if($networkItemIDArray==""){ 
            if($debug){echo "NetworkIDArray is empty <br> "; }
            if($debug){echo "--END isItemIDValid <br><br>"; }
            return $success; 
        }

        if( in_array($itemIDNumber,$networkItemIDArray) )
        {
            if($debug){echo "itemIDNumber: $itemIDNumber found in array <br><br> "; }
            $success = true;
        }
        else
        {
            if($debug){echo "itemIDNumber: $itemIDNumber not found in array <br><br> "; }
            $success = false;
        }
        if($debug){echo "--END isItemIDValid <br><br>"; }
        return $success;
    }
    function reportError($error, $scriptName)
    {
        $errorMsg = "Date and Time: ".date("Y-m-d H:i:s")."\n";
        $errorMsg.= "IP Address: ".getIP()."\n";
        $errorMsg.= $error;
        sendEmail("Sendairtime Web",FROM_EMAIL,TECH_ADMIN,$bcc,$scriptName,$errorMsg);
    }

    function getIPLocation()
    {
        $buf = "";
        $license_key = MAX_MIND_LICENSE;
        $ipaddress = getIP(); // from GeneralFunctions.php
        $query = "http://geoip1.maxmind.com/b?l=" . $license_key . "&i=" . $ipaddress;
        $url = parse_url($query);
        $host = $url["host"];
        $path = $url["path"] . "?" . $url["query"];
        $timeout = 1;
        $fp = fsockopen ($host, 80, $errno, $errstr, $timeout);
            //or die('Can not open connection to server.');
        if ($fp)
        {
          fputs ($fp, "GET $path HTTP/1.0\nHost: " . $host . "\n\n");
          while (!feof($fp)) {
            $buf .= fgets($fp, 128);
          }
          $lines = explode("\n", $buf);
          $data = $lines[count($lines)-1];
          fclose($fp);
        }
        else
        {
            if(REPORT_MAX_MIND_ERROR)
            {
                # enter error handing code here
                $errorMsg = "Date and Time: ".date("Y-m-d H:i:s")."\n";
                $errorMsg.= "IP Address: ".getIP()."\n";
                $errorMsg.= "Error: $errno: $errstr\n";
                $errorMsg.= "URL: ".$_SERVER['SCRIPT_FILENAME']."\n";
                //send email
                sendEmail("Sendairtime Web",FROM_EMAIL,TECH_ADMIN,SUPER_TECH_ADMIN,"MaxMind Error",$errorMsg);
            }
        }

        //echo "Your IP Based Geo Location is:   ";
        //echo "Your IP Address is: ".$ipaddress."<br/>";
        //echo $data;
        
        $geo = explode(",",$data);
        $ipLocationArray['ip_country'] = $geo[0];
        $ipLocationArray['ip_state'] = $geo[1];
        $ipLocationArray['ip_city'] = $geo[2];
        $ipLocationArray['ip_latitude'] = $geo[3];
        $ipLocationArray['ip_longitude'] = $geo[4];
        $ipLocationArray['ip_address'] = $ipaddress;
        
        return $ipLocationArray;
    }

    /*
    @function name authenticateAPIUser
    @description This function must be encapsulated between lock database table statements
    */
    function authenticateAPIUser($emailAddress,$password,$connection){
        
        $result['authenticated']=false;
        $debug=true;
        
        if($debug){echo "BEGIN authenticateAPIUser --<br>"; }
        
        $subscriberStatus='DEALER';
        //check username and password to see if they match
        //future:later on we may have to use md5 rehashing to provide extra security
        //this user also needs to be a verified user
        //search SMSUsers Table DB
        $SMSUsersSearchQuery = "SELECT * FROM APIUsers ";
        $SMSUsersSearchQuery.= "WHERE emailAddress='$emailAddress' AND passwd=PASSWORD('$password') AND airtimeStatus='$subscriberStatus'";
        
        if($debug) {echo "authentication query: $SMSUsersSearchQuery <br>";}
        $SMSUsersSearchResult = mysql_query($SMSUsersSearchQuery,$connection) or handleDatabaseError(''. mysql_error(),$SMSUsersSearchQuery);
        $SMSUsersSearchData = mysql_fetch_array($SMSUsersSearchResult, MYSQL_ASSOC);
        
        if (mysql_num_rows($SMSUsersSearchResult) == 1) {//VERIFIED user exists and passwd is correct
            
            //return true if user is authenticated
            $result['authenticated']=true;
            if($debug){echo "Authentication Successful<br>";}
            //log user into session
            //$_SESSION['smsLoggedIn']=true;
            
            //obtain clients name
            $clientFirstName=$SMSUsersSearchData['firstName'];
            if($debug){echo "Logged in User is: $clientFirstName <br>";}
            $result['clientFirstName']=$clientFirstName;
            
            //obtain Client Email Address
            $result['emailAddress']=$emailAddress;
            
            //obtain number of credits left
            $creditsLeft=$SMSUsersSearchData['creditsLeft'];
            if($debug){echo "Number Of SMS Credits Left: $creditsLeft <br>";}
            $result['smsCreditsLeft']=$creditsLeft;//store the number of credits left in the users session container
            $airtimeCreditsLeft = $SMSUsersSearchData['airtimeCreditsLeft'];
            $result['airtimeCreditsLeft'] = $airtimeCreditsLeft;
            if($debug){echo "Number Of Airtime Credits Left: $airtimeCreditsLeft <br>";}
            $allowAnonClient = $SMSUsersSearchData['allowAnonClient'];
            $result['allowAnonClient'] = $allowAnonClient;
            if($debug){echo "Allow Anonymous Client: $allowAnonClient <br>";}
            
        }
        else{
            $result['authenticated']=false;
            $result['airtimeCreditsLeft']=0;
            $result['allowAnonClient'] = false;
            if($debug){echo "Authentication Failure, email:$emailAddress, passwd: $password";}
        }
        //disconnectDB($connection);
        if($debug){echo "--END authenticateAPIUser <br>"; }
        return $result;
    }

    function decrementAPIAirtimeCredits($clientEmail, $countryAbbreviation, $deduction,$connection){
        //This function should be inserted in a LOCK TABLES table statement
        $debug = true;
        if($debug){echo "BEGIN decrementAirtimeCredits --<br>"; }
        $updateAirtimeCreditsQuery="UPDATE APIUsers SET airtimeCreditsLeft=airtimeCreditsLeft-$deduction ";
        $updateAirtimeCreditsQuery.="WHERE emailAddress='$clientEmail'";
        if($debug){echo "decrementAirtimeCredits query: $updateAirtimeCreditsQuery <br>"; }
        mysql_query($updateAirtimeCreditsQuery,$connection) or handleDatabaseError(''.mysql_error(),$updateAirtimeCreditsQuery);
        $numberOfAffectedRows = mysql_affected_rows();
        if($debug){echo"Number of Affected Rows: $numberOfAffectedRows <br>";}
        if($debug){echo "--END decrementAirtimeCredits <br>"; }
    }

    /* Create connection outside of this function then invoke it */
    function clientOnBlackList($clientEmail,$countryAbbreviation,$connect){
        $debug = false;
        if($debug){echo "BEGIN clientOnBlackList---<br>"; }
        $blackListQuery="SELECT * FROM SMSUsers ";
        $blackListQuery.="WHERE emailAddress='$clientEmail' AND airtimeStatus='BLOCKED'";
        if($debug){echo"Blacklist query: $blackListQuery <br>";}
        
        $blackListResult = mysql_query($blackListQuery,$connect) or handleDatabaseError(''.mysql_error(),$blackListQuery);

        if($debug){echo "---END clientOnBlackList<br>"; }

        if (mysql_num_rows($blackListResult) > 0) {	return true; }
        else{ return false;	}
    }

    /* Create connection outside of this function then invoke it */
    function apiUserOnWhiteList($clientEmail,$countryAbbreviation,$connect){
        $debug = true;
        if($debug){echo "BEGIN clientOnWhiteList---<br>"; }
        $whiteListQuery="SELECT * FROM APIUsers ";
        $whiteListQuery.="WHERE emailAddress='$clientEmail' AND (airtimeStatus='VERIFIED' OR airtimeStatus='DEALER')";
        if($debug){echo"Whitelist query: $whiteListQuery <br>";}
        $whiteListResult = mysql_query($whiteListQuery,$connect) or handleDatabaseError(''.mysql_error(),$whiteListQuery);

        if($debug){echo "---END clientOnWhiteList<br>"; }
        if (mysql_num_rows($whiteListResult) > 0) { return true; }
        else{ return false; }
    }

        /* Create connection outside of this function then invoke it */
    function clientOnWhiteList($clientEmail,$countryAbbreviation,$connect){
        $debug = false;
        if($debug){echo "BEGIN clientOnWhiteList---<br>"; }
        $whiteListQuery="SELECT * FROM SMSUsers ";
        $whiteListQuery.="WHERE emailAddress='$clientEmail' AND (airtimeStatus='VERIFIED' OR airtimeStatus='DEALER')";
        if($debug){echo"Whitelist query: $whiteListQuery <br>";}
        $whiteListResult = mysql_query($whiteListQuery,$connect) or handleDatabaseError(''.mysql_error(),$whiteListQuery);

        if($debug){echo "---END clientOnWhiteList<br>"; }
        if (mysql_num_rows($whiteListResult) > 0) { return true; }
        else{ return false; }
    }

    //function to connect to any country's database based on the country abbreviation
    function connect2SMSDB2($countryAbbreviation)
    {
        $debug = false;
        if($debug){echo "BEGIN connect2SMSDB2---<br>"; }

        $connection = mysql_pconnect(LIVE_DB_ADDRESS, LIVE_DB_USERNAME, LIVE_DB_PASSWORD) or 
                                     handleDatabaseError(''.mysql_error(),"mysql_pconnect command");

        //b make sure that the database connects
        if ($connection == false){
            echo "<li>Failed to get the Database connection, please try again later...</li>";
            exit;
        }

        mysql_select_db(LIVE_DB_NAME,$connection) or handleDatabaseError('Could not select database','connectDB');

        if($debug){echo "---END connect2SMSDB2<br>"; }
        return $connection;
    }

?>
