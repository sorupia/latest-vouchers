<!-- Facebook Conversion Code for Checkouts - SendAirTime.com -->
<script>
    (function()
    {
        var _fbq = window._fbq || (window._fbq = []);

        if (!_fbq.loaded)
        {
            var fbds = document.createElement('script');
            fbds.async = true;
            fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s);
            _fbq.loaded = true;
        }
    })();

    window._fbq = window._fbq || [];

    window._fbq.push(['track', '6022177393481', {'value':'<?=$paymentAmount?>','currency':'<?=$paymentCurrency?>'}]);
</script>
<noscript>
    <img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6022177393481&amp;cd[value]=<?=$paymentAmount?>&amp;cd[currency]=<?=$paymentCurrency?>&amp;noscript=1" />
</noscript>