<!-- Original:  Ronnie T. Moore -->
<!-- Dynamic 'fix' by: Nannette Thacker -->
<!-- Web Site: http://www.shiningstar.net -->
<!-- The JavaScript Source!! http://javascript.internet.com -->
//submit any form
function sub_frm(){
	document.login.submit();
}
<!-- Begin
function textCounter(field, countfield, maxlimit) {
if (field.value.length > maxlimit) // if too long...trim it!
field.value = field.value.substring(0, maxlimit);
// otherwise, update 'characters left' counter
else 
countfield.value = maxlimit - field.value.length;
}
// End -->

//developed by Arthur Ntozi for 3-NITYLABS
//this function verifies the sms module on the main page to ensure that 
//messages are only sent to Ugandan mobile phones and also that these mobile phones
//are mtn,utl or ctl
function smsChecker(phoneNumberIN, smsMessageIN){
	//detailed check for phone number
	//phone number must start with 077,078,071 and 075
	var returnValue=false;
	var phoneNumber=phoneNumberIN.value;
	var smsContent=smsMessageIN.value;
	var smsLength=smsContent.length;

	if(phoneNumber.length >0 || smsContent.length>0){
	
		if(phoneNumber.length!=13){
			alert("Phone Number should contain 12 digits i.e. +256782551234: \n Please re-enter number and then Submit message.");
		}
		else if(phoneNumber.length==13 && smsContent.length==0){
			alert("Please fill the message field");
		}
		else{//+256782551234
			var firstElement = phoneNumber.charAt(0);
			var secondElement = phoneNumber.charAt(1);
			var thirdElement = phoneNumber.charAt(2);
			var fourthElement = phoneNumber.charAt(3);
			var fifthElement =  phoneNumber.charAt(4);
			var sixthElement =  phoneNumber.charAt(5);
			var seventhElement = phoneNumber.charAt(6);
			
			if(secondElement==2 && thirdElement==5 && fourthElement==6 && fifthElement==7 && 
				(sixthElement==7 || sixthElement==8 || sixthElement==5 || sixthElement==1 ||
                 sixthElement==0 || sixthElement==9)){
				//alert("Phone Number is valid")
				//now check to see whether there are less than 155 characters in the sms message field
				if(smsLength>143){
					alert("The message is "+smsLength+" characters long but can only be 143 please shorten your message");
				}			
				else{
					returnValue = true;
				}
			}
			else{
				alert("Phone Number's first five digits has a problem: \n The phone must start as +25677,+25678,+25671,+25675,+25670 \n Please re-enter number and then proceed to type message.")
			}
		}
	}
	if (returnValue==false) phoneNumberIN.select();
	return returnValue;
}

//ensure that a user does not send mtn airtime to ctl
//since we have airtime for 4 different networks, we need to ensure that a user is only sending to
//the network that corresponds to the airtime purchased.
function verifyPhoneWithCard(itemName,phoneNumberField){
	//alert("itemName = "+itemName.value.substr(0,3));
	var network=itemName.value.substr(0,3);
	var phone=phoneNumberField.value.substr(4,2);
	if((network=="MTN" && (phone=="77" || phone=="78"))
	||(network=="UTL" && (phone=="71"))
	||(network=="CTL" && (phone=="75"))
    ||(network=="ZAI" && (phone=="75"))
    ||(network=="AIR" && (phone=="75"))
    ||(network=="WAR" && (phone=="70"))
    ||(network=="WRD" && (phone=="70"))
    ||(network=="ORA" && (phone=="79"))){
		return true;
	}
	else{
		alert("Make sure that the phone is on "+network+"'s network");
		return false;
	}
}

//function to check form that will send the pin to the receiver
function pinSenderChecker(phoneNumberField, phoneNumberField2, itemName){
	
	//detailed check for phone number
	if(phoneNumberField.value==phoneNumberField2.value){
		//phone number must start with +25677,+25678,+25671 and +25675
		var returnValue=false;
		var phoneNumber=phoneNumberField.value;
	
		if(phoneNumber.length >0){
			if(phoneNumber.length!=13){
				alert("Phone Number should contain 13 characters i.e. +256782551234: \n Please re-enter number and then Submit message.");
				phoneNumberField.select()
				return false;
			}
			else{
				var firstElement =  phoneNumber.charAt(0);
				var secondElement =  phoneNumber.charAt(1);
				var thirdElement =  phoneNumber.charAt(2);
				var fourthElement =  phoneNumber.charAt(3);
				var fifthElement =  phoneNumber.charAt(4);
				var sixthElement =  phoneNumber.charAt(5);
				var seventhElement = phoneNumber.charAt(6);
				//var thirdElement = phoneNumber.charAt(2);
				
				if(secondElement==2 && thirdElement==5 && fourthElement==6 && fifthElement==7 && 
					(sixthElement==7 || sixthElement==8 || sixthElement==5 || sixthElement==1 || 
                     sixthElement==0 || sixthElement==9)){
					//alert("Phone Number is valid")
					//now check to see whether there are less than 155 characters in the sms message field
					var isAMatch=verifyPhoneWithCard(itemName,phoneNumberField);
					if(isAMatch==true){
						return true;
					}
					else{
						phoneNumberField.select()
						return false;
					}
				}
				else{
					alert("Phone Number's first five digits has a problem: \n The phone must start as +25677,+25678,+25671,+25675,+25670 \n Please re-enter number and then proceed to type message.")
					phoneNumberField.select()
					return false;
				}
			}
		}
		else{//else (phoneNumber.length <=0)
			phoneNumberField.select();
			return false;
		}
		//if (returnValue==false) phoneNumberField.select();
		//return returnValue;
	}//if (verifyPhoneWithCard(itemName,phoneNumberField))
	else{
		alert("The phone numbers you entered do not match please check them and try again");
		phoneNumberField.select();
		return false;
	}
}


<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- V1.1.3: Sandeep V. Tamhankar (stamhankar@hotmail.com) -->
<!-- Original:  Sandeep V. Tamhankar (stamhankar@hotmail.com) -->
<!-- Changes:
/* 1.1.4: Fixed a bug where upper ASCII characters (i.e. accented letters
international characters) were allowed.

1.1.3: Added the restriction to only accept addresses ending in two
letters (interpreted to be a country code) or one of the known
TLDs (com, net, org, edu, int, mil, gov, arpa), including the
new ones (biz, aero, name, coop, info, pro, museum).  One can
easily update the list (if ICANN adds even more TLDs in the
future) by updating the knownDomsPat variable near the
top of the function.  Also, I added a variable at the top
of the function that determines whether or not TLDs should be
checked at all.  This is good if you are using this function
internally (i.e. intranet site) where hostnames don't have to 
conform to W3C standards and thus internal organization e-mail
addresses don't have to either.
Changed some of the logic so that the function will work properly
with Netscape 6.

1.1.2: Fixed a bug where trailing . in e-mail address was passing
(the bug is actually in the weak regexp engine of the browser; I
simplified the regexps to make it work).

1.1.1: Removed restriction that countries must be preceded by a domain,
so abc@host.uk is now legal.  However, there's still the 
restriction that an address must end in a two or three letter
word.

1.1: Rewrote most of the function to conform more closely to RFC 822.

1.0: Original  */
// -->

<!-- Begin
function emailCheck (emailStr) {

	
	/* The following variable tells the rest of the function whether or not
	to verify that the address ends in a two-letter country or well-known
	TLD.  1 means check it, 0 means don't. */
	
	var checkTLD=1;
	
	/* The following is the list of known TLDs that an e-mail address must end with. */
	
	var knownDomsPat=/^(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum)$/;
	
	/* The following pattern is used to check if the entered e-mail address
	fits the user@domain format.  It also is used to separate the username
	from the domain. */
	
	var emailPat=/^(.+)@(.+)$/;
	
	/* The following string represents the pattern for matching all special
	characters.  We don't want to allow special characters in the address. 
	These characters include ( ) < > @ , ; : \ " . [ ] */
	
	var specialChars="\\(\\)><@,;:\\\\\\\"\\.\\[\\]";
	
	/* The following string represents the range of characters allowed in a 
	username or domainname.  It really states which chars aren't allowed.*/
	
	var validChars="\[^\\s" + specialChars + "\]";
	
	/* The following pattern applies if the "user" is a quoted string (in
	which case, there are no rules about which characters are allowed
	and which aren't; anything goes).  E.g. "jiminy cricket"@disney.com
	is a legal e-mail address. */
	
	var quotedUser="(\"[^\"]*\")";
	
	/* The following pattern applies for domains that are IP addresses,
	rather than symbolic names.  E.g. joe@[123.124.233.4] is a legal
	e-mail address. NOTE: The square brackets are required. */
	
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	
	/* The following string represents an atom (basically a series of non-special characters.) */
	
	var atom=validChars + '+';
	
	/* The following string represents one word in the typical username.
	For example, in john.doe@somewhere.com, john and doe are words.
	Basically, a word is either an atom or quoted string. */
	
	var word="(" + atom + "|" + quotedUser + ")";
	
	// The following pattern describes the structure of the user
	
	var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
	
	/* The following pattern describes the structure of a normal symbolic
	domain, as opposed to ipDomainPat, shown above. */
	
	var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");
	
	/* Finally, let's start trying to figure out if the supplied address is valid. */
	
	/* Begin with the coarse pattern to simply break up user@domain into
	different pieces that are easy to analyze. */
	
	var matchArray=emailStr.match(emailPat);
	
	if (matchArray==null) {
	
	/* Too many/few @'s or something; basically, this address doesn't
	even fit the general mould of a valid e-mail address. */
	
	alert("Email address seems incorrect (check @ and .'s)");
	return false;
	}
	var user=matchArray[1];
	var domain=matchArray[2];
	
	// Start by checking that only basic ASCII characters are in the strings (0-127).
	
	for (i=0; i<user.length; i++) {
	if (user.charCodeAt(i)>127) {
	alert("Ths email address contains invalid characters.");
	return false;
	   }
	}
	for (i=0; i<domain.length; i++) {
	if (domain.charCodeAt(i)>127) {
	alert("Ths domain name contains invalid characters.");
	return false;
	   }
	}
	
	// See if "user" is valid 
	
	if (user.match(userPat)==null) {
	
	// user is not valid
	
	alert("The email address doesn't seem to be valid.");
	return false;
	}
	
	/* if the e-mail address is at an IP address (as opposed to a symbolic
	host name) make sure the IP address is valid. */
	
	var IPArray=domain.match(ipDomainPat);
	if (IPArray!=null) {
	
	// this is an IP address
	
	for (var i=1;i<=4;i++) {
	if (IPArray[i]>255) {
	alert("Destination IP address is invalid!");
	return false;
	   }
	}
	return true;
	}
	
	// Domain is symbolic name.  Check if it's valid.
	 
	var atomPat=new RegExp("^" + atom + "$");
	var domArr=domain.split(".");
	var len=domArr.length;
	for (i=0;i<len;i++) {
	if (domArr[i].search(atomPat)==-1) {
	alert("The domain name does not seem to be valid.");
	return false;
	   }
	}
	
	/* domain name seems valid, but now make sure that it ends in a
	known top-level domain (like com, edu, gov) or a two-letter word,
	representing country (uk, nl), and that there's a hostname preceding 
	the domain or country. */
	
	if (checkTLD && domArr[domArr.length-1].length!=2 && 
	domArr[domArr.length-1].search(knownDomsPat)==-1) {
		alert("The address must end in a well-known domain or two letter " + "country.");
		return false;
	}
	
	// Make sure there's a host name preceding the domain.
	
	if (len<2) {
		alert("This address is missing a hostname!");
		return false;
	}
	
	// If we've gotten this far, everything's valid!
	return true;
}

//function to check form that will send the pin to the receiver
function pinSenderChecker2(phoneNumberField, phoneNumberField2, itemName, email){
	//var emailIsOK=emailCheck(email.value);
	if(email.value!=''){//check the email only if its not empty
		if(!emailCheck(email.value)){//return false if it does not conform to RFC822
			return false;
		}
	}

	
	//detailed check for phone number
	if(phoneNumberField.value==phoneNumberField2.value){
		//phone number must start with +25677,+25678,+25671 and +25675
		var returnValue=false;
		var phoneNumber=phoneNumberField.value;
	
		if(phoneNumber.length >0){
			if(phoneNumber.length!=13){
				alert("Phone Number should contain 13 characters i.e. +256782551234: \n Please re-enter number and then Submit message.");
				phoneNumberField.select()
				return false;
			}
			else{
				var firstElement =  phoneNumber.charAt(0);
				var secondElement =  phoneNumber.charAt(1);
				var thirdElement =  phoneNumber.charAt(2);
				var fourthElement =  phoneNumber.charAt(3);
				var fifthElement =  phoneNumber.charAt(4);
				var sixthElement =  phoneNumber.charAt(5);
				var seventhElement = phoneNumber.charAt(6);
				//var thirdElement = phoneNumber.charAt(2);
				
				if(secondElement==2 && thirdElement==5 && fourthElement==6 && fifthElement==7 && 
					(sixthElement==7 || sixthElement==8 || sixthElement==5 || sixthElement==1 ||
                     sixthElement==0 || sixthElement==9)){
					//alert("Phone Number is valid")
					//now check to see whether there are less than 155 characters in the sms message field
					var isAMatch=verifyPhoneWithCard(itemName,phoneNumberField);
					if(isAMatch==true){
						return true;
					}
					else{
						phoneNumberField.select()
						return false;
					}
				}
				else{
					alert("Phone Number's first five digits has a problem: \n The phone must start as +25677,+25678,+25671,+25675, +25670 \n Please re-enter number and then proceed to type message.")
					phoneNumberField.select()
					return false;
				}
			}
		}
		else{//else (phoneNumber.length <=0)
			phoneNumberField.select();
			return false;
		}
		//if (returnValue==false) phoneNumberField.select();
		//return returnValue;
	}//if (verifyPhoneWithCard(itemName,phoneNumberField))
	else{
		alert("The phone numbers you entered do not match please check them and try again");
		phoneNumberField.select();
		return false;
	}

}

function phoneChecker(phone){
		var returnValue=false;
		
		var phoneNumber=phone.value;
		//var length=phoneNumber.length;
		var firstElement = phoneNumber.charAt(0);
		var secondElement = phoneNumber.charAt(1);
		var thirdElement = phoneNumber.charAt(2);
		var fourthElement = phoneNumber.charAt(3);
		var fifthElement =  phoneNumber.charAt(4);
		var sixthElement =  phoneNumber.charAt(5);
		var seventhElement = phoneNumber.charAt(6);
		
		if(secondElement==2 && thirdElement==5 && fourthElement==6 && fifthElement==7 && 
			(sixthElement==7 || sixthElement==8 || sixthElement==5 || sixthElement==1 ||
             sixthElement==0 || sixthElement==9)){
			//alert("Phone Number is valid")
			//now check to see whether there are less than 155 characters in the sms message field
			if(phoneNumber.length==13){
				//alert('Phone Number Length = '+phoneNumber.length);
				return true;
			}
			else{
				return false;
			}
			
		}
		else{
			alert("Phone Number's first five digits has a problem: \n The phone must start as +25677,+25678,+25671,+25675,+25670 \n Please re-enter number and then proceed to type message.")
			return false;
		}
		return false;
}

//function to check form submitted when user is distracted by paypal
//and transaction fails.
function checkPPRPFailForm(payPalEmail, phoneNumberField, phoneNumberField2, receiverEmail){

	//var formIsOk=false;
	//make sure payPalEmail is valid
	if( emailCheck (payPalEmail.value)){
		//make sure phoneNumberField is valid format
		if(phoneChecker(phoneNumberField)){
			//make sure phoneNumberField==phoneNUmberField2
			if(phoneNumberField.value==phoneNumberField2.value){
				//make sure receiverEmail is valid
				if(receiverEmail.value.length==0){
					formIsOk=true;
					return true;
				}
				else if(emailCheck(receiverEmail.value)){
					formIsOk=true;
					return true;
				}
				else{//receiverEmail is invalid
					return false;
				}
			}
			else{//phoneNumbers do not match
				alert("The phone numbers you entered do not match please review the form");
				return false;
			}
		}
		else{//the phoneNumber field is not in the correct format
			//the function already reports this to the user through an alert 
			//hence no need to insert an alert here
			alert("Receiver phone is invalid please review the form");
			return false;
		}
	}
	else{//email is invalid and has already been reported to the user
		alert("Client email is invalid please review the form");
		return false;
	}
	
	//return formIsOk;
}

//function to make sure user does not send airtime to "None" in history sending
//support for src/Displays/DisplaySendHistory.php
function checkHistorySend(destinationNumber,itemName){
	var numberFromHistory = destinationNumber.value;
	if(numberFromHistory=="None"){
		alert("Please select a number from the send history or input a new number in the form below");
		return false;
	}
	return verifyPhoneWithCard(itemName,destinationNumber);
	//for test purposes
	//alert("You have chosen a destination number");
	//return true;
}

function checkRegistrationInfo(fname, lname,email,phone,pass,pass2,address,city,sprov,country,pzcode,randomText){
	var firstName = fname.value;
	var lastName = lname.value;
	var emailAddress = email.value;
	var phoneNumber = phone.value;
	var password = pass.value;
	var password2 = pass2.value;
	var add = address.value;
	var cityValue = city.value;
	var province = sprov.value;
	var countryValue = country.value;
	var postalCode = pzcode.value;
	var randomTxt = randomText.value;

	if(firstName==""||lastName==""
	  ||phoneNumber==""||password==""||add==""||cityValue==""||
	  province==""||countryValue==""||postalCode==""||randomTxt==""){
		alert("Please fill in all the fields");
		return false;
	}
	
	if(!emailCheck(email.value)){
		//alert("Please enter a valid email address");
		return false;
	}
	
	if(password!=password2){
		alert("Passwords do not match, please re-enter passwords");
		return false;
	}
	return true;
}

function IncompletePaymentQChecker(phoneNumberField, phoneNumberField2, itemIDInfo, email, transactionID, clientFirstName)
{
	
	//do not even bother if the transaction ID is missing
    if(transactionID.value == "" ){
        alert("Please enter a Transaction ID");
        return false;
    }
	
	if(clientFirstName.value == "" ){
        alert("Please enter Client First Name");
        return false;
    }
	
	if(itemIDInfo.value == "" ){
        alert("Please select an Item ID");
        return false;
    }
	
	return true;
	//extract the network abbreviation
	//var network = itemIDInfo.value.substr(2,3);
    //network = network.toUpperCase(network);
	//itemIDInfo.value = ""+network;
    //return pinSenderChecker2(phoneNumberField, phoneNumberField2, itemIDInfo, email);
}