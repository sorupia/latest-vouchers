<?php
	
	
	if($_POST['subscribe4AT']){
		include 'GeneralFunctions.php';
		// we must never forget to start the session
		session_start();
		
		//get the input data
		$clientEmail=trim($_POST['clientEmail']);
		$clientPassword=trim($_POST['clientPassword1']);
		$countryAbbreviation=trim($_POST['countryAbbreviation']);		
		//call the function to create a new subscriber
		createSubscriber($clientEmail,$clientPassword,$countryAbbreviation);
		
		//obtain subscribers details
		$subscriberDetailsQuery ="SELECT clientFirstName, clientLastName, addressCountry ";
		$subscriberDetailsQuery.="FROM Clients WHERE clientEmail='$clientEmail'";
		$subscriberDetailsData=executeQuery($subscriberDetailsQuery,$countryAbbreviation);
		
		$_SESSION['clientFirstName']=$subscriberDetailsData['clientFirstName'];
		$_SESSION['clientLastName']=$subscriberDetailsData['clientLastName'];
		$_SESSION['clientCountry']=$subscriberDetailsData['addressCountry'];
		$_SESSION['clientEmail']=$clientEmail;
		
		//login subscriber
		$_SESSION['ATSubscriberLoggedIn']=true;
		
				
		//redirect to confirmation page
		header("Location: ".INHERIT_SITE."signup/confirmation.php?purpose=airtime");
	}

?>