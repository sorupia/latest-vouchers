<?php
/*******************************************************************************
**   FILE: constants.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Static Config file for web application
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 15.Jul.2006
**
**   MODIFIED BY: Arthur Ntozi (3nitylabs-AVMM, Kampala)   DATE: 30.Nov.2015
**   Code review: replaced tabs for whitespaces
**
*********************************************************************************/

    //error_reporting(0);

    $constCreditsIssued=62;
    $SIGNUPEMAIL='signup@sendairtime.com'; //keeps records of signups
    $CARDADMINEMAIL='cardadmin@sendairtime.com';//keeps records of card purchases
    define("SMS_MAX_CHARS",140);
    define("COUNTRY_CODE",256);
    define("COUNTRY_MOBILE_PREFIX","+256");
    define("COUNTRY_NAME","Uganda");
    define("COUNTRY_ABBREVIATION","ug");
    define("SMS_MAX_CREDITS",62);
    define("MGMT_NAME","3nitylabs Inc.");
    define("SERVICED_BY","3nitylabs Inc.");
    define("SUCCESS_SMS_RETURN_PAGE","https://sms.sendairtime.com");
    define("PURCHASE_SMS_RETURN_PAGE","https://sms.sendairtime.com");
    define("SMS_PAGE_URL","https://sms.sendairtime.com");
    define("INHERIT_SITE","http://vouchers-ni.3nitylabs.com/");
    define("SMS_CONSOLE_NAME","sms.sendairtime.com");
    define("DOMAIN_PREFIX","vouchers-ni.3nitylabs.com/");
    define("DOMAIN_NAME","SendAirTime.com");
    define("DOMAIN_SLOGAN","Instant Airtime in No time");
    define("SERVICE_PROVIDER","3nitylabs Inc.");
    define("SERVICE_PROVIDER_ADDRESS","Edmonton, Alberta, Canada.");
    define("SUPPORT_PHONE","+1.617.340.2408");

    define("TWITTER_NAME","sendairtime");
    define("FACEBOOK_NAME","sendairtime");
    define("SKYPE_NAME","sendairtime");

    //CHANGE back to live page
    define("INHERIT_PATH","/home/148612/domains/vouchers-ni.3nitylabs.com/html/");

    //Real
    //define("PAYPAL_LIVE","ssl://www.paypal.com");
    //define("PAYPAL_PAYMENT_URL","https://www.paypal.com/cgi-bin/webscr");
    //define("PAYPAL_SUCCESS_URL","https://vouchers-ni.3nitylabs.com/return_page_vchr.php");
    //define("PAYPAL_CANCEL_URL","https://vouchers-ni.3nitylabs.com/");
    //define("PAYPAL_NOTIFY_URL","https://vouchers-ni.3nitylabs.com/ipn_processor.php");
    //define("PAYPAL_HOSTNAME","www.paypal.com");

    //Test
    define("PAYPAL_LIVE","ssl://www.sandbox.paypal.com");    
    define("PAYPAL_PAYMENT_URL","https://www.sandbox.paypal.com/cgi-bin/webscr");
    define("PAYPAL_SUCCESS_URL","https://vouchers-ni.3nitylabs.com/return_page.php");
    define("PAYPAL_CANCEL_URL","https://vouchers-ni.3nitylabs.com/");
    define("PAYPAL_NOTIFY_URL","https://vouchers-ni.3nitylabs.com/ipn_processor.php");
    define("PAYPAL_HOSTNAME","www.paypal.com");

    define("PAYPAL_LIVE_PORT",443);
    define("PAYPAL_CMD","_s-xclick");
    define("PAYPAL_EMAIL","pay@sendairtime.com");
    define("PAYPAL_RETURN_METHOD",2);
    define("LOCALE","US");
    define("PAYPAL_CMD_AUTO","_xclick");
    define("PAYPAL_BN","toolkit-php");
    define("PAYPAL_DISPLAY_SHIPPING_ADDRESS","");

    //For a non-ssl setup
    //define("PAYPAL_LIVE","www.paypal.com");
    //define("PAYPAL_LIVE_PORT",80);

    //PayPal percentage fees International, US/CA
    //define("PP_TX_PCT_INT",0.032);
    //define("PP_TX_PCT_US",0.022);
    //define("PP_TX_FEE",0.3);

    //PayPal percentage fees International, US/CA
    define("PP_TX_PCT_INT",0.032);
    define("PP_TX_PCT_US",0.022);
    define("PP_TX_FEE",0.3);

    define("SUPPORT_NAME","SendAirTime.com Support");
    define("POINTS_EMAIL","points@sendairtime.com");
    define("RECORDS_EMAIL","records@sendairtime.com");
    define("CARD_ADMIN","cardadmin@sendairtime.com");
    define("CARD_LOAD","cardload@sendairtime.com");
    define("SIGNUP_EMAIL","records@sendairtime.com");
    define("FROM_EMAIL","support@sendairtime.com");
    define("FROM_NAME","Sendairtime dot Com");
    define("SUPER_TECH_ADMIN","techadmin@sendairtime.com");
    define("TECH_ADMIN","techadmin@sendairtime.com");
    define("SUPPORT_EMAIL","support@sendairtime.com");
    define("SUPPORT_PASSWD","fr4m1ngh4m");
    define("DEFAULT_CLIENT_EMAIL","cardadmin@sendairtime.com");

    //define("DEV_CELL","+15146231699");
    define("DEV_CELL","+256790834853");
    define("SUPPORT_PHONE_1","+1.617.340.2408");
    define("FOREIGN_SUPPORT_PHONE","+1.617.340.2408");
    define("FOREIGN_SUPPORT_PHONE_COUNTRY","USA");
    define("LOCAL_SUPPORT_PHONE","");
    define("LOADER_CELL","+256775260647");
    define("LOADER_CELL_2","+256775670970");
    define("MANAGER_CELL","+256792700888");
    define("SUPPORT_CELL","+15877832000");
    define("NOTIFY_PHONE","+256775260647");
    $notifyPhones = array(DEV_CELL,MANAGER_CELL,LOADER_CELL,LOADER_CELL_2);
    $newClientNotifyPhones = array(DEV_CELL,SUPPORT_CELL,LOADER_CELL);

    define("DEV_EMAIL","antozi@sendairtime.com");
    define("LOADER_EMAIL","clovis.wanziguya@sendairtime.com");
    define("LOADER_EMAIL_2","paul.ndungutse@sendairtime.com");
    define("MANAGER_EMAIL","olavi.ndaula@sendairtime.com");
    $notifyEmails = array(SUPPORT_EMAIL, DEV_EMAIL, LOADER_EMAIL, MANAGER_EMAIL, LOADER_EMAIL_2);

    define("SALE_CURRENCY","USD");
    define("LOCAL_CURRENCY","UGX");

    define("DAILY_TRANSACTION_LIMIT",100);
    //define("DAILY_TRANSACTION_LIMIT",10);
    define("NEW_CLIENT_24HR_TX_LIMIT",4);
    define("NEW_CLIENT_48HR_TX_LIMIT",6);

    define("MONEY_TRANSFER_RATE",2890.32);
    define("EXCHANGE_RATE",2890.32);
    //define("EX_RATE_LOSS",0.90);
    define("EX_RATE_LOSS",1);

    define("SMTP_SERVER","localhost");
    define("ERROR_NAME","Sendairtime Web");
    define("OUT_QUEUE_ENABLED",FALSE);

    //clickatel
    define("SMS_GATEWAY","clickatel");
    define("MTN_SMS_GATEWAY","infobip");
    //define("MTN_SMS_GATEWAY","clickatel");
    define("PRICE_PER_SMS",0.0444968);

    //Ghana
    //define("CLICKATELL_USER","ghanaairtime");
    //define("CLICKATELL_PASSWD","ghana21");
    //define("CLICKATELL_DISPLAY","+233245867777");
    //define("CLICKATELL_API_ID","2985505");

    define("CLICKATELL_USER","antozi");
    define("CLICKATELL_PASSWD","sb4-t3ch");
    define("CLICKATELL_DISPLAY","3nitylabs");
    define("CLICKATELL_DISPLAY_AT","gotairtime");
    define("CLICKATELL_URL","http://api.clickatell.com");
    define("CLICKATELL_API_ID","2737070");
    $clickAPIID=array("15149"=>"2737070","25677"=>"2735257","25678"=>"2735257","25671"=>"2735259",
                        "25675"=>"2735258");

    define("INFOBIP_USER","ghaairt");
    define("INFOBIP_PASSWD","ghantime");                    

    //Used to enable the frontpage message board txt using msgBoard.php
    define("MSG_BOARD_ON",true);

    //define("GOOGLE_ANALYTICS_ACCT","UA-696547-4"); //Ghana
    //define("GOOGLE_ANALYTICS_ACCT","UA-696547-1"); //Uganda
    //define("GOOGLE_ANALYTICS_ACCT",""); //UgandaVouchers.com
    define("GOOGLE_ANALYTICS_ACCT_YO","UA-696547-5"); //Uganda MoMo purchases

    //Rewards Point System
    define("POINTS_SYSTEM_ENABLED",false);
    define("REWARD_POINTS_REQUIRED",300);
    define("REWARD","21 GHC Card");

    //Referral Point System
    define("REFERRAL_POINTS_REQUIRED",5);
    define("REFERRAL_REWARD","21 GHC Card");

    define("PURCHASE_NOTIFY_SMS",false);
    define("PURCHASE_NOTIFY_EMAIL",false);

    define("NOTIFY_ON_LOW_CARDS",true);
    define("LOW_CARD_THRESHOLD",10);

    //MaxMind License Key
    //Ghana
    //define("MAX_MIND_LICENSE","hwsJFBE4KEFN");
    //Uganda
    define("MAX_MIND_LICENSE","HhO8rkLFUSEY");
    define("REPORT_MAX_MIND_ERROR",false);

    define("TIME_ZONE",'America/Los_Angeles');
    //define("TIME_ZONE",'America/New_York');
    //date_default_timezone_set(TIME_ZONE);

    //define("BLACK_LIST_ACTIVE",true);
    define("BLACK_LIST_ACTIVE",false);
    define("WHITE_LIST_ACTIVE",true);

    define("RX_PHONE_LENGTH",13);
    define("RX_PHONE_PREFIX_LENGTH",6);
    define("MNP_IN_USE",false);
    define("VERIFY_RECEIVER_STATUS",true);

    define("LIMIT_RECEIVER", false);
    define("RECEIVER_TX_LIMIT", 10);
    define("RECEIVER_UPPER_LIMIT", 200000);
    define("RECEIVER_LOWER_LIMIT", 5000);
    define("RX_LIMIT_TIMELINE", 1);
    define("CMP_RCVR_AMT_WITH_YO_BAL", true);
    define("NFY_CMP_RCVR_AMT_WITH_YO_BAL", true);
    define("ROUND_RECEIVER_AMOUNT_IN_LOCAL", true);

    define("LIMIT_IP", false);
    define("CLIENT_LIMIT", 150);
    define("CX_LIMIT_TIMELINE", 1);

    define("MATCH_COUNTRY",false);
    define("MATCH_STATE",false);
    define("FLAG_REUSED_IP",true);
    define("FLAG_REUSED_TEL",true);
    define("FLAG_MULTI_STATE",true);
    define("BLOCK_FLAGGED_PURCHASE",false);
    define("REUSED_IP_MONTHS_TIMELINE", 3);

    define("FLAG_REUSED_RECEIVER",true);
    define("REPORT_REUSED_RECEIVER_PHONE",false);
    define("REUSED_RECEIVER_PHONE_MONTHS",0);

    define("DEBUG_PDT",true);
    define("DEBUG_IPN",true);

    define("FIRST_NAME_MAX_LENGTH",12);
    define("LAST_NAME_MAX_LENGTH",12);

    //Ghana PDT
    //define("PDT_TOKEN","zsU-tbJ9FGut2DtrpFB5KS2hibbnYOa2N2kC8sU0Jmk-wyRS3pqQb284MUq");//Real
    //define("PDT_TOKEN","9eMTmtVIS9P5ipiwdu1mbTZfflQloyBKa_qAqBkLzbtZJgQu3LnWRnztpI8");//Test

    //Uganda PDT
    //define("PDT_TOKEN","ikPL0KaLSwL-JuddYxyZtBkMT_5UmFAmLM6lEO5pr0kIRjRjLga4nrgr3ta");//Real
    define("PDT_TOKEN","M5MFFCwxfbwmijoh6PpxWZ6jH6MjRK1cK26c-mvYtiehYdRUXlnB37fytUa");//Test

    define("TRANSACTION_ID_PREFIX","3nity");

    define("YO_PAYMENT_ENABLED",true);

    //Airtime Processor Transaction Fees
    define("AP_TX_FIXED_FEE_IN_LOCAL", 0);
    define("AP_TX_PCT_FEE", 0);

    //Auto Dispatch/ Auto Load/ E-Recharge/ Direct top-up
    define("AUTO_LOAD_ENABLED", false);
    define("VOUCHER_LOAD_ENABLED", true);
    define("CARD_LOAD_ENABLED", false);
    define("AIRTIME_LOAD_OPTION_AUTO", 'auto');
    define("AIRTIME_LOAD_OPTION_CARD", 'card');
    define("AIRTIME_LOAD_OPTION_VCHR", 'vchr');
    define("ALLOW_IPN_CARD", false);
    define("ALLOW_PDT_AUTO", true);

    define("PAYMENT_OPTION_UG_MOMO", 'ugmomo');
    define("PAYMENT_OPTION_PAYPAL", 'paypal');

    //our fees when charging foreign clients
    define("WEBSHOP_TX_PCT_FEE_FOREIGN",0.13);
    define("WEBSHOP_TX_FIXED_FEE_FOREIGN",0);

    define("PRODUCT","Supermarket Voucher");

    //Currency decimal places
    define("LOCAL_DECIMAL_PLACES",0);
    define("FOREIGN_DECIMAL_PLACES",2);

    define("NOTIFY_VIEW_OF_REPORTS_ENABLED",false);
    define("NOTIFY_VIEW_OF_REPORTS_EMAIL","antozi@sendairtime.com");

    define("IP_HARD_CODE_BLOCK_ENABLED",false);
    define("COUNTRY_HARD_CODE_BLOCK_ENABLED",false);

    define("PAY_PROC_FIXED_FEE",0);
    define("PAY_PROC_US_PCT",0.022);
    define("PAY_PROC_INT_PCT",0.032);

    define("ADMIN_TX_PCT_FEE",0.05);
    define("ADMIN_TX_FIXED_FEE",0);

    //
    define("PHPMAILER_LOCATION","/home/148612/users/.home/domains/vouchers-ni.3nitylabs.com/html/lib/PHPMailer_5");
    
    define("NOTIFY_ON_LOW_YO_BALANCE",true);
    define("LOW_YO_BALANCE_THRESHOLD",400000); 
    
    // Delivery Management System Webservice URL & Logs
    define("DELIVERY_SERVICE_ENABLED", true);
    define("DELIVERY_LOG_PATH", "Logs"); 
    define("DELIVERY_URL", "http://www.peakbw.com/sandbox/avmmdelivery/backend/delivery/vendor_to_delivery_request_handler.php");
              
    // define active theme
    // will always be prepended wth DOCUMENT_ROOT
    define("ACTIVE_THEME_PATH", "themes/ni");

    //this will get the card details that we need.
    include_once "cards.php";

    //database info for each country
    include_once "dbInfo.php";

    //country info for each country
    include_once "countryInfo.php";

    //email info
    include_once "emailInfo.php";
?>
