<?
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/GeneralFunctions.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/sendATSimpleAuto.php';

    function delete_order_queue_item($countryAbbreviation,
                                     $order_id)
    {
        $connection = connect2DB2($countryAbbreviation);

            $query = "DELETE ";
            $query.= "FROM Ordered_Cart_Items ";
            $query.= "WHERE order_id = '$order_id'";
            $result=mysql_query($query) or handleDatabaseError(''.mysql_error(),$query);

        disconnectDB($connection);

        return $result;	
    }

    function process_order_queue($countryAbbreviation,
                                 $OrderQueueArray,
                                 $userStatus,
                                 $username)
    {
        /*Its assumed that not all orders in this queue have been paid for. */
        /*The individual processing this queue should do so while verifying */
        /*that the payment received from PayPal is complete*/
        
        /* extract Order ID from OrderQueueArray*/
        $order_id        = $OrderQueueArray['order_id'];
        $itemIDNumber    = $OrderQueueArray['itemIDNumber'];

        $resultArray = array();
        $cardPIN = 0;

        /*use API to send the airtime that was purchased*/
        if($userStatus == 'VERIFIED')
        {
            //print_r($OrderQueueArray);
            $resultArray = sendATSimpleAuto($countryAbbreviation, $OrderQueueArray);
            $status = $resultArray['status'];
            $cardPIN = $resultArray['card'];
        }
        else
        {
            //provide no airtime
        }
        
        /* Delete Order Queue Row only if airtime has been assigned to recipient */
        if( (!isAutoDispatch($itemIDNumber) && $cardPIN != 0) ||
            ( isAutoDispatch($itemIDNumber) && $cardPIN == "AUTO_LOAD"))
        {
            delete_order_queue_item($countryAbbreviation,$order_id);
        }
        
        return $resultArray;
    }

?>