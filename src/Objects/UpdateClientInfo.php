<?
//201012131511: Modified code such that we can cater for special characters in Client's addresses
//201012131511: Added POINTS_SYSTEM_ENABLED to constants.php file, removed tab characters
//201012141247: Added REWARDS_POINTS_REQUIRED, REWARD

function updateClientInfo($countryAbbreviation, $keyarray, $connection)
{
    $pointsSystemEnabled = POINTS_SYSTEM_ENABLED;
    $rewardPointsRequired = REWARD_POINTS_REQUIRED;
    $referralPointsRequired = REFERRAL_POINTS_REQUIRED;
    $rewardsReward = REWARD;
    $inTesting = false;

    //Client information from PayPal.
    $firstname      = $keyarray['first_name'];
    $lastname       = $keyarray['last_name'];
    $clientEmail    = $keyarray['payer_email'];
    $clientID       = $keyarray['payer_id'];
    $addressStreet  = $keyarray['address_street'];
    $addressCity    = $keyarray['address_city'];
    $addressState   = $keyarray['address_state'];
    $addressCountry = $keyarray['address_country'];
    $addressZip     = $keyarray['address_zip'];	
    $paymentAmount  = $keyarray['payment_gross'];
    $clientPhone    = $keyarray['contact_phone'];        

    //Locate this Client in our records
    //we use the Clients table because clients could use a different email address with paypal and with us as usernames
    $isClientInTable="SELECT clientFirstName FROM Clients WHERE clientID='$clientID'";
    $isClientInResult=mysql_query($isClientInTable,$connection) or handleDatabaseError(''. mysql_error(),$isClientInTable);
    $isClientInNumber=mysql_num_rows($isClientInResult);//number of results returned


    $storeClientQuery="";

    //If this client does not exist in our records then add him/her
    if($isClientInNumber==0)
    {
        //new client
        //store clients information
        $dateOFP=date("Y-m-d H:i:s");
        $storeClientQuery="INSERT IGNORE INTO Clients(clientID,clientFirstName, clientLastName,";	
        $storeClientQuery.="dateOfFirstPurchase,";
        $storeClientQuery.="clientEmail, phoneNumber, addressStreet, addressCity,";
        $storeClientQuery.="addressState,addressCountry,addressZip)" ;
        $storeClientQuery.="VALUES('";
        $storeClientQuery.=mysql_real_escape_string($clientID,$connection)."','";
        $storeClientQuery.=mysql_real_escape_string($firstname,$connection)."','";
        $storeClientQuery.=mysql_real_escape_string($lastname,$connection)."','";
        $storeClientQuery.=mysql_real_escape_string($dateOFP,$connection)."','";
        $storeClientQuery.=mysql_real_escape_string($clientEmail,$connection)."','";
        $storeClientQuery.=mysql_real_escape_string($clientPhone,$connection)."','";
        $storeClientQuery.=mysql_real_escape_string($addressStreet,$connection)."','";
        $storeClientQuery.=mysql_real_escape_string($addressCity,$connection)."','";
        $storeClientQuery.=mysql_real_escape_string($addressState,$connection)."','";
        $storeClientQuery.=mysql_real_escape_string($addressCountry,$connection)."','";
        $storeClientQuery.=mysql_real_escape_string($addressZip,$connection)."')";

        if($debug=="true") echo "storeClientQuery = $storeClientQuery<br>";

        $storeClientResult=mysql_query($storeClientQuery,$connection) or handleDatabaseError(''. mysql_error(),$storeClientQuery);

        //REFERRAL POINT SYSTEM
        calculateReferralPoints($keyarray, $connection);

        //Purchase POINTS PROGRAMME
        //PointsTable(clientEmail,points)
        if($pointsSystemEnabled)
        {
            if($paymentAmount>$rewardPointsRequired)
            {
                //is payment amount more than 300?
                //send admin an email
                //inform the admin and issue user a free card
                $body="Hi Admin, send user $clientEmail a free $10 card.\n";
                $body.="The user qualifies for Purchase POINTS";
                sendEmail('OAS Points System',$fromAddress,$cardAdminEmail,POINTS_EMAIL,"Points for - $clientEmail",$body);
                //deduct the refunded 300 points
                $pointsValue=$pointsValue-$rewardPointsRequired;
            }
            else
            {
                //do nothing
            }

            //insert the current amount directly in the points section
            $pointsQuery="INSERT INTO PointsTable(clientEmail,points) VALUES('$clientEmail',$paymentAmount)";

            //todo: possible BUG: paypal creates a new client id for each payment if client is using credit card.
            $pointsResults=mysql_query($pointsQuery,$connection) or handleDatabaseError(''. mysql_error(),$pointsQuery);
        }
        else
        {
            //do nothing
        }
    }//($isClientInNumber==0)
    else
    {//this client already exists
        $storeClientQuery = "UPDATE Clients ";
        $storeClientQuery.= "SET clientFirstName='".mysql_real_escape_string($firstname,$connection)."',";
        $storeClientQuery.= "clientLastName='".mysql_real_escape_string($lastname,$connection)."',";
        $storeClientQuery.= "phoneNumber='".mysql_real_escape_string($clientPhone,$connection)."' ";
        
        if($addressStreet != '')
        {
            $storeClientQuery.=",addressStreet='".mysql_real_escape_string($addressStreet,$connection)."' ";
        }
        
        if($addressCity != '')
        {
            $storeClientQuery.=",addressCity='".mysql_real_escape_string($addressCity,$connection)."' ";
        }
        
        if($addressState != '')
        {
            $storeClientQuery.=",addressState='".mysql_real_escape_string($addressState,$connection)."' ";
        }
        
        if($addressCountry != '')
        {
            $storeClientQuery.=",addressCountry='".mysql_real_escape_string($addressCountry,$connection)."' ";
        }
        
        if($addressZip != '')
        {
            $storeClientQuery.=",addressZip='".mysql_real_escape_string($addressZip,$connection)."' ";
        }
        
        $storeClientQuery.="WHERE clientID='".mysql_real_escape_string($clientID,$connection)."'";

        if($debug) echo"storeClientQuery = $storeClientQuery<br>";
        mysql_query($storeClientQuery,$connection) or handleDatabaseError(''. mysql_error(),$storeClientQuery);
        
        /* Calculate reward points*/
        calculateRewardPoints($keyarray,$connection);
            
    }//end else ($isClientInNumber==0) i.e. does client exist

    /* DISPLAY REFERRAL POINTS - this is only for display purposes */
    /* the script above checks each new referred user and hence its unlikely to miss a referral point */
    if($pointsSystemEnabled)
    {
        //how many people has clientEmail referred
        $countPointsQuery="SELECT referID FROM CompletedReferrals WHERE senderEmail='$clientEmail'";
        $countPointsResult=mysql_query($countPointsQuery,$connection) or handleDatabaseError(''. mysql_error(),$countPointsQuery);
        $numberOfReferrals=mysql_num_rows($countPointsResult);
        $numberOfPoints=$numberOfReferrals%$referralPointsRequired;
        $pointsLeft=$referralPointsRequired-$numberOfPoints;
        if($inTesting){
            //echo "You have referred ".$numberOfPoints." users and you need $referralPointsRequired referrals to obtain a free $rewardsReward";
        }
    }
    else
    {//since the rewardSystemIsOn is false, then do not tell client about it
    }
}

function calculateReferralPoints($keyarray, $connection)
{
    $referralPointsRequired = REFERRAL_POINTS_REQUIRED;
    $referralReward = REFERRAL_REWARD;

    //Client information from PayPal
    $clientEmail    = $keyarray['payer_email'];
    $clientID       = $keyarray['payer_id'];
    $paymentAmount  = $keyarray['payment_gross'];	
    
    //since this Client did not exist before today, then (s)he could have been referred
    //can be optimized
    $checkReferQuery="SELECT senderEmail FROM Referrals WHERE referredEmail='$clientEmail'";
    $checkReferResult=mysql_query($checkReferQuery,$connection) or handleDatabaseError(''. mysql_error(),$checkReferQuery);
    $numberOfCheckRefer=mysql_num_rows($checkReferResult);
    $checkReferData=mysql_fetch_array($checkReferResult);
    $senderEmail=$checkReferData['senderEmail'];
    
    if($numberOfCheckRefer>0)
    {
        //this user has been referred
        $insertCompletedReferralsQuery="INSERT INTO CompletedReferrals(referredEmail,senderEmail) VALUES('$clientEmail','$senderEmail')";
        $insertCompletedReferralsResult=mysql_query($insertCompletedReferralsQuery,$connection) or handleDatabaseError(''. mysql_error(),$insertCompletedReferralsQuery);
        
        //how many people has senderEmail referred
        $countPointsQuery="SELECT referID FROM CompletedReferrals WHERE senderEmail='$senderEmail'";
        $countPointsResult=mysql_query($countPointsQuery,$connection) or handleDatabaseError(''. mysql_error(),$countPointsQuery);
        $numberOfPoints=mysql_num_rows($countPointsResult);
        
        if($debug) echo"Number of Points for $senderEmail = $numberOfPoints<br>";
        
        $modulus = $numberOfPoints%$referralPointsRequired;
        if($debug) echo"referralPoints Required: $referralPointsRequired <br>";
        if($debug) echo"modulus = $modulus<br>";
        
        if($modulus==0)
        {
            //user has completed another $referralPointsRequired points
            //inform the admin and issue user a free card
            $body="Hi Admin, check the CompletedReferrals Table and send user $senderEmail a free card.\n";
            $body.="Make sure the user has Completed Referrals that amount to a multiple of $referralPointsRequired";
            sendEmail('OAS Points System',$fromAddress,SUPPORT_EMAIL,POINTS_EMAIL,"Points for - $senderEmail",$body);
            //TODO: deduct the referral points
        }
        else
        {//user needs more points
        }
        
        //delete this user from Referrals
        //commented out for speed but again we are loosing space.
        //$deleteQuery="DELETE FROM Referrals WHERE referredEmail='$clientEmail'"
        //$deleteResult=mysql_query($deleteQuery,$connection) or handleDatabaseError(''. mysql_error(),$deleteQuery);
        
    }//end if($numberOfCheckRefer>0)
    else{//user has not been referred
    }
    //TODO: Also calculate this clients referral points and display them
    //DISPLAY REFERRAL POINTS
    //END DISPLAY----------------------
    //END REFERRAL POINT SYSTEM	
}


function calculateRewardPoints($keyarray,$connection)
{
    $pointsSystemEnabled = POINTS_SYSTEM_ENABLED;    
    $rewardPointsRequired = REWARD_POINTS_REQUIRED;
    $rewardsReward = REWARD;
    $inTesting = false;

    //client information from paypal
    $clientEmail    = $keyarray['payer_email'];
    $clientID       = $keyarray['payer_id'];
    $paymentAmount  = $keyarray['payment_gross'];
    
    if($pointsSystemEnabled)
    {
        //Purchase POINTS PROGRAMME
        //For every $300 give user a free $rewardsReward card
        //PointsTable(clientEmail,points)
        //else user is already a client
        //increment the value we already have in the PointsTable
        //LOCK TABLE
        $pointsQuery="SELECT points FROM PointsTable WHERE clientEmail='$clientEmail' ";
        $pointsResults=mysql_query($pointsQuery,$connection) or handleDatabaseError(''. mysql_error(),$pointsQuery);
        $pointsData=mysql_fetch_array($pointsResults);
        $pointsValue=$pointsData['points'];
        $pointsValue+=$paymentAmount;
        $modulus=$pointsValue%$rewardPointsRequired;
        
        if($inTesting)
        {
            //echo"You have ".$modulus." Reward Points and you require ".$rewardPointsRequired." Points to obtain a Free ".$rewardsReward." <br>";
        }
        
        // originally we used $modulus==0
        if($pointsValue >= $rewardPointsRequired)
        {
            //inform the admin and issue user a free card
            $body="Hi Admin, check the PointsTable Table and send user $clientEmail a free card.\n";
            $body.="Make sure the user has enought points";
            sendEmail('OAS Points System',$fromAddress,SUPPORT_EMAIL,POINTS_EMAIL,"Points for - $clientEmail",$body);
            
            //deduct the refunded 300 points
            $pointsValue=$pointsValue-$rewardPointsRequired;
        }
        else{
            //do nothing
        }
        
        $pointsQuery="UPDATE PointsTable SET points=$pointsValue WHERE clientEmail='$clientEmail'";
        $pointsResults=mysql_query($pointsQuery,$connection) or handleDatabaseError(''. mysql_error(),$pointsQuery);
    }
    else{//points system is not enabled
    }
}

?>
