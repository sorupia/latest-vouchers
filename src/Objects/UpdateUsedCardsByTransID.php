<?
//20110311: Function to update the UsedCards table
function updateUsedCardsByTransID($inputArray)
{
    $transid         = $inputArray['transid'];
    $receiverEmail   = $inputArray['receiverEmailAddField1'];
    $receiverPhone   = $inputArray['receiverPhoneField'];
    $smsGWID         = $inputArray['smsGWID'];
    $cardPIN         = $inputArray['cardPIN'];
    $networkName     = $inputArray['networkName'];
    $valueInUGSh     = $inputArray['valueInUGSh'];
    $ourPriceInUSD   = $inputArray['ourPriceInUSD'];
    $dateWePurchased = $inputArray['dateWePurchased'];
    $dateWeSoldIt    = $inputArray['dateWeSoldIt'];
    $itemID          = $inputArray['itemID'];
    $clientEmail     = $inputArray['clientEmail'];
    $loadedBy        = $inputArray['loadedBy'];
    $loadersIP       = $inputArray['loadersIP'];
    $serialNumber    = $inputArray['serialNumber'];
    $paymentFee      = $inputArray['paymentFee'];
    
    $connection = $inputArray['connection'];

    $inputArray['debug_output'].= "updateUsedCardsByTransID function\n";
    
    $fieldToModify = false;
    $query = '';

    //if the dispatching means is set to card option then complete the UsedCards table
    $updatecards_query = "UPDATE UsedCards ";
    $updatecards_query.= "SET ";

    if($receiverEmail!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "receiverEmail='$receiverEmail' ";
        $fieldToModify = true;
    }

    if($receiverPhone!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "receiverPhone='$receiverPhone' ";
        $fieldToModify = true;
    }

    if($smsGWID!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "smsGWID='$smsGWID' ";
        $fieldToModify = true;
    }
    
    if($cardPIN!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "cardPIN='$cardPIN' ";
        $fieldToModify = true;
    }

    if($networkName!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "networkName='$networkName' ";
        $fieldToModify = true;
    }

    if($valueInUGSh!=0)
    {
        if($fieldToModify) $query.= ",";
        $query.= "valueInUGSh=$valueInUGSh ";
        $fieldToModify = true;
    }

    if($ourPriceInUSD!=0)
    {
        if($fieldToModify) $query.= ",";
        $query.= "ourPriceInUSD=$ourPriceInUSD ";
        $fieldToModify = true;
    }

    if($dateWePurchased!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "dateWePurchased='$dateWePurchased' ";
        $fieldToModify = true;
    }

    if($dateWeSoldIt!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "dateWeSoldIt='$dateWeSoldIt' ";
        $fieldToModify = true;
    }

    if($itemID!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "itemID='$itemID' ";
        $fieldToModify = true;
    }

    if($clientEmail!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "clientEmail='$clientEmail' ";
        $fieldToModify = true;
    }

    if($loadedBy!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "loadedBy='$loadedBy' ";
        $fieldToModify = true;
    }

    if($loadersIP!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "loadersIP='$loadersIP' ";
        $fieldToModify = true;
    }

    if($serialNumber!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "serialNumber='$serialNumber' ";
        $fieldToModify = true;
    }

    if($sessionUser!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "sessionUser='$sessionUser' ";
        $fieldToModify = true;
    }

    if($paymentFee!=0)
    {
        if($fieldToModify) $query.= ",";
        $query.= "paymentFee=$paymentFee ";
        $fieldToModify = true;
    }

    if($query !='')
    {
        $updatecards_query.= $query;
        $updatecards_query.= "WHERE transactionID='$transid' ";
        $updatecardsresult = mysql_query($updatecards_query,$connection) or handleDatabaseError(''. mysql_error(),$updatecards_query);
        $inputArray['debug_output'].= "UsedCards update query: $updatecards_query\n";
    }
    else
    {
        $inputArray['debug_output'].= "UsedCards query empty: $updatecards_query\n";
    }
    
    return $inputArray;
}

?>