<?

//20110307: Function to complete outgoing queue receiver information.
function completeOutQueueInfo($inputArray)
{
    $receiverEmail     = trim($inputArray['receiverEmailAddField1']);
    $receiverPhone     = trim($inputArray['receiverPhoneField']);
    $receiverFirstName = trim($inputArray['receiverFNameField1']);
    $transid           = trim($inputArray['transid']);

    $autoDispatch = $inputArray['autoDispatch'];
    $connection   = $inputArray['connection'];
    $debug        = $inputArray['debug'];

    $numberOfCardsToUpdate = 1;

    $inputArray['debug_output'].= "completeOutQueueInfo function \n";
    $inputArray['debug_output'].= "Transaction ID: $transid \n";

    if($autoDispatch)
    {
        $dispatchStatus = 'autoready';
    }
    else
    {
        $dispatchStatus = 'pinready';
    }

    $updatecards_query = "UPDATE OutgoingQueue ";
    $updatecards_query.= "SET ";
    $updatecards_query.= "dispatchStatus='$dispatchStatus', ";
    $updatecards_query.= "receiverEmail='$receiverEmail', ";
    $updatecards_query.= "receiverFirstName='$receiverFirstName', ";
    $updatecards_query.= "receiverPhone='$receiverPhone' ";
    $updatecards_query.= "WHERE transactionID='$transid' ";
    $updatecardsresult = mysql_query($updatecards_query,$connection) or handleDatabaseError(''. mysql_error(),$updatecards_query);

    $inputArray['numberOfCardsToUpdate'] = $numberOfCardsToUpdate;
    $inputArray['debug_output'].= "$dispatchStatus transaction \n";    
    $inputArray['debug_output'].= "update queue query: $updatecards_query \n";
    $inputArray['dispatchStatus'] = $dispatchStatus;

    return $inputArray;
}
?>