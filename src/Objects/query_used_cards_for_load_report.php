<?
/*******************************************************************************
**  FILE: query_used_cards_for_load_report.php
**
**  FUNCTION: query_used_cards_for_load_report
**
**  PURPOSE: Query the Used Cards table given a number of inputs to be used 
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.10.24
*********************************************************************************/

	function query_used_cards_for_load_report($input_array)
	{
        $start_date          = $input_array['start_date'];
        $end_date            = $input_array['end_date'];
		$connection          = $input_array['connection'];
        $item_id             = $input_array['itemID'];

		$query = "SELECT ";
        $query.= "dateWePurchased,itemID,cardPIN,dateWeSoldIt,";
		$query.= "transactionID,clientEmail ";
		$query.= "FROM UsedCards ";
		$query.= "WHERE dateWePurchased > '$start_date' AND dateWePurchased < '$end_date' ";

        if($item_id != "")
        {
            $query.= "AND itemID = '$item_id' ";
        }

		$query.= "ORDER BY dateWePurchased DESC";

        $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
		return $result;
	}
?>