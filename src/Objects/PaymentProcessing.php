<?
/*******************************************************************************
**   FILE: blockSMSUser.php
**
**   FUNCTION: handleBlackListUserState, handleNewClientState, handleIncompletePaymentState
**
**   PURPOSE: Several functions to handle the client state in the return page
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: A long time ago
**   Several functions to handle the different client states in the return page
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs-AVMM, Kampala) DATE: 2015.12.01
**  Fixed bug that distorts debug comments
**
*********************************************************************************/

    require_once($_SERVER['DOCUMENT_ROOT'].'/src/Objects/IncompletePaymentsQueue.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/src/Objects/NewClientsQueueTable.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/src/Objects/InsertBlockedClient.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/src/SecurityFunctions.php');

    /* called by paypalreturnpage via verifierpage */
    function handleBlackListUserState($countryAbbreviation, $input_array, $ipLocationArray, $connection)
    {
        $debug = false;
        
        /* email support information */
        /* obtain PayPal Variables */
        $payment_status     = $input_array['payment_status'];
        $our_paypal_email   = $input_array['receiver_email'];//our email where the payment goes
        $transID            = $input_array['txn_id'];
        $payment_amount     = $input_array['mc_gross'];//payment_amount does not seem to work
        $payment_currency   = $input_array['mc_currency'];
        
        $first_name         = $input_array['first_name'];
        $last_name          = $input_array['last_name'];
        $item_name          = $input_array['item_name'];
        $client_email       = $input_array['payer_email'];
        $residence_country  = $input_array['residence_country'];
        $client_phone       = $input_array['contact_phone'];
        $itemIDNumber       = $input_array['item_number'];
        $client_id          = $input_array['payer_id'];
        $address_street     = $input_array['address_street'];
        $address_city       = $input_array['address_city'];
        $address_state      = $input_array['address_state'];
        $address_country    = $input_array['residence_country'];
        $address_zip        = $input_array['address_zip'];
        $receiver_phone     = $input_array['order_data']['receiver_phone'];
        $receiver_email     = $input_array['order_data']['receiver_email'];
        $receiver_first_name= $input_array['order_data']['receiver_first_name'];

        $ip_country     = $ipLocationArray['ip_country'];
        $ip_state       = $ipLocationArray['ip_state'];
        $ip_city        = $ipLocationArray['ip_city'];

        $comments = "FOUND BLOCKED: $client_email - $first_name $last_name - $itemIDNumber :BLOCKED-NO PIN ";
        $subject = $comments;
        $status = "BLOCKED";

        $email_msg = "A customer was found during the payment process with BLOCKED status. ";
        $email_msg.= "The system has therefore added the user to the Queues -> blocked purchases. ";
        $email_msg.= "Please review their purchase and either verify or refund purchase.\n";
        $email_msg.= "Payment Status: ".$payment_status."\n";
        $email_msg.= "Our PayPal Email: ".$our_paypal_email."\n";
        $email_msg.= "Transaction ID: ".$transID."\n";
        $email_msg.= "Payment Amount: ".$payment_amount."\n";
        $email_msg.= "Payment Currency: ".$payment_currency."\n";
        $email_msg.= "First Name: ".$first_name."\n";
        $email_msg.= "Last Name: ".$last_name."\n";
        $email_msg.= "Item Name: ".$item_name."\n";
        $email_msg.= "Client Email: ".$client_email."\n";
        $email_msg.= "Item ID: ".$itemIDNumber."\n";        
        $email_msg.= "Client ID: ".$client_id."\n\n";
        $email_msg.= "Address Street: ".$address_street."\n\n";
        $email_msg.= "Address City: ".$address_city." | ";
        $email_msg.= "IP: ".$ip_city."\n\n";
        $email_msg.= "Address State: ".$address_state." | ";
        $email_msg.= "IP: ".$ip_state."\n\n";
        $email_msg.= "Address Country: ".$address_country." | ";
        $email_msg.= "IP: ".$ip_country."\n\n";
        $email_msg.= "Address Zip/Postal Code: ".$address_zip."\n\n";
        $email_msg.= $input_array['ip_reused_info']."\n";

        $email_msg.= $input_array['phone_reused_info']."\n";

        $email_msg.= "Receiver Phone: ".$receiver_phone."\n";
        $email_msg.= $input_array['reused_receiver_phone_info']."";
        $email_msg.= "Receiver First Name: ".$receiver_first_name."\n";
        $email_msg.= "Receiver Email: ".$receiver_email."\n\n";

        $email_msg.= "Status: BLOCKED\n";
        $email_msg.= "Comments: ".$comments."\n\n";

        $client_redirect_link = INHERIT_SITE."adm2/index.php?redirect=out_queue_new.php";
        $email_msg.="To verify this purchase, go here: $client_redirect_link\n\n";

        $receiver_redirect_link = INHERIT_SITE."adm2/index.php?redirect=receivers_search.php?keywords=".substr($receiver_phone,1)."&searchType=all";
        $email_msg.="To modify the status of $receiver_phone, go here: $receiver_redirect_link\n\n";

        /* email client */
        $client_subject = "Order awaiting approval";
        $client_msg = "Dear $first_name,\n";
        $client_msg.= "We have received your order#: $transID to $receiverPhone and it is ";
        $client_msg.= "currently awaiting approval by our support TEAM.\n\n";
        $client_msg.= DOMAIN_NAME." Support Team\n";
        $client_msg.= DOMAIN_SLOGAN."\n";
        $client_msg.= SERVICE_PROVIDER.",".SERVICE_PROVIDER_ADDRESS.",".SUPPORT_PHONE."\n";
        $client_msg.= "\n\n";
        $client_msg.= "III: $ip_country~$ip_state\n";
        $client_msg.= "PPP: $address_country~$address_state\n";
        sendEmail(SUPPORT_NAME,FROM_EMAIL,$client_email,$bcc,$client_subject,$client_msg);
        
        //log ip address over here to Failed IPs.
        $logQuery="INSERT INTO FailedIPs(dateOfAccess,ipAddress,notes) VALUES('$timeOfAccess','$clientIP','BLOCKED')";
        $logResult=mysql_query($logQuery,$connection) or handleDatabaseError(''. mysql_error(),$logQuery);

        $resultArray = insertBlockedClient($countryAbbreviation,$input_array,$ipLocationArray);

        if($resultArray['status'] == true)
        {
            if($debug)
            {
                sendEmail("Sendairtime Web",FROM_EMAIL,TECH_ADMIN,$bcc,$subject,$email_msg);
            }
            else
            {
                sendEmail("Sendairtime Web",CARD_ADMIN,SUPPORT_EMAIL,$bcc,$subject,$email_msg);
            }
        }
        else
        {
            echo $resultArray['message'];
        }

        return $resultArray;
    }

    /* called by paypalreturnpage via verifierpage */
    function handleNewClientState($countryAbbreviation, $input_array, $ipLocationArray, $connection)
    {
        $debug = false;
        
        /* obtain PayPal Variables */
        $payment_status     = $input_array['payment_status'];
        $our_paypal_email   = $input_array['receiver_email'];//our email where the payment goes
        $transID            = $input_array['txn_id'];
        $payment_amount     = $input_array['mc_gross'];//payment_amount does not seem to work
        $payment_currency   = $input_array['mc_currency'];
        
        $first_name         = $input_array['first_name'];
        $last_name          = $input_array['last_name'];
        $item_name          = $input_array['item_name'];
        $client_email       = $input_array['payer_email'];
        $residence_country  = $input_array['residence_country'];
        $client_phone       = $input_array['contact_phone'];
        $itemIDNumber       = $input_array['item_number'];
        $client_id          = $input_array['payer_id'];
        $address_street     = $input_array['address_street'];
        $address_city       = $input_array['address_city'];
        $address_state      = $input_array['address_state'];
        $address_country    = $input_array['residence_country'];
        $address_zip        = $input_array['address_zip'];
        $receiver_phone     = $input_array['order_data']['receiver_phone'];
        $receiver_email     = $input_array['order_data']['receiver_email'];
        $receiver_first_name= $input_array['order_data']['receiver_first_name'];
        
        $ip_country     = $ipLocationArray['ip_country'];
        $ip_state       = $ipLocationArray['ip_state'];
        $ip_city        = $ipLocationArray['ip_city'];
        $ip_address     = $ipLocationArray['ip_address'];

        $status="BLOCKED";

        /* email support information */
        $subject = "NOT FOUND: $client_email - $first_name $last_name - $itemIDNumber :BLOCKED-NO PIN ";
        
        $email_msg ="A new customer has made a purchase. The system has therefore added them to the new ";
        $email_msg.="client list and automatically set their status to UNVERIFIED. ";
        $email_msg.="The details of the new client are as follows: \n\n";
        $email_msg.="Payment Status: $payment_status \n\n";
        $email_msg.="Our PayPal Email: $our_paypal_email \n\n";
        $email_msg.="Transaction ID:  $transID \n\n";
        $email_msg.="PaymentAmount:  $payment_amount \n\n";
        $email_msg.="Payment Currency: $payment_currency \n\n";
        $email_msg.="First Name: $first_name \n\n";
        $email_msg.="Last Name: $last_name \n\n";
        $email_msg.="Item Name: $item_name \n\n";
        $email_msg.="Client Email: $client_email \n\n";
        $email_msg.="Item ID Number: $itemIDNumber \n\n";
        $email_msg.="Client ID: $client_id \n\n";

        $email_msg.="Street Address: $address_street \n\n";

        $email_msg.="City Address: $address_city | ";
        $email_msg.="IP: $ip_city \n\n";

        $email_msg.="State Address: $address_state | ";
        $email_msg.="IP: $ip_state \n\n";

        $email_msg.="Country: $address_country | ";
        $email_msg.="IP: $ip_country \n\n";

        $email_msg.="Zip: $address_zip\n\n";
        $email_msg.=$input_array['ip_reused_info']."\n";

        $email_msg.=$input_array['phone_reused_info']."\n";

        $email_msg.="Receiver Phone: ".$receiver_phone."\n";
        $email_msg.=$input_array['reused_receiver_phone_info']."";
        $email_msg.="Receiver First Name: ".$receiver_first_name."\n";
        $email_msg.="Receiver Email: ".$receiver_email."\n";

        $email_msg.="Client Status: $status\n\n";
        $email_msg.="Comments: $subject\n\n";
        $email_msg.="To verify this new client go to the new admin section under 'Queues' tab.\n\n";

        $client_redirect_link = INHERIT_SITE."adm2/index.php?redirect=out_queue_new.php";
        $email_msg.="To verify this purchase, go here: $client_redirect_link\n\n";

        $receiver_redirect_link = INHERIT_SITE."adm2/index.php?redirect=receivers_search.php?keywords=".substr($receiver_phone,1)."&searchType=all";
        $email_msg.="To modify the status of $receiver_phone, go here: $receiver_redirect_link\n\n";

        $input_array['comments'].= $subject;

        /* email client */
        $client_subject = DOMAIN_NAME." #: $transID awaiting approval";
        $client_msg = "Dear $first_name,\n";
        $client_msg.= "Thank you for your first order#: $transID. ";
        $client_msg.= "The item $itemIDNumber to $receiver_phone is currently awaiting approval by our support staff. ";
        $client_msg.= "If necessary, one of our staff will call you to verify your purchase. ";
        $client_msg.= "Once approved your future purchases will be instant.\n\n";
        $client_msg.= DOMAIN_NAME." Support Team\n";
        $client_msg.= DOMAIN_SLOGAN."\n";
        $client_msg.= SERVICE_PROVIDER.",".SERVICE_PROVIDER_ADDRESS.",".SUPPORT_PHONE."\n";
        $client_msg.= "\n\n";
        $client_msg.= "III: $ip_country~$ip_state\n";
        $client_msg.= "PPP: $address_country~$address_state\n";
        sendEmail(SUPPORT_NAME,FROM_EMAIL,$client_email,$bcc,$client_subject,$client_msg);

        /* insert into NewClientQueue */
        $resultArray = insertNewClientQ( $countryAbbreviation,$input_array,$ipLocationArray);

        notifyOnNewClientPurchase($input_array);

        if($resultArray['status'] == true)
        {
            if($debug) { 
                sendEmail("Sendairtime Web",FROM_EMAIL,TECH_ADMIN,$bcc,$subject,$email_msg);
            }
            else
            {
                sendEmail("Sendairtime Web",CARD_ADMIN,SUPPORT_EMAIL,$bcc,$subject,$email_msg);     
            }
        }
        else
        {
            echo $resultArray['message'];
        }

        return $resultArray;
    }

    /* called by paypalreturnpage via verifierpage */
    function handleIncompletePaymentState($countryAbbreviation, $input_array, $ipLocationArray, $connection)
    {

        /* Obtain client IP location */
        $payment_status     = $input_array['payment_status'];
        $our_paypal_email   = $input_array['receiver_email'];//our email where the payment goes
        $transID            = $input_array['txn_id'];
        $payment_amount     = $input_array['mc_gross'];//payment_amount does not seem to work
        $payment_currency   = $input_array['mc_currency'];

        $first_name         = $input_array['first_name'];
        $last_name          = $input_array['last_name'];
        $item_name          = $input_array['item_name'];
        $client_email       = $input_array['payer_email'];
        $residence_country  = $input_array['residence_country'];
        $client_phone       = $input_array['contact_phone'];
        $itemIDNumber       = $input_array['item_number'];
        $client_id          = $input_array['payer_id'];
        $address_street     = $input_array['address_street'];
        $address_city       = $input_array['address_city'];
        $address_state      = $input_array['address_state'];
        $address_country    = $input_array['residence_country'];
        $address_zip        = $input_array['address_zip'];
        $receiverEmail      = $input_array['order_data']['receiver_email'];
        $receiverFirstName  = $input_array['order_data']['receiver_first_name'];
        $receiverPhone      = $input_array['order_data']['receiver_phone'];

        $ip_country     = $ipLocationArray['ip_country'];
        $ip_state       = $ipLocationArray['ip_state'];
        $ip_city        = $ipLocationArray['ip_city'];
        $ip_address     = $ipLocationArray['ip_address'];

        /* insert into IncompletePaymentQueue info */
        insertIntoIncompletePaymentsQ($countryAbbreviation,$input_array);

        /* email support recipient mobile */
        $subject = "INCOMPLETE: $client_email attempted to send to $receiverPhone";
        $email_msg = "The following Incomplete Payment is awaiting approval \n";
        $email_msg.= "Client Email: ".$client_email."\n";
        $email_msg.= "Receiver Name: ".$receiverFirstName."\n";
        $email_msg.= "Receiver Phone: ".$receiverPhone."\n";
        $email_msg.= "Receiver Email: ".$receiverEmail."\n";
        $email_msg.= "ip_country: ".$ip_country."\n";
        $email_msg.= "ip_state: ".$ip_state."\n";
        $email_msg.= "ip_city: ".$ip_city."\n";
        $email_msg.= "ip_address: ".$ip_address."\n";

        if($debug) 
        {
            sendEmail("Sendairtime Web",FROM_EMAIL,TECH_ADMIN,$bcc,$subject,$email_msg);
        }
        else
        {
            sendEmail("Sendairtime Web",CARD_ADMIN,SUPPORT_EMAIL,$bcc,$subject,$email_msg);
        }

        /* email client */
        $client_subject = "Order awaiting approval";
        $client_msg = "Dear $first_name,\n";
        $client_msg.= "We have received your order#: $transID to $receiverPhone and it is ";
        $client_msg.= "currently awaiting approval by our support TEAM.\n\n";
        $client_msg.= $_SERVER['SERVER_NAME']."\n";
        sendEmail("Sendairtime Web",FROM_EMAIL,$client_email,$bcc,$client_subject,$client_msg);

        include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayManualProcInfo.php";
    }
?>