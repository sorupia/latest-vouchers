<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/GeneralFunctions.php');
	
	function queryByLastLogin($countryAbbreviation)
	{
		$connection = connect2SMSDB($countryAbbreviation);
		
			$usersQuery = "SELECT lastLogin as 'Last Login',emailAddress as 'Email',";
			$usersQuery.= "firstName as 'First Name',lastName as 'Last Name',";
			$usersQuery.= "cellphone as 'Phone',subscriberStatus as 'SMS Status',airtimeStatus as 'Airtime Status',";
			$usersQuery.= "creditsLeft as 'SMS Credits',airtimeCreditsLeft as 'Airtime Credits',";
			$usersQuery.= "purchasedCredits as 'Purchased SMS',registrationDate as 'Reg. Date',";
			$usersQuery.= "addressCity as 'City',addressCountry as 'Country' ";									
			$usersQuery.= "FROM SMSUsers ";
			$usersQuery.= "WHERE airtimeStatus = 'VERIFIED' OR subscriberStatus = 'VERIFIED' ";
			$usersQuery.= "ORDER BY lastLogin DESC";
			$usersResult= mysql_query($usersQuery) or handleDatabaseError(''.mysql_error(),$usersQuery);
	
		disconnectDB($connection);
		return $usersResult;	
	}
	
	function queryByRegistrationDate($countryAbbreviation)
	{
		$connection = connect2SMSDB($countryAbbreviation);
		
			$usersQuery = "SELECT registrationDate as 'Reg. Date',lastLogin as 'Last Login',emailAddress as 'Email',";
			$usersQuery.= "firstName as 'First Name',lastName as 'Last Name',";
			$usersQuery.= "cellphone as 'Phone',subscriberStatus as 'SMS Status',airtimeStatus as 'Airtime Status',";
			$usersQuery.= "creditsLeft as 'SMS Credits',airtimeCreditsLeft as 'Airtime Credits',";
			$usersQuery.= "purchasedCredits as 'Purchased SMS',";
			$usersQuery.= "addressCity as 'City',addressCountry as 'Country' ";									
			$usersQuery.= "FROM SMSUsers ";
			$usersQuery.= "WHERE airtimeStatus = 'VERIFIED' OR subscriberStatus = 'VERIFIED' ";
			$usersQuery.= "ORDER BY registrationDate DESC";
			$usersResult= mysql_query($usersQuery) or handleDatabaseError(''.mysql_error(),$usersQuery);
	
		disconnectDB($connection);
		return $usersResult;	
	}
	
	function queryByClientEmail($clientEmail,$countryAbbreviation)
	{
		$connection = connect2SMSDB($countryAbbreviation);
		
			$usersQuery = "SELECT lastLogin as 'Last Login',emailAddress as 'Email',";
			$usersQuery.= "firstName as 'First Name',lastName as 'Last Name',";
			$usersQuery.= "cellphone as 'Phone',subscriberStatus as 'SMS Status',airtimeStatus as 'Airtime Status',";
			$usersQuery.= "creditsLeft as 'SMS Credits',airtimeCreditsLeft as 'Airtime Credits',";
			$usersQuery.= "purchasedCredits as 'Purchased SMS',registrationDate as 'Reg. Date',";
			$usersQuery.= "addressCity as 'City',addressCountry as 'Country' ";									
			$usersQuery.= "FROM SMSUsers ";
			$usersQuery.= "WHERE emailAddress = '$clientEmail'";
			$usersResult= mysql_query($usersQuery) or handleDatabaseError(''.mysql_error(),$usersQuery);
	
		disconnectDB($connection);
		return $usersResult;		
	}

	function queryByBlocked($countryAbbreviation)
	{
		$connection = connect2SMSDB($countryAbbreviation);
		
			$usersQuery = "SELECT registrationDate as 'Reg. Date',lastLogin as 'Last Login',emailAddress as 'Email',";
			$usersQuery.= "firstName as 'First Name',lastName as 'Last Name',";
			$usersQuery.= "cellphone as 'Phone',subscriberStatus as 'SMS Status',airtimeStatus as 'Airtime Status',";
			$usersQuery.= "creditsLeft as 'SMS Credits',airtimeCreditsLeft as 'Airtime Credits',";
			$usersQuery.= "purchasedCredits as 'Purchased SMS',";
			$usersQuery.= "addressCity as 'City',addressCountry as 'Country' ";									
			$usersQuery.= "FROM SMSUsers ";
			$usersQuery.= "WHERE airtimeStatus = 'BLOCKED' ";
			$usersQuery.= "ORDER BY registrationDate DESC";
			$usersResult= mysql_query($usersQuery) or handleDatabaseError(''.mysql_error(),$usersQuery);
	
		disconnectDB($connection);
		return $usersResult;		
	}
	
	function queryByUnVerified($countryAbbreviation)
	{
		$connection = connect2SMSDB($countryAbbreviation);
		
			$usersQuery = "SELECT registrationDate as 'Reg. Date',lastLogin as 'Last Login',emailAddress as 'Email',";
			$usersQuery.= "firstName as 'First Name',lastName as 'Last Name',";
			$usersQuery.= "cellphone as 'Phone',subscriberStatus as 'SMS Status',airtimeStatus as 'Airtime Status',";
			$usersQuery.= "creditsLeft as 'SMS Credits',airtimeCreditsLeft as 'Airtime Credits',";
			$usersQuery.= "purchasedCredits as 'Purchased SMS',";
			$usersQuery.= "addressCity as 'City',addressCountry as 'Country' ";									
			$usersQuery.= "FROM SMSUsers ";
			$usersQuery.= "WHERE airtimeStatus = 'UNVERIFIED' OR subscriberStatus = 'UNVERIFIED' ";
			$usersQuery.= "ORDER BY registrationDate DESC";
			$usersResult= mysql_query($usersQuery) or handleDatabaseError(''.mysql_error(),$usersQuery);
	
		disconnectDB($connection);
		return $usersResult;			
	}
		
	function modifyUserInfo($clientEmail,
							$firstName,
							$lastName,
							$phone,
							$subscriberStatus,
							$creditsLeft,
							$purchasedCredits,
							$airtimeStatus,
							$airtimeCreditsLeft,
							$countryAbbreviation)
	{
		$updateRequired = false;
		$more = false;
		$connection = connect2SMSDB($countryAbbreviation);
		
			$usersQuery = "UPDATE SMSUsers ";
			$usersQuery.= "SET ";
			
			if($firstName!=""){ 		$preparedQuery = "firstName = '$firstName' "; $updateRequired = true;}
			if($lastName!=""){ 		  	
				if($updateRequired){ $preparedQuery.= ", ";}
				$preparedQuery.= "lastName = '$lastName' "; $updateRequired = true;
			}
			
			if($phone!=""){			  	
				if($updateRequired){ $preparedQuery.= ", ";}
				$preparedQuery.= "cellphone = '$phone' "; $updateRequired = true;
			}
			
			if($subscriberStatus!=""){ 	
				if($updateRequired){ $preparedQuery.= ", ";}
				$preparedQuery.= "subscriberStatus = '$subscriberStatus' "; $updateRequired = true;
			}
			
			if($creditsLeft!=""){ 	  	
				if($updateRequired){ $preparedQuery.= ", ";}
				$preparedQuery.= "creditsLeft = '$creditsLeft' "; $updateRequired = true;
			}
			
			if($purchasedCredits!=""){ 	
				if($updateRequired){ $preparedQuery.= ", ";}
				$preparedQuery.= "purchasedCredits = '$purchasedCredits' "; $updateRequired = true;
			}
			
			if($airtimeStatus!=""){    	
				if($updateRequired){ $preparedQuery.= ", ";}
				$preparedQuery.= "airtimeStatus = '$airtimeStatus' "; $updateRequired = true;
			}
			
			if($airtimeCreditsLeft!=""){
				if($updateRequired){ $preparedQuery.= ", ";}
				$preparedQuery.= "airtimeCreditsLeft = '$airtimeCreditsLeft' "; $updateRequired = true;
			}
			
			if( $updateRequired )
			{
				$usersQuery.= $preparedQuery;
				$usersQuery.= "WHERE emailAddress = '$clientEmail'";
				$usersResult= mysql_query($usersQuery) or handleDatabaseError(''.mysql_error(),$usersQuery);
			}
	
		disconnectDB($connection);
		return $updateRequired;
	}
		
	function approveATUser($clientEmail,$countryAbbreviation)
	{
		modifyUserInfo( $clientEmail,
						"",
						"",
						"",
						"VERIFIED",
						"",
						"",
						"VERIFIED",
						"",
						$countryAbbreviation);
						
		$body="Hi, \n";
		$body.="The verification of your airtime account at ".INHERIT_SITE." is complete \n";
		$body.="Now you have access to send airtime anytime to ".COUNTRY_NAME.". \n";
		$body.="--Thanks, TrinityLabs Software Inc [".SUPPORT_EMAIL."]";
		$subject="Airtime account verification complete";

		//send email
		//sendEmail('','',$clientEmail,$SIGNUPEMAIL,$subject,$body);						
	}
	
	function approveSMSUser($clientEmail,$countryAbbreviation)
	{
		modifyUserInfo( $clientEmail,
						"",
						"",
						"",
						"VERIFIED",
						"",
						"",
						"",
						"",
						$countryAbbreviation);

		$body="Hi, \n";
		$body.="The verification of your SMS account at ".INHERIT_SITE." is complete \n";
		$body.="Now you have access to SMS anytime to ".COUNTRY_NAME.". \n";
		$body.="--Thanks, TrinityLabs Software Inc [".SUPPORT_EMAIL."]";
		$subject="SMS account verification complete";

		//send email
		//sendEmail('','',$clientEmail,$SIGNUPEMAIL,$subject,$body);		
	}

	function blockUser($clientEmail,$countryAbbreviation)
	{
		modifyUserInfo( $clientEmail,
						"",
						"",
						"",
						"BLOCKED",
						"",
						"",
						"BLOCKED",
						"",
						$countryAbbreviation);	
	}
		
	function deleteUser($clientEmail,$countryAbbreviation)
	{
		$connection = connect2SMSDB($countryAbbreviation);
		
			$usersQuery = "DELETE ";
			$usersQuery.= "FROM SMSUsers ";
			$usersQuery.= "WHERE emailAddress = '$clientEmail' ";
			$usersResult= mysql_query($usersQuery) or handleDatabaseError(''.mysql_error(),$usersQuery);
	
		disconnectDB($connection);	
	}
	
	function displayUserResults($queryResult)
	{
		// Print the Client data in a HTML table 
		echo "<table class=\"large-tbl\" width=\"90%\"  border=\"0\" >";
		echo"<tr>";
		
		$numOfColumns=mysql_num_fields($queryResult);
		
		for($i=0 ; $i<$numOfColumns ; $i++){
			$columnName=mysql_field_name($queryResult,$i);	
			echo"<td>$columnName</td>";
		}
		
		echo"</tr>";
		$j=0;
		while ($queryData = mysql_fetch_array($queryResult, MYSQL_ASSOC)) {
			$j++;
			if($j%2){
				print "<tr>";
			}
			else{
				print "<tr bgcolor=\"#F0F0F0\">";
			}
			foreach ($queryData as $col_value) {
				print "<td>$col_value</td>";
			}
			print "</tr>";
		}
		print "</table>";		
	}			
	
?>
