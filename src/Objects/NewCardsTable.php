<?
	require_once$_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
	
	function queryNewCardsByDateWePurchased($countryAbbreviation,
											$start_date,
											$end_date,
											$username)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$newCardsQuery = "SELECT dateWePurchased,cardPIN,itemID,ourPriceInUSD,loadedBy,loadersIP,cardStatus, serialNumber ";
			$newCardsQuery.= "FROM NewCards ";
			$newCardsQuery.= "WHERE dateWePurchased > '$start_date' AND dateWePurchased < '$end_date' ";
			$newCardsQuery.= "ORDER BY dateWePurchased DESC";
			$newCardsResult=mysql_query($newCardsQuery) or handleDatabaseError(''.mysql_error(),$newCardsQuery);
	
		disconnectDB($connection);
		
		/* log view by username*/
		logView($countryAbbreviation, $username);
		
		return $newCardsResult;	
	}

	function queryNewCardsByPIN($countryAbbreviation,
								$cardPIN,
								$username)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$newCardsQuery = "SELECT dateWePurchased,cardPIN,itemID,ourPriceInUSD,loadedBy,loadersIP,cardStatus,serialNumber ";
			$newCardsQuery.= "FROM NewCards ";
			$newCardsQuery.= "WHERE cardPIN = '$cardPIN' ";
			$newCardsQuery.= "ORDER BY dateWePurchased DESC";
			$newCardsResult=mysql_query($newCardsQuery) or handleDatabaseError(''.mysql_error(),$newCardsQuery);
	
		disconnectDB($connection);
		
		/* log view by username*/
		logView($countryAbbreviation, $username);
		
		return $newCardsResult;		
	}
	
	function queryNewCardsBySerial($countryAbbreviation,
								   $serial,
								   $username)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$newCardsQuery = "SELECT dateWePurchased,cardPIN,itemID,ourPriceInUSD,loadedBy,loadersIP,cardStatus,serialNumber ";
			$newCardsQuery.= "FROM NewCards ";
			$newCardsQuery.= "WHERE serialNumber = '$serial' ";
			$newCardsQuery.= "ORDER BY dateWePurchased DESC";
			$newCardsResult=mysql_query($newCardsQuery) or handleDatabaseError(''.mysql_error(),$newCardsQuery);
	
		disconnectDB($connection);
		
		/* log view by username*/
		logView($countryAbbreviation, $username);
		
		return $newCardsResult;		
	}

	function queryNewCardsByItemID($countryAbbreviation,
								   $itemID,
								   $username)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$newCardsQuery = "SELECT dateWePurchased,cardPIN,itemID,ourPriceInUSD,loadedBy,loadersIP,cardStatus,serialNumber ";
			$newCardsQuery.= "FROM NewCards ";
			$newCardsQuery.= "WHERE itemID = '$itemID' ";
			$newCardsQuery.= "ORDER BY dateWePurchased DESC";
			$newCardsResult=mysql_query($newCardsQuery) or handleDatabaseError(''.mysql_error(),$newCardsQuery);
	
		disconnectDB($connection);
		
		/* log view by username*/
		logView($countryAbbreviation, $username);
		
		return $newCardsResult;		
	}
	
	function modifyNewCardsInfo($countryAbbreviation,
								$oldCardPIN,
								$newCardPIN,
								$itemID,
								$serialNumber,
								$username)
	{
		$updateRequired = false;
		$more = false;
		$connection = connect2DB2($countryAbbreviation);
		
			$newCardsQuery = "UPDATE NewCards ";
			$newCardsQuery.= "SET ";
			
			if($newCardPIN!=""){ 		
				$preparedQuery = "cardPIN = '$newCardPIN' "; $updateRequired = true;
			}
			
			if($itemID!=""){ 		  	
				if($updateRequired){ $preparedQuery.= ", ";}
				$preparedQuery.= "itemID = '$itemID' "; $updateRequired = true;
			}
			
			if($serialNumber!=""){			  	
				if($updateRequired){ $preparedQuery.= ", ";}
				$preparedQuery.= "serialNumber = '$serialNumber' "; $updateRequired = true;
			}
			
			if( $updateRequired )
			{
				$newCardsQuery.= $preparedQuery;
				$newCardsQuery.= "WHERE cardPIN = '$oldCardPIN'";
				echo "mod Query: $newCardsQuery<br>";
				$newCardsResult= mysql_query($newCardsQuery) or handleDatabaseError(''.mysql_error(),$newCardsQuery);
			}
	
		disconnectDB($connection);
		
		/* log view by username*/
		logView($countryAbbreviation, $username);
		
		return $updateRequired;	
	}
	
	function deleteNewCard($countryAbbreviation,
						   $cardPIN,
						   $username)
	{
		if($cardPIN==''){
			echo"No card provided";
		}
		else{
			$connection = connect2DB2($countryAbbreviation);
				//first search through NewCards since we need some information on card being deleted
				$searchQuery="SELECT itemID, loadedBy FROM NewCards WHERE cardPIN='$cardPIN'";
				$searchResult=mysql_query($searchQuery) or handleDatabaseError(''. mysql_error(),$searchQuery);
				$num=0;
				$num=mysql_num_fields($searchResult);
				$searchedData=mysql_fetch_array($searchResult);
				$itemID=$searchedData['itemID'];
				$loadedBy=$searchedData['loadedBy'];
				
				//if there is a card that matches our number then delete it and log
				if($num!=0){
					$timeDeleted= date("Y-m-d H:i:s");
					
					$myIP=getIP();
									
					$myHostName=gethostbyaddr($myIP);
									
					//before deletion make sure that the user is logged
					$loggingQuery = "INSERT INTO DeleteLog(username, cardPINDeleted, itemID, timeDeleted, loadedBy, deleteIP) "; 
					$loggingQuery.= "VALUES('$username', '$cardPIN', '$itemID', '$timeDeleted', '$loadedBy','$myIP')";
					$loggingResult=mysql_query($loggingQuery) or handleDatabaseError(''. mysql_error(),$loggingQuery);
					
					$deleteQuery="DELETE FROM NewCards WHERE cardPIN='$cardPIN'";
					$deleteResult=mysql_query($deleteQuery) or handleDatabaseError(''. mysql_error(),$deleteQuery);
				}
				else{
					echo"No card Matching $cardPIN";
				}
			disconnectDB($connection);
		}
	}
?>
