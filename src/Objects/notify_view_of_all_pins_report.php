<?
    function notify_view_of_all_pins_report($input_array)
    {
        $time = date("Y-m-d H:i:s");

        $to = NOTIFY_VIEW_OF_REPORTS_EMAIL;
        $bcc = SUPER_TECH_ADMIN;
        $subject = "All PINs report viewed by ".$input_array['employee_username'];
        $body = "Viewed by: ".$input_array['employee_username']."\n";
        $body.= "Time: ".$time." ".TIME_ZONE."\n";
        $body.= "Date Range: ".$input_array['start_date']." - ".$input_array['end_date']."\n";
        $body.= "IP Address: ".$_SESSION['ip_address']."\n";
        sendEmail(FROM_NAME,SUPPORT_EMAIL,$to,$bcc,$subject,$body);
    }

    function notify_view_of_delivery_report($input_array)
    {
        $time = date("Y-m-d H:i:s");

        $to = NOTIFY_VIEW_OF_REPORTS_EMAIL;
        $bcc = SUPER_TECH_ADMIN;
        $subject = "Delivery report viewed by ".$input_array['employee_username'];
        $body = "Viewed by: ".$input_array['employee_username']."\n";
        $body.= "Time: ".$time." ".TIME_ZONE."\n";
        $body.= "Date Range: ".$input_array['start_date']." - ".$input_array['end_date']."\n";
        $body.= "IP Address: ".$_SESSION['ip_address']."\n";
        sendEmail(FROM_NAME,SUPPORT_EMAIL,$to,$bcc,$subject,$body);
    }

    function notify_view_of_new_pins($input_array)
    {
        $time = date("Y-m-d H:i:s");

        $to = NOTIFY_VIEW_OF_REPORTS_EMAIL;
        $bcc = SUPER_TECH_ADMIN;
        $subject = "New PINs viewed by ".$input_array['employee_username'];
        $body = "Viewed by: ".$input_array['employee_username']."\n";
        $body.= "Time: ".$time." ".TIME_ZONE."\n";
        $body.= "Details: ".$input_array['details']."\n";
        $body.= "IP Address: ".$_SESSION['ip_address']."\n";
        sendEmail(FROM_NAME,SUPPORT_EMAIL,$to,$bcc,$subject,$body);
    }

    function notify_view_of_load_report($input_array)
    {
        $time = date("Y-m-d H:i:s");

        $to = NOTIFY_VIEW_OF_REPORTS_EMAIL;
        $bcc = SUPER_TECH_ADMIN;
        $subject = "Load report viewed by ".$input_array['employee_username'];
        $body = "Viewed by: ".$input_array['employee_username']."\n";
        $body = "Item ID: ".$input_array['itemID']."\n";
        $body.= "Time: ".$time." ".TIME_ZONE."\n";
        $body.= "Date Range: ".$input_array['start_date']." - ".$input_array['end_date']."\n";
        $body.= "IP Address: ".$_SESSION['ip_address']."\n";
        sendEmail(FROM_NAME,SUPPORT_EMAIL,$to,$bcc,$subject,$body);
    }

?>