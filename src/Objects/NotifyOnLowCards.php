<?
//201012171145: Functions to notify site admin

function notifyOnLowCards($inputArray, $connection)
{
    $itemidnumber = $inputArray['itemIDNumber'];
    $lowCardThreshold = LOW_CARD_THRESHOLD;
    
    global $notifyPhones;
    
    if(NOTIFY_ON_LOW_CARDS)
    {
        $lowNotificationQuery = "SELECT itemID ";
        $lowNotificationQuery.= "FROM NewCards ";
        $lowNotificationQuery.= "WHERE itemID = '$itemidnumber'";
        $result = mysql_query($lowNotificationQuery, $connection) or handleDatabaseError(''. mysql_error(),$lowNotificationQuery);
        $numberOfCardsLeft = mysql_num_rows($result);			


        //if equal to threshold then notify
        if($numberOfCardsLeft == $lowCardThreshold || $numberOfCardsLeft == 0)
        {
            $lowMessage = substr(DOMAIN_PREFIX,0,-1)."\n"."Low Card Notification: There are $numberOfCardsLeft $itemidnumber cards left";
            
            for($i=0; $i<count($notifyPhones); $i++)
            {
                $notifyPhone = $notifyPhones[$i];
                sendTextUsingClick2($notifyPhone,$lowMessage);
            }
        }
    }
}
?>