<?
	require_once$_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
	
	function queryDeletedForAllPins($countryAbbreviation,
							        $start_date,
							        $end_date)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$deletedCardsQuery = "SELECT dateWePurchased, itemID, ";
			$deletedCardsQuery.= "cardPINDeleted, timeDeleted, reason ";
			$deletedCardsQuery.= "FROM DeleteLog ";
			$deletedCardsQuery.= "WHERE timeDeleted > '$start_date' AND timeDeleted < '$end_date' ";
			$deletedCardsQuery.= "ORDER BY timeDeleted, cardPINDeleted DESC";
			$deletedCardsResult=mysql_query($deletedCardsQuery) or handleDatabaseError(''.mysql_error(),$deletedCardsQuery);
	
		disconnectDB($connection);
		return $deletedCardsResult;
	}
	
	/*
	* Purpose: log views performed on the new cards table 
	*/
	function logView($countryAbbreviation,
					 $username)
	{
		$connection = connect2DB2($countryAbbreviation);
			$timeViewed= date("Y-m-d H:i:s");
			$myIP=getIP();
			$viewLogQuery = "INSERT INTO ViewLog(viewDate,username,ipAddress) ";
			$viewLogQuery.= "VALUES('$timeViewed','$username','$myIP')";
			$viewLogResult=mysql_query($viewLogQuery) or handleDatabaseError(''. mysql_error(),$viewLogQuery);	
		disconnectDB($connection);
	}

	function queryNewCardsForAllPins($countryAbbreviation,
											$start_date,
											$end_date,
											$username)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$newCardsQuery = "SELECT dateWePurchased,itemID,cardPIN,cardStatus ";
			$newCardsQuery.= "FROM NewCards ";
			$newCardsQuery.= "WHERE dateWePurchased > '$start_date' AND dateWePurchased < '$end_date' ";
			$newCardsQuery.= "ORDER BY dateWePurchased DESC";
			$newCardsResult=mysql_query($newCardsQuery) or handleDatabaseError(''.mysql_error(),$newCardsQuery);
	
		disconnectDB($connection);
		
		/* log view by username*/
		logView($countryAbbreviation, $username);
		
		return $newCardsResult;	
	}

	function queryUsedCardsForAllPins($countryAbbreviation,
							   		  $start_date,
							   		  $end_date)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$usedCardsQuery = "SELECT dateWePurchased,itemID,cardPIN,dateWeSoldIt,";
			$usedCardsQuery.= "transactionID,clientEmail ";
			$usedCardsQuery.= "FROM UsedCards ";
			$usedCardsQuery.= "WHERE dateWePurchased > '$start_date' AND dateWePurchased < '$end_date' ";
			$usedCardsQuery.= "ORDER BY dateWePurchased DESC";
			$usedCardsResult=mysql_query($usedCardsQuery) or handleDatabaseError(''.mysql_error(),$usedCardsQuery);
	
		disconnectDB($connection);
		return $usedCardsResult;
	}
	
	/* NOTE: flipped table with columns are rows and vice versa 
	   this was done to take advantage of the array_multisort function 
	   that will sort a multi dimension array
	*/
	function organizeUsedCardsResults($queryResult,$allCardsArray)
	{
		//dateWePurchased,itemID,cardPIN,dateWeSoldIt,transactionID,clientEmail 
		$row=0;
		$column=0;

		while ($queryData = mysql_fetch_array($queryResult, MYSQL_ASSOC)) {
			$row = 0;
			$allCardsArray [$row++][$column] = $queryData["dateWePurchased"];
			$allCardsArray [$row++][$column] = $queryData["itemID"];
			$allCardsArray [$row++][$column] = $queryData["cardPIN"];
			$allCardsArray [$row++][$column] = $queryData["dateWeSoldIt"];
			$allCardsArray [$row++][$column] = "SOLD";
			$allCardsArray [$row++][$column] = $queryData["transactionID"];
			$allCardsArray [$row++][$column] = $queryData["clientEmail"];
			$allCardsArray [$row++][$column]   = "N/A";
			$column++;
		}

		return $allCardsArray;
	}
	
	function organizeNewCardsResults($queryResult,$allCardsArray)
	{
		//dateWePurchased,itemID,cardPIN,Status Date,cardStatus,Transaction ID, clientEmail, reason
		//start from where we stopped in previous additions to the allCardsArray
		$column = count($allCardsArray[0]);

		while ($queryData = mysql_fetch_array($queryResult, MYSQL_ASSOC)) {
			$row = 0;
			$allCardsArray [$row++][$column] = $queryData["dateWePurchased"];
			$allCardsArray [$row++][$column] = $queryData["itemID"];
			$allCardsArray [$row++][$column] = $queryData["cardPIN"];
			$allCardsArray [$row++][$column] = $queryData["dateWePurchased"];
			$allCardsArray [$row++][$column] = $queryData["cardStatus"];
			$allCardsArray [$row++][$column] = "N/A";
			$allCardsArray [$row++][$column] = "N/A";
			$allCardsArray [$row++][$column] = "N/A";
			$column++;
		}

		return $allCardsArray;
	}
	
	function organizeDeletedCardsResults($queryResult,$allCardsArray)
	{
		//dateWePurchased,itemID,cardPIN,Status Date,cardStatus,Transaction ID,clientEmail,reason
		//start from where we stopped in previous additions to the allCardsArray
		$column = count($allCardsArray[0]);
	
		while ($queryData = mysql_fetch_array($queryResult, MYSQL_ASSOC)) {
			$row = 0;
			$allCardsArray [$row++][$column] = $queryData["dateWePurchased"];
			$allCardsArray [$row++][$column] = $queryData["itemID"];
			$allCardsArray [$row++][$column] = $queryData["cardPINDeleted"];
			$allCardsArray [$row++][$column] = $queryData["timeDeleted"];
			$allCardsArray [$row++][$column] = "DELETED";
			$allCardsArray [$row++][$column] = "N/A";
			$allCardsArray [$row++][$column] = "N/A";
			$allCardsArray [$row++][$column] = $queryData["reason"];
			$column++;			
		}
		return $allCardsArray;		
	}
	
	function displayAllCardsResults($allCardsArray)
	{
		echo "<table class=\"large-tbl\" width=\"90%\"  border=\"0\" >";
		echo"<tr>";
			echo"<td>dateWePurchased</td>";
			echo"<td>itemID</td>";
			echo"<td>cardPIN</td>";
			echo"<td>Status Date</td>";
			echo"<td>Card Status</td>";
			echo"<td>Transaction ID</td>";
			echo"<td>Client Email</td>";
			echo"<td>Reason</td>";
		echo"</tr>";

		for( $i=0 ; $i < count($allCardsArray[0]) ; $i++ )
		{
			if($i%2){
				print "<tr>";
			}
			else{
				print "<tr bgcolor=\"#F0F0F0\">";
			}
			
			for($j=0 ; $j<8 ; $j++){
				$array_element = $allCardsArray[$j][$i];
				echo"<td>$array_element</td>";
			}
			print "</tr>";
		}

		print "</table>";		
	}
    function displayAllCardsReport($allCardsArray)
    {
        echo'<thead>
			<tr>
			<th>Date We Purchased</th>
			<th>Item ID</th>
            <th>CardPIN</th>
            <th>Status Date</th>
			<th>Card Status</th>
            <th>Transaction ID</th>
			<th>Client Email</th>
			<th>Reason</th>
			</tr>
		</thead>';

        for( $i=0 ; $i < count($allCardsArray[0]) ; $i++ )
		{
            echo'<tbody><tr>';

			for($j=0 ; $j<8 ; $j++)
            {
				$array_element = $allCardsArray[$j][$i];
				echo"<td>$array_element</td>";
			}

			echo '</tr></tbody>';
		}
    }
?>