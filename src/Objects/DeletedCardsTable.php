<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/GeneralFunctions.php');
	
	function queryDeletedByPurchaseDate($countryAbbreviation,
							   $start_date,
							   $end_date)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$deletedCardsQuery = "SELECT dateWePurchased, timeDeleted, ";
			$deletedCardsQuery.= "cardPINDeleted, itemID, reason, ";
			$deletedCardsQuery.= "username, deleteIP ";
			$deletedCardsQuery.= "FROM DeleteLog ";
			$deletedCardsQuery.= "WHERE timeDeleted > '$start_date' AND timeDeleted < '$end_date' ";
			$deletedCardsQuery.= "ORDER BY timeDeleted, cardPINDeleted DESC";
			$deletedCardsResult=mysql_query($deletedCardsQuery) or handleDatabaseError(''.mysql_error(),$deletedCardsQuery);
	
		disconnectDB($connection);
		return $deletedCardsResult;
	}
	
?>