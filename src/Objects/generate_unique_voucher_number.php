<?

/*******************************************************************************
**   FILE: generate_unique_voucher_number.php
**
**   FUNCTION: generate_unique_voucher_number
**
**   PURPOSE: Generates unique numbers that we can use for our voucher system
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, 1579 Weston Road, York, ON)   DATE: 05.Sep.2015
**
**
*********************************************************************************/

function generate_unique_voucher_number($input_array)
{
    $input_array['serialNumber']     = "Not Applicable";

    //Generation of Unique Voucher Number
    //18 digits
    //2 digits from Unix Time stamp
    $unix_time = (string)time();
    $unix_time_part = substr($unix_time,-2);

    //12 digit Time stamp 150908172601
    $time_stamp_part = date("ymdHis");

    //4 random digits
    $random_part = (string)mt_rand(1000,9999);

    $unique_voucher = $unix_time_part.$time_stamp_part.$random_part;

    $input_array['cardPIN']        = $unique_voucher;
    $input_array["loadersIP"]      = $input_array["clientIP"];
    $input_array['loadedBy']       = "AUTO_GEN";
    $input_array['invoke_success'] = 1;

    return $input_array;
}

?>