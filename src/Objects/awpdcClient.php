<?

	require_once($_SERVER['DOCUMENT_ROOT'].'/src/GeneralFunctions.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/src/sendATSimple.php');

    function processUnblockAndSend($countryAbbreviation,
                                   $newClientArray,
                                   $userStatus,
                                   $username)
	{
		/* extract contents of newClientArray*/
        $comments         = $newClientArray['comments'];
		$transID          = $newClientArray['transID'];
		$status           = "UnBlocked";		
		
		$resultArray = array();
		$cardPIN = 0;

		/*add user to the SMSUsers table as verified*/
		$connection = connect2_awpc_db($countryAbbreviation);
            $updateUserQuery = "UPDATE cust_table ";
            $updateUserQuery.= "SET ";
			$updateUserQuery.= "paymentStatus = '', ";
			$updateUserQuery.= "ourPaypalEmail = '', ";
			$updateUserQuery.= "paymentAmount = '', ";
			$updateUserQuery.= "paymentCurrency = '', ";
			$updateUserQuery.= "itemname = '', ";
			$updateUserQuery.= "clientID = '', ";
			$updateUserQuery.= "addressStreet = '', ";
			$updateUserQuery.= "addressCity = '', ";
			$updateUserQuery.= "addressState = '', ";
			$updateUserQuery.= "addressCountry = '', ";
			$updateUserQuery.= "addressZip = '', ";
            $updateUserQuery.= "status = '$status', ";
            $updateUserQuery.= "comments = '$comments' ";
			$updateUserQuery.= "WHERE transID = '$transID' ";
            $updateUserResult=mysql_query($updateUserQuery) or handleDatabaseError(''.mysql_error(),$updateUserQuery);
		disconnectDB($connection);

        $resultArray['result'] = true;

		/*use API to send the airtime that was purchased*/
		/* it will also update the UsedCards and Sales tables*/
		if($userStatus == 'VERIFIED')
		{
			//print_r($newClientArray);
            $resultArray = sendATSimple($countryAbbreviation, $newClientArray);
			$status = $resultArray['status'];
			$cardPIN = $resultArray['card'];
		}
		else
		{
			//provide no airtime
		}

        return $resultArray;
	}
    
    function processUnblock($countryAbbreviation,
                            $newClientArray,
                            $userStatus,
                            $username)
	{
		/* extract contents of newClientArray*/
        $comments         = $newClientArray['comments'];
		$transID          = $newClientArray['transID'];
		$status           = $newClientArray['status'];
		
		$resultArray = array();
		$cardPIN = 0;

		/*add user to the SMSUsers table as verified*/
		$connection = connect2_awpc_db($countryAbbreviation);
            $updateUserQuery = "UPDATE cust_table ";
            $updateUserQuery.= "SET ";
			$updateUserQuery.= "paymentStatus = '', ";
			$updateUserQuery.= "ourPaypalEmail = '', ";
			$updateUserQuery.= "paymentAmount = '', ";
			$updateUserQuery.= "paymentCurrency = '', ";
			$updateUserQuery.= "itemname = '', ";
			$updateUserQuery.= "clientID = '', ";
			$updateUserQuery.= "addressStreet = '', ";
			$updateUserQuery.= "addressCity = '', ";
			$updateUserQuery.= "addressState = '', ";
			$updateUserQuery.= "addressCountry = '', ";
			$updateUserQuery.= "addressZip = '', ";
            $updateUserQuery.= "status = '$status', ";
            $updateUserQuery.= "comments = '$comments' ";
			$updateUserQuery.= "WHERE transID = '$transID' ";			
            $updateUserResult=mysql_query($updateUserQuery) or handleDatabaseError(''.mysql_error(),$updateUserQuery);
		disconnectDB($connection);

        $resultArray['status'] = $status;
        $resultArray['card'] = $cardPIN;
        $resultArray['result'] = TRUE;

        return $resultArray;
	}
	
?>