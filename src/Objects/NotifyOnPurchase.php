<?
//201012171145: Functions to notify site admin

function notifyOnPurchase($inputArray)
{
    $itemidnumber = $inputArray['itemIDNumber'];
    $firstName = $inputArray['first_name'];
    $lastName = $inputArray['last_name'];
    $receiverFirstName = $inputArray['order_data']['receiver_first_name'];
    $receiverPhone = $inputArray['order_data']['receiver_phone'];
    $clientEmail = $inputArray['payer_email'];
    $pin2Send = $inputArray['cardPIN'];

    if(PURCHASE_NOTIFY_SMS)
    {
        // Notification to Admin
        $notifyPhone = NOTIFY_PHONE;
        $notifyMessage = DOMAIN_NAME.":Old Cx: $firstName $lastName / Rcv: $receiverFirstName / Rcv Tel: $receiverPhone / Item: $itemidnumber / pin: $pin2Send";
        sendTextUsingClick2($notifyPhone,$notifyMessage);
    }

    if(PURCHASE_NOTIFY_EMAIL)
    {
        //send an email to the card administrator informing him about the exact details of the purchase.
        $networkName=getNetworkName($itemidnumber);
        $subject="$networkName:$itemidnumber:Destination Number:$receiverPhone:PIN:$pin2Send:Sender:$firstName:Receiver:$receiverFirstName";
        $body="";
        sendEmail(FROM_NAME,SUPPORT_EMAIL,CARD_ADMIN,$bcc,$subject,$body);
    }
}
?>