<?
//20110313: Function to check no. of occurrences for a particular transaction ID
function checkDuplicateTransID($inputArray)
{
    $transID = $inputArray['txn_id'];
    $connection = $inputArray['connection'];

    $sales_table   = isset($inputArray['sales_table']) ? $inputArray['sales_table'] : "Sales";
    $sales_table   = ($sales_table =="") ? "Sales" : $sales_table;

    $query = "SELECT * ";
    $query.= "FROM $sales_table WHERE ";
    $query.= "transactionID = '$transID' ";
    $result = mysql_query($query, $connection) or handleDatabaseError(''. mysql_error(),$query);
    $num_rows = mysql_num_rows($result);

    $inputArray['transaction_id_exists'] = $num_rows > 0 ? 1 : 0;
    
    return $inputArray;
}
?>