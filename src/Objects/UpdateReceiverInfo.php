<?
/*******************************************************************************
**   FILE: updateReceiverInfo.php
**
**   FUNCTION: updateReceiverInfo
**
**   PURPOSE: Add or update the recipient information in the Receivers table
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 15.Dec.2010
**   1652hrs: Created  function to reduce clutter in PINSender.php
**
**   MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala) DATE 15.May.2015
**   Added code to update the receiver network ID in the Receivers table
**
*********************************************************************************/



function updateReceiverInfo($inputArray)
{
    $receiverPhone       = $inputArray['order_data']['receiver_phone'];
    $receiverEmail       = $inputArray['order_data']['receiver_email'];
    $receiverFirstName   = $inputArray['order_data']['receiver_first_name'];
    $networkID           = $inputArray['order_data']['receiver_network_id'];
    $clientEmail         = $inputArray['payer_email'];
    $connection          = $inputArray['connection'];

    //check to see if receiver is already in table
    $isReceiverInTable ="SELECT * ";
    $isReceiverInTable.="FROM Receivers ";
    $isReceiverInTable.="WHERE ";
    $isReceiverInTable.="receiverPhone='$receiverPhone' AND clientEmail='$clientEmail' ";
    $isReceiverInResult=mysql_query($isReceiverInTable,$connection) or handleDatabaseError(''. mysql_error(),$isReceiverInTable);
    $isReceiverInNumber=mysql_num_rows($isReceiverInResult);//number of results returned
    $data = mysql_fetch_array($isReceiverInResult);

    //If receiver does not exist
    if($isReceiverInNumber==0)
    {
        $storeReceiverQuery ="INSERT INTO ";
        $storeReceiverQuery.="Receivers(receiverPhone,clientEmail";
        $add1="";
        $add2="";
        $add3="";
        if($receiverEmail!='')
        {
            $storeReceiverQuery.=" ,receiverEmail";
            $add1=",'$receiverEmail'";
        }
        else
        {
            //there is no receiverEmail provided so we will not update this information
        }

        if($receiverFirstName!='')
        {
            $storeReceiverQuery.=" ,receiverFirstName";
            $add2=",'$receiverFirstName'";
        }
        else
        {
            //there is no receiverFirstName provided so we will not update this information
        }

        if($networkID!='')
        {
            $storeReceiverQuery.=" ,networkID";
            $add3=",'$networkID'";
        }
        else
        {
            //there is no receiverFirstName provided so we will not update this information
        }

        $storeReceiverQuery.=") VALUES('$receiverPhone','$clientEmail'";
        $storeReceiverQuery.=$add1;
        $storeReceiverQuery.=$add2;
        $storeReceiverQuery.=$add3;
        $storeReceiverQuery.=")";

        $storeReceiverResult=mysql_query($storeReceiverQuery,$connection) or handleDatabaseError(''. mysql_error(),$storeReceiverQuery);

    }
    else
    {
        //this Receiver already exists
        //update the receiver table
        $receiverID = $data['receiverID'];

        $updateReceiverQuery="UPDATE Receivers SET ";

        if($receiverEmail!='')
        {
            $updateReceiverQuery.="receiverEmail='$receiverEmail' ";
        }

        if($receiverFirstName!='')
        {
            if($receiverEmail!='') $updateReceiverQuery.=",";
            $updateReceiverQuery.="receiverFirstName='$receiverFirstName' ";
        }

        if($networkID != $data['networkID'])
        {
            if($receiverEmail!='' || $receiverFirstName!='') $updateReceiverQuery.=",";
            $updateReceiverQuery.="networkID='$networkID' ";
        }

        if($receiverEmail!=''||$receiverFirstName!='')
        {
            $updateReceiverQuery.="WHERE receiverID=$receiverID";
            $updateReceiverResult=mysql_query($updateReceiverQuery,$connection) or handleDatabaseError(''. mysql_error(),$updateReceiverQuery);

            if($debug) echo"updateReceiverQuery = $updateReceiverQuery<br>";
        }
    }
}
?>