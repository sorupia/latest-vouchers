<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/SecurityFunctions.php');
	require_once($_SERVER['DOCUMENT_ROOT']."/src/country_codes.php");	

    function countryIsBlocked($subjectCountry,$countryAbbreviation)
	{
		/* Check the Country Table*/
		$connection = connect2DB2($countryAbbreviation);
		
			$countryListQuery = "SELECT * ";
			$countryListQuery.= "FROM CountryList ";
			$countryListQuery.= "WHERE countryAbbreviation = '$subjectCountry' AND status = 'BLOCKED' ";
			$countryListResult=mysql_query($countryListQuery) or handleDatabaseError(''.mysql_error(),$countryListQuery);
	
		disconnectDB($connection);
		
		/* If there is an occurence in the DB then this country is blocked */
		if( mysql_num_rows($countryListResult) > 0 )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function IPIsBlocked($usersIP, $countryAbbreviation)
	{
		$debug = false;
		/* Check the IP Table by searching for the $IPAddress and anything that prefix matches it*/
		$connection = connect2DB2($countryAbbreviation);
		
			$IPListQuery = "SELECT IPAddress ";
			$IPListQuery.= "FROM IPList ";
			$IPListQuery.= "WHERE status = 'BLOCKED' ";
			$IPListResult=mysql_query($IPListQuery) or handleDatabaseError(''.mysql_error(),$IPListQuery);
	
		disconnectDB($connection);	
		
		while ($blockedIP = mysql_fetch_array($IPListResult, MYSQL_ASSOC)) {
			
			foreach ($blockedIP as $currentPrefix) {
				if($debug){ echo"currentPrefix: $currentPrefix <br>"; }
				
				$prefixLength = strlen($currentPrefix);
				if($debug){ echo"prefixLength: $prefixLength <br>"; }
	
				//do a prefix match with the db IP as the prefix
				if(strncmp($currentPrefix,$usersIP,$prefixLength) == 0)
				{
					if($debug){ echo"Prefix Match <br>";}
					return true;
				}			
			}
		
		}
		
		if($debug){ echo"NO Prefix Match <br>";}
		return false;				
	}

	function addCountryToList($countryAbbreviation,
							  $countryToAdd,
			                 $countryName,
							 $status,
							 $username)
	{
		/* Add to the Country Table*/
		$result = false;

		$connection = connect2DB2($countryAbbreviation);

			$countryListQuery = "SELECT countryAbbreviation ";
			$countryListQuery.= "FROM CountryList ";
			$countryListQuery.= "WHERE countryAbbreviation = '$countryToAdd' ";
			$countryListResult= mysql_query($countryListQuery) or handleDatabaseError(''.mysql_error(),$countryListQuery);
			
			/* If there is an occurence in the DB then no need to add this country */
			if( mysql_num_rows($countryListResult) > 0 )
			{
				$result = false;
			}
			else
			{
				$countryListQuery = "INSERT INTO CountryList(countryAbbreviation, countryName, status, modifier) ";
				$countryListQuery.= "VALUES('$countryToAdd','$countryName','$status', '$username') ";
				$countryListResult= mysql_query($countryListQuery) or handleDatabaseError(''.mysql_error(),$countryListQuery);
				
				$result = true;
			}
			
		disconnectDB($connection);
		
		return $result;
	}
	
	function editCountry($countryAbbreviation,
						$countryToEdit,
						$status,
						$username)
	{
		/* Add to the Country Table*/
		$connection = connect2DB2($countryAbbreviation);
			$countryListQuery = "UPDATE CountryList ";
			$countryListQuery.= "SET status = '$status', modifier = '$username' ";
			$countryListQuery.= "WHERE countryAbbreviation = '$countryToEdit' ";
			$countryListResult=mysql_query($countryListQuery) or handleDatabaseError(''.mysql_error(),$countryListQuery);
		disconnectDB($connection);
	}
	
	function addIPToList($countryAbbreviation,
			             $ipToEdit,
						 $ipStatus,
						 $comments,
						 $username)
	{
		/* Add to the IP Table*/
		$result = false;

		$connection = connect2DB2($countryAbbreviation);

			$IPListQuery = "SELECT id ";
			$IPListQuery.= "FROM IPList ";
			$IPListQuery.= "WHERE IPAddress = '$ipToEdit' ";
			$IPListResult= mysql_query($IPListQuery) or handleDatabaseError(''.mysql_error(),$IPListQuery);
			
			/* If there is an occurence in the DB then no need to add this IP */
			if( mysql_num_rows($IPListResult) > 0 )
			{
				$result = false;
			}
			else
			{
				$IPListQuery = "INSERT INTO IPList(IPAddress, status, comments, modifier) ";
				$IPListQuery.= "VALUES('$ipToEdit','$ipStatus','$comments', '$username') ";
				$IPListResult= mysql_query($IPListQuery) or handleDatabaseError(''.mysql_error(),$IPListQuery);

				$result = true;
			}

		disconnectDB($connection);

		return $result;
	}
	
	function editIP($countryAbbreviation,
					$inputArray)
	{
        $ipToEdit = $inputArray['IPAddress'];
        $status   = $inputArray['ipStatus'];
        $id       = $inputArray['id'];
        $comments = $inputArray['comments'];
        $username = $inputArray['employee_username'];

		/* Add to the IP Table*/
		$connection = connect2DB2($countryAbbreviation);
			$countryListQuery = "UPDATE IPList ";
			$countryListQuery.= "SET status = '$status', ";
            $countryListQuery.= "modifier = '$username', ";
            $countryListQuery.= "comments = '$comments', ";
            $countryListQuery.= "IPAddress = '$ipToEdit' ";
			$countryListQuery.= "WHERE id = '$id' ";
			$countryListResult= mysql_query($countryListQuery) or handleDatabaseError(''.mysql_error(),$countryListQuery);
		disconnectDB($connection);
	}
	
	/* Test Code */
	//$ipLocationArray = getIPLocation();
	//echo "Subscriber's country: ".$ipLocationArray['ip_country']."<br>";
	//$result = countryIsBlocked($ipLocationArray['ip_country'],'ug');
	//echo "country status: ".(int)$result." <br>";
	
	//$ipaddress = getIP();
	//echo "My IP address: $ipaddress <br>";
	//$result = IPIsBlocked($ipaddress, 'ug');
	//echo "IP Status: ".(int)$result."<br>";
?>