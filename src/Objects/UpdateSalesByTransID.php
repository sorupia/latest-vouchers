<?
//20110311: Function to fill in the missing spaces in the Sales DB
function updateSalesByTransID($inputArray)
{
    $transid       = $inputArray['transid'];
    $receiverEmail = $inputArray['receiverEmailAddField1'];
    $clientEmail   = $inputArray['clientEmail'];
    $amount        = $inputArray['amount'];
    $clientIP      = $inputArray['clientIP'];
    $dealerEmail   = $inputArray['dealerEmail'];
    
    $connection = $inputArray['connection'];
    
    $inputArray['debug_output'].= "updateSalesByTransID function\n";
    
    $fieldToModify = false;
    $query = '';

    $updatecards_query = "UPDATE Sales ";
    $updatecards_query.= "SET ";

    if($receiverEmail!='')
    {
        $query.= "receiverEmail='$receiverEmail' ";
        $fieldToModify = true;
    }

    if($clientEmail!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "clientEmail='$clientEmail' ";
        $fieldToModify = true;
    }

    if($amount!=0)
    {
        if($fieldToModify) $query.= ",";
        $query.= "amount=$amount ";
        $fieldToModify = true;
    }

    if($clientIP!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "clientIP='$clientIP' ";
        $fieldToModify = true;
    }

    if($dealerEmail!='')
    {
        if($fieldToModify) $query.= ",";
        $query.= "dealerEmail='$dealerEmail' ";
        $fieldToModify = true;
    }
    
    if($query !='')
    {
        $updatecards_query.= $query;
        $updatecards_query.= "WHERE transactionID='$transid'";
        $updatecardsresult = mysql_query($updatecards_query,$connection) or handleDatabaseError(''. mysql_error(),$updatecards_query);
        $inputArray['debug_output'].= "Sales update query: $updatecards_query\n";
    }
    else
    {
        $inputArray['debug_output'].= "Sales query empty: $updatecards_query\n";
        $inputArray['debug_output'].= "No query executed\n";
    }
    
    return $inputArray;
}
?>