<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/GeneralFunctions.php');

	function submitFeedback($fname, $lname, $email, $subject, $message, $countryAbbreviation )
	{
		$nameOfSender=$fname.' '.$lname;
		$emailAddressOfSender=$email;
		$subject.=" - from account center";
		sendEmail($nameOfSender,$emailAddressOfSender,SUPPORT_EMAIL,'',$subject,$message);
		return "<div align=\"center\">feedback sent</div><br>";
	}
	
	/* Display last 5 transactions */
	function queryRecentTransactions($email, $countryAbbreviation)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$usedCardsQuery = "SELECT dateWeSoldIt as 'Date', cardPIN as 'Airtime',";
			$usedCardsQuery.= "itemID as 'Item',receiverPhone as 'Receiver', transactionID as 'Transaction ID' ";
			$usedCardsQuery.= "FROM UsedCards ";
			$usedCardsQuery.= "WHERE clientEmail = '$email' ";
			$usedCardsQuery.= "ORDER BY dateWeSoldIt DESC ";
			$usedCardsQuery.= "LIMIT 5";
			$usedCardsResult=mysql_query($usedCardsQuery) or handleDatabaseError(''.mysql_error(),$usedCardsQuery);
	
		disconnectDB($connection);
		return $usedCardsResult;	
	}

	function obtainAirtimeCreditsLeft($email,$countryAbbreviation)
	{
		$connection = connect2SMSDB($countryAbbreviation);
		
			$airtimeLeftQuery = "SELECT airtimeCreditsLeft ";
			$airtimeLeftQuery.= "FROM SMSUsers ";
			$airtimeLeftQuery.= "WHERE emailAddress = '$email' ";
			$airtimeLeftResult= mysql_query($airtimeLeftQuery) or handleDatabaseError(''.mysql_error(),$airtimeLeftQuery);
	
		disconnectDB($connection);

		$airtimeData = mysql_fetch_array($airtimeLeftResult);
		return $airtimeData['airtimeCreditsLeft'];		
	}
		
	function obtainSMSCreditsLeft($email,$countryAbbreviation)
	{
		$connection = connect2SMSDB($countryAbbreviation);
		
			$SMSLeftQuery = "SELECT creditsLeft ";
			$SMSLeftQuery.= "FROM SMSUsers ";
			$SMSLeftQuery.= "WHERE emailAddress = '$email' ";
			$SMSLeftResult= mysql_query($SMSLeftQuery) or handleDatabaseError(''.mysql_error(),$SMSLeftQuery);
	
		disconnectDB($connection);
		
		$SMSData = mysql_fetch_array($SMSLeftResult);
		return $SMSData['creditsLeft'];
	}
	
	/* Return true if daily purchase limit is exceeded by clientEmail*/
	function checkDailyLimitExceeded($clientEmail, $purchaseLimit, $countryAbbreviation)
	{
		$day = date("Y-m-d ");
		$day_start = $day."00:00";
		$day_end = $day."23:59";
		$limitExceeded = false;
		
		$connection = connect2DB2($countryAbbreviation);
		
			$limitQuery = "SELECT SUM(ourPriceInUSD) ";
			$limitQuery.= "FROM UsedCards ";
			$limitQuery.= "WHERE clientEmail = '$clientEmail' AND ";
			$limitQuery.= "dateWeSoldIt > '$day_start' AND dateWeSoldIt < '$day_end'";
			$limitResult= mysql_query($limitQuery) or handleDatabaseError(''.mysql_error(),$limitQuery);
	
		disconnectDB($connection);
		
		$totalPurchases = mysql_result($limitResult, 0);
		//echo "limitResult: ".$totalPurchases."<br>";
		//echo "purchaseLimit: ".$purchaseLimit."<br>";
		
		if($totalPurchases > $purchaseLimit)
		{
			$limitExceeded = true;
		}
		else
		{
			$limitExceeded = false;
		}
		
		return $limitExceeded;	
	}
	
	function queryReceivers($clientEmail, $countryAbbreviation)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$receiversQuery = "SELECT DISTINCT receiverPhone, receiverFirstName ";
			$receiversQuery.= "FROM Receivers ";
			$receiversQuery.= "WHERE clientEmail='$clientEmail' ";
			$receiversQuery.= "AND receiverPhone!='' ";
			$receiversQuery.= "LIMIT 7 ";
			$receiversResult= mysql_query($receiversQuery) or handleDatabaseError(''.mysql_error(),$receiversQuery);
			
		disconnectDB($connection);
		
		return $receiversResult;
	}
	
	function displaySendHistory()
	{
		//TODO: Modify search to search only within past month
		$historyQuery="SELECT DISTINCT receiverPhone FROM UsedCards WHERE clientEmail='$clientEmail' ";
		$historyQuery.="AND receiverPhone!='' ";
		$historyQuery.="LIMIT 7 ";
		
		$historyResult=mysql_query($historyQuery)  or handleDatabaseError(''. mysql_error(),$historyQuery);
		$historyResultCount=mysql_num_rows($historyResult);
		echo"<form name=\"historyForm\" method=\"post\" action=\"PINSender.php\" onsubmit=\"return checkHistorySend(receiverPhoneField,itemName)\">
			<table border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"details-tbl\">
			  <tr>
				<td height=\"20\" align=\"center\" bgcolor=\"#F0F0F0\"><b>Option 1:</b> Send airtime to a receiver you sent airtime to in the past</td>
			  </tr>
			  <tr>
				<td height=\"20\" align=\"center\">
				  <select name=\"receiverPhoneField\" id=\"SendHistory\">
					<option value=\"None\" selected=\"selected\">Receiver Not in History</option>";
				
				while ($historyData = mysql_fetch_array($historyResult, MYSQL_ASSOC)) {
					foreach ($historyData as $label_value) {
						//quick fix december 15th 2006
						echo"<option value=\"$label_value\">$label_value</option>";
					}
				}
					
				echo"</select>
					<input name=\"pinToSend\" type=\"hidden\" value=\"$cardPIN\">
					<input name=\"cardsAvailable\" type=\"hidden\" value=\"$cardsArePresent\">
					<input name=\"clientemail\" type=\"hidden\" value=\"$clientEmail\">
					<input name=\"itemidnumber\" type=\"hidden\" value=\"$itemIDNumber\">
					<input name=\"transid\" type=\"hidden\" value=\"$transID\">
					<input name=\"firstName\" type=\"hidden\" value=\"$firstname\"> 
					<input name=\"itemName\" type=\"hidden\" value=\"$itemname\">                   
				</td>
			  </tr>
			  <tr>
				<td colspan=\"2\" align=\"center\" bgcolor=\"#F0F0F0\"><input name=\"Submit\" type=\"submit\" class=\"bu\" value=\"Send Airtime\" /></td>
			  </tr>
		  </table>
		  </form>";	
	}
	
	function registerToMailingList($clientEmail)
	{
		/* todo: add user to mailing list, find out from how its done in phplist*/
	}
	
	function addReceiver($clientEmail, $receiverEmail)
	{
		/*Only way of adding a receiver is by making a purchase hence adding an entry in usedCards*/
		/*We could change this to have every receiver affiliated with a sender */
	}
	
	function editReceiver($clientEmail, $receiverEmail)
	{
		/*Only added once we have fixed the receiver info storage */
	}
	
	function deleteReceiver($clientEmail, $receiverEmail)
	{
		/*This should not be allowed. Whatever is in the DB stays there*/
	}		
	
?>