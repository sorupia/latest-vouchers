<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/GeneralFunctions.php');

    function insertBlockedClient( $countryAbbreviation,$newClientArray,$ipLocationArray)
	{
		/* extract contents of newClientArray*/
		$firstName 			= $newClientArray['first_name'];
	    $lastName  			= $newClientArray['last_name'];
	    $emailAddress 		= $newClientArray['payer_email'];
	    $cellPhone 			= $newClientArray['contact_phone'];
	    $signUpCountry 		= $newClientArray['residence_country'];//todo
	    $timeStamp          = date("Y-m-d H:i:s");
	    $addressStreet 		= $newClientArray['address_street'];
	    $addressCity 		= $newClientArray['address_city'];
	    $addressState 		= $newClientArray['address_state'];
	    $addressCountry 	= $newClientArray['address_country'];
	    $addressZip 		= $newClientArray['address_zip'];
	    $paymentStatus 		= $newClientArray['payment_status'];
	    $ourPayPalEmail 	= $newClientArray['receiver_email'];
	    
		$transID 			= $newClientArray['txn_id'];
	    $paymentAmount 		= $newClientArray['mc_gross'];
		$paymentCurrency	= $newClientArray['mc_currency'];
		$itemName			= $newClientArray['item_name'];
		$itemIDNumber		= $newClientArray['item_number'];

		$receiverPhone 		= $newClientArray['order_data']['receiver_phone'];//will be updated
		$receiverFirstName 	= $newClientArray['order_data']['receiver_first_name'];//will be updated
		$receiverEmail 		= $newClientArray['order_data']['receiver_email'];//will be updated

		$apiUser			= $newClientArray['apiUser'];// to be added in the paypalreturnpage

		$ip_country 		= $ipLocationArray['ip_country'];
		$ip_state 			= $ipLocationArray['ip_state'];
		$ip_city 			= $ipLocationArray['ip_city'];
        $ip_address         = $ipLocationArray['ip_address'];

        $comments           = $newClientArray['comments'];
        $connection         = $newClientArray['connection'];

        $resultArray = array();

        $query    = "SELECT * FROM BlockedClientsQueue WHERE transID='$transID'";
        $result   = mysql_query($query, $connection) or handleDatabaseError(''.mysql_error(),$query);
        $num_rows = mysql_num_rows($result);
    
        if($num_rows == 0)
        {
            $blockedQuery = "INSERT INTO BlockedClientsQueue ";
            $blockedQuery.= "SET ";
            $blockedQuery.= "firstName = '".mysql_real_escape_string($firstName)."', ";
            $blockedQuery.= "lastName = '".mysql_real_escape_string($lastName)."', ";
            $blockedQuery.= "emailAddress = '".mysql_real_escape_string($emailAddress)."', ";
            $blockedQuery.= "cellphone = '".mysql_real_escape_string($cellPhone)."', ";
            $blockedQuery.= "timeStamp = '".mysql_real_escape_string($timeStamp)."', ";
            $blockedQuery.= "addressStreet = '".mysql_real_escape_string($addressStreet)."',";
            $blockedQuery.= "addressCity = '".mysql_real_escape_string($addressCity)."', ";
            $blockedQuery.= "addressState = '".mysql_real_escape_string($addressState)."', ";
            $blockedQuery.= "addressCountry = '".mysql_real_escape_string($addressCountry)."', ";
            $blockedQuery.= "addressZip = '".mysql_real_escape_string($addressZip)."', ";
            $blockedQuery.= "paymentStatus = '".mysql_real_escape_string($paymentStatus)."', ";
            $blockedQuery.= "ourPayPalEmail = '".mysql_real_escape_string($ourPayPalEmail)."', ";
            $blockedQuery.= "transID = '".mysql_real_escape_string($transID)."', ";
            $blockedQuery.= "paymentAmount = '".mysql_real_escape_string($paymentAmount)."', ";
            $blockedQuery.= "paymentCurrency = '".mysql_real_escape_string($paymentCurrency)."', ";
            $blockedQuery.= "itemName = '".mysql_real_escape_string($itemName)."',";
            $blockedQuery.= "itemIDNumber = '".mysql_real_escape_string($itemIDNumber)."', ";
            $blockedQuery.= "clientIP = '".mysql_real_escape_string($ip_address)."', ";
            $blockedQuery.= "receiverPhone = '".mysql_real_escape_string($receiverPhone)."', ";
            $blockedQuery.= "receiverFirstName = '".mysql_real_escape_string($receiverFirstName)."', ";
            $blockedQuery.= "receiverEmail = '".mysql_real_escape_string($receiverEmail)."', ";
            $blockedQuery.= "apiUser = '".mysql_real_escape_string($apiUser)."', ";
            $blockedQuery.= "ipCountry = '".mysql_real_escape_string($ip_country)."', ";
            $blockedQuery.= "ipState = '".mysql_real_escape_string($ip_state)."', ";
            $blockedQuery.= "ipCity = '".mysql_real_escape_string($ip_city)."', ";
            $blockedQuery.= "comments = '".mysql_real_escape_string($comments)."' ";
            $newClientsResult=mysql_query($blockedQuery, $connection) or handleDatabaseError(''.mysql_error(),$blockedQuery);
            $resultArray['status'] = true;
        }
        else
        {
            $resultArray['status'] = false;
            $resultArray['message'] = "<li>Sorry this appears to be a duplicate transaction ID </li>\n";
            $resultArray['message'].= "<li>Please email us at ".SUPPORT_EMAIL." </li>\n";
        }

		return $resultArray;
	}
?>