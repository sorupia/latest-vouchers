<?
/*******************************************************************************
**   FILE: NewClientsQueueTable.php
**
**   FUNCTIONS: insertNewClientQ, queryNewClientsQ, processNewClientsQ, 
**   modifyNewClientsQ, deleteNewClientsQItem, displayNewClientsQ, displaySubmittableResults
**
**   PURPOSE: Functions to help with a New Client 
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2007 
**
**   ADAPTED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 06.Nov.2015
**   Adapted for UgandaVouchers.com 
**
*********************************************************************************/

    include_once $_SERVER['DOCUMENT_ROOT'].'/src/GeneralFunctions.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/sendATSimpleVchr.php';

    function insertNewClientQ( $countryAbbreviation,$newClientArray,$ipLocationArray)
    {
        /* extract contents of newClientArray*/
        $firstName          = $newClientArray['first_name'];
        $lastName           = $newClientArray['last_name'];
        $emailAddress       = $newClientArray['payer_email'];
        $passwd             = $newClientArray['passwd'];//to be included in the form
        $cellPhone          = $newClientArray['contact_phone'];
        $signUpCountry      = $newClientArray['residence_country'];//todo
        $dateOfFirstPurchase = date("Y-m-d H:i:s");
        $registrationDate   = date("Ymd");
        $addressStreet      = $newClientArray['address_street'];
        $addressCity        = $newClientArray['address_city'];
        $addressState       = $newClientArray['address_state'];
        $addressCountry     = $newClientArray['address_country'];
        $addressZip         = $newClientArray['address_zip'];
        $paymentStatus      = $newClientArray['payment_status'];
        $ourPayPalEmail     = $newClientArray['receiver_email'];
        
        $transID            = $newClientArray['txn_id'];
        $paymentAmount      = $newClientArray['mc_gross'];
        $paymentCurrency    = $newClientArray['mc_currency'];
        $itemName           = $newClientArray['item_name'];
        $itemIDNumber       = $newClientArray['item_number'];

        $receiverPhone      = $newClientArray['order_data']['receiver_phone'];//will be updated
        $receiverFirstName  = $newClientArray['order_data']['receiver_first_name'];//will be updated
        $receiverLastName   = $newClientArray['order_data']['receiver_last_name'];//will be updated
        $receiverEmail      = $newClientArray['order_data']['receiver_email'];//will be updated
        
        $apiUser            = $newClientArray['apiUser'];// to be added in the paypalreturnpage
        
        $ip_country         = $ipLocationArray['ip_country'];
        $ip_state           = $ipLocationArray['ip_state'];
        $ip_city            = $ipLocationArray['ip_city'];
        $ip_latitude        = $ipLocationArray['ip_latitude'];
        $ip_longitude       = $ipLocationArray['ip_longitude'];
        $ip_address         = $ipLocationArray['ip_address'];

        $order_id           = $newClientArray['invoice'];
        
        $comments           = $newClientArray['comments'];

        $connection         = $newClientArray['connection'];
        
        $resultArray = array();
        
        
        $query    = "SELECT * FROM NewClientsQueue WHERE transID='$transID'";
        $result   = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
        $num_rows = mysql_num_rows($result);

        if($num_rows == 0)
        {
            $newClientsQuery = "INSERT INTO ";
            $newClientsQuery.= "NewClientsQueue ";            
            $newClientsQuery.= "SET ";
            $newClientsQuery.= "firstName='".mysql_real_escape_string($firstName)."',";
            $newClientsQuery.= "lastName='".mysql_real_escape_string($lastName)."',";
            $newClientsQuery.= "emailAddress='".mysql_real_escape_string($emailAddress)."',";
            $newClientsQuery.= "passwd='".mysql_real_escape_string($passwd)."',";
            $newClientsQuery.= "cellphone='".mysql_real_escape_string($cellPhone)."',";
            $newClientsQuery.= "signUpCountry='".mysql_real_escape_string($signUpCountry)."', ";
            $newClientsQuery.= "dateOfFirstPurchase='".mysql_real_escape_string($dateOfFirstPurchase)."',";
            $newClientsQuery.= "registrationDate=".$registrationDate.",";
            $newClientsQuery.= "addressStreet='".mysql_real_escape_string($addressStreet)."',";
            $newClientsQuery.= "addressCity='".mysql_real_escape_string($addressCity)."',";
            $newClientsQuery.= "addressState='".mysql_real_escape_string($addressState)."',";
            $newClientsQuery.= "addressCountry='".mysql_real_escape_string($addressCountry)."',";
            $newClientsQuery.= "addressZip='".mysql_real_escape_string($addressZip)."',";
            $newClientsQuery.= "paymentStatus='".mysql_real_escape_string($paymentStatus)."',";
            $newClientsQuery.= "ourPayPalEmail='".mysql_real_escape_string($ourPayPalEmail)."',";
            $newClientsQuery.= "transID='".mysql_real_escape_string($transID)."',";
            $newClientsQuery.= "paymentAmount='".mysql_real_escape_string($paymentAmount)."',";
            $newClientsQuery.= "paymentCurrency='".mysql_real_escape_string($paymentCurrency)."',";
            $newClientsQuery.= "itemName='".mysql_real_escape_string($itemName)."',";
            $newClientsQuery.= "itemIDNumber='".mysql_real_escape_string($itemIDNumber)."',";
            $newClientsQuery.= "clientIP='".mysql_real_escape_string($ip_address)."',";
            $newClientsQuery.= "receiverPhone='".mysql_real_escape_string($receiverPhone)."',";
            $newClientsQuery.= "receiverFirstName='".mysql_real_escape_string($receiverFirstName)."',";
            $newClientsQuery.= "receiverLastName='".mysql_real_escape_string($receiverLastName)."',";
            $newClientsQuery.= "receiverEmail='".mysql_real_escape_string($receiverEmail)."',";
            $newClientsQuery.= "apiUser='".mysql_real_escape_string($apiUser)."',";
            $newClientsQuery.= "ipCountry='".mysql_real_escape_string($ip_country)."',";
            $newClientsQuery.= "ipState='".mysql_real_escape_string($ip_state)."',";
            $newClientsQuery.= "ipCity='".mysql_real_escape_string($ip_city)."', ";
            $newClientsQuery.= "order_id='".mysql_real_escape_string($order_id)."', ";
            $newClientsQuery.= "comments='".mysql_real_escape_string($comments)."' ";
            $newClientsResult=mysql_query($newClientsQuery,$connection) or handleDatabaseError(''.mysql_error(),$newClientsQuery);
            $resultArray['status'] = true;
        }
        else
        {
            $resultArray['status'] = false;
            $resultArray['message'] = "<li>Sorry this appears to be a duplicate transaction ID </li>\n";
            $resultArray['message'].= "<li>Please email us at ".SUPPORT_EMAIL." </li>\n";
        }

        return $resultArray;
    }

    function queryNewClientsQ($countryAbbreviation)
    {
        $connection = connect2DB2($countryAbbreviation);
            $clientQuery = "SELECT firstName, lastName, emailAddress, passwd, cellphone, ";
            $clientQuery.= "signUpCountry, dateOfFirstPurchase, addressStreet, ";
            $clientQuery.= "addressCity, addressState, addressCountry, addressZip, ";
            $clientQuery.= "paymentStatus, ourPayPalEmail, transID, paymentAmount, ";
            $clientQuery.= "paymentCurrency, itemName, itemIDNumber, clientIP, ";
            $clientQuery.= "comments, receiverPhone, receiverFirstName, receiverEmail, apiUser ";
            $clientQuery.= "FROM NewClientsQueue ";
            $clientResult=mysql_query($clientQuery) or handleDatabaseError(''.mysql_error(),$clientQuery);
        disconnectDB($connection);
        

        return $clientResult;
    }

    function processNewClientsQ($countryAbbreviation,
                               $newClientArray,
                               $userStatus,
                               $username)
    {
        /* extract contents of newClientArray*/
        $firstName      = $newClientArray['clientFirstName'];
        $lastName       = $newClientArray['clientLastName'];
        $emailAddress   = $newClientArray['clientEmail'];
        $passwd         = $newClientArray['passwd'];
        $cellPhone      = $newClientArray['clientPhone'];
        $signUpCountry  = $newClientArray['addressCountry'];
        $dateOfFirstPurchase = $newClientArray['dateOfFirstPurchase'];
        $registrationDate = $newClientArray['registrationDate'];
        $addressStreet  = $newClientArray['addressStreet'];
        $addressCity    = $newClientArray['addressCity'];
        $addressState   = $newClientArray['addressState'];
        $addressCountry = $newClientArray['addressCountry'];
        $addressZip     = $newClientArray['addressZip'];
        $transID        = $newClientArray['transID'];
        $clientIP       = $newClientArray['clientIP'];
        $itemIDNumber   = $newClientArray['itemIDNumber'];
        $order_id       = $newClientArray['order_id'];
        
        $resultArray = array();
        $cardPIN = 0;
        $client_exists =true;

        /*add user to the SMSUsers table as verified*/
        $connection = connect2DB2($countryAbbreviation);
            $query    = "SELECT * FROM SMSUsers WHERE emailAddress='$emailAddress'";
            $result   = mysql_query($query) or handleDatabaseError(''.mysql_error(),$query);
            $num_rows = mysql_num_rows($result);
            
            if($num_rows==0)
            {
                $insertUserQuery = "INSERT INTO ";
                $insertUserQuery.= "SMSUsers ";
                $insertUserQuery.= "SET ";
                $insertUserQuery.= "firstName='".mysql_real_escape_string($firstName)."',";
                $insertUserQuery.= "lastName='".mysql_real_escape_string($lastName)."',";
                $insertUserQuery.= "emailAddress='".mysql_real_escape_string($emailAddress)."',";
                $insertUserQuery.= "passwd='".mysql_real_escape_string($passwd)."',";
                $insertUserQuery.= "cellphone='".mysql_real_escape_string($cellPhone)."',";
                $insertUserQuery.= "signUpCountry='".mysql_real_escape_string($signUpCountry)."', ";
                $insertUserQuery.= "dateOfFirstPurchase='".mysql_real_escape_string($dateOfFirstPurchase)."',";
                $insertUserQuery.= "registrationDate='".mysql_real_escape_string($registrationDate)."',";
                $insertUserQuery.= "addressStreet='".mysql_real_escape_string($addressStreet)."',";
                $insertUserQuery.= "addressCity='".mysql_real_escape_string($addressCity)."',";
                $insertUserQuery.= "addressState='".mysql_real_escape_string($addressState)."',";
                $insertUserQuery.= "addressCountry='".mysql_real_escape_string($addressCountry)."',";
                $insertUserQuery.= "addressZip='".mysql_real_escape_string($addressZip)."',";
                $insertUserQuery.= "subscriberStatus='$userStatus', ";
                $insertUserQuery.= "airtimeStatus='$userStatus', ";
                $insertUserQuery.= "creditsLeft=".SMS_MAX_CREDITS.", ";
                $insertUserQuery.= "verifiedBy='$username', ";
                $insertUserQuery.= "signUpIP='".mysql_real_escape_string($clientIP)."' ";
                $insertUserResult=mysql_query($insertUserQuery) or handleDatabaseError(''.mysql_error(),$insertUserQuery);
                $client_exists = false;
            }
            else
            {
                $updateUserQuery = "UPDATE ";
                $updateUserQuery.= "SMSUsers ";
                $updateUserQuery.= "SET ";
                $updateUserQuery.= "subscriberStatus='$userStatus', ";
                $updateUserQuery.= "airtimeStatus='$userStatus' ";
                $updateUserQuery.= "WHERE emailAddress='$emailAddress' ";
                $updateUserResult=mysql_query($updateUserQuery) or handleDatabaseError(''.mysql_error(),$updateUserQuery);
                $client_exists =true;
            }
        disconnectDB($connection);

        /*use API to send the airtime that was purchased*/
        /* it will also update the UsedCards and Sales tables*/
        if($userStatus == 'VERIFIED')
        {
            //print_r($newClientArray);
            $resultArray = sendATSimpleVchr($countryAbbreviation, $newClientArray);
            $status = $resultArray['status'];
            $cardPIN = $resultArray['card'];

            $body ="Hi $firstName, \n";
            $body.="The verification of your Airtime and SMS account at ".DOMAIN_PREFIX." is complete. ";
            $body.="Now you have access to send airtime and free SMS anytime to ".COUNTRY_NAME.". \n";
            $body.="To create a password for your SMS account please go to the following link: ";
            $body.=INHERIT_SITE."change_password.php?type=airtime&country=".COUNTRY_ABBREVIATION." \n";
            $body.="--Thanks,\n";
            $body.=$username."\n";
            $body.=SERVICED_BY." [".SUPPORT_EMAIL."]";
            $subject="Account verification complete";
            sendEmail('','',$emailAddress,SIGNUP_EMAIL,$subject,$body);
        }
        else
        {
            //provide no airtime
        }

        /* Delete NewClientsQueueTable Row only if airtime has been assigned to recipient */
        if( (!isAutoDispatch($itemIDNumber) && $cardPIN != 0) ||
            ( isAutoDispatch($itemIDNumber) && $cardPIN == "AUTO_LOAD"))
        {
            deleteNewClientsQItem($countryAbbreviation,$transID);
            
            if($client_exists) $resultArray['message'].="$emailAddress already exists<br>";
        }

        return $resultArray;
    }

    // only modifies comments
    function modifyNewClientsQ($countryAbbreviation,
                              $transID,
                              $modifications)
    {
        $debug = false;
        /*for now the only thing we can modify here is the comments*/
        $updateRequired = false;
        $comments               = $modifications['comments'];
        $receiverFirstName      = $modifications['receiverFirstName'];
        $receiverNumberField    = $modifications['receiverNumberField'];
        $receiverEmail          = $modifications['receiverEmail'];
        
        $connection = connect2DB2($countryAbbreviation);
        
            $modNewClientQuery = "UPDATE NewClientsQueue ";
            $modNewClientQuery.= "SET ";
            
            if($comments!=""){
                $preparedQuery = "comments = '$comments' "; 
                $updateRequired = true;
            }
            if($receiverFirstName!=""){
                if($updateRequired){ $preparedQuery.= ", ";}
                $preparedQuery.= "receiverFirstName = '$receiverFirstName' "; 
                $updateRequired = true;
            }
            if($receiverNumberField!=""){
                if($updateRequired){ $preparedQuery.= ", ";}
                $preparedQuery.= "receiverPhone = '$receiverNumberField' "; 
                $updateRequired = true;
            }
            if($receiverEmail!=""){
                if($updateRequired){ $preparedQuery.= ", ";}
                $preparedQuery.= "receiverEmail = '$receiverEmail' "; 
                $updateRequired = true;
            }
            
            
            if( $updateRequired )
            {
                $modNewClientQuery.= $preparedQuery;
                $modNewClientQuery.= "WHERE transID = '$transID'";
                if($debug) echo "mod Query: $modNewClientQuery<br>";
                $modNewClientResult= mysql_query($modNewClientQuery) or handleDatabaseError(''.mysql_error(),$modNewClientQuery);
            }

        disconnectDB($connection);
        
        return $updateRequired;	
    }

    function deleteNewClientsQItem($countryAbbreviation,
                                  $transID)
    {
        /* Delete NewClientsQueueTable Row*/
        $connection = connect2DB2($countryAbbreviation);
            $deleteNewClientQuery = "DELETE FROM NewClientsQueue ";
            $deleteNewClientQuery.= "WHERE transID='$transID' ";
            $deleteNewClientResult=mysql_query($deleteNewClientQuery) or handleDatabaseError(''.mysql_error(),$deleteNewClientQuery);
        disconnectDB($connection);
    }

    function displayNewClientsQ($result)
    {
        /*Create a display that has inputs to Rx Phone
          waiting for Anthony's designs*/
        
    }

    function displaySubmittableResults($queryResult)
    {
        // Print the Client data in a HTML table 
        echo "<table class=\"large-tbl\" width=\"90%\"  border=\"0\" >";
        echo"<tr>";
        
        $numOfColumns=mysql_num_fields($queryResult);
        
        for($i=0 ; $i<$numOfColumns ; $i++){
            $columnName=mysql_field_name($queryResult,$i);	
            echo"<td>$columnName</td>";
        }
        echo "<td>Option 1</td><td>Option 2</td><td>Option 3</td>";
        echo"</tr>";
        $j=0;
        while ($queryData = mysql_fetch_array($queryResult, MYSQL_ASSOC)) {
            $j++;
            if($j%2){
                print "<tr>";
            }
            else{
                print "<tr bgcolor=\"#F0F0F0\">";
            }
            foreach ($queryData as $col_value) {
                print "<td>$col_value</td>";
            }

            print "<form name=\"newClientQ\" id=\"newClientQ\" method=\"post\" action=\"newClientQHandler.php\">";
            print "<td><input name=\"SendAT\" type=\"submit\" id=\"SendAT\" value=\"SendAT\" />";
            
            print "<input name=\"firstName\" type=\"hidden\" value=\"".$queryData['firstName']."\"> ";
            print "<input name=\"lastName\" type=\"hidden\" value=\"".$queryData['lastName']."\">";
            print "<input name=\"emailAddress\" type=\"hidden\" value=\"".$queryData['emailAddress']."\"> ";
            print "<input name=\"passwd\" type=\"hidden\" value=\"".$queryData['passwd']."\"> ";
            print "<input name=\"passwd\" type=\"hidden\" value=\"".$queryData['passwd']."\"> ";
            print "<input name=\"dateOfFirstPurchase\" type=\"hidden\" value=\"".$queryData['dateOfFirstPurchase']."\"> ";
            //print "<input name=\"registrationDate\" type=\"hidden\" value=\"".$queryData['registrationDate']."\"> ";
            print "<input name=\"addressStreet\" type=\"hidden\" value=\"".$queryData['addressStreet']."\"> ";
            print "<input name=\"addressCity\" type=\"hidden\" value=\"".$queryData['addressCity']."\"> ";
            print "<input name=\"addressState\" type=\"hidden\" value=\"".$queryData['addressState']."\"> ";
            print "<input name=\"addressCountry\" type=\"hidden\" value=\"".$queryData['addressCountry']."\"> ";
            print "<input name=\"signUpCountry\" type=\"hidden\" value=\"".$queryData['signUpCountry']."\"> ";
            print "<input name=\"addressZip\" type=\"hidden\" value=\"".$queryData['addressZip']."\"> ";
            print "<input name=\"paymentStatus\" type=\"hidden\" value=\"".$queryData['paymentStatus']."\"> ";
            print "<input name=\"ourPayPalEmail\" type=\"hidden\" value=\"".$queryData['ourPayPalEmail']."\"> ";
            print "<input name=\"transID\" type=\"hidden\" value=\"".$queryData['transID']."\"> ";
            print "<input name=\"paymentAmount\" type=\"hidden\" value=\"".$queryData['paymentAmount']."\"> ";
            print "<input name=\"paymentCurrency\" type=\"hidden\" value=\"".$queryData['paymentCurrency']."\"> ";
            print "<input name=\"itemName\" type=\"hidden\" value=\"".$queryData['itemName']."\"> ";
            print "<input name=\"itemIDNumber\" type=\"hidden\" value=\"".$queryData['itemIDNumber']."\"> ";
            print "<input name=\"clientIP\" type=\"hidden\" value=\"".$queryData['clientIP']."\"> ";
            print "<input name=\"receiverFirstName\" type=\"hidden\" value=\"".$queryData['receiverFirstName']."\"> ";
            print "<input name=\"receiverEmail\" type=\"hidden\" value=\"".$queryData['receiverEmail']."\"> ";
            print "<input name=\"receiverPhone\" type=\"hidden\" value=\"".$queryData['receiverPhone']."\"> ";
            print "<input name=\"apiUser\" type=\"hidden\" value=\"".$queryData['apiUser']."\"></td> ";
                    
            print "<td><input name=\"Block\" type=\"submit\" id=\"Block\" value=\"Block\" /></td> ";
            print "<td><input name=\"Delete\" type=\"submit\" id=\"Delete\" value=\"Delete\" /></td> ";
            print "</form>";

            print "</tr>";
        }
        print "</table>";
    }
?>