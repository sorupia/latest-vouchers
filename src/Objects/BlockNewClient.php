<?
	function blockNewClient($newClientArray)
	{
		/* extract contents of newClientArray*/
		$firstName 		= $newClientArray['clientFirstName'];
	    $lastName  		= $newClientArray['clientLastName'];
	    $emailAddress 	= $newClientArray['clientEmail'];
		$passwd	 		= $newClientArray['passwd'];
	    $cellPhone 		= $newClientArray['clientPhone'];
	    $signUpCountry 	= $newClientArray['addressCountry'];
	    $dateOfFirstPurchase = $newClientArray['dateOfFirstPurchase'];
	    $registrationDate = $newClientArray['registrationDate'];
	    $addressStreet 	= $newClientArray['addressStreet'];
	    $addressCity 	= $newClientArray['addressCity'];
	    $addressState 	= $newClientArray['addressState'];
	    $addressCountry = $newClientArray['addressCountry'];
	    $addressZip 	= $newClientArray['addressZip'];
	    $transID 		= $newClientArray['transID'];
		$clientIP		= $newClientArray['clientIP'];
        $username       = $newClientArray['employeeUsername'];
        $userStatus     = $newClientArray['userStatus'];
		
		$resultArray = array();
		$cardPIN = 0;
        $client_exists = true;

		/*add user to the SMSUsers table as verified*/
		$connection = connect2DB2(COUNTRY_ABBREVIATION);
			$query    = "SELECT * FROM SMSUsers WHERE emailAddress='$emailAddress'";
            $result   = mysql_query($query) or handleDatabaseError(''.mysql_error(),$query);
            $num_rows = mysql_num_rows($result);
            
            if($num_rows==0)
            {
                $insertUserQuery = "INSERT INTO ";
                $insertUserQuery.= "SMSUsers ";
                $insertUserQuery.= "SET ";
                $insertUserQuery.= "firstName='".mysql_real_escape_string($firstName)."',";
                $insertUserQuery.= "lastName='".mysql_real_escape_string($lastName)."',";
                $insertUserQuery.= "emailAddress='".mysql_real_escape_string($emailAddress)."',";
                $insertUserQuery.= "passwd='".mysql_real_escape_string($passwd)."',";
                $insertUserQuery.= "cellphone='".mysql_real_escape_string($cellPhone)."',";
                $insertUserQuery.= "signUpCountry='".mysql_real_escape_string($signUpCountry)."', ";
                $insertUserQuery.= "dateOfFirstPurchase='".mysql_real_escape_string($dateOfFirstPurchase)."',";
                $insertUserQuery.= "registrationDate='".mysql_real_escape_string($registrationDate)."',";
                $insertUserQuery.= "addressStreet='".mysql_real_escape_string($addressStreet)."',";
                $insertUserQuery.= "addressState='".mysql_real_escape_string($addressState)."',";
                $insertUserQuery.= "addressCountry='".mysql_real_escape_string($addressCountry)."',";
                $insertUserQuery.= "addressZip='".mysql_real_escape_string($addressZip)."',";
                $insertUserQuery.= "subscriberStatus='$userStatus',";
                $insertUserQuery.= "airtimeStatus='$userStatus', ";
                $insertUserQuery.= "creditsLeft=".SMS_MAX_CREDITS.", ";
                $insertUserQuery.= "verifiedBy='$username', ";
                $insertUserQuery.= "signUpIP='".mysql_real_escape_string($clientIP)."' ";
                $insertUserResult=mysql_query($insertUserQuery) or handleDatabaseError(''.mysql_error(),$insertUserQuery);
                $client_exists = false;
                
                $resultArray['result'] = true;
                $resultArray['message'].="$emailAddress is now blacklisted<br>";
            }
            else
            {
                $resultArray['result'] = false;
                $resultArray['message'].="$emailAddress already exists<br>";
            }
		disconnectDB($connection);

        deleteNewClientsQItem(COUNTRY_ABBREVIATION,$transID);

        return $resultArray;
	}
?>