<?

//201012171125: Function to notify support staff of a client in the OutgoingQueue.
//20091004, Arthur: Inform the support staff that there is a client in the outgoing queue

function clientInOutgoingQueue($inputArray)
{
    $receiverEmail = $inputArray['receiverEmailAddField1'];
    $clientEmail = $inputArray['clientemail'];
    $transid = $inputArray['transid'];
    $itemidnumber = $inputArray['itemidnumber'];
    $receiverPhone = $inputArray['receiverPhoneField'];

    $fromName = FROM_NAME;
    $toAddress = SUPPORT_EMAIL;
    $bcc = CARD_ADMIN;
    $subject = "Client: $receiverEmail in Outgoing Queue";
    $body = "The user below has been added to the outgoing queue because ";
    $body.= "there are no more cards left: \n";
    $body.= "Client Email: $clientEmail \n";
    $body.= "Transaction ID: $transid \n";
    $body.= "Item ID: $itemidnumber \n";
    $body.= "Receiver Phone: $receiverPhone \n";

    sendEmail($fromName,$fromAddress,$toAddress,$bcc,$subject,$body);
}
?>