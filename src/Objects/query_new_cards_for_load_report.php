<?
/*******************************************************************************
**  FILE: query_new_cards_for_load_report.php
**
**  FUNCTION: query_new_cards_for_load_report
**
**  PURPOSE: Query the NewCards table if given a number of inputs to be used 
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.10.24
*********************************************************************************/

    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/log_new_cards_views.php';

	function query_new_cards_for_load_report($input_array)
	{
        $countryAbbreviation = $input_array['country_abbreviation'];
        $start_date          = $input_array['start_date'];
        $end_date            = $input_array['end_date'];
		$connection          = $input_array['connection'];
        $item_id             = $input_array['itemID'];
        $username            = $input_array['username'];

        $query = "SELECT ";
        $query.= "dateWePurchased,itemID,cardPIN,cardStatus ";
        $query.= "FROM NewCards ";
        $query.= "WHERE dateWePurchased > '$start_date' AND dateWePurchased < '$end_date' ";

        if($item_id != "")
        {
            $query.= "AND itemID = '$item_id' ";
        }

        $query.= "ORDER BY dateWePurchased DESC";
        $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);

		/* log view by username*/
        $input_array['comments'] = "Load report viewed for $item_id for dates $start_date to $end_date";
		log_new_cards_views($input_array);

		return $result;
	}
?>