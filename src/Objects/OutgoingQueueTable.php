<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/GeneralFunctions.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/src/sendATSimpleAuto.php');

	function queryOutgoingQ($countryAbbreviation)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$outgoingQQuery = "SELECT timeStamp as 'Trans Time', transactionID as 'Trans ID', ";
			$outgoingQQuery.= "clientEmail as 'Client Email', receiverPhone as 'Receiver Phone',";
			$outgoingQQuery.= "itemID as 'Item ID',dispatchStatus as 'Dispatch Status',";
			$outgoingQQuery.= "clientFirstName as 'Client Name', receiverFirstName as 'Receiver Name',";
			$outgoingQQuery.= "receiverEmail as 'Receiver Email' ";
			$outgoingQQuery.= "FROM OutgoingQueue ";
			$outgoingQQuery.= "ORDER BY timeStamp DESC";
			$outgoingQResult=mysql_query($outgoingQQuery) or handleDatabaseError(''.mysql_error(),$outgoingQQuery);
	
		disconnectDB($connection);
		
		return $outgoingQResult;		
	}

	function processOutgoingQ($countryAbbreviation,
							  $OutgoingQueueArray,
							  $userStatus,
							  $username)
	{
		/*Its assumed that this user is already signed up with us and the only reason */
		/*he is in the queue is because there is no airtime left or has exceeded the daily limit*/
		/*use API to send the airtime*/
		
		/* extract transaction ID from OutgoingQueueArray*/
	    $transID 		= $OutgoingQueueArray['transID'];
        $itemIDNumber   = $OutgoingQueueArray['itemIDNumber'];

        $resultArray = array();
		$cardPIN = 0;

		/*use API to send the airtime that was purchased*/
		if($userStatus == 'VERIFIED')
		{
			//print_r($OutgoingQueueArray);
            $resultArray = sendATSimpleAuto($countryAbbreviation, $OutgoingQueueArray);
			$status = $resultArray['status'];
			$cardPIN = $resultArray['card'];
		}
		else
		{
			//provide no airtime
		}

		/* Delete NewClientsQueueTable Row only if airtime has been assigned to recipient */
        if( (!isAutoDispatch($itemIDNumber) && $cardPIN != 0) )
        {
            deleteOutgoingQItem($countryAbbreviation,$transID);
        }
        else if( isAutoDispatch($itemIDNumber) && ($cardPIN == "AUTO_LOAD"))
        {
            $transID = str_replace("_1","",$transID);
            deleteOutgoingQItem($countryAbbreviation,$transID);
        }

        return $resultArray;
	}
	
	function modifyOutgoingQ($countryAbbreviation,
							 $transID,
							 $modifications)
	{
		$updateRequired = false;
		$receiverPhone = $modifications['receiverPhone'];
		
		$connection = connect2DB2($countryAbbreviation);
		
			$modOutQQuery = "UPDATE OutgoingQueue ";
			$modOutQQuery.= "SET ";
			
			if($receiverPhone!=""){ 		
				$preparedQuery = "receiverPhone = '$receiverPhone' "; 
				$updateRequired = true;
			}
			
			if( $updateRequired )
			{
				$modOutQQuery.= $preparedQuery;
				$modOutQQuery.= "WHERE transactionID = '$transID'";
				echo "mod Query: $modOutQQuery<br>";
				$modOutQResult= mysql_query($modOutQQuery) or handleDatabaseError(''.mysql_error(),$modOutQQuery);
			}
	
		disconnectDB($connection);
		
		return $updateRequired;		
	}
	
	function deleteOutgoingQItem($countryAbbreviation,
								 $transID)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$deleteOutQQuery = "DELETE ";
			$deleteOutQQuery.= "FROM OutgoingQueue ";
			$deleteOutQQuery.= "WHERE transactionID LIKE '%$transID%'";
			$deleteOutQResult=mysql_query($deleteOutQQuery) or handleDatabaseError(''.mysql_error(),$deleteOutQQuery);
	
		disconnectDB($connection);
		
		return $deleteOutQResult;	
	}
	
	function displayOutgoingQ()
	{
		/*Create a display that has inputs to Rx Phone*/
	}	

?>