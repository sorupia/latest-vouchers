<?
	//require_once($_SERVER['DOCUMENT_ROOT'].'/api/getCard.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/GeneralFunctions.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/src/sendATSimpleAuto.php');
	
	function insertIntoIncompletePaymentsQ($countryAbbreviation,$clientArray)
	{
		//echo"fn: insertIntoIncompletePaymentsQ: $countryAbbreviation<br>";
		$clientEmail 		= $clientArray['payer_email'];
		$receiverPhone 		= $clientArray['order_data']['receiver_phone'];
		$receiverFirstName 	= $clientArray['order_data']['receiver_first_name'];
		$receiverEmail 		= $clientArray['order_data']['receiver_email'];
		$timeOfPayment      = $clientArray['timeCardSent'];
		
		$ip_country 		= $clientArray['ip_country'];
		$ip_state 			= $clientArray['ip_state'];
		$ip_city 			= $clientArray['ip_city'];
        $clientIP 			= $clientArray['ip_address'];
		
		$connection = connect2DB2($countryAbbreviation);
			$incompletePaymentsQuery = "INSERT INTO ";
			$incompletePaymentsQuery.= "IncompletePaymentsQueue(timeOfPayment,clientEmail,receiverPhone,receiverFirstName,";
			$incompletePaymentsQuery.= "receiverEmail,clientIP,ipCountry,ipState,ipCity) ";
			$incompletePaymentsQuery.= "VALUES('$timeOfPayment','$clientEmail','$receiverPhone','$receiverFirstName','$receiverEmail','$clientIP',";
			$incompletePaymentsQuery.= "'$ip_country','$ip_state','$ip_city')";
			$incompletePaymentsResult=mysql_query($incompletePaymentsQuery,$connection) or handleDatabaseError(''.mysql_error(),$incompletePaymentsQuery);
		disconnectDB($connection);
		
		return $incompletePaymentsResult;
	}
	
	function queryIncompletePaymentsQ($countryAbbreviation)
	{
		$connection = connect2DB2($countryAbbreviation);
			$incompletePaymentsQuery = "SELECT * FROM IncompletePaymentsQueue";
			$incompletePaymentsResult=mysql_query($incompletePaymentsQuery) or handleDatabaseError(''.mysql_error(),$incompletePaymentsQuery);
		disconnectDB($connection);
		
		return $incompletePaymentsResult;
	}
	
	function deleteIncompletePaymentsQ($countryAbbreviation,
									   $id)
	{
		$connection = connect2DB2($countryAbbreviation);
			$deleteIncomplete = "DELETE FROM IncompletePaymentsQueue ";
			$deleteIncomplete.= "WHERE id = '$id'";
			$deleteIncompleteResult=mysql_query($deleteIncomplete) or handleDatabaseError(''.mysql_error(),$deleteIncomplete);
		disconnectDB($connection);
		
		return "Deleted record with id = $id";
	}
	
	function processIncompletePaymentsQ($countryAbbreviation,
										$incompleteQueryArray)
	{
		$id = $incompleteQueryArray['id'];
        $resultArray = array();
        
        $resultArray = sendATSimpleAuto($countryAbbreviation, $incompleteQueryArray);
        $status = $resultArray['status'];
        $cardPIN = $resultArray['card'];

		/* Delete IncompleteQueueTable Row*/
		if($cardPIN != 0){
            deleteIncompletePaymentsQ($countryAbbreviation,$id);
        }
        
        return $resultArray;
	}
    
    function
        modifyIncompletePayment($countryAbbreviation,
                                $id,
                                $modifications)
    {
        $debug = false;
        $updateRequired = false;
		$comments      = $modifications['comments'];
		$transactionID = $modifications['transactionID'];
        
        $connection = connect2DB2($countryAbbreviation);

            $query = "UPDATE IncompletePaymentsQueue ";
            $query.= "SET ";
            
            if($comments!=""){
                $preparedQuery = "comments = '$comments' "; 
				$updateRequired = true;
            }
            if($transactionID){
                if($updateRequired){ $preparedQuery.= ", ";}
				$preparedQuery.= "transactionID = '$transactionID' "; 
				$updateRequired = true;
            }
            
            if( $updateRequired )
			{
				$query.= $preparedQuery;
				$query.= "WHERE id = '$id'";
				$queryResult= mysql_query($query) or handleDatabaseError(''.mysql_error(),$query);
			}

        disconnectDB($connection);

        return $updateRequired;
        
    }

	
	function displayIncompletePaymentsQ()
	{
	}
?>