<?
    //20110125: Function to use Card details
    function useCard($inputArray)
    {
        //INPUTS
        $itemIDNumber = $inputArray['item_number'];
        $transID      = $inputArray['txn_id'];
        $clientEmail  = $inputArray['payer_email'];
        $paymentFee   = $inputArray['mc_fee'];
        $connection   = $inputArray['connection'];
        $timeCardSent = $inputArray['timeCardSent'];
        $sessionUser  = isset($inputArray['sessionUser']) ? $inputArray['sessionUser']: '';
        $used_cards_table   = isset($inputArray['used_cards_table']) ? $inputArray['used_cards_table'] : "UsedCards";
        $used_cards_table   = ($used_cards_table =="") ? "UsedCards" : $used_cards_table;

        //LOCK
        $lockQuery  = "LOCK TABLES NewCards WRITE";
        $lockResult = mysql_query($lockQuery,$connection) or handleDatabaseError(''. mysql_error(),$lockQuery);
        
        //OBTAIN PIN FROM NEWCARDS
        $newcards_query = "SELECT * ";
        $newcards_query.= "FROM NewCards ";
        $newcards_query.= "WHERE cardStatus <>'USED' AND itemID='$itemIDNumber' ";
        $newcards_query.= "ORDER BY `dateWePurchased` ASC ";
        $newcards_query.= "LIMIT 1 FOR UPDATE";
        $newcardsresult = mysql_query($newcards_query,$connection) or handleDatabaseError(''. mysql_error(),$newcards_query);
        $newcards_data = mysql_fetch_array($newcardsresult);
        $numberOfCardsLeft = mysql_num_rows($newcardsresult);
        mysql_free_result($newcardsresult);         

        if($numberOfCardsLeft>0)
        {
            $cardPIN = $newcards_data['cardPIN'];
            $inputArray['cardPIN'] = $cardPIN;
        }
        else
        {
            $cardPIN = -1;
            $inputArray['cardPIN'] = $cardPIN;
        }

        //HOLD CARD
        $updatecards_query = "UPDATE NewCards ";
        $updatecards_query.= "SET cardStatus='USED' ";
        $updatecards_query.= "WHERE cardPIN='$cardPIN' ";
        $updatecardsresult = mysql_query($updatecards_query,$connection) or handleDatabaseError(''. mysql_error(),$updatecards_query);

        //DELETE CARD FROM NEWCARDS
        $newcards_query = "DELETE FROM NewCards ";
        $newcards_query.= "WHERE cardPIN = '$cardPIN' AND cardStatus='USED' ";
        $deleteresult = mysql_query($newcards_query,$connection) or handleDatabaseError(''. mysql_error(),$newcards_query);

        //UNLOCK
        $lockQuery = "UNLOCK TABLES";
        $lockResult = mysql_query($lockQuery,$connection) or handleDatabaseError(''. mysql_error(),$lockQuery);

        if($numberOfCardsLeft>0)
        {
            $cardsArePresent = "true";
            $inputArray['cardsArePresent'] = $cardsArePresent;

            //EXTRACT VARIABLES TO BE STORED TO DB
            $networkName     = $newcards_data['networkName'];
            $valueInLocal    = $newcards_data['valueInUGSh'];
            $ourPriceInUSD   = $newcards_data['ourPriceInUSD'];
            $dateWePurchased = $newcards_data['dateWePurchased'];
            $itemIDNumber    = $newcards_data['itemID'];
            $loadedBy        = $newcards_data['loadedBy'];
            $loadersIP       = $newcards_data['loadersIP'];
            $serialNumber    = $newcards_data['serialNumber'];

            //RETURN VALUES
            $inputArray['networkName']     = $networkName;
            $inputArray['valueInLocal']    = $valueInLocal;
            $inputArray['ourPriceInUSD']   = $ourPriceInUSD;
            $inputArray['dateWePurchased'] = $dateWePurchased;
            $inputArray['itemIDNumber']    = $itemIDNumber;
            $inputArray['loadedBy']        = $loadedBy;
            $inputArray['loadersIP']       = $loadersIP;
            $inputArray['serialNumber']    = $serialNumber;
            $inputArray['receiverPhone']   = $inputArray['order_data']['receiver_phone'];
            $receiverPhone                 = $inputArray['order_data']['receiver_phone'];
            $receiverFirstName             = $inputArray['order_data']['receiver_first_name'];
            $receiverEmail                 = $inputArray['order_data']['receiver_email'];
            $firstName                     = $inputArray['first_name'];

            //$receiverPhone = '+256790834853';
            $clickMessageID = sendEmail2Phone($receiverPhone,$receiverFirstName,$firstName,$cardPIN,$ourPriceInUSD,$itemIDNumber);
            $inputArray['clickMessageID'] = $clickMessageID;

            if($receiverEmail!='')
            {
                emailAirtimeRx($inputArray);
            }

            //send an email to the card administrator informing him about the exact details of the purchase.
            notifyOnPurchase($inputArray);
            notifyOnLowCards($inputArray,$connection);            
            
            //MOVE EXTRACTED CARD TO USEDCARDS
            $newcards_query = "INSERT IGNORE INTO $used_cards_table ";
            $newcards_query.= "SET ";
            $newcards_query.= "cardPIN = '$cardPIN', ";
            $newcards_query.= "networkName = '$networkName', ";
            $newcards_query.= "valueInUGSh = $valueInLocal, ";
            $newcards_query.= "ourPriceInUSD = $ourPriceInUSD, ";
            $newcards_query.= "dateWePurchased = '$dateWePurchased', ";
            $newcards_query.= "dateWeSoldIt = '$timeCardSent', ";
            $newcards_query.= "transactionID = '$transID', ";
            $newcards_query.= "itemID = '$itemIDNumber', ";
            $newcards_query.= "clientEmail = '$clientEmail', ";
            $newcards_query.= "loadedBy = '$loadedBy', ";
            $newcards_query.= "loadersIP = '$loadersIP', ";
            $newcards_query.= "serialNumber = '$serialNumber', ";
            $newcards_query.= "receiverEmail='$receiverEmail', ";
            $newcards_query.= "receiverPhone='$receiverPhone', ";
            $newcards_query.= "smsGWID='$clickMessageID', ";
            $newcards_query.= "sessionUser = '$sessionUser', ";
            $newcards_query.= "paymentFee = $paymentFee ";

            if(isset($inputArray['saleType']) && strlen($inputArray['saleType']) > 1)
            {
                $newcards_query.= ", saleType = '".$inputArray['saleType']."' ";
            }

            if(isset($inputArray['our_fee']) && $inputArray['our_fee'] > 0)
            {
                $newcards_query.= ", our_fee = ".$inputArray['our_fee']." ";
            }

            if(isset($inputArray['invoice']) && $inputArray['invoice'] > 0)
            {
                $newcards_query.= ", order_id = ".$inputArray['invoice']." ";
            }

            $insertresult = mysql_query($newcards_query,$connection) or handleDatabaseError(''. mysql_error(),$newcards_query);

        }
        else
        {
            $inputArray['cardsArePresent'] = "false";
        }

        return $inputArray;
    }
?>
