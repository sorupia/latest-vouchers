<?
    require_once($_SERVER['DOCUMENT_ROOT'].'/api/getCard.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/GeneralFunctions.php');
    
    function 
        insertFailedTransaction($countryAbbreviation,
                                $clientEmail,
                                $receiverPhone,
                                $receiverFirstName,
                                $receiverEmail
                                $ipAddress,
                                $ipCity,
                                $ipState,
                                $ipCountry)
    {
        $connection = connect2DB2($countryAbbreviation);
		
			$query = "INSERT INTO FailedTransactions";
			$query.= "(clientEmail, receiverPhone, receiverFirstName, receiverEmail,";
            $query.= "ipAddress,ipCity,ipState,ipCountry) ";
			$query.= "VALUES('$clientEmail', '$receiverPhone', '$receiverFirstName', ";
			$query.= "'$receiverEmail','$ipAddress','$ipCity','$ipState','$ipCountry')";
			$queryResult=mysql_query($query) or handleDatabaseError(''.mysql_error(),$query);

        disconnectDB($connection);
        
        return $queryResult;
    }
    
    function 
        deleteFailedTransaction($countryAbbreviation,
                                $tableID)
    {
        $connection = connect2DB2($countryAbbreviation);
		
			$query = "DELETE FROM FailedTransactions ";
			$query.= "WHERE id = '$tableID'";
			$queryResult=mysql_query($query) or handleDatabaseError(''.mysql_error(),$query);

		disconnectDB($connection);

        return $queryResult;
    }
    
    function
        modifyFailedTransaction($countryAbbreviation,
                                $id,
                                $modifications)
    {
        $debug = false;
        $updateRequired = false;
		$comments      = $modifications['comments'];
		$transactionID = $modifications['transactionID'];
        
        $connection = connect2DB2($countryAbbreviation);

            $query = "UPDATE FailedTransactions ";
            $query.= "SET ";
            
            if($comments!=""){
                $preparedQuery = "comments = '$comments' "; 
				$updateRequired = true;
            }
            if($transactionID){
                if($updateRequired){ $preparedQuery.= ", ";}
				$preparedQuery.= "transactionID = '$transactionID' "; 
				$updateRequired = true;
            }
            
            if( $updateRequired )
			{
				$query.= $preparedQuery;
				$query.= "WHERE transactionID = '$transactionID'";
				$queryResult= mysql_query($query) or handleDatabaseError(''.mysql_error(),$query);
			}

        disconnectDB($connection);
        
        return $updateRequired;
        
    }
    
    function
        processFailedTransaction($countryAbbreviation,
                                 $id,
                                 $userStatus,
                                 $username)
    {
        //query db for details on this $id
        $connection = connect2DB2($countryAbbreviation);
            $query = "SELECT * FROM FailedTransactions ";
            $query.= "WHERE id = '$id'";
            $queryResult= mysql_query($query) or handleDatabaseError(''.mysql_error(),$query);
        disconnectDB($connection);
        
        return $result;
    }
?>