<?
    //20110126: function to send clients to outgoing queue when cards are finished
    function sendToOutQ($inputArray)
    {
        $valueInLocal      = $inputArray['valueInLocal'];
        $networkName       = $inputArray['networkName'];
        $clientEmail       = $inputArray['payer_email'];
        $clientFirstName   = $inputArray['first_name'];
        $clientLastName    = $inputArray['last_name'];
        $itemID            = $inputArray['itemIDNumber'];
        $transID           = $inputArray['txn_id'];
        $receiverPhone     = $inputArray['order_data']['receiver_phone'];
        $receiverFirstName = $inputArray['order_data']['receiver_first_name'];
        $receiverEmail     = $inputArray['order_data']['receiver_email'];
        $connection        = $inputArray['connection'];
        $cardsArePresent   = $inputArray['cardsArePresent'];
        $reason            = $inputArray['reason'];
        $localCurrency     = LOCAL_CURRENCY;

        $timeCardSent = date("Y-m-d H:i:s");        

        //Send to OutgoingQueue
        $query = "INSERT INTO OutgoingQueue ";
        $query.= "SET ";
        $query.= "transactionID = '$transID',";
        $query.= "clientEmail = '$clientEmail',";
        $query.= "itemID = '$itemID',";
        $query.= "timeStamp = '$timeCardSent',";
        $query.= "clientFirstName = '$clientFirstName',";

        if($receiverPhone == '')
        {
            //It is assumed that the PINSender page will complete the other queue details
            $query.= "dispatchStatus = 'incomplete' ";
        }
        else
        {
            $autoDispatch = isAutoDispatch($itemID);
            
            if($autoDispatch)
            {
                if(isset($inputArray['api_response']['TransactionStatus']))
                {
                    $dispatchStatus = $inputArray['api_response']['TransactionStatus'];
                }
                else
                {
                    $dispatchStatus = "autoready";
                }
            }
            else
            {
                $dispatchStatus = "pinready";
            }
            
            $query.= "dispatchStatus = '$dispatchStatus',";
            $query.= "receiverPhone = '$receiverPhone', ";
            $query.= "receiverEmail = '$receiverEmail', ";
            $query.= "reason = '$reason', ";
            $query.= "receiverFirstName = '$receiverFirstName'";

            if(isset($inputArray['airtime_gw_id']) && strlen($inputArray['airtime_gw_id']) > 1)
            {
                $query.= ", airtime_gw_id = '".$inputArray['airtime_gw_id']."' ";
            }

            if(isset($inputArray['saleType']) && strlen($inputArray['saleType']) > 1)
            {
                $query.= ", saleType = '".$inputArray['saleType']."' ";
            }

            if(isset($inputArray['invoice']) && $inputArray['invoice'] > 0)
            {
                $query.= ", order_id = ".$inputArray['invoice']." ";
            }

        }

        $result = mysql_query($query,$connection) or handleDatabaseError(''. mysql_error(),$query);

        //EMAIL Admin and Client

        if($autoDispatch)
        {
            if(isset($inputArray['api_response']['TransactionStatus']) && 
               $inputArray['api_response']['TransactionStatus'] == 'PENDING')
            {
                //do nothing as client will get a confirmation message when the transaction status is checked
                //by cronjob
            }
            else
            {
                email_client_queued_transfer($inputArray);
            }
        }
        else
        {
            if($inputArray['cardsArePresent'] == "false")
            {
                $reason  = $inputArray['reason'];
                $subject = "$clientEmail Purchase Added to Outgoing Queue";
                $body    = "Hi Admin,\n";
                $body   .= "The following user has been added to the outgoing queue\n";
                $body   .= "Reason: $reason\n";
                $body   .= "Client: $clientFirstName $clientLastName\n";
                $body   .= "Client Email: $clientEmail\n";
                $body   .= "Item ID: $itemID\n";
                $body   .= "Time of Transaction: $timeCardSent\n";

                if($inputArray['reason'] == 'out_of_stock')
                {
                    sendZeroCardsEmail2Client($clientEmail,$recordsEmail,$clientFirstName,$itemID,$transID);
                }

                sendEmail(DOMAIN_NAME,SUPPORT_EMAIL,SUPPORT_EMAIL,CARD_ADMIN,$subject,$body);
            }
            else
            {

            }
        }
        
        return $inputArray;
    }
?>