<?

//201012151652: Created updateReceiverInfo function to reduce clutter in PINSender.php

function updatePromoReceiverInfo($inputArray)
{
    $receiverPhone     = $inputArray['order_data']['receiver_phone'];
    $receiverEmail     = $inputArray['order_data']['receiver_email'];
    $receiverFirstName = $inputArray['order_data']['receiver_first_name'];
    $clientEmail       = $inputArray['payer_email'];
    $connection        = $inputArray['connection'];

    //check to see if receiver is already in table
    $isReceiverInTable ="SELECT * ";
    $isReceiverInTable.="FROM NewReceivers ";
    $isReceiverInTable.="WHERE ";
    $isReceiverInTable.="receiverPhone='$receiverPhone' AND clientEmail='$clientEmail'";
    $isReceiverInResult=mysql_query($isReceiverInTable,$connection) or handleDatabaseError(''. mysql_error(),$isReceiverInTable);
    $isReceiverInNumber=mysql_num_rows($isReceiverInResult);//number of results returned
    $data = mysql_fetch_array($isReceiverInResult);

    //If receiver does not exist
    if($isReceiverInNumber==0)
    {
        //new receivers recorded for marketing purposes
        //its ok to remove this piece of code once marketing done
        $newRxQuery ="INSERT INTO ";
        $newRxQuery.="NewReceivers ";
        $newRxQuery.="SET ";
        $newRxQuery.="receiverPhone = '$receiverPhone', ";
        $newRxQuery.="clientEmail = '$clientEmail' ";
        if($receiverEmail!='') $newRxQuery.= ",receiverEmail = '$receiverEmail' ";
        if($receiverFirstName!='') $newRxQuery.= ",receiverFirstName = '$receiverFirstName' ";
        $result = mysql_query($newRxQuery,$connection) or handleDatabaseError(''. mysql_error(),$newRxQuery);
    }
    else
    {
        //update receivers
        //its ok to remove this piece of code once marketing done
        $updateQuery ="UPDATE ";
        $updateQuery.="NewReceivers ";
        $updateQuery.="SET ";
        $updateQuery.="lastUpdated = CURRENT_TIMESTAMP ";
        $updateQuery.="WHERE ";
        $updateQuery.="clientEmail = '$clientEmail' AND receiverPhone='$receiverPhone'";
        $result = mysql_query($updateQuery,$connection) or handleDatabaseError(''. mysql_error(),$updateQuery);
    }
}
?>