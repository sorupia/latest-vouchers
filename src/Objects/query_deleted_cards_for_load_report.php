<?
/*******************************************************************************
**  FILE: query_deleted_cards_for_load_report.php
**
**  FUNCTION: query_deleted_cards_for_load_report
**
**  PURPOSE: Query the Deleted Cards table if given a number of inputs to be used 
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.10.24
*********************************************************************************/

	function query_deleted_cards_for_load_report($input_array)
	{
        $countryAbbreviation = $input_array['country_abbreviation'];
        $start_date          = $input_array['start_date'];
        $end_date            = $input_array['end_date'];
		$connection          = $input_array['connection'];
        $item_id             = $input_array['itemID'];
        $username            = $input_array['username'];

        $query = "SELECT ";
        $query.= "dateWePurchased, itemID, ";
        $query.= "cardPINDeleted, timeDeleted, reason ";
        $query.= "FROM DeleteLog ";
        $query.= "WHERE timeDeleted > '$start_date' AND timeDeleted < '$end_date' ";

        if($item_id != "")
        {
            $query.= "AND itemID = '$item_id' ";
        }

        $query.= "ORDER BY timeDeleted, cardPINDeleted DESC";
        $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);

		return $result;
	}
?>