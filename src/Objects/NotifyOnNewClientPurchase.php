<?
    function notifyOnNewClientPurchase($input_array)
    {
		$first_name 		 = $input_array['first_name'];
		$last_name 			 = $input_array['last_name'];
		$client_email 		 = $input_array['payer_email'];
		$client_phone 		 = $input_array['contact_phone'];
		$itemIDNumber 		 = $input_array['item_number'];
		$address_state		 = $input_array['address_state'];
		$address_country	 = $input_array['residence_country'];
        $receiver_phone      = $input_array['order_data']['receiver_phone'];
        $receiver_first_name = $input_array['order_data']['receiver_first_name'];
        $ipLocationArray     = $input_array['order_data'];

        $ip_country 	= $ipLocationArray['ip_country'];
        $ip_state 		= $ipLocationArray['ip_state'];
        
        global $newClientNotifyPhones;

        //prepare message
        $notifyMessage ="NEW Cust: $first_name $last_name / Tel: $client_phone /Cnt: $address_country ";
        $notifyMessage.="/ St: $address_state / IPCnt: $ip_country / IPSt: $ip_state / Item: $itemIDNumber ";
        $notifyMessage.="/ Rcv: $receiver_first_name @ $receiver_phone";

        //send to all phones that are to be notified
        for($i=0 ; $i < count($newClientNotifyPhones) ; $i++)
        {
            $notifyPhone = $newClientNotifyPhones[$i];
            
            if(COUNTRY_ABBREVIATION == "gh")
            {
                sendTextUsingInfoBip($notifyPhone,$notifyMessage);
            }
            else
            {
                sendTextUsingClick2($notifyPhone,$notifyMessage);
            }
        }
        
        return $input_array;
    }
?>