<?

/*******************************************************************************
**  FILE: retrieve_from_used_cards.php
**
**  FUNCTION: retrieve_from_used_cards
**
**  PURPOSE: Retrieve data to display for the auto load case
**           This data will mainly be used by DisplayAutoDispatch.php
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 08.Aug.2012
*********************************************************************************/

function retrieve_from_used_cards($input_array)
{

    $transaction_id = $input_array['txn_id'];
    $connection     = $input_array['connection'];
    $itemIDNumber   = $input_array['item_number'];
    $clientEmail    = $input_array['payer_email'];

    $cards_query  = "SELECT * ";
    $cards_query .= "FROM UsedCards ";
    $cards_query .= "WHERE ";
    $cards_query .= "transactionID = '$transaction_id' ";
    $cards_query .= "AND ";
    $cards_query .= "itemID = '$itemIDNumber' ";
    $cards_query .= "AND ";
    $cards_query .= "clientEmail = '$clientEmail' ";
    $cards_result = mysql_query($cards_query,$connection) or handleDatabaseError(''. mysql_error(),$cards_query);
    $cards_data   = mysql_fetch_array($cards_result);
    mysql_free_result($cards_result);

    $input_array['used_cards_data']              = $cards_data;
    $input_array['order_data']['receiver_phone'] = $cards_data['receiverPhone'];

    return $input_array;

}

?>