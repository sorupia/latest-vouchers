<?
    //20110203: Created this function to record sales
    function recordSale($inputArray)
    {
        $connection    = $inputArray["connection"];
        $transID       = $inputArray['txn_id'];
        $clientEmail   = $inputArray['payer_email'];
        $paymentAmount = $inputArray['mc_gross'];
        $clientIP      = $inputArray["clientIP"];
        $receiverEmail = $inputArray['order_data']['receiver_email'];
        $ip_city       = $inputArray['order_data']['ip_city'];
        $ip_state      = $inputArray['order_data']['ip_state'];
        $ip_country    = $inputArray['order_data']['ip_country'];

        $sales_table   = isset($inputArray['sales_table']) ? $inputArray['sales_table'] : "Sales";
        $sales_table   = ($sales_table =="") ? "Sales" : $sales_table;

        $query = "INSERT INTO $sales_table ";
        $query.= "SET ";
        $query.= "transactionID = '$transID', ";
        $query.= "clientEmail = '$clientEmail', ";
        $query.= "amount = $paymentAmount, ";
        $query.= "clientIP = '$clientIP', ";
        $query.= "ip_city = '$ip_city', ";
        $query.= "ip_state = '$ip_state', ";
        $query.= "ip_country = '$ip_country', ";
        $query.= "receiverEmail = '$receiverEmail' ";

        if(isset($inputArray['saleType']) && strlen($inputArray['saleType']) > 1)
        {
            $query.= ", saleType = '".$inputArray['saleType']."' ";
        }

        if(isset($inputArray['saleCurrency']) && strlen($inputArray['saleCurrency']) == 3)
        {
            $query.= ", saleCurrency = '".$inputArray['saleCurrency']."' ";
        }

        $result = mysql_query($query,$connection) or handleDatabaseError(''. mysql_error(),$query);

        return $inputArray;
    }

?>