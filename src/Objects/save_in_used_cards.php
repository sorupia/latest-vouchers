<?

function save_in_used_cards($input_array)
{
    $used_cards_table = isset($input_array['used_cards_table']) ? $input_array['used_cards_table'] : "UsedCards";
    $used_cards_table = ($used_cards_table =="") ? "UsedCards" : $used_cards_table;

    $cardPIN         = $input_array['cardPIN'];
    $networkName     = $input_array['networkName'];
    $valueInLocal    = isset($input_array['valueInLocal']) ? $input_array['valueInLocal'] : 0;
    $ourPriceInUSD   = isset($input_array['ourPriceInUSD']) ? $input_array['ourPriceInUSD'] : 0;
    $dateWePurchased = $input_array['dateWePurchased'];
    $timeCardSent    = $input_array['timeCardSent'];
    $transID         = $input_array['txn_id'];
    $itemIDNumber    = $input_array['item_number'];
    $clientEmail     = $input_array['payer_email'];

    $loadedBy        = $input_array['loadedBy'];
    $loadersIP       = $input_array['loadersIP'];
    $serialNumber    = $input_array['serialNumber'];

    $receiverEmail          = $input_array['order_data']['receiver_email'];
    $receiverPhone          = $input_array['order_data']['receiver_phone'];
    $sms_gw_id              = $input_array['sms_gw_id'];
    $sessionUser            = $input_array['sessionUser'];
    $paymentFee             = isset($input_array['mc_fee']) ? $input_array['mc_fee'] : 0;
    $our_fee                = isset($input_array['our_fee']) ? $input_array['our_fee'] : 0;
    $airtime_processor_fee  = isset($input_array['airtime_processor_fee']) ? $input_array['airtime_processor_fee'] : 0;
    $connection             = $input_array['connection'];

   
    $cards_query = "INSERT IGNORE INTO $used_cards_table ";
    $cards_query.= "SET ";
    $cards_query.= "cardPIN = '$cardPIN', ";
    $cards_query.= "networkName = '$networkName', ";
    $cards_query.= "valueInUGSh = $valueInLocal, ";
    $cards_query.= "ourPriceInUSD = $ourPriceInUSD, ";
    $cards_query.= "dateWePurchased = '$dateWePurchased', ";
    $cards_query.= "dateWeSoldIt = '$timeCardSent', ";
    $cards_query.= "transactionID = '$transID', ";
    $cards_query.= "itemID = '$itemIDNumber', ";
    $cards_query.= "clientEmail = '$clientEmail', ";
    $cards_query.= "loadedBy = '$loadedBy', ";
    $cards_query.= "loadersIP = '$loadersIP', ";
    $cards_query.= "serialNumber = '$serialNumber', ";
    $cards_query.= "receiverEmail='$receiverEmail', ";
    $cards_query.= "receiverPhone='$receiverPhone', ";
    $cards_query.= "smsGWID='$sms_gw_id', ";
    $cards_query.= "sessionUser = '$sessionUser', ";
    $cards_query.= "paymentFee = $paymentFee, ";
    $cards_query.= "our_fee = $our_fee, ";
    $cards_query.= "airtime_processor_fee = $airtime_processor_fee ";

    if(isset($input_array['saleType']) && strlen($input_array['saleType']) > 1)
    {
        $cards_query.= ", saleType = '".$input_array['saleType']."' ";
    }

    if(isset($input_array['airtime_gw_id']) && strlen($input_array['airtime_gw_id']) > 1)
    {
        $cards_query.= ", airtime_gw_id = '".$input_array['airtime_gw_id']."' ";
    }

    if(isset($input_array['invoice']) && $input_array['invoice'] > 0)
    {
        $cards_query.= ", order_id = ".$input_array['invoice']." ";
    }

    if(isset($input_array['api_response']['MNOTransactionReferenceId']) && strlen($input_array['api_response']['MNOTransactionReferenceId']) > 1)
    {
        $cards_query.= ", network_transaction_id = '".$input_array['api_response']['MNOTransactionReferenceId']."' ";
    }

    $insertresult = mysql_query($cards_query,$connection) or handleDatabaseError(''. mysql_error(),$cards_query);

    return $input_array;
}

?>