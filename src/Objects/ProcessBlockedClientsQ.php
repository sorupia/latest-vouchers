<?
    require_once($_SERVER['DOCUMENT_ROOT'].'/src/sendATSimpleAuto.php');

    function processBlockedClientsQ($countryAbbreviation,
                                    $inputArray)
    {
        $id = $inputArray['id'];
        $itemIDNumber = $inputArray['itemIDNumber'];
        $resultArray = array();
        
        $resultArray = sendATSimpleAuto($countryAbbreviation, $inputArray);
        $status = $resultArray['status'];
        $cardPIN = $resultArray['card'];

        /* Delete BlockedClientsQueue Row*/
        if( (!isAutoDispatch($itemIDNumber) && $cardPIN != 0) ||
            ( isAutoDispatch($itemIDNumber) && $cardPIN == "AUTO_LOAD"))
        {
            deleteBlockedClientsQ($countryAbbreviation,$id);
        }
        
        return $resultArray;
    }
?>