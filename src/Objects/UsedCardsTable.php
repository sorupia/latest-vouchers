<?
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/GeneralFunctions.php');

    function queryByDateSold($countryAbbreviation,
							   $start_date,
							   $end_date)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$usedCardsQuery = "SELECT dateWeSoldIt as 'Sale Date',clientEmail as 'Client Email', ";
			$usedCardsQuery.= "cardPIN as 'PIN', itemID as 'Item ID', ourPriceInUSD as 'Price', ";
			$usedCardsQuery.= "receiverPhone as 'Receiver Phone', ";
			$usedCardsQuery.= "transactionID as 'Transaction ID',smsGWID as 'SMS ID' ";
			$usedCardsQuery.= "FROM UsedCards ";
			$usedCardsQuery.= "WHERE dateWeSoldIt > '$start_date' AND dateWeSoldIt < '$end_date' ";
			$usedCardsQuery.= "ORDER BY dateWeSoldIt DESC";
			$usedCardsResult=mysql_query($usedCardsQuery) or handleDatabaseError(''.mysql_error(),$usedCardsQuery);
	
		disconnectDB($connection);
		return $usedCardsResult;
	}

    function queryByDateSold4AsReport($countryAbbreviation,
							   		  $start_date,
							          $end_date)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$usedCardsQuery = "SELECT dateWeSoldIt as 'Sale Date',clientEmail as 'Client Email', ";
			$usedCardsQuery.= "cardPIN as 'PIN', itemID as 'Item ID', ourPriceInUSD as 'Price', ";
			$usedCardsQuery.= "receiverPhone as 'Receiver Phone', ";
			$usedCardsQuery.= "transactionID as 'Transaction ID',smsGWID as 'SMS ID' ";
			$usedCardsQuery.= "FROM UsedCards ";
			$usedCardsQuery.= "WHERE dateWeSoldIt > '$start_date' AND dateWeSoldIt < '$end_date' ";
			$usedCardsQuery.= "AND receiverPhone NOT LIKE '%233245867777%' ";
			$usedCardsQuery.= "AND transactionID NOT LIKE '%jacob%' ";
			$usedCardsQuery.= "ORDER BY dateWeSoldIt DESC";
			$usedCardsResult=mysql_query($usedCardsQuery) or handleDatabaseError(''.mysql_error(),$usedCardsQuery);
	
		disconnectDB($connection);
		return $usedCardsResult;
	}
	
	function queryByDateWePurchased($countryAbbreviation,
							   		$start_date,
							   		$end_date)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$usedCardsQuery = "SELECT dateWePurchased as 'AT Purchase Date',dateWeSoldIt as 'Sale Date',clientEmail as 'Client Email',";
			$usedCardsQuery.= "cardPIN as 'PIN',itemID as 'Item ID',receiverPhone as 'Receiver Phone',transactionID as 'Transaction ID',";
			$usedCardsQuery.= "smsGWID as 'SMS ID'";
			$usedCardsQuery.= "FROM UsedCards ";
			$usedCardsQuery.= "WHERE dateWePurchased > '$start_date' AND dateWePurchased < '$end_date' ";
			$usedCardsQuery.= "ORDER BY dateWePurchased DESC";
			$usedCardsResult=mysql_query($usedCardsQuery) or handleDatabaseError(''.mysql_error(),$usedCardsQuery);
	
		disconnectDB($connection);
		return $usedCardsResult;
	}

	function queryByReceiver($countryAbbreviation,
							 $receiverPhone)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$usedCardsQuery = "SELECT receiverPhone,dateWeSoldIt,clientEmail,cardPIN,itemID,transactionID,smsGWID ";
			$usedCardsQuery.= "FROM UsedCards ";
			$usedCardsQuery.= "WHERE receiverPhone LIKE '%$receiverPhone%' ";
			$usedCardsQuery.= "ORDER BY dateWeSoldIt DESC";
			$usedCardsResult=mysql_query($usedCardsQuery) or handleDatabaseError(''.mysql_error(),$usedCardsQuery);
	
		disconnectDB($connection);
		return $usedCardsResult;
	}	
	
	function queryByEmail($countryAbbreviation,
						  $clientEmail)
	{
		$connection = connect2DB2($countryAbbreviation);
		
			$usedCardsQuery = "SELECT dateWeSoldIt,clientEmail,cardPIN,itemID,receiverPhone,transactionID,smsGWID ";
			$usedCardsQuery.= "FROM UsedCards ";
			$usedCardsQuery.= "WHERE clientEmail = '$clientEmail' ";
			$usedCardsQuery.= "ORDER BY dateWeSoldIt DESC";
			$usedCardsResult=mysql_query($usedCardsQuery) or handleDatabaseError(''.mysql_error(),$usedCardsQuery);
	
		disconnectDB($connection);
		return $usedCardsResult;
	}
	
	function updateUsedCard($countryAbbreviation,
							$cardID,
							$receiverPhone,
							$receiverFirstName,
							$receiverEmail,
							$smsGWID)
	{
    	$fieldToModify = false;
		
		$query = "UPDATE UsedCards SET ";

		if($receiverPhone){
		 	$query.="receiverPhone = '$receiverPhone' ";
			$fieldToModify = true;
		}
		
		//if($receiverFirstName){
    	//	if($fieldToModify) $query.=", ";
		// 	$query.="receiverFirstName = '$receiverFirstName' ";
		//	$fieldToModify = true;
		//}
		
		if($receiverEmail){
    		if($fieldToModify) $query.=", ";
		 	$query.="receiverEmail = '$receiverEmail' ";
			$fieldToModify = true;
		}
		
		if($smsGWID){
    		if($fieldToModify) $query.=", ";
		 	$query.="smsGWID = '$smsGWID' ";
			$fieldToModify = true;
		}
		
		
		if($fieldToModify){
			$query.= "WHERE cardID = '$cardID'";
			
			$connection = connect2DB2($countryAbbreviation);
				$result = mysql_query($query) or handleDatabaseError(''.mysql_error(),$query);
			disconnectDB($connection);
			return $result;
		}
	}

	function deleteUsedCardByCardID($countryAbbreviation,
	                                $cardID)
	{
		$connection = connect2DB2($countryAbbreviation);
			$query = "DELETE FROM UsedCards ";
			$query.= "WHERE cardID = '$cardID'";
			$result = mysql_query($query) or handleDatabaseError(''.mysql_error(),$query);
		disconnectDB($connection);
		return $result;
	}
	
	function deleteUsedCardByCardPIN($countryAbbreviation,
	                                $cardPIN)
	{
		$connection = connect2DB2($countryAbbreviation);
			$query = "DELETE FROM UsedCards ";
			$query.= "WHERE cardPIN = '$cardPIN'";
			$result = mysql_query($query) or handleDatabaseError(''.mysql_error(),$query);
		disconnectDB($connection);
		return $result;
	}

	function displayResults($queryResult)
	{
		// Print the Client data in a HTML table 
		echo "<table class=\"large-tbl\" width=\"90%\"  border=\"0\" >";
		echo"<tr>";
		
		$numOfColumns=mysql_num_fields($queryResult);
		
		for($i=0 ; $i<$numOfColumns ; $i++){
			$columnName=mysql_field_name($queryResult,$i);	
			echo"<td>$columnName</td>";
		}
		
		echo"</tr>";
		$j=0;
		while ($queryData = mysql_fetch_array($queryResult, MYSQL_ASSOC)) {
			$j++;
			if($j%2){
				print "<tr>";
			}
			else{
				print "<tr bgcolor=\"#F0F0F0\">";
			}
			foreach ($queryData as $col_value) {
				print "<td>$col_value</td>";
			}
			print "</tr>";
		}
		print "</table>";		
	}		
?>