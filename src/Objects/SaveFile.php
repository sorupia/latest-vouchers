<?
    //201012061135: Created function to write data to a file

    function saveFile($inputParams)
    {
    
        /* write to temporary file */
        $file_name = $inputParams['file_name'];
        $file_text = $inputParams['file_text'];
        
        $file_handler = @fopen($file_name,'w') or die("failed to open file: $file_name for writing ");			
        $num_of_bytes = @fwrite($file_handler, $file_text) or die("failed to write to $file_name");						
        @fclose($file_handler);
        
        /* initiate save as local file option */
        $ext = strtolower(substr(strrchr($file_name,"."),1));
        
        // required for IE, otherwise Content-disposition is ignored
        if(ini_get('zlib.output_compression'))
        ini_set('zlib.output_compression', 'Off');
        
        if($ext == "php"){

        } 
        else {
            switch($ext)
            {
                case "pdf": $ctype="application/pdf"; break;
                case "exe": $ctype="application/octet-stream"; break;
                case "zip": $ctype="application/zip"; break;
                case "doc": $ctype="application/msword"; break;
                case "xls": $ctype="application/vnd.ms-excel"; break;
                case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
                case "gif": $ctype="image/gif"; break;
                case "png": $ctype="image/png"; break;
                case "jpeg":
                case "jpg": $ctype="image/jpg"; break;
                default: $ctype="application/force-download";
            }
            
            header("Pragma: public"); // required
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false); // required for certain browsers 
            header("Content-Type: $ctype");
            // change, added quotes to allow spaces in filenames, by Rajkumar Singh
            header("Content-Disposition: attachment; filename=\"".basename($file_name)."\";" );
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: ".filesize($file_name));
            readfile("$file_name");
            exit();
        }
    }
?>