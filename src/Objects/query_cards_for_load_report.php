<?
/*******************************************************************************
**  FILE: query_cards_for_load_report.php
**
**  FUNCTION: query_cards_for_load_report
**
**  PURPOSE: Query Used, New and Deleted Cards table for load report 
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.10.29
*********************************************************************************/

function query_cards_for_load_report($input_array)
{
    $use_echo = $input_array['use_echo'];

    /* Initialize array that will be used to contain merged card tables. Rows will be columns and vice versa*/
    $allCardsArray = array();																	

    /* Used Cards */
    $usedCardsResult = query_used_cards_for_load_report($input_array);
    $allCardsArray = organizeUsedCardsResults($usedCardsResult,$allCardsArray);
    $numUsedCards = count($allCardsArray[0]);

    if($use_echo) echo "<br>Used cards: ".$numUsedCards;

    /* New Cards */
    $newCardsResult = query_new_cards_for_load_report($input_array);
    $allCardsArray = organizeNewCardsResults($newCardsResult,$allCardsArray);
    $numNewCards = count($allCardsArray[0]) - $numUsedCards;

    if($use_echo) echo "<br>New cards: ".$numNewCards;						
    
    /*  Deleted Cards */
    $deletedCardsResult = query_deleted_cards_for_load_report($input_array);
    $allCardsArray = organizeDeletedCardsResults($deletedCardsResult,$allCardsArray);
    $numDeletedCards = count($allCardsArray[0]) - $numUsedCards - $numNewCards;

    if($use_echo) echo "<br>Deleted cards: ".$numDeletedCards;

    if(($numUsedCards != 0) || ($numNewCards != 0) || ($numDeletedCards != 0))
    {
        /* Sort the table with a php4+ provided function*/
        array_multisort($allCardsArray[0],SORT_ASC,SORT_STRING,
                        $allCardsArray[1],SORT_ASC,SORT_STRING,
                        $allCardsArray[2],SORT_ASC,SORT_STRING,
                        $allCardsArray[3],SORT_ASC,SORT_STRING,
                        $allCardsArray[4],SORT_ASC,SORT_STRING,
                        $allCardsArray[5],SORT_ASC,SORT_STRING,
                        $allCardsArray[6],SORT_ASC,SORT_STRING,
                        $allCardsArray[7],SORT_ASC,SORT_STRING);
    }
    else
    {
        //echo"<br><br> No records please check dates entered";
    }

    return $allCardsArray;
}
?>