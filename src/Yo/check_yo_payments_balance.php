<?
    function check_yo_payments_balance($input_array)
    {
        $input_array = yo_account_balance($input_array);
        $account_balance = $input_array['api_response']['Balance']['Currency']['Balance'];

        if($account_balance < ($input_array["amount_sent_from_api_account"] + $input_array["api_usage_pct_fee"]))
        {
            $input_array['from']    = SUPPORT_EMAIL;
            $input_array['to']      = TECH_ADMIN;
            $input_array['subject'] = "Yo Payments Balance LOW!";
            $input_array['msg']     = "The Yo Payments Balance is: ".$account_balance;
            send_basic_email($input_array);
        }
        return $input_array;
    }
?>