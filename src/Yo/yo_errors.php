<?
    $error_msg_n_9999 = "You have submitted invalid XML, or one or more fields in the XML request you have submitted is missing or invalid. ";
    $error_msg_n_9999.= "If you get this error code, check the StatusMessage parameter for more detailed information.";

    $error_msg_n_23 = "This transaction has been declined because you have reached or exceeded one or more of your withdrawal limits.";

    $error_msg_n_22 = "This transaction requires extra authorization before it may be completed. Requests for authorization have been duly sent. ";
    $error_msg_n_22.= "Upon successful authorization by all parties, the transaction shall be processed. DO NOT RE-SUBMIT THIS TRANSACTION UNLESS ";
    $error_msg_n_22.= "YOU ARE SURE IT HAS FAILED OR WAS REJECTED. ";

    $error_msg_n_21 = "Your IP address is not permitted to carry out transactions on this account.";

    $error_msg_n_20 = "Your account was CANCELLED. Please contact your account representative for further advice.";

    $error_msg_n_19 = "Your account was TERMINATED. Please contact your account representative for further advice.";

    $error_msg_n_18 = "Your account is SUSPENDED. Please contact your account representative for further advice.";

    $error_msg_n_13 = "You do not have sufficient funds to complete this transaction.";

    $error_msg_n_12 = "There was a problem initiating this transaction. This transaction has failed. Please try again later. ";
    $error_msg_n_12.= "If this problem persists, contact support services.";

    $error_msg_n_11 = "Invalid or Unsupported Currency";

    $error_msg_n_10 = "The transaction has not been processed. This is the default transaction status for a transaction ";
    $error_msg_n_10.= "whose details have been successfully validated but it has not yet been submitted to the transaction processing system.";

    $error_msg_n_9 = "Error committing initial statement entry. The transaction was not processed, and will not appear in the web interface.";

    $error_msg_n_8 = "This is likley a duplicate transaction. Please vary your submission parameters such as references, if this is not in error.";

    $error_msg_n_7 = "Invalid internal reference parameter. If you are sure that the internal reference you have provided is correct then the ";
    $error_msg_n_7.= "transaction was archived, or may have been deleted as a result of its old age.";

    $error_msg_n_6 = "Duplicate transaction code (try again later)";

    $error_msg_n_5 = "The file specified in 'file_narrative' was not found";

    $error_msg_n_4 = "Invalid 'amount' parameter";

    $error_msg_n_3 = "Unsupported transaction type parameter";

    $error_msg_n_2 = "Unsupported MSISDN Network or Currency";

    $error_msg_n_1 = "An internal error occurred. More information in 'ErrorMessage'";

    $error_msg_0 = "The Transaction was Successful";

    $error_msg_p_1 = "The transaction has been successfully recorded but is pending at the low-level.";

    $error_msg_p_2 = "The transaction failed -- see 'ErrorMessage' for more information.";

    $error_msg_p_3 = "The transaction failed but we encountered an error updating the transaction ";
    $error_msg_p_3.= "state to mark the transaction as FAILED. The transaction is still in the INDETERMINATE ";
    $error_msg_p_3.= "state and will need to be manually marked as FAILED, and for a completion time to be added. ";
    $error_msg_p_3.= "If you get this error code, consider your transaction as FAILED. The transaction will state ";
    $error_msg_p_3.= "will be updated to FAILED within 24 hours. Contact support services if the transaction state ";
    $error_msg_p_3.= "is not yet updated within 24 hours of your receiving this message.";

    $error_msg_p_4 = "The transaction succeeded but we encountered an error updating the transaction state to mark the ";
    $error_msg_p_4.= "transaction as successful. The transaction is still in the INDETERMINATE state and will need to be ";
    $error_msg_p_4.= "manually marked as SUCCEEDED, and for a completion time to be added. Your account may or may not have ";
    $error_msg_p_4.= "been credited with the funds. Check your statement on the online interface to verify whether your account ";
    $error_msg_p_4.= "was credited or not. If you get this error code, consider your transaction SUCCEEDED but ensure to contact ";
    $error_msg_p_4.= "support services if your account statement is not updated within 24 hours.";

    $error_msg_p_5 = "The transaction was rendered indeterminate in as far as our interaction with the network goes, however ";
    $error_msg_p_5.= "we failed to update the transaction state to add a completion date/time. The transaction will need to be ";
    $error_msg_p_5.= "manually completed and changed to FAILED or SUCCEEDED based on investigations with the mobile network. ";
    $error_msg_p_5.= "If you get this error code, contact support services if the transaction state is not definitively ";
    $error_msg_p_5.= "resolved within 1 hour. Your transaction may have succeeded or may have failed.";

    $error_msg_p_6 = "The transaction succeeded. However, because of an internal problem, your balance has not yet been updated ";
    $error_msg_p_6.= "to reflect the transaction. You shall notice that this transaction is still in the INDETERMINATE state. ";
    $error_msg_p_6.= "We shall mark the transaction as SUCCEEDED shortly and update your account balance. If you get this error code, ";
    $error_msg_p_6.= "consider your transaction successful. Contact support services if your account statement / balance is not ";
    $error_msg_p_6.= "updated to reflect this transaction within 24 hours.";

    $error_msg_p_7 = "Unsupported transaction type ''.The transaction was not processed and will appear in the web interface as a ";
    $error_msg_p_7.= "FAILED transaction. If you get this error code, consider your transaction as FAILED.";

    $error_msg_p_8 = "Unsupported transaction type ''.processed but there was a problem marking the transaction as FAILED. ";
    $error_msg_p_8.= "The transaction will be manually marked as failed shortly. If this transaction is still in the INDETERMINATE ";
    $error_msg_p_8.= "state within 24 hours of receiving this message, please contact support services. If you get this error code, ";
    $error_msg_p_8.= "consider your transaction as FAILED.";

    $error_msg_p_9 = "Indeterminate Transaction. The response we got from the mobile network mobile money system was inconclusive. ";
    $error_msg_p_9.= "It is not clear whether this transaction succeeded or failed. This situation shall be resolved within 1 hour ";
    $error_msg_p_9.= "and your transaction shall be moved to either the SUCCEEDED state or FAILED state, depending on the information ";
    $error_msg_p_9.= "from the mobile network mobile money system. If this situation is unresolved within 1 hour of your receipt of ";
    $error_msg_p_9.= "this message, kindly contact support services.";

    $error_msg_p_10 = "The transaction failed. There was an error contacting the internal processing gateway. Please try your transaction ";
    $error_msg_p_10.= "again later, or contact support services if this error persists.";

    $error_msg_p_11 = "The transaction failed. There was an error contacting the internal processing gateway. However, there was, also, a ";
    $error_msg_p_11.= "problem marking this transaction as FAILED. Therefore, you shall notice that this transaction is in the INDETERMINATE ";
    $error_msg_p_11.= "state. This shall be resolved within 1 hour of receiving this message. If you get this error code, your transaction ";
    $error_msg_p_11.= "failed. Contact support services if this transaction remains in the INDETERMINATE state for more than 1 hour or if ";
    $error_msg_p_11.= "this error persists.";

    $error_msg_p_12 = "Indeterminate Transaction. There was an error communicating with the internal processing gateway. Therefore, ";
    $error_msg_p_12.= "you shall notice that this transaction is in the INDETERMINATE state. This shall be resolved within 1 hour ";
    $error_msg_p_12.= "of your receiving this message. If you get this error code, your transaction may have succeeded or may have ";
    $error_msg_p_12.= "failed. Contact support services if this transaction remains in the INDETERMINATE state for more than 1 hour ";
    $error_msg_p_12.= "of if this error persists.";

    $error_msg_p_13 = "Indeterminate Transaction. There was an error communicating with the internal processing gateway. However, ";
    $error_msg_p_13.= "there was, also, a problem completing this transaction, therefore you shall notice that there is no completion ";
    $error_msg_p_13.= "date for this transaction. This situation shall be resolved within 1 hour of your receiving this message. If you ";
    $error_msg_p_13.= "get this error code, your transaction may have succeeded or may have failed. Contact support services if this ";
    $error_msg_p_13.= "transaction remains in the INDETERMINATE state for more than 1 hour of if this error persists.";

    $error_msg_p_14 = "The OUTBOUND_MSISDN_DEBIT transation failed. Your account balance is unaffected. See 'ErrorMessage' ";
    $error_msg_p_14.= "for more information on why the transaction failed.";

    $error_msg_p_15 = "The OUTBOUND_MSISDN_DEBIT transaction failed. However, there was a problem restoring your account balance. ";
    $error_msg_p_15.= "Therefore, this transaction is still in the INDETERMINATE state. Your account balance will be restored within ";
    $error_msg_p_15.= "1 hour of your receiving this message. If your account balance is still unrestored after 1 hour, contact support ";
    $error_msg_p_15.= "services and provide this reference code: ''";

    $error_msg_p_16 = "The OUTBOUND_MSISDN_DEBIT transaction could not be completed because of a problem posting the initial ";
    $error_msg_p_16.= "transaction statement. Therefore, this transaction is in the INDETERMINATE state. Your account balance ";
    $error_msg_p_16.= "will be restored within 1 hour of your receiving this message. If your account balance is still unrestored ";
    $error_msg_p_16.= "after 1 hour, contact support services and provide this reference code: ''. Please note that while your web ";
    $error_msg_p_16.= "interface will indicate SUCCEEDED, the transaction did not completely succeed.";

    $error_msg_p_17 = "The OUTBOUND_MSISDN_DEBIT succeeded, however you need to manually do the following: (a) Mark the transaction with ";
    $error_msg_p_17.= "code '' in the Suspense Account '' as SUCCEEDED; and (b) Debit the network tracking account '' for network '' with ";
    $error_msg_p_17.= "the amount: '";

    $error_msg_p_18 = "The transaction failed. There was an error contacting the internal processing gateway. Please try your ";
    $error_msg_p_18.= "transaction again later, or contact support services if this error persists.";

    $error_msg_p_19 = "The transaction failed. There was an error contacting the internal processing gateway. However, there was, ";
    $error_msg_p_19.= "also, a problem marking this transaction as FAILED. Therefore, you shall notice that this transaction is in ";
    $error_msg_p_19.= "the INDETERMINATE state. This shall be resolved within 1 hour of receiving this message. If you get this ";
    $error_msg_p_19.= "error code, your transaction failed. Contact support services if this transaction remains in the INDETERMINATE ";
    $error_msg_p_19.= "state for more than 1 hour or if this error persists. If you contact support services, provide this reference code: ''.";

    $error_msg_p_20 = "Indeterminate Transaction. There was an error communicating with the internal processing gateway. Therefore, ";
    $error_msg_p_20.= "you shall notice that this transaction is in the INDETERMINATE state. This shall be resolved within 1 hour of ";
    $error_msg_p_20.= "your receiving this message. If you get this error code, your transaction may have succeeded or may have failed. ";
    $error_msg_p_20.= "Contact support services if this transaction remains in the INDETERMINATE state for more than 1 hour of if this ";
    $error_msg_p_20.= "error persists. If you contact support services, provide this reference code: ''.";

    $error_msg_p_21 = "Indeterminate Transaction. There was an error communicating with the internal processing gateway. However, ";
    $error_msg_p_21.= "there was, also, a problem completing this transaction, therefore you shall notice that there is no completion ";
    $error_msg_p_21.= "date for this transaction. This situation shall be resolved within 1 hour of your receiving this message. ";
    $error_msg_p_21.= "If you get this error code, your transaction may have succeeded or may have failed. Contact support services ";
    $error_msg_p_21.= "if this transaction remains in the INDETERMINATE state for more than 1 hour of if this error persists. If you ";
    $error_msg_p_21.= "contact support services, provide this reference code: ''.";

    $error_msg_p_22 = "The transaction failed but we encountered an error updating the transaction state to mark the transaction as ";
    $error_msg_p_22.= "FAILED. The transaction is still in the INDETERMINATE state and will need to be manually marked as FAILED, and ";
    $error_msg_p_22.= "for a completion time to be added. If you get this error code, consider your transaction as FAILED. The transaction ";
    $error_msg_p_22.= "will state will be updated to FAILED within 24 hours. Contact support services if the transaction state is not ";
    $error_msg_p_22.= "yet updated within 24 hours of your receiving this message. If you contact support services, provide this reference code: ''.";

    $error_msg_p_23 = "The transaction was rendered indeterminate in as far as our interaction with the network goes, however we failed ";
    $error_msg_p_23.= "to update the transaction state to add a completion date/time. The transaction will need to be manually completed ";
    $error_msg_p_23.= "and changed to FAILED or SUCCEEDED based on investigations with the mobile network. If you get this error code, ";
    $error_msg_p_23.= "contact support services if the transaction state is not definitively resolved within 1 hour. Your transaction may ";
    $error_msg_p_23.= "have succeeded or may have failed. If you contact support services, provide this reference code: ''.";

    $error_msg_p_24 = "The transaction failed -- see 'ErrorMessage' for more information. Your balance remains unaffected.";

    $error_msg_p_25 = "Indeterminate Transaction. The response we got from the mobile network mobile money system was inconclusive. ";
    $error_msg_p_25.= "It is not clear whether this transaction succeeded or failed. This situation shall be resolved within 1 hour ";
    $error_msg_p_25.= "and your transaction shall be moved to either the SUCCEEDED state or FAILED state, depending on the information ";
    $error_msg_p_25.= "from the mobile network mobile money system. If this situation is unresolved within 1 hour of your receipt of ";
    $error_msg_p_25.= "this message, kindly contact support services. If you contact support services, provide this reference code: ''.";
    
    $error_msg_p_26 = "The transaction was declined by the user.";
    
    $yo_errors_array = array(-9999=>$error_msg_n_9999,
                             -23=>$error_msg_n_23,
                             -22=>$error_msg_n_22,
                             -21=>$error_msg_n_21,
                             -20=>$error_msg_n_20,
                             -19=>$error_msg_n_19,
                             -18=>$error_msg_n_18,
                             -13=>$error_msg_n_13,
                             -12=>$error_msg_n_12,
                             -11=>$error_msg_n_11,
                             -10=>$error_msg_n_10,
                             -9=>$error_msg_n_9,
                             -8=>$error_msg_n_8,
                             -7=>$error_msg_n_7,
                             -6=>$error_msg_n_6,
                             -5=>$error_msg_n_5,
                             -4=>$error_msg_n_4,
                             -3=>$error_msg_n_3,
                             -2=>$error_msg_n_2,
                             -1=>$error_msg_n_1,
                              0=>$error_msg_0,
                             1=>$error_msg_p_1,
                             2=>$error_msg_p_2,
                             3=>$error_msg_p_3,
                             4=>$error_msg_p_4,
                             5=>$error_msg_p_5,
                             6=>$error_msg_p_6,
                             7=>$error_msg_p_7,
                             8=>$error_msg_p_8,
                             9=>$error_msg_p_9,
                             10=>$error_msg_p_10,
                             11=>$error_msg_p_11,
                             12=>$error_msg_p_12,
                             13=>$error_msg_p_13,
                             14=>$error_msg_p_14,
                             15=>$error_msg_p_15,
                             16=>$error_msg_p_16,
                             17=>$error_msg_p_17,
                             18=>$error_msg_p_18,
                             19=>$error_msg_p_19,
                             20=>$error_msg_p_20,
                             21=>$error_msg_p_21,
                             22=>$error_msg_p_22,
                             23=>$error_msg_p_23,
                             24=>$error_msg_p_24,
                             25=>$error_msg_p_25,
                             26=>$error_msg_p_26);
?>