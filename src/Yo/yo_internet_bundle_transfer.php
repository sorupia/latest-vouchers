<?
    function yo_internet_bundle_transfer($input_array)
    {
        global $yo_provider_name_array;
        $yo_username                  = YO_USERNAME;
        $yo_password                  = YO_PASSWORD;
        $client_email                 = $input_array['payer_email'];
        $receiver_amount_in_local     = $input_array['order_data']['receiver_amount_in_local'];
        $receiver_phone               = $input_array['order_data']['receiver_phone'];
        $receiver_network_id          = $input_array['order_data']['receiver_network_id'];
        $acc_provider_code            = $yo_provider_name_array["$receiver_network_id"]["account_provider_code"];
        $narrative                    = PRODUCT." from $client_email to $receiver_phone";
        $transaction_id               = $input_array['txn_id'];
        $receiver_account             = substr($receiver_phone,1);

        $page = check_me();

        $is_blocking_request = $yo_provider_name_array["$receiver_network_id"]["is_blocking_request"];

        if($is_blocking_request)
        {
            $blocking_option = "<NonBlocking>FALSE</NonBlocking>";
        }
        else
        {
            $blocking_option = "<NonBlocking>TRUE</NonBlocking>";
        }

        $request = "<?xml version='1.0' encoding='UTF-8'?>";
        $request.= "<AutoCreate>";
        $request.= "<Request>";
        $request.= "<APIUsername>$yo_username</APIUsername>";
        $request.= "<APIPassword>$yo_password</APIPassword>";
        $request.= "<Method>acsendairtimemobile</Method>";
        $request.= "<Amount>$receiver_amount_in_local</Amount>";
        $request.= "<Account>$receiver_account</Account>";
        $request.= "<AccountProviderCode>$acc_provider_code</AccountProviderCode>";
        $request.= "<Narrative>$narrative</Narrative>";
        $request.= "<ExternalReference>$transaction_id</ExternalReference>";
        $request.= "<InternetBundle>TRUE</InternetBundle>";
        $request.= $blocking_option;
        $request.= "</Request>";
        $request.= "</AutoCreate>";

        $header = "POST ".YO_API_POST_PATH." HTTP/1.0\r\n";
        $header.= "Content-Type: text/xml\r\n"; 
        $header.= "Content-transfer-encoding: text\r\n"; 
        $header.= "Content-Length: " . strlen($request) . "\r\n\r\n";

        if(DEBUG_API_REQUEST)
        {
            $from    = "From: ".SUPPORT_EMAIL;
            $to      = TECH_ADMIN;
            $subject = "API_REQUEST DEBUG ". $page;
            mail($to,$subject,$header.$request,$from);
        }

        $fp = fsockopen (YO_LIVE, YO_LIVE_PORT, $errno, $errstr, 30);

        if (!$fp)
        {
            $input_array['invoke_success'] = 0;
        }
        else
        {
            fputs ($fp, $header . $request);
            $xml_response = '';
            $headerdone = false;

            while (!feof($fp))
            {
                //read till we get the header
                $line = fgets ($fp, 1024);

                if (strcmp($line, "\r\n") == 0)
                {
                    // read the header
                    $headerdone = true;
                }
                else if ($headerdone)
                {
                    // header has been read. now read the contents
                    $xml_response .= $line;
                }
            }

            fclose ($fp);

            $xmlObj = simplexml_load_string($xml_response);
            $arrXml = objectsIntoArray($xmlObj);

            if(isset($arrXml['Response']))
            {
                $input_array['api_response'] = $arrXml['Response'];
                $success = $input_array['api_response']['StatusCode'];
                $input_array['invoke_success'] = ($success == 0)? 1 : 0;
            }
            else
            {
                $input_array['invoke_success'] = 0;
                $input_array['error'].= "Issue with XML string returned: ".$xml_response."\n";
            }

            if($input_array['invoke_success'])
            {
                //email_ipn_post($input_array['api_response']);
            }
            else
            {
                email_ipn_post($input_array['api_response']);
                $input_array['error'].=$input_array['api_response']['ErrorMessage'];
            }

            if(DEBUG_API_RESPONSE)
            {
                $from    = "From: ".SUPPORT_EMAIL;
                $to      = TECH_ADMIN;
                $subject = "API_RESPONSE DEBUG ". $page;
                mail($to,$subject,$xml_response,$from);
                sendEmail(FROM_NAME,FROM_EMAIL,TECH_ADMIN,$bcc,$subject,$xml_response);
            }
        }

        return $input_array;
    }
?>