<?
    function yo_account_tx_check_status($input_array)
    {
        global $yo_provider_name_array;
        $yo_username                  = YO_USERNAME;
        $yo_password                  = YO_PASSWORD;
        $unique_transaction_id        = $input_array['unique_transaction_id'];
        $xml_response                 = '';
        $success                      =true;

        $page = check_me();

        $request = "<?xml version='1.0' encoding='UTF-8'?>
                    <AutoCreate>
                        <Request>
                            <APIUsername>$yo_username</APIUsername>
                            <APIPassword>$yo_password</APIPassword>
                            <Method>actransactioncheckstatus</Method>
                            <TransactionReference>$unique_transaction_id</TransactionReference>
                            <DepositTransactionType></DepositTransactionType>
                        </Request>
                    </AutoCreate>";

        $header = "POST ".YO_API_POST_PATH." HTTP/1.0\r\n";
        $header.= "Content-Type: text/xml\r\n"; 
        $header.= "Content-transfer-encoding: text\r\n"; 
        $header.= "Content-Length: " . strlen($request) . "\r\n\r\n";

        if(DEBUG_API_REQUEST)
        {
            $from    = "From: ".SUPPORT_EMAIL;
            $to      = TECH_ADMIN;
            $subject = "API_REQUEST DEBUG ". $page;
            mail($to,$subject,$header.$request,$from);
        }

        $fp = fsockopen (YO_LIVE, YO_LIVE_PORT, $errno, $errstr, 30);

        if (!$fp)
        {
            $input_array['invoke_success'] = 0;
            $success = false;
        }
        else
        {
            fputs ($fp, $header . $request);
            $xml_response = '';
            $headerdone = false;

            while (!feof($fp))
            {
                //read till we get the header
                $line = fgets ($fp, 1024);

                if (strcmp($line, "\r\n") == 0)
                {
                    // read the header
                    $headerdone = true;
                }
                else if ($headerdone)
                {
                    // header has been read. now read the contents
                    $xml_response .= $line;
                }
            }

            fclose ($fp);

            $xmlObj = simplexml_load_string($xml_response);
            $arrXml = objectsIntoArray($xmlObj);

            $input_array['api_response'] = $arrXml['Response'];
            $success = $input_array['api_response']['StatusCode'];
            $input_array['invoke_success'] = ($success == 0)? 1 : 0;

            if($input_array['invoke_success'])
            {
                //email_ipn_post($input_array['api_response']);
            }
            else
            {
                email_ipn_post($input_array['api_response']);
            }

            if(DEBUG_API_RESPONSE)
            {
                $from    = "From: ".SUPPORT_EMAIL;
                $to      = TECH_ADMIN;
                $subject = "API_RESPONSE DEBUG ". $page;
                mail($to,$subject,$xml_response,$from);
            }
        }

        return $input_array;
    }
?>