<?
    function yo_transfer($input_array)
    {
        global $yo_provider_name_array;
        $yo_username                  = YO_USERNAME;
        $yo_password                  = YO_PASSWORD;
        $client_email                 = $input_array['payer_email'];
        $amount_sent_from_api_account = $input_array['cart_data']['amount_sent_from_api_account'];
        $receiver_phone               = $input_array['cart_data']['receiver_phone'];
        $receiver_network_id          = $input_array['cart_data']['receiver_network_id'];
        $acc_provider_code            = $yo_provider_name_array["$receiver_network_id"]['account_provider_code'];
        $narrative                    = PRODUCT." from $client_email to $receiver_phone";
        $transaction_id               = $input_array['txn_id'];
        $receiver_account             = substr($receiver_phone,1);
        
        $request = "<?xml version='1.0' encoding='UTF-8'?>
                    <AutoCreate>
                        <Request>
                            <APIUsername>$yo_username</APIUsername>
                            <APIPassword>$yo_password</APIPassword>
                            <Method>acwithdrawfunds</Method>
                            <Amount>$amount_sent_from_api_account</Amount>
                            <Account>$receiver_account</Account>
                            <AccountProviderCode>$acc_provider_code</AccountProviderCode>
                            <Narrative>$narrative</Narrative>
                            <ExternalReference>$transaction_id</ExternalReference>
                        </Request>
                    </AutoCreate>";

        $header = "POST ".YO_API_POST_PATH." HTTP/1.0\r\n";
        $header.= "Content-Type: text/xml\r\n"; 
        $header.= "Content-transfer-encoding: text\r\n"; 
        $header.= "Content-Length: " . strlen($request) . "\r\n\r\n";

        if(DEBUG_API_REQUEST)
        {
            $from    = "From: support@sendmomo.com";
            $to      = TECH_ADMIN;
            $subject = "API DEBUG";
            mail($to,$subject,$header.$request,$from);
        }

        $fp = fsockopen (YO_LIVE, YO_LIVE_PORT, $errno, $errstr, 30);

        if (!$fp)
        {
            $input_array['invoke_success'] = 0;
        }
        else
        {
            fputs ($fp, $header . $request);
            $xml_response = '';
            $headerdone = false;

            while (!feof($fp))
            {
                //read till we get the header
                $line = fgets ($fp, 1024);

                if (strcmp($line, "\r\n") == 0)
                {
                    // read the header
                    $headerdone = true;
                }
                else if ($headerdone)
                {
                    // header has been read. now read the contents
                    $xml_response .= $line;
                }
            }

            fclose ($fp);

            $xmlObj = simplexml_load_string($xml_response);
            $arrXml = objectsIntoArray($xmlObj);

            $input_array['api_response'] = $arrXml['Response'];
            $success = $input_array['api_response']['StatusCode'];
            $input_array['invoke_success'] = ($success == 0)? 1 : 0;
            
            if($input_array['invoke_success'])
            {
                email_ipn_post($input_array['api_response']);
            }
            else
            {
                email_ipn_post($input_array['api_response']);
                $input_array['error'].=$input_array['api_response']['ErrorMessage'];
            }

            if(DEBUG_API_RESPONSE)
            {
                $from    = "From: support@sendmomo.com";
                $to      = TECH_ADMIN;
                $subject = "API DEBUG";
                mail($to,$subject,$xml_response,$from);
            }
        }

        return $input_array;
    }
?>