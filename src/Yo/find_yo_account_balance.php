<?

function find_yo_account_balance($input_array)
{
    global $yo_provider_name_array;

    $network_id            = $input_array['network_id'];
    $currency_code_type    = $input_array['currency_code_type'];
    $currency_code         = $yo_provider_name_array["$network_id"]["$currency_code_type"];
    $account_balance       = 0;
    $account_balance_index = 0;

    for($i = 0; $i < count($input_array['api_response']['Balance']['Currency']) ; $i++)
    {
        $this_currency_code = $input_array['api_response']['Balance']['Currency'][$i]['Code'];

        if($currency_code == $this_currency_code)
        {
            $account_balance_index = $i;
            $account_balance = $input_array['api_response']['Balance']['Currency'][$i]['Balance'];
            break;
        }
    }

    $input_array['found_yo_account_balance']       = $account_balance;
    $input_array['found_yo_account_balance_index'] = $account_balance_index;
    return $input_array;
}

?>