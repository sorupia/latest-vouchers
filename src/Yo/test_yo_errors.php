<?
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Messaging/send_basic_email.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Yo/handle_yo_error_codes.php";

    error_reporting(E_ALL);

    //set error handler
    set_error_handler("custom_error");

    //test Yo errors
    $value = -9999;
    $input_array['msg']     = handle_yo_error_codes($value);
    $input_array['from']    = "support@sendmomo.com";
    $input_array['to']      = "antozi@sendairtime.com";
    $input_array['subject'] = "Yo Error";
    send_basic_email($input_array);
    
?>