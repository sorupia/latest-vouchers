<?php
	
	//include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
	function trackSubscriberExpiry($thisPurchase,$clientEmail,$countryAbbreviation){
		//$countryAbbreviation="ug";
		//$thisPurchase=14;
		//$clientEmail="antozi@gmai.com";
		
		//obtain amount required to be a max discounted subscriber
		global $subscriptionAmountArray;
		$subscriptionAmount=$subscriptionAmountArray[$countryAbbreviation];
		if($subscriptionAmount==""){$subscriptionAmount=$subscriptionAmountArray["default"];}
		
		
		//check to see if this client signup to be a subscriber and obtain subscription details
		$trackSubscriberQuery="SELECT clientEmail, totalSinceIncrement, dateOfExpiry, lastDayOfIncr ";
		$trackSubscriberQuery.="FROM Subscribers WHERE clientEmail='$clientEmail'";
		$trackSubscriberData=executeQuery($trackSubscriberQuery,$countryAbbreviation);
		if($debug){echo"$trackSubscriberQuery <br>";}
		$email=$trackSubscriberData['clientEmail'];
		if($inTesting){echo"email: $email <br>";}
		
		if($email!=""){//it means that this user is in the subscriber table
			$totalSinceIncrement=$trackSubscriberData['totalSinceIncrement'];
			$dateOfExpiry= $trackSubscriberData['dateOfExpiry'];
			$lastDayOfIncr= $trackSubscriberData['lastDayOfIncr'];
			
			$today = date("Ymd");//input date in the format 20061123
			
			//include todays purchase
			$totalSinceIncrement+=$thisPurchase;
			
			if($inTesting){echo"$totalSinceIncrement, $dateOfExpiry, $lastDayOfIncr <br>";}
			if(incrementDateByAMonth($lastDayOfIncr)>=$today){//the last day we incremented the expiry date is less than a month ago
				//hence we can go ahead and use the accumulating amount in totalSinceIncrement
				if($totalSinceIncrement>=$subscriptionAmount){
					if($today>$dateOfExpiry){//today is more recent than dateOfExpiry
						$dateOfExpiry=incrementDateByAMonth($today);
						if($debug){echo"Expiry Date less than today: $dateOfExpiry <br>";}
					}
					else{
						$dateOfExpiry=incrementDateByAMonth($dateOfExpiry);
						if($inTesting){echo"Expiry Date greater than or equal to today: $dateOfExpiry <br>";}
					}
					
					$lastDayOfIncr = $today;
					$totalSinceIncrement-=$subscriptionAmount;
					$subscriberUpdateQuery="UPDATE Subscribers SET lastDayOfIncr=$lastDayOfIncr, ";
					$subscriberUpdateQuery.="totalSinceIncrement=$totalSinceIncrement, ";
					$subscriberUpdateQuery.="dateOfExpiry=$dateOfExpiry ";
					$subscriberUpdateQuery.="WHERE clientEmail='$clientEmail'";
					$subscriberUpdateData = executeQuery($subscriberUpdateQuery,$countryAbbreviation);
				}
				else{
					$subscriberUpdateQuery="UPDATE Subscribers SET totalSinceIncrement=$totalSinceIncrement ";
					$subscriberUpdateQuery.="WHERE clientEmail='$clientEmail'";
					$subscriberUpdateData = executeQuery($subscriberUpdateQuery,$countryAbbreviation);
					if($inTesting){echo"Incremented totalSince: $totalSinceIncrement<br>";}
				}
			}
			else{//the last day we incremented the expiry date is more than a month ago and hence we have to reset
				//the amount that has been accumulating to todays purchase amount
				$totalSinceIncrement=$thisPurchase;//reset accumulating amount
				$lastDayOfIncr = $today;//set today as the last day of increment so that the next time we can accumulate the amount with above code
				$subscriberUpdateQuery="UPDATE Subscribers SET totalSinceIncrement=$totalSinceIncrement, ";
				$subscriberUpdateQuery.="lastDayOfIncr=$lastDayOfIncr ";
				$subscriberUpdateQuery.="WHERE clientEmail='$clientEmail'";
				$subscriberUpdateData = executeQuery($subscriberUpdateQuery,$countryAbbreviation);
				if($inTesting){echo"Incremented totalSince: $totalSinceIncrement<br>";}
			}
		}
		else{//this user is not a subscriber, do nothing
			if($inTesting){echo"this user is not a subscriber<br>";}
		}
	}
?>