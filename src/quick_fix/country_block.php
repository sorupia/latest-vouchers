<?
include_once $_SERVER['DOCUMENT_ROOT']."/src/compare_country_info.php";

$country_status = compare_ISO_to_cname($ip_country,$clientCountry) ||
				  $ip_country == "" || $clientCountry == "";

if( !$country_status )
{
    //There has been a country mismatch
    $status="Blocked";
    
    //contact db and block client
    $blockQuery = "UPDATE cust_table SET status = '$status' WHERE clientEmail='$clientEmail'";
    mysql_query($blockQuery) or handleDatabaseError('' . mysql_error(),$blockQuery);

    // email to support to be parsed to receivers_info.php
    $from    = "From: admin@ghanaairtime.com";
    $to      = "support@ghanaairtime.com";        
    $subject = "COUNTRY / BLOCKED / $clientEmail / $firstname $lastname / ";
    $subject.= "$clientPhone / $clientCountry / $ip_country";
    $msg     = "A client has been blocked due to a country mismatch \n\n";

    //email client
    $cfrom    = "From: support@ghanaairtime.com";
    $cto      = $clientEmail;
    $csubject = "GhanaAirTime.com Order";
    $cmsg     = "Dear $firstname, \n";
    $cmsg    .= "Thank you for your order with Ghanaairtime.com. ";
    $cmsg    .= "Your order will require manual processing. Thanks for your patience";
    @mail($cto,$csubject,$cmsg,$cfrom);
    ?>

    <form name="recieve" action="recievers_info.php" method="POST">
        <input type="hidden" name="fbs" value="fbs"/>
        <input type="hidden" name="flag" value="0"/>
        <input type="hidden" name="blocked" value="true"/>
        <input type="hidden" name="note" value="Thanks for your order. We cannot process your request at this time someone from Ghanaairtime will contact you soon after you fill out the form below to verify your order"/>
        <input type="hidden" name="to" value="<?php echo $to; ?>" />
        <input type="hidden" name="from" value="<?php echo $from; ?>" />
        <input type="hidden" name="subject" value="<?php echo $subject; ?>" />
        <input type="hidden" name="msg" value="<?php echo $msg; ?>" />
        <input type="hidden" name="error" value="Thanks for your order\nWe cannot process your request at this time\nSomeone from Ghanaairtime will contact you soon" />
    </form>

    <script>
        document.recieve.submit();
    </script>

    <?php

    exit();
}
 ?>