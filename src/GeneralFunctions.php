<?php
/*******************************************************************************
**  FILE: GeneralFunctions.php
**
**  FUNCTION: Several
**
**  PURPOSE: Hold several functions that are for general use
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Montreal)   DATE: 2006.07.15
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2010.12.01
**  201012011454 EAST added mysql_free_result($result) to executeQuery function
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2015.02.13
**  Added validation of the IP address to logLogin function
**
*********************************************************************************/

	//GeneralFunctions.php
    //
	include_once $_SERVER['DOCUMENT_ROOT']."/src/constants.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/sms/Messenger.php";
	
    $dbgFh = '';
    
    function dbgOpen($fname) {
        global $dbgFh;
        
        $dbgFh = fopen($fname, 'w');
    }
    
    function dbgWrite($txt) {
        global $dbgFh;
        
        $ts = date('Y-m-d H:i:s');
        
        $txt = $ts . ' ' . $txt;
        if ($dbgFh) {        
            if (1) {
                echo $txt;            
            }
            fwrite($dbgFh, $txt);
        }        
    }
    
    function dbgClose() {
        global $dbgFh;
        
        if ($dbgFh) {
            fclose($dbgFh);
        }
    }
    
    function validate_ip($ip)
    {
        if (filter_var($ip, FILTER_VALIDATE_IP, 
                       FILTER_FLAG_IPV4 | 
                       FILTER_FLAG_IPV6 |
                       FILTER_FLAG_NO_PRIV_RANGE | 
                       FILTER_FLAG_NO_RES_RANGE) === false)
         return false;

        return true;
    }

	//function to get the IP address of the user.
	function getIP()
    {
		$ip = "0.0.0.0";
        // check for shared internet/ISP IP
		if (getenv("HTTP_CLIENT_IP"))
        {
            $ip = getenv("HTTP_CLIENT_IP");
        }
        // check for IPs passing through proxies
		else if(getenv("HTTP_X_FORWARDED_FOR"))
        {
            //$ip = getenv("HTTP_X_FORWARDED_FOR");
            // check if multiple ips exist in var
            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($iplist as $ip)
            {
             //if (validate_ip($ip))
              return $ip;
            }
        }
        // return unreliable ip since all else failed
		else if(getenv("REMOTE_ADDR"))
        {
            $ip = getenv("REMOTE_ADDR");
        }
		else
        {
            $ip = "0.0.0.0";
        }

		return $ip;
	}

	//return the users country
	function getCountry($ip){
		return geoip_country_name_by_name('$ip');		
	}

	//function to get the network name from the card
	//this should go in the database or in the constants
	function getNetworkName($itemID){
		//new array based code
		global $networkArray; //from constants->cards include file
		$itemPrefix=substr($itemID,0,5);
		$countryAbbreviation=substr($itemID,0,2);
		$name=$networkArray[$countryAbbreviation][$itemPrefix]["networkName"];
		return $name;
	}

	//function to get the price in USD
	function getPriceInUSD($itemID){
		//new array based code
		global $networkArray; //from constants->cards include file
		$priceinusd=0;		
		$networkCode=substr($itemID,0,5);
		$countryAbbreviation=substr($itemID,0,2);
		$priceinusd=$networkArray[$countryAbbreviation][$networkCode][0][$itemID]["PriceInUSD"];
		return $priceinusd;
	}
	
	//function to tell form the card if its an auto dispatch
	//from the itemID you can check the 6th,7th,8th and 9th chars that represent
	//the type of dispatch
	function isAutoDispatch($itemID){
		if(substr($itemID,5,4)=="auto"){
			return true;
		}
		else{
			return false;
		}
	}
	
	//function to connect to the live database DELETE
	function connect2DB(){
		
		$connection = mysql_pconnect(LIVE_DB_ADDRESS,LIVE_DB_USERNAME,LIVE_DB_PASSWORD);
		mysql_select_db(LIVE_DB_NAME, $connection) or handleDatabaseError('Could not select database','connectDB');
							
		//b make sure that the database connects
		if ($connection == false){
			echo "<li>Failed to get the Database connection, please try again later...</li>";
			exit;
		}

		return $connection;
	}
	
	//function to connect to the live database
	function connect2DB2($countryAbbreviation){
		
		global $liveDBArrray;
		$dbAddress=$liveDBArrray[$countryAbbreviation]["address"];
		$dbUsername=$liveDBArrray[$countryAbbreviation]["username"];
		$dbPassword=$liveDBArrray[$countryAbbreviation]["password"];
		$dbName=$liveDBArrray[$countryAbbreviation]["name"];
		
		$connection = mysql_pconnect($dbAddress,$dbUsername,$dbPassword);
		mysql_select_db($dbName, $connection) or handleDatabaseError('Could not select database','connectDB');
							
		//b make sure that the database connects
		if ($connection == false){
			echo "<li>Failed to get the Database connection, please try again later...</li>";
			exit;
		}

		return $connection;
	}
	
		//function to connect to the live database DELETE
	function connect2SMSDB(){
		
		$connection = mysql_pconnect(LIVE_DB_ADDRESS,LIVE_DB_USERNAME,LIVE_DB_PASSWORD);
		mysql_select_db(LIVE_DB_NAME, $connection) or handleDatabaseError('Could not select database','connectDB');
							
		//b make sure that the database connects
		if ($connection == false){
			echo "<li>Failed to get the Database connection, please try again later...</li>";
			exit;
		}

		return $connection;
	}
	//function to disconnect a database connection
	function disconnectDB($connection){
		mysql_close($connection);
	}
	
	//function to connect to the database with the page content DELETE
	function connect2PageContentDB(){
		//name of database is PAGE_DB_NAME and should be the same for all sites.
		$connection = mysql_pconnect(LIVE_DB_ADDRESS,LIVE_DB_USERNAME,LIVE_DB_PASSWORD);
		mysql_select_db(PAGE_DB_NAME, $connection) or handleDatabaseError('Could not select database','connectDB');
							
		//b make sure that the database connects
		if ($connection == false){
			echo "<li>Failed to get the Database connection, please try again later...</li>";
			exit;
		}

		return $connection;
	}
	
		//function to connect to the database with the page content
	function connect2PageContentDB2($countryAbbreviation){

		global $liveDBArrray;
		$dbAddress=$liveDBArrray[$countryAbbreviation]["address"];
		$dbUsername=$liveDBArrray[$countryAbbreviation]["username"];
		$dbPassword=$liveDBArrray[$countryAbbreviation]["password"];
		//name of database is PAGE_DB_NAME and should be the same for all sites.
		
		$connection = mysql_pconnect($dbAddress,$dbUsername,$dbPassword);
		mysql_select_db(PAGE_DB_NAME, $connection) or handleDatabaseError('Could not select database','connectDB');
							
		//b make sure that the database connects
		if ($connection == false){
			echo "<li>Failed to get the Database connection, please try again later...</li>";
			exit;
		}

		return $connection;
	}

	//function to report any database error to the tech admin(s)
	function handleDatabaseError($error,$query){
		//echo "There has been an error: $error<br>";
		//echo "Please send an email to ".SUPPORT_EMAIL." informing them about this<br>";
		//we should set it such that every database error is emailed
		//compose message
		$errorMsg = "Date and Time: ".date("Y-m-d H:i:s")."\n";
		$errorMsg.= "IP Address: ".getIP()."\n";
		$errorMsg.= "Query: ".$query."\n";
		$errorMsg.= "Error: ".$error."\n";
        $errorMsg.= "URL: ".$_SERVER['SCRIPT_FILENAME']."\n";
		
		//send email
		sendEmail("Sendairtime Web",FROM_EMAIL,TECH_ADMIN,SUPER_TECH_ADMIN,"Database Error",$errorMsg);
		
		die("There was an error!");
	}
	
	function printDebug($debug,$msg){
		if($debug){echo"$msg<br>";}
	}
	
	//DELETE
	function decrementCredits($clientEmail){
		//lock db
		$connection=connect2DB();
			$updateCreditsQuery="UPDATE SMSUsers SET creditsLeft=creditsLeft-1 WHERE emailAddress='$clientEmail'";
			mysql_query($updateCreditsQuery) or handleDatabaseError(''.mysql_error(),$updateCreditsQuery);
		disconnectDB($connection);
		//unlock db
	}
	
	function decrementCredits2($clientEmail,$countryAbbreviation){
		//lock db
		$connection=connect2DB($countryAbbreviation);
			$updateCreditsQuery="UPDATE SMSUsers SET creditsLeft=creditsLeft-1 WHERE emailAddress='$clientEmail'";
			mysql_query($updateCreditsQuery) or handleDatabaseError(''.mysql_error(),$updateCreditsQuery);
		disconnectDB($connection);
		//unlock db
	}
	
	function decrementAirtimeCredits($clientEmail, $countryAbbreviation, $deduction){
		//$airtimeCredits = 
		$debug = true;
		//lock db
		$connection=connect2DB( $countryAbbreviation );
			//1. lock NewCards Table 	---BEGIN CRITICAL SECTION 
			$lockQuery="LOCK TABLES SMSUsers WRITE";
			$lockResult=mysql_query($lockQuery) or handleDatabaseError(''. mysql_error(),$lockQuery);
		
			$updateAirtimeCreditsQuery="UPDATE SMSUsers SET airtimeCreditsLeft=airtimeCreditsLeft-$deduction ";
			$updateAirtimeCreditsQuery.="WHERE emailAddress='$clientEmail'";
			if($debug){echo "decrementAirtimeCredits query: $updateAirtimeCreditsQuery <br>"; }
			mysql_query($updateAirtimeCreditsQuery) or handleDatabaseError(''.mysql_error(),$updateAirtimeCreditsQuery);
			
			//--END CRITICAL SECTION  UNLOCK DATABASE ACCESS 7
			//unlock NewCards Table
			$lockQuery="UNLOCK TABLES";
			$lockResult=mysql_query($lockQuery) or handleDatabaseError(''. mysql_error(),$lockQuery);
		disconnectDB($connection);
		//unlock db
	}

	//Function to log SMS messages that are sent out. DELETE
	function logSMS($clientEmail, $smsGatewayID, $ip){
		//LOG SMS SENT
		$timeStamp = date("Y-m-d H:i:s");
		$logSMSInsertQuery="INSERT INTO SMSSendLog (emailAddress, smsGWID, ip, date) ";
		$logSMSInsertQuery.="VALUES('$clientEmail', '$smsGatewayID', '$ip', '$timeStamp')";
		$connection=connect2DB();
			mysql_query($logSMSInsertQuery) or handleDatabaseError(''.mysql_error(),$logSMSInsertQuery);
		disconnectDB($connection);
	}
	
		//Function to log SMS messages that are sent out.
	function logSMS2($clientEmail, $smsGatewayID, $ip, $countryAbbreviation){
		//LOG SMS SENT
		$timeStamp = date("Y-m-d H:i:s");
		$logSMSInsertQuery="INSERT INTO SMSSendLog (emailAddress, smsGWID, ip, date) ";
		$logSMSInsertQuery.="VALUES('$clientEmail', '$smsGatewayID', '$ip', '$timeStamp')";
		$connection=connect2DB($countryAbbreviation);
			mysql_query($logSMSInsertQuery) or handleDatabaseError(''.mysql_error(),$logSMSInsertQuery);
		disconnectDB($connection);
	}
	
			//Function to log SMS messages that are sent out.
	function logLogin($clientEmail, $ip, $loginResult, $countryAbbreviation){
		//LOG SMS SENT
		$timeStamp = date("Y-m-d H:i:s");
        $ip = (filter_var($ip, FILTER_VALIDATE_IP)) ? $ip : "INVALID_IP";
		$logSMSInsertQuery="INSERT INTO LoginLog (emailAddress, ipAddress, loginTime, loginResult) ";
		$logSMSInsertQuery.="VALUES('$clientEmail', '$ip', '$timeStamp', $loginResult)";
		
		$logLastLoginQuery="UPDATE SMSUsers SET lastLogin = '$timeStamp' WHERE emailAddress = '$clientEmail'";
		
		$connection=connect2SMSDB();
			mysql_query($logSMSInsertQuery) or handleDatabaseError(''.mysql_error(),$logSMSInsertQuery);
			mysql_query($logLastLoginQuery) or handleDatabaseError(''.mysql_error(),$logLastLoginQuery);
		disconnectDB($connection);
	}
	//returns the length of the phone number of a particular country
	//these are constants and hence do not really need to be put in a database 
	//since a database will require time to extract them
	//DELETE
	function countryPhoneLength($countryCode){
		switch($countryCode){
			case 1:
				$numberLength=12;
				break;
			case 233://gh
				$numberLength=12;
				break;				
			case 250://rw
				$numberLength=12;
				break;
			case 254://ke
				$numberLength=13;
				break;			
			case 255://tz
				$numberLength=12;
				break;
			case 256://ug
				$numberLength=13;
				break;
			case 257://bu
				$numberLength=12;
				break;							
			default:
				$numberLength=13;
		}
		return $numberLength;
	}
	
	function countryPhoneLength2($countryAbbreviation){
		global $countryInfo;
		return $countryInfo[$countryAbbreviation]["mobilePhoneLength"];
	}
	
	//get value in the local currency of the country
	function getValueInLocal($itemID){
		return substr($itemID,9);
	}
	
	//function to create a subscriber that does not already exist
	function createSubscriber($clientEmail,$password,$countryAbbreviation){
		$newSubscriberQuery="INSERT INTO Subscribers(clientEmail,clientPassword,lastDayOfIncr,dateOfExpiry,totalSinceIncrement) ";
		$newSubscriberQuery.="VALUES('$clientEmail',PASSWORD('$password'),0,0,0.0)";
		$connection=connect2DB($countryAbbreviation);
			$newSubscriberResult=mysql_query($newSubscriberQuery) or handleDatabaseError(''. mysql_error(),$newSubscriberQuery);
		disconnectDB($connection);
	}
	
	function executeQuery($query,$countryAbbreviation){
		$connection=connect2DB($countryAbbreviation);
			$result = mysql_query($query) or handleDatabaseError(''. mysql_error(),$query);
			$data = @mysql_fetch_array($result);
            mysql_free_result($result);
		disconnectDB($connection);	
		return $data;
	}
	
	function executeSMSQuery($query,$countryAbbreviation){
		$connection=connect2SMSDB();
			$result = mysql_query($query) or handleDatabaseError(''. mysql_error(),$query);
			$data = @mysql_fetch_array($result);
		disconnectDB($connection);	
		return $data;
	}
	function incrementDateByAMonth($dateToIncrement){
		$sum=intval($dateToIncrement);
		$month=intval(substr($dateToIncrement,4,2));
		if($month<12){
			$sum=$sum+100;
		}
		else{//substr($dateToIncrement,4,2)>=12
			$sum=$sum+8900; //i.e. sum=date-1100; sum=sum+10000;
		}
		return $sum;
	}

	function connect2_awpc_db($countryAbbreviation){
		global $liveDBArrray;
		$dbAddress  = $liveDBArrray[$countryAbbreviation]["address"];
		$dbUsername = $liveDBArrray[$countryAbbreviation]["username"];
		$dbPassword = $liveDBArrray[$countryAbbreviation]["password"];
		$dbName     = AWPC_DB;
		
		$connection = mysql_pconnect($dbAddress,$dbUsername,$dbPassword);
		mysql_select_db($dbName, $connection) or handleDatabaseError('Could not select database: '.mysql_error(),'connectDB');
							
		//b make sure that the database connects
		if ($connection == false){
			echo "<li>Failed to get the Database connection, please try again later...</li>";
			exit;
		}

		return $connection;
	}

    function executeSingleRowQuery($query,$connection,$debug=false){
			$result = mysql_query($query,$connection) or handleDatabaseError(''. mysql_error(),$query);
			$data = @mysql_fetch_array($result);
            mysql_free_result($result);
            if($debug) echo"Query: $query <br>";
		return $data;
	}

    function executeCountQuery($query,$connection,$debug=false){
			$result = mysql_query($query,$connection) or handleDatabaseError(''. mysql_error(),$query);
			$count = mysql_num_rows($result);
            mysql_free_result($result);
            if($debug) echo"Query: $query <br>";
			if($debug) echo"Number of rows = $count <br>";
		return $count;
	}
    
    function executeCountDataQuery($query,$countryAbbreviation,$debug=false){
        
        $result = mysql_query($query) or handleDatabaseError(''. mysql_error(),$query);
        $data = @mysql_fetch_array($result);
        $count = mysql_num_rows($result);
        $resultArray['count'] = $count;
        $resultArray['data'] = $data;
        mysql_free_result($result);        
        return resultArray;
    }

?>
