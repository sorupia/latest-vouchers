<?
    //20110110: Created formCases.php file for deletion of receivers
	session_start();
	require_once$_SERVER['DOCUMENT_ROOT']."/functions.php";
	require_once$_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
    require_once$_SERVER['DOCUMENT_ROOT']."/src/Objects/Receivers.php";
    
    switch($_POST['type'])
    {
        case "deleteReceiver":
            //for security purposes we use the session instead of a POST
			$clientEmail   = $_SESSION['clientEmail'];
            $receiverPhone = $_POST['receiverPhone'];
            $returnPage    = $_POST['returnPage'];
            
            $inputArray['clientEmail']   = $clientEmail;
            $inputArray['receiverPhone'] = $receiverPhone;
            $inputArray['returnPage']    = $returnPage;
            
            $success = deleteReceiver($inputArray);
            
            if($success)
            {
                $message = "Successfully deleted $receiverPhone for $clientEmail";
            }
            else
            {
                $message = "Unable to delete $receiverPhone for $clientEmail ";
                $message.= "make sure you select a receiver phone to delete. ";
            }
            
            $_SESSION['message'] = $message;
            
            header("Location: ../$returnPage");
            
            break;
            
        case "sendat_form":
            //for security purposes we use the session instead of a POST
			$clientEmail     = $_POST['clientEmail'];
            $itemIDNumber    = $_POST['itemIDNumber'];
            $receiverPhone   = $_POST['receiverPhone'];
            $cardsArePresent = $_POST['cardsArePresent'];
            $returnPage      = $_POST['returnPage'];

            $success = true;
            
            if($success)
            {
   
                echo "<body onload='document.paypal_form.submit();'><br>";
                echo "<form name='paypal_form' action='https://www.paypal.com/cgi-bin/webscr' method='post'>
                      <input type='hidden' name='cmd' value='_s-xclick'>
                      <input type='hidden' name='hosted_button_id' value='Y99UUQ8VAK9U8'>
                      </form>";
                echo "</body>";
                
            }
            else
            {
                $_SESSION['message'] = "Unable to delete $receiverPhone for $clientEmail ";
                $_SESSION['message'].= "make sure you select a receiver phone to delete. ";
                header("Location: ../$returnPage");
            }

            break;
            
    }
    
?>