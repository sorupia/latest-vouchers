<?
function calculate_yo_fee_local($input_array)
{
    $sale_price_in_local = $input_array['sale_price_in_local'];
    $yo_fee_in_local = ($sale_price_in_local * YO_TX_PCT_FEE) + YO_TX_FIXED_FEE;
    $input_array['yo_fee_in_local'] = round($yo_fee_in_local,LOCAL_DECIMAL_PLACES);
    
    return $input_array;
}
?>