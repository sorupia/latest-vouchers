<?
/*******************************************************************************
**   FILE: calculate_sale_price_foreign_auto.php
**
**   FUNCTION: calculate_sale_price_foreign_auto
**
**   PURPOSE: Function to calculate the sale price in foreign currency
**   Costs include:
**   -Airtime Processor percentage fee
**   -Airtime Processor fixed fee
**   -Payment Processor fixed fee
**   -SMS Price
**   -Webshop fixed fee (our fee)
**   -Payment Processor percentage fee
**   -Webshop percentage fee
**
**   NOTE: make sure to set input_array['receiver_amount_in_local'] if known prior
**   or unset it if this function is called within a while loop
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 02.Jul.2012
**
**   MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 21.Jan.2013
**   added airtime processor fixed fee to the equation
**
*********************************************************************************/

function calculate_sale_price_foreign_auto($input_array)
{

    if( isset($input_array['item_id']) && substr($input_array['item_id'],5,4) == AIRTIME_LOAD_OPTION_CARD )
    {
        $item_id = $input_array['item_id'];
		$sale_price_in_foreign = getPriceInUSD($item_id);
        $airtime_processor_fee = 0;
    }
    else if( isset($input_array['receiver_amount_in_local']) || 
             ( isset($input_array['item_id']) && isAutoDispatch($input_array['item_id']) ))
    {
        if(!isset($input_array['receiver_amount_in_local']))
        {
            $item_id = $input_array['item_id'];
            $input_array['receiver_amount_in_local'] = getValueInLocal($item_id);
        }

        $cost_price_in_local        = $input_array['receiver_amount_in_local'];

        $cost_price_in_foreign      = $cost_price_in_local / EXCHANGE_RATE;

        $ap_tx_fixed_fee_in_foreign = AP_TX_FIXED_FEE_IN_LOCAL / EXCHANGE_RATE;

        $ap_tx_pct_fee_in_foreign   = $cost_price_in_foreign * AP_TX_PCT_FEE;

        $sale_price_in_foreign = ($cost_price_in_foreign + $ap_tx_pct_fee_in_foreign + 
                                  $ap_tx_fixed_fee_in_foreign + PP_TX_FEE + PRICE_PER_SMS + 
                                  WEBSHOP_TX_FIXED_FEE_FOREIGN)/
                                 (1 - PP_TX_PCT_INT - WEBSHOP_TX_PCT_FEE_FOREIGN - AP_TX_PCT_FEE);

        $airtime_processor_fee                = $ap_tx_fixed_fee_in_foreign + $ap_tx_pct_fee_in_foreign;
    }
    else
    {
        $sale_price_in_foreign = 0;
        $payment_processor_fee = 0;
        $airtime_processor_fee = 0;
        $our_fee               = 0;
    }

    $payment_processor_fee                = PP_TX_FEE + (PP_TX_PCT_INT * $sale_price_in_foreign);
    $our_fee                              = WEBSHOP_TX_FIXED_FEE_FOREIGN + (WEBSHOP_TX_PCT_FEE_FOREIGN * $sale_price_in_foreign);

    $input_array['sale_price_in_foreign'] = round($sale_price_in_foreign,FOREIGN_DECIMAL_PLACES);
    $input_array['payment_processor_fee'] = round($payment_processor_fee,FOREIGN_DECIMAL_PLACES);
    $input_array['airtime_processor_fee'] = round($airtime_processor_fee,FOREIGN_DECIMAL_PLACES);
    $input_array['our_fee']               = round($our_fee,FOREIGN_DECIMAL_PLACES);

    return $input_array;
}
?>