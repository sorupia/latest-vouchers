<?
function calculate_sale_price_local($input_array)
{
    $cost_price_in_local = substr($input_array['item_number'],9);
    $sale_price_in_local = ($cost_price_in_local * ( 1 + WEBSHOP_TX_PCT_FEE ) + WEBSHOP_TX_FIXED_FEE + YO_TX_FIXED_FEE)/( 1 - YO_TX_PCT_FEE );
    $input_array['sale_price_in_local'] = round($sale_price_in_local,LOCAL_DECIMAL_PLACES);
    
    return $input_array;
}
?>