<div class="receiptDetails">
    <div class="spacing">
        <div class="floatLeft">
            <div class="carrier">
                <? echo "<img src='images/telecoms/".$input_array['network_id'].".png' alt='".$input_array['network_id']."' />"; ?>
            </div>
        </div>
        <div class="floatLeft">
            <div>
                <div>
                    <ul>
                        <li><h2>Confirm airtime receiver details</h2></li>
                        <li><b>Receiver Name:</b> <?=$input_array["receiver_first_name"]?> <?=$input_array["receiver_last_name"]?></li>                
                        <li><b>Receiver Phone:</b> <?=$input_array["receiver_phone"]?></li>
                        <li><b>Airtime To Be Received:</b> <?=LOCAL_CURRENCY?> <?=number_format($input_array['receiver_amount_in_local'], LOCAL_DECIMAL_PLACES)?></li>
                        <li><b>Airtime To Be Received As: </b> <?=$input_array["airtime_load_option"]?></li>
                        <li><b>Payment Type: </b> <?=$input_array["payment_option"]?></li>
                        <?
                            if($input_array["payment_option"] == PAYMENT_OPTION_UG_MOMO)
                            {
                        ?>
                        <li><b>Amount Due:</b> <?=LOCAL_CURRENCY." ".number_format($input_array["receiver_amount_in_local"],LOCAL_DECIMAL_PLACES)?></li>
                        <?  }
                            else
                            {
                        ?>
                        <li><b>Amount Due:</b> <?=SALE_CURRENCY." ".number_format($input_array["sale_price_in_foreign"],FOREIGN_DECIMAL_PLACES)?></li>
                        <?  }
                        ?>
                    </ul>
                </div>
                <div class="clear"></div>
                <form action="submit_order_to_pp.php" method="post" name="submit_order_form_auto" id="submit_order_form_auto">
                    <div align="right" class="row" style="padding-right:20px;">
                        <input type="hidden" name="receiver_amount_in_local" value="<?=$input_array['receiver_amount_in_local']?>">
                        <input type="hidden" name="receiver_phone"           value="<?=$input_array['receiver_phone']?>">
                        <input type="hidden" name="phoneConfirm"             value="<?=$input_array['phoneConfirm']?>">
                        <input type="hidden" name="receiver_first_name"      value="<?=$input_array['receiver_first_name']?>">
                        <input type="hidden" name="receiver_email"           value="<?=$input_array['receiver_email']?>">
                        <input type="hidden" name="payment_option"           value="<?=$input_array['payment_option']?>">
                        <input type="hidden" name="airtime_load_option"      value="<?=$input_array['airtime_load_option']?>">
                        <input type="hidden" name="ip_address"               value="<?=$input_array['ip_address']?>">
                        <input type="hidden" name="ip_country"               value="<?=$input_array['ip_country']?>">
                        <input type="hidden" name="ip_state"                 value="<?=$input_array['ip_state']?>">
                        <input type="hidden" name="ip_city"                  value="<?=$input_array['ip_city']?>">
                        <input type="hidden" name="network_id"               value="<?=$input_array['network_id']?>">
                        <input type="hidden" name="item_id"                  value="<?=$input_array['item_id']?>">
                        <input type="hidden" name="item_name"                value="<?=$input_array['item_name']?>">
                        <input type="hidden" name="hosted_button_id"         value="<?=$input_array['hosted_button_id']?>">
                        <input name="submit" type="submit"                   value="Submit"/>
                        <input name="cancel" type="button"                   value="Cancel" onclick="history.go(-2);" />
                    </div>
                </form>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>