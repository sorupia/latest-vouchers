<?
    function make_order($input_array)
    {
        $date_added              = date("Y-m-d H:i:s");
        $session_id              = $input_array['session_id'];
        $connection              = $input_array['connection'];
        $ip_country              = $input_array['ip_country'];
        $ip_state                = $input_array['ip_state'];
        $ip_city                 = $input_array['ip_city'];
        $ip_address              = $input_array['ip_address'];
        $item_id                 = $input_array['item_id'];
        $button_id               = $input_array['button_id'];
        $receiver_network_id     = $input_array['network_id'];
        $receiver_email          = $input_array['receiver_email'];
        $receiver_phone          = $input_array['receiver_phone'];
        $receiver_first_name     = $input_array['receiver_first_name'];

        $query = "INSERT INTO Ordered_Cart_Items ";
        $query.= "SET ";
        $query.= "date_added = '".mysql_real_escape_string($date_added)."', ";
        $query.= "session_id = '".mysql_real_escape_string($session_id)."', ";
        $query.= "receiver_first_name = '".mysql_real_escape_string($receiver_first_name)."', ";
        $query.= "receiver_phone = '".$receiver_phone."', ";
        $query.= "receiver_email = '".$receiver_email."', ";
        $query.= "receiver_network_id = '".$receiver_network_id."', ";
        $query.= "item_id = '".$item_id."', ";
        $query.= "button_id = '".$button_id."', ";
        $query.= "ip_country = '".mysql_real_escape_string($ip_country)."', ";
        $query.= "ip_state = '".mysql_real_escape_string($ip_state)."', ";
        $query.= "ip_city = '".mysql_real_escape_string($ip_city)."', ";
        $query.= "ip_address = '".$ip_address."' ";

        $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
        $input_array['order_id'] = mysql_insert_id($connection);

        return $input_array;
    }
?>