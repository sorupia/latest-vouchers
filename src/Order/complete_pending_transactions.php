<?
/*******************************************************************************
*   FILE: complete_pending_transactions.php
*   PURPOSE: This is a function that facilitates the cron job
*            It checks the outgoing queue for items with dispatchStatus='PENDING'
*            then it checks their transaction status at Yo! Payments API
*            then it moves them to the UsedCards table if the processing is complete.
*            For Warid airtime, it was found that Yo! has to perform a non blocking call
*            and therefore the need for a cronjob to poll the status.
*
*   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 06.Feb.2013
*********************************************************************************/

include_once $_SERVER['DOCUMENT_ROOT']."/sms/Messenger.php";
include_once $_SERVER['DOCUMENT_ROOT']."/src/Messaging/MessagingFunctions.php";
include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/ObjectFunctions.php";
include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/OrderFunctions.php";
include_once $_SERVER['DOCUMENT_ROOT']."/src/Yo/yo_functions.php";

function complete_pending_transactions($input_array)
{
    $connection = $input_array['connection'];
    $completed_items_array = array();
    $i = 0;

    $query = "SELECT * ";
    $query.= "FROM OutgoingQueue ";
    $query.= "WHERE ";
    $query.= "dispatchStatus = 'PENDING' ";
    $query.= "AND ";
    $query.= "airtime_gw_id <> '' ";

    $result = mysql_query($query,$connection) or handleDatabaseError(''. mysql_error(),$query);

    //Iterate through database for PENDING items
    while ($data = mysql_fetch_array($result, MYSQL_ASSOC))
    {
        if(strlen($data['airtime_gw_id']) > 1)
        {
            //Using the airtime_gw_id check the TransactionStatus
            $input_array['unique_transaction_id'] = $data['airtime_gw_id'];
            $input_array = yo_account_tx_check_status($input_array);

            //Check if the TransactionStatus is a SUCCESS
            if($input_array['api_response']['StatusCode'] == 0)
            {
                //Insert into UsedCards
                $input_array['cardPIN']         = "AUTO_LOAD";
                $input_array['networkName']     = getNetworkName($data['itemID']);
                $input_array['valueInLocal']    = getValueInLocal($data['itemID']);
                $input_array['dateWePurchased'] = $data['timeStamp'];
                $input_array['timeCardSent']    = $data['timeStamp'];
                $input_array['txn_id']          = $data['transactionID'];
                $input_array['item_number']     = $data['itemID'];
                $input_array['payer_email']     = $data['clientEmail'];
                $input_array['invoice']         = $data['order_id'];
                $input_array['loadedBy']        = "Not Applicable";
                $input_array['loadersIP']       = "Not Applicable";
                $input_array['serialNumber']    = "Not Applicable";

                //Send confirmation SMS to receiver and obtain sms_gw_id
                $input_array['first_name'] = $data['clientFirstName'];
                $input_array['last_name']  = "";
                $input_array['order_data']['receiver_amount_in_local'] = $input_array['valueInLocal'];
                $input_array['order_data']['receiver_currency']        = LOCAL_CURRENCY;
                $input_array['order_data']['receiver_phone']           = $data['receiverPhone'];
                $input_array['order_data']['receiver_first_name']      = $data['receiverFirstName'];
                $input_array['order_data']['receiver_last_name']       = "";
                $input_array['order_data']['receiver_email']           = $data['receiverEmail'];
                $input_array = sms_receiver_success_transfer($input_array);

                $input_array['sessionUser'] = "";
                $input_array['item_id']     = $data['itemID'];

                //Returns $input_array['our_fee'], $input_array['airtime_processor_fee']
                $input_array = calculate_sale_price_foreign_auto($input_array);
                $input_array['mc_fee']        = $input_array['payment_processor_fee'];
                $input_array['ourPriceInUSD'] = $input_array['sale_price_in_foreign'];
                $input_array['mc_gross']      = $input_array['sale_price_in_foreign'];
                $input_array['mc_currency']   = SALE_CURRENCY;

                $input_array['saleType']      = $data['saleType'];
                $input_array['airtime_gw_id'] = $data['airtime_gw_id'];
                save_in_used_cards($input_array);

                //Delete from OutgoingQueue
                $query = "DELETE FROM OutgoingQueue WHERE transactionID = '".$data['transactionID']."'";
                mysql_query($query,$connection) or handleDatabaseError(''. mysql_error(),$query);

                //Send confirmation email to client
                email_client_success_transfer($input_array);

                //Send confirmation email to support
                email_support_success_transfer($input_array);

                $completed_items_array[$i++] = $data;
                
                echo "Transfer of ".$data['itemID']." airtime to ".$data['receiverPhone']." complete.\n";
            }
        }
        else
        {
            //Delete from OutgoingQueue
            //$query = "DELETE FROM OutgoingQueue WHERE transactionID = '".$data['transactionID']."'";
            //mysql_query($query,$connection) or handleDatabaseError(''. mysql_error(),$query);
        }
    }

    if(mysql_num_rows($result) == 0) echo "Nothing to process.\n";
    
    return $input_array;
}

?>