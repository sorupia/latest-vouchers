<?
function calculate_our_fee_local($input_array)
{
    $cost_price_in_local = getValueInLocal($input_array['item_number']);
    $our_fee_in_local = ($cost_price_in_local * WEBSHOP_TX_PCT_FEE) + WEBSHOP_TX_FIXED_FEE ;
    $input_array['our_fee_in_local'] = round($our_fee_in_local,LOCAL_DECIMAL_PLACES);
    
    return $input_array;
}
?>