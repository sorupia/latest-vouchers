<?
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/OrderFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Security/SecurityFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/constants.php";

    $input_array = $_POST;

    //Validate receiver form
    $input_array = validate_receiver_form_vchr($input_array);
    $success = $input_array['success'];

    if($success)
    {
        $input_array["connection"] = connect2DB();
    }

    //Verify this receiver's status
    if($success && VERIFY_RECEIVER_STATUS)
    {
        $input_array = verify_receiver_status($input_array);
        $success = !$input_array['receiver_status_blocked'];
    }

    //Verify this receiver's limit
    if($success && LIMIT_RECEIVER)
    {
        $input_array = verify_receiver_limit($input_array);
        $success = !$input_array['receiver_limit_exceeded'];
    }

    //Verify receiver amount low limit
    if($success)
    {
        $input_array = verify_receiver_lower_limit($input_array);
        $success = !$input_array['receiver_amount_low'];
    }

    //Verify this IP limit
    if($success && LIMIT_IP)
    {
        $input_array = verify_ip_limit($input_array);
        $success = !$input_array['ip_limit_exceeded'];
    }

    if(!$success)
    {
        echo'<form action="receiver.php" method="post" name="receiver_form" id="receiver_form">
             <input type="hidden" name="receiver_phone"      value="'.$_POST['receiver_phone'].'" />
             <input type="hidden" name="phoneConfirm"        value="'.$_POST['phoneConfirm'].'" />
             <input type="hidden" name="receiver_first_name" value="'.$_POST['receiver_first_name'].'" />
             <input type="hidden" name="receiver_email"      value="'.$_POST['receiver_email'].'" />
             <input type="hidden" name="ip_address"          value="'.$_POST['ip_address'].'" />
             <input type="hidden" name="ip_country"          value="'.$_POST['ip_country'].'" />
             <input type="hidden" name="ip_state"            value="'.$_POST['ip_state'].'" />
             <input type="hidden" name="ip_city"             value="'.$_POST['ip_city'].'" />
             <input type="hidden" name="network_id"          value="'.$_POST['network_id'].'" />
             <input type="hidden" name="item_id"             value="'.$_POST['item_id'].'" />
             <input type="hidden" name="item_name"           value="'.$_POST['item_name'].'" />
             <input type="hidden" name="hosted_button_id"    value="'.$_POST['button_id'].'">
             <input type="hidden" name="error"               value="'.$input_array['error'].'">
             </form>';

        echo'<script language="JavaScript" type="text/javascript">
                window.onload=function(){ window.document.receiver_form.submit(); }
             </script>';
    }
    else
    {
        echo'<center>
                <p>&nbsp;</p>
                <p><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="333333">
                    Processing Transaction . . . 
                </font>
                </p>
            </center>';

        $input_array['session_id'] = session_id();

        //Delete abandoned orders
        delete_abandoned_orders($input_array);

        //Issue order ID
        $input_array = make_order($input_array);

        //Modify order id to accommodate other countries on one PayPal
        $input_array['order_id'] = COUNTRY_ABBREVIATION.$input_array['order_id'];

        $_SESSION['order_id'] = $input_array['order_id'];

        if (!isset($_SESSION['order_id']))
        {
            echo "ERROR: Order ID not assigned<br>";
            exit;
        }

        //If all checks have passed then submit the order to PayPal
        echo'<form action="'.PAYPAL_PAYMENT_URL.'" method="post" name="submit_order_to_paypal" id="submit_order_to_paypal">
                <input type="hidden" name="hosted_button_id" value="'.$input_array['button_id'].'">
                <input type="hidden" name="cmd"              value="'.PAYPAL_CMD.'">
                <input type="hidden" name="invoice"          value="'.$input_array['order_id'].'">                
            </form>';

        echo'<script language="JavaScript" type="text/javascript">
                window.onload=function(){ window.document.submit_order_to_paypal.submit(); }
             </script>';
    }
?>