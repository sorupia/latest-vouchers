<?

/*******************************************************************************
**   FILE: process_submit_order_form_auto.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Processes receiver form data and redirects user to payment gateway
**            Also validates receiver form data
**            Also stores user order for processing on succesful payment
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 09.Jul.2012
**
**   MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 30.Nov.2015
**   Modified $success boolean statements
**   Added an option for voucher load option to automatically generate
**   PayPal attributes
**
*********************************************************************************/

    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/OrderFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Security/SecurityFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/constants.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Yo/yo_functions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Constants/auto_load_constants.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/calculate_sale_price_foreign_vchr.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/validate_receiver_form_vchr.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/verify_receiver_phone_network_auto.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/verify_receiver_amount.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Security/verify_receiver_lower_limit.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/make_order_vchr.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Messaging/MessagingFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/create_item_id.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/dbInfo.php";

    $input_array = $_POST;
    
   
    
    //Do entire validation all over again for security reasons
    //Validate receiver form
    $input_array = validate_receiver_form_vchr($input_array);
    $success = $input_array['success'];


    
    
    //Connect to database
    if($success)
    {
        $input_array["connection"] = connect2DB();
    }

    
    //Verify this receiver's status
    if($success && VERIFY_RECEIVER_STATUS)
    {
        $input_array = verify_receiver_status($input_array);
        $success = !$input_array['receiver_status_blocked'];
    }

    //Verify this receiver's limit
    if($success && LIMIT_RECEIVER)
    {
        $input_array = verify_receiver_limit($input_array);
        $success = !$input_array['receiver_limit_exceeded'];
    }

    //Verify receiver amount low limit
    if($success)
    {
        $input_array = verify_receiver_lower_limit($input_array);
        $success = !$input_array['receiver_amount_low'];
    }

    //Verify this IP limit
    if($success && LIMIT_IP)
    {
        $input_array = verify_ip_limit($input_array);
        $success = !$input_array['ip_limit_exceeded'];
    }
    
    //Calculate amount to be paid
    if($success)
    {

        $input_array = calculate_sale_price_foreign_vchr($input_array);

        if($input_array['item_id'] == '')
        {
            $input_array = create_item_id($input_array);
            $input_array['item_id'] = $input_array['created_item_id'];
        }
        else
        {

            if($input_array['airtime_load_option'] == AIRTIME_LOAD_OPTION_CARD || 
               $input_array['airtime_load_option'] == AIRTIME_LOAD_OPTION_VCHR)
            {

            }
            else
            {
                //if client chose the airtime load option of AUTO then recreate the item id
                $input_array = create_item_id($input_array);
                $input_array['item_id'] = $input_array['created_item_id'];
            }
        }

		/* -- Using fancy spinning effect instead, see submit_to_pp.php in active theme
        echo'<center>
                <p>&nbsp;</p>
                <p><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="333333">
                    Redirecting to Payment, Please Wait . . . 
                </font>
                </p>
            </center><br>';
        */   

        $input_array['session_id'] = session_id();

        //Delete abandoned orders
        delete_abandoned_orders($input_array);

        //Issue order ID
        $input_array = make_order_vchr($input_array);

        $input_array['order_id'] = COUNTRY_ABBREVIATION."vchr".$input_array['order_id'];

        $_SESSION['order_id'] = $input_array['order_id'];


        if (!isset($_SESSION['order_id']))
        {
            echo "ERROR: Order ID not assigned<br>";
            exit;
        }

        if($input_array['payment_option'] == PAYMENT_OPTION_PAYPAL)
        {
            if($input_array['airtime_load_option'] == AIRTIME_LOAD_OPTION_CARD)
            {
               // IPN Enabled
               //If all checks have passed then submit the order to PayPal
                echo'<form action="'.PAYPAL_PAYMENT_URL.'" method="post" name="submit_order_to_paypal" id="submit_order_to_paypal">
                        <input type="hidden" name="hosted_button_id" value="'.$input_array['hosted_button_id'].'">
                        <input type="hidden" name="cmd"              value="'.PAYPAL_CMD.'">
                        <input type="hidden" name="invoice"          value="'.$input_array['order_id'].'">
                        <input type="hidden" name="notify_url"       value="'.PAYPAL_NOTIFY_URL.'">
                    </form>';
            }
            else if($input_array['airtime_load_option'] == AIRTIME_LOAD_OPTION_VCHR)
            {
                //FILL IN VOUCHERLOAD OPTION
                $receiver_amount_in_local = number_format($input_array['receiver_amount_in_local'], LOCAL_DECIMAL_PLACES);
                $network_name             = getNetworkName($input_array['item_id']);
                $item_name                = LOCAL_CURRENCY." ".$receiver_amount_in_local." ".$network_name." ".PRODUCT;

                echo'<form action="'.PAYPAL_PAYMENT_URL.'" method="post" name="submit_order_to_paypal" id="submit_order_to_paypal">
                        <input type="hidden" name="amount" value="'.$input_array['receiver_amount_in_foreign'].'">
                        <input type="hidden" name="invoice" value="'.$input_array['order_id'].'">
                        <input type="hidden" name="item_name" value="'.$item_name.'">
                        <input type="hidden" name="item_number" value="'.$input_array['item_id'].'">
                        <input type="hidden" name="business" value="'.PAYPAL_EMAIL.'">
                        <input type="hidden" name="cmd" value="'.PAYPAL_CMD_AUTO.'">
                        <input type="hidden" name="return" value="'.PAYPAL_SUCCESS_URL.'">
                        <input type="hidden" name="cancel_return" value="'.PAYPAL_CANCEL_URL.'">
                        <input type="hidden" name="rm" value="'.PAYPAL_RETURN_METHOD.'">
                        <input type="hidden" name="currency_code" value="'.SALE_CURRENCY.'">
                        <input type="hidden" name="lc" value="'.LOCALE.'">
                        <input type="hidden" name="bn" value="'.PAYPAL_BN.'">
                        <input type="hidden" name="no_shipping" value="'.PAYPAL_DISPLAY_SHIPPING_ADDRESS.'">
                    </form>';
            }
            else
            {
                //FILL IN AUTOLOAD OPTION
                global $auto_load_network_array;
                $receiver_amount_in_local = number_format($input_array['receiver_amount_in_local'], LOCAL_DECIMAL_PLACES);
                $network_id               = $input_array["network_id"];
                $network_name             = $auto_load_network_array[COUNTRY_ABBREVIATION]["$network_id"]["networkName"];
                $item_name                = LOCAL_CURRENCY." ".$receiver_amount_in_local." ".$network_name." ".PRODUCT;
                
                echo'<form action="'.PAYPAL_PAYMENT_URL.'" method="post" name="submit_order_to_paypal" id="submit_order_to_paypal">
                        <input type="hidden" name="amount" value="'.$input_array['receiver_amount_in_foreign'].'">
                        <input type="hidden" name="invoice" value="'.$input_array['order_id'].'">
                        <input type="hidden" name="item_name" value="'.$item_name.'">
                        <input type="hidden" name="item_number" value="'.$input_array['item_id'].'">
                        <input type="hidden" name="business" value="'.PAYPAL_EMAIL.'">
                        <input type="hidden" name="cmd" value="'.PAYPAL_CMD_AUTO.'">
                        <input type="hidden" name="return" value="'.PAYPAL_SUCCESS_URL.'">
                        <input type="hidden" name="cancel_return" value="'.PAYPAL_CANCEL_URL.'">
                        <input type="hidden" name="notify_url" value="'.PAYPAL_NOTIFY_URL.'">
                        <input type="hidden" name="rm" value="'.PAYPAL_RETURN_METHOD.'">
                        <input type="hidden" name="currency_code" value="'.SALE_CURRENCY.'">
                        <input type="hidden" name="lc" value="'.LOCALE.'">
                        <input type="hidden" name="bn" value="'.PAYPAL_BN.'">
                        <input type="hidden" name="no_shipping" value="'.PAYPAL_DISPLAY_SHIPPING_ADDRESS.'">
                    </form>';
            }

            echo'<script language="JavaScript" type="text/javascript">
                    window.onload=function(){ window.document.submit_order_to_paypal.submit(); }
                 </script>';
        }
        else
        {
            //Payment by Mobile Money
            $input_array['item_number'] = $input_array['item_id'];

            if($input_array['airtime_load_option'] == AIRTIME_LOAD_OPTION_CARD)
            {
                //If all checks have passed then submit the order to PayPal
                $input_array = calculate_sale_price_local($input_array);
            
                echo'<form action="'.YO_PROC_URL.'" method="post" name="submit_order_to_yo" id="submit_order_to_yo">
                        <input type="hidden" name="bid"       value="13" />
                        <input type="hidden" name="currency"  value="'.LOCAL_CURRENCY.'" />
                        <input type="hidden" name="amount"    value="'.$input_array['receiver_amount_in_local'].'" />
                        <input type="hidden" name="narrative" value="'.$input_array['item_name'].'" />
                        <input type="hidden" name="reference" value="'.$input_array['order_id'].'" />
                        <input type="hidden" name="account"   value="'.YO_USERNAME.'" />
                        <input type="hidden" name="return"    value="'.YO_RETURN_URL.'?unique_transaction_id=0&transaction_reference=0&item_number='.$input_array['item_id'].'" />
                        <input type="hidden" name="prefilled_payer_email_address"         value="" />
                        <input type="hidden" name="prefilled_payer_mobile_payment_msisdn" value="" />
                        <input type="hidden" name="prefilled_payer_names"                 value="" />
                        <input type="hidden" name="abort_payment_url"                     value="'.INHERIT_SITE.'" />
                    </form>';
            }
            else
            {
                //If all checks have passed then submit the order to Mobile Money Payment Gateway
                //FILL IN AUTOLOAD OPTION
                global $auto_load_network_array;
                $receiver_amount_in_local = number_format($input_array['receiver_amount_in_local'], LOCAL_DECIMAL_PLACES);
                $network_id               = $input_array["network_id"];
                $network_name             = $auto_load_network_array[COUNTRY_ABBREVIATION]["$network_id"]["networkName"];
                $item_name                = LOCAL_CURRENCY." ".$receiver_amount_in_local." ".$network_name." ".PRODUCT;

                $input_array = calculate_sale_price_local_auto($input_array);

                echo'<form action="'.YO_PROC_URL.'" method="post" name="submit_order_to_yo" id="submit_order_to_yo">
                        <input type="hidden" name="bid"       value="13" />
                        <input type="hidden" name="currency"  value="'.LOCAL_CURRENCY.'" />
                        <input type="hidden" name="amount"    value="'.$input_array['receiver_amount_in_local'].'" />
                        <input type="hidden" name="narrative" value="'.$item_name.'" />
                        <input type="hidden" name="reference" value="'.$input_array['order_id'].'" />
                        <input type="hidden" name="account"   value="'.YO_USERNAME.'" />
                        <input type="hidden" name="return"    value="'.YO_RETURN_URL.'?unique_transaction_id=0&transaction_reference=0&item_number='.$input_array['item_id'].'" />
                        <input type="hidden" name="prefilled_payer_email_address"         value="" />
                        <input type="hidden" name="prefilled_payer_mobile_payment_msisdn" value="" />
                        <input type="hidden" name="prefilled_payer_names"                 value="" />
                        <input type="hidden" name="abort_payment_url"                     value="'.INHERIT_SITE.'" />
                    </form>';
            }

            /*
            else
            {
                $input_array['error'].="SORRY PAYMENT OF AUTO LOAD AIRTIME NOT YET SUPPORTED FOR MOBILE MONEY";
                //REPOST information back to form
                echo'<form action="receiver-auto.php" method="post" name="submit_order_to_yo" id="submit_order_to_yo">
                     <input type="hidden" name="receiver_amount_in_local" value="'.$_POST['receiver_amount_in_local'].'" />
                     <input type="hidden" name="receiver_phone"           value="'.$_POST['receiver_phone'].'" />
                     <input type="hidden" name="phoneConfirm"             value="'.$_POST['phoneConfirm'].'" />
                     <input type="hidden" name="receiver_first_name"      value="'.$_POST['receiver_first_name'].'" />
                     <input type="hidden" name="receiver_email"           value="'.$_POST['receiver_email'].'" />
                     <input type="hidden" name="payment_option"           value="'.$_POST['payment_option'].'">
                     <input type="hidden" name="airtime_load_option"      value="'.$_POST['airtime_load_option'].'">
                     <input type="hidden" name="ip_address"               value="'.$_POST['ip_address'].'" />
                     <input type="hidden" name="ip_country"               value="'.$_POST['ip_country'].'" />
                     <input type="hidden" name="ip_state"                 value="'.$_POST['ip_state'].'" />
                     <input type="hidden" name="ip_city"                  value="'.$_POST['ip_city'].'" />
                     <input type="hidden" name="network_id"               value="'.$_POST['network_id'].'" />
                     <input type="hidden" name="item_id"                  value="'.$_POST['item_id'].'" />
                     <input type="hidden" name="item_name"                value="'.$_POST['item_name'].'" />
                     <input type="hidden" name="hosted_button_id"         value="'.$_POST['hosted_button_id'].'">
                     <input type="hidden" name="error"                    value="'.$input_array['error'].'">
                     </form>';
            }
            */

            echo'<script language="JavaScript" type="text/javascript">
                    window.onload=function(){ window.document.submit_order_to_yo.submit(); }
                 </script>';
        }
    }
    else
    {
        //REPOST information back to form
        echo'<form action="receiver-auto.php" method="post" name="receiver_form" id="receiver_form">
             <input type="hidden" name="receiver_amount_in_local" value="'.$_POST['receiver_amount_in_local'].'" />
             <input type="hidden" name="receiver_phone"           value="'.$_POST['receiver_phone'].'" />
             <input type="hidden" name="phoneConfirm"             value="'.$_POST['phoneConfirm'].'" />
             <input type="hidden" name="receiver_first_name"      value="'.$_POST['receiver_first_name'].'" />
             <input type="hidden" name="receiver_last_name"       value="'.$_POST['receiver_last_name'].'" />
             <input type="hidden" name="receiver_email"           value="'.$_POST['receiver_email'].'" />
             <input type="hidden" name="receiver_region"          value="'.$_POST['receiver_region'].'" />
             <input type="hidden" name="payment_option"           value="'.$_POST['payment_option'].'">
             <input type="hidden" name="airtime_load_option"      value="'.$_POST['airtime_load_option'].'">
             <input type="hidden" name="ip_address"               value="'.$_POST['ip_address'].'" />
             <input type="hidden" name="ip_country"               value="'.$_POST['ip_country'].'" />
             <input type="hidden" name="ip_state"                 value="'.$_POST['ip_state'].'" />
             <input type="hidden" name="ip_city"                  value="'.$_POST['ip_city'].'" />
             <input type="hidden" name="network_id"               value="'.$_POST['network_id'].'" />
             <input type="hidden" name="item_id"                  value="'.$_POST['item_id'].'" />
             <input type="hidden" name="item_name"                value="'.$_POST['item_name'].'" />
             <input type="hidden" name="hosted_button_id"         value="'.$_POST['hosted_button_id'].'">
             <input type="hidden" name="error"                    value="'.$input_array['error'].'">
             </form>';

        echo'<script language="JavaScript" type="text/javascript">
                window.onload=function(){ window.document.receiver_form.submit(); }
             </script>';
    }
?>