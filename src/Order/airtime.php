<?php 
    //See wrapper/header.php for includes
    //Also see wrapper/header.php for siteTurnedON
    //Also see wrapper/header.php for country_is_blocked

    global $networkMap;
    global $networkArray;
    global $cardMap;
    $quantityOfCardsArray = array();
    $numberOfNetworks = count($networkMap[$countryAbbreviation]);
    $networkNames = array();

    //Check if IP is staff blocked
    $ipaddress     = $ipLocationArray['ip_address'];
    $ip_is_blocked = IPIsBlocked($ipaddress, $countryAbbreviation);

    //Check if IP belongs to a New Client
    //$ip_is_new_clients = isNewClientIP($ipaddress);

    //Check if Country is staff blocked
    $ipCountry          = $ipLocationArray['ip_country'];
    $country_is_blocked = countryIsBlocked($ipCountry,$countryAbbreviation);

    //Block Staff Blocked IP or Country
    if($ip_is_blocked || $country_is_blocked)
    {
        $siteTurnedON = false;
    }

    if($siteTurnedON || $ipLocationArray['ip_country'] == 'GH')
    {
        //Loop through all the cards to see if they are still available
        //store the denomination quantities in the $quantityOfCardsArray array
        $connection=connect2DB();
            for($networkIndex=0;$networkIndex<$numberOfNetworks;$networkIndex++)
            {
                $thisNetwork=$networkMap[$countryAbbreviation][$networkIndex];
                $numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);
                $networkNames[] = $networkArray[$countryAbbreviation]["$thisNetwork"]["networkName"];
                
                for($j=0;$j<$numberOfDenominations;$j++){
                    $thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
                    $oneQuery = sprintf ("SELECT itemID FROM NewCards WHERE NewCards.itemID = '$thisCard' AND NewCards.cardStatus != 'USED'");
                    $oneResult = mysql_query($oneQuery) or handleDatabaseError('' . mysql_error(),$oneQuery);
                    $quantityOfCardsArray["$thisCard"] = mysql_num_rows($oneResult);
                    mysql_free_result($oneResult);
                }
            }
        disconnectDB($connection);
    }
    else
    {
    }
?>

<?php
    if($siteTurnedON)
    {
        if($_SESSION['ATSubscriberLoggedIn'])
        {
            //display regular prices
            //BEGIN FOR LOOPS
            $formNumber=0;
            for($i=0;$i<$numberOfNetworks;$i++)
            {
                $thisNetwork=$networkMap[$countryAbbreviation][$i];
                $numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);
                echo "<div class='chart'>
                      <div class='provider'>Service Provider</div>
                      <div class='label'>Price (".LOCAL_CURRENCY.")</div>
                      <div class='label'>Price (USD)</div>
                      <div class='label'>Option</div>
                      <div class='clear'></div>
                      <div class='telcom'><img src='images/telecoms/".$thisNetwork.".gif' alt='".$thisNetwork."' /></div>";
                $j=0;
                for(;$j<$numberOfDenominations;$j++)
                {
                    $cardIndex=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
                    $priceInLocal=$networkArray[$countryAbbreviation]["$thisNetwork"][0]["$cardIndex"]["PriceInUGX"];
                    $priceInUSD=$networkArray[$countryAbbreviation]["$thisNetwork"][0]["$cardIndex"]["sPriceInUSD"];
                    $item=$networkArray[$countryAbbreviation]["$thisNetwork"][0]["$cardIndex"]["Item"];
                    $localCurrency = LOCAL_CURRENCY;
                    $item_name = "$priceInLocal $item";
                    $input_array['item_id'] = $cardIndex;

                    echo '<div class="prices">
                          <div>'.$priceInLocal.'</div>
                          <div>'.$priceInUSD.'</div>';

                    if($quantityOfCardsArray["$cardIndex"]>0)
                    {
                        $formInfo ='<form name="airtime_form'.$formNumber.'" action="receiver-auto.php" method="post"> ';
                        $formInfo.='<input type="hidden" name="ip_address"          id="ip_address" value="'.$ipLocationArray['ip_address'].'"/> ';
                        $formInfo.='<input type="hidden" name="ip_country"          id="ip_country" value="'.$ipLocationArray['ip_country'].'"/> ';
                        $formInfo.='<input type="hidden" name="ip_state"            id="ip_state"   value="'.$ipLocationArray['ip_state'].'"/> ';
                        $formInfo.='<input type="hidden" name="ip_city"             id="ip_city"    value="'.$ipLocationArray['ip_city'].'"/> ';
                        $formInfo.='<input type="hidden" name="network_id"          id="network_id" value="'.$thisNetwork.'"/> ';
                        $formInfo.='<input type="hidden" name="item_id"             id="item_id"    value="'.$cardIndex.'"/> ';
                        $formInfo.='<input type="hidden" name="item_name"           id="item_name"  value="'.$item_name.'"/> ';

                        $hosted_button_id=$networkArray[$countryAbbreviation]["$thisNetwork"][0]["$cardIndex"]["hosted_button_id"];
                        $hosted_button_id_line = '<input type="hidden" name="hosted_button_id" value="'.$hosted_button_id.'">';

                        echo"<div> ";
                        echo $formInfo.$hosted_button_id_line;
                        echo"<a href='javascript:document.airtime_form".$formNumber++.".submit()' class='login' onfocus='this.blur();' title='Send airtime now'>send now</a></form></div></div>";
                    }
                    else if(this_network_has_auto_load($input_array) && AUTO_LOAD_ENABLED)
                    {
                        echo"<div><a href='receiver-auto.php' class='login' onfocus='this.blur();' title='Send airtime now'>send now</a></div></div>";
                    }
                    else
                    {
                        echo"<div><span class='soldOut'>SOLD OUT</span></div></div>";
                    }
                }

                //to pad the last row with less than 4 rows
                while($j<4)
                {
                    echo"<div class='prices'><div>&nbsp;</div><div>&nbsp;</div><div class='norightborder'>&nbsp;</div></div>";
                    $j++;
                }
                echo"</div>";
                echo"<div class='clear'></div>";
            }
        }//end if($_SESSION['ATSubscriberLoggedIn'])
        else
        {
            //display regular prices
            //BEGIN FOR LOOPS
            $formNumber=0;
            for($i=0;$i<$numberOfNetworks;$i++)
            {
                $thisNetwork=$networkMap[$countryAbbreviation][$i];
                $numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);
                echo "<div class='chart'>
                      <div class='provider'>Service Provider</div>
                      <div class='label'>Price (".LOCAL_CURRENCY.")</div>
                      <div class='label'>Price (USD)</div>
                      <div class='label'>Option</div>
                      <div class='clear'></div>
                      <div class='telcom'><img src='images/telecoms/".$thisNetwork.".gif' alt='".$thisNetwork."' /></div>";
                $j=0;
                for(;$j<$numberOfDenominations;$j++)
                {
                    $cardIndex=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
                    $priceInLocal=$networkArray[$countryAbbreviation]["$thisNetwork"][0]["$cardIndex"]["PriceInUGX"];
                    $priceInUSD=$networkArray[$countryAbbreviation]["$thisNetwork"][0]["$cardIndex"]["sPriceInUSD"];
                    $item=$networkArray[$countryAbbreviation]["$thisNetwork"][0]["$cardIndex"]["Item"];
                    $localCurrency = LOCAL_CURRENCY;
                    $item_name = "$priceInLocal $item";
                    $input_array['item_id'] = $cardIndex;

                    echo '<div class="prices">
                          <div>'.$priceInLocal.'</div>
                          <div>'.$priceInUSD.'</div>';

                    if($quantityOfCardsArray["$cardIndex"]>0)
                    {
                        $formInfo ='<form name="airtime_form'.$formNumber.'" action="receiver-auto.php" method="post"> ';
                        $formInfo.='<input type="hidden" name="ip_address"          id="ip_address" value="'.$ipLocationArray['ip_address'].'"/> ';
                        $formInfo.='<input type="hidden" name="ip_country"          id="ip_country" value="'.$ipLocationArray['ip_country'].'"/> ';
                        $formInfo.='<input type="hidden" name="ip_state"            id="ip_state"   value="'.$ipLocationArray['ip_state'].'"/> ';
                        $formInfo.='<input type="hidden" name="ip_city"             id="ip_city"    value="'.$ipLocationArray['ip_city'].'"/> ';
                        $formInfo.='<input type="hidden" name="network_id"          id="network_id" value="'.$thisNetwork.'"/> ';
                        $formInfo.='<input type="hidden" name="item_id"             id="item_id"    value="'.$cardIndex.'"/> ';
                        $formInfo.='<input type="hidden" name="item_name"           id="item_name"  value="'.$item_name.'"/> ';

                        $hosted_button_id=$networkArray[$countryAbbreviation]["$thisNetwork"][0]["$cardIndex"]["hosted_button_id"];
                        $hosted_button_id_line = '<input type="hidden" name="hosted_button_id" value="'.$hosted_button_id.'">';

                        echo"<div> ";
                        echo $formInfo.$hosted_button_id_line;
                        echo"<a href='javascript:document.airtime_form".$formNumber++.".submit()' class='login' onfocus='this.blur();' title='Send airtime now'>send now</a></form></div></div>";
                    }
                    else if(this_network_has_auto_load($input_array) && AUTO_LOAD_ENABLED)
                    {
                        echo"<div><a href='receiver-auto.php' class='login' onfocus='this.blur();' title='Send airtime now'>send now</a></div></div>";
                    }
                    else
                    {
                        echo"<div><span class='soldOut'>SOLD OUT</span></div></div>";
                    }
                }

                //to pad the last row with less than 4 rows
                while($j<4)
                {
                    echo"<div class='prices'><div>&nbsp;</div><div>&nbsp;</div><div class='norightborder'>&nbsp;</div></div>";
                    $j++;
                }
                echo"</div>";
                echo"<div class='clear'></div>";
            }
        }
    }
    else if ($ipLocationArray['ip_country'] == 'GH')
    {
        $formNumber=0;
        for($i=0;$i<$numberOfNetworks;$i++)
        {
            $thisNetwork=$networkMap[$countryAbbreviation][$i];
            $numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);
            echo "<div class='chart'>
                  <div class='provider'>Service Provider</div>
                  <div class='label'>Price (".LOCAL_CURRENCY.")</div>
                  <div class='label'>Price (".SALE_CURRENCY.")</div>
                  <div class='label'>Option</div>
                  <div class='clear'></div>
                  <div class='telcom'><img src='images/telecoms/".$thisNetwork.".gif' alt='".$thisNetwork."' /></div>";
            $j=0;
            for(;$j<$numberOfDenominations;$j++)
            {
                $cardIndex=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
                $priceInLocal=$networkArray[$countryAbbreviation]["$thisNetwork"][0]["$cardIndex"]["PriceInUGX"];
                $priceInUSD=$networkArray[$countryAbbreviation]["$thisNetwork"][0]["$cardIndex"]["sPriceInUSD"];
                $item=$networkArray[$countryAbbreviation]["$thisNetwork"][0]["$cardIndex"]["Item"];

                echo '<div class="prices">
                      <div>'.$priceInLocal.'</div>
                      <div>'.$priceInUSD.'</div>';

                if($quantityOfCardsArray["$cardIndex"]>0)
                {
                    echo"<div>";
                    echo"<a href='ghanaIndex.php' class='login' onfocus='this.blur();'>SEND NOW</a></div>";
                }
                else
                {
                    echo"<div><span class='soldOut'>SOLD OUT</span></div>";
                }

                echo"</div>";
            }

            //to pad the last row of denominations less than 4 rows
            while($j<4)
            {
                echo"<div class='prices'><div>&nbsp;</div><div>&nbsp;</div><div class='norightborder'>&nbsp;</div></div>";
                $j++;
            }

            echo"</div>";
            echo"<div class='clear'></div>";
        }
    }
    else if($ip_is_blocked || $country_is_blocked)
    {
        echo '<div class="ghanaIndex">';
        echo 'Sorry airtime purchase is not available in ';
        echo 'your country or your first purchase has not been verified.<br/> ';
        echo 'Ask your family and friends abroad to buy airtime for you at this website. ';
        echo 'Incase you just made your first purchase please wait until it is verified<br/>';
        echo 'For more assistance in '.COUNTRY_NAME.' call<br/> ';
        echo SUPPORT_PHONE_1;
        echo '<br> ';
        echo '--'.SERVICED_BY.'--<br><br> ';
        echo '</div>';
    }
    else
    {
        echo '<div class="ghanaIndex">';
        echo "<strong>Airtime Unavailable</strong><br/>";
        echo "We are currently experiencing problems with our server. ";
        echo "As a result the Airtime System is unavailable until this issue is resolved. ";
        echo "Sorry about any inconvenience please try again later. <br/>";
        echo "--".SERVICED_BY."--<br/><br/>";
        echo '</div>';
    }

?>