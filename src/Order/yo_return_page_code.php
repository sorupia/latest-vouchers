<?php
/*******************************************************************************
**  FILE: yo_return_page_code.php
**
**  FUNCTION: N/A
**
**  PURPOSE: Created Yo Return Page based on return_page_code.php to process requests
**  coming in from Yo Payment Processor invocations.
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2010.12.09
**  Added code to maintain serial number in UsedCards
**  Added code to include PayPal attribute "payment_fee"
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2010.12.14
**  Replaced Client update code with updateClientInfo
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs-AVMM, Kampala) DATE: 2015.12.01
**  Fixed bug that distorts debug comments
**
*********************************************************************************/

    //2010-12-09: Added code to maintain serial number in UsedCards
    //2010-12-09: Added code to include PayPal attribute "payment_fee"
    //20101214: Replaced Client update code with updateClientInfo

    include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/constants.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/SecurityFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/ObjectFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/OrderFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/SecurityFunctions.php';
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Yo/yo_functions.php";

    $debug = false;
    $success = true;
    $countryAbbreviation = COUNTRY_ABBREVIATION;
    $google_analytics_acct = GOOGLE_ANALYTICS_ACCT_YO;

    $transactionArray['transaction_reference'] = trim($_GET['transaction_reference']);
    $transactionArray['unique_transaction_id'] = trim($_GET['unique_transaction_id']);
    $transactionArray['item_number']           = trim($_GET['item_number']);

    //Verify that Yo Payment is Enabled
    if($success)
    {
        if(!YO_PAYMENT_ENABLED)
        {
            $success = false;
            $transactionArray['reason'] = "YO_PAYMENT_ENABLED_FALSE";
        }
    }

    //Verify Transaction Reference
    if($success)
    {
        if( !isset($_GET['transaction_reference']) || 
            $transactionArray['transaction_reference'] <= 0 )
        {
            $success = false;
            $transactionArray['reason'] = "TRANSACTION_REFERENCE_LESS_THAN_ZERO";
        }
    }

    //Verify Unique Transaction ID
    if($success)
    {
        if(!isset($_GET['unique_transaction_id']) || 
            strlen($transactionArray['unique_transaction_id']) < 90)
        {
            $success = false;
            $transactionArray['reason'] = "UNIQUE_TRANSACTION_ID_LENGTH";
        }
    }

    //Verify Item ID Number
    if($success)
    {
        if(!isset($_GET['item_number']) || 
            substr($transactionArray['item_number'],0,2) != COUNTRY_ABBREVIATION)
        {
            $success = false;
            $transactionArray['reason'] = "ITEM_ID_COUNTRY_MISMATCH";
        }
    }

    //Verify that this transaction id exists in our Yo account
    if($success)
    {
        $transactionArray = yo_account_tx_check_status($transactionArray);

        $status                      = $transactionArray['api_response']['Status'];
        $status_code                 = $transactionArray['api_response']['StatusCode'];
        $amount                      = $transactionArray['api_response']['Amount'];
        $amount_formatted            = $transactionArray['api_response']['AmountFormatted'];
        $currency_code               = $transactionArray['api_response']['CurrencyCode'];
        $transaction_initiation_date = $transactionArray['api_response']['TransactionInitiationDate'];
        $transaction_completion_date = $transactionArray['api_response']['TransactionCompletionDate'];
        $returned_transaction_ref    = $transactionArray['api_response']['TransactionReference'];

        //if the invoke status is success
        $success = ($status_code == 0) ? 1 : 0;
    }

    //Verify the transaction status of the transaction id
    if($success)
    {
        if($transactionArray['api_response']['TransactionStatus'] == "SUCCEEDED")
        {
            $success = true;
        }
        else
        {
            $success = false;
            $transactionArray['reason'] = "YO_TRANSACTION_STATUS";
        }
    }

    //Verify the price
    if($success)
    {
        $transactionArray = calculate_sale_price_local($transactionArray);

        if($transactionArray['api_response']['Amount'] != $transactionArray['sale_price_in_local'])
        {
            $success = false;
            $transactionArray['reason'] = "YO_TRANSACTION_AMOUNT";
        }
    }

    //Connect to DB
    if($success)
    {
        $connection = connect2DB();
        $transactionArray["connection"] = $connection;

        if(!$connection)
        {
            $success = false;
            $transactionArray['reason'] = "DATABASE_CONNECTION";
        }
    }

    if($success)
    {
        $transactionArray['invoice']        = $transactionArray['transaction_reference'];
        $transactionArray['payment_status'] = "Completed";
        $transactionArray['receiver_email'] = PAYPAL_EMAIL;
        $transactionArray['txn_id']         = $transactionArray['unique_transaction_id'];
        $transactionArray['mc_gross']       = $transactionArray['api_response']['Amount'];
        $transactionArray['mc_currency']    = $transactionArray['api_response']['CurrencyCode'];
        $transactionArray['mc_fee']         = $transactionArray['mc_gross'] * YO_TX_PCT_FEE;

        $transactionArray['first_name']        = "Client";
        $transactionArray['last_name']         = "";
        $transactionArray['item_name']         = "";
        $transactionArray['payer_email']       = DEFAULT_CLIENT_EMAIL;
        $transactionArray['residence_country'] = "";
        $transactionArray['contact_phone']     = "";
        $transactionArray['payer_id']          = "";
        $transactionArray['address_street']    = "";
        $transactionArray['address_city']      = "";
        $transactionArray['address_state']     = "";
        $transactionArray['address_country']   = strtoupper(COUNTRY_ABBREVIATION);
        $transactionArray['address_zip']       = "";

        //prepare the variables required for successful transaction
        //Payment info from PayPal
        $paymentStatus   = $transactionArray['payment_status'];
        $ourPaypalEmail  = $transactionArray['receiver_email'];//our email where the payment goes
        $transID         = $transactionArray['txn_id'];
        $paymentAmount   = $transactionArray['mc_gross'];//payment_amount does not seem to work
        $paymentCurrency = $transactionArray['mc_currency'];
        $paymentFee      = $transactionArray['mc_fee'];

        //Client info from PayPal
        $firstname      = $transactionArray['first_name'];
        $lastname       = $transactionArray['last_name'];
        $itemname       = $transactionArray['item_name'];
        $clientEmail    = $transactionArray['payer_email'];
        $clientCountry  = $transactionArray['residence_country'];
        $clientPhone    = $transactionArray['contact_phone'];
        $itemIDNumber   = $transactionArray['item_number'];
        $clientID       = $transactionArray['payer_id'];
        $addressStreet  = $transactionArray['address_street'];
        $addressCity    = $transactionArray['address_city'];
        $addressState   = $transactionArray['address_state'];
        $addressCountry = $transactionArray['address_country'];
        $addressZip     = $transactionArray['address_zip'];
        

        $networkName     = getNetworkName($itemIDNumber);
        $valueInUGSh     = getValueInLocal($itemIDNumber);
        $ourPriceInUSD   = 0;
        $cardPIN         = "AUTO_LOAD";
        $loadedBy        = "AUTO_LOAD";
        $loadersIP       = "AUTO_LOAD";
        $serialNumber    = "AUTO_LOAD";
        $sessionUser     = "AUTO_LOAD";
        $dateWePurchased = date("Y-m-d H:i:s");
        $timeCardSent    = date("Y-m-d H:i:s");

        $networkCode = substr($itemIDNumber,0,5);
        $loadCode    = $networkArray[$countryAbbreviation][$networkCode]["loadCode"];            
        
        //switch to be parsed to form to know whether cards were sent
        $cardsArePresent     = "false";

        $transactionArray['networkName']     = $networkName;
        $transactionArray['valueInLocal']    = $valueInUGSh;
        $transactionArray['itemIDNumber']    = $itemIDNumber;
        $transactionArray['cardsArePresent'] = $cardsArePresent;
        $transactionArray['timeCardSent']    = $timeCardSent;

        $pointsSystemEnabled          = true;
        $clientIP                     = getIP();
        $transactionArray["clientIP"] = $clientIP;

        //card or auto transaction
        $autoDispatch = isAutoDispatch($itemIDNumber);
        $clientVerified = true;
    }

    // Extract saved order matching the following: Invoice, ItemID
    if($success)
    {
        $transactionArray = get_saved_order($transactionArray);
        $success = $transactionArray['order_exists'];
        $ipLocationArray = $transactionArray['order_data'];
    }

    // Check that txn_id has not been previously processed
    if($success)
    {
        $transactionArray = checkDuplicateTransID($transactionArray);
        $success = !$transactionArray['transaction_id_exists'];
        delete_saved_order($transactionArray);                
    }

    // Compare Address and IP Country
    if($success && YO_MATCH_COUNTRY)
    {
        $success = compare_pay_and_ip_country($transactionArray);
        $transactionArray['pay_and_ip_country_match'] = $success ? 1 : 0;
    }
    else
    {
        $transactionArray['pay_and_ip_country_match'] = -1;
    }

    //Display Airtime
    if($success)
    {
        //RECORD SALE
        $transactionArray['saleType'] = YO_SALE_TYPE;
        $transactionArray['saleCurrency'] = YO_SALE_CURRENCY;
        $transactionArray = recordSale($transactionArray);

        if($autoDispatch)
        {
            //AUTODISPATCH
            $transactionArray['autoDispatch'] = 1;
            $transactionArray = process_cart_auto_item($transactionArray);

            $transactionArray['comments'].="This is an auto dispatch\n";
            include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayAutoDispatch.php";
        }
        else
        {
            //SCRATCH CARD
            $transactionArray['autoDispatch'] = 0;

            $transactionArray = calculate_our_fee_local($transactionArray);
            $transactionArray['our_fee'] = $transactionArray['our_fee_in_local'];
            $transactionArray['saleType'] = YO_SALE_TYPE;
            $transactionArray = useCard($transactionArray);

            $cardsArePresent = $transactionArray['cardsArePresent'];
            $cardPIN         = $transactionArray['cardPIN'];

            if($cardsArePresent=="true")
            {
                //Mail the PIN to $clientEmail
                emailAirtime($transactionArray);

                //TODO: Print the card details to the screen
                include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayAccessNo.php";
            }
            else
            {
                $transactionArray['reason']            = "out_of_stock";
                $transactionArray                      = sendToOutQ($transactionArray);
                include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayOutOfStock.php";
            }
        }

        if(LIMIT_YO_CLIENT)
        {
            //Check if client exceeded the daily transaction limit
            $transactionArray = checkUserTransactionLimit($transactionArray);
            $purchase_limit_reached = $transactionArray['limit_reached'];
        }

        if(UPDATE_YO_CLIENT)
        {
            //Update Client info and calculate purchase and referral reward points.
            updateClientInfo($countryAbbreviation, $transactionArray, $connection);
        }

        //update receiver information.
        updateReceiverInfo($transactionArray);

        if(LIMIT_YO_CLIENT)
        {
            //if purchases exceed configured maximum then set customer status to blocked
            limitUserTransactions($transactionArray);

            //TODO: warn the user that they have reached the daily tx limit
            warnUserOnTransactionLimit($transactionArray, $countryAbbreviation, false);
        }

        // Email Debug
        if(DEBUG_PDT) email_ipn_post($transactionArray);
    }
    else if ($transactionArray['pay_and_ip_country_match'] == 0 && YO_MATCH_COUNTRY )
    {
        $transactionArray['comments'].         = "COUNTRY_MISMATCH";
        handle_country_mismatch($transactionArray);
        email_ipn_post($transactionArray);
    }
    else
    {
        //a duplicate transaction id will end up here
        //the special case of the duplicate was removed since we do not have order data
        //hence the Displays/DuplicateID.php was not showing everything

        $input_array = $transactionArray;
        include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayGeneralTempError.php";
        email_ipn_post($transactionArray);
    }

?>

<?php
    include_once INHERIT_PATH."src/GoogleSales.php";
    include_once INHERIT_PATH."src/FacebookMoMoPixel.php";
?>
