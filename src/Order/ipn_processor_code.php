<?

/*******************************************************************************
**  FILE: ipn_processor_code.php
**
**  FUNCTION: N/A
**
**  PURPOSE: Created IPN Processor Code based on return_page_code.php to process requests
**  coming in from PayPal IPN invokations.
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 25.Jul.2012
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 21.Jan.2013
**  Ported functionality from return_page_code to bring this up to speed
**  Replaced fgets and fput for curl functions
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs-AVMM, Kampala) DATE: 2015.12.01
**  Fixed bug that distorts debug comments
**
*********************************************************************************/

    include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/constants.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/SecurityFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/ObjectFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/OrderFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/SecurityFunctions.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Messaging/Messaging.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Yo/yo_functions.php';

    $debug = false;
    $countryAbbreviation = COUNTRY_ABBREVIATION;

    //cards.php
    global $networkArray;

    $success = false;

    // STEP 1: Read POST data
    // reading posted data from directly from $_POST causes serialization 
    // issues with array data in POST
    // reading raw POST data from input stream instead. 
    $raw_post_data = file_get_contents('php://input');
    $raw_post_array = explode('&', $raw_post_data);
    $myPost = array();
    foreach ($raw_post_array as $keyval)
    {
      $keyval = explode ('=', $keyval);
      if (count($keyval) == 2)
         $myPost[$keyval[0]] = urldecode($keyval[1]);
    }

    // Read the post from PayPal and add 'cmd' 
    $req = 'cmd=_notify-validate'; 
    if(function_exists('get_magic_quotes_gpc')) 
    {
        $get_magic_quotes_exists = true; 
    }

    foreach ($myPost as $key => $value) 
    {
        // Handle escape characters, which depends on setting of magic quotes
        if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1)
        {
            $value = urlencode(stripslashes($value)); 
        }
        else
        {
            $value = urlencode($value); 
        } 
        $req .= "&$key=$value"; 
    }

    // STEP 2: Post IPN data back to paypal to validate
    $ch = curl_init(PAYPAL_PAYMENT_URL);
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

    // In wamp like environments that do not come bundled with root authority certificates,
    // please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
    // of the certificate as shown below.
    // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
    if ( !($res = curl_exec($ch)) )
	{
        $success = false;

        // HTTP ERROR
        $from    = "From: ".SUPPORT_EMAIL;
        $to      = TECH_ADMIN;
        $subject = DOMAIN_NAME." ipn_processor";
        $msg     = "SendAT IPN invocation\n";
        $msg    .= "CURL ERROR";
        $msg    .= "Error : ".curl_error($ch);
        @mail($to,$subject,$msg,$from);
    }
    else
    {
        $success = true;
    }

    curl_close($ch);

    if($success)
    {
        if (strcmp ($res, "VERIFIED") == 0)
        {
            $success = true;
        }
        else if (strcmp ($res, "INVALID") == 0)
        {
            $success = false;
            email_ipn_post($_POST);
        }
        else
        {
            $success = false;
            $debug_array = $_POST;
            $debug_array['res'] = $res;
            email_ipn_post($debug_array);
            handle_paypal_service_unavailable($debug_array);
        }
    }

    if($success)
    {
        //$test_ipn         = $_POST['test_ipn'];

        //Obtain PayPal information as POST
        //store to keyarray variable coz of legacy code
        $keyarray         = $_POST;
        $paymentStatus    = $keyarray['payment_status'];
        $ourPaypalEmail   = $keyarray['receiver_email'];//our email where the payment goes
        $transID          = $keyarray['txn_id'];
        $paymentAmount    = $keyarray['mc_gross'];//payment_amount does not seem to work
        $paymentCurrency  = $keyarray['mc_currency'];
        $paymentFee       = $keyarray['mc_fee'];
        $payment_invoice  = $keyarray['invoice'];

        $firstname        = $keyarray['first_name'];
        $lastname         = $keyarray['last_name'];
        $itemname         = $keyarray['item_name'];
        $clientEmail      = $keyarray['payer_email'];
        $clientCountry    = $keyarray['residence_country'];
        $clientPhone      = $keyarray['contact_phone'];
        $itemIDNumber     = $keyarray['item_number'];
        $clientID         = $keyarray['payer_id'];
        $addressStreet    = $keyarray['address_street'];
        $addressCity      = $keyarray['address_city'];
        $addressState     = $keyarray['address_state'];
        $addressCountry   = $keyarray['address_country'];
        $addressZip       = $keyarray['address_zip'];
        $payer_status     = $keyarray['payer_status'];

        //Remove the country abbreviation if it exists in the invoice
        if(substr($keyarray['invoice'],0,2) == COUNTRY_ABBREVIATION)
        {
            $keyarray['invoice'] = substr($keyarray['invoice'],2);
        }

        $transactionArray = $keyarray;

        $transactionArray['item_id'] = $transactionArray['item_number'];
        $transactionArray = calculate_sale_price_foreign_auto($transactionArray);

        $networkName      = getNetworkName($itemIDNumber);
        $valueInUGSh      = getValueInLocal($itemIDNumber);
        $ourPriceInUSD    = $transactionArray['sale_price_in_foreign'];
        $cardPIN          = "AUTO_LOAD";
        $loadedBy         = "AUTO_LOAD";
        $loadersIP        = "AUTO_LOAD";
        $serialNumber     = "AUTO_LOAD";
        $sessionUser      = "AUTO_LOAD";
        $dateWePurchased  = date("Y-m-d H:i:s");
        $timeCardSent     = date("Y-m-d H:i:s");

        $networkCode = substr($itemIDNumber,0,5);
        $transactionArray['networkCode'] = $networkCode;

        $loadCode    = $networkArray[$countryAbbreviation][$networkCode]["loadCode"];
        $transactionArray['loadCode'] = $loadCode;

        //switch to be parsed to form to know whether cards were sent
        $cardsArePresent     = "false";

        $transactionArray['networkName']     = $networkName;
        $transactionArray['valueInLocal']    = $valueInUGSh;
        $transactionArray['itemIDNumber']    = $itemIDNumber;
        $transactionArray['cardsArePresent'] = $cardsArePresent;
        $transactionArray['timeCardSent']    = $timeCardSent;
        $transactionArray['ourPriceInUSD']   = $ourPriceInUSD;
        $transactionArray['cardPIN']         = $cardPIN;
        $transactionArray['dateWePurchased'] = $dateWePurchased;
        $transactionArray['loadedBy']        = $loadedBy;
        $transactionArray['loadersIP']       = $loadersIP;
        $transactionArray['serialNumber']    = $serialNumber;
        $transactionArray['sessionUser']     = $sessionUser;

        $pointsSystemEnabled          = true;

        //card or auto transaction
        $autoDispatch = isAutoDispatch($itemIDNumber) ? 1 : 0;
        $clientVerified = 1;
        $success = true;

        $transactionArray['num_reused_phone'] = 0;
        $transactionArray['num_reused_ip'] = 0;

        $transactionArray['client_whitelisted'] = 0;
        $transactionArray['client_new'] = 0;
        $transactionArray['client_blacklisted'] = 0;
        $transactionArray['clientVerified'] = 0;
        $transactionArray['client_already_logged_in'] = 0;
        $transactionArray['client_passed_checks'] = 0;
        $transactionArray['autoDispatch'] = $autoDispatch;
        $transactionArray['transaction_id_exists'] = 0;
        $transactionArray['pay_and_ip_country_match'] = 1;
        $transactionArray['pay_and_ip_state_match'] = 1;
        $transactionArray['flagged_phone_blocked'] = 0;
        $transactionArray['flagged_ip_blocked'] = 0;
        $transactionArray['order_exists'] = 0;
        
        $connection = connect2DB();
        $transactionArray["connection"] = $connection;

        //for now only allow IPN for auto dispatch only
        if(!ALLOW_IPN_CARD && !$transactionArray['autoDispatch'])
        {
            $success = false;
        }

        // Extract saved order matching the following: Invoice, ItemID
        if($success)
        {
            $transactionArray = get_saved_order($transactionArray);
            $success = $transactionArray['order_exists'];
            $ipLocationArray = $transactionArray['order_data'];
            $clientIP = $ipLocationArray['ip_address'];
            $transactionArray["clientIP"] = $clientIP;
        }

        //Check the payment amount with Ordered_Cart_Items
        if($success)
        {
            $transactionArray = verify_payment_amount($transactionArray);
            $success = $transactionArray['payment_amount_verified'];
        }

        //Check Payment Status
        if($paymentStatus != "Completed")
        {
            $success = false;
            $transactionArray['comments'].= "paymentStatus != Completed = ".$paymentStatus."\n";
        }

        // Check that receiver_email is your Primary PayPal email
        if($success && $ourPaypalEmail != PAYPAL_EMAIL)
        {
            $success = false;
            $transactionArray['comments'].= "receiver_email != ".PAYPAL_EMAIL." = ".$ourPaypalEmail."\n";
        }

        //TODO: Use the values of txn_type or reason_code of a VERIFIED notification to 
        //determine your processing actions

        // Check that txn_id has not been previously processed
        if($success)
        {
            $transactionArray = checkDuplicateTransID($transactionArray);
            $success = !$transactionArray['transaction_id_exists'];
        }

        // Compare PayPal and IP Country
        if($success && MATCH_COUNTRY)
        {
            $success = compare_pay_and_ip_country($transactionArray);
            $transactionArray['pay_and_ip_country_match'] = $success ? 1 : 0;
        }

        // Compare PayPal and IP State
        if($success && MATCH_STATE)
        {
            $success = compare_pay_and_ip_state($transactionArray);
            $transactionArray['pay_and_ip_state_match'] = $success ? 1 : 0;
        }

        //Check for re-used IP
        if($success && FLAG_REUSED_IP)
        {
            $transactionArray = flag_reused_ip($transactionArray);
        }

        //Check for re-used Telephone
        if($success && FLAG_REUSED_TEL)
        {
            $transactionArray = flag_reused_telephone($transactionArray);
        }

        //Check for re-used Receiver
        if($success && FLAG_REUSED_RECEIVER)
        {
            $transactionArray = flag_reused_receiver_phone($transactionArray);
            $transactionArray = flag_reused_receiver_phone_used_cards($transactionArray);
        }

        //Check for multi-state purchase by same client in 24 hours
        if($success && FLAG_MULTI_STATE)
        {
            $transactionArray = detect_diff_state($transactionArray);
        }

        //Handle flagged purchase case
        if($success &&
           BLOCK_FLAGGED_PURCHASE &&
           ($transactionArray['num_reused_phone'] > 0 ||
            $transactionArray['num_reused_ip'] > 0 ||
            $transactionArray['num_diff_state'] > 0))
        {
            $transactionArray = block_flagged_purchase($transactionArray);
            $success = !$transactionArray['flagged_phone_blocked'] && !$transactionArray['flagged_ip_blocked'];
        }

        //Check Black List
        if($success && 
           BLACK_LIST_ACTIVE &&
           clientOnBlackList($clientEmail,$countryAbbreviation,$connection))
        {
            $transactionArray['comments'].= "CLIENT_BLACKLISTED\n";
            handleBlackListUserState($countryAbbreviation,$transactionArray,$ipLocationArray,$connection); 
            $clientVerified = 0;
            $transactionArray['clientVerified'] = $clientVerified;
            $transactionArray['client_blacklisted'] = 1;
            $transactionArray['client_new'] = 0;
            $success = $clientVerified;
        }

        //Detect New Client
        if($success &&
           WHITE_LIST_ACTIVE &&
           !clientOnWhiteList($clientEmail,$countryAbbreviation,$connection))
        {
            handleNewClientState($countryAbbreviation,$transactionArray,$ipLocationArray,$connection);
            $clientVerified = 0;
            $transactionArray['clientVerified'] = $clientVerified;
            $transactionArray['client_whitelisted'] = 0;
            $transactionArray['client_new'] = 1;
            $success = $clientVerified;
        }

        //Display Airtime
        if($success)
        {
            //RECORD SALE
            $transactionArray = recordSale($transactionArray);

            if($autoDispatch)
            {
                //AUTODISPATCH
                $transactionArray['autoDispatch'] = 1;
                $transactionArray = process_cart_auto_item($transactionArray);
            }
            else
            {
                //SCRATCH CARD
                $transactionArray['autoDispatch'] = 0;

                $transactionArray = useCard($transactionArray);

                $cardsArePresent = $transactionArray['cardsArePresent'];
                $cardPIN         = $transactionArray['cardPIN'];

                if($cardsArePresent=="true")
                {
                    //Mail the PIN to $clientEmail
                    emailAirtime($transactionArray);

                    //TODO: Print the card details to the screen
                    include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayAccessNo.php";
                }
                else
                {
                    $transactionArray['reason']            = "out_of_stock";
                    $transactionArray                      = sendToOutQ($transactionArray);
                    include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayOutOfStock.php";
                }
            }

            delete_saved_order($transactionArray);

            //Check if client exceeded the daily transaction limit
            $transactionArray = checkUserTransactionLimit($transactionArray);
            $purchase_limit_reached = $transactionArray['limit_reached'];

            //Update Client info and calculate purchase and referral reward points.
            updateClientInfo($countryAbbreviation, $transactionArray, $connection);

            //update receiver information.
            updateReceiverInfo($transactionArray);
            updatePromoReceiverInfo($transactionArray);

            //if purchases exceed configured maximum then set customer status to blocked
            limitUserTransactions($transactionArray);

            //TODO: warn the user that they have reached the daily tx limit
            warnUserOnTransactionLimit($transactionArray, $countryAbbreviation, false);

            record_sale_to_google_analytics($transactionArray);

            if(DEBUG_IPN) email_ipn_post($transactionArray);
        }
        else if($paymentStatus != "Completed")
        {
            if($paymentStatus == "Refunded")
            {
            }
            else if($paymentStatus == "Reversed")
            {
            }
            else if($paymentStatus == "Canceled_Reversal")
            {
            }
            else if($paymentStatus == "")
            {
            }
            else
            {
                handleIncompletePaymentState($countryAbbreviation, $transactionArray, $ipLocationArray, $connection);

                if(DEBUG_IPN) email_ipn_post($transactionArray);
            }

        }
        else if($transactionArray['autoDispatch'] == 0 && ALLOW_IPN_CARD == false)
        {
            $transactionArray['comments']         .= "CARD_NOT_IPN_SUPPORTED\n";

            //if(DEBUG_IPN) email_ipn_post($transactionArray);
        }
        else if($transactionArray['transaction_id_exists'])
        {
            $transactionArray['comments'].="number of transID rows: $num_rows\n";

            //TODO: its a duplicate transactionID
            include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DuplicateID.php";

            if(DEBUG_IPN) email_ipn_post($transactionArray);
        }
        else if($transactionArray['order_exists'] == 0)
        {
            $transactionArray['comments']         .= "NO_ORDER_EXISTS\n";
            include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DuplicateID.php";

            if(DEBUG_IPN) email_ipn_post($transactionArray);
        }
        else if ($transactionArray['pay_and_ip_country_match'] == 0 && MATCH_COUNTRY )
        {
            $transactionArray['comments']         .= "COUNTRY_MISMATCH\n";
            handle_country_mismatch($transactionArray);

            if(DEBUG_IPN) email_ipn_post($transactionArray);
        }
        else if ($transactionArray['pay_and_ip_state_match'] == 0 && MATCH_STATE )
        {
            $transactionArray['comments']         .= "STATE_MISMATCH\n";
            handle_country_mismatch($transactionArray);

            if(DEBUG_IPN) email_ipn_post($transactionArray);
        }
        else if (BLOCK_FLAGGED_PURCHASE && 
                ($transactionArray['flagged_phone_blocked'] || $transactionArray['flagged_ip_blocked']))
        {
            handle_flagged_purchase($transactionArray);

            if(DEBUG_IPN) email_ipn_post($transactionArray);
        }
        else if($transactionArray['client_blacklisted'] == 1 && BLACK_LIST_ACTIVE)
        {
            $input_array = $transactionArray;
            include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayManualProcInfo.php";

            if(DEBUG_IPN) email_ipn_post($transactionArray);
        }
        else if($transactionArray['client_new'] == 1 && WHITE_LIST_ACTIVE)
        {
            $input_array = $transactionArray;
            include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayManualProcInfo.php";

            if(DEBUG_IPN) email_ipn_post($transactionArray);
        }
        else
        {
            $transactionArray['comments']         .= "UNKNOWN\n";
            handle_return_page_catch_all($transactionArray);

            email_ipn_post($transactionArray);
        }

        // Email Debug
        //if(DEBUG_IPN) email_ipn_post($transactionArray);
    }

?>

<?
    //include_once INHERIT_PATH."src/GoogleSales.php";
?>