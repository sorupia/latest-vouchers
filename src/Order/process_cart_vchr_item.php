<?

/*******************************************************************************
**   FILE: process_cart_vchr_item.php
**
**   FUNCTION: process_cart_vchr_item
**
**   PURPOSE: Process Cart Item & Store Completed Order
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, 113-3353 16A Avenue NW, Edmonton, AB)   DATE: 08.Sep.2015
**
**
*********************************************************************************/

include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/generate_unique_voucher_number.php";

function process_cart_vchr_item($input_array)
{
    $input_array = generate_unique_voucher_number($input_array);
    $invoke_success = $input_array['invoke_success'];

    if($invoke_success)
    {
        $input_array['used_cards_table'] = "UsedCards";
        $input_array = sms_receiver_success_voucher($input_array);
        $input_array = save_in_used_cards($input_array);
        email_client_success_voucher($input_array);

        if($input_array['order_data']['receiver_email'] != '')
        {
            email_receiver_success_voucher($input_array);
        }

        notifyOnPurchase($input_array);
    }
    else
    {
        email_support_queued_voucher($input_array);
        $input_array = sendToOutQ($input_array);
    }

    return $input_array;
}

?>