<?
/*******************************************************************************
**   FILE: src/Order/receiver_form_vchr.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Content file for taking recipient's order
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 03.Jul.2012
**
**   ADAPTED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 22.May.2015
**   Adapted for UgandaVouchers.com
**
*********************************************************************************/

?>
<div id="body">
    <div class="content">
        <div id="section">
            <div id="form_div">
                <h3>Enter Receiver Details</h3>
                <br>
                <form action="process_vchr.php" method="post" name="receiver_form">
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td height="50" valign="top">Voucher Value(UGX) and Price (USD)</td>
                                <td valign="middle">
                                    <select name="item_id" id="item_id" onblur="select_voucher();" required="required">
                                    <?php
                                        include_once $_SERVER['DOCUMENT_ROOT']."UgandaVouchers"."/adm2/itemIDDropDown.php";
                                        getDropDownSelected($countryAbbreviation, $_POST['item_id']);
                                    ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Receiver's Phone Number</td>
                                <td>
                                    <div id="err1" style="color: red; display: none;"></div>
                                    <?
                                        //Number of digits in recipient phone after the country code
                                        $country_code_length = strlen(COUNTRY_CODE);
                                        $rx_phone_length_no_country_code = RX_PHONE_LENGTH - $country_code_length - 1;
                                        $rx_phone_pattern = "[\+]{1}[".COUNTRY_CODE."]{".$country_code_length."}[\d]{".$rx_phone_length_no_country_code."}";
                                    ?>
                                    <input name="receiver_phone"
                                        class="select_form"
                                        pattern="<?=$rx_phone_pattern?>"
                                        maxlength="<?=RX_PHONE_LENGTH?>"
                                        id="receiver_phone"
                                        value="+<?=COUNTRY_CODE?>"
                                        onblur="mobile1_onblur();"
                                        required=""
                                        type="text">
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Re-enter Receiver's Phone Number</td>
                                <td> 
                                    <div id="err2" style="color: red; display: none;"></div>
                                    <?//Todo: Configurable items in pattern and value ?>
                                    <input name="phoneConfirm"
                                        class="select_form"
                                        pattern="<?=$rx_phone_pattern?>"
                                        maxlength="<?=RX_PHONE_LENGTH?>" 
                                        id="phoneConfirm"
                                        value="+<?=COUNTRY_CODE?>"
                                        onblur="mobile2_onblur();"
                                        required=""
                                        type="text">
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Receiver's First Name </td>
                                <td><input name="receiver_first_name" class="select_form" id="receiver_first_name" value="" required="" type="text"></td>
                            </tr>
                            <tr>
                                <td valign="top">Receiver's Last Name  </td>
                                <td><input name="receiver_last_name" class="select_form" id="receiver_last_name" value="" required="" type="text"></td>
                            </tr>
                            <tr>
                                <td valign="top">Receiver's Region </td>
                                <td>
                                    <select name="receiver_region">
                                        <option value="Kampala">Kampala</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td height="70"></td>
                                <td colspan="2">
                                    <?//Todo: incoming post for supermarket ?>
                                    <input name="supermarket"         id="supermarket"         value="<?=$_POST['network_name']?>" type="hidden">
                                    <input name="payment_option"      id="payment_option"      value="<?=PAYMENT_OPTION_PAYPAL?>" type="hidden">
                                    <input name="network_id"          id="network_id"          value="<?=$input_array['network_id']?>" type="hidden">
                                    <input name="airtime_load_option" id="airtime_load_option" value="<?=$input_array['airtime_load_option']?>" type="hidden">
                                    <input name="ip_address"          id="ip_address"          value="<?=$input_array['ip_address']?>" type="hidden">
                                    <input name="ip_country"          id="ip_country"          value="<?=$input_array['ip_country']?>" type="hidden">
                                    <input name="ip_state"            id="ip_state"            value="<?=$input_array['ip_state']?>" type="hidden">
                                    <input name="ip_city"             id="ip_city"             value="<?=$input_array['ip_city']?>" type="hidden">
                                    <input name="item_id"             id="item_id"             value="<?=$input_array['item_id']?>" type="hidden">
                                    <input name="item_name"           id="item_name"           value="<?=$input_array['item_name']?>" type="hidden">
                                    <input name="hosted_button_id"    id="hosted_button_id"    value="<?=$input_array['hosted_button_id']?>" type="hidden">
                                    <input name="send"                id="send" onclick="send_onclick();" value="send now" class="select_form" style="width: 100px; background-color: #56919C; color: #ffffff; border-radius: 5px; padding-top: 1px;" type="submit">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
        <div id="sidebar">
            <div class="contact">
                <h3><?=$_POST['network_name']?></h3>
                <img src="images/logos/<?=$input_array['network_id']?>.png" class="receiver_auto_logo">
            </div>
            <div id="voucher_div">
                <div style="width: 100%; height: auto; float: left; margin-left: 20px; margin-top:10px; font-weight: bold; font-size: 16px;">  
                    Recipient will be hand delivered a voucher like this.
                </div>
                <img id="vouchers" src="images/vouchers/<?=$input_array['network_id']?>.png">
            </div>
            <div style="width: 90%; height: auto; font-size: 15px; float: left; margin-left:20px; margin-top:10px; margin-bottom:5px; text-align: center; font-weight: bold;">
                Pay By PayPal (Bank or Credit Card).
            </div>
            <div style="width: 100%; float: left; margin-top: -10px;">
                <img src="images/logos/paypal_big.png" style="width: 95%;">
            </div>
        </div>
    </div>
</div>