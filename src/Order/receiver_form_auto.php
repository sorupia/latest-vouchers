<?
/*******************************************************************************
**   FILE: receiver_form_auto.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Display the receiver form. 
**            Also displays various form content based on passed variables.
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 03.Jul.2012
*********************************************************************************/
?>

<div class="receiptDetails">
    <div class="spacing">
        <?
            if($input_array['airtime_load_option'] == AIRTIME_LOAD_OPTION_CARD)
            {
                //Display the network logo based of the supplied network id
        ?>
        <div class="floatLeft">
            <div class="carrier">
                <? echo "<img src='images/telecoms/".$_POST['network_id'].".gif' alt='".$_POST['network_id']."' />"; ?>
            </div>
        </div>
        <?
            }
            else
            {
                //Display the network logo for all networks that provide auto load
        ?>
        <div class="floatLeft">
            <div class="carrier">
                <?
                    //Display logos of all networks that provide auto load
                    global $auto_load_network_array;
                    $network_ids = array_keys($auto_load_network_array[COUNTRY_ABBREVIATION]);
                    for($i = 0; $i < count($network_ids); $i++)
                    {
                        if($i > 0) echo "<br><br>";
                        $this_network_id = $network_ids[$i];
                        echo "<img src='images/telecoms/".$this_network_id.".gif' alt='".$this_network_id."' />";
                    }
                ?>
            </div>
        </div>
        <?
            }
        ?>
        <div class="floatLeft">
            <? 
                $form_height = "330px";
            ?>
            <div class="form" style="height:<?=$form_height?>;">
            <?
                if($input_array['error'] != '')
                {
                    echo "<p align=\"center\"><strong><font color=\"#FF0000\"> ";
                    echo "Error: ".$input_array['error']; 
                    echo "</font></strong></p>";
                }
            ?>
                <h4>Enter airtime receiver details</h4>
                <form name="receiver_form" id="receiver_form" method="post" action="process_auto.php" >
                    <div class="row" style="padding-right:20px;">
                        <label for="receiver_amount_in_local" style="width:200px;">Amount to be received in <?=LOCAL_CURRENCY?></label>
                    <?
                        if($input_array['airtime_load_option'] == AIRTIME_LOAD_OPTION_CARD)
                        {
                            //Show amount in local
                    ?>
                        <?=$_POST['item_name'];?>
                        <input type="hidden" name="receiver_amount_in_local" id="receiver_amount_in_local" value="<?=substr($input_array['item_id'],9)?>" />
                        <a href="receiver-auto.php" title="Receiver Form" onfocus="this.blur();"><strong>(or enter OTHER amount HERE)</strong></a>
                    <?
                        }
                        else
                        {
                            //Request for amount in local
                    ?>                        
                        <input type="text" name="receiver_amount_in_local" id="receiver_amount_in_local" value="<?=$input_array['receiver_amount_in_local']?>" class="input" />
                    <?
                        }
                    ?>
                    </div>
                    <div class="row" style="padding-right:20px;">
                        <label for="receiver_phone" style="width:200px;">Receiver's Phone</label>
                        <input type="text" name="receiver_phone" id="receiver_phone" value="<?=$input_array['receiver_phone']?>" class="input" />
                    </div>
                    <div class="row" style="padding-right:20px;">
                        <label for="phoneConfirm" style="width:200px;">Re-enter Receiver's Phone</label>
                        <input type="text" name="phoneConfirm" id="phoneConfirm" value="<?=$input_array['phoneConfirm']?>" class="input" />
                    </div>
                    <div class="row" style="padding-right:20px;">
                        <label for="receiver_first_name" style="width:200px;">Receiver's First Name (Optional)</label>
                        <input type="text" name="receiver_first_name" id="receiver_first_name" value="<?=$input_array['receiver_first_name']?>" class="input" />
                    </div>
                    <div class="row" style="padding-right:20px;">
                        <label for="receiver_email" style="width:200px;">Receiver's Email (Optional)</label>
                        <input type="text" name="receiver_email" id="receiver_email" value="<?=$input_array['receiver_email']?>" class="input" />
                    </div>
                    <div class="row" style="padding-right:20px;">
                        <label for="airtime_load_option" style="width:200px;">Airtime To Be Received As</label>
                    <?
                        if($input_array['airtime_load_option'] == AIRTIME_LOAD_OPTION_CARD && 
                           this_network_has_auto_load($input_array) && AUTO_LOAD_ENABLED )
                        {
                            //Only display the two options when we have selected a card and if this network has auto load
                    ?>
                        <select name="airtime_load_option" size="2">
                            <option value="card">Access Number (PIN sent as SMS)</option>
                            <option value="auto" selected="selected">Auto Load (PIN-less topup)</option>
                      </select>
                    <?
                        }
                        else if($input_array['airtime_load_option'] == AIRTIME_LOAD_OPTION_CARD)
                        {
                    ?>
                        Access Number (PIN sent as SMS)
                        <input type="hidden" name="airtime_load_option" id="airtime_load_option" value="<?=AIRTIME_LOAD_OPTION_CARD?>" />
                    <?
                        }
                        else
                        {
                    ?>
                        Auto Load (PIN-less topup)
                        <input type="hidden" name="airtime_load_option" id="airtime_load_option" value="<?=AIRTIME_LOAD_OPTION_AUTO?>" />
                    <?
                        }
                    ?>
                    </div>
                    <?
                        if(YO_PAYMENT_ENABLED)
                        {
                    ?>
					<div class="row" style="padding-right:20px;">
                        <label for="payment_option" style="width:200px;">Pay by</label>
                        <select name="payment_option" size="2">
                            <option value="<?=PAYMENT_OPTION_PAYPAL?>" selected="selected">PayPal (Bank or Credit Card)</option>
                            <option value="<?=PAYMENT_OPTION_UG_MOMO?>">Mobile Money (Telecom rates apply)</option>
                      </select>
                    </div>
                    <?
                        }
                        else
                        {
                    ?>
                    <div class="row" style="padding-right:20px;">
                        <label for="payment_option" style="width:200px;">Pay by</label>
                        <select name="payment_option" size="2">
                            <option value="<?=PAYMENT_OPTION_PAYPAL?>" selected="selected">PayPal (Bank or Credit Card)</option>
                        </select>
                    </div>
                    <?
                        }
                    ?>
                    <?
                        if($input_array['airtime_load_option'] == AIRTIME_LOAD_OPTION_AUTO && !AUTO_LOAD_ENABLED)
                        {
                            //disable the submit button since auto load is not enabled
                    ?>
                    <div class="row" style="padding-right:20px;">
                        <label for="disabled_airtime_load_option" style="width:500px;">
                            Unfortunately the airtime auto load option is not available at the moment. 
                            Please send a voucher by clicking <a href="<?=INHERIT_SITE?>">HERE</a>
                        </label>
                    </div>
                    <?
                        }
                        else
                        {
                            //Display a message about auto load not being possible
                    ?>
                    <div align="right" class="row" style="padding-right:20px;">
                        <input type="image" src="images/layout/send_now.png" name="submit" id="submit" />
                        <input type="hidden" name="ip_address"       id="ip_address"        value="<?=$input_array['ip_address']?>" />
                        <input type="hidden" name="ip_country"       id="ip_country"        value="<?=$input_array['ip_country']?>" />
                        <input type="hidden" name="ip_state"         id="ip_state"          value="<?=$input_array['ip_state']?>" />
                        <input type="hidden" name="ip_city"          id="ip_city"           value="<?=$input_array['ip_city']?>" />
                        <input type="hidden" name="hosted_button_id" id="hosted_button_id"  value="<?=$_POST['hosted_button_id']?>" />
                        <input type="hidden" name="network_id"       id="network_id"        value="<?=$_POST['network_id']?>" />
                        <input type="hidden" name="item_id"          id="item_id"           value="<?=$_POST['item_id']?>" />
                        <input type="hidden" name="item_name"        id="item_name"         value="<?=$_POST['item_name']?>" />
                    </div>
                    <?
                        }
                    ?>
                </form>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>
        <?
            echo "<p align=\"center\"><strong><font color=\"#FF0000\"> ";
            echo $input_array['error']; 
            echo "</font></strong></p>";
        ?>