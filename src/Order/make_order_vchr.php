<?
/*******************************************************************************
**   FILE: make_order_vchr.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Stores user order for processing on succesful payment
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 09.Jul.2012
**
**   ADAPTED BY: Arthur Ntozi (3nitylabs, 6 Kataza Close, Kampala) DATE: 04.June.2015
**   Adapted for UgandaVouchers.com from src/General/make_order_auto.php
**
*********************************************************************************/
    
    
    function make_order_vchr($input_array)
    {
        
        
        $date_added              = date("Y-m-d H:i:s");
        $session_id              = $input_array['session_id'];
        $connection              = $input_array['connection'];
        $ip_country              = $input_array['ip_country'];
        $ip_state                = $input_array['ip_state'];
        $ip_city                 = $input_array['ip_city'];
        $ip_address              = $input_array['ip_address'];
        $item_id                 = $input_array['item_id'];
        $button_id               = $input_array['hosted_button_id'];
        $receiver_network_id     = $input_array['network_id'];
        $receiver_email          = $input_array['receiver_email'];
        $receiver_phone          = $input_array['receiver_phone'];
        $receiver_first_name     = $input_array['receiver_first_name'];
        $receiver_last_name      = $input_array['receiver_last_name'];
        $payment_option          = $input_array['payment_option'];
        $airtime_load_option     = $input_array['airtime_load_option'];
        $sale_price_in_foreign   = $input_array['sale_price_in_foreign'];
        $receiver_region         = $input_array['receiver_region'];
        $receiver_amount_in_local= getValueInLocal($item_id);
        $receiver_currency       = LOCAL_CURRENCY;
        $client_currency         = SALE_CURRENCY;

        $query = "INSERT INTO Ordered_Cart_Items ";
        $query.= "SET ";
        $query.= "date_added = '".mysql_real_escape_string($date_added)."', ";
        $query.= "session_id = '".mysql_real_escape_string($session_id)."', ";
        $query.= "receiver_first_name = '".mysql_real_escape_string($receiver_first_name)."', ";
        $query.= "receiver_last_name = '".mysql_real_escape_string($receiver_last_name)."', ";
        $query.= "receiver_phone = '".$receiver_phone."', ";
        $query.= "receiver_email = '".$receiver_email."', ";
        $query.= "receiver_network_id = '".$receiver_network_id."', ";
        $query.= "item_id = '".$item_id."', ";
        $query.= "button_id = '".$button_id."', ";
        $query.= "ip_country = '".mysql_real_escape_string($ip_country)."', ";
        $query.= "ip_state = '".mysql_real_escape_string($ip_state)."', ";
        $query.= "ip_city = '".mysql_real_escape_string($ip_city)."', ";
        $query.= "ip_address = '".$ip_address."', ";
        $query.= "receiver_amount_in_local = '".$receiver_amount_in_local."', ";
        $query.= "payment_option = '".$payment_option."', ";
        $query.= "airtime_load_option = '', ";
        $query.= "receiver_currency = '".$receiver_currency."', ";
        $query.= "client_currency = 'UGX', ";
        $query.= "receiver_region = '".$receiver_region."', ";
        $query.= "sale_price_in_foreign = '".$sale_price_in_foreign."' ";

        $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
        $input_array['order_id'] = mysql_insert_id($connection);

        return $input_array;
    }
?>