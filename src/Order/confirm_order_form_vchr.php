<?
/*******************************************************************************
**   FILE: confirm_order_form_vchr.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Prompts user to confirm the receiver form details. 
**            Also performs checks to validate input data.
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 03.Jul.2012
**
**   ADAPTED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 25.May.2015
**   Adapted for UgandaVouchers.com from src/Order/confirm_order_form_auto.php
**
*********************************************************************************/

    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/OrderFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Security/SecurityFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/constants.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Yo/yo_functions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Constants/auto_load_constants.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/calculate_sale_price_foreign_vchr.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/validate_receiver_form_vchr.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/verify_receiver_phone_network_auto.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Security/verify_receiver_lower_limit.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/verify_receiver_amount.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Messaging/MessagingFunctions.php";

    $input_array = $_POST;

    //Remove any commas in the receiver_amount_in_local
    $quotes_array = array(",");
    $input_array['receiver_amount_in_local'] = substr($input_array['item_id'],9); 

    //Validate receiver form
    $input_array = validate_receiver_form_vchr($input_array);
    $success = $input_array['success'];

    //Connect to database
    if($success == 1)
    {
        $input_array["connection"] = connect2DB();
    }

    //Verify this receiver's status
    if($success == 1 && VERIFY_RECEIVER_STATUS)
    {
        $input_array = verify_receiver_status($input_array);
        $success = !$input_array['receiver_status_blocked'];
    }

    //Verify this receiver's limit
    if($success == 1 && LIMIT_RECEIVER)
    {
        $input_array = verify_receiver_limit($input_array);
        $success = !$input_array['receiver_limit_exceeded'];
    }

    //Verify receiver amount low limit
    if($success == 1)
    {
        $input_array = verify_receiver_lower_limit($input_array);
        $success = !$input_array['receiver_amount_low'];
    }

    //Verify this IP limit
    if($success == 1 && LIMIT_IP)
    {
        $input_array = verify_ip_limit($input_array);
        $success = !$input_array['ip_limit_exceeded'];
    }

    //Calculate amount to be paid
    if($success == 1)
    {
        $input_array = calculate_sale_price_foreign_vchr($input_array);
        include_once $_SERVER['DOCUMENT_ROOT']."/". ACTIVE_THEME_PATH ."/src/Order/submit_order_form_vchr.php";
    }
    else
    {
        //REPOST information back to form
        echo'<form action="receiver_vchr.php" method="post" name="receiver_form" id="receiver_form">
			<input type="hidden" name="receiver_amount_in_foreign" value="'.$_POST['receiver_amount_in_foreign'].'" />
             <input type="hidden" name="receiver_amount_in_local" value="'.$_POST['receiver_amount_in_local'].'" />
             <input type="hidden" name="receiver_phone"           value="'.$_POST['receiver_phone'].'" />
             <input type="hidden" name="phoneConfirm"             value="'.$_POST['phoneConfirm'].'" />
             <input type="hidden" name="receiver_first_name"      value="'.$_POST['receiver_first_name'].'" />
			 <input type="hidden" name="receiver_last_name"       value="'.$_POST['receiver_last_name'].'" />
             <input type="hidden" name="receiver_email"           value="'.$_POST['receiver_email'].'" />
             <input type="hidden" name="payment_option"           value="'.$_POST['payment_option'].'">
             <input type="hidden" name="airtime_load_option"      value="'.$_POST['airtime_load_option'].'">
             <input type="hidden" name="ip_address"               value="'.$_POST['ip_address'].'" />
             <input type="hidden" name="ip_country"               value="'.$_POST['ip_country'].'" />
             <input type="hidden" name="ip_state"                 value="'.$_POST['ip_state'].'" />
             <input type="hidden" name="ip_city"                  value="'.$_POST['ip_city'].'" />
             <input type="hidden" name="network_id"               value="'.$_POST['network_id'].'" />
             <input type="hidden" name="item_id"                  value="'.$_POST['item_id'].'" />
             <input type="hidden" name="item_name"                value="'.$_POST['item_name'].'" />
             <input type="hidden" name="hosted_button_id"         value="'.$_POST['hosted_button_id'].'">
             <input type="hidden" name="error"                    value="'.$input_array['error'].'">
             </form>';

        echo'<script language="JavaScript" type="text/javascript">
                window.onload=function(){ window.document.receiver_form.submit(); }
             </script>';
    }
?>
