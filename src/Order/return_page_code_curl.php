<?php
    //2010-12-09: Added code to maintain serial number in UsedCards
    //2010-12-09: Added code to include PayPal attribute "payment_fee"
    //20101214: Replaced Client update code with updateClientInfo
    
    include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/constants.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/SecurityFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/ObjectFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/OrderFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/SecurityFunctions.php';
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Messaging/MessagingFunctions.php";

    $debug = false;
    $countryAbbreviation = COUNTRY_ABBREVIATION;

    //PayPalDetails.php
    global $PDT_TOKEN_ARRAY;

    //cards.php
    global $networkArray;

    $auth_token         = PDT_TOKEN;

    //Read the post from PayPal system and add 'cmd'
    $req = 'cmd=_notify-synch';
    $tx_token = $_GET['tx'];    

    $req .= "&tx=$tx_token&at=$auth_token";

    $ch = curl_init();
    //curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_URL, PAYPAL_PAYMENT_URL);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    //set cacert.pem verisign certificate path in curl using 'CURLOPT_CAINFO' field here,
    //if your server does not bundled with default verisign certificates.
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: ".PAYPAL_HOSTNAME.""));
    //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: ".PAYPAL_HOSTNAME."", "Connection: close"));
    $res = curl_exec($ch);
    curl_close($ch);

    if(!$res)
    {
        //HTTP ERROR
    }
    else
    {
        // parse the data
        $lines = explode("\n", $res);
        $keyarray = array();
        if (strcmp ($lines[0], "SUCCESS") == 0) 
        {
            for ($i=1; $i<count($lines);$i++)
            {
                list($key,$val) = explode("=", $lines[$i]);
                $keyarray[urldecode($key)] = urldecode($val);
            }

            //Payment info from PayPal
            $paymentStatus   = $keyarray['payment_status'];
            $ourPaypalEmail  = $keyarray['receiver_email'];//our email where the payment goes
            $transID         = $keyarray['txn_id'];
            $paymentAmount   = $keyarray['mc_gross'];//payment_amount does not seem to work
            $paymentCurrency = $keyarray['mc_currency'];
            $paymentFee      = $keyarray['mc_fee'];

            //Client info from PayPal
            $firstname      = $keyarray['first_name'];
            $lastname       = $keyarray['last_name'];
            $itemname       = $keyarray['item_name'];
            $clientEmail    = $keyarray['payer_email'];
            $clientCountry  = $keyarray['residence_country'];
            $clientPhone    = $keyarray['contact_phone'];
            $itemIDNumber   = $keyarray['item_number'];
            $clientID       = $keyarray['payer_id'];
            $addressStreet  = $keyarray['address_street'];
            $addressCity    = $keyarray['address_city'];
            $addressState   = $keyarray['address_state'];
            $addressCountry = $keyarray['address_country'];
            $addressZip     = $keyarray['address_zip'];
            
            $transactionArray = $keyarray;

            $networkName         = getNetworkName($itemIDNumber);
            $valueInUGSh         = getValueInLocal($itemIDNumber);
            $ourPriceInUSD       = 0;
            $dateWePurchased     = 0;
            $timeCardSent        = date("Y-m-d H:i:s");

            $networkCode = substr($itemIDNumber,0,5);
            $loadCode    = $networkArray[$countryAbbreviation][$networkCode]["loadCode"];            
            
            //switch to be parsed to form to know whether cards were sent
            $cardsArePresent     = "false";

            $transactionArray['networkName']     = $networkName;
            $transactionArray['valueInLocal']    = $valueInUGSh;
            $transactionArray['itemIDNumber']    = $itemIDNumber;
            $transactionArray['cardsArePresent'] = $cardsArePresent;
            $transactionArray['timeCardSent']    = $timeCardSent;

            $pointsSystemEnabled          = true;
            $clientIP                     = getIP();
            $transactionArray["clientIP"] = $clientIP;

            //card or auto transaction
            $autoDispatch = isAutoDispatch($itemIDNumber);
            $clientVerified = 1;
            $success = true;

            $transactionArray['num_reused_phone'] = 0;
            $transactionArray['num_reused_ip'] = 0;

            $transactionArray['client_whitelisted'] = 0;
            $transactionArray['client_new'] = 0;
            $transactionArray['client_blacklisted'] = 0;
            $transactionArray['clientVerified'] = 0;
            $transactionArray['client_already_logged_in'] = 0;
            $transactionArray['client_passed_checks'] = 0;
            
            $connection = connect2DB();
            $transactionArray["connection"] = $connection;

            // Extract saved order matching the following: Invoice, ItemID
            if($success)
            {
                $transactionArray = get_saved_order($transactionArray);
                $success = $transactionArray['order_exists'];
                $ipLocationArray = $transactionArray['order_data'];
            }

            //Check Payment Status
            if($paymentStatus != "Completed")
            {
                $success = false;
                $transactionArray['debug_output'].= "paymentStatus != Completed\n";
            }

            // Check that receiver_email is your Primary PayPal email
            if($success && $ourPaypalEmail != PAYPAL_EMAIL)
            {
                $success = false;
                $transactionArray['debug_output'].= "receiver_email != ".PAYPAL_EMAIL."\n";
            }


            // Check that txn_id has not been previously processed
            if($success)
            {
                $transactionArray = checkDuplicateTransID($transactionArray);
                $success = !$transactionArray['transaction_id_exists'];
                delete_saved_order($transactionArray);                
            }

            // Compare PayPal and IP Country
            if($success && MATCH_COUNTRY)
            {
                $success = compare_pay_and_ip_country($transactionArray);
                $transactionArray['pay_and_ip_country_match'] = $success ? 1 : 0;
            }

            // Compare PayPal and IP State
            if($success && MATCH_STATE)
            {
                $success = compare_pay_and_ip_state($transactionArray);
                $transactionArray['pay_and_ip_state_match'] = $success ? 1 : 0;
            }

            //Check for re-used IP
            if($success && FLAG_REUSED_IP)
            {
                $transactionArray = flag_reused_ip($transactionArray);
            }

            //Check for re-used Telephone
            if($success && FLAG_REUSED_TEL)
            {
                $transactionArray = flag_reused_telephone($transactionArray);
            }

            //Check for re-used Receiver
            if($success && FLAG_REUSED_RECEIVER)
            {
                $transactionArray = flag_reused_receiver_phone($transactionArray);
                $transactionArray = flag_reused_receiver_phone_used_cards($transactionArray);
            }

            //Check for multi-state purchase by same client in 24 hours
            if($success && FLAG_MULTI_STATE)
            {
                $transactionArray = detect_diff_state($transactionArray);
            }

            //Handle flagged purchase case
            if($success &&
               BLOCK_FLAGGED_PURCHASE &&
               ($transactionArray['num_reused_phone'] > 0 ||
                $transactionArray['num_reused_ip'] > 0 ||
                $transactionArray['num_diff_state'] > 0))
            {
                $transactionArray = block_flagged_purchase($transactionArray);
                $success = !$transactionArray['flagged_phone_blocked'] && !$transactionArray['flagged_ip_blocked'];
            }

            //Verify Client
            if($success)
            {
                //VERIFY USER
                include $_SERVER['DOCUMENT_ROOT']."/src/Security/verifierPage.php";
                $success = $clientVerified;
            }

            //Display Airtime
            if($success)
            {
                //RECORD SALE
                $transactionArray = recordSale($transactionArray);

                if($autoDispatch)
                {
                    //AUTODISPATCH
                    $transactionArray['autoDispatch'] = 1;

                    //insert into outgoing queue for comm. with J2ME process
                    $transactionArray['reason']            = "auto_dispatch";
                    $transactionArray                      = sendToOutQ($transactionArray);
                }
                else
                {
                    //SCRATCH CARD
                    $transactionArray['autoDispatch'] = 0;

                    $transactionArray = useCard($transactionArray);

                    $cardsArePresent = $transactionArray['cardsArePresent'];
                    $cardPIN         = $transactionArray['cardPIN'];

                    if($cardsArePresent=="true")
                    {
                        //Mail the PIN to $clientEmail
                        emailAirtime($transactionArray);

                        //TODO: Print the card details to the screen
                        include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayAccessNo.php";
                    }
                    else
                    {
                        $transactionArray['reason']            = "out_of_stock";
                        $transactionArray                      = sendToOutQ($transactionArray);
                        include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayOutOfStock.php";
                    }
                }

                //Check if client exceeded the daily transaction limit
                $transactionArray = checkUserTransactionLimit($transactionArray);
                $purchase_limit_reached = $transactionArray['limit_reached'];

                //Update Client info and calculate purchase and referral reward points.
                updateClientInfo($countryAbbreviation, $transactionArray, $connection);

                //update receiver information.
                updateReceiverInfo($transactionArray);
                updatePromoReceiverInfo($transactionArray);

                //if purchases exceed configured maximum then set customer status to blocked
                limitUserTransactions($transactionArray);

                //TODO: warn the user that they have reached the daily tx limit
                warnUserOnTransactionLimit($transactionArray, $countryAbbreviation, false);
            }
            else if($transactionArray['transaction_id_exists'])
            {
                $transactionArray['debug_output'].="number of transID rows: $num_rows\n";

                //TODO: its a duplicate transactionID
                include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DuplicateID.php";
            }
            else if($paymentStatus != "Completed")
            {
                handleIncompletePaymentState($countryAbbreviation, $transactionArray, $ipLocationArray, $connection);
            }
            else if ($transactionArray['pay_and_ip_country_match'] == 0 && MATCH_COUNTRY )
            {
                $transactionArray['comments']          = "COUNTRY_MISMATCH";
                handle_country_mismatch($transactionArray);
            }
            else if ($transactionArray['pay_and_ip_state_match'] == 0 && MATCH_STATE )
            {
                $transactionArray['comments']          = "STATE_MISMATCH";
                handle_country_mismatch($transactionArray);
            }
            else if (BLOCK_FLAGGED_PURCHASE && 
                    ($transactionArray['flagged_phone_blocked'] || $transactionArray['flagged_ip_blocked']))
            {
                handle_flagged_purchase($transactionArray);
            }
            else if($transactionArray['client_blacklisted'] == 1 && BLACK_LIST_ACTIVE)
            {
                $input_array = $transactionArray;
                include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayManualProcInfo.php";
            }
            else if($transactionArray['client_new'] == 1 && WHITE_LIST_ACTIVE)
            {
                $input_array = $transactionArray;
                include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayManualProcInfo.php";
            }
            else if($transactionArray['order_exists'] == 0)
            {
                $transactionArray['comments']          = "NO_ORDER_EXISTS";
                include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DuplicateID.php";
            }
            else
            {
                $transactionArray['comments']          = "UNKNOWN";
                handle_return_page_catch_all($transactionArray);
                email_ipn_post($transactionArray);
            }

            // Email Debug
            if(DEBUG_PDT) email_ipn_post($transactionArray);

        }
        else if (strcmp ($lines[0], "FAIL") == 0)
        {
            include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayPPNewGlitch.php";
            echo "<br>".$header."<br>";
            echo "<br>".$req."<br>";
        }
        else
        {
            //its neither a SUCCESS or a FAILURE
            include $_SERVER['DOCUMENT_ROOT']."/src/Displays/PPAuthenticateElse.php";
        }
    }
?>

<?php
    include_once INHERIT_PATH."src/GoogleSales.php";
?>
