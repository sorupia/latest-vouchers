<link rel="stylesheet" href="css/receiver_auto.css" type="text/css">
<div id="body">
    <div class="content">
        <div id="section">
            <h3>Confirm <?=PRODUCT?> receiver details</h3>
            <br/>
            <?php

                if(isset($_POST['send']))
                {
                    //$input_array = $_POST;
                }
                else
                {
                    header("location:receiver_vchr.php?msg=no_selection#show");
                    exit;
                }

            ?>
            <form action="submit_order_to_pp.php" method="post">
                <table width="90%" align="center">
                <tr>
                    <td  height="50">Amount to be received in <?=LOCAL_CURRENCY?></td>
                    <td><?=LOCAL_CURRENCY?> <?=number_format($input_array['receiver_amount_in_local'], LOCAL_DECIMAL_PLACES)?></td>
                </tr>
                <tr>
                    <td  height="50">Amount to be paid in <?=SALE_CURRENCY?></td>
                    <td><?=SALE_CURRENCY." ".number_format($input_array["sale_price_in_foreign"],FOREIGN_DECIMAL_PLACES)?></td>
                </tr>
                <tr>
                    <td>Receiver's Phone</td><td><?=$input_array["receiver_phone"]?></td>
                </tr>
                <tr>
                    <td>Receiver's Name</td><td><?=$input_array["receiver_first_name"]?> <?=$input_array["receiver_last_name"]?></td>
                </tr>
                <tr>
                    <td>Receiver's region</td><td><?=$input_array["receiver_region"]?></td>
                </tr>
                <tr>
                    <td>Pay By</td><td><?=$input_array["payment_option"]?></td>
                </tr>
                <tr>
                    <td height="100"></td>
                    <td colspan="2">
                        <input type="hidden" name="receiver_amount_in_local" value="<?=$input_array['receiver_amount_in_local']?>">
                        <input type="hidden" name="receiver_phone"           value="<?=$input_array['receiver_phone']?>">
                        <input type="hidden" name="phoneConfirm"             value="<?=$input_array['phoneConfirm']?>">
                        <input type="hidden" name="receiver_first_name"      value="<?=$input_array['receiver_first_name']?>">
                        <input type="hidden" name="receiver_last_name"       value="<?=$input_array['receiver_last_name']?>">
                        <input type="hidden" name="receiver_region"          value="<?=$input_array['receiver_region']?>">
                        <input type="hidden" name="payment_option"           value="<?=$input_array['payment_option']?>">
                        <input type="hidden" name="airtime_load_option"      value="<?=$input_array['airtime_load_option']?>">
                        <input type="hidden" name="ip_address"               value="<?=$input_array['ip_address']?>">
                        <input type="hidden" name="ip_country"               value="<?=$input_array['ip_country']?>">
                        <input type="hidden" name="ip_state"                 value="<?=$input_array['ip_state']?>">
                        <input type="hidden" name="ip_city"                  value="<?=$input_array['ip_city']?>">
                        <input type="hidden" name="network_id"               value="<?=$input_array['network_id']?>">
                        <input type="hidden" name="item_id"                  value="<?=$input_array['item_id']?>">
                        <input type="hidden" name="item_name"                value="<?=$input_array['item_name']?>">
                        <input type="hidden" name="hosted_button_id"         value="<?=$input_array['hosted_button_id']?>">
                        <input type="submit" name="send" id="send" value="Send Now" style="width: 100px; background-color: #56919C; color: #ffffff; border-radius: 5px; padding-top: 1px; font-family: roboto; font-size: 16px; letter-spacing: 1px; height: 30px;"/>
                        <input name="cancel" type="button"                   value="Cancel" onclick="history.go(-2);" />
                    </td>
                </tr>
                </table>
            </form>
        </div>
        <div id="sidebar">
            <div class="contact">
                <h3><?=$input_array['network_name']?></h3>
                <img src="images/logos/<?=$input_array['network_id']?>.png" class="receiver_auto_logo">
            </div>
            <div id="voucher_div">
                <div style="width: 100%; height: auto; float: left; margin-left: 20px; margin-top:10px; font-weight: bold; font-size: 16px;">  
                    Recipient will be hand delivered a voucher like this.
                </div>
                <img id="vouchers" src="images/vouchers/<?=$input_array['network_id']?>.png">
            </div>
            <div style="width: 90%; height: auto; font-size: 15px; float: left; margin-left:20px; margin-top:10px; margin-bottom:5px; text-align: center; font-weight: bold;">
                Pay By PayPal (Bank or Credit Card).
            </div>
            <div style="width: 100%; float: left; margin-top: -10px;">
                <img src="images/logos/paypal_big.png" style="width: 95%;">
            </div>
        </div>
    </div>
</div>
