<?
/*******************************************************************************
**  FILE: ipn_processor_code.php
**
**  FUNCTION: N/A
**
**  PURPOSE: Created Return Page Auto Code based on return_page_code.php to process redirects
**  coming in from PayPal PDT invokations.
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 25.Jul.2012
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 10.Mar.2014
**  Verify and log client into system if they have an auto dispatch
**  This was added because the clients that were using auto dispatch were not being
**  automatically logged in like the clients that are using vouchers.
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 05.Mar.2015
**  Added code to support the use of the return page to support auto load transactions
**  PayPal's IPN has proven to be unreliable since the IPN messages are not reaching
**  our server and there is no HTTP response code indicated in the IPN history
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, 1579 Weston Road, Toronto) DATE: 03.Sep.2015
**  Adapted return_page_auto_code for vouchers
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 2015.12.01
**  Fixed bug that distorts debug comments
**
*********************************************************************************/

    include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/constants.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/SecurityFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/ObjectFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/OrderFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/SecurityFunctions.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Messaging/Messaging.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Yo/yo_functions.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Delivery/DeliveryFunctions.php';
   
    $debug = false;
    $countryAbbreviation = COUNTRY_ABBREVIATION;

    //cards.php
    global $networkArray;

    $success = false;
    $auth_token         = PDT_TOKEN;

    //Read the post from PayPal system and add 'cmd'
    $req = 'cmd=_notify-synch';
    $tx_token = $_GET['tx'];    

    $req .= "&tx=$tx_token&at=$auth_token";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, PAYPAL_PAYMENT_URL);
    //curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    //set cacert.pem verisign certificate path in curl using 'CURLOPT_CAINFO' field here,
    //if your server does not bundled with default verisign certificates.
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: ".PAYPAL_HOSTNAME.""));
    //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: ".PAYPAL_HOSTNAME."", "Connection: close"));

    if ( !($res = curl_exec($ch)) )
	{
        $success = false;

        // HTTP ERROR
        $from    = "From: ".SUPPORT_EMAIL;
        $to      = TECH_ADMIN;
        $subject = DOMAIN_NAME." return_page_auto_code";
        $msg     = "SendAT Return Page\n";
        $msg    .= "CURL ERROR";
        $msg    .= "Error : ".curl_error($ch);
        @mail($to,$subject,$msg,$from);
    }
    else
    {
        $success = true;
    }

    curl_close($ch);

    if($success)
    {
        // parse the data
        $lines = explode("\n", $res);
        $keyarray = array();
        if (strcmp ($lines[0], "SUCCESS") == 0) 
        {
            $success = true;

            for ($i=1; $i<count($lines);$i++)
            {
                list($key,$val) = explode("=", $lines[$i]);
                $keyarray[urldecode($key)] = urldecode($val);
            }
        }
        else if (strcmp ($lines[0], "FAIL") == 0)
        {
            $success = false;
            include $_SERVER['DOCUMENT_ROOT']. '/' . ACTIVE_THEME_PATH ."/src/Displays/DisplayPPNewGlitch.php";
        }
        else
        {
            //its neither a SUCCESS or a FAILURE
            $success = false;
            include $_SERVER['DOCUMENT_ROOT']. '/' . ACTIVE_THEME_PATH ."/src/Displays/PPAuthenticateElse.php";
        }
    }

    if($success)
    {
        //Payment info from PayPal
        $paymentStatus   = $keyarray['payment_status'];
        $ourPaypalEmail  = $keyarray['receiver_email'];//our email where the payment goes
        $transID         = $keyarray['txn_id'];
        $paymentAmount   = $keyarray['mc_gross'];//payment_amount does not seem to work
        $paymentCurrency = $keyarray['mc_currency'];
        $paymentFee      = $keyarray['mc_fee'];
        $payment_invoice = $keyarray['invoice'];

        //Client info from PayPal
        $firstname      = $keyarray['first_name'];
        $lastname       = $keyarray['last_name'];
        $itemname       = $keyarray['item_name'];
        $clientEmail    = $keyarray['payer_email'];
        $clientCountry  = $keyarray['residence_country'];
        $clientPhone    = $keyarray['contact_phone'];
        $itemIDNumber   = $keyarray['item_number'];
        $clientID       = $keyarray['payer_id'];
        $addressStreet  = $keyarray['address_street'];
        $addressCity    = $keyarray['address_city'];
        $addressState   = $keyarray['address_state'];
        $addressCountry = $keyarray['address_country'];
        $addressZip     = $keyarray['address_zip'];
        $payer_status    = $keyarray['payer_status'];

        //Remove the country abbreviation if it exists in the invoice
        if(substr($keyarray['invoice'],0,6) == COUNTRY_ABBREVIATION."vchr")
        {
            $keyarray['invoice'] = substr($keyarray['invoice'],6);
        }

        $transactionArray = $keyarray;

        $transactionArray['item_id'] = $transactionArray['item_number'];
        $transactionArray = calculate_sale_price_foreign_auto($transactionArray);

        $networkName      = getNetworkName($itemIDNumber);
        $valueInUGSh      = getValueInLocal($itemIDNumber);
        $ourPriceInUSD    = $transactionArray['sale_price_in_foreign'];
        $cardPIN          = "AUTO_LOAD";
        $loadedBy         = "AUTO_LOAD";
        $loadersIP        = "AUTO_LOAD";
        $serialNumber     = "AUTO_LOAD";
        $sessionUser      = "AUTO_LOAD";
        $dateWePurchased  = date("Y-m-d H:i:s");
        $timeCardSent     = date("Y-m-d H:i:s");

        $networkCode = substr($itemIDNumber,0,5);
        $transactionArray['networkCode'] = $networkCode;


        $loadCode    = $networkArray[$countryAbbreviation][$networkCode]["loadCode"];
        $transactionArray['loadCode'] = $loadCode;

        //switch to be parsed to form to know whether cards were sent
        $cardsArePresent     = "false";

        $transactionArray['networkName']     = $networkName;
        $transactionArray['valueInLocal']    = $valueInUGSh;
        $transactionArray['itemIDNumber']    = $itemIDNumber;
        $transactionArray['cardsArePresent'] = $cardsArePresent;
        $transactionArray['timeCardSent']    = $timeCardSent;
        $transactionArray['ourPriceInUSD']   = $ourPriceInUSD;
        $transactionArray['cardPIN']         = $cardPIN;
        $transactionArray['dateWePurchased'] = $dateWePurchased;
        $transactionArray['loadedBy']        = $loadedBy;
        $transactionArray['loadersIP']       = $loadersIP;
        $transactionArray['serialNumber']    = $serialNumber;
        $transactionArray['sessionUser']     = $sessionUser;

        $pointsSystemEnabled          = true;

        //card or auto transaction
        $autoDispatch = isAutoDispatch($itemIDNumber) ? 1 : 0;
        $clientVerified = 1;
        $success = true;

        $transactionArray['num_reused_phone'] = 0;
        $transactionArray['num_reused_ip'] = 0;

        $transactionArray['client_whitelisted'] = 0;
        $transactionArray['client_new'] = 0;
        $transactionArray['client_blacklisted'] = 0;
        $transactionArray['clientVerified'] = 0;
        $transactionArray['client_already_logged_in'] = 0;
        $transactionArray['client_passed_checks'] = 0;
        $transactionArray['autoDispatch'] = $autoDispatch;
        $transactionArray['transaction_id_exists'] = 0;
        $transactionArray['pay_and_ip_country_match'] = 1;
        $transactionArray['pay_and_ip_state_match'] = 1;
        $transactionArray['flagged_phone_blocked'] = 0;
        $transactionArray['flagged_ip_blocked'] = 0;
        $transactionArray['order_exists'] = 0;
        
        $connection = connect2DB();
        $transactionArray["connection"] = $connection;

		// Extract saved order matching the following: Invoice, ItemID
        if($success)
        {
            $transactionArray = get_saved_order($transactionArray);
            $success = $transactionArray['order_exists'];
            $ipLocationArray = $transactionArray['order_data'];
            $clientIP = $ipLocationArray['ip_address'];
            $transactionArray["clientIP"] = $clientIP;
        }

        //Check the payment amount with Ordered_Cart_Items
        if($success)
        {
            $transactionArray = verify_payment_amount($transactionArray);
            $success = $transactionArray['payment_amount_verified'];
        }

        //Check Payment Status
        if($paymentStatus != "Completed")
        {
            $success = false;
            $transactionArray['comments'].= "paymentStatus != Completed = ".$paymentStatus."\n";
        }

        // Check that receiver_email is your Primary PayPal email
        if($success && $ourPaypalEmail != PAYPAL_EMAIL)
        {
            $success = false;
            $transactionArray['comments'].= "receiver_email != ".PAYPAL_EMAIL." = ".$ourPaypalEmail."\n";
        }

        //AutoDispatch should only display a message of processing
        if(!ALLOW_PDT_AUTO && $transactionArray['autoDispatch'] == 1)
        {
            
            $success = false;

            if($transactionArray['order_exists'] == 0)
            {
                 $transactionArray = retrieve_from_used_cards($transactionArray);
            }
        }

        // Check that txn_id has not been previously processed
        if($success)
        {
            $transactionArray = checkDuplicateTransID($transactionArray);
            $success = !$transactionArray['transaction_id_exists'];
        }

        // Compare PayPal and IP Country
        if($success && MATCH_COUNTRY)
        {
            $success = compare_pay_and_ip_country($transactionArray);
            $transactionArray['pay_and_ip_country_match'] = $success ? 1 : 0;
        }

        // Compare PayPal and IP State
        if($success && MATCH_STATE)
        {
            $success = compare_pay_and_ip_state($transactionArray);
            $transactionArray['pay_and_ip_state_match'] = $success ? 1 : 0;
        }

        //Check for re-used IP
        if($success && FLAG_REUSED_IP)
        {
            $transactionArray = flag_reused_ip($transactionArray);
        }

        //Check for re-used Telephone
        if($success && FLAG_REUSED_TEL)
        {
            $transactionArray = flag_reused_telephone($transactionArray);
        }

        //Check for re-used Receiver
        if($success && FLAG_REUSED_RECEIVER)
        {
            $transactionArray = flag_reused_receiver_phone($transactionArray);
            $transactionArray = flag_reused_receiver_phone_used_cards($transactionArray);

            if(REPORT_REUSED_RECEIVER_PHONE)
            {
                notify_on_reused_receiver_phone($transactionArray);
            }
        }

        //Check for multi-state purchase by same client in 24 hours
        if($success && FLAG_MULTI_STATE)
        {
            $transactionArray = detect_diff_state($transactionArray);
        }

        //Handle flagged purchase case
        if($success &&
           BLOCK_FLAGGED_PURCHASE &&
           ($transactionArray['num_reused_phone'] > 0 ||
            $transactionArray['num_reused_ip'] > 0 ||
            $transactionArray['num_diff_state'] > 0))
        {
            $transactionArray = block_flagged_purchase($transactionArray);
            $success = !$transactionArray['flagged_phone_blocked'] && !$transactionArray['flagged_ip_blocked'];
        }

        //Check Black List
        if($success && 
           BLACK_LIST_ACTIVE &&
           clientOnBlackList($clientEmail,$countryAbbreviation,$connection))
        {
            
			$transactionArray['comments'].= "CLIENT_BLACKLISTED\n";
            handleBlackListUserState($countryAbbreviation,$transactionArray,$ipLocationArray,$connection); 
            $clientVerified = 0;
            $transactionArray['clientVerified'] = $clientVerified;
            $transactionArray['client_blacklisted'] = 1;
            $transactionArray['client_new'] = 0;
            $success = $clientVerified;
        }

        //Detect New Client
        if($success &&
           WHITE_LIST_ACTIVE &&
           !clientOnWhiteList($clientEmail,$countryAbbreviation,$connection))
        {
            
			handleNewClientState($countryAbbreviation,$transactionArray,$ipLocationArray,$connection);
            $clientVerified = 0;
            $transactionArray['clientVerified'] = $clientVerified;
            $transactionArray['client_whitelisted'] = 0;
            $transactionArray['client_new'] = 1;
            $success = $clientVerified;
        }

        //Verify Client
        if($success)
        {
            $clientVerified = 1;
            $transactionArray['clientVerified'] = $clientVerified;
            $transactionArray['client_new'] = 0;
            $success = $clientVerified;

            if($_SESSION['ATSubscriberLoggedIn'] == true)
            {
                $transactionArray['client_already_logged_in'] = 1;
                $transactionArray['client_new'] = 0;
            }
            else
            {
                $transactionArray = logClientIn($transactionArray);

                $transactionArray['client_passed_checks'] = 1;
                $transactionArray['client_whitelisted'] = 1;
            }
        }

		//Display Airtime
        if($success)
        {
            //RECORD SALE
            $transactionArray = recordSale($transactionArray);

            if($autoDispatch)
            {
                //AUTODISPATCH
                $transactionArray['autoDispatch'] = 1;
                $transactionArray = process_cart_auto_item($transactionArray);

                if($transactionArray['invoke_success'])
                {
                    //Display a confirmation message to the buyer
                    include $_SERVER['DOCUMENT_ROOT']. '/' . ACTIVE_THEME_PATH ."/src/Displays/DisplayAutoDispatch.php";
                }
                else
                {
                    //Display a delayed message to the buyer
                    include $_SERVER['DOCUMENT_ROOT']. '/' . ACTIVE_THEME_PATH ."/src/Displays/DisplayOutOfStock.php";
                }
            }
            elseif(substr($transactionArray['item_number'],5,4) == "vchr")
            {
				//RETAIL VOUCHER CARD
                $transactionArray['autoDispatch'] = 0;
                $transactionArray = process_cart_vchr_item($transactionArray);      
                 
				if (DELIVERY_SERVICE_ENABLED) {
					//Send request to delivery system and update RVP db
					$transactionArray = delivery_send_request($transactionArray); 
				}
               
			    $cardsArePresent = $transactionArray['cardsArePresent'];
                $cardPIN         = $transactionArray['cardPIN'];

                if($transactionArray['invoke_success'])
                {
                    //Display a confirmation message to the buyer
                    include $_SERVER['DOCUMENT_ROOT']. '/' . ACTIVE_THEME_PATH ."/src/Displays/DisplayVchrDispatch.php";
                }
                else
                {                    
                    //Display a delayed message to the buyer
                    include $_SERVER['DOCUMENT_ROOT']. '/' . ACTIVE_THEME_PATH ."/src/Displays/DisplayVchrDispatch.php";
                }
            }
            else
            {
                //SCRATCH CARD
                $transactionArray['autoDispatch'] = 0;

                $transactionArray = useCard($transactionArray);

                $cardsArePresent = $transactionArray['cardsArePresent'];
                $cardPIN         = $transactionArray['cardPIN'];

                if($cardsArePresent=="true")
                {
                    //Mail the PIN to $clientEmail
                    emailAirtime($transactionArray);

                    //TODO: Print the card details to the screen
                    include $_SERVER['DOCUMENT_ROOT']. '/' . ACTIVE_THEME_PATH ."/src/Displays/DisplayAccessNo.php";
                }
                else
                {
                    $transactionArray['reason']            = "out_of_stock";
                    $transactionArray                      = sendToOutQ($transactionArray);
                    include $_SERVER['DOCUMENT_ROOT']. '/' . ACTIVE_THEME_PATH ."/src/Displays/DisplayOutOfStock.php";
                }
            }

            delete_saved_order($transactionArray);

            //Check if client exceeded the daily transaction limit
            $transactionArray = checkUserTransactionLimit($transactionArray);
            $purchase_limit_reached = $transactionArray['limit_reached'];

            //Update Client info and calculate purchase and referral reward points.
            updateClientInfo($countryAbbreviation, $transactionArray, $connection);

            //update receiver information.
            updateReceiverInfo($transactionArray);
            updatePromoReceiverInfo($transactionArray);

            //if purchases exceed configured maximum then set customer status to blocked
            limitUserTransactions($transactionArray);

            //TODO: warn the user that they have reached the daily tx limit
            warnUserOnTransactionLimit($transactionArray, $countryAbbreviation, false);

            record_sale_to_google_analytics($transactionArray);

            if(DEBUG_PDT) email_ipn_post($transactionArray);
        }
        else if($paymentStatus != "Completed")
        {
            handleIncompletePaymentState($countryAbbreviation, $transactionArray, $ipLocationArray, $connection);

            if(DEBUG_PDT) email_ipn_post($transactionArray);
        }
        else if($transactionArray['autoDispatch'] == 1)
        {
            $transactionArray['comments'].="This is an auto dispatch\n";

            //TODO: handle auto dispatch
            include $_SERVER['DOCUMENT_ROOT']. '/' . ACTIVE_THEME_PATH ."/src/Displays/DisplayAutoDispatch.php";

            //Verify and log client into system if they have an auto dispatch
            if($_SESSION['ATSubscriberLoggedIn'] == true)
            {
                $transactionArray['client_already_logged_in'] = 1;
                $transactionArray['client_new'] = 0;
            }
            else
            {
                $transactionArray = logClientIn($transactionArray);
            }

            if(DEBUG_PDT) email_ipn_post($transactionArray);
        }
        else if($transactionArray['transaction_id_exists'])
        {
            $transactionArray['comments'].="number of transID rows: $num_rows\n";

            //TODO: its a duplicate transactionID
            include $_SERVER['DOCUMENT_ROOT']. '/' . ACTIVE_THEME_PATH ."/src/Displays/DuplicateID.php";

            if(DEBUG_PDT) email_ipn_post($transactionArray);
        }
        else if($transactionArray['order_exists'] == 0)
        {
            $transactionArray['comments']         .= "NO_ORDER_EXISTS\n";
            include $_SERVER['DOCUMENT_ROOT']. '/' . ACTIVE_THEME_PATH ."/src/Displays/DuplicateID.php";

            if(DEBUG_PDT) email_ipn_post($transactionArray);
        }
        else if ($transactionArray['pay_and_ip_country_match'] == 0 && MATCH_COUNTRY )
        {
            $transactionArray['comments']         .= "COUNTRY_MISMATCH\n";
            handle_country_mismatch($transactionArray);

            if(DEBUG_PDT) email_ipn_post($transactionArray);
        }
        else if ($transactionArray['pay_and_ip_state_match'] == 0 && MATCH_STATE )
        {
            $transactionArray['comments']         .= "STATE_MISMATCH\n";
            handle_country_mismatch($transactionArray);

            if(DEBUG_PDT) email_ipn_post($transactionArray);
        }
        else if (BLOCK_FLAGGED_PURCHASE && 
                ($transactionArray['flagged_phone_blocked'] || $transactionArray['flagged_ip_blocked']))
        {
            handle_flagged_purchase($transactionArray);

            if(DEBUG_PDT) email_ipn_post($transactionArray);
        }
        else if($transactionArray['client_blacklisted'] == 1 && BLACK_LIST_ACTIVE)
        {
            $input_array = $transactionArray;
            include $_SERVER['DOCUMENT_ROOT']. '/' . ACTIVE_THEME_PATH ."/src/Displays/DisplayManualProcInfo.php";

            if(DEBUG_PDT) email_ipn_post($transactionArray);
        }
        else if($transactionArray['client_new'] == 1 && WHITE_LIST_ACTIVE)
        {
            $input_array = $transactionArray;
            include $_SERVER['DOCUMENT_ROOT']. '/' . ACTIVE_THEME_PATH ."/src/Displays/DisplayManualProcNewClient.php";

            if(DEBUG_PDT) email_ipn_post($transactionArray);
        }
        else
        {
             $transactionArray['comments']         .= "UNKNOWN\n";
            handle_return_page_catch_all($transactionArray);
            email_ipn_post($transactionArray);
        }

        // Email Debug
        //if(DEBUG_PDT) email_ipn_post($transactionArray);
    }    

?>

<?
    include_once INHERIT_PATH."src/GoogleSales.php";
    //include_once INHERIT_PATH."src/FacebookMoMoPixel.php";
?>
