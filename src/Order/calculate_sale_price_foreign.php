<?
function calculate_sale_price_foreign_card($input_array)
{
    $cost_price_in_local = substr($input_array['item_number'],9);
    
    $cost_price_in_foreign = $cost_price_in_local / EXCHANGE_RATE;

    $sale_price_in_foreign = ($cost_price_in_foreign + PP_TX_FEE + PRICE_PER_SMS + WEBSHOP_TX_FIXED_FEE_FOREIGN)/
                             (1 - PP_TX_PCT_INT - WEBSHOP_TX_PCT_FEE_FOREIGN);

    $input_array['sale_price_in_foreign'] = round($sale_price_in_foreign,FOREIGN_DECIMAL_PLACES);

    return $input_array;
}
?>