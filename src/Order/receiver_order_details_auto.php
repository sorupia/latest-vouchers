<?
/*******************************************************************************
**   FILE: receiver_order_details_auto.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Handles variables passed onto the receiver_form before displaying form
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 03.Jul.2012
**
**   ADOPTED BY: Arthur Ntozi (3nitylabs, Kampala) DATE 20.May.2015
**   Adapted for UgandaVouchers.com site
**
*********************************************************************************/

    include_once $_SERVER['DOCUMENT_ROOT']."/src/constants.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Constants/auto_load_constants.php";

    $input_array        = $_POST;
    $site_turned_on     = true;
    $country_is_blocked = false;
    $ip_is_blocked      = false;

    if(!array_key_exists('ip_address',$input_array))
    {
        $ipLocationArray           = getIPLocation();
        $input_array['ip_address'] = $ipLocationArray['ip_address'];
        $input_array['ip_country'] = $ipLocationArray['ip_country'];
        $input_array['ip_city']    = $ipLocationArray['ip_city'];
        $input_array['ip_state']   = $ipLocationArray['ip_state'];
    }

    if(!array_key_exists("receiver_phone",$input_array))
    {
        $input_array['receiver_phone'] = COUNTRY_MOBILE_PREFIX;
    }

    if(!array_key_exists("phoneConfirm",$input_array))
    {
        $input_array['phoneConfirm'] = COUNTRY_MOBILE_PREFIX;
    }

    //Determine the load option based on the specification of a passed item_id
    //if(!array_key_exists('auto_load_option', $input_array) && AUTO_LOAD_ENABLED)
    //{
        //do nothing
    //}
    //else

    if(!array_key_exists('item_id', $input_array) && AUTO_LOAD_ENABLED)
    {
        $input_array['airtime_load_option'] = AIRTIME_LOAD_OPTION_AUTO;

        $ipaddress       = $input_array['ip_address'];
        $ip_is_blocked   = IPIsBlocked($ipaddress, $countryAbbreviation);

        //$ip_is_new_clients = isNewClientIP($ipaddress);

        $ipCountry          = $input_array['ip_country'];
        $country_is_blocked = countryIsBlocked($ipCountry,$countryAbbreviation);

        if($ip_is_blocked || $country_is_blocked)
        {
            $site_turned_on = false;
        }
    }
    else if(array_key_exists('item_id', $input_array))
    {
        $input_array['airtime_load_option'] = substr($input_array['item_id'],5,4);

        if(!array_key_exists('receiver_amount_in_local', $input_array))
        {
            $input_array['receiver_amount_in_local'] = substr($input_array['item_id'],9);
        }
    }
    else
    {
        $input_array['airtime_load_option'] = AIRTIME_LOAD_OPTION_AUTO;
    }

    include $_SERVER['DOCUMENT_ROOT']."/src/Order/receiver_form_auto.php";
?>