<?
    function move_cart_item($input_array)
    {
        $receiver_amount_in_local     = $input_array['cart_data']['receiver_amount_in_local'];
        $receiver_phone               = $input_array['cart_data']['receiver_phone'];
        $receiver_first_name          = $input_array['cart_data']['receiver_first_name'];
        $receiver_last_name           = $input_array['cart_data']['receiver_last_name'];
        $receiver_email               = $input_array['cart_data']['receiver_email'];
        $receiver_network_id          = $input_array['cart_data']['receiver_network_id'];
        $receiver_country             = $input_array['cart_data']['receiver_country'];
        $session_id                   = $input_array['cart_data']['session_id'];
        $date_added                   = $input_array['cart_data']['date_added'];
        $receiver_currency            = $input_array['cart_data']['receiver_currency'];
        $api_usage_pct_fee            = $input_array['cart_data']['api_usage_pct_fee'];
        $api_usage_fixed_fee          = $input_array['cart_data']['api_usage_fixed_fee'];
        $our_fixed_fee                = $input_array['cart_data']['our_fixed_fee'];
        $our_pct_fee                  = $input_array['cart_data']['our_pct_fee'];
        $network_sender_fee           = $input_array['cart_data']['network_sender_fee'];
        $network_receiver_fee         = $input_array['cart_data']['network_receiver_fee'];
        $amount_sent_from_api_account = $input_array['cart_data']['amount_sent_from_api_account'];
        $amount_before_pay_gway       = $input_array['cart_data']['amount_before_pay_gway'];
        $note_to_receiver             = $input_array['cart_data']['note_to_receiver'];
        $cart_item_id                 = $input_array['cart_data']['cart_item_id'];
        $date_completed               = $input_array['date_completed'];
        $order_id                     = $input_array['invoice'];
        $api_id                       = $input_array['api_id'];
        $sms_gw_id                    = $input_array['sms_gw_id'];
        $transaction_id               = $input_array['txn_id'];
        $connection                   = $input_array['connection'];
        $process_cart_table           = $input_array['process_cart_table'];
        $to_cart_table                = $input_array['to_cart_table'];

        //Then update with the more recent request
        $query = "INSERT INTO $to_cart_table ";
        $query.= "SET ";
        $query.= "session_id = '".$session_id."', ";
        $query.= "transaction_id = '".$transaction_id."', ";
        $query.= "api_id = '".$api_id."', ";
        $query.= "order_id = ".$order_id.", ";
        $query.= "sms_gw_id = '".$sms_gw_id."', ";
        $query.= "date_added = '".$date_added."', ";
        $query.= "date_completed = '".$date_completed."', ";
        $query.= "receiver_first_name = '".$receiver_first_name."', ";
        $query.= "receiver_last_name = '".$receiver_last_name."', ";
        $query.= "receiver_phone = '".$receiver_phone."', ";
        $query.= "receiver_email = '".$receiver_email."', ";
        $query.= "receiver_network_id = '".$receiver_network_id."', ";
        $query.= "receiver_amount_in_local = ".$receiver_amount_in_local.", ";
        $query.= "receiver_currency = '".$receiver_currency."', ";
        $query.= "receiver_country = '".$receiver_country."', ";
        $query.= "api_usage_pct_fee = ".$api_usage_pct_fee.", ";
        $query.= "api_usage_fixed_fee = ".$api_usage_fixed_fee.", ";
        $query.= "our_fixed_fee = ".$our_fixed_fee.", ";
        $query.= "our_pct_fee = ".$our_pct_fee.", ";
        $query.= "network_sender_fee = ".$network_sender_fee.", ";
        $query.= "network_receiver_fee = ".$network_receiver_fee.", ";
        $query.= "amount_sent_from_api_account = ".$amount_sent_from_api_account.", ";
        $query.= "amount_before_pay_gway = ".$amount_before_pay_gway.", ";
        $query.= "note_to_receiver = '".$note_to_receiver."' ";
        $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
        $input_array['cart_item_id'] = mysql_insert_id($connection);

        if($process_cart_table !="")
        {
            //Since we are still doing single item purchases we should delete other items with this session id
            $query = "DELETE FROM $process_cart_table WHERE cart_item_id = $cart_item_id";
            $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
        }

        return $input_array;
    }
?>