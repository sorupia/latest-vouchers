<?
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/make_order.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/delete_abandoned_orders.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/get_saved_order.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/delete_saved_order.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/calculate_sale_price_local.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/calculate_our_fee_local.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/calculate_yo_fee_local.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/process_cart_auto_item.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/calculate_sale_price_foreign_auto.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/calculate_sale_price_local_auto.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/process_cart_vchr_item.php";
?>