<?

//Process Cart Item & Store Completed Order
function process_cart_auto_item($input_array)
{
    $input_array = yo_at_transfer($input_array);
    $invoke_success = $input_array['invoke_success'];
    $input_array['airtime_gw_id'] = $input_array['api_response']['TransactionReference'];
    $input_array['sms_gw_id'] = "X";

    if($invoke_success)
    {
        $input_array['cardPIN']          = "AUTO_LOAD";
        $input_array['serialNumber']     = "Not Applicable";
        $input_array['loadedBy']         = "Not Applicable";
        $input_array['loadersIP']        = "Not Applicable";
        $input_array['used_cards_table'] = "UsedCards";
        $input_array = sms_receiver_success_transfer($input_array);
        $input_array = save_in_used_cards($input_array);
        email_client_success_transfer($input_array);

        if($input_array['order_data']['receiver_email'] != '')
        {
            email_receiver_success_transfer($input_array);
        }

        notifyOnPurchase($input_array);
    }
    else
    {
        email_support_queued_transfer($input_array);
        $input_array = sendToOutQ($input_array);
    }

    return $input_array;
}

?>