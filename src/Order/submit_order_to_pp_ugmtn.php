<?
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/OrderFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Security/SecurityFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/constants.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Yo/yo_functions.php";

    $input_array = $_POST;

    //Validate receiver form
    $input_array = validate_receiver_form($input_array);
    $success = $input_array['success'];

    if($success)
    {
        $input_array["connection"] = connect2DB();
    }

    //Verify this receiver's status
    if($success && VERIFY_RECEIVER_STATUS)
    {
        $input_array = verify_receiver_status($input_array);
        $success = !$input_array['receiver_status_blocked'];
    }

    //Verify this receiver's limit
    if($success && LIMIT_RECEIVER)
    {
        $input_array = verify_receiver_limit($input_array);
        $success = !$input_array['receiver_limit_exceeded'];
    }

    //Verify this IP limit
    if($success && LIMIT_IP)
    {
        $input_array = verify_ip_limit($input_array);
        $success = !$input_array['ip_limit_exceeded'];
    }

    if(!$success)
    {
        echo'<form action="receiver.php" method="post" name="receiver_form" id="receiver_form">
             <input type="hidden" name="receiver_phone"      value="'.$_POST['receiver_phone'].'" />
             <input type="hidden" name="phoneConfirm"        value="'.$_POST['phoneConfirm'].'" />
             <input type="hidden" name="receiver_first_name" value="'.$_POST['receiver_first_name'].'" />
             <input type="hidden" name="receiver_email"      value="'.$_POST['receiver_email'].'" />
             <input type="hidden" name="ip_address"          value="'.$_POST['ip_address'].'" />
             <input type="hidden" name="ip_country"          value="'.$_POST['ip_country'].'" />
             <input type="hidden" name="ip_state"            value="'.$_POST['ip_state'].'" />
             <input type="hidden" name="ip_city"             value="'.$_POST['ip_city'].'" />
             <input type="hidden" name="network_id"          value="'.$_POST['network_id'].'" />
             <input type="hidden" name="item_id"             value="'.$_POST['item_id'].'" />
             <input type="hidden" name="item_name"           value="'.$_POST['item_name'].'" />
             <input type="hidden" name="hosted_button_id"    value="'.$_POST['button_id'].'">
             <input type="hidden" name="error"               value="'.$input_array['error'].'">
             </form>';

        echo'<script language="JavaScript" type="text/javascript">
                window.onload=function(){ window.document.receiver_form.submit(); }
             </script>';
    }
    else
    {
        echo'<center>
                <p>&nbsp;</p>
                <p><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="333333">
                    Processing Transaction . . . 
                </font>
                </p>
            </center>';

        $input_array['session_id'] = session_id();

        //Delete abandoned orders
        delete_abandoned_orders($input_array);

        //Issue order ID
        $input_array = make_order($input_array);

        $_SESSION['order_id'] = $input_array['order_id'];

        if (!isset($_SESSION['order_id']))
        {
            echo "ERROR: Order ID not assigned<br>";
            exit;
        }

        $input_array['item_number'] = $input_array['item_id'];
        $input_array = calculate_sale_price_local($input_array);

        //If all checks have passed then submit the order to PayPal
        echo'<form action="'.YO_PROC_URL.'" method="post" name="submit_order_to_pp_ugmtn" id="submit_order_to_pp_ugmtn">
                <input type="hidden" name="bid" value="13" />
                <input type="hidden" name="currency" value="'.LOCAL_CURRENCY.'" />
                <input type="hidden" name="amount" value="'.$input_array['sale_price_in_local'].'" />
                <input type="hidden" name="narrative" value="'.$input_array['item_name'].'" />
                <input type="hidden" name="reference" value="'.$input_array['order_id'].'" />
                <input type="hidden" name="account" value="'.YO_USERNAME.'" />
                <input type="hidden" name="return" value="'.YO_RETURN_URL.'?unique_transaction_id=0&transaction_reference=0&item_number='.$input_array['item_id'].'" />
                <input type="hidden" name="prefilled_payer_email_address" value="" />
                <input type="hidden" name="prefilled_payer_mobile_payment_msisdn" value="" />
                <input type="hidden" name="prefilled_payer_names" value="" />
                <input type="hidden" name="abort_payment_url" value="'.INHERIT_SITE.'" />
            </form>';

        echo'<script language="JavaScript" type="text/javascript">
                window.onload=function(){ window.document.submit_order_to_pp_ugmtn.submit(); }
             </script>';
    }
?>