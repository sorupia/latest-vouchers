<?
    $access_level_map['loader']     = 1;
    $access_level_map['support']    = 3;
    $access_level_map['admin']      = 7;
    $access_level_map['superadmin'] = 15;

    define("LOAD_PAGE_MASK",1);
    define("SUPPORT_PAGE_MASK",2);
    define("ADMIN_PAGE_MASK",4);
    define("SUPER_ADMIN_PAGE_MASK",8);

    $pageMaskMap['add_employee.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['awpdc.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['awpdc_blocked.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['awpdc_unblocked.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['clients.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['clients_add_country.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['clients_add_ip.php'] = SUPPORT_PAGE_MASK;	
    $pageMaskMap['clients_blocked_clients.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['clients_blocked_countries.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['clients_blocked_ips.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['clients_delete_client.php'] = SUPPORT_PAGE_MASK;    
    $pageMaskMap['clients_edit_client.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['clients_edit_country.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['clients_edit_ip.php'] = SUPPORT_PAGE_MASK;    		
    $pageMaskMap['clients_email.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['clients_header_nav.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['clients_ip_lookup.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['clients_last_login.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['clients_new_clients.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['clients_paypal_info.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['clients_reg_date.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['clients_search.php'] = SUPPORT_PAGE_MASK;	
    $pageMaskMap['clients_sms_clients.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['complete_used_card.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['controllers.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['confirm_new_at_send_at_auto.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['dashboard.php'] = LOAD_PAGE_MASK;

    $pageMaskMap['delete_card.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['edit_card.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['edit_incomplete_payment.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['edit_employee.php'] = ADMIN_PAGE_MASK;
    $pageMaskMap['employees.php'] = ADMIN_PAGE_MASK;
    $pageMaskMap['employees_login_log.php'] = ADMIN_PAGE_MASK;

    $pageMaskMap['export_delivery_report.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['images.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['index.php'] = LOAD_PAGE_MASK;
    $pageMaskMap['inventory.php'] = LOAD_PAGE_MASK;
    $pageMaskMap['itemIDDropDown.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['new_at.php'] = LOAD_PAGE_MASK;
    $pageMaskMap['new_at_file.php'] = LOAD_PAGE_MASK;
    $pageMaskMap['new_at_file_action.php'] = LOAD_PAGE_MASK;
    $pageMaskMap['new_at_header_nav.php'] = LOAD_PAGE_MASK;
    $pageMaskMap['new_at_purchase_date.php'] = LOAD_PAGE_MASK;
    $pageMaskMap['new_at_query_by_type.php'] = LOAD_PAGE_MASK;
    $pageMaskMap['new_at_query_pin.php'] = LOAD_PAGE_MASK;
    $pageMaskMap['new_at_send_at.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['new_at_send_at_auto.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['out_queue.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['out_queue_awpdc.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['out_queue_incomplete.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['out_queue_nav.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['out_queue_new.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['out_queue_blocked.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['order_queue.php']  = SUPPORT_PAGE_MASK;

    $pageMaskMap['process_awpdc_client.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['reports.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['reports_comp_referrals.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['reports_header_nav.php'] = SUPPORT_PAGE_MASK;
	$pageMaskMap['reports_load_report.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['reports_reward_points.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['reports_all_pins.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['reports_reversal_effect.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['restock_used_card.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['search_awpdc_clients.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['search_clients.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['sendAT.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['send_incomplete_payment.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['send_blocked_purchase.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['settings.php'] = ADMIN_PAGE_MASK;

    $pageMaskMap['used_at.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['used_at_deleted.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['used_at_manual_submitted.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['used_at_dest_number.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['used_at_full.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['used_at_search.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['verify_new_client_transaction.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['verify_old_client_transaction.php'] = SUPPORT_PAGE_MASK;
    $pageMaskMap['block_new_client_transaction.php']  = SUPPORT_PAGE_MASK;

    $pageMaskMap['verify_order.php'] = SUPPORT_PAGE_MASK;

    $pageMaskMap['receivers.php']  = SUPPORT_PAGE_MASK;
    $pageMaskMap['receiver-auto.php']  = SUPPORT_PAGE_MASK;
    $pageMaskMap['receivers_edit_receiver.php']  = SUPPORT_PAGE_MASK;
    $pageMaskMap['receivers_search.php']  = SUPPORT_PAGE_MASK;
    $pageMaskMap['receivers_new_receivers.php']  = SUPPORT_PAGE_MASK;
    $pageMaskMap['receivers_all_receivers.php']  = SUPPORT_PAGE_MASK;
    $pageMaskMap['receivers_original_receivers.php']  = SUPPORT_PAGE_MASK;
    $pageMaskMap['receivers_called_receivers.php']  = SUPPORT_PAGE_MASK;
    $pageMaskMap['receivers_edit_new_receivers.php']  = SUPPORT_PAGE_MASK;

    $pageMaskMap['resend_used_card.php']  = SUPPORT_PAGE_MASK;

    $pageMaskMap['yo.php']  = SUPPORT_PAGE_MASK;
?>
