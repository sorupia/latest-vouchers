<?
    include_once "../src/GeneralFunctions.php";
    require_once$_SERVER['DOCUMENT_ROOT']."/src/Reports/PreciseRevenueReport.php";
    require_once$_SERVER['DOCUMENT_ROOT']."/src/Reports/sendat_local_revenue_report.php";
    require_once$_SERVER['DOCUMENT_ROOT']."/src/Reports/sendat_foreign_revenue_report_auto.php";
    
    function revenueReport($start_date, 
                           $end_date, 
                           $exchange_rate, 
                           $cost_per_sms, 
                           $countryAbbreviation,
                           $debug,
                           $return_type)
    {
        global $networkMap;
        global $networkArray;
        global $cardMap;//help determine what index value is for a particular card
        
        $revenue_array = array();
        $index = 0;

        $numberOfNetworks=count($networkMap[$countryAbbreviation]); 
        $pp_tx_fee_2 = 0.30;        
        //connect to DB
        $connection=connect2DB($countryAbbreviation);
                
                $totalNumber = 0;
                $totalSold = 0;
                $totalProfitInt = 0;
                $totalProfitUS = 0; 
                
    
                
                $textmsg = "";
                $row_msg = "";
                
                for($networkIndex=0;$networkIndex<$numberOfNetworks;$networkIndex++){
                    
                    $thisNetwork=$networkMap[$countryAbbreviation][$networkIndex];
                    $numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);
                    $text_msg.= $thisNetwork;
                    
                    for($j=0;$j<$numberOfDenominations;$j++){
                        $thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
                        
                        $soldQuery = "SELECT itemID FROM UsedCards WHERE ";
                        $soldQuery.= "UsedCards.itemID = '$thisCard' AND dateWeSoldIt > '$start_date' AND dateWeSoldIt < '$end_date' ";
                        $soldQuery.= "AND saleType = 'PAYPAL' ";
                        if($debug) echo "soldQuery: ".$soldQuery."<br>";

                        $soldResult = mysql_query($soldQuery) or handleDatabaseError('' . mysql_error(),$soldQuery);


                        $numberSold = mysql_num_rows($soldResult);
                        $totalSold+= $numberSold;
                        
                        $priceInUSD = getPriceInUSD($thisCard);
                        $priceInLocal = getValueInLocal($thisCard);
                        $actual_price = $priceInLocal / $exchange_rate;
                        
                        $pp_tx_fee_int_1 = 0.039 * $priceInUSD;
                        $pp_tx_fee_us_1 = 0.029 * $priceInUSD;
                        
                        $cost_b4_markup_int = $actual_price + $pp_tx_fee_int_1 + $pp_tx_fee_2 + $cost_per_sms;
                        $cost_b4_markup_us = $actual_price + $pp_tx_fee_us_1 + $pp_tx_fee_2 + $cost_per_sms;
                        
                        $gross_per_card_int = $priceInUSD - $cost_b4_markup_int;
                        $gross_per_card_us = $priceInUSD - $cost_b4_markup_us;      
                        
                        if($debug) echo "priceInUSD: ".$priceInUSD."<br>";
                        if($debug) echo "actual price: ".$actual_price."<br>";
                        if($debug) echo "pp_tx_fee_int_1: ".$pp_tx_fee_int_1."<br>";
                        if($debug) echo "pp_tx_fee_int_2: ".$pp_tx_fee_2."<br>";
                        if($debug) echo "cost_per_sms: ".$cost_per_sms."<br>";
                        if($debug) echo "gross_per_card_int: ".$gross_per_card_int."<br><br>";
                                            
                                    
                        $profit_per_item_int = $gross_per_card_int * $numberSold;
                        $profit_per_item_us = $gross_per_card_us * $numberSold;
                        
                        $totalProfitInt+= $profit_per_item_int;
                        $totalProfitUS+= $profit_per_item_us;
                        $totalRevenue+= ($priceInUSD * $numberSold);
                        
                        if($return_type == "return_msg")
                        {
                            $row_msg.= "<tr>";
                            $row_msg.= "<td>$thisCard</td> ";
                            $row_msg.= "<td>$numberSold</td> ";
                            $row_msg.= "<td>$priceInUSD</td> ";
                            $row_msg.= "<td>".round($gross_per_card_int,2)."</td> ";
                            $row_msg.= "<td>".round($profit_per_item_int,2)."</td> ";
                            $row_msg.= "<td>".round($profit_per_item_us,2)."</td> ";
                            $row_msg.= "</tr>"; 
                        }
                        else
                        {
                            //ItemID,Quantity,Price,Card_Profit,Profit_Int,Profit_US
                            $revenue_array[$index]["item_id"]         = $thisCard;
                            $revenue_array[$index]["quantity"]        = $numberSold;
                            $revenue_array[$index]["price_in_usd"]    = $priceInUSD;
                            $revenue_array[$index]["card_profit_int"] = round($gross_per_card_int,2);
                            $revenue_array[$index]["profit_int"]      = round($profit_per_item_int,2);
                            $revenue_array[$index]["profit_us"]       = round($profit_per_item_us,2);
                            $index++;
                        }
                        mysql_free_result($soldResult);
                    }
    
                }
                            
        disconnectDB($connection);
        
        //compile email and send it to stakeholders
        if($return_type == "return_msg")
        {
            $txt_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."\n";
            $txt_msg.=  substr(DOMAIN_PREFIX,0,-1)." \n";
            $txt_msg.= "Revenue: ".SALE_CURRENCY."".$totalRevenue."\n";
            $txt_msg.= "Gross Profit: ".SALE_CURRENCY."".round($totalProfitInt,2)."\n";
            $txt_msg.= "Number of Transactions: ".$totalSold."\n";
            $txt_msg.= "Ex. Rate: ".$exchange_rate."\n";

            if($debug) echo "txt_msg: ".$txt_msg." <br>";
            if($debug) echo "txt_msg.len: ".strlen($txt_msg)."<br>";

            $email_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."<br>";
            $email_msg.=  substr(DOMAIN_PREFIX,0,-1)." <br>";
            $email_msg.= "Revenue: ".SALE_CURRENCY."".$totalRevenue."<br>";
            $email_msg.= "Gross Profit: ".SALE_CURRENCY."".round($totalProfitInt,2)."<br>";
            $email_msg.= "Number of Transactions: ".$totalSold."<br>";
            $email_msg.= "Ex. Rate: ".$exchange_rate."<br><br>";            
            $email_msg.= "\n";

            $email_msg.= "<table width=\"65%\"  border=\"1\" >";
            
            $email_msg.= "<tr>";
            $email_msg.= "<td>Item ID</td><td>Quantity</td><td>Price in USD</td> ";
            $email_msg.= "<td>Card Profit</td><td>Profit(Int)</td><td>Profit(USD)</td> ";       
            $email_msg.= "</tr>";

            $email_msg.= $row_msg;

            $email_msg.= "<tr>";
            $email_msg.= "<td>TOTALS</td>";
            $email_msg.= "<td>$totalSold</td>";
            $email_msg.= "<td></td>";
            $email_msg.= "<td></td>";
            $email_msg.= "<td>".round($totalProfitInt,2)."</td>";
            $email_msg.= "<td>".round($totalProfitUS,2)."</td>";
            $email_msg.= "</tr>";
            
            $email_msg.= "</table>";
            
            $email_msg.= "\n---Copyright 3nitylabs Software Inc";
            
            return array("email"=>$email_msg,"txt"=>$txt_msg);
        }
        else
        {
            //return array containing report elements
            //ItemID,Quantity,Price,Card_Profit,Profit_Int,Profit_US
            $revenue_array[$index]["item_id"]         = "TOTAL";
            $revenue_array[$index]["quantity"]        = $totalSold;
            $revenue_array[$index]["price_in_usd"]    = $totalRevenue;
            $revenue_array[$index]["card_profit_int"] = "N/A";
            $revenue_array[$index]["profit_int"]      = round($totalProfitInt,2);
            $revenue_array[$index]["profit_us"]       = round($totalProfitUS,2);


            if($debug) echo "Revenue Array returned <br>";
            return $revenue_array;
            
        }       
    }
    
    /* Performs the cronjob for revenue reports*/
    function revenue_cron($countryAbbreviation, $exchange_rate, $revenue_emails, $revenue_phones, $debug)
    {
        $utc_start_date = mktime(0, 0, 0, date("m")  , date("d")-7, date("Y"));
        $start_date     = date("Y-m-d",$utc_start_date)." 00:00:01";//Y-m-d H:i:s
        $utc_end_date   = mktime(0, 0, 0, date("m")  , date("d")-1, date("Y"));
        $end_date       = date("Y-m-d",$utc_end_date)." 23:59:59";
    

        $cost_per_sms = PRICE_PER_SMS;

        
        $result_array = preciseRevenueReport($start_date, 
                                      $end_date, 
                                      $exchange_rate, 
                                      $cost_per_sms, 
                                      $countryAbbreviation,
                                      $debug,
                                      "return_msg");
    
        $email_msg = $result_array["email"];
        $txt_msg = $result_array["txt"];
        
        if($debug) echo $email_msg;
        
        $subject = "Revenue: ".substr($start_date,0,10)." to ".substr($end_date,0,10);
        
        //sendEmail(FROM_NAME,FROM_EMAIL,TECH_ADMIN,$bcc,$subject,$email_msg);
        
        for( $count=0 ; ($count < count($revenue_emails)) && !$debug ; $count++)
        {
            $dest_email = $revenue_emails[$count];
            echo "Revenue Report Sent to: ".$dest_email."\n";
            $isHTML = true;
            sendEmail3(FROM_NAME,FROM_EMAIL,$dest_email,$bcc,$subject,$email_msg,$isHTML);
        }
    
        //compile txt message and send to stakeholders
        for( $count=0 ; ($count < count($revenue_phones)) && !$debug ; $count++)
        {
            $dest_phone = $revenue_phones[$count];
            echo "Revenue Report Sent to: ".$dest_phone."\n";
            sendTextUsingClick2($dest_phone,$txt_msg);
        }
    }

    /* Performs the cronjob for local revenue reports*/
    function local_revenue_cron($countryAbbreviation, $exchange_rate, $revenue_emails, $revenue_phones, $debug)
    {
        $utc_start_date = mktime(0, 0, 0, date("m")  , date("d")-7, date("Y"));
        $start_date     = date("Y-m-d",$utc_start_date)." 00:00:01";//Y-m-d H:i:s
        $utc_end_date   = mktime(0, 0, 0, date("m")  , date("d")-1, date("Y"));
        $end_date       = date("Y-m-d",$utc_end_date)." 23:59:59";

        $cost_per_sms = PRICE_PER_SMS;

        $input_array['start_date']          = $start_date;
        $input_array['end_date']            = $end_date;
        $input_array['exchange_rate']       = $exchange_rate;
        $input_array['cost_per_sms']        = $cost_per_sms;
        $input_array['countryAbbreviation'] = $countryAbbreviation;
        $input_array['debug']               = $debug;
        $input_array['return_type']         = "return_msg";

        $result_array = sendat_local_revenue_report($input_array);

        $email_msg = $result_array["email"];
        $txt_msg = $result_array["txt"];

        if($debug) echo $email_msg;

        $subject = "Local Revenue: ".substr($start_date,0,10)." to ".substr($end_date,0,10);


        //sendEmail(FROM_NAME,FROM_EMAIL,TECH_ADMIN,$bcc,$subject,$email_msg);

        for( $count=0 ; ($count < count($revenue_emails)) && !$debug ; $count++)
        {
            $dest_email = $revenue_emails[$count];
            echo "Revenue Report Sent to: ".$dest_email."\n";
            $isHTML = true;
            sendEmail3(FROM_NAME,FROM_EMAIL,$dest_email,$bcc,$subject,$email_msg,$isHTML);
        }

        //compile txt message and send to stakeholders
        for( $count=0 ; ($count < count($revenue_phones)) && !$debug ; $count++)
        {
            $dest_phone = $revenue_phones[$count];
            echo "Revenue Report Sent to: ".$dest_phone."\n";
            sendTextUsingClick2($dest_phone,$txt_msg);
        }
    }

    /* Performs the cronjob for local revenue reports*/
    function foreign_revenue_auto_cron($countryAbbreviation, $exchange_rate, $revenue_emails, $revenue_phones, $debug)
    {
        $utc_start_date = mktime(0, 0, 0, date("m")  , date("d")-7, date("Y"));
        $start_date     = date("Y-m-d",$utc_start_date)." 00:00:01";//Y-m-d H:i:s
        $utc_end_date   = mktime(0, 0, 0, date("m")  , date("d")-1, date("Y"));
        $end_date       = date("Y-m-d",$utc_end_date)." 23:59:59";

        $cost_per_sms = PRICE_PER_SMS;

        $input_array['start_date']          = $start_date;
        $input_array['end_date']            = $end_date;
        $input_array['exchange_rate']       = $exchange_rate;
        $input_array['cost_per_sms']        = $cost_per_sms;
        $input_array['countryAbbreviation'] = $countryAbbreviation;
        $input_array['debug']               = $debug;
        $input_array['return_type']         = "return_msg";

        $result_array = sendat_foreign_revenue_report_auto($input_array);

        $email_msg = $result_array["email"];
        $txt_msg = $result_array["txt"];

        if($debug) echo $email_msg;

        $subject = "Auto Load Foreign Revenue: ".substr($start_date,0,10)." to ".substr($end_date,0,10);

        //sendEmail(FROM_NAME,FROM_EMAIL,TECH_ADMIN,$bcc,$subject,$email_msg);

        for( $count=0 ; ($count < count($revenue_emails)) && !$debug ; $count++)
        {
            $dest_email = $revenue_emails[$count];
            echo "Revenue Report Sent to: ".$dest_email."\n";
            $isHTML = true;
            sendEmail3(FROM_NAME,FROM_EMAIL,$dest_email,$bcc,$subject,$email_msg,$isHTML);
        }

        //compile txt message and send to stakeholders
        for( $count=0 ; ($count < count($revenue_phones)) && !$debug ; $count++)
        {
            $dest_phone = $revenue_phones[$count];
            echo "Revenue Report Sent to: ".$dest_phone."\n";
            sendTextUsingClick2($dest_phone,$txt_msg);
        }
    }

    function stock_report($countryAbbreviation)
    {
        global $networkMap;
        global $networkArray;
        global $cardMap;//help determine what index value is for a particular card
        $quantityOfCardsArray=array();//stores the quantities for each denomination available
        $numberOfNetworks=count($networkMap[$countryAbbreviation]);
    
        
        //connect to DB
        $connection=connect2DB($countryAbbreviation);
                
                $totalNumber = 0;
                $totalSold = 0;
                $text_msg = "Stock\n";
                for($networkIndex=0;$networkIndex<$numberOfNetworks;$networkIndex++){
                    
                    $thisNetwork=$networkMap[$countryAbbreviation][$networkIndex];
                    $numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);
                    $text_msg.= $thisNetwork;
                    
                    for($j=0;$j<$numberOfDenominations;$j++){
                        $thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
                        $oneQuery = sprintf ("SELECT itemID FROM NewCards WHERE NewCards.itemID = '$thisCard' AND NewCards.cardStatus != 'USED'");
                        $oneResult = mysql_query($oneQuery) or handleDatabaseError('' . mysql_error(),$oneQuery);
                        $quantityOfCardsArray["$thisCard"] = mysql_num_rows($oneResult);
                        $totalNumber+=$quantityOfCardsArray["$thisCard"];
                        $text_msg.= substr($thisCard,9)."=".$quantityOfCardsArray["$thisCard"].",";
                        mysql_free_result($oneResult);
                    }
                    $text_msg.= "\n";
                }
                echo $text_msg."<br>";
                echo "number of characters in text: ".strlen($text_msg);
                            
        disconnectDB($connection);
        sendTextUsingClick2(DEV_CELL,$text_msg);
        sendTextUsingClick2(LOADER_CELL,$text_msg);
        sendTextUsingClick2(LOADER_CELL_2,$text_msg);
        sendTextUsingClick2(SUPPORT_CELL,$text_msg);
    }       
    
    function purchase_report($start_date, 
                             $end_date, 
                             $exchange_rate,  
                             $countryAbbreviation,
                             $debug,
                             $return_type)
    {
        global $networkMap;
        global $networkArray;
        global $cardMap;//help determine what index value is for a particular card
        $purchase_array = array();//stores the quantities for each denomination available
        $numberOfNetworks = count($networkMap[$countryAbbreviation]);
        $index = 0;
        $totalPurchases = 0;
        $totalPurchaseQuantity = 0;
        $totalPurchaseCost = 0; 
        $totalPurchaseUSD = 0;
        $totalValueUSD = 0;
    
        
        //connect to DB
        $connection=connect2DB($countryAbbreviation);
                
                $totalNumber = 0;
                $totalSold = 0;
                $text_msg = "Inventory Report\n";
                for($networkIndex=0;$networkIndex<$numberOfNetworks;$networkIndex++){
                    
                    $thisNetwork=$networkMap[$countryAbbreviation][$networkIndex];
                    $numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);
                    
                    for($j=0;$j<$numberOfDenominations;$j++){
                        $thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];

                        $purchaseQuery = "SELECT itemID FROM NewCards WHERE ";
                        $purchaseQuery.= "NewCards.itemID = '$thisCard' AND dateWePurchased > '$start_date' AND dateWePurchased < '$end_date'";
                        $purchaseResult = mysql_query($purchaseQuery) or handleDatabaseError('' . mysql_error(),$purchaseQuery);
                        $usedPurchases = mysql_num_rows($purchaseResult);

                        $purchaseQuery = "SELECT itemID FROM UsedCards WHERE ";
                        $purchaseQuery.= "UsedCards.itemID = '$thisCard' AND dateWePurchased > '$start_date' AND dateWePurchased < '$end_date'";
                        $purchaseResult = mysql_query($purchaseQuery) or handleDatabaseError('' . mysql_error(),$purchaseQuery);
                        $newPurchases = mysql_num_rows($purchaseResult);
                        
                        $numberPurchased = $usedPurchases + $newPurchases;
                        $priceInLocal = getValueInLocal($thisCard);
                        
                        $cardTotal = $priceInLocal*$numberPurchased;
                        $totalUSD = $cardTotal/$exchange_rate;
                        
                        $priceInUSD = getPriceInUSD($thisCard);
                        $valueUSD = $priceInUSD*$numberPurchased;

                        $totalPurchaseQuantity+= $numberPurchased;
                        $totalPurchaseCost+= $cardTotal;
                        $totalPurchaseUSD+= $totalUSD;
                        $totalValueUSD+= $valueUSD;
                        

                        
                        if($return_type == "return_msg")
                        {
                            //$row_msg.= "$thisCard\t\t$numberPurchased\t\t$cardTotal\t\t\t\t$totalUSD\t\t$valueUSD\n";
                            
                            $row_msg.= "<tr>";
                            $row_msg.= "<td>$thisCard</td> ";
                            $row_msg.= "<td>$numberPurchased</td> ";
                            $row_msg.= "<td>$cardTotal</td> ";
                            $row_msg.= "<td>$totalUSD</td> ";
                            $row_msg.= "<td>$valueUSD</td> ";
                            $row_msg.= "</tr>"; 
                        }
                        else
                        {
                            //ItemID,Quantity,Card_Total
                            $purchase_array[$index]["item_id"]      = $thisCard;
                            $purchase_array[$index]["quantity"]     = $numberPurchased;
                            $purchase_array[$index]["card_total"]   = $cardTotal;
                            $purchase_array[$index]["usd_total"]    = round($totalUSD,2);
                            $purchase_array[$index]["value_usd"]    = $valueUSD;
                            $index++;
                        }
                        
                        mysql_free_result($purchaseResult);
                    }
                }
                
        
        disconnectDB($connection);

        //compile email and send it to stakeholders
        if($return_type == "return_msg")
        {
            $txt_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."\n";
            $txt_msg.=  substr(DOMAIN_PREFIX,0,-1)." \n";
            $txt_msg.= "Restock in Local: ".$totalPurchaseCost."\n";
            $totalInUSD = $totalPurchaseCost/$exchange_rate;
            $txt_msg.= "Restock: ".SALE_CURRENCY."".round($totalInUSD,2)."\n";
            $txt_msg.= "Number of Cards: ".$totalPurchaseQuantity."\n";
            $txt_msg.= "Ex. Rate: ".$exchange_rate."\n";
        
            $email_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."<br>";
            $email_msg.=  substr(DOMAIN_PREFIX,0,-1)." <br>";
            $email_msg.= "Restock in Local: ".$totalPurchaseCost."<br>";
            $totalInUSD = $totalPurchaseCost/$exchange_rate;
            $email_msg.= "Restock: ".SALE_CURRENCY."".round($totalInUSD,2)."<br>";
            $email_msg.= "Number of Cards: ".$totalPurchaseQuantity."<br>";
            $email_msg.= "Ex. Rate: ".$exchange_rate."<br><br>";
            
            if($debug) echo "txt_msg: ".$txt_msg."<br>";
            if($debug) echo "txt_msg.len: ".strlen($txt_msg)."<br>";
            
            $email_msg.= "<table width=\"65%\"  border=\"1\" >";
            
            $email_msg.= "<tr>";
            $email_msg.= "<td>Item ID</td> ";
            $email_msg.= "<td>Quantity</td> ";
            $email_msg.= "<td>Price in Local</td> ";
            $email_msg.= "<td>Price in USD</td> ";
            $email_msg.= "<td>Value</td> ";     
            $email_msg.= "</tr>";

            $email_msg.= $row_msg;

            $email_msg.= "<tr>";
            $email_msg.= "<td>TOTALS</td> ";
            $email_msg.= "<td>$totalPurchaseQuantity</td> ";
            $email_msg.= "<td>$totalPurchaseCost</td> ";
            $email_msg.= "<td>$totalPurchaseUSD</td> ";
            $email_msg.= "<td>$totalValueUSD</td> ";
            $email_msg.= "</tr>";
            
            $email_msg.= "</table>";
            
            $email_msg.= "<br> ---Copyright 3nitylabs Software Inc";
            
            return array("email"=>$email_msg,"txt"=>$txt_msg);
        }
        else
        {
            //return array containing report elements
            //ItemID,Quantity,Price,Card_Profit,Profit_Int,Profit_US
            
            //ItemID,Quantity,Card_Total
            $purchase_array[$index]["item_id"]      = "TOTAL";
            $purchase_array[$index]["quantity"]     = $totalPurchaseQuantity;
            $purchase_array[$index]["card_total"]   = $totalPurchaseCost;
            $purchase_array[$index]["usd_total"]    = round($totalPurchaseUSD,2);           
            $purchase_array[$index]["value_usd"]    = $totalValueUSD;
            
            if($debug) echo "Purchase Array returned <br>";
            return $purchase_array;
        }   

    }       

    /* Performs the cronjob for purchase reports*/
    function purchase_cron($countryAbbreviation, $exchange_rate, $revenue_emails, $revenue_phones, $debug)
    {
        //obtain the past weeks purchase report
        $utc_start_date = mktime(0, 0, 0, date("m")  , date("d")-7, date("Y"));
        $start_date     = date("Y-m-d",$utc_start_date)." 00:00:01";//Y-m-d H:i:s
        $utc_end_date   = mktime(0, 0, 0, date("m")  , date("d")-1, date("Y"));
        $end_date       = date("Y-m-d",$utc_end_date)." 23:59:59";
    
        
        
        $result_array = purchase_report($start_date, 
                                        $end_date, 
                                        $exchange_rate, 
                                        $countryAbbreviation,
                                        $debug,
                                        "return_msg");
    
        $email_msg = $result_array["email"];
        $txt_msg = $result_array["txt"];
        
        if($debug) echo $email_msg;
        
        $subject = "Stock Input: ".substr($start_date,0,10)." to ".substr($end_date,0,10);
        
        //sendEmail(FROM_NAME,FROM_EMAIL,TECH_ADMIN,$bcc,$subject,$email_msg);
        
        for( $count=0 ; ($count < count($revenue_emails)) && !$debug ; $count++)
        {
            $dest_email = $revenue_emails[$count];
            echo "Stock Report Sent to: ".$dest_email."\n";
            $isHTML = true;
            sendEmail3(FROM_NAME,FROM_EMAIL,$dest_email,$bcc,$subject,$email_msg,$isHTML);
        }
    
        //compile txt message and send to stakeholders
        for( $count=0 ; ($count < count($revenue_phones)) && !$debug ; $count++)
        {
            $dest_phone = $revenue_phones[$count];
            echo "Stock Report Sent to: ".$dest_phone."\n";
            sendTextUsingClick2($dest_phone,$txt_msg);
        }
    }
    
    function displayStockReportResults($purchase_array)
    {
        $numOfColumns=count($purchase_array[0]);
        $numOfRows=count($purchase_array);
        
        // Print the Client data in a HTML table 
        echo "<table class=\"large-tbl\" width=\"90%\"  border=\"0\" >";
        echo"<tr>";
            echo"<td>item_id</td>";
            echo"<td>quantity</td>";
            echo"<td>card_total</td>";
            echo"<td>usd_total</td>";
            echo"<td>value_usd</td>";
        echo"</tr>";
        
        $j=0;       
        for($index =0 ; $index<$numOfRows ; $index++)
        {
            $thisCard = $purchase_array[$index]["item_id"];
            $numberPurchased = $purchase_array[$index]["quantity"];
            $cardTotal = $purchase_array[$index]["card_total"];
            $totalUSD = $purchase_array[$index]["usd_total"];           
            $valueUSD = $purchase_array[$index]["value_usd"];
            
            $j++;
            if($j%2){
                print "<tr>";
            }
            else{
                print "<tr bgcolor=\"#F0F0F0\">";
            }
            
            print "<td>$thisCard</td>";
            print "<td>$numberPurchased</td>";
            print "<td>$cardTotal</td>";
            print "<td>$totalUSD</td>";
            print "<td>$valueUSD</td>";
            
            print "</tr>";
        }

        print "</table>";       
    }
    function displayStockReportNew($purchase_array)
    {
        $numOfColumns=count($purchase_array[0]);
        $numOfRows=count($purchase_array);
        
        // Print the Client data in a HTML table 
        echo"<thead> ";
        echo"<tr> ";
            echo"<th>item_id</th> ";
            echo"<th>quantity</th> ";
            echo"<th>card_total</th> ";
            echo"<th>usd_total</th> ";
            echo"<th>value_usd</th> ";
        echo"</tr>";
        echo"</thead> ";
        
        for($index =0 ; $index<$numOfRows ; $index++)
        {
            $thisCard = $purchase_array[$index]["item_id"];
            $numberPurchased = $purchase_array[$index]["quantity"];
            $cardTotal = $purchase_array[$index]["card_total"];
            $totalUSD = $purchase_array[$index]["usd_total"];           
            $valueUSD = $purchase_array[$index]["value_usd"];
            
            echo "<tbody><tr> ";
            
            echo "<td>$thisCard</td> ";
            echo "<td>$numberPurchased</td> ";
            echo "<td>$cardTotal</td> ";
            echo "<td>$totalUSD</td> ";
            echo "<td>$valueUSD</td> ";
            
            echo "</tr></tbody> ";
        }
    
    }
    
    function displayRevenueReportNew($revenue_array)
    {
        $numOfColumns=count($revenue_array[0]);
        $numOfRows=count($revenue_array);

        $column_keys_array = array_keys($revenue_array[0]);

        // Print the Client data in a HTML table 
        echo "<thead>";
        echo"<tr>";

        for($i = 0; $i < $numOfColumns; $i++)
        {
            echo"<th>".$column_keys_array[$i]."</th> ";
        }

        echo"</tr>";
        echo"</thead>";

        for($index =0 ; $index<$numOfRows ; $index++)
        {
            echo"<tbody><tr> ";

            for($i = 0; $i < $numOfColumns; $i++)
            {
                $this_key = $column_keys_array[$i];
                $this_value = $revenue_array[$index]["$this_key"];
                print "<td>$this_value</td> ";
            }

            print "</tr></tbody> ";
        }
    }

    function displayRevenueReportResults($purchase_array)
    {
        $numOfColumns=count($purchase_array[0]);
        $numOfRows=count($purchase_array);
        
        // Print the Client data in a HTML table 
        echo "<table class=\"large-tbl\" width=\"90%\"  border=\"0\" >";
        echo"<tr>";
            echo"<td>item_id</td>";
            echo"<td>quantity</td>";
            echo"<td>price_in_usd</td>";
            echo"<td>card_profit_int</td>";
            echo"<td>profit_int</td>";
            echo"<td>profit_us</td>";           
        echo"</tr>";
        
        $j=0;       
        for($index =0 ; $index<$numOfRows ; $index++)
        {
            $thisCard = $purchase_array[$index]["item_id"];
            $numberPurchased = $purchase_array[$index]["quantity"];
            $priceInUSD = $purchase_array[$index]["price_in_usd"];
            $cardProfitInt = $purchase_array[$index]["card_profit_int"];            
            $profitInt = $purchase_array[$index]["profit_int"];
            $profitUSD = $purchase_array[$index]["profit_us"];
            
            $j++;
            if($j%2){
                print "<tr>";
            }
            else{
                print "<tr bgcolor=\"#F0F0F0\">";
            }
            
            print "<td>$thisCard</td>";
            print "<td>$numberPurchased</td>";
            print "<td>$priceInUSD</td>";
            print "<td>$cardProfitInt</td>";
            print "<td>$profitInt</td>";
            print "<td>$profitUSD</td>";
            
            print "</tr>";
        }

        print "</table>";       
    }
        
?>