<?
/*******************************************************************************
**  FILE: block_flagged_purchase.php
**
**  FUNCTION: block_flagged_purchase
**
**  PURPOSE: Block a flagged purchase 
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.03.21
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.11.08
**  Modified to add an exception to clients with same last name
**  Also modified to block both users once IP/Tel reuse is found
**
*********************************************************************************/

    function block_flagged_purchase($input_array)
    {
        $connection = $input_array['connection'];
        $input_array['flagged_phone_blocked'] = 0;
        $input_array['flagged_ip_blocked'] = 0;

        //block_flagged_purchase
        if($input_array['num_reused_phone'] > 0)
        {
            $same_phone_email = $input_array['reused_phone_data']['clientEmail'];
        }
        else
        {
            $same_phone_email = "";
        }

        //Only block if:
        //1. There exists another email address detected with the same phone
        //2. The two users with the same phone do not have the same last name
        //Note: Old GHANAAT stores phone data in cust_table and Clients
        if($same_phone_email != "" && 
           $input_array['payer_email'] != "" &&
           compare_shorter_string($input_array['last_name'],$input_array['reused_phone_data']['clientLastName']) == false)
        {
            $comments = "SAME PHONE AS ".$input_array['payer_email'];
            $query ="UPDATE SMSUsers ";
            $query.="SET airtimeStatus = 'BLOCKED', ";
            $query.="comments = '$comments' ";
            $query.="WHERE ";
            $query.="emailAddress = '".$same_phone_email."' ";
            $query.="OR ";
            $query.="emailAddress = '".$input_array['payer_email']."' ";
            mysql_query($query,$connection);

            $email_msg = $input_array['ip_reused_info'];
            $email_msg.= $input_array['phone_reused_info'];

            $subject = "BLOCKED / ";
            $subject.= "$same_phone_email / ";
            $subject.= $input_array['payer_email']." ";
            $subject.= "RE-USED PHONE OF ".$same_phone_email." / ";
            $subject.= $input_array['contact_phone']." ";
            sendEmail(FROM_NAME,FROM_EMAIL,FROM_EMAIL,SUPER_TECH_ADMIN,$subject,$email_msg);

            $input_array['flagged_phone_blocked'] = 1;
            $input_array['comments'].= "REUSED_PHONE";
        }

        if($input_array['num_reused_ip'] > 0)
        {
            $same_ip_email = $input_array['reused_ip_data']['clientEmail'];
        }
        else
        {
            $same_ip_email = "";
        }

        //Only block if
        //1. There exists another email address detected with the same IP.
        //2. This email address has not already been blocked by the same phone feature previously called.
        //3. The two users with the same IP do not have the same last name.
        //4. The IP address does not belong to AOL.
        if(($same_ip_email != "") &&
           ($same_ip_email != $same_phone_email) &&
           ($input_array['payer_email'] != "") &&
           (compare_shorter_string($input_array['last_name'],$input_array['reused_ip_data']['clientLastName']) == false ) &&
          !($input_array['ip_location']['ip_country'] == "US" &&
           ($input_array['ip_location']['ip_state'] == "(null)" || 
            $input_array['ip_location']['ip_state'] == "" )))
        {
            $comments = "SAME IP AS ".$input_array['payer_email'];
            $query ="UPDATE SMSUsers ";
            $query.="SET airtimeStatus = 'BLOCKED', ";
            $query.="comments = '$comments' ";
            $query.="emailAddress = '".$same_ip_email."' ";
            $query.="OR ";
            $query.="emailAddress = '".$input_array['payer_email']."' ";
            mysql_query($query,$connection);

            $email_msg = $input_array['ip_reused_info'];
            $email_msg.= $input_array['phone_reused_info'];

            $subject = "BLOCKED / ";
            $subject.= "$same_ip_email / ";
            $subject.= $input_array['payer_email']." ";
            $subject.= "RE-USED IP OF ".$same_ip_email." / ";
            $subject.= $clientIP." ";
            sendEmail(FROM_NAME,FROM_EMAIL,FROM_EMAIL,SUPER_TECH_ADMIN,$subject,$email_msg);

            $input_array['flagged_ip_blocked'] = 1;
            $input_array['comments'].= "REUSED_IP";
        }

        if($input_array['num_diff_state'] > 0)
        {
            $diff_state_email = $input_array['diff_state_data']['clientEmail'];
        }
        else
        {
            $diff_state_email = "";
        }

        if($diff_state_email != "" &&
           $diff_state_email != $same_phone_email &&
           $diff_state_email != $same_ip_email)
        {
            $comments = "DIFFERENT STATES";
            $query ="UPDATE SMSUsers ";
            $query.="SET airtimeStatus = 'BLOCKED', ";
            $query.="comments = '$comments' ";
            $query.="WHERE emailAddress = '".$diff_state_email."'";
            //mysql_query($query,$connection);

            $email_msg = $input_array['ip_reused_info'];
            $email_msg.= $input_array['phone_reused_info'];
            $email_msg.= $input_array['diff_state_info'];

            $subject = "BLOCKED / ";
            $subject.= "$diff_state_email / ";
            $subject.= "DIFFERENT STATES / ";
            $subject.= $input_array['diff_state_data']['ip_state']." / ";
            $subject.= $input_array['order_data']['ip_state'];
            //sendEmail(FROM_NAME,FROM_EMAIL,FROM_EMAIL,SUPER_TECH_ADMIN,$subject,$email_msg);    
        }

        return $input_array;
    }
?>