<?
include_once $_SERVER['DOCUMENT_ROOT']."/src/blockCountry.php";

$country_match = (strcmp($ip_country,$clientCountry) == 0) ||
                   $ip_country == "" || $clientCountry == "";

$country_blocked = isBlockedCountry($ipaddress);

if( $country_blocked )
{
    //There has been a country mismatch
    $status="Blocked";
    
    //block this client with a country mismatch
    $blockQuery = "UPDATE cust_table SET status = '$status' WHERE clientEmail='$clientEmail'";
    mysql_query($blockQuery) or handleDatabaseError('' . mysql_error(),$blockQuery);

    //email to support to be parsed to receivers_info.php    
    $from    = SUPPORT_EMAIL;
    $to      = SUPER_TECH_ADMIN;
    $subject = "BLOCKED COUNTRY / $clientEmail / $firstname $lastname / ";
    $subject.= "$clientPhone / $clientCountry / $ip_country";
    $msg     = "This client perfomed a transaction from a blocked country:\n";
    $msg    .= "Client Email: $clientEmail\n";
    $msg    .= "Transaction ID: $transID\n";
    $msg    .= "Client IP: $ipaddress\n\n";
    $msg    .= "MaxMind Country: $ip_country\n";
    $msg    .= "PayPal Country: $clientCountry\n";
    $msg    .= "PayPal Country: $addressCountry\n";
    $msg    .= "\n\n";
    $debug_msg .= "FOR DEBUG PURPOSES\n";
    $debug_msg .= "HTTP_CLIENT_IP: ".$_SERVER['HTTP_CLIENT_IP']."\n";
    $debug_msg .= "HTTP_X_FORWARDED_FOR: ".$_SERVER['HTTP_X_FORWARDED_FOR']."\n";
    $debug_msg .= "REMOTE_ADDR: ".$_SERVER['REMOTE_ADDR']."\n";
    sendEmail(DOMAIN_NAME,$from,$to,$bcc,$subject,$msg.$debug_msg);

    //email client
    $cfrom    = "From: ".SUPPORT_EMAIL;
    $cto      = $clientEmail;
    $csubject = "GhanaAirTime.com Order";
    $cmsg     = "Dear $firstname,\n";
    $cmsg    .= "Thank you for your order with Ghanaairtime.com. ";
    $cmsg    .= "Your order will require manual processing. Thanks for your patience";
    @mail($cto,$csubject,$cmsg,$cfrom);
    ?>

    <form name="recieve" action="recievers_info.php" method="POST">
        <input type="hidden" name="fbs" value="fbs"/>
        <input type="hidden" name="flag" value="0"/>
        <input type="hidden" name="blocked" value="true"/>
        <input type="hidden" name="note" value="Thanks for your order. We cannot process your request at this time someone from Ghanaairtime will contact you soon after you fill out the form below to verify your order"/>
        <input type="hidden" name="to" value="<?php echo $to; ?>" />
        <input type="hidden" name="from" value="<?php echo $cfrom; ?>" />
        <input type="hidden" name="subject" value="<?php echo $subject; ?>" />
        <input type="hidden" name="msg" value="<?php echo $msg; ?>" />
        <input type="hidden" name="error" value="Thanks for your order\nWe cannot process your request at this time\nSomeone from Ghanaairtime will contact you soon" />
    </form>

    <script>
        document.recieve.submit();
    </script>

    <?php

    exit();
}
else if( !$country_match )
{
    //There has been a country mismatch
    $status="Blocked";
    
    //block this client with a country mismatch
    $blockQuery = "UPDATE cust_table SET status = '$status' WHERE clientEmail='$clientEmail'";
    mysql_query($blockQuery) or handleDatabaseError('' . mysql_error(),$blockQuery);

    //email to support to be parsed to receivers_info.php    
    $from    = SUPPORT_EMAIL;
    $to      = SUPER_TECH_ADMIN;
    $subject = "COUNTRY MISMATCH / $clientEmail / $firstname $lastname / ";
    $subject.= "$clientPhone / $clientCountry / $ip_country";
    $msg     = "There has been a country mismatch: \n";
    $msg    .= "Client Email: $clientEmail \n";
    $msg    .= "Transaction ID: $transID \n";
    $msg    .= "Client IP: $ipaddress \n\n";
    $msg    .= "MaxMind Country: $ip_country \n";
    $msg    .= "PayPal Country: $clientCountry\n";
    $msg    .= "PayPal Country: $addressCountry\n";
    $msg    .= "\n\n";
    $debug_msg .= "FOR DEBUG PURPOSES\n";
    $debug_msg .= "HTTP_CLIENT_IP: ".$_SERVER['HTTP_CLIENT_IP']."\n";
    $debug_msg .= "HTTP_X_FORWARDED_FOR: ".$_SERVER['HTTP_X_FORWARDED_FOR']."\n";
    $debug_msg .= "REMOTE_ADDR: ".$_SERVER['REMOTE_ADDR']."\n";
    sendEmail(DOMAIN_NAME,$from,$to,$bcc,$subject,$msg.$debug_msg);

    //email client
    $cfrom    = "From: ".SUPPORT_EMAIL;
    $cto      = $clientEmail;
    $csubject = "GhanaAirTime.com Order";
    $cmsg     = "Dear $firstname, \n";
    $cmsg    .= "Thank you for your order with Ghanaairtime.com. ";
    $cmsg    .= "Your order will require manual processing. Thanks for your patience";
    @mail($cto,$csubject,$cmsg,$cfrom);
    ?>

    <form name="recieve" action="recievers_info.php" method="POST">
        <input type="hidden" name="fbs" value="fbs"/>
        <input type="hidden" name="flag" value="0"/>
        <input type="hidden" name="blocked" value="true"/>
        <input type="hidden" name="note" value="Thanks for your order. We cannot process your request at this time someone from Ghanaairtime will contact you soon after you fill out the form below to verify your order"/>
        <input type="hidden" name="to" value="<?php echo $to; ?>" />
        <input type="hidden" name="from" value="<?php echo $cfrom; ?>" />
        <input type="hidden" name="subject" value="<?php echo $subject; ?>" />
        <input type="hidden" name="msg" value="<?php echo $msg; ?>" />
        <input type="hidden" name="error" value="Thanks for your order\nWe cannot process your request at this time\nSomeone from Ghanaairtime will contact you soon" />
    </form>

    <script>
        document.recieve.submit();
    </script>

    <?php

    exit();
}
 ?>