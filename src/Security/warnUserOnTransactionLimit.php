<?
    /*
    201101131656 EAST: created this file to separate the functions into single function files
    */

    function warnUserOnTransactionLimit($clientArray,
                                        $countryAbbreviation,
                                        $debug = false)
    {
        $purchaseLimitReached = $clientArray['limit_reached'];
        $clientEmail          = $clientArray['payer_email'];

        //if purchases exceed configured maximum then set customer status to blocked
        if ( $purchaseLimitReached )
        {
            if($debug) echo"User $clientEmail is at the daily tx limit <br>";
            $limitExceededMsg = "YOU HAVE REACHED YOUR MAXIMUM NUMBER OF TRANSACTIONS FOR THE DAY. ";
            $limitExceededMsg.= "PLEASE COME BACK TOMORROW.";
            
            echo "<p align=\"center\"><strong><font color=\"#FF0000\"> ";
            echo "$limitExceededMsg ";
            echo "</font></strong></p>";
            
            //$bcc = SUPER_TECH_ADMIN;
            $subject = "User $clientEmail informed of MAX";
            sendEmail(FROM_NAME,FROM_EMAIL,TECH_ADMIN,$bcc,$subject,$email_msg);
        }
        else
        {
            if($debug) echo"User $clientEmail is below the daily tx limit <br>";
            
            $subject = "User $clientEmail NOT informed of MAX ";
            //sendEmail(FROM_NAME,FROM_EMAIL,TECH_ADMIN,SUPER_TECH_ADMIN,$subject,$email_msg);
        }
    }
?>