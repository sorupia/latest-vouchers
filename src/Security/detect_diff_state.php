<?
/*******************************************************************************
**  FILE: detect_diff_state.php
**
**  FUNCTION: detect_diff_state
**
**  PURPOSE: Function to detect if a client has purchased from
**           a different state in the past 24 hours.
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Montreal)   DATE: 2011.06.16
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.11.09
**               Modified diff_state_info to include 'state of $ip_state'
**               Added check for blank ip_state
**
*********************************************************************************/

    function detect_diff_state($input_array)
    {
        $clientEmail     = $input_array['payer_email'];
        $ip_state        = $input_array['order_data']['ip_state'];
        $diff_state_info = "Client has purchased from same state of $ip_state in past 24hrs";
        $connection      = $input_array['connection'];

        $required_time_ago  = mktime(0, 0, 0, date("m") , date("d")-1 , date("Y"));
		$required_timeline  = date("Y-m-d H:i:s",$required_time_ago);

        if($ip_state != "")
        {
            $query = "SELECT * ";
            $query.= "FROM Sales ";
            $query.= "WHERE clientEmail = '$clientEmail' ";
            $query.= "AND saleDate > '$required_timeline' ";
            $query.= "AND ip_state <> '$ip_state' ";

            $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
            $input_array['num_diff_state']  = mysql_num_rows($result);
            $input_array['diff_state_data'] = mysql_fetch_array($result);
            
            if($input_array['num_diff_state'] > 0)
            {
                $diff_state_info = "Client: $clientEmail purchased from $ip_state ";
                $diff_state_info.= "AND ".$input_array['diff_state_data']['ip_state']." ";
                $diff_state_info.= "in the past 24 hours";
            }
        }
        else
        {
            $diff_state_info = "IP State $ip_state is blank";
        }
        $input_array['diff_state_info'] = $diff_state_info;

        return $input_array;
    }
?>