<?
    function verify_ip_limit($input_array)
    {
        $connection = $input_array['connection'];
        $ip_address = $input_array['ip_address'];

        $required_time_ago  = mktime(0, 0, 0, date("m") , date("d")-CX_LIMIT_TIMELINE , date("Y"));
		$required_timeline  = date("Y-m-d H:i:s",$required_time_ago);

        $query = "SELECT SUM(amount) as ip_total ";
        $query.= "FROM Sales ";
        $query.= "WHERE clientIP = '$ip_address' ";
        $query.= "AND saleDate > '$required_timeline' ";

        $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
        $data = mysql_fetch_array($result);
        
        $ip_total = $data['ip_total'] + 0;

        if($ip_total > CLIENT_LIMIT)
        {
            $input_array['ip_limit_exceeded'] = 1;
            $input_array['error'].= "Unfortunately you have exceeded the limit of ".CLIENT_LIMIT." ";
            $input_array['error'].= "for the past ".CX_LIMIT_TIMELINE." day(s).<br>";
        }
        else
        {
            $input_array['ip_limit_exceeded'] = 0;
        }

        $input_array['ip_total'] = $ip_total;

        $input_array['debug_output'].="ip_total: $ip_total<br>\n";
        
        return $input_array;
    }
?>