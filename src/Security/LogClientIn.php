<?
    function logClientIn($inputArray)
    {
        $loginResult  = 0;
        $emailAddress = $inputArray['payer_email'];
        $connection   = $inputArray['connection'];
        $clientIP     = $inputArray['clientIP'];

        $inputArray['debug_output'].= "logClientIn function\n";

        if($_SESSION['ATSubscriberLoggedIn']===true)
        {
            //do nothing
            $inputArray['debug_output'].= "Client already logged in\n";
        }
        else
        {
            $inputArray['debug_output'].= "Client not yet logged in\n";
        
            if($emailAddress != "")
            {
                $query    = "SELECT * ";
                $query   .= "FROM SMSUsers ";
                $query   .= "WHERE emailAddress='$emailAddress'";
                $result   = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
                $num_rows = mysql_num_rows($result);
                $data     = mysql_fetch_array($result);

                if ($num_rows==1)
                {
                    $month=date("F");
                    $_SESSION['user']  = $data['emailAddress'];
                    $_SESSION['cname'] = $data['firstName'];
                    $_SESSION['ATSubscriberLoggedIn'] = true;
                    $_SESSION['smsLoggedIn']     = true;
                    $_SESSION['clientFirstName'] = $data['firstName'];
                    $_SESSION['emailAddress']    = $data['emailAddress'];
                    $_SESSION['smsCreditsLeft']  = $data['creditsLeft'];
                    $_SESSION['longMsg']="You have ".$data['creditsLeft']." free SMS credits left for $month";
                    $loginResult = 1;
                    $inputArray['debug_output'].= "Client now logged in\n";
                }

                logLogin($emailAddress, $clientIP, $loginResult, $connection);
            }
        }

        return $inputArray;
    }
?>