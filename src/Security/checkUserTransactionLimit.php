<?
/*******************************************************************************
**   FILE: checkUserTransactionLimit.php
**
**   FUNCTION: checkUserTransactionLimit
**
**   PURPOSE: Function to check if the user has exceeded their limit
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2011.01.13 1656
**   created this file to separate the functions into single function files
**
**   MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 2011.01.15 0015
**   added $comment to the outputArray so that the auto unblock is more precise
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 2015.12.01
**  Fixed bug that distorts debug comments
**
*********************************************************************************/

    function checkUserTransactionLimit($inputArray)
    {
        $countryAbbreviation = COUNTRY_ABBREVIATION;
        $clientEmail         = $inputArray['payer_email'];
        $debug               = $inputArray['debug'];
        $connection          = $inputArray['connection'];
        
        $limit_reached = 0;
        $email_msg = "";
        $limit = DAILY_TRANSACTION_LIMIT;
        $resultArray = array();

        //obtain todays date
        $today = date("Y-m-d")." 00:00:01";

        //obtain date and time of exactly 24 hours ago
        $utc_date_24hrs_ago = mktime(date("H"), date("i"), date("s"), date("m")  , date("d")-1, date("Y"));
        $date_time_24hrs_ago = date("Y-m-d H:i:s",$utc_date_24hrs_ago);//Y-m-d H:i:s
        
        if($debug) echo "date_time_24hrs_ago: $date_time_24hrs_ago <br>";
        
        //obtain date and time of exactly 48 hours ago
        $utc_date_48hrs_ago = mktime(date("H"), date("i"), date("s"), date("m")  , date("d")-2, date("Y"));
        $date_time_48hrs_ago = date("Y-m-d H:i:s",$utc_date_48hrs_ago);//Y-m-d H:i:s
        if($debug) echo "date_time_48hrs_ago: $date_time_48hrs_ago <br>";
        
        //obtain the date of first purchase for this client
        $query = "SELECT dateWeSoldIt FROM UsedCards WHERE clientEmail = '$clientEmail' ORDER BY dateWeSoldIt ASC LIMIT 1";
        //$query = "SELECT dateOfFirstPurchase FROM Clients WHERE clientEmail = '$clientEmail'";
        $row = executeSingleRowQuery($query,$connection,$debug);
        $dateOfFirstPurchase = $row['dateWeSoldIt'];
        if($debug) echo"Date of first purchase query : $query <br>";
        if($debug) echo"Date of first purchase by $clientEmail : $dateOfFirstPurchase <br>";

        //determine reference date time & the period limit
        $ref_date = $dateOfFirstPurchase;
        if($dateOfFirstPurchase >= $date_time_24hrs_ago)
        {
            $limit = NEW_CLIENT_24HR_TX_LIMIT;

            if($debug) echo "$dateOfFirstPurchase >= $date_time_24hrs_ago <br>";
            $email_msg.= "dateOfFirstPurchase >= date_time_24hrs_ago\n";
            $email_msg.= "$dateOfFirstPurchase >= $date_time_24hrs_ago\n";
            $user_limit_comments = "MAX_TX_REACHED_24HR";
        }
        else if($dateOfFirstPurchase >= $date_time_48hrs_ago)
        {
            $limit = NEW_CLIENT_48HR_TX_LIMIT;

            if($debug) echo "$dateOfFirstPurchase >= $date_time_48hrs_ago <br>";
            $email_msg.= "dateOfFirstPurchase >= date_time_48hrs_ago\n";
            $email_msg.= "$dateOfFirstPurchase >= $date_time_48hrs_ago\n";
            $user_limit_comments = "MAX_TX_REACHED_48HR";
        }
        else
        {
            //not a new user

            $ref_date = $today;
            $limit = DAILY_TRANSACTION_LIMIT;
            if($debug) echo "Client is not a new user in past 48hrs <br>";
            $email_msg.= "Client is not a new user in past 48hrs\n";
            $user_limit_comments = "MAX_TX_REACHED";
        }

        //check the number of purchases the customer has made since '$ref_date'
        $limitQuery = "SELECT dateWeSoldIt,cardPIN FROM UsedCards WHERE clientEmail = '$clientEmail' AND dateWeSoldIt >= '$ref_date' ";
        $numberOfPurchases = executeCountQuery($limitQuery,$connection,$debug);
        
        //$email_msg.="limitQuery: $limitQuery\n";
        $email_msg.="numberOfPurchases: $numberOfPurchases\n";

        //if purchases exceed configured maximum then set customer status to blocked
        if ( $numberOfPurchases >= $limit )
        {
            $limit_reached = 1;
        }
        else
        {
            $limit_reached = 0;
            $user_limit_comments = "MAX_TX_NOT_REACHED";
        }

        $inputArray['limit_reached']        = $limit_reached;
        $inputArray['numberOfPurchases']    = $numberOfPurchases;
        $inputArray['email_msg']            = $email_msg;
        $inputArray['user_limit_comments']  = $user_limit_comments;
        return $inputArray;
    }
?>
