<?
    function verify_receiver_status($input_array)
    {
        $connection        = $input_array['connection'];
        $receiver_phone    = $input_array['receiver_phone'];

        $query = "SELECT * ";
        $query.= "FROM Receivers ";
        $query.= "WHERE receiverPhone = '$receiver_phone' ";
        $query.= "AND receiverStatus = 'BLOCKED' ";

        $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
        $exists = mysql_num_rows($result);

        if($exists == 0)
        {
            $input_array['receiver_status_blocked'] = 0;
        }
        else
        {
            $input_array['receiver_status_blocked'] = 1;
            $input_array['error'].= "Unfortunately $receiver_phone is not supported. Please try another number<br>";
        }

        $input_array['debug_output'].="receiver_status_query: $query<br>\n";

        return $input_array;
    }
?>