<?
    function verify_receiver_lower_limit($input_array)
    {
        if($input_array['receiver_amount_in_local'] < RECEIVER_LOWER_LIMIT)
        {
            $input_array['receiver_limit_exceeded'] = 1;
            $input_array['error'].= "Unfortunately this amount is lower than the minimum of ".RECEIVER_LOWER_LIMIT.". ";
            $input_array['error'].= "Please increase the amount and try again.<br>";
        }
        else
        {
            $input_array['receiver_amount_low'] = 0;
        }

        return $input_array;
    }
?>