<?
/*******************************************************************************
**  FILE: flag_reused_receiver_phone.php
**
**  FUNCTION: flag_reused_receiver_phone
**
**  PURPOSE: Flag receiver phones that are used by more than one client in Receivers
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.11.19
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2013.08.08
**  Included a check to ignore wrong recipient phones that only include
**  the country prefix.
**
*********************************************************************************/

function flag_reused_receiver_phone($input_array)
{
    $clientEmail                               = $input_array['payer_email'];
    $receiver_phone                            = $input_array['order_data']['receiver_phone'];
    $connection                                = $input_array['connection'];
    $input_array['reused_receiver_phone_info'] = "Receiver $receiver_phone not reused in Receivers\n";
    $input_array['num_reused_receiver_phone']  = 0;

    $i = 0; //iterator

    if($receiver_phone != "" &&
       $receiver_phone != COUNTRY_MOBILE_PREFIX &&
       $clientEmail != "")
    {
        //Search the Receivers table
        $query = "SELECT * ";
        $query.= "FROM ";
        $query.= "Receivers, SMSUsers ";
        $query.= "WHERE ";
        $query.= "Receivers.clientEmail = SMSUsers.emailAddress ";
        $query.= "AND ";
        $query.= "Receivers.ReceiverPhone = '$receiver_phone' ";
        $query.= "AND ";
        $query.= "Receivers.clientEmail <> '$clientEmail' ";

        $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
        $input_array['num_reused_receiver_phone'] = mysql_num_rows($result);

        while($row = mysql_fetch_array($result))
        {
            if(strlen($row['clientEmail']) > 4)
            {
                $input_array['reused_receiver_phone_data'][$i++] = $row;

                if($i == 1)
                {
                    $input_array['reused_receiver_phone_info'] = "Receivers: ";
                }
                else
                {
                    $input_array['reused_receiver_phone_info'].= "Receivers: ";
                }

                $input_array['reused_receiver_phone_info'].= $row['receiverPhone'];
                $input_array['reused_receiver_phone_info'].= " (".$row['receiverStatus'].") ";
                $input_array['reused_receiver_phone_info'].= "also received airtime from ";
                $input_array['reused_receiver_phone_info'].= $row['firstName'];
                $input_array['reused_receiver_phone_info'].= " ";
                $input_array['reused_receiver_phone_info'].= $row['lastName'];
                $input_array['reused_receiver_phone_info'].= ", ";
                $input_array['reused_receiver_phone_info'].= $row['clientEmail'];
                $input_array['reused_receiver_phone_info'].= " (".$row['airtimeStatus'].")";
                $input_array['reused_receiver_phone_info'].= ".\n";
            }
        }
    }

    return $input_array;
}
?>