<?
function handle_flagged_purchase($input_array)
{
    $clientEmail    = $input_array['payer_email'];
    $first_name     = $input_array['first_name'];
    $last_name      = $input_array['last_name'];
    $clientPhone    = $input_array['contact_phone'];
    $clientCountry  = $input_array['residence_country'];
    $ip_country     = $input_array['order_data']['ip_country'];
    $ip_state       = $input_array['order_data']['ip_state'];
    $transID        = $input_array['txn_id'];
    $ipaddress      = $input_array['clientIP'];
    $addressCountry = $input_array['address_country'];
    $addressState   = $input_array['address_state'];
    $itemIDNumber   = $input_array['item_number'];
    $receiver_phone = $input_array['order_data']['receiver_phone'];

    $comments       = $input_array['comments'];

    $ipLocationArray = $input_array['order_data'];

    //There has been a flagged purchase that was blocked
    $status="BLOCKED";

    //Email support
    $from    = SUPPORT_EMAIL;
    $to      = SUPPORT_EMAIL;
    $bcc     = SUPER_TECH_ADMIN;
    $subject = "$comments / $clientEmail / $first_name $last_name / ";
    $subject.= "$clientPhone / $clientCountry / $ip_country / $addressState / $ip_state";
    $msg     = "There has been a flagged purchase and the user has been added to Blocked Clients Queue:\n";
    $msg    .= "Client Email: $clientEmail\n";
    $msg    .= "Receiver Phone: $receiver_phone\n";
    $msg    .= "Item ID: $itemIDNumber\n";
    $msg    .= "Transaction ID: $transID\n";
    $msg    .= $input_array['phone_reused_info']." ";
    $msg    .= $input_array['ip_reused_info']." ";
    $msg    .= "Client IP: $ipaddress\n\n";
    $msg    .= "MaxMind Country: $ip_country\n";
    $msg    .= "MaxMind State: $ip_state\n";
    $msg    .= "PayPal Country: $clientCountry\n";
    $msg    .= "PayPal Country: $addressCountry\n";
    $msg    .= "PayPal State: $addressState\n";
    $msg    .= "\n\n";
    $debug_msg .= "FOR DEBUG PURPOSES\n";
    $debug_msg .= "HTTP_CLIENT_IP: ".$_SERVER['HTTP_CLIENT_IP']."\n";
    $debug_msg .= "HTTP_X_FORWARDED_FOR: ".$_SERVER['HTTP_X_FORWARDED_FOR']."\n";
    $debug_msg .= "REMOTE_ADDR: ".$_SERVER['REMOTE_ADDR']."\n";

    //Email client
    $cto      = $clientEmail;
    $csubject = DOMAIN_NAME." Order";
    $cmsg     = "Dear $first_name, \n";
    $cmsg    .= "Thank you for your order with ".DOMAIN_NAME.". ";
    $cmsg    .= "Your order will require manual processing from one of our support staff. Thank you for your patience";

    $resultArray = insertBlockedClient(COUNTRY_ABBREVIATION,$input_array,$ipLocationArray);

    if($resultArray['status'] == true)
    {
        sendEmail(DOMAIN_NAME,$from,$to,$bcc,$subject,$msg.$debug_msg);
        sendEmail(DOMAIN_NAME,$from,$cto,$cbcc,$csubject,$cmsg);
        include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayManualProcInfo.php";
    }
    else
    {
        echo $resultArray['message'];
    }
}
?>