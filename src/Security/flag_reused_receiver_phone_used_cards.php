<?
/*******************************************************************************
**  FILE: flag_reused_receiver_phone_used_cards.php
**
**  FUNCTION: flag_reused_receiver_phone_used_cards
**
**  PURPOSE: Flag receiver phones that are used by more than one client in UsedCards
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.11.19
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2013.08.08
**  Included a check to ignore wrong recipient phones that only include
**  the country prefix.
**
*********************************************************************************/

function flag_reused_receiver_phone_used_cards($input_array)
{
    $clientEmail                                          = $input_array['payer_email'];
    $receiver_phone                                       = $input_array['order_data']['receiver_phone'];
    $connection                                           = $input_array['connection'];
    $input_array['reused_receiver_phone_used_cards_info'] = "Receiver $receiver_phone not reused in UsedCards\n";
    $input_array['num_reused_receiver_phone_used_cards']  = 0;

    $number_of_months_back = REUSED_RECEIVER_PHONE_MONTHS;
    $utc_n_months_ago = mktime(0, 0, 0, date("m")- $number_of_months_back , date("d") , date("Y"));
    $n_months_ago     = date("Y-m-d H:i:s",$utc_n_months_ago);

    $i = 0; //iterator
    $array_index = 0;
    $found_clients_array = array();

    if($receiver_phone != "" &&
       $receiver_phone != COUNTRY_MOBILE_PREFIX &&
       $clientEmail != "")
    {
        //First search UsedCards table
        $query = "SELECT * ";
        $query.= "FROM ";
        $query.= "UsedCards, SMSUsers ";
        $query.= "WHERE ";
        $query.= "UsedCards.clientEmail = SMSUsers.emailAddress ";
        $query.= "AND ";
        $query.= "UsedCards.ReceiverPhone = '$receiver_phone' ";
        $query.= "AND ";
        $query.= "UsedCards.clientEmail <> '$clientEmail' ";

        if($number_of_months_back > 0)
        {
            $query.= "AND ";
            $query.= "UsedCards.dateWeSoldIt > '$n_months_ago' ";
        }

        $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
        $input_array['num_reused_receiver_phone_used_cards'] = mysql_num_rows($result);

        while($row = mysql_fetch_array($result))
        {
            if(strlen($row['clientEmail']) > 4)
            {
                //Record only clients we have not already found so as to avoid repitition
                if(array_search($row['clientEmail'], $found_clients_array)  === FALSE)
                {
                    $found_clients_array[$array_index++] = $row['clientEmail'];

                    $input_array['reused_receiver_phone_used_cards_data'][$i++] = $row;

                    if($i == 1)
                    {
                        $input_array['reused_receiver_phone_used_cards_info'] = "Used Cards: ";
                    }
                    else
                    {
                        $input_array['reused_receiver_phone_used_cards_info'].= "Used Cards: ";
                    }

                    $input_array['reused_receiver_phone_used_cards_info'].= $row['receiverPhone'];
                    $input_array['reused_receiver_phone_used_cards_info'].= " also received airtime from ";
                    $input_array['reused_receiver_phone_used_cards_info'].= $row['firstName'];
                    $input_array['reused_receiver_phone_used_cards_info'].= " ";
                    $input_array['reused_receiver_phone_used_cards_info'].= $row['lastName'];
                    $input_array['reused_receiver_phone_used_cards_info'].= ", ";
                    $input_array['reused_receiver_phone_used_cards_info'].= $row['clientEmail'];
                    $input_array['reused_receiver_phone_used_cards_info'].= " (".$row['airtimeStatus'].")";
                    $input_array['reused_receiver_phone_used_cards_info'].= ".\n";
                }
            }
        }
    }
    return $input_array;
}
?>