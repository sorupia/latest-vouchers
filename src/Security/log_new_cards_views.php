<?
/*******************************************************************************
**  FILE: log_new_cards_views.php
**
**  FUNCTION: log_new_cards_views
**
**  PURPOSE: log views performed on the new cards table 
**           this is a replacement function for logView in AllCardsTable.php 
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.10.30
*********************************************************************************/

/*
* Purpose: log views performed on the new cards table 
*/

function log_new_cards_views($input_array)
{
    $connection = $input_array['connection'];
    $username   = $input_array['username'];
    $comments   = $input_array['comments'];

    $timeViewed= date("Y-m-d H:i:s");

    $ip_address = getIP();

    $viewLogQuery = "INSERT INTO ";
    $viewLogQuery.= "ViewLog(viewDate,username,ipAddress, comments) ";
    $viewLogQuery.= "VALUES('$timeViewed','$username','$ip_address', '$comments')";
    $viewLogResult=mysql_query($viewLogQuery, $connection) or handleDatabaseError(''. mysql_error(),$viewLogQuery);	
}
?>