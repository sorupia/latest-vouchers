<?
/*******************************************************************************
**  FILE: handle_paypal_service_unavailable.php
**
**  FUNCTION: handle_paypal_service_unavailable
**
**  PURPOSE: Adapted from handle_return_page_catch_all.php.
**  This function is called by /src/Order/ipn_processor_code.php
**
**  Handle situation where PayPal returned 'res' variable is neither VERIFIED nor INVALID
**  res = <HTML><HEAD>
**  <TITLE>Service Unavailable</TITLE>
**  </HEAD><BODY>
**  <H1>Service Unavailable - DNS failure</H1>
**  The server is temporarily unable to service your request.  Please try again
**  later.
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 28.April.2015
**
**  MODIFIED BY: (3nitylabs, Kampala)   DATE:
**
*********************************************************************************/

function handle_paypal_service_unavailable($input_array)
{
    $clientEmail    = $input_array['payer_email'];
    $first_name     = $input_array['first_name'];
    $last_name      = $input_array['last_name'];
    $clientPhone    = $input_array['contact_phone'];
    $clientCountry  = $input_array['residence_country'];
    $transID        = $input_array['txn_id'];
    $addressCountry = $input_array['address_country'];
    $addressState   = $input_array['address_state'];
    $itemIDNumber   = $input_array['item_number'];
    $order_id       = $input_array['invoice'];
    $paypal_result  = $input_array['res'];
    $comments       = "Service Unavailable";

    $input_array['valueInLocal'] = getValueInLocal($itemIDNumber);
    $input_array['networkName']  = getNetworkName($itemIDNumber);

    //Email support
    $from    = CARD_ADMIN;
    $to      = SUPPORT_EMAIL;
    $bcc     = SUPER_TECH_ADMIN;
    $subject = "$clientEmail / $first_name $last_name / ";
    $subject.= "$clientPhone / $clientCountry / $addressState ";
    $msg     = "The user with the following details is in the Order Queue:\n";
    $msg    .= "Client Email: $clientEmail\n";
    $msg    .= "Item ID: $itemIDNumber\n";
    $msg    .= "Transaction ID: $transID\n";
    $msg    .= "Order/Invoice ID: $order_id\n";
    $msg    .= "Residence Country: $clientCountry\n";
    $msg    .= "Address Country: $addressCountry\n";
    $msg    .= "PayPal Address State: $addressState\n\n";
    $msg    .= "Comments: $comments\n";
    $msg    .= "PayPal res: $paypal_result\n\n";
    $msg    .= "Investigate further on what the issue could be and approve if legitimate.";
    $msg    .= "\n\n";
    sendEmail(DOMAIN_NAME,$from,$to,$bcc,$subject,$msg);

    //Email client
    $cto      = $clientEmail;
    $csubject = DOMAIN_NAME." Order";
    $cmsg     = "Dear $first_name, \n";
    $cmsg    .= "Thank you for your order with ".DOMAIN_NAME.". ";
    $cmsg    .= "Your order will require manual processing from one of our support staff. Thank you for your patience";
    sendEmail(DOMAIN_NAME,SUPPORT_EMAIL,$cto,$cbcc,$csubject,$cmsg);

    //include $_SERVER['DOCUMENT_ROOT']."/src/Displays/DisplayManualProcInfo.php";
}
?>