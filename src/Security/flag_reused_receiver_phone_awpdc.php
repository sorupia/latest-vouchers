<?
/*******************************************************************************
**  FILE: flag_reused_receiver_phone_awpdc.php
**
**  FUNCTION: flag_reused_receiver_phone_awpdc
**
**  PURPOSE: Flag receiver phones that are used by more than one client in awpdc
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.11.24
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2013.08.08
**  Included a check to ignore wrong recipient phones that only include
**  the country prefix.
**
*********************************************************************************/

function flag_reused_receiver_phone_awpdc($input_array)
{
    $clientEmail                                     = $input_array['payer_email'];
    $receiver_phone                                  = $input_array['order_data']['receiver_phone'];
    $connection_awpdc                                = $input_array['connection_awpdc'];
    $input_array['reused_receiver_phone_awpdc_info'] = "Receiver $receiver_phone not reused in cust_table\n";
    $input_array['num_reused_receiver_phone_awpdc']  = 0;

    $i = 0; //iterator

    if($receiver_phone != "" &&
       $receiver_phone != COUNTRY_MOBILE_PREFIX &&
       $clientEmail != "")
    {
        //Also search cust_table if GhanaAirTime
        $query = "SELECT * ";
        $query.= "FROM ";
        $query.= "cust_table ";
        $query.= "WHERE ";
        $query.= "rmobile = '$receiver_phone' ";
        $query.= "AND ";
        $query.= "clientEmail <> '$clientEmail' ";

        //we could search for only those not found in the UsedCards table above

        $result = mysql_query($query,$connection_awpdc) or handleDatabaseError(''.mysql_error(),$query);
        $input_array['num_reused_receiver_phone_awpdc'] = mysql_num_rows($result);

        while($row = mysql_fetch_array($result))
        {
            if(strlen($row['clientEmail']) > 4)
            {
                $input_array['reused_receiver_phone_awpdc_data'][$i++] = $row;

                if($i == 1)
                {
                    $input_array['reused_receiver_phone_awpdc_info'] = "cust_table: ";
                }
                else
                {
                    $input_array['reused_receiver_phone_awpdc_info'].= "cust_table: ";
                }
                $input_array['reused_receiver_phone_awpdc_info'].= $row['rmobile'];
                $input_array['reused_receiver_phone_awpdc_info'].= " also received airtime from ";
                $input_array['reused_receiver_phone_awpdc_info'].= $row['firstname'];
                $input_array['reused_receiver_phone_awpdc_info'].= " ";
                $input_array['reused_receiver_phone_awpdc_info'].= $row['lastname'];
                $input_array['reused_receiver_phone_awpdc_info'].= ", ";
                $input_array['reused_receiver_phone_awpdc_info'].= $row['clientEmail'];
                $input_array['reused_receiver_phone_awpdc_info'].= " (".$row['status'].")";
                $input_array['reused_receiver_phone_awpdc_info'].= ".\n";
            }
        }
    }
    return $input_array;
}
?>