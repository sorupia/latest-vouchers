<?
/*******************************************************************************
**  FILE: flag_reused_ip.php
**
**  FUNCTION: flag_reused_ip, flag_reused_telephone
**
**  PURPOSE: Flag both reused Client IPs and Phone Numbers
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Montreal)   DATE: 2011.06.16
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.10.22
**               Check both client IP and phone for blank fields
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.11.12
**               Check both client IP for unknown case
**
*********************************************************************************/

    //flag IP address reused by a different client in last 3 months
    function flag_reused_ip($input_array)
    {
        $clientIP       = $input_array['clientIP'];
        $clientEmail    = $input_array['payer_email'];
        $connection     = $input_array['connection'];
        $ip_reused_info = "IP $clientIP not reused\n";

        $utc_three_months_ago = mktime(0, 0, 0, date("m")-3 , date("d") , date("Y"));
		$three_months_ago     = date("Y-m-d H:i:s",$utc_three_months_ago);

        if($clientIP != ""&& 
           $clientIP != "unknown")
        {
            $query = "SELECT * ";
            $query.= "FROM Sales,SMSUsers ";
            $query.= "WHERE Sales.clientEmail = SMSUsers.emailAddress ";
            $query.= "AND Sales.clientEmail <> '$clientEmail' ";
            $query.= "AND Sales.saleDate > '$three_months_ago' ";
            $query.= "AND Sales.clientIP = '$clientIP' ";

            $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
            $input_array['num_reused_ip'] = mysql_num_rows($result);
            $input_array['reused_ip_data'] = mysql_fetch_array($result);
            
            if($input_array['num_reused_ip'] > 0)
            {
                $ip_reused_info = "Client IP: ".$input_array['clientIP']." ";
                $ip_reused_info.= "has already been used by ".$input_array['reused_ip_data']['firstName']." ";
                $ip_reused_info.= $input_array['reused_ip_data']['lastName'].", ";
                $ip_reused_info.= $input_array['reused_ip_data']['clientEmail']." ";
                $ip_reused_info.= "in the last 3 months \n\n";

                //Observation code
                $subject = "RE USED IP";
                $email_msg = "$clientEmail\n";
                $email_msg.= "Current client last name: ".$input_array['last_name']."\n";
                $email_msg.= "Previous client last name: ".$input_array['reused_ip_data']['lastName'].".\n";
                $email_msg.= "$ip_reused_info\n";
                //sendEmail(FROM_NAME,FROM_EMAIL,SUPER_TECH_ADMIN,$bcc,$subject,$email_msg);
            }
            else
            {
                $ip_reused_info = "IP $clientIP not reused\n";
            }
        }
        else
        {
            $ip_reused_info = "IP $clientIP is blank\n\n";
        }
        $input_array['ip_reused_info'] = $ip_reused_info;

        return $input_array;
    }

    //flag phone number reused by a different client in last 6 months
    function flag_reused_telephone($input_array)
    {
        $clientPhone       = $input_array['contact_phone'];
        $clientEmail       = $input_array['payer_email'];
        $connection        = $input_array['connection'];
        $phone_reused_info = "Telephone $clientPhone not reused\n";


        $strippedPhone = ereg_replace("[^0-9]", "", $clientPhone);

        if($clientPhone != "")
        {
            $query = "SELECT * ";
            $query.= "FROM SMSUsers ";
            $query.= "WHERE cellPhone LIKE '%$clientPhone%' ";
            $query.= "AND emailAddress <> '$clientEmail' ";

            $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
            $input_array['num_reused_phone'] = mysql_num_rows($result);
            $input_array['reused_phone_data'] = mysql_fetch_array($result);

            if($input_array['num_reused_phone'] > 0)
            {
                $phone_reused_info = "Telephone: ".$clientPhone." ";
                $phone_reused_info.= "has already been used by ".$input_array['reused_phone_data']['firstName']." ";
                $phone_reused_info.= $input_array['reused_phone_data']['lastName'].", ";
                $phone_reused_info.= $input_array['reused_phone_data']['emailAddress']."\n\n";
            }
            else
            {
                $query = "SELECT * ";
                $query.= "FROM Clients ";
                $query.= "WHERE phoneNumber LIKE '%$clientPhone%' ";
                $query.= "AND clientEmail <> '$clientEmail' ";

                $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
                $input_array['num_reused_phone'] = mysql_num_rows($result);
                $input_array['reused_phone_data'] = mysql_fetch_array($result);
                
                if($input_array['num_reused_phone'] > 0)
                {
                    $phone_reused_info = "Telephone: ".$clientPhone." ";
                    $phone_reused_info.= "has already been used by ".$input_array['reused_phone_data']['clientFirstName']." ";
                    $phone_reused_info.= $input_array['reused_phone_data']['clientLastName'].", ";
                    $phone_reused_info.= $input_array['reused_phone_data']['clientEmail']."\n\n";

                    //Observation code
                    $subject = "RE USED TELEPHONE";
                    $email_msg = "$clientEmail\n";
                    $email_msg.= "Current client last name: ".$input_array['last_name'].".\n";
                    $email_msg.= "Previous client last name: ".$input_array['reused_phone_data']['clientLastName'].".\n";
                    $email_msg.= "$phone_reused_info\n";
                    //sendEmail(FROM_NAME,FROM_EMAIL,SUPER_TECH_ADMIN,$bcc,$subject,$email_msg);
                }
            }
        }
        else
        {
            $phone_reused_info = "Telephone $clientPhone is blank\n\n";
        }
        $input_array['phone_reused_info'] = $phone_reused_info;

        return $input_array;
    }
?>