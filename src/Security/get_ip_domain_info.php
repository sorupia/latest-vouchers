<?
/************************************************************************************
**
**  FILE: get_ip_domain_info.php
**
**  FUNCTION: get_ip_domain_info
** 
**  PURPOSE: A function to obtain the domain information of an IP using the MaxMind Omni webservice
**  This function was created in response to a request from Ghanaairtime.com.
**  Before using this function, make sure that the Omni service has sufficient credit.
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 11.Mar.2014
**
**/
    function get_ip_domain_info($input_array)
    {
        $buf = "";
        $license_key = MAX_MIND_LICENSE;
        $ipaddress = $input_array['ip_address']; // from GeneralFunctions.php
        $query = "http://maxmind.com:8010/e?l=" . $license_key . "&i=" . $ipaddress;
        $url = parse_url($query);
        $host = $url["host"];
        $path = $url["path"] . "?" . $url["query"];
        $timeout = 1;
        $fp = fsockopen ($host, 80, $errno, $errstr, $timeout);
            //or die('Can not open connection to server.');
        if ($fp)
        {
          fputs ($fp, "GET $path HTTP/1.0\nHost: " . $host . "\n\n");
          while (!feof($fp)) {
            $buf .= fgets($fp, 128);
          }
          $lines = explode("\n", $buf);
          $data = $lines[count($lines)-1];
          fclose($fp);
        }
        else
        {
            if(REPORT_MAX_MIND_ERROR)
            {
                # enter error handing code here
                $errorMsg = "Date and Time: ".date("Y-m-d H:i:s")."\n";
                $errorMsg.= "IP Address: ".getIP()."\n";
                $errorMsg.= "Error: $errno: $errstr\n";
                $errorMsg.= "URL: ".$_SERVER['SCRIPT_FILENAME']."\n";
                //send email
                sendEmail("Sendairtime Web",FROM_EMAIL,TECH_ADMIN,SUPER_TECH_ADMIN,"MaxMind Error",$errorMsg);
            }
        }

        //echo "Your IP Based Geo Location is:   ";
        //echo "Your IP Address is: ".$ipaddress."<br/>";
        //echo $data;

        $quotes_array = array("'","\"");

        $geo = explode(",",$data);
        $ipLocationArray['ip_country']    = $geo[0];
        $ipLocationArray['ip_state']      = $geo[2];
        $ipLocationArray['ip_city']       = str_replace($quotes_array, "", $geo[4]);
        $ipLocationArray['ip_latitude']   = $geo[5];
        $ipLocationArray['ip_longitude']  = $geo[6];
        $ipLocationArray['ip_domain']     = $geo[14];
        $ipLocationArray['ip_address']    = $ipaddress;

        $input_array['ip_location_array'] = $ipLocationArray;

        return $input_array;
    }

?>