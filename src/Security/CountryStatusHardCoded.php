<?
/*******************************************************************************
**  FILE: CountryStatusHardCoded.php
**
**  FUNCTION: CountryStatusHardCoded
**
**  PURPOSE: Check if provided Country is on our hard coded list
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.11.22
**
*********************************************************************************/

    $hard_coded_country_white_list = array("AU","CA","DE","GB");

    function CountryStatusHardCoded($input_array)
    {
        $ip_country          = $input_array['ip_country'];

        global $hard_coded_country_white_list;
        $input_array['country_hard_coded_unblocked'] = 0;
        
        //This hard coded white list was provided by Jacob
        if(array_search($ip_country, $hard_coded_country_white_list)  === FALSE)
        {
            $input_array['country_hard_coded_unblocked'] = 0;
        }
        else
        {
            $input_array['country_hard_coded_unblocked'] = 1;
        }

        return $input_array;
    }
?>