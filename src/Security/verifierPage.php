<?
    if(BLACK_LIST_ACTIVE && clientOnBlackList($clientEmail,$countryAbbreviation,$connection))
    {
        $transactionArray['comments'].= "CLIENT_BLACKLISTED";
        handleBlackListUserState($countryAbbreviation,$transactionArray,$ipLocationArray,$connection); 
        $clientVerified = 0;
        $transactionArray['clientVerified'] = $clientVerified;
        $transactionArray['client_blacklisted'] = 1;
        $transactionArray['client_new'] = 0;
    }
    else if($_SESSION['ATSubscriberLoggedIn'] != true &&
            WHITE_LIST_ACTIVE && !clientOnWhiteList($clientEmail,$countryAbbreviation,$connection))
    {
        handleNewClientState($countryAbbreviation,$transactionArray,$ipLocationArray,$connection);
        $clientVerified = 0;
        $transactionArray['clientVerified'] = $clientVerified;
        $transactionArray['client_whitelisted'] = 0;
        $transactionArray['client_new'] = 1;
    }
    else if($_SESSION['ATSubscriberLoggedIn'] == true)
    {
        $clientVerified = 1;
        $transactionArray['clientVerified'] = $clientVerified;
        $transactionArray['client_already_logged_in'] = 1;
        $transactionArray['client_new'] = 0;
    }
    else
    {
        $clientVerified = 1;
        $transactionArray['clientVerified'] = $clientVerified;
        $transactionArray = logClientIn($transactionArray);

        $transactionArray['client_passed_checks'] = 1;
        $transactionArray['client_whitelisted'] = 1;
        $transactionArray['client_new'] = 0;
    }

?>