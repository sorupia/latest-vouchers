<?
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/compare_pay_and_ip_country.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/LogClientIn.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/handle_country_mismatch.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/checkUserTransactionLimit.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/blockSMSUser.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/detect_diff_state.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/flag_reused_ip.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/limitUserTransactions.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/warnUserOnTransactionLimit.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/verify_receiver_status.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/verify_receiver_limit.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/verify_ip_limit.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/block_flagged_purchase.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/handle_flagged_purchase.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/handle_return_page_catch_all.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/verify_payment_amount.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/flag_reused_receiver_phone.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/flag_reused_receiver_phone_used_cards.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/notify_on_reused_receiver_phone.php';
    include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/handle_paypal_service_unavailable.php';
?>