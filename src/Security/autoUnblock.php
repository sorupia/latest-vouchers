<?
/*
20100114: Added cron job to automatically unblock clients that were blocked by MAX_TX_REACHED
*/
    include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php";

    //Note that this function will have a feature interaction with the 24hr and 48hr new clients
    //in that if a 24hrs client is blocked just before midnight, (s)he will be unblocked at midnight
    //before the 24hrs lapse
    function autoUnblockClients($inputArray)
    {
        $today = date("Y-m-d")." 00:00:01";
        $now = date("Y-m-d H:i:s");
        $performUnblock = false;
        $numberUnblocked = 0;
        $count = 0;
        $debug = true;

        //search cust_table or whatever table has the Clients we want to unblock
        $query = "SELECT emailAddress, comments, dateLastBlocked ";
        $query.= "FROM SMSUsers ";
        $query.= "WHERE ";
        $query.= "airtimeStatus = 'BLOCKED' ";
        $query.= "AND ";
        $query.= "comments LIKE '%MAX_TX_REACHED%' ";
        $query.= "AND ";
        $query.= "dateLastBlocked < '$today' ";

        $connection=connect2SMSDB(COUNTRY_ABBREVIATION);
            $result = mysql_query($query, $connection) or handleDatabaseError(''.mysql_error(),$query);

            $unblockQuery = "UPDATE SMSUsers ";
            $unblockQuery.= "SET ";
            $unblockQuery.= "airtimeStatus = 'VERIFIED', ";
            $unblockQuery.= "subscriberStatus = 'VERIFIED', ";
            $unblockQuery.= "comments = 'AUTO_UNBLOCKED' ";
            $unblockQuery.= "WHERE ";
            
            $whereQuery = "";
            $unblockedClients = "";

            while ($data = mysql_fetch_array($result, MYSQL_ASSOC))
            {
                $clientEmail     = $data['emailAddress'];
                $comments        = $data['comments'];
                $dateLastBlocked = $data['dateLastBlocked'];

                $performUnblock  = false;

                if($comments == "MAX_TX_REACHED_24HR")
                {
                    if($debug) echo "Client had reached a 24hrs maximum <br>";
                    
                    //obtain date and time of exactly 24 hours ago
                    $utc_date_24hrs_ago = mktime(date("H"), date("i"), date("s"), date("m")  , date("d")-1, date("Y"));
                    $date_time_24hrs_ago = date("Y-m-d H:i:s",$utc_date_24hrs_ago);//Y-m-d H:i:s

                    if($date_time_24hrs_ago > $dateLastBlocked)
                    {
                        if($debug) echo "It has been more than 24hrs. <br>";
                        $performUnblock = true;
                    }
                }
                else if($comments == "MAX_TX_REACHED_48HR")
                {
                    if($debug) echo "Client had reached a 48hrs maximum. <br>";

                    //obtain date and time of exactly 48 hours ago
                    $utc_date_48hrs_ago = mktime(date("H"), date("i"), date("s"), date("m")  , date("d")-2, date("Y"));
                    $date_time_48hrs_ago = date("Y-m-d H:i:s",$utc_date_48hrs_ago);//Y-m-d H:i:s

                    if($date_time_48hrs_ago > $dateLastBlocked)
                    {
                        if($debug) echo "It has been more than 48hrs. <br>";
                        $performUnblock = true;
                    }
                }
                else
                {
                    if($debug) echo "Perform unblock since this client's limit is on a day's basis. <br>";
                    $performUnblock = true;
                }

                if($performUnblock  && $clientEmail != '')
                {
                    if($count > 0) $whereQuery.= "OR ";
                    
                    $whereQuery.= "emailAddress = '$clientEmail'";
                    $unblockedClients.= "$clientEmail\n";
                    $count++;                
                }
            }

            if($count > 0)
            {
                $unblockQuery.= $whereQuery;

                //query to unblock those that were blocked due to max transactions any day before $today
                if($debug) echo $unblockQuery."<br>";
                $result = mysql_query($unblockQuery, $connection) or handleDatabaseError(''.mysql_error(),$unblockQuery);
                $numberUnblocked = mysql_affected_rows();
            }
        disconnectDB($connection);

        $outputArray['numberUnblocked'] = $numberUnblocked;
        $outputArray['unblockedClients'] = $unblockedClients;

        return $outputArray;
    }

?>