<?
    function compare_pay_and_ip_country($input_array)
    {
        $address_country = $input_array['address_country'];
        $ip_country      = $input_array['order_data']['ip_country'];

        $country_match = (strcmp($ip_country,$address_country) == 0) || $ip_country == "" || $address_country == "";
        return $country_match;
    }
    
    function compare_pay_and_ip_state($input_array)
    {
        $address_state = $input_array['address_state'];
        $ip_state      = $input_array['order_data']['ip_state'];        

        $state_match = (strcmp($ip_state,$address_state) == 0) || $ip_state == "" || $address_state == "";
        return $state_match;
    }
?>