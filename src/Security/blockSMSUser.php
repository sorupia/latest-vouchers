<?
/*******************************************************************************
**   FILE: blockSMSUser.php
**
**   FUNCTION: blockSMSUser
**
**   PURPOSE: Function to block a user that has exceeded their limit
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2011.01.13 1656
**   created this file to separate the functions into single function files
**
**   MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 2011.01.15 0015
**   made the comment more specific for more precise auto unblocking
**
**  MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 2015.12.01
**  Fixed bug that distorts debug comments
**
*********************************************************************************/

    /*
    201101131656 EAST: created this file to separate the functions into single function files
    201101150015 EAST: made the comment more specific for more precise auto unblocking  
    */
    
    function blockSMSUser($inputArray)
    {
        $countryAbbreviation = COUNTRY_ABBREVIATION;
        $debug               = $inputArray['debug'];
        $clientEmail         = $inputArray['payer_email'];
        $comments            = $inputArray['user_limit_comments'];
        $connection          = $inputArray['connection'];
        
        $now = date("Y-m-d H:i:s");

        $blockQuery = "UPDATE SMSUsers ";
        $blockQuery.= "SET ";
        $blockQuery.= "airtimeStatus = 'BLOCKED', ";
        $blockQuery.= "subscriberStatus = 'BLOCKED', ";
        $blockQuery.= "comments = '$comments', ";
        $blockQuery.= "dateLastBlocked = '$now' ";
        $blockQuery.= "WHERE emailAddress ='$clientEmail'";
        mysql_query($blockQuery,$connection) or handleDatabaseError(''.mysql_error(),$blockQuery);
        if($debug) echo"blockQuery: $blockQuery <br>";
        if($debug) echo "result: ".mysql_info();
    }
?>
