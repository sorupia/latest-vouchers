<?
    function verify_receiver_limit($input_array)
    {
        $connection        = $input_array['connection'];
        $receiver_phone    = $input_array['receiver_phone'];

        $required_time_ago  = mktime(0, 0, 0, date("m") , date("d")-RX_LIMIT_TIMELINE , date("Y"));
		$required_timeline  = date("Y-m-d H:i:s",$required_time_ago);

        $query = "SELECT * ";
        $query.= "FROM UsedCards ";
        $query.= "WHERE receiverPhone = '$receiver_phone' ";
        $query.= "AND dateWeSoldIt > '$required_timeline' ";

        $result = mysql_query($query,$connection) or handleDatabaseError(''.mysql_error(),$query);
        $receiver_total = mysql_num_rows($result);

        if($receiver_total > RECEIVER_TX_LIMIT)
        {
            $input_array['receiver_limit_exceeded'] = 1;
            $input_array['error'].= "Unfortunately this receiver has exceeded the total transactions of ".RECEIVER_TX_LIMIT." ";
            $input_array['error'].= "received for the past ".RX_LIMIT_TIMELINE." day(s). Try again tomorrow.<br>";
        }
        else
        {
            $input_array['receiver_limit_exceeded'] = 0;
        }

        $input_array['receiver_total'] = $receiver_total;

        $input_array['debug_output'].="receiver_total: $receiver_total<br>\n";

        return $input_array;
    }
?>