<?
/*******************************************************************************
**  FILE: IPStatusHardCoded.php
**
**  FUNCTION: IPStatusHardCoded
**
**  PURPOSE: Check if provided IP is on our hard coded list
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.11.22
**
*********************************************************************************/

    $hard_coded_ip_white_list = array("41.202.233.184");

    function IPStatusHardCoded($input_array)
    {
        $ip_address          = $input_array['ip_address'];

        global $hard_coded_ip_white_list;
        $input_array['ip_hard_coded_unblocked'] = 0;

        //This hard coded white list was taken from the cust_table
        //from all clients that are unblocked
        if(array_search($ip_address, $hard_coded_ip_white_list) === FALSE)
        {
            $input_array['ip_hard_coded_unblocked'] = 0;
        }
        else
        {
            $input_array['ip_hard_coded_unblocked'] = 1;
        }

        return $input_array;
    }
?>