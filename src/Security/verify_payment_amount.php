<?
/*******************************************************************************
**   FILE: verify_payment_amount.php
**
**   FUNCTION: verify_payment_amount
**
**   PURPOSE: Function to compare the calculated or stored pay amount and the 
**   payment processor returned pay amount. This is used in the ipn_processor_code
**   as well as the return_page_auto_code.php
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 13.Jul.2012
*********************************************************************************/

    function verify_payment_amount($input_array)
    {

        if($input_array['mc_gross'] == $input_array['order_data']['sale_price_in_foreign'])
        {
            $input_array['payment_amount_verified'] = 1;
        }
        else
        {
            $input_array['payment_amount_verified'] = 0;
        }

        return $input_array;
    }
?>