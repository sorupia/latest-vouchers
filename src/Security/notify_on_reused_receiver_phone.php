<?
/*******************************************************************************
**  FILE: notify_on_reused_receiver_phone.php
**
**  FUNCTION: notify_on_reused_receiver_phone
**
**  PURPOSE: Notify the support staff about a reused receiver phone
**
**  WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2013.01.02
**
*********************************************************************************/

function notify_on_reused_receiver_phone ($input_array)
{
    $clientEmail    = $input_array['payer_email'];
    $receiver_phone = $input_array['order_data']['receiver_phone'];

    if($input_array['num_reused_receiver_phone'] > 0 ||
       $input_array['num_reused_receiver_phone_used_cards'] > 0 ||
       $input_array['num_reused_receiver_phone_awpdc'] > 0)
    {
        //Only report if there is something to report
        $subject = "RE-USED RECEIVER: $receiver_phone //";
        $subject.= "$clientEmail //".$input_array['txn_id'];

        $email_msg = $input_array['first_name']." ".$input_array['last_name']." $clientEmail ";
        $email_msg.= "has sent airtime to ".$receiver_phone.". This receiver has the following history:\n";

        $email_msg.= $input_array['reused_receiver_phone_info'];
        $email_msg.= $input_array['reused_receiver_phone_used_cards_info'];
        $email_msg.= $input_array['reused_receiver_phone_awpdc_info'];

        sendEmail(FROM_NAME,FROM_EMAIL,SUPPORT_EMAIL,$bcc,$subject,$email_msg);
    }

    return $input_array;
}

?>