<?
/*******************************************************************************
**   FILE: limitUserTransactions.php
**
**   FUNCTION: limitUserTransactions
**
**   PURPOSE: Function to check if the user has exceeded their limit
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2011.01.13 1656
**   created this file to separate the functions into single function files
**
**   MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala) DATE: 2011.01.15 0015
**   modified the input attributes to remove $countryAbbreviation 
**
**   MODIFIED BY: Arthur Ntozi (3nitylabs-AVMM, Kampala) DATE: 2015.12.01
**   Adding these top header comments
**
*********************************************************************************/

    /*
    201101131656 EAST: created this file to separate the functions into single function files
    201101150015 EAST: modified the input attributes to remove $countryAbbreviation  
    */

    function limitUserTransactions($inputArray)
    {
        //first call checkUserTransactionLimit before calling this function
        //if purchases exceed configured maximum then set customer status to blocked
        $limit_reached     = $inputArray['limit_reached'];
        $debug             = $inputArray['debug'];
        $clientEmail       = $inputArray['payer_email'];
        $firstname         = $inputArray['first_name'];
        $lastname          = $inputArray['last_name'];
        $clientPhone       = $inputArray['contact_phone'];
        $numberOfPurchases = $inputArray['numberOfPurchases'];
        $email_msg         = $inputArray['email_msg'];

        if ( $limit_reached == 1 )
        {
            if($debug) echo"User $clientEmail is over the daily tx limit <br>";

            blockSMSUser($inputArray);

            $_SESSION['ATSubscriberLoggedIn'] = false;
            $_SESSION['smsLoggedIn']          = false;
            $_SESSION['ATSubscriberStatus']   = 'MAX_TX_REACHED';            

            //send an email to support@ghanaairtime.com
            $bcc = SUPER_TECH_ADMIN;
            $subject = "MAX Tx REACHED - BLOCKED / $clientEmail / $firstname $lastname / $clientPhone / $numberOfPurchases";
            sendEmail(FROM_NAME,CARD_ADMIN,FROM_EMAIL,$bcc,$subject,$email_msg);
        }
        else
        {
            if($debug) echo"User $clientEmail is below the daily tx limit <br>";
            $subject = "MAX Tx NOT REACHED / $clientEmail / $firstname $lastname / $clientPhone / $numberOfPurchases";
            //sendEmail(FROM_NAME,CARD_ADMIN,"antozi@sendairtime.com",TECH_ADMIN,$subject,$email_msg);
        }
    }
?>