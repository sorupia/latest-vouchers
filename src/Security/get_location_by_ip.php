<?
    function get_location_by_ip($ip_address)
    {
        $buf = "";
        $license_key = MAX_MIND_LICENSE;
        $query = "http://geoip1.maxmind.com/b?l=" . $license_key . "&i=" . $ip_address;
        $url = parse_url($query);
        $host = $url["host"];
        $path = $url["path"] . "?" . $url["query"];
        $timeout = 1;
        $fp = fsockopen ($host, 80, $errno, $errstr, $timeout)
            or die('Can not open connection to server.');
        if ($fp) {
          fputs ($fp, "GET $path HTTP/1.0\nHost: " . $host . "\n\n");
          while (!feof($fp)) {
            $buf .= fgets($fp, 128);
          }
          $lines = split("\n", $buf);
          $data = $lines[count($lines)-1];
          fclose($fp);
        } else {
          # enter error handing code here
        }
        
        //echo "Your IP Based Geo Location is:   ";
        //echo "Your IP Address is: ".$ip_address."<br/>";
        //echo $data;
        
        $geo = explode(",",$data);
        $ipLocationArray['ip_country'] = $geo[0];
        $ipLocationArray['ip_state'] = $geo[1];
        $ipLocationArray['ip_city'] = $geo[2];
        $ipLocationArray['ip_latitude'] = $geo[3];
        $ipLocationArray['ip_longitude'] = $geo[4];
        $ipLocationArray['ip_address'] = $ip_address;
        
        return $ipLocationArray;
    }
?>