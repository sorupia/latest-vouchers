<?
    function email_support_success_transfer($input_array)
    {
        $client_first_name        = $input_array['first_name'];
        $client_last_name         = $input_array['last_name'];
        $client_email             = $input_array['payer_email'];
        $receiver_amount_in_local = $input_array['order_data']['receiver_amount_in_local'];
        $receiver_currency        = $input_array['order_data']['receiver_currency'];
        $receiver_phone           = $input_array['order_data']['receiver_phone'];
        $receiver_first_name      = $input_array['order_data']['receiver_first_name'];
        $receiver_last_name       = $input_array['order_data']['receiver_last_name'];

        $status_message           = $input_array['api_response']['TransactionStatus'];

        $address_street           = $input_array['address_street'];
        $address_city             = $input_array['address_city'];
        $address_state            = $input_array['address_state'];
        $address_country          = $input_array['address_country'];
        $address_zip              = $input_array['address_zip'];

        $ip_city                  = $input_array['order_data']['ip_city'];
        $ip_state                 = $input_array['order_data']['ip_state'];
        $ip_country               = $input_array['order_data']['ip_country'];
        $ip_address               = $input_array['order_data']['ip_address'];

        $payment_amount           = $input_array['mc_gross'];
        $client_currency          = $input_array['mc_currency'];
        $order_id                 = $input_array['invoice'];
        $transaction_id           = $input_array['txn_id'];
        $cost                     = $client_currency." ".$payment_amount;
        $receiver_amount          = $receiver_currency." ".$receiver_amount_in_local;
        
        $calculated_amount      = $input_array['amount_after_pay_gway'];
        $verified_pay_amount    = $client_currency." ".number_format($calculated_amount, FOREIGN_DECIMAL_PLACES);

        $message = "The transfer of $receiver_amount to $receiver_phone has been completed.\n\n";

        $message.= "The full details of the transaction are as follows: \n";
        $message.= "Client Name : $client_first_name $client_last_name\n";
        $message.= "Client Email : $client_email\n";
        $message.= "Address Street : $address_street\n";
        $message.= "Address City : $address_city\n";
        $message.= "IP City : $ip_city\n";
        $message.= "Address State : $address_state\n";
        $message.= "IP State : $ip_state\n";
        $message.= "Address Country : $address_country\n";
        $message.= "IP Country : $ip_country\n";
        $message.= "Address Zip : $address_zip\n";
        $message.= "IP Address : $ip_address\n\n";

        $message.= "Receiver : $receiver_first_name $receiver_last_name\n";
        $message.= "Receiver Phone : $receiver_phone\n";
        $message.= "Amount To Be Received : $receiver_amount\n";
        $message.= "Cost : $cost\n";
        $message.= "Verified Cost : $verified_pay_amount\n";
        $message.= "Invoice ID : $order_id\n";
        $message.= "Transaction ID : $transaction_id\n\n";

        $message.= PRODUCT." transfer message : $status_message\n\n";

        $headers = "From: ".SUPPORT_NAME." <".CARD_ADMIN.">\r\n";
        $subject = "New ".PRODUCT." transfer from $client_first_name $client_last_name Complete!";
        $to      = SUPPORT_EMAIL;
        mail($to,$subject,$message,$headers);
    }
?>