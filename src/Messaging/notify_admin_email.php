<?
    function notify_admin_email($input_array)
    {
        global $notifyEmails;

        $from    = "From: ".SUPPORT_NAME." ";
        $from   .= "<".SUPPORT_EMAIL.">";

        $subject = $input_array['subject'];
        $msg     = $input_array['msg'];

        for($i = 0 ; $i < count($notifyEmails) ; $i++)
        {
            $to = $notifyEmails[$i];
            mail($to,$subject,$msg,$from);
        }
    }
?>