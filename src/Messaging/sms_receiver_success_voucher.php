<?
/*******************************************************************************
**   FILE: sms_receiver_success_voucher.php
**
**   FUNCTION: sms_receiver_success_voucher
**
**   PURPOSE: SMS recipients vouchers that have just been purchased for them
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, 113-3353 16A Ave NW, Edmonton, AB)   DATE: 09.Sep.2015
**
**
*********************************************************************************/

    function sms_receiver_success_voucher($input_array)
    {
        $client_first_name        = $input_array['first_name'];
        $client_last_name         = $input_array['last_name'];
        $client_email             = $input_array['payer_email'];
        $receiver_amount_in_local = $input_array['order_data']['receiver_amount_in_local'];
        $receiver_currency        = $input_array['order_data']['receiver_currency'];
        $receiver_phone           = $input_array['order_data']['receiver_phone'];
        $receiver_first_name      = $input_array['order_data']['receiver_first_name'];
        $receiver_last_name       = $input_array['order_data']['receiver_last_name'];
        $payment_amount           = $input_array['mc_gross'];
        $client_currency          = $input_array['mc_currency'];
        $order_id                 = $input_array['invoice'];
        $transaction_id           = $input_array['txn_id'];
        $voucher_id               = $input_array['cardPIN'];
        $network_name             = getNetworkName($input_array['item_number']);
        $cost                     = $client_currency." ".$payment_amount;
        $receiver_amount          = $receiver_currency." ".number_format($receiver_amount_in_local, LOCAL_DECIMAL_PLACES);

        //54 chars before variables
        $message = "Hi $receiver_first_name $receiver_last_name, ";
        $message.= "$client_first_name $client_last_name has sent you a ";
        $message.= $network_name." ".PRODUCT." worth $receiver_amount. ";
        $message.= "One of our agents will call you shortly to deliver it. ";
        $message.= "Reference ID is $voucher_id. ";
        $message.= "Sent from ".DOMAIN_NAME;

        $input_array["text_message"] = $message;
        $input_array["receiver_phone"] = $receiver_phone;
        $input_array = send_txt_using_click($input_array);

        return $input_array;
    }
?>