<?
    function email_client_failed_transfer($input_array)
    {
        $client_first_name        = $input_array['first_name'];
        $client_last_name         = $input_array['last_name'];
        $client_email             = $input_array['payer_email'];
        $receiver_amount_in_local = $input_array['order_data']['receiver_amount_in_local'];
        $receiver_currency        = $input_array['order_data']['receiver_currency'];
        $receiver_phone           = $input_array['order_data']['receiver_phone'];
        $receiver_first_name      = $input_array['order_data']['receiver_first_name'];
        $receiver_last_name       = $input_array['order_data']['receiver_last_name'];
        $payment_amount           = $input_array['mc_gross'];
        $client_currency          = $input_array['mc_currency'];
        $order_id                 = $input_array['invoice'];
        $transaction_id           = $input_array['txn_id'];
        $cost                     = $client_currency." ".$payment_amount;
        $receiver_amount          = $receiver_currency." ".number_format($receiver_amount_in_local, LOCAL_DECIMAL_PLACES);

        if(LOCAL_SUPPORT_PHONE != "")
        {
            $localSupportPhoneRow = "Your friend in ".COUNTRY_NAME." may also contact us at ";
            $localSupportPhoneRow.= LOCAL_SUPPORT_PHONE." (".COUNTRY_NAME." telephone number)\n\n";
        }

        $message = "Dear $client_first_name,\n";
        $message.= "Thank you for your ".PRODUCT." purchase. ";
        $message.= "Unfortunately the transfer of $receiver_amount to $receiver_phone has delayed temporarily. ";
        $message.= "Our staff have been informed and are working to resolve the issue. ";
        $message.= "You will receive a confirmation email as soon as the issue has been resolved.\n\n";

        $message.= "ORDER DETAILS ---------------------------------------\n";
        $message.= "The full details of your transaction are as follows: \n";
        $message.= "Receiver's Name : $receiver_first_name $receiver_last_name\n";
        $message.= "Receiver's Phone : $receiver_phone\n";
        $message.= "Client Email : $client_email\n";
        $message.= PRODUCT." Amount Not Yet Received : $receiver_amount\n";
        $message.= "Cost : $cost\n";
        $message.= "Invoice ID : $order_id\n";
        $message.= "Transaction ID : $transaction_id\n\n";
        $message.= "-----------------------------------------------------\n";

        $message.= "Please contact us in case of any problems at ".SUPPORT_EMAIL." or at ";
        $message.= FOREIGN_SUPPORT_PHONE." (".FOREIGN_SUPPORT_PHONE_COUNTRY." telephone number)\n";
        $message.= $localSupportPhoneRow;
        $message.= "Use the Invoice ID $order_id when contacting customer service.\n\n";

        $message.= "Thank you for your patience.\n";
        $message.= SERVICED_BY."\n";
        $message.= SERVICE_PROVIDER_ADDRESS."\n";
        $message.= SUPPORT_PHONE."\n";

        $headers = "From: ".SUPPORT_NAME." <".SUPPORT_EMAIL.">\r\n";
        $headers.= "Bcc: ".CARD_ADMIN."\r\n";
        $subject = PRODUCT." transfer to $receiver_phone Delayed!";
        $to      = "$client_first_name $client_last_name <$client_email>";
        mail($to,$subject,$message,$headers);
    }
?>