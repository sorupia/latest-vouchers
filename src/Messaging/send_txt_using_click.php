<?
    function send_txt_using_click($input_array)
    {
        // Turn off all error reporting
        error_reporting(0);
        $phone       = $input_array["receiver_phone"];
        $textMessage = $input_array["text_message"];
        $user        = CLICKATELL_USER;
        $password    = CLICKATELL_PASSWD;
        $baseurl     = CLICKATELL_URL;
        $api_id      = CLICKATELL_API_ID;
        $text        = urlencode("$textMessage");
        $from        = urlencode(CLICKATELL_DISPLAY_AT);

        $confirmationMsg = "";
        $clickMessageID  = "";

        $to = $phone;

        //auth call
        $url = "$baseurl/http/auth?user=$user&password=$password&api_id=$api_id";

        //do auth call
        $ret = file($url);

        //split our response. return string is on first line of the data returned
        $sess = explode(":",$ret[0]);

        if ($sess[0] == "OK")
        {
            $sess_id = trim($sess[1]); // remove any whitespace
            $url = "$baseurl/http/sendmsg?session_id=$sess_id&to=$to&text=$text&from=$from&concat=3";
            
            //do sendmsg call
            $ret = file($url);
            $send = explode(":",$ret[0]);
            
            if ($send[0] == "ID")
            {
                $clickMessageID="".$send[1];
                $confirmationMsg.="SMS sent to $to ";
            }
            else
            {
                $confirmationMsg.="SMS Unavailable try again l8r<br>";

                $message = "Error sending message to $to \n";
                $message.= "SMS returned error: ". $ret[0]."\n";
                $message.= "Text message: ".$textMessage;
                $headers = "From: ".SUPPORT_NAME." <".SUPPORT_EMAIL.">\r\n";
                $subject = "SMS error";
                $to      = SUPPORT_EMAIL;
                mail($to,$subject,$message,$headers);
            }
        }
        else
        {
            $confirmationMsg.="Authentication failure: ". $ret[0];
            $subject = "SMS error";
            $message = "SMS returned error: $confirmationMsg";
            $to      = SUPPORT_EMAIL;
            mail($to,$subject,$message,$headers);
            $clickMessageID = "NO_SMS";
        }

        $input_array['sms_gw_id'] = $clickMessageID;
        return $input_array;
    }
?>