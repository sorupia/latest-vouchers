<?
/*******************************************************************************
**   FILE: email_receiver_success_voucher.php
**
**   FUNCTION: email_receiver_success_voucher
**
**   PURPOSE: Email recipient the voucher that have just been purchased for them
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, 113-3353 16A Ave NW, Edmonton, AB) DATE: 10.Sep.2015
**
**
*********************************************************************************/

    function email_receiver_success_voucher($input_array)
    {
        $client_first_name        = $input_array['first_name'];
        $client_last_name         = $input_array['last_name'];
        $receiver_amount_in_local = $input_array['order_data']['receiver_amount_in_local'];
        $receiver_currency        = $input_array['order_data']['receiver_currency'];
        $receiver_phone           = $input_array['order_data']['receiver_phone'];
        $receiver_first_name      = $input_array['order_data']['receiver_first_name'];
        $receiver_last_name       = $input_array['order_data']['receiver_last_name'];
        $receiver_email           = $input_array['order_data']['receiver_email'];
        $payment_amount           = $input_array['mc_gross'];
        $client_currency          = $input_array['mc_currency'];
        $order_id                 = $input_array['invoice'];
        $transaction_id           = $input_array['txn_id'];
        $network_name             = getNetworkName($input_array['item_number']);
        $cost                     = $client_currency." ".$payment_amount;
        $receiver_amount          = $receiver_currency." ".number_format($receiver_amount_in_local, LOCAL_DECIMAL_PLACES);

        if(LOCAL_SUPPORT_PHONE != "")
        {
            $localSupportPhoneRow = "Your friend in ".COUNTRY_NAME." may also contact us at ";
            $localSupportPhoneRow.= LOCAL_SUPPORT_PHONE." (".COUNTRY_NAME." telephone number)\n\n";
        }

        $message = "Dear $receiver_first_name,\n";
        $message.= "You have received ".PRODUCT."! \n";
        $message.= "$client_first_name $client_last_name has bought you a ";
        $message.= $network_name." ".PRODUCT." worth ".$receiver_amount.".\n\n";

        $message.= "ORDER DETAILS -------------------------------------\n";
        $message.= "The full details of your transaction are as follows: \n";
        $message.= "Receiver's Name : $receiver_first_name $receiver_last_name\n";
        $message.= "Receiver's Phone : $receiver_phone\n";
        $message.= $network_name." ".PRODUCT." Amount Received : $receiver_amount\n";
        $message.= "Invoice ID : $order_id\n";
        $message.= "Transaction ID : $transaction_id\n";
        $message.= "$network_name Voucher ID : $voucher_id\n";
        $message.= "------------------------------------------------------\n\n";

        $message.= "Instructions:\n";
        $message.= "You should have the Voucher ID ready as you will ";
        $message.= "be contacted by one of our agents. A physical delivery will be made to you ";
        $message.= "at a convenient location.\n\n";

        $message.= "Please contact us in case of any problems at ".SUPPORT_EMAIL.". ";
        $message.= "Use the Invoice ID $order_id when contacting customer service.\n\n";

        $message.= "Thank you for using ".DOMAIN_PREFIX.".\n";
        $message.= SERVICE_PROVIDER."\n";
        $message.= SERVICE_PROVIDER_ADDRESS."\n";
        $message.= SUPPORT_PHONE."\n";

        $headers = "From: ".SUPPORT_NAME." <".SUPPORT_EMAIL.">\r\n";
        $headers.= "Bcc: ".CARD_ADMIN."\r\n";
        $subject = $order_id." : ".PRODUCT." transfer to $receiver_phone Complete!";
        $to      = "$receiver_first_name $receiver_last_name <$receiver_email>";
        mail($to,$subject,$message,$headers);
    }
?>