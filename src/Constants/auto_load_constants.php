<?
    //Networks with the auto load option
    $auto_load_network_array = array(
                                "ug"=>array(
                                   "ugmtn"=>array("networkName"=>"MTN Uganda","load_limit"=>100000,"prefixes"=>array("+25677",
                                                                                                                     "+25678",
                                                                                                                     "+25631",
                                                                                                                     "+25632",
                                                                                                                     "+25633",
                                                                                                                     "+25634",
                                                                                                                     "+25635",
                                                                                                                     "+25636",
                                                                                                                     "+25637",
                                                                                                                     "+25638",
                                                                                                                     "+25639")),
                                   "ugafr"=>array("networkName"=>"AFRICELL/ORANGE Uganda","load_limit"=>100000,"prefixes"=>array("+25679")),
                                   "ugair"=>array("networkName"=>"AIRTEL Uganda","load_limit"=>100000,"prefixes"=>array("+25675","+25670")),
                                   "ugsma"=>array("networkName"=>"SMART Uganda","load_limit"=>100000,"prefixes"=>array("+25674")),
                                   "ugutl"=>array("networkName"=>"UTL Uganda","load_limit"=>100000,"prefixes"=>array("+25671")),
                                   )
                               );
?>