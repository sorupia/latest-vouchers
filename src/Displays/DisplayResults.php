<?
	function displayTableResults($queryResult)
	{
		// Print the Client data in a HTML table 
		echo "<table class=\"large-tbl\" width=\"90%\"  border=\"0\" >";
		echo"<tr>";
		
		$numOfColumns=mysql_num_fields($queryResult);
		
		for($i=0 ; $i<$numOfColumns ; $i++){
			$columnName=mysql_field_name($queryResult,$i);	
			echo"<td>$columnName</td>";
		}
		
		echo"</tr>";
		$j=0;
		while ($queryData = mysql_fetch_array($queryResult, MYSQL_ASSOC)) {
			$j++;
			if($j%2){
				print "<tr>";
			}
			else{
				print "<tr bgcolor=\"#F0F0F0\">";
			}
			foreach ($queryData as $col_value) {
				print "<td>$col_value</td>";
			}
			print "</tr>";
		}
		print "</table>";		
	}	
    
    function displayTableNew($queryResult)
	{
		// Print the Client data in a HTML table 
		echo "<thead> ";
		echo"<tr> ";
		
		$numOfColumns=mysql_num_fields($queryResult);
		
		for($i=0 ; $i<$numOfColumns ; $i++){
			$columnName=mysql_field_name($queryResult,$i);	
			echo"<th>$columnName</th> ";
		}
		
		echo"</tr> ";
		echo"</thead> ";
		
		while ($queryData = mysql_fetch_array($queryResult, MYSQL_ASSOC)) {
			echo"<tbody><tr> ";
			foreach ($queryData as $col_value) {
				print "<td>$col_value</td> ";
			}
			print "</tr></tbody> ";
		}
	}

		
	/* process the db results into string */
	function convertResult2String($queryResult)
	{
		$title_string = "";
		$content_string = "";
		
		$numOfColumns=mysql_num_fields($queryResult);
	
		for($i=0 ; $i<$numOfColumns ; $i++){
			$columnName=mysql_field_name($queryResult,$i);	
			if($i!=0){ $title_string.=','; }
			$title_string.=$columnName;
		}
		$title_string.="\n";
		
		$is_first_column=true;
		while ($queryData = mysql_fetch_array($queryResult, MYSQL_ASSOC)) {
			foreach ($queryData as $col_value) {
				
				if(!$is_first_column){ $content_string.=','; }
				
				$content_string.=$col_value;
				$is_first_column=false;
			}
			$content_string.="\n";
			$is_first_column=true;
		}

		return $title_string.$content_string;
	}
	
	/* process the db results into xls file string */
	function convertResult2XLS($queryResult)
	{	
		$file_text.= "<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" \n";
		$file_text.= "xmlns:x=\"urn:schemas-microsoft-com:office:excel\" \n";
		$file_text.= "xmlns=\"http://www.w3.org/TR/REC-html40\"> \n";
		$file_text.= "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"> \n";
		$file_text.= "<html> \n";
		$file_text.= "<head> \n";
		$file_text.= "<meta http-equiv=\"Content-type\" content=\"text/html;charset=utf-8\" /> \n";
		$file_text.= "<style id=\"Classeur1_16681_Styles\"> \n";
		$file_text.= "</style> \n";
		$file_text.= "</head> \n";
		$file_text.= "<body> \n";
		$file_text.= "<div id=\"Classeur1_16681\" align=center x:publishsource=\"Excel\"> \n";
		$file_text.= "<table x:str border=0 cellpadding=0 cellspacing=0 width=100% style=\"border-collapse: collapse\"> \n";
		
		/* process the db results into string */
		$file_text.= "<tr>";
		$numOfColumns=mysql_num_fields($queryResult);
		for($i=0 ; $i<$numOfColumns ; $i++){
			$columnName=mysql_field_name($queryResult,$i);	
			$file_text.="<td class=xl2216681 nowrap><b>".$columnName."</b></td>";
		}
		$file_text.= "</tr>";
		$file_text.= "\n";
		
		while ($queryData = mysql_fetch_array($queryResult, MYSQL_ASSOC)) 
		{
			$file_text.= "<tr>";
			foreach ($queryData as $col_value) {
				$file_text.="<td class=xl2216681 nowrap>".$col_value."</td>";
			}
			$file_text.= "</tr>";
			$file_text.= "\n";
		}	
	
		$file_text.= "</table> \n";
		$file_text.= "</div> \n";
		$file_text.= "</body> \n";
		$file_text.= "</html> \n";
		
		return $file_text;
	}
?>
