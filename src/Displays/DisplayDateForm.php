<?
function print_year_form()
{
    $year = date("Y");

    for($i = 2006 ; $i < 2015 ; $i ++ )
    {
        if ($i == $year )
        {
            echo"<option value=\"$i\" selected=\"selected\">$i</option>";
        }
        else
        {
            echo"<option value=\"$i\">$i</option>";
        }
    }
}

function print_month_form()
{
    $month_short_text = date("M");
    $month_number  = date("n");
    $month_num_array = array("00","01","02","03","04","05","06","07","08","09","10","11","12");
    $month_txt_array = array("","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

    for($i = 1 ; $i <= 12 ; $i ++ )
    {
        $month_num_txt = $month_num_array[$i];
        $month_str_txt = $month_txt_array[$i];

        if ($i == $month_number )
        {
            echo"<option value=\"$month_num_txt\" selected=\"selected\">$month_str_txt</option>";
        }
        else
        {
            echo"<option value=\"$month_num_txt\">$month_str_txt</option>";
        }
    }
}

function print_day_form()
{
    $day = date("d");
    
    for($i = 1 ; $i <= 31 ; $i ++ )
    {
        $day_num_txt = $i<10 ? "0".$i : $i;

        if ($i == $day )
        {
            echo"<option value=\"$day_num_txt\" selected=\"selected\">$day_num_txt</option>";
        }
        else
        {
            echo"<option value=\"$day_num_txt\">$day_num_txt</option>";
        }
    }
}

function print_select_year_form($year)
{

    for($i = 2006 ; $i < 2015 ; $i ++ )
    {
        if ($i == $year )
        {
            echo"<option value=\"$i\" selected=\"selected\">$i</option>";
        }
        else
        {
            echo"<option value=\"$i\">$i</option>";
        }
    }
}

function print_select_month_form($month_number)
{
    $month_num_array = array("00","01","02","03","04","05","06","07","08","09","10","11","12");
    $month_txt_array = array("","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

    for($i = 1 ; $i <= 12 ; $i ++ )
    {
        $month_num_txt = $month_num_array[$i];
        $month_str_txt = $month_txt_array[$i];

        if ($i == $month_number )
        {
            echo"<option value=\"$month_num_txt\" selected=\"selected\">$month_str_txt</option>";
        }
        else
        {
            echo"<option value=\"$month_num_txt\">$month_str_txt</option>";
        }
    }
}

function print_select_day_form($day)
{
    
    for($i = 1 ; $i <= 31 ; $i ++ )
    {
        $day_num_txt = $i<10 ? "0".$i : $i;

        if ($i == $day )
        {
            echo"<option value=\"$day_num_txt\" selected=\"selected\">$day_num_txt</option>";
        }
        else
        {
            echo"<option value=\"$day_num_txt\">$day_num_txt</option>";
        }
    }
}
?>
