<?
function displayTableFromArray($array_to_display)
{
    echo "<thead> ";
    echo "<tr> ";

    $numOfColumns = count($array_to_display[0]);
    $columnNameArray = array_keys($array_to_display[0]);

    for($i=0 ; $i<$numOfColumns ; $i++)
    {
        $columnName = $columnNameArray[$i]; 
        echo"<th>$columnName</th> ";
    }

    echo "</tr> ";
    echo "</thead> ";

    $numOfRows = count($array_to_display);
    for ($j = 0 ; $j < $numOfRows ; $j++)
    {
        echo"<tbody><tr> ";
        for($k=0 ; $k < $numOfColumns ; $k++)
        {
            $column_name = $columnNameArray[$k];
            $col_value = $array_to_display[$j]["$column_name"];
            print "<td>$col_value</td> ";
        }
        print "</tr></tbody> ";
    }
}
?>