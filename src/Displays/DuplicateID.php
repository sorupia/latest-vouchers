<div id="body">
    <div class="content">
        <div id="section">
            <h3>Thank you <?=$transactionArray['first_name']?> for your purchase</h3>
            <br/>
It appears that we have already received this transaction. Kindly check your email at <?=$transactionArray['payer_email']?> for the details.
In case of any issues email us at <?=SUPPORT_EMAIL?> or call <?=SUPPORT_PHONE?>.<br>
    <?php echo $transactionArray['comments'];  ?>
        </div>
        <div id="sidebar">
            <div class="contact">
                <h3><?=$transactionArray['networkName']?></h3>
                <img src="images/logos/<?=$transactionArray['networkCode']?>.png" class="receiver_auto_logo">
            </div>
            <div id="voucher_div">
                <div style="width: 100%; height: auto; float: left; margin-left: 20px; margin-top:10px; font-weight: bold; font-size: 16px;">  
                    Recipient will be hand delivered a voucher like this.
                </div>
                <img id="vouchers" src="images/vouchers/<?=$transactionArray['networkCode']?>.png">
            </div>
            <div style="width: 90%; height: auto; font-size: 15px; float: left; margin-left:20px; margin-top:10px; margin-bottom:5px; text-align: center; font-weight: bold;">
                Pay By PayPal (Bank or Credit Card).
            </div>
            <div style="width: 100%; float: left; margin-top: -10px;">
                <img src="images/logos/paypal_big.png" style="width: 95%;">
            </div>
        </div>
    </div>
</div>