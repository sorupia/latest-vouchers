<div class="receiptDetails">
    <div class="spacing">
        <div class="floatLeft">
            <ul>
                <li><h2>Thank you <?=$transactionArray['first_name']?> for your purchase</h2></li>
                <li><b>There was a temporary error with your PayPal payment</b></li>
                <li>Please email us at <?=SUPPORT_EMAIL?>.</li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>

<?php
    // This file caters for the else alternative to displaying airtime
    $clientIP=getIP();
    $myHostName=gethostbyaddr($clientIP);
    $timeOfAccess = date("Y-m-d H:i:s");

    //25.Oct.2007: Send admin an email to find out what is causing this
	$body="Hi Admin, a user tried to access the PayPalReturnPage and had the following problem.\n";
	$body.="PayPal returned:\n ";

    for($i=0;$i<count($lines);$i++){
        $body.="$i: $lines[$i] \n";
    }

    $body.="Time of access is: $timeOfAccess \n";
    sendEmail('PayPal Return Page',$fromAddress,TECH_ADMIN,'',"PPAuthenticateElse",$body);
    sendEmail('PayPal Return Page',$fromAddress,SUPER_TECH_ADMIN,'',"PPAuthenticateElse",$body);
?>