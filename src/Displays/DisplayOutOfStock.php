<?
    $networkCode   = substr($itemIDNumber,0,5);
    $valueInUGSh   = $input_array['valueInLocal'];
    $localCurrency = LOCAL_CURRENCY;
    $networkName   = $input_array['networkName'];
?>
<div id="body">
    <div class="content">
        <div id="section">
            <h3>Thank you <?=$input_array['first_name']?>, the details of your purchase are listed below:</h3>
            <br/>
            <table width="100%">
                <tr>
                    <td width="200px"><b>Airtime Access Number: </b></td><td>---DELAYED---</td>
                </tr>
                <tr>
                    <td width="200px"><b>Card Type: </b></td><td><? echo LOCAL_CURRENCY." ".number_format($valueInUGSh, LOCAL_DECIMAL_PLACES)." $networkName ".PRODUCT;?></td>
                </tr>
                <tr>
                    <td width="200px">Receiver's Name: </td><td><?=$input_array['order_data']['receiver_first_name']?> <?=$input_array['order_data']['receiver_last_name']?></td>
                </tr>
                <tr>
                    <td>Receiver's Phone: </td><td><?=$input_array['order_data']['receiver_phone']?></td>
                </tr>
                <tr>
                    <td>Cost: </td><td><?=LOCAL_CURRENCY." ".number_format($input_array['mc_gross'],FOREIGN_DECIMAL_PLACES)?></td>
                </tr>
                <tr>
                    <td><b>Date & Time:</b></td><td><? echo "$timeCardSent";?> (<?=TIME_ZONE?> Time)</td>
                </tr>
                <tr>
                    <td>Transaction ID: </td><td><?=$input_array['txn_id']?></td>
                </tr>
                <tr>
                    <td>Order ID: </td><td><?=$input_array['invoice']?></td>
                </tr>
                <tr>
                    <td valign="top">Receiver Instructions:</td>
                    <td>Airtime will be automatically loaded onto the receiver's phone.
In case of any issues please email us at <?=SUPPORT_EMAIL?> or call <?=SUPPORT_PHONE?>.</td>
                </tr>
            </table>
        </div>
        <div id="sidebar">
            <div class="contact">
                <h3><?=$input_array['networkName']?></h3>
                <img src="images/logos/<?=$input_array['networkCode']?>.png" class="receiver_auto_logo">
            </div>
            <div id="voucher_div">
                <div style="width: 100%; height: auto; float: left; margin-left: 20px; margin-top:10px; font-weight: bold; font-size: 16px;">  
                    Recipient will be hand delivered a voucher like this.
                </div>
                <img id="vouchers" src="images/vouchers/<?=$input_array['networkCode']?>.png">
            </div>
            <div style="width: 90%; height: auto; font-size: 15px; float: left; margin-left:20px; margin-top:10px; margin-bottom:5px; text-align: center; font-weight: bold;">
                Pay By PayPal (Bank or Credit Card).
            </div>
            <div style="width: 100%; float: left; margin-top: -10px;">
                <img src="images/logos/paypal_big.png" style="width: 95%;">
            </div>
        </div>
    </div>
</div>
