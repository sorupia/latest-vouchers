<div class="receiptDetails">
    <div class="spacing">
        <div class="floatLeft">
            <div class="carrier">
                <? echo "<img src='images/telecoms/".$networkCode.".gif' alt='".$networkCode."' />"; ?>
            </div>
        </div>        
        <div class="floatLeft">
            <ul>
                <li><h2>Thank you <?=$transactionArray['first_name']?>, the details of your purchase are listed below:</h2></li>
                <li><b>Airtime Access No.:</b> <?=$transactionArray['cardPIN']?></li>
                <li><b>Card Type:</b> <? echo "$valueInUGSh $LocalCurrency $networkName";?></li>
                <li><b>Receiver's First Name:</b> <?=$transactionArray['order_data']['receiver_first_name']?></li>                
                <li><b>Receiver's Phone:</b> <?=$transactionArray['order_data']['receiver_phone']?></li>
                <li><b>Date & Time:</b> <? echo "$timeCardSent";?> (<?=TIME_ZONE?> Time)</li>
                <li><b>Receiver Instructions:</b> To load dial <? echo $loadCode.$cardPIN."#";?></li>

                <? if($transactionArray['payer_email'] != DEFAULT_CLIENT_EMAIL){ ?>
                <li>The access number has also been sent to <?=$transactionArray['payer_email']?></li>
                <? } ?>
                <li>In case of any issues please email us at <?=SUPPORT_EMAIL?> or call <?=FOREIGN_SUPPORT_PHONE?>.</li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>