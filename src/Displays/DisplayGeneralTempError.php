<div class="receiptDetails">
    <div class="spacing">
        <div class="floatLeft">
            <ul>
                <li><h2>Thank you <?=$transactionArray['first_name']?> for your purchase</h2></li>
                <li><b>There was a temporary error with your payment</b></li>
                <li>Please email us at <?=SUPPORT_EMAIL?>.</li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>