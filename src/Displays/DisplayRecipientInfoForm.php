<?
	/*
	 * Function to display the recipient info required to complete airtime transaction
	 * this function prints out a form for a user to fill incase they experience
	 * the infamous paypal glitch in which if a user pays by credit card and then
	 * signs up for a new account, they loose the auth. details.
	 * log for manual investigation	 
	 *  Created from PPNewGlitchForm.php
	 *
	 *Info required: recipient name, recipient phone, confirm phone, recipient email 
	 */
	function displayRecipientInfoForm($action,$formName,$keyarray,$ipLocationArray)
	{

		//$action = "PINSender.php";
		
		$payment_status 	= $keyarray['payment_status'];
		$our_paypal_email 	= $keyarray['receiver_email'];//our email where the payment goes
		$transID 			= $keyarray['txn_id'];
		$payment_amount 	= $keyarray['mc_gross'];//payment_amount does not seem to work
		$payment_currency 	= $keyarray['mc_currency'];
		
		$first_name 		= $keyarray['first_name'];
		$last_name 			= $keyarray['last_name'];
		$item_name 			= $keyarray['item_name'];
		$client_email 		= $keyarray['payer_email'];
		echo "client_email: $client_email";
		$residence_country 	= $keyarray['residence_country'];
		$client_phone 		= $keyarray['contact_phone'];
		$itemIDNumber 		= $keyarray['item_number'];
		$client_id			= $keyarray['payer_id'];
		$address_street		= $keyarray['address_street'];
		$address_city		= $keyarray['address_city'];
		$address_state		= $keyarray['address_state'];
		$address_country	= $keyarray['address_country'];
		$address_zip		= $keyarray['address_zip'];
		
		$ip_country = $ipLocationArray['ip_country'];
		$ip_state 	= $ipLocationArray['ip_state'];
		$ip_city 	= $ipLocationArray['ip_city'];
		$ip_latitude= $ipLocationArray['ip_latitude'];
		$ip_longitude = $ipLocationArray['ip_longitude'];
		
		
		echo"<h3>Your purchase will require further approval. This is normal for first time purchases. ";
		echo"Please fill the form below so we can manually send your airtime. </h3>";
		echo"<ul class=\"details\">";
		echo"<li>Airtime Code(PIN) : to be sent within 2 hours if approved.</li> ";
		echo"</ul>";
		echo"<h3>Please fill in the information below to complete your order.</h3>";
		
		echo"<form name=\"$formName\" method=\"post\" action=\"$action\" 
		onSubmit=\"return checkPPRPFailForm(payPalEmail, phoneNumberField, phoneNumberField2, receiverEmail)\">
		  <table border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" class=\"details-tbl\">
			<tr>
			  <td bgcolor=\"#F0F0F0\" colspan=\"2\" align=\"center\">Enter Details </td>
			</tr>";
		if(strlen($client_email) == 0){
		echo"<tr>
			  <td>Your PayPal Email </td>
			  <td><input name=\"payPalEmail\" type=\"text\" id=\"payPalEmail\"></td>
			</tr>";
		}
		echo"<tr>
			  <td bgcolor=\"#F0F0F0\">Receiver's First Name</td>
			  <td bgcolor=\"#F0F0F0\"><input name=\"receiverFirstName\" type=\"text\" id=\"receiverFirstName\" size=\"9\" maxlength=\"9\">
			  Optional</td>
			</tr>
			<tr>
			  <td>Receiver's Mobile</td>
			  <td><input name=\"phoneNumberField\" type=\"text\" id=\"phoneNumberField\" value=\"+2567\" size=\"13\" maxlength=\"13\"></td>
			</tr>
			<tr>
			  <td bgcolor=\"#F0F0F0\">Re-enter Receiver's Mobile</td>
			  <td bgcolor=\"#F0F0F0\"><input name=\"phoneNumberField2\" type=\"text\" id=\"phoneNumberField2\" value=\"+2567\" size=\"13\" maxlength=\"13\"></td>
			</tr>
			<tr>
			  <td>Receiver's Email Address </td>
			  <td><input name=\"receiverEmail\" type=\"text\" id=\"receiverEmail\">
			  Optional</td>
			</tr>
			<tr>
			  <td bgcolor=\"#F0F0F0\">&nbsp;</td>
			  <td bgcolor=\"#F0F0F0\">
				<div align=\"right\">
				  <input name=\"$formName\" type=\"submit\" id=\"PPNACForm\" value=\"Submit\">
					<input name=\"payment_status\" type=\"hidden\" value=\"$payment_status\">
					<input name=\"our_paypal_email\" type=\"hidden\" value=\"$our_paypal_email\">
					<input name=\"transID\" type=\"hidden\" value=\"$transID\">
					<input name=\"payment_amount\" type=\"hidden\" value=\"$payment_amount\">
					<input name=\"payment_currency\" type=\"hidden\" value=\"$payment_currency\">
					<input name=\"first_name\" type=\"hidden\" value=\"$first_name\">
					<input name=\"last_name\" type=\"hidden\" value=\"$last_name\">
					<input name=\"item_name\" type=\"hidden\" value=\"$item_name\">
					<input name=\"client_email\" type=\"hidden\" value=\"$client_email\">
					<input name=\"residence_country\" type=\"hidden\" value=\"$residence_country\">
					<input name=\"client_phone\" type=\"hidden\" value=\"$client_phone\">
					<input name=\"itemIDNumber\" type=\"hidden\" value=\"$itemIDNumber\">
					<input name=\"client_id\" type=\"hidden\" value=\"$client_id\">
					<input name=\"address_street\" type=\"hidden\" value=\"$address_street\">
					<input name=\"address_city\" type=\"hidden\" value=\"$address_city\">
					<input name=\"address_state\" type=\"hidden\" value=\"$address_state\">
					<input name=\"address_country\" type=\"hidden\" value=\"$address_country\">
					<input name=\"address_zip\" type=\"hidden\" value=\"$address_zip\">
					<input name=\"ip_country\" type=\"hidden\" value=\"$ip_country\">
					<input name=\"ip_state\" type=\"hidden\" value=\"$ip_state\">
					<input name=\"ip_city\" type=\"hidden\" value=\"$ip_city\">
					<input name=\"ip_latitude\" type=\"hidden\" value=\"$ip_latitude\">
					<input name=\"ip_longitude\" type=\"hidden\" value=\"$ip_longitude\">  
				</div></td></tr>
		  </table>
		</form>";	
	}
?>	