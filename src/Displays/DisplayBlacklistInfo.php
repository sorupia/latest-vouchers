<div class="receiptDetails">
    <div class="spacing">
        <div class="floatLeft">
            <div class="carrier">
                <? echo "<img src='images/telecoms/".$networkCode.".gif' alt='".$networkCode."' />"; ?>
            </div>
        </div>        
        <div class="floatLeft">
            <ul>
                <li><h2>Thank you <?=$transactionArray['first_name']?> for your purchase.</h2></li>
                <li>This transaction will require manual processing. We will inform you at <?=$transactionArray['payer_email']?> once complete.</li>
                <li><b>Card Type:</b> <? echo "$valueInUGSh $LocalCurrency $networkName";?></li>
                <li><b>Receiver First Name:</b> <?=$transactionArray['order_data']['receiver_first_name']?></li>                
                <li><b>Receiver Phone:</b> <?=$transactionArray['order_data']['receiver_phone']?></li>
                <li><b>Date & Time:</b> <? echo "$timeCardSent";?> (<?=TIME_ZONE?>)</li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>