<div class="receiptDetails">
    <div class="spacing">        
        <div class="floatLeft">
            <ul>
                <li><h2>Thank you <?=$transactionArray['first_name']?> for your purchase</h2></li>
                <li><b>There was a temporary error with your PayPal payment</b></li>
                <li>Please email us at <?=SUPPORT_EMAIL?>.</li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>

<?php
	// This file caters for 
	// the infamous paypal glitch in which if a user pays by credit card and then
	// signs up for a new account, they loose the auth. details.
	// log for manual investigation
	//TODO: Insert this into an include
	$clientIP=getIP();
	$myHostName=gethostbyaddr($clientIP);
	$timeOfAccess = date("Y-m-d H:i:s");

	//log ip address over here to Failed IPs.
	//connect
	$connection=connect2DB2($countryAbbreviation);
		$logQuery="INSERT INTO FailedIPs(dateOfAccess,ipAddress,notes) VALUES('$timeOfAccess','$clientIP','Failed Auth.')";
		$logResult=mysql_query($logQuery) or handleDatabaseError(''. mysql_error(),$logQuery);
	//disconnect
	disconnectDB($connection);
	
	//send admin an email
	//inform the admin of failed authentication at PPRP
	$body="Hi Admin, a user tried to access the PayPalReturnPage and did not have proper credentials.\n";
	$body.="Check the Orders Queue under Queues in the admin section and compare the Order ID with the PayPal payment email.\n";
	$body.="The client's details are:\n ";
	$body.="IP address is $clientIP \n";
	$body.="Host name is $myHostName \n";
	$body.="Time of access is: $timeOfAccess \n";
	
	$body.="PayPal returned:\n ";
	
	for($i=0;$i<count($lines);$i++){
		$body.="$i: $lines[$i] \n";
	}
	
	sendEmail('PayPal Return Page',$fromAddress,RECORDS_EMAIL,'',"strcmp($lines[0], 'FAIL') == 0",$body);
	sendEmail('PayPal Return Page',$fromAddress,SUPER_TECH_ADMIN,'',"PPNewGlitchForm",$body);
	sendEmail('PayPal Return Page',$fromAddress,SUPPORT_EMAIL,'',"PPNewGlitchForm",$body);
?>