<?
/*
** DisplayManualProcNewClient.php
*/
?>
<div id="body">
    <div class="content">
        <div id="section">
            <h3>Thank you <?=$transactionArray['first_name']?> for your first transaction with us. This transaction will require manual processing. The details of your purchase are below:</h3>
            <br/>
            <table width="100%">
                <tr>
                    <td width="200px">Receiver's Name: </td><td><?=$transactionArray['order_data']['receiver_first_name']?> <?=$transactionArray['order_data']['receiver_last_name']?></td>
                </tr>
                <tr>
                    <td>Receiver's Phone: </td><td><?=$transactionArray['order_data']['receiver_phone']?></td>
                </tr>
                <tr>
                    <td>Voucher Value: </td><td><?=LOCAL_CURRENCY." ".number_format($valueInUGSh, LOCAL_DECIMAL_PLACES)." $networkName ".PRODUCT?></td>
                </tr>
                <tr>
                    <td>Cost: </td><td><?=LOCAL_CURRENCY." ".number_format($transactionArray['mc_gross'],FOREIGN_DECIMAL_PLACES)?></td>
                </tr>
                <tr>
                    <td>Receiver's Region: </td><td><?=$transactionArray['order_data']['receiver_region']?></td>
                </tr>
                <tr>
                    <td>Transaction ID: </td><td><?=$transactionArray['txn_id']?></td>
                </tr>
                <tr>
                    <td>Order ID: </td><td><?=$transactionArray['invoice']?></td>
                </tr>
                <tr>
                    <td valign="top">Receiver Instructions:</td>
                    <td>The recipient will be called by one of our agents and the voucher delivered to him/her.
In case of any issues please email us at <?=SUPPORT_EMAIL?> or call <?=SUPPORT_PHONE?>.</td>
                </tr>
            </table>
        </div>
        <div id="sidebar">
            <div class="contact">
                <h3><?=$transactionArray['networkName']?></h3>
                <img src="images/logos/<?=$transactionArray['networkCode']?>.png" class="receiver_auto_logo">
            </div>
            <div id="voucher_div">
                <div style="width: 100%; height: auto; float: left; margin-left: 20px; margin-top:10px; font-weight: bold; font-size: 16px;">  
                    Recipient will be hand delivered a voucher like this.
                </div>
                <img id="vouchers" src="images/vouchers/<?=$transactionArray['networkCode']?>.png">
            </div>
            <div style="width: 90%; height: auto; font-size: 15px; float: left; margin-left:20px; margin-top:10px; margin-bottom:5px; text-align: center; font-weight: bold;">
                Pay By PayPal (Bank or Credit Card).
            </div>
            <div style="width: 100%; float: left; margin-top: -10px;">
                <img src="images/logos/paypal_big.png" style="width: 95%;">
            </div>
        </div>
    </div>
</div>
