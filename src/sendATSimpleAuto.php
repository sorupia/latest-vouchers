<?php

/*******************************************************************************
*   FILE: sendATSimpleAuto.php
*   PURPOSE: Provides an access function for internal scripts to sendairtime
*            It has been adapted from sendAT.php form handling script.
*   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 26.Jul.2012
*
*   MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 13.Oct.2014
*   Added code to include the Network Transaction Reference ID
*
*********************************************************************************/
    
    include_once $_SERVER['DOCUMENT_ROOT']."/src/SessionFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/ObjectFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/General/GeneralFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Messaging/MessagingFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Yo/yo_functions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/OrderFunctions.php";
    include_once $_SERVER['DOCUMENT_ROOT']."/src/Constants/Constants.php";

    function sendATSimpleAuto($countryAbbreviation, $transactionArray)
    {
        $employeeUsername    = $transactionArray['employeeUsername'];
        $employeePassword    = $transactionArray['employeePassword']; 
        $transID             = $transactionArray['transID']; 
        $clientEmail         = $transactionArray['clientEmail']; 
        $clientFirstName     = $transactionArray['clientFirstName']; 
        $receiverFirstName   = $transactionArray['receiverFirstName']; 
        $receiverPhone       = $transactionArray['receiverPhone']; 
        $receiverPhone2      = $transactionArray['phoneConfirm'];
        $receiverEmail       = $transactionArray['receiverEmail']; 
        $itemIDNumber        = $transactionArray['itemIDNumber'];
        $order_id            = $transactionArray['order_id'];

        $paymentFee          = 0;
        $valueInLocal        = getValueInLocal($itemIDNumber);
        $ourPriceInUSD       = 0;

        $transactionArray['txn_id']       = $transID;
        $transactionArray['item_number']  = $itemIDNumber;
        $transactionArray['payer_email']  = $clientEmail;
        $transactionArray['mc_fee']       = $paymentFee;
        $transactionArray['timeCardSent'] = date("Y-m-d H:i:s");
        $transactionArray['mc_gross']     = getPriceInUSD($itemIDNumber);
        $transactionArray['first_name']   = $clientFirstName;
        $transactionArray['sessionUser']  = $employeeUsername;
        $transactionArray['valueInLocal'] = $valueInLocal;
        $transactionArray['invoice']      = $order_id;

        $transactionArray['order_data']['receiver_phone']           = $receiverPhone;
        $transactionArray['order_data']['receiver_email']           = $receiverEmail;
        $transactionArray['order_data']['receiver_first_name']      = $receiverFirstName;
        $transactionArray['order_data']['receiver_network_id']      = substr($itemIDNumber,0,5);
        $transactionArray['order_data']['receiver_amount_in_local'] = $valueInLocal;
        $transactionArray['order_data']['receiver_currency']        = LOCAL_CURRENCY;
        $transactionArray['mc_currency']                            = SALE_CURRENCY;

        $transactionArray['receiver_phone']      = $receiverPhone;
        $transactionArray['receiver_email']      = $receiverEmail;
        $transactionArray['receiver_first_name'] = $receiverFirstName;
        $transactionArray['network_id']          = substr($itemIDNumber,0,5);

        $message = "";
        $transactionArray['message'] = "";
        $transactionArray['card'] = 0;
        $transactionArray['status'] = "FAILURE";
        $transactionArray['result'] = false;

        //If the client ip exists then use its data
        if(isset($transactionArray['clientIP']))
        {
            $clientIP                                     = $transactionArray['clientIP'];
            $transactionArray["clientIP"]                 = $clientIP;
            $transactionArray['order_data']['ip_city']    = $transactionArray['ipCity'];
            $transactionArray['order_data']['ip_state']   = $transactionArray['ipState'];
            $transactionArray['order_data']['ip_country'] = $transactionArray['ipCountry'];
        }
        else
        {
            //If not client IP exists then use IP of personal manually processing the airtime
            $ipLocationArray                              = getIPLocation();
            $clientIP                                     = $ipLocationArray['ip_address'];
            $transactionArray["clientIP"]                 = $clientIP;
            $transactionArray['order_data']['ip_city']    = $ipLocationArray['ip_city'];
            $transactionArray['order_data']['ip_state']   = $ipLocationArray['ip_state'];
            $transactionArray['order_data']['ip_country'] = $ipLocationArray['ip_country'];
        }

        //Assumption is that if receive_amount_in_local is not provided then itemID is
        if($transactionArray['receiver_amount_in_local'] == "")
        {
            $transactionArray['receiver_amount_in_local'] = $valueInLocal;
            $transactionArray['valueInLocal'] = $valueInLocal;
        }

        if(!isset($transactionArray['airtime_load_option']))
        {
            $transactionArray['airtime_load_option'] = AIRTIME_LOAD_OPTION_CARD;
        }

        //Some forms do not have the second phone number provided
        if(!isset($transactionArray['phoneConfirm']))
        {
            $transactionArray['phoneConfirm'] = $transactionArray['receiverPhone'];
            $receiverPhone2      = $transactionArray['phoneConfirm'];
        }
        
        //Validate receiver form and return receiver network id
        $transactionArray = validate_receiver_form_auto($transactionArray);
        $success = $transactionArray['success'];

        //If the itemID does not exist, create it
        if($itemIDNumber == "")
        {
            $transactionArray = create_item_id($transactionArray);
            $itemIDNumber                                               = $transactionArray['created_item_id'];
            $valueInLocal                                               = getValueInLocal($itemIDNumber);
            $transactionArray['item_number']                            = $itemIDNumber;
            $transactionArray['itemIDNumber']                           = $itemIDNumber;
            $transactionArray['order_data']['receiver_network_id']      = substr($itemIDNumber,0,5);
            $transactionArray['order_data']['receiver_amount_in_local'] = $valueInLocal;
            $transactionArray['valueInLocal']                           = $valueInLocal;
            $transactionArray                                           = calculate_sale_price_foreign_auto($transactionArray);
            $transactionArray['mc_gross']                               = $transactionArray['sale_price_in_foreign'];
            $transactionArray['ourPriceInUSD']                          = $transactionArray['sale_price_in_foreign'];
            $transactionArray['mc_fee']                                 = $transactionArray['payment_processor_fee'];
        }
        else
        {
            
            $transactionArray['receiver_amount_in_local'] = $valueInLocal;
            $transactionArray['valueInLocal']             = $valueInLocal;
            $transactionArray                             = calculate_sale_price_foreign_auto($transactionArray);
            $transactionArray['mc_gross']                 = $transactionArray['sale_price_in_foreign'];
            $transactionArray['ourPriceInUSD']            = $transactionArray['sale_price_in_foreign'];
            $transactionArray['mc_fee']                   = $transactionArray['payment_processor_fee'];
        }

        if($success)
        {
            if(authenticateEmployee($employeeUsername, $employeePassword, $countryAbbreviation))
            {
                $connection=connect2DB2($countryAbbreviation);
                $transactionArray['connection'] = $connection;

                $transactionArray = checkDuplicateTransID($transactionArray);
                $success = !$transactionArray['transaction_id_exists'];

                if($clientEmail==""||$clientFirstName==""||$receiverPhone==""||$transID=="")
                {
                    $message.= 'The transaction ID, client email, client first name and receiver phone cannot be blank<br>';
                    disconnectDB($connection);
                    $_SESSION['message'] = $message;
                    $success = false;
                }
                else if( $success == false )
                {
                    $message.= "The transaction id you entered is a duplicate<br>";
                    disconnectDB($connection);
                    $_SESSION['message'] = $message;
                }
                else
                {
                    if(isAutoDispatch($itemIDNumber))
                    {
                        //AUTO_LOAD
                        $transactionArray['autoDispatch'] = 1;
                        $transactionArray = process_cart_auto_item($transactionArray);
                        
                        if($transactionArray['invoke_success'])
                        {
                            $transactionArray = recordSale($transactionArray);

                            updateReceiverInfo($transactionArray);

                            $cardPIN = $transactionArray['cardPIN'];
                            $airtime_gw_id = $transactionArray['airtime_gw_id'];
                            $sms_gw_id = $transactionArray['sms_gw_id'];
                            $network_transaction_id = $transactionArray['api_response']['MNOTransactionReferenceId'];
                            $message.= "Airtime Card with ItemID: $itemIDNumber Sent <br>";
                            $message.= "Access No.: $cardPIN <br>";
                            $message.= "Receiver Phone: $receiverPhone <br>";
                            $message.= "Receiver Email: $receiverEmail <br>";
                            $message.= "Client Email: $clientEmail <br>";
                            $message.= "SMS Gateway Confirmation: $sms_gw_id <br>";
                            $message.= "Airtime Gateway Confirmation: $airtime_gw_id <br>";
                            $message.= "Network Transaction ID: $network_transaction_id <br>";
                        }
                        else
                        {
                            if($transactionArray['api_response']['Status'] == 'OK')
                            {
                                $message.= "The airtime to $receiverPhone is being processed. ";
                                $message.= "A confirmation email will be sent once it is complete. ";
                                $message.= "It will also appear in the Used Airtime once complete. ";
                                $message.= "Transaction Status: ".$transactionArray['api_response']['TransactionStatus'].". ";
                                $message.= "Transaction Reference: ".$transactionArray['api_response']['TransactionReference'];
                            }
                            else
                            {
                                $message.= "Unable to send airtime. Item has been saved to outgoing queue. ";
                                $message.= $transactionArray['api_response']['StatusMessage'];
                                $success = false;
                            }
                        }
                    }
                    else
                    {
                        //CARD_LOAD
                        $transactionArray['autoDispatch'] = 0;
                        $transactionArray = useCard($transactionArray);

                        if($transactionArray['cardsArePresent'] == "true")
                        {
                            $transactionArray = recordSale($transactionArray);
                            emailAirtime($transactionArray);

                            updateReceiverInfo($transactionArray);

                            $cardPIN = $transactionArray['cardPIN'];
                            $message.= "Airtime Card with ItemID: $itemIDNumber Sent <br>";
                            $message.= "Access No.: $cardPIN <br>";
                            $message.= "Receiver Phone: $receiverPhone <br>";
                            $message.= "Receiver Email: $receiverEmail <br>";
                            $message.= "Client Email: $clientEmail <br>";
                        }
                        else
                        {
                            $message.= "There are no more $itemIDNumber cards left";
                            $success = false;
                        }
                    }

                    disconnectDB($connection);
                    $_SESSION['message'] = $message;
                }
            }
            else
            {
                $message.= 'The username and password are incorrect <br>';
                $_SESSION['message'] = $message;
                $success = false;
            }
        }
        else
        {
            $message = $transactionArray['error'];
            $_SESSION['message'] = $transactionArray['error'];
        }

        $transactionArray['status']  = $success ? "SUCCESS" : "FAILURE";
        $transactionArray['card']    = $transactionArray['cardPIN'];
        $transactionArray['result']  = $success;
        $transactionArray['success'] = $success;
        $transactionArray['message'] = $message;

        return $transactionArray;
}
?>