$(document).ready(function()
{
    $("#err1,#err2,#err3").css("display","none");
    $("#voucherselect1, #handdeliverymsg, #voucherselect2,#voucherselect3,#voucherselect4").css("display","none");
    select_voucher();
});

function mobile2_onblur()
{
    var mob1 = document.form1.mobile1;
    var mob2 = document.form1.mobile2;
    
    if (mob1.value !== mob2.value)
    {
      $("#err2").css("display","block");
      document.getElementById("err2").innerHTML = "Phone numbers do not match";
      mob2.select();
      mob2.value = '+256';
    }
    else
    {
        $("#err2").css("display","none");
    }
}

function mobile1_onblur()
{
    var mob1 = document.form1.mobile1;
    if (isNaN(mobile1.value) === true || mobile1.value.length < 13)
    {
        $("#err1").css("display","block");
          document.getElementById("err1").innerHTML = "Please enter the a valid phone number";
          mob1.select();
          mob1.value = '+256';
    }
    else
    {
        $("#err1").css("display","none");
    }
}

function send_onclick()
{
    var email = document.form1.email;
    if (email.value.length < 1)
    {
        $("#err3").css("display","block");
          document.getElementById("err1").innerHTML = "Please enter an email address";
          email.select();
          email.value = '';
    }
    else
    {
        $("#err3").css("display","none");
    }
}

function select_voucher()
{
    var price = document.form1.price;
    if (price.value == 20000)
    {
        $("#voucherselect1, #handdeliverymsg").css("display","block");
        $("#voucherselect2,#voucherselect3,#voucherselect4").css("display","none");
    }

    if (price.value == 50000)
    {
        $("#voucherselect2, #handdeliverymsg").css("display","block");
        $("#voucherselect1,#voucherselect3,#voucherselect4").css("display","none");
    }

    if (price.value == 100000)
    {
        $("#voucherselect3, #handdeliverymsg").css("display","block");
        $("#voucherselect1,#voucherselect2,#voucherselect4").css("display","none");
    }

    if (price.value == 150000)
    {
        $("#voucherselect4, #handdeliverymsg").css("display","block");
        $("#voucherselect2,#voucherselect3,#voucherselect1").css("display","none");
    }
}