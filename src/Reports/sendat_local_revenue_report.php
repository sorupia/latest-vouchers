<?
    require_once$_SERVER['DOCUMENT_ROOT']."/src/Yo/yo_constants.php";

    function sendat_local_revenue_report($input_array)
    {
        global $networkMap;
        global $networkArray;
        global $cardMap;//help determine what index value is for a particular card

        $start_date          = $input_array['start_date'];
        $end_date            = $input_array['end_date'];
        $exchange_rate       = $input_array['exchange_rate'];
        $cost_per_sms        = $input_array['cost_per_sms'];
        $countryAbbreviation = $input_array['countryAbbreviation'];
        $debug               = $input_array['debug'];
        $return_type         = $input_array['return_type'];

        $revenue_array = array();
        $index = 0;

        $ex_rate_loss = EX_RATE_LOSS; //10% loss in conversion
        $exchange_rate = $exchange_rate * $ex_rate_loss;

        $numberOfNetworks=count($networkMap[$countryAbbreviation]);	

        $connection=connect2DB($countryAbbreviation);

                $totalSold = 0;
                $totalPriceInLocal = 0;
                $totalRevenue = 0;

                $txt_msg = "";
                $row_msg = "";

                for($networkIndex=0;$networkIndex<$numberOfNetworks;$networkIndex++){

                    $thisNetwork=$networkMap[$countryAbbreviation][$networkIndex];
                    $numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);
                    //$text_msg.= $thisNetwork;
                    
                    for($j=0;$j<$numberOfDenominations;$j++)
                    {
                        $thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
                        
                        $soldQuery = "SELECT itemID,paymentFee FROM UsedCards WHERE ";
                        $soldQuery.= "UsedCards.itemID = '$thisCard' AND dateWeSoldIt > '$start_date' AND dateWeSoldIt < '$end_date' ";
                        $soldQuery.= "AND receiverPhone NOT LIKE '%233245867777%' AND transactionID NOT LIKE '%jacob%' ";
                        $soldQuery.= "AND saleType = '".YO_SALE_TYPE."' ";

                        $soldResult = mysql_query($soldQuery) or handleDatabaseError('' . mysql_error(),$soldQuery);

                        $numberSold = mysql_num_rows($soldResult);
                        $totalSold+= $numberSold;

                        $priceInLocal = getValueInLocal($thisCard);
                        $total_item_in_local = 0;

                        while ($data = mysql_fetch_array($soldResult, MYSQL_ASSOC))
                        {
                            $total_item_in_local+= $priceInLocal;
                        }

                        $totalPriceInLocal+= ($priceInLocal * $numberSold);

                        if($return_type == "return_msg")
                        {
                            //create email and txt message
                            $row_msg.= "<tr>";
                            $row_msg.= "<td>$thisCard</td> ";
                            $row_msg.= "<td>$numberSold</td> ";
                            $row_msg.= "<td>".$total_item_in_local."</td> ";
                            $row_msg.= "</tr>";	
                        }
                        else
                        {
                            //create array
                            //ItemID,Quantity,Price,Card_Profit,Profit_Int,Profit_US
                            $revenue_array[$index]["item_id"]             = $thisCard;
                            $revenue_array[$index]["quantity"]            = $numberSold;
                            $revenue_array[$index]["total_item_in_local"] = $total_item_in_local;
                            $index++;
                        }
                        mysql_free_result($soldResult);
                    }
                }

        disconnectDB($connection);

        //compile email and send it to stakeholders
        if($return_type == "return_msg")
        {
            $totalSMSCostInLocal = $cost_per_sms * $exchange_rate * $totalSold;
            $txt_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."\n";
            $txt_msg.=  substr(DOMAIN_PREFIX,0,-1)." \n";
            $txt_msg.= "Sales: ".LOCAL_CURRENCY."".$totalPriceInLocal."\n";
            $txt_msg.= "Number of Transactions: ".$totalSold."\n";

            if($debug) echo "txt_msg: ".$txt_msg."<br>";
            if($debug) echo "txt_msg.len: ".strlen($txt_msg)."<br>";
            
            $email_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."<br>";
            $email_msg.= substr(DOMAIN_PREFIX,0,-1)." <br>";
            $email_msg.= "Sales: ".LOCAL_CURRENCY."".$totalPriceInLocal."<br>";
            $email_msg.= "Number of Transactions: ".$totalSold."<br>";
            $email_msg.= "Total SMS Cost: ".LOCAL_CURRENCY."".$totalSMSCostInLocal."<br>";
            $email_msg.= "\n";

            $email_msg.= "<table width=\"75%\"  border=\"1\" >";

            $email_msg.= "<tr>";
            $email_msg.= "<td>Item ID</td> ";
            $email_msg.= "<td>Quantity</td> ";
            $email_msg.= "<td>Total Item Revenue</td> ";
            $email_msg.= "</tr>";

            $email_msg.= $row_msg;

            $email_msg.= "<tr>";
            $email_msg.= "<td>TOTALS</td> ";
            $email_msg.= "<td>$totalSold</td> ";
            $email_msg.= "<td>".$totalPriceInLocal."</td> ";
            $email_msg.= "</tr>";

            $email_msg.= "</table>";

            $email_msg.= "<br>---Reports provided by 3nitylabs Inc.";

            return array("email"=>$email_msg,"txt"=>$txt_msg);
        }
        else
        {
            //return array containing report elements
            //ItemID,Quantity,Price,Cost to us,PayPal_Fee,Card_Profit,Profit_Int,Profit_US
            $revenue_array[$index]["item_id"]             = "TOTAL";
            $revenue_array[$index]["quantity"]            = $totalSold;
            $revenue_array[$index]["total_item_in_local"] = $totalPriceInLocal;
            if($debug) echo "Revenue Array returned <br>";
            return $revenue_array;
        }
    }
?>