<?

/*******************************************************************************
**   FILE: sendat_foreign_revenue_report_auto.php
**
**   FUNCTION: sendat_foreign_revenue_report_auto
**
**   PURPOSE: Function to calculate the foreign revenue collected for auto loads
**   
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 13.Aug.2012
*********************************************************************************/

    require_once$_SERVER['DOCUMENT_ROOT']."/src/Constants/Constants.php";
    require_once$_SERVER['DOCUMENT_ROOT']."/src/Order/OrderFunctions.php";

    function sendat_foreign_revenue_report_auto($input_array)
    {
        $start_date          = $input_array['start_date'];
        $end_date            = $input_array['end_date'];
        $exchange_rate       = $input_array['exchange_rate'];
        $cost_per_sms        = $input_array['cost_per_sms'];
        $countryAbbreviation = $input_array['countryAbbreviation'];
        $debug               = $input_array['debug'];
        $return_type         = $input_array['return_type'];

        global $auto_load_network_array;
        $revenue_array = array();
        $index = 0;

        $orig_exchange_rate = $exchange_rate;
        $ex_rate_loss = EX_RATE_LOSS; // loss in conversion
        $exchange_rate = $exchange_rate * $ex_rate_loss;

        $numberOfNetworks = count($auto_load_network_array["$countryAbbreviation"]);
        $thisCountryNetworkMap = array_keys($auto_load_network_array["$countryAbbreviation"]);

        $connection = connect2DB($countryAbbreviation);
        $totalProfit = 0;
        $totalRevenue = 0;
        $totalAirtimeProcFee = 0;
        $totalPayProcFee = 0;
        $totalActualPrice = 0;
        $totalSold = 0;

        $txt_msg = "";
        $row_msg = "";

        for( $networkIndex = 0 ; $networkIndex < $numberOfNetworks ; $networkIndex++ )
        {
            //Do a summation for each network
            $thisNetwork = $thisCountryNetworkMap[$networkIndex];
            $text_msg.= $thisNetwork;
            
            $soldQuery = "SELECT itemID,paymentFee,our_fee,airtime_processor_fee ";
            $soldQuery.= "FROM UsedCards ";
            $soldQuery.= "WHERE ";
            $soldQuery.= "UsedCards.itemID LIKE '%".$thisNetwork.AIRTIME_LOAD_OPTION_AUTO."%' ";
            $soldQuery.= "AND dateWeSoldIt > '$start_date' ";
            $soldQuery.= "AND dateWeSoldIt < '$end_date' ";
            $soldQuery.= "AND saleType = 'PAYPAL' ";

            if($debug) echo "soldQuery: ".$soldQuery."<br>";

            $soldResult = mysql_query($soldQuery) or handleDatabaseError('' . mysql_error(),$soldQuery);
            $numberSold = mysql_num_rows($soldResult);
            $totalSold+= $numberSold;

            $totalNetworkRevenue = 0;
            $total_network_actual_price = 0;
            $total_network_pp_tx_fee = 0;
            $total_network_ap_tx_fee = 0;
            $total_network_gross_profit = 0;
            $our_fee = 0;

            while ($data = mysql_fetch_array($soldResult, MYSQL_ASSOC))
            {
                $thisCard = $data['itemID'];

                $priceInLocal = getValueInLocal($thisCard);
                $actual_price = $priceInLocal / $exchange_rate;
                $total_network_actual_price += $actual_price;

                $input_array['item_id'] = $thisCard;
                $input_array['receiver_amount_in_local'] = $priceInLocal;
                $input_array = calculate_sale_price_foreign_auto($input_array);
                $priceInUSD = $input_array['sale_price_in_foreign'];

                $totalNetworkRevenue+= $priceInUSD;
                $totalRevenue+= $priceInUSD;

                $pp_tx_fee = $data['paymentFee'];
                $ap_tx_fee = $data['airtime_processor_fee'];
                $our_fee   = $data['our_fee'];

                if($ap_tx_fee == 0)
                {
                    if($debug) echo "No Airtime Processor fee in table therefore use calculated<br>";
                    $ap_tx_fee = $input_array['airtime_processor_fee'];
                }

                if($pp_tx_fee == 0)
                {
                    if($debug) echo "No PayPal fee in table therefore use calculated<br>";
                    $pp_tx_fee = $input_array['payment_processor_fee'];
                }
                else
                {
                    if($debug) echo "PayPal fee in table therefore calculate our fee using paypal<br>";
                    $our_fee = $priceInUSD - $actual_price - $pp_tx_fee - $ap_tx_fee - $cost_per_sms;
                }

                if($our_fee == 0)
                {
                    if($debug) echo "No Our fee or PayPal fee in table therefore use calculated<br>";
                    $our_fee = $input_array['our_fee'];
                }

                $total_network_pp_tx_fee += $pp_tx_fee;
                $total_network_ap_tx_fee += $ap_tx_fee;
                $total_network_gross_profit+= $our_fee;

                if($debug) echo "thisCard: ".$thisCard."<br>";                
                if($debug) echo "priceInUSD: ".$priceInUSD."<br>";
                if($debug) echo "actual price: ".$actual_price."<br>";
                if($debug) echo "pp_tx_fee: ".$pp_tx_fee."<br>";
                if($debug) echo "ap_tx_fee: ".$ap_tx_fee."<br>";
                if($debug) echo "cost_per_sms: ".$cost_per_sms."<br>";
                if($debug) echo "our_fee: ".$our_fee."<br><br>";

                if($return_type == "return_msg")
                {
                    //create email and txt message
                    $row_msg.= "<tr>";
                    $row_msg.= "<td>".$thisCard."</td> ";
                    $row_msg.= "<td>1</td> ";
                    $row_msg.= "<td>".$priceInUSD."</td> ";
                    $row_msg.= "<td>".number_format($actual_price,FOREIGN_DECIMAL_PLACES)."</td> ";
                    $row_msg.= "<td>".$pp_tx_fee."</td> ";
                    $row_msg.= "<td>".$ap_tx_fee."</td> ";
                    $row_msg.= "<td>".number_format($our_fee,FOREIGN_DECIMAL_PLACES)."</td> ";
                    $row_msg.= "</tr>";
                }
                else
                {
                    //create array
                    //ItemID,Quantity,Price,Card_Profit,Profit_Int,Profit_US
                    $revenue_array[$index]["item_id"]          = $thisCard;
                    $revenue_array[$index]["total_sold"]       = 1;
                    $revenue_array[$index]["price_in_usd"]     = $priceInUSD;
                    $revenue_array[$index]["cost_in_usd"]      = number_format($actual_price,FOREIGN_DECIMAL_PLACES);
                    $revenue_array[$index]["paypal_fee"]       = $pp_tx_fee;
                    $revenue_array[$index]["airtime_proc_fee"] = $ap_tx_fee;
                    $revenue_array[$index]["card_profit"]      = number_format($our_fee,FOREIGN_DECIMAL_PLACES);
                    $index++;
                }
            }

            $totalProfit+= $total_network_gross_profit;
            $totalAirtimeProcFee+= $total_network_ap_tx_fee;
            $totalPayProcFee+= $total_network_pp_tx_fee;
            $totalActualPrice+= $total_network_actual_price;

            //Insert a summation row per network id
            if($return_type == "return_msg")
            {
                //create email and txt message
                $row_msg.= "<tr>";
                $row_msg.= "<td>".$thisNetwork."</td> ";
                $row_msg.= "<td>".$numberSold." </td> ";
                $row_msg.= "<td>".$totalNetworkRevenue."</td> ";
                $row_msg.= "<td>".number_format($total_network_actual_price,FOREIGN_DECIMAL_PLACES)."</td> ";
                $row_msg.= "<td>".$total_network_pp_tx_fee."</td> ";
                $row_msg.= "<td>".$total_network_ap_tx_fee."</td> ";
                $row_msg.= "<td>".number_format($total_network_gross_profit,FOREIGN_DECIMAL_PLACES)."</td> ";
                $row_msg.= "</tr>";
            }
            else
            {
                //create array
                //ItemID,Quantity,Price,Card_Profit,Profit_Int,Profit_US
                $revenue_array[$index]["item_id"]          = $thisNetwork;
                $revenue_array[$index]["total_sold"]       = $numberSold;
                $revenue_array[$index]["price_in_usd"]     = $totalNetworkRevenue;
                $revenue_array[$index]["cost_in_usd"]      = number_format($total_network_actual_price,FOREIGN_DECIMAL_PLACES);
                $revenue_array[$index]["paypal_fee"]       = $total_network_pp_tx_fee;
                $revenue_array[$index]["airtime_proc_fee"] = $total_network_ap_tx_fee;
                $revenue_array[$index]["total_item_profit"]= number_format($total_network_gross_profit,FOREIGN_DECIMAL_PLACES);
                $index++;
            }

            mysql_free_result($soldResult);
        }

        disconnectDB($connection);

        //compile email and send it to stakeholders
        if($return_type == "return_msg")
        {
            $txt_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."\n";
            $txt_msg.=  substr(DOMAIN_PREFIX,0,-1)." \n";
            $txt_msg.= "Auto Load Foreign Revenue: ".SALE_CURRENCY." ".$totalRevenue."\n";
            $txt_msg.= "Gross Profit: ".SALE_CURRENCY." ".number_format($totalProfit,FOREIGN_DECIMAL_PLACES)."\n";
            $txt_msg.= "Number of Transactions: ".$totalSold."\n";
            $txt_msg.= "Ex. Rate: ".$exchange_rate."\n";

            if($debug) echo "txt_msg: ".$txt_msg."<br>";
            if($debug) echo "txt_msg.len: ".strlen($txt_msg)."<br>";

            $email_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."<br>";
            $email_msg.=  substr(DOMAIN_PREFIX,0,-1)." <br>";
            $email_msg.= "Auto Load Foreign Revenue: ".SALE_CURRENCY." ".$totalRevenue."<br>";
            $email_msg.= "Gross Profit: ".SALE_CURRENCY." ".number_format($totalProfit,FOREIGN_DECIMAL_PLACES)."<br>";
            $email_msg.= "Number of Transactions: ".$totalSold."<br>";
            $email_msg.= "Orig Ex. Rate: ".$orig_exchange_rate."<br>";
            $email_msg.= "Ex. Rate: ".$exchange_rate."<br>";
            $email_msg.= "Cost Per SMS: ".$cost_per_sms."<br>";
            $email_msg.= "\n";

            $totalGrossProfit = $totalProfit;

            if($totalGrossProfit >= 4500)      $weeklyExpenses = 500;
            else if($totalGrossProfit >= 3250) $weeklyExpenses = 250;
            else                               $weeklyExpenses = 150;

            //$email_msg.= "Weekly Expenses: ".SALE_CURRENCY."".$weeklyExpenses." <br>";

            $ourRevenue = ($totalGrossProfit - $weeklyExpenses)/(4.0);
            //$email_msg.= "3nitylabs 25%: ".SALE_CURRENCY."".round($ourRevenue,2)."<br>";

            $email_msg.= "<table width=\"75%\"  border=\"1\" >";

            $email_msg.= "<tr>";
            $email_msg.= "<td>Item ID</td> ";
            $email_msg.= "<td>Quantity</td> ";
            $email_msg.= "<td>Price in USD</td> ";
            $email_msg.= "<td>Cost in USD</td> ";
            $email_msg.= "<td>PayPal Fee</td> ";
            $email_msg.= "<td>Airtime Proc Fee</td> ";
            $email_msg.= "<td>Total Item Profit</td> ";
            $email_msg.= "</tr>";

            $email_msg.= $row_msg;

            $email_msg.= "<tr>";
            $email_msg.= "<td>TOTALS</td> ";
            $email_msg.= "<td>".$totalSold."</td> ";
            $email_msg.= "<td>".number_format($totalRevenue, FOREIGN_DECIMAL_PLACES)."</td> ";
            $email_msg.= "<td>".number_format($totalActualPrice, FOREIGN_DECIMAL_PLACES)."</td> ";
            $email_msg.= "<td>".number_format($totalPayProcFee, FOREIGN_DECIMAL_PLACES)."</td> ";
            $email_msg.= "<td>".number_format($totalAirtimeProcFee, FOREIGN_DECIMAL_PLACES)."</td> ";
            $email_msg.= "<td>".number_format($totalProfit,FOREIGN_DECIMAL_PLACES)."</td> ";
            $email_msg.= "</tr>";

            $email_msg.= "</table>";

            $email_msg.= "<br>---Reports provided by 3nitylabs Inc.";

            return array("email"=>$email_msg,"txt"=>$txt_msg);
        }
        else
        {
            //return array containing report elements
            //ItemID,Quantity,Price,Cost to us,PayPal_Fee,Card_Profit,Profit_Int,Profit_US
            $revenue_array[$index]["item_id"]           = "TOTAL";
            $revenue_array[$index]["quantity"]          = $totalSold;
            $revenue_array[$index]["price_in_usd"]      = number_format($totalRevenue, FOREIGN_DECIMAL_PLACES);
            $revenue_array[$index]["cost_in_usd"]       = number_format($totalActualPrice, FOREIGN_DECIMAL_PLACES);
            $revenue_array[$index]["paypal_fee"]        = number_format($totalPayProcFee, FOREIGN_DECIMAL_PLACES);
            $revenue_array[$index]["airtime_proc_fee"]  = number_format($totalAirtimeProcFee, FOREIGN_DECIMAL_PLACES);
            $revenue_array[$index]["total_item_profit"] = number_format($totalProfit,FOREIGN_DECIMAL_PLACES);
            if($debug) echo "Revenue Array returned <br>";
            return $revenue_array;
        }
    }
?>