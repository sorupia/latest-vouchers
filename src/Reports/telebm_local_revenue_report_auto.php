<?

/*******************************************************************************
**   FILE: telebm_local_revenue_report_auto.php
**
**   FUNCTION: telebm_local_revenue_report_auto
**
**   PURPOSE: Function to calculate the local revenue collected for auto loads
**   
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.12.06
*********************************************************************************/

require_once$_SERVER['DOCUMENT_ROOT']."/src/Constants/Constants.php";
require_once$_SERVER['DOCUMENT_ROOT']."/src/Order/OrderFunctions.php";

function telebm_local_revenue_report_auto($input_array)
{
    global $auto_load_network_array;

    $start_date          = $input_array['start_date'];
    $end_date            = $input_array['end_date'];
    $exchange_rate       = $input_array['exchange_rate'];
    $cost_per_sms        = $input_array['cost_per_sms'];
    $countryAbbreviation = $input_array['countryAbbreviation'];
    $debug               = $input_array['debug'];
    $return_type         = $input_array['return_type'];

    $revenue_array = array();
    $index = 0;

    $ex_rate_loss = EX_RATE_LOSS; // loss in conversion
    $exchange_rate = $exchange_rate * $ex_rate_loss;

    $numberOfNetworks = count($auto_load_network_array["$countryAbbreviation"]);
    $thisCountryNetworkMap = array_keys($auto_load_network_array["$countryAbbreviation"]);

    $connection = connect2DB($countryAbbreviation);
    $totalSold = 0;

    $totalPriceInLocal = 0;

    $txt_msg = "";
    $row_msg = "";

    for( $networkIndex = 0 ; $networkIndex < $numberOfNetworks ; $networkIndex++ )
    {
        //Do a summation for each network
        $thisNetwork = $thisCountryNetworkMap[$networkIndex];
        //$txt_msg.= $thisNetwork;
        
        $soldQuery = "SELECT itemID,paymentFee,our_fee,airtime_processor_fee ";
        $soldQuery.= "FROM UsedCards ";
        $soldQuery.= "WHERE ";
        $soldQuery.= "UsedCards.itemID LIKE '%".$thisNetwork.AIRTIME_LOAD_OPTION_AUTO."%' ";
        $soldQuery.= "AND dateWeSoldIt > '$start_date' ";
        $soldQuery.= "AND dateWeSoldIt < '$end_date' ";
        //$soldQuery.= "AND saleType = 'PAYPAL' ";

        if($debug) echo "soldQuery: ".$soldQuery."<br>";

        $soldResult = mysql_query($soldQuery,$connection) or handleDatabaseError('' . mysql_error(),$soldQuery);
        $numberSold = mysql_num_rows($soldResult);
        $totalSold+= $numberSold;

        $totalNetworkRevenueLocal = 0;

        while ($data = mysql_fetch_array($soldResult, MYSQL_ASSOC))
        {
            $thisCard = $data['itemID'];
            $input_array['item_id'] = $thisCard;

            $priceInLocal = getValueInLocal($thisCard);

            $totalNetworkRevenueLocal+= $priceInLocal;
            $totalPriceInLocal+= $priceInLocal;

            if($debug) echo "thisCard: ".$thisCard."<br>";                
            if($debug) echo "priceInLocal: ".$priceInLocal."<br>";

            if($return_type == "return_msg")
            {
                //create email and txt message
                $row_msg.= "<tr>";
                $row_msg.= "<td>".$thisCard."</td> ";
                $row_msg.= "<td>1</td> ";
                $row_msg.= "<td>".number_format($priceInLocal,LOCAL_DECIMAL_PLACES)."</td> ";
                $row_msg.= "</tr>";
            }
            else
            {
                //create array
                //ItemID,Quantity,Price,Card_Profit,Profit_Int,Profit_US
                $revenue_array[$index]["item_id"]          = $thisCard;
                $revenue_array[$index]["total_sold"]       = 1;
                $revenue_array[$index]["price_in_local"]   = number_format($priceInLocal,LOCAL_DECIMAL_PLACES);
                $index++;
            }
        }

        //Insert a summation row per network id
        if($return_type == "return_msg")
        {
            //create email and txt message
            $row_msg.= "<tr>";
            $row_msg.= "<td>".$thisNetwork."</td> ";
            $row_msg.= "<td>".$numberSold." </td> ";
            $row_msg.= "<td>".number_format($totalNetworkRevenueLocal,LOCAL_DECIMAL_PLACES)."</td> ";
            $row_msg.= "</tr>";
        }
        else
        {
            //create array
            //ItemID,Quantity,Price,Card_Profit,Profit_Int,Profit_US
            $revenue_array[$index]["item_id"]        = $thisNetwork;
            $revenue_array[$index]["quantity"]       = $numberSold;
            $revenue_array[$index]["price_in_local"] = number_format($totalNetworkRevenueLocal,LOCAL_DECIMAL_PLACES);
            $index++;
        }

        mysql_free_result($soldResult);
    }

    disconnectDB($connection);

    //compile email and send it to stakeholders
    if($return_type == "return_msg")
    {
        $totalSMSCostInLocal = $cost_per_sms * $exchange_rate * $totalSold;
        $ourRevenue = $totalPriceInLocal*(1.00 + ADMIN_TX_PCT_FEE) + ($totalSold * ADMIN_TX_FIXED_FEE) + $totalSMSCostInLocal;

        $txt_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."\n";
        $txt_msg.= substr(DOMAIN_PREFIX,0,-1)." \n";
        $txt_msg.= "Auto Load Local Revenue: ".LOCAL_CURRENCY." ".number_format($totalPriceInLocal,LOCAL_DECIMAL_PLACES)."\n";
        $txt_msg.= "Number of Transactions: ".$totalSold."\n";
        $txt_msg.= "3nitylabs: ".LOCAL_CURRENCY." ".number_format($ourRevenue,LOCAL_DECIMAL_PLACES)."\n";

        if($debug) echo "txt_msg: ".$txt_msg."<br>";
        if($debug) echo "txt_msg.len: ".strlen($txt_msg)."<br>";

        $email_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."<br>";
        $email_msg.= substr(DOMAIN_PREFIX,0,-1)." <br>";
        $email_msg.= "Auto Load Local Revenue: ".LOCAL_CURRENCY." ".number_format($totalPriceInLocal,LOCAL_DECIMAL_PLACES)."<br>";
        $email_msg.= "Number of Transactions: ".$totalSold."<br>";
        $email_msg.= "Total SMS Cost: ".LOCAL_CURRENCY." ".number_format($totalSMSCostInLocal,LOCAL_DECIMAL_PLACES)."<br>";
        $email_msg.= "\n";

        $email_msg.= SERVICED_BY." to pay 3nitylabs : ".LOCAL_CURRENCY." ".number_format($ourRevenue,LOCAL_DECIMAL_PLACES)."<br><br>";

        $email_msg.= "<table width=\"75%\"  border=\"1\" >";

        $email_msg.= "<tr>";
        $email_msg.= "<td>Item ID</td> ";
        $email_msg.= "<td>Quantity</td> ";
        $email_msg.= "<td>Total Item Revenue</td> ";
        $email_msg.= "</tr>";

        $email_msg.= $row_msg;

        $email_msg.= "<tr>";
        $email_msg.= "<td>TOTALS</td> ";
        $email_msg.= "<td>$totalSold</td> ";
        $email_msg.= "<td>".number_format($totalPriceInLocal,LOCAL_DECIMAL_PLACES)."</td> ";
        $email_msg.= "</tr>";

        $email_msg.= "</table>";

        $email_msg.= "<br>---Reports provided by 3nitylabs Inc.";
        
        return array("email"=>$email_msg,"txt"=>$txt_msg);
    }
    else
    {
        //return array containing report elements
        //ItemID,Quantity,Price,Cost to us,PayPal_Fee,Card_Profit,Profit_Int,Profit_US
        $revenue_array[$index]["item_id"]             = "TOTAL";
        $revenue_array[$index]["quantity"]            = $totalSold;
        $revenue_array[$index]["total_item_in_local"] = number_format($totalPriceInLocal, LOCAL_DECIMAL_PLACES);
        if($debug) echo "Revenue Array returned <br>";
        return $revenue_array;
    }
}
?>