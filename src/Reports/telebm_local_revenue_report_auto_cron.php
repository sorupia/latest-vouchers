<?
/*******************************************************************************
**   FILE: telebm_local_revenue_report_auto_cron.php
**
**   FUNCTION: telebm_local_revenue_report_auto_cron
**
**   PURPOSE: Performs the cronjob for the auto load revenue report
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2012.12.11
*********************************************************************************/

require_once$_SERVER['DOCUMENT_ROOT']."/src/Reports/telebm_local_revenue_report_auto.php";

/* Performs the cronjob for revenue reports*/
function telebm_local_revenue_report_auto_cron($input_array)
{
    $countryAbbreviation = $input_array['countryAbbreviation'];
    $exchange_rate       = $input_array['exchange_rate'];
    $revenue_emails      = $input_array['revenue_emails'];
    $revenue_phones      = $input_array['revenue_phones'];
    $debug               = $input_array['debug'];

    $utc_start_date = mktime(0, 0, 0, date("m")  , date("d")-7, date("Y"));
    $start_date     = date("Y-m-d",$utc_start_date)." 00:00:01";//Y-m-d H:i:s
    $utc_end_date   = mktime(0, 0, 0, date("m")  , date("d")-1, date("Y"));
    $end_date       = date("Y-m-d",$utc_end_date)." 23:59:59";

    $cost_per_sms = PRICE_PER_SMS;

    $input_array['start_date']          = $start_date;
    $input_array['end_date']            = $end_date;
    $input_array['cost_per_sms']        = $cost_per_sms;
    $input_array['return_type']         = "return_msg";

    $result_array = telebm_local_revenue_report_auto($input_array);

    $email_msg = $result_array["email"];
    $txt_msg = $result_array["txt"];

    if($debug) echo $email_msg;

    $subject = "Revenue: ".substr($start_date,0,10)." to ".substr($end_date,0,10);

    for( $count=0 ; ($count < count($revenue_emails)) && !$debug ; $count++)
    {
        $dest_email = $revenue_emails[$count];
        echo "Revenue Report Sent to: ".$dest_email."\n";
        $isHTML = true;
        sendEmail3(FROM_NAME,FROM_EMAIL,$dest_email,$bcc,$subject,$email_msg,$isHTML);
    }

    //compile txt message and send to stakeholders
    for( $count=0 ; ($count < count($revenue_phones)) && !$debug ; $count++)
    {
        $dest_phone = $revenue_phones[$count];
        echo "Revenue Report Sent to: ".$dest_phone."\n";
        sendTextUsingClick2($dest_phone,$txt_msg);
    }
}
?>