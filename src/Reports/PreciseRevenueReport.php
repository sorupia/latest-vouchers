<?
    function preciseRevenueReport($start_date, 
                                  $end_date, 
                                  $exchange_rate, 
                                  $cost_per_sms, 
                                  $countryAbbreviation,
                                  $debug,
                                  $return_type)
    {
        global $networkMap;
        global $networkArray;
        global $cardMap;//help determine what index value is for a particular card
        
        $revenue_array = array();
        $index = 0;
        
        $orig_exchange_rate = $exchange_rate;
        $ex_rate_loss = EX_RATE_LOSS; // loss in conversion
        $exchange_rate = $exchange_rate * $ex_rate_loss;

        $numberOfNetworks=count($networkMap[$countryAbbreviation]);	
        $pp_tx_fee_2 = PP_TX_FEE;		

        $connection=connect2DB($countryAbbreviation);
                
                $totalNumber = 0;
                $totalSold = 0;
                $totalProfit = 0;
                $totalProfitInt_orig = 0;
                $totalProfitUS_orig = 0;

                $txt_msg = "";
                $row_msg = "";
                
                for($networkIndex=0;$networkIndex<$numberOfNetworks;$networkIndex++)
                {
                    
                    $thisNetwork=$networkMap[$countryAbbreviation][$networkIndex];
                    $numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);
                    $text_msg.= $thisNetwork;
                    
                    for($j=0;$j<$numberOfDenominations;$j++)
                    {
                        $thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];
                        
                        $soldQuery = "SELECT itemID,paymentFee FROM UsedCards WHERE ";
                        $soldQuery.= "UsedCards.itemID = '$thisCard' AND dateWeSoldIt > '$start_date' AND dateWeSoldIt < '$end_date' ";
                        $soldQuery.= "AND saleType = 'PAYPAL' ";

                        $soldResult = mysql_query($soldQuery) or handleDatabaseError('' . mysql_error(),$soldQuery);
                        
                        $numberSold = mysql_num_rows($soldResult);
                        $totalSold+= $numberSold;
                        
                        $priceInUSD = getPriceInUSD($thisCard);
                        $priceInLocal = getValueInLocal($thisCard);
                        $actual_price = $priceInLocal / $exchange_rate;
                        $profit_per_item = 0;
                        $gross_per_card = 0;
                        $pp_tx_fee = 0;
                        $pp_tx_fee_int_1 = PP_TX_PCT_INT * $priceInUSD;
                        $pp_tx_fee_us_1  = PP_TX_PCT_US * $priceInUSD;
                        
                        //attributes for a more detailed report
                        //since PayPal charges different fees for US & Rest of World
                        //_0 is for those UsedCards we have no payment_fee record for
                        //_1 & _2 are for either US or Rest of World which ever record comes first
                        $fee_0 = 0;
                        $fee_1 = 0;
                        $fee_2 = 0;
                        $fee_1_populated = false;
                        $fee_selected = 0;
                        $profit_per_item_0 = 0;
                        $profit_per_item_1 = 0;
                        $profit_per_item_2 = 0;
                        $gross_per_card_0 = 0;
                        $gross_per_card_1 = 0;
                        $gross_per_card_2 = 0;
                        $count_0 = 0;
                        $count_1 = 0;
                        $count_2 = 0;
                        
                        
                        while ($data = mysql_fetch_array($soldResult, MYSQL_ASSOC))
                        {
                            $pp_tx_fee = $data['paymentFee'];
                            
                            if($pp_tx_fee == 0)
                            {
                                if($debug) echo "No PayPal fee in table therefore calculate<br>";
                                $pp_tx_fee = $pp_tx_fee_2 + (($pp_tx_fee_int_1 + $pp_tx_fee_us_1)/2);
                                $fee_0 = $pp_tx_fee;
                                $fee_selected = 0;
                                $count_0++;
                            }
                            else if($fee_1 == 0 && !$fee_1_populated)
                            {
                                $fee_1 = $pp_tx_fee;
                                $fee_1_populated = true;
                                $fee_selected = 1;
                                $count_1++;
                            }
                            else if($fee_1 == $pp_tx_fee)
                            {
                                $fee_selected = 1;
                                $count_1++;
                            }
                            else
                            {
                                $fee_2 = $pp_tx_fee;
                                $fee_selected = 2;
                                $count_2++;
                            }

                            $cost_b4_markup = $actual_price + $pp_tx_fee + $cost_per_sms;
                            $gross_per_card = $priceInUSD - $cost_b4_markup;		
                            $profit_per_item+= $gross_per_card;

                            //code to provide a more detailed report
                            if($fee_selected == 0)
                            {
                                $gross_per_card_0  = $gross_per_card;
                                $profit_per_item_0+= $gross_per_card;
                            }
                            else if ($fee_selected == 1)
                            {
                                $gross_per_card_1  = $gross_per_card;
                                $profit_per_item_1+= $gross_per_card;
                            }
                            else
                            {
                                $gross_per_card_2  = $gross_per_card;
                                $profit_per_item_2+= $gross_per_card;
                            }

                            if($debug) echo "priceInUSD: ".$priceInUSD."<br>";
                            if($debug) echo "actual price: ".$actual_price."<br>";
                            if($debug) echo "pp_tx_fee: ".$pp_tx_fee."<br>";
                            if($debug) echo "cost_per_sms: ".$cost_per_sms."<br>";
                            if($debug) echo "gross_per_card: ".$gross_per_card."<br><br>";
                        }

                        $totalProfit+= $profit_per_item;
                        $totalRevenue+= ($priceInUSD * $numberSold);

                        /* as requested by Jacob for original revenue with regular ex rate*/
                        $actual_price_orig = ($priceInLocal * $ex_rate_loss) / $exchange_rate;						
                        $cost_b4_markup_int_orig = $actual_price_orig + $pp_tx_fee_int_1 + $pp_tx_fee_2 + $cost_per_sms;
                        $cost_b4_markup_us_orig = $actual_price_orig + $pp_tx_fee_us_1 + $pp_tx_fee_2 + $cost_per_sms;
                        $totalProfitInt_orig+= ($priceInUSD-$cost_b4_markup_int_orig)*$numberSold;
                        $totalProfitUS_orig+= ($priceInUSD-$cost_b4_markup_us_orig)*$numberSold;
                        /* end orig revenue calculation*/
                        
                        if($return_type == "return_msg")
                        {
                            //create email and txt message
                            $row_msg.= "<tr>";
                            $row_msg.= "<td>".$thisCard."_0</td> ";
                            $row_msg.= "<td>$count_0</td> ";
                            $row_msg.= "<td>$priceInUSD</td> ";
                            $row_msg.= "<td>".round($actual_price,2)."</td> ";
                            $row_msg.= "<td>".round($fee_0,2)."</td> ";
                            $row_msg.= "<td>".round($gross_per_card_0,2)."</td> ";
                            $row_msg.= "<td>".round($profit_per_item_0,2)."</td> ";
                            $row_msg.= "</tr>";

                            $row_msg.= "<tr>";
                            $row_msg.= "<td>".$thisCard."_1</td> ";
                            $row_msg.= "<td>$count_1</td> ";
                            $row_msg.= "<td>$priceInUSD</td> ";
                            $row_msg.= "<td>".round($actual_price,2)."</td> ";
                            $row_msg.= "<td>".round($fee_1,2)."</td> ";
                            $row_msg.= "<td>".round($gross_per_card_1,2)."</td> ";
                            $row_msg.= "<td>".round($profit_per_item_1,2)."</td> ";
                            $row_msg.= "</tr>";

                            $row_msg.= "<tr>";
                            $row_msg.= "<td>".$thisCard."_2</td> ";
                            $row_msg.= "<td>$count_2</td> ";
                            $row_msg.= "<td>$priceInUSD</td> ";
                            $row_msg.= "<td>".round($actual_price,2)."</td> ";
                            $row_msg.= "<td>".round($fee_2,2)."</td> ";
                            $row_msg.= "<td>".round($gross_per_card_2,2)."</td> ";
                            $row_msg.= "<td>".round($profit_per_item_2,2)."</td> ";
                            $row_msg.= "</tr>";
                        }
                        else
                        {
                            //create array
                            //ItemID,Quantity,Price,Card_Profit,Profit_Int,Profit_US
                            $revenue_array[$index]["item_id"]          = $thisCard."_0";
                            $revenue_array[$index]["quantity"]         = $count_0;
                            $revenue_array[$index]["price_in_usd"]     = $priceInUSD;
                            $revenue_array[$index]["cost_in_usd"]      = round($actual_price,2);
                            $revenue_array[$index]["paypal_fee"]       = $fee_0;
                            $revenue_array[$index]["card_profit"]      = round($gross_per_card_0,2);
                            $revenue_array[$index]["total_item_profit"]= round($profit_per_item_0,2);
                            $index++;

                            $revenue_array[$index]["item_id"]          = $thisCard."_1";
                            $revenue_array[$index]["quantity"]         = $count_1;
                            $revenue_array[$index]["price_in_usd"]     = $priceInUSD;
                            $revenue_array[$index]["cost_in_usd"]      = round($actual_price,2);
                            $revenue_array[$index]["paypal_fee"]       = $fee_1;
                            $revenue_array[$index]["card_profit"]      = round($gross_per_card_1,2);
                            $revenue_array[$index]["total_item_profit"]= round($profit_per_item_1,2);
                            $index++;

                            $revenue_array[$index]["item_id"]          = $thisCard."_2";
                            $revenue_array[$index]["quantity"]         = $count_2;
                            $revenue_array[$index]["price_in_usd"]     = $priceInUSD;
                            $revenue_array[$index]["cost_in_usd"]      = round($actual_price,2);
                            $revenue_array[$index]["paypal_fee"]       = $fee_2;
                            $revenue_array[$index]["card_profit"]      = round($gross_per_card_2,2);
                            $revenue_array[$index]["total_item_profit"]= round($profit_per_item_2,2);
                            $index++;
                        }
                                                
                        mysql_free_result($soldResult);
                    }

                }
                            
        disconnectDB($connection);
        
        //compile email and send it to stakeholders
        if($return_type == "return_msg")
        {
            $txt_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."\n";
            $txt_msg.=  substr(DOMAIN_PREFIX,0,-1)." \n";
            $txt_msg.= "Revenue: ".SALE_CURRENCY."".$totalRevenue."\n";
            $txt_msg.= "Gross Profit: ".SALE_CURRENCY."".round($totalProfit,2)."\n";
            $txt_msg.= "Number of Transactions: ".$totalSold."\n";
            $txt_msg.= "Ex. Rate: ".$exchange_rate."\n";
            
            if($debug) echo "txt_msg: ".$txt_msg."<br>";
            if($debug) echo "txt_msg.len: ".strlen($txt_msg)."<br>";
            
            $email_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."<br>";
            $email_msg.=  substr(DOMAIN_PREFIX,0,-1)." <br>";
            $email_msg.= "Revenue: ".SALE_CURRENCY."".$totalRevenue."<br>";
            $email_msg.= "Gross Profit: ".SALE_CURRENCY."".round($totalProfit,2)."<br>";
            $email_msg.= "Number of Transactions: ".$totalSold."<br>";
            $email_msg.= "Orig Ex. Rate: ".$orig_exchange_rate."<br>";
            $email_msg.= "Ex. Rate: ".$exchange_rate."<br>";
            $email_msg.= "Cost Per SMS: ".$cost_per_sms."<br>";
            $email_msg.= "\n";

            $totalGrossProfit = $totalProfit;

            if($totalGrossProfit >= 4500)      $weeklyExpenses = 500;
            else if($totalGrossProfit >= 3250) $weeklyExpenses = 250;
            else                               $weeklyExpenses = 150;

            //$email_msg.= "Weekly Expenses: ".SALE_CURRENCY."".$weeklyExpenses." <br>";

            $ourRevenue = ($totalGrossProfit - $weeklyExpenses)/(4.0);
            //$email_msg.= "3nitylabs 25%: ".SALE_CURRENCY."".round($ourRevenue,2)."<br>";
            
            $orig_profit = ($totalProfitInt_orig + $totalProfitUS_orig)/2;
            //$email_msg.= "Gross Profit with Orig Ex. Rate: ".SALE_CURRENCY."".round($orig_profit,2)."<br><br>";
            
            $email_msg.= "<table width=\"75%\"  border=\"1\" >";
            
            $email_msg.= "<tr>";
            $email_msg.= "<td>Item ID</td> ";
            $email_msg.= "<td>Quantity</td> ";
            $email_msg.= "<td>Price in USD</td> ";
            $email_msg.= "<td>Cost in USD</td> ";
            $email_msg.= "<td>PayPal Fee</td> ";
            $email_msg.= "<td>Card Profit</td> ";
            $email_msg.= "<td>Total Item Profit</td> ";
            $email_msg.= "</tr>";

            $email_msg.= $row_msg;

            $email_msg.= "<tr>";
            $email_msg.= "<td>TOTALS</td> ";
            $email_msg.= "<td>$totalSold</td> ";
            $email_msg.= "<td>N/A</td> ";
            $email_msg.= "<td>N/A</td> ";
            $email_msg.= "<td>N/A</td> ";
            $email_msg.= "<td>N/A</td> ";
            $email_msg.= "<td>".round($totalProfit,2)."</td> ";
            $email_msg.= "</tr>";
            
            $email_msg.= "</table>";
            
            $email_msg.= "<br>Note: _0 represent items for which we do not have a PayPal Fee ";
            $email_msg.= "_1 and _2 represent the different PayPal Fees with the lower for the U.S.";
            $email_msg.= "<br>---Reports provided by 3nitylabs Inc.";
            
            return array("email"=>$email_msg,"txt"=>$txt_msg);
        }
        else
        {
            //return array containing report elements
            //ItemID,Quantity,Price,Cost to us,PayPal_Fee,Card_Profit,Profit_Int,Profit_US
            $revenue_array[$index]["item_id"]           = "TOTAL";
            $revenue_array[$index]["quantity"]          = $totalSold;
            $revenue_array[$index]["price_in_usd"]      = $totalRevenue;
            $revenue_array[$index]["cost_in_usd"]       = "N/A";
            $revenue_array[$index]["paypal_fee"]        = "N/A";
            $revenue_array[$index]["card_profit"]       = "N/A";
            $revenue_array[$index]["total_item_profit"] = round($totalProfit,2);
            if($debug) echo "Revenue Array returned <br>";
            return $revenue_array;
        }		
    }
?>