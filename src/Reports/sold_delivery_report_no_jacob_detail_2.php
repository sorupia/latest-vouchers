<?
function sold_delivery_report_no_jacob_detail_2($input_array)
{
    $countryAbbreviation = $input_array['countryAbbreviation'];
    $start_date          = $input_array['start_date'];
    $end_date            = $input_array['end_date'];
    $connection          = $input_array['connection'];

    //first query all the records that do not have jacob in txn_id or 233245867777 as receiver phone
    $query = "SELECT dateWeSoldIt as 'Sale Date',clientEmail as 'Client Email', ";
    $query.= "cardPIN as 'PIN', itemID as 'Item ID', ourPriceInUSD as 'Price', ";
    $query.= "receiverPhone as 'Receiver Phone', ";
    $query.= "transactionID as 'Transaction ID', smsGWID as 'SMS ID' ";
    $query.= "FROM UsedCards ";
    $query.= "WHERE ";
    $query.= "dateWeSoldIt > '$start_date' ";
    $query.= "AND dateWeSoldIt < '$end_date' ";
    $query.= "AND receiverPhone NOT LIKE '%233245867777%' ";
    $query.= "AND transactionID NOT LIKE '%jacob%' ";
    $query.= "ORDER BY dateWeSoldIt DESC";
    $result=mysql_query($query, $connection) or handleDatabaseError(''.mysql_error(),$query);

    //Use merged table results that contain First and Last Name to create a query
    //that will get us those results that have no first and last name
    $record_counter = 0;
    $report_detail_data = array();
    $query_addition = "";
    while($data = mysql_fetch_array($result,MYSQL_ASSOC))
    {
        //Find the First and Last Name for this client
        $name_query = "SELECT ";
        $name_query.= "clientFirstName as 'First Name', ";
        $name_query.= "clientLastName as 'Last Name' ";
        $name_query.= "FROM Clients ";
        $name_query.= "WHERE clientEmail = '".$data['Client Email']."' ";
        $name_result = mysql_query($name_query, $connection) or handleDatabaseError(''.mysql_error(),$name_query);

        $name_data = mysql_fetch_array($name_result);

        //store the record
        $report_detail_data[$record_counter]['Sale Date']      = $data['Sale Date'];
        $report_detail_data[$record_counter]['Client Email']   = $data['Client Email'];
        $report_detail_data[$record_counter]['PIN']            = $data['PIN'];
        $report_detail_data[$record_counter]['Item ID']        = $data['Item ID'];
        $report_detail_data[$record_counter]['Price']          = $data['Price'];
        $report_detail_data[$record_counter]['Receiver Phone'] = $data['Receiver Phone'];
        $report_detail_data[$record_counter]['Transaction ID'] = $data['Transaction ID'];
        $report_detail_data[$record_counter]['SMS ID']         = $data['SMS ID'];
        $report_detail_data[$record_counter]['First Name']     = $name_data['First Name'];
        $report_detail_data[$record_counter]['Last Name']      = $name_data['Last Name'];

        $record_counter++;
    }

    return $report_detail_data;
}
?>