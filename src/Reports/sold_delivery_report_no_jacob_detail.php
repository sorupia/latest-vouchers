<?
function sold_delivery_report_no_jacob_detail($input_array)
{
    $countryAbbreviation = $input_array['countryAbbreviation'];
    $start_date          = $input_array['start_date'];
    $end_date            = $input_array['end_date'];
    $connection          = $input_array['connection'];

    //First Query all the records of UsedCards that have a Clients record
    $usedCardsQuery = "SELECT UsedCards.dateWeSoldIt as 'Sale Date', UsedCards.clientEmail as 'Client Email', ";
    $usedCardsQuery.= "UsedCards.cardPIN as 'PIN', UsedCards.itemID as 'Item ID', UsedCards.ourPriceInUSD as 'Price', ";
    $usedCardsQuery.= "UsedCards.receiverPhone as 'Receiver Phone', ";
    $usedCardsQuery.= "UsedCards.transactionID as 'Transaction ID', UsedCards.smsGWID as 'SMS ID', ";
    $usedCardsQuery.= "Clients.clientFirstName as 'First Name', Clients.clientLastName as 'Last Name', Clients.clientEmail as 'Client Email2' ";
    $usedCardsQuery.= "FROM Clients, UsedCards ";
    $usedCardsQuery.= "WHERE ";
    $usedCardsQuery.= "Clients.clientEmail COLLATE latin1_general_cs = UsedCards.clientEmail ";
    $usedCardsQuery.= "AND (UsedCards.dateWeSoldIt > '$start_date' ";
    $usedCardsQuery.= "AND UsedCards.dateWeSoldIt < '$end_date' ";
    $usedCardsQuery.= "AND UsedCards.receiverPhone NOT LIKE '%233245867777%' ";
    $usedCardsQuery.= "AND UsedCards.transactionID NOT LIKE '%jacob%') ";
    $usedCardsQuery.= "ORDER BY UsedCards.dateWeSoldIt DESC";
    $usedCardsResult=mysql_query($usedCardsQuery, $connection) or handleDatabaseError(''.mysql_error(),$usedCardsQuery);

    //Use merged table results that contain First and Last Name to create a query
    //that will get us those results that have no first and last name
    $record_counter = 0;
    $report_detail_data = array();
    $query_addition = "";
    while($data = mysql_fetch_array($usedCardsResult,MYSQL_ASSOC))
    {
        //get the transaction ID
        $query_addition.= " AND cardPIN <> '".$data['PIN']."' ";

        //store the record
        $report_detail_data[$record_counter]['Sale Date']      = $data['Sale Date'];
        $report_detail_data[$record_counter]['Client Email']   = $data['Client Email'];
        $report_detail_data[$record_counter]['PIN']            = $data['PIN'];
        $report_detail_data[$record_counter]['Item ID']        = $data['Item ID'];
        $report_detail_data[$record_counter]['Price']          = $data['Price'];
        $report_detail_data[$record_counter]['Receiver Phone'] = $data['Receiver Phone'];
        $report_detail_data[$record_counter]['Transaction ID'] = $data['Transaction ID'];
        $report_detail_data[$record_counter]['SMS ID']         = $data['SMS ID'];
        $report_detail_data[$record_counter]['First Name']     = $data['First Name'];
        $report_detail_data[$record_counter]['Last Name']      = $data['Last Name'];

        $record_counter++;
    }


    $query = "SELECT dateWeSoldIt as 'Sale Date',clientEmail as 'Client Email', ";
    $query.= "cardPIN as 'PIN', itemID as 'Item ID', ourPriceInUSD as 'Price', ";
    $query.= "receiverPhone as 'Receiver Phone', ";
    $query.= "transactionID as 'Transaction ID', smsGWID as 'SMS ID' ";
    $query.= "FROM UsedCards ";
    $query.= "WHERE ";
    $query.= "dateWeSoldIt > '$start_date' ";
    $query.= "AND dateWeSoldIt < '$end_date' ";
    $query.= $query_addition;
    $query.= "AND receiverPhone NOT LIKE '%233245867777%' ";
    $query.= "AND transactionID NOT LIKE '%jacob%' ";
    $query.= "ORDER BY dateWeSoldIt DESC";
    $result=mysql_query($query, $connection) or handleDatabaseError(''.mysql_error(),$query);

    $body = $usedCardsQuery."\n".$query;
    sendEmail(FROM_NAME,SUPPORT_EMAIL,SUPER_TECH_ADMIN,$bcc,"Report Query",$body);
    
    //Store the second result in the array
    while($data = mysql_fetch_array($result,MYSQL_ASSOC))
    {
        //store the record
        $report_detail_data[$record_counter]['Sale Date']      = $data['Sale Date'];
        $report_detail_data[$record_counter]['Client Email']   = $data['Client Email'];
        $report_detail_data[$record_counter]['PIN']            = $data['PIN'];
        $report_detail_data[$record_counter]['Item ID']        = $data['Item ID'];
        $report_detail_data[$record_counter]['Price']          = $data['Price'];
        $report_detail_data[$record_counter]['Receiver Phone'] = $data['Receiver Phone'];
        $report_detail_data[$record_counter]['Transaction ID'] = $data['Transaction ID'];
        $report_detail_data[$record_counter]['SMS ID']         = $data['SMS ID'];
        $report_detail_data[$record_counter]['First Name']     = "";
        $report_detail_data[$record_counter]['Last Name']      = "";

        $record_counter++;
    }

    return $report_detail_data;
}
?>