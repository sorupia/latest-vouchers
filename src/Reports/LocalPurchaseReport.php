<?
    function local_purchase_report($input_array)
    {
        global $networkMap;
        global $networkArray;
        global $cardMap;//help determine what index value is for a particular card

        $start_date          = $input_array['start_date'];
        $end_date            = $input_array['end_date'];
        $exchange_rate       = $input_array['exchange_rate'];
        $countryAbbreviation = $input_array['countryAbbreviation'];
        $debug               = $input_array['debug'];
        $return_type         = $input_array['return_type'];

        $purchase_array = array();//stores the quantities for each denomination available
        $numberOfNetworks = count($networkMap[$countryAbbreviation]);
        $index = 0;
        $totalPurchases = 0;
        $totalPurchaseQuantity = 0;
        $totalPurchaseCost = 0;
        $totalPurchaseUSD = 0;
        $totalValueUSD = 0;

        
        //connect to DB
        $connection=connect2DB($countryAbbreviation);
                
                $totalNumber = 0;
                $totalSold = 0;
                $text_msg = "Inventory Report\n";
                for($networkIndex=0;$networkIndex<$numberOfNetworks;$networkIndex++){
                    
                    $thisNetwork=$networkMap[$countryAbbreviation][$networkIndex];
                    $numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);
                    
                    for($j=0;$j<$numberOfDenominations;$j++){
                        $thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];

                        $purchaseQuery = "SELECT itemID FROM NewCards WHERE ";
                        $purchaseQuery.= "NewCards.itemID = '$thisCard' AND dateWePurchased > '$start_date' AND dateWePurchased < '$end_date'";
                        $purchaseResult = mysql_query($purchaseQuery) or handleDatabaseError('' . mysql_error(),$purchaseQuery);
                        $usedPurchases = mysql_num_rows($purchaseResult);

                        $purchaseQuery = "SELECT itemID FROM UsedCards WHERE ";
                        $purchaseQuery.= "UsedCards.itemID = '$thisCard' AND dateWePurchased > '$start_date' AND dateWePurchased < '$end_date'";
                        $purchaseResult = mysql_query($purchaseQuery) or handleDatabaseError('' . mysql_error(),$purchaseQuery);
                        $newPurchases = mysql_num_rows($purchaseResult);
                        
                        $numberPurchased = $usedPurchases + $newPurchases;
                        $priceInLocal = getValueInLocal($thisCard);
                        
                        $cardTotal = $priceInLocal*$numberPurchased;
                        $totalUSD = $cardTotal/$exchange_rate;
                        
                        $priceInUSD = getPriceInUSD($thisCard);
                        $valueUSD = $priceInUSD*$numberPurchased;

                        $totalPurchaseQuantity+= $numberPurchased;
                        $totalPurchaseCost+= $cardTotal;
                        $totalPurchaseUSD+= $totalUSD;
                        $totalValueUSD+= $valueUSD;
                        

                        
                        if($return_type == "return_msg")
                        {
                            //$row_msg.= "$thisCard\t\t$numberPurchased\t\t$cardTotal\t\t\t\t$totalUSD\t\t$valueUSD\n";

                            $row_msg.= "<tr>";
                            $row_msg.= "<td>$thisCard</td> ";
                            $row_msg.= "<td>$numberPurchased</td> ";
                            $row_msg.= "<td>$cardTotal</td> ";
                            $row_msg.= "</tr>";	
                        }
                        else
                        {
                            //ItemID,Quantity,Card_Total
                            $purchase_array[$index]["item_id"]      = $thisCard;
                            $purchase_array[$index]["quantity"]     = $numberPurchased;
                            $purchase_array[$index]["card_total"]   = $cardTotal;
                            $index++;
                        }

                        mysql_free_result($purchaseResult);
                    }
                }
                
        
        disconnectDB($connection);

        //compile email and send it to stakeholders
        if($return_type == "return_msg")
        {
            //Text Message
            $txt_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."\n";
            $txt_msg.=  substr(DOMAIN_PREFIX,0,-1)." \n";
            $txt_msg.= "Restock in Local: ".LOCAL_CURRENCY."".$totalPurchaseCost."\n";
            $txt_msg.= "Number of Cards: ".$totalPurchaseQuantity."\n";
            $txt_msg.= "Ex. Rate: ".$exchange_rate."\n";

            //Email Message
            $email_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."<br>";
            $email_msg.=  substr(DOMAIN_PREFIX,0,-1)." <br>";
            $email_msg.= "Restock in Local: ".$totalPurchaseCost."<br>";
            $email_msg.= "Number of Cards: ".$totalPurchaseQuantity."<br>";
            $email_msg.= "Ex. Rate: ".$exchange_rate."<br><br>";
            
            if($debug) echo "txt_msg: ".$txt_msg."<br>";
            if($debug) echo "txt_msg.len: ".strlen($txt_msg)."<br>";
            
            $email_msg.= "<table width=\"65%\"  border=\"1\" >";
            
            $email_msg.= "<tr>";
            $email_msg.= "<td>Item ID</td> ";
            $email_msg.= "<td>Quantity</td> ";
            $email_msg.= "<td>Price in Local</td> ";
            $email_msg.= "</tr>";

            $email_msg.= $row_msg;

            $email_msg.= "<tr>";
            $email_msg.= "<td>TOTALS</td> ";
            $email_msg.= "<td>$totalPurchaseQuantity</td> ";
            $email_msg.= "<td>$totalPurchaseCost</td> ";
            $email_msg.= "</tr>";
            
            $email_msg.= "</table>";
            
            $email_msg.= "<br> ---Copyright 3nitylabs Software Inc";
            
            return array("email"=>$email_msg,"txt"=>$txt_msg);
        }
        else
        {
            //return array containing report elements
            //ItemID,Quantity,Price,Card_Profit,Profit_Int,Profit_US

            //ItemID,Quantity,Card_Total
            $purchase_array[$index]["item_id"]      = "TOTAL";
            $purchase_array[$index]["quantity"]     = $totalPurchaseQuantity;
            $purchase_array[$index]["card_total"]   = $totalPurchaseCost;

            if($debug) echo "Purchase Array returned <br>";
            return $purchase_array;
        }	

    }
?>