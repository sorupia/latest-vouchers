<?
    function telebm_revenue_report($start_date, 
                                   $end_date,
                                   $exchange_rate,
                                   $cost_per_sms,
                                   $countryAbbreviation,
                                   $debug,
                                   $return_type)
    {
        global $networkMap;
        global $networkArray;
        global $cardMap;//help determine what index value is for a particular card

        $revenue_array = array();
        $index = 0;

        $numberOfNetworks=count($networkMap[$countryAbbreviation]);	
        $pp_tx_fee_2 = 0.30;
        //connect to DB
        $connection=connect2DB($countryAbbreviation);

                $totalNumber = 0;
                $totalSold = 0;
                $totalProfitInt = 0;
                $totalProfitUS = 0; 



                $textmsg = "";
                $row_msg = "";

                for($networkIndex=0;$networkIndex<$numberOfNetworks;$networkIndex++){

                    $thisNetwork=$networkMap[$countryAbbreviation][$networkIndex];
                    $numberOfDenominations=count($cardMap[$countryAbbreviation][$thisNetwork]);
                    $text_msg.= $thisNetwork;

                    for($j=0;$j<$numberOfDenominations;$j++){
                        $thisCard=$cardMap[$countryAbbreviation]["$thisNetwork"]["$j"];

                        $soldQuery = "SELECT itemID FROM UsedCards WHERE ";
                        $soldQuery.= "UsedCards.itemID = '$thisCard' AND dateWeSoldIt > '$start_date' AND dateWeSoldIt < '$end_date'";

                        $soldResult = mysql_query($soldQuery) or handleDatabaseError('' . mysql_error(),$soldQuery);

                        $numberSold = mysql_num_rows($soldResult);
                        $totalSold+= $numberSold;

                        $priceInUSD = getPriceInUSD($thisCard);
                        $priceInLocal = getValueInLocal($thisCard);
                        $actual_price = $priceInLocal / $exchange_rate;

                        $pp_tx_fee_int_1 = PAY_PROC_INT_PCT * $priceInUSD;
                        $pp_tx_fee_us_1  = PAY_PROC_US_PCT * $priceInUSD;

                        $cost_b4_markup_int = $actual_price + $pp_tx_fee_int_1 + PAY_PROC_FIXED_FEE + $cost_per_sms;
                        $cost_b4_markup_us = $actual_price + $pp_tx_fee_us_1 + PAY_PROC_FIXED_FEE + $cost_per_sms;

                        $gross_per_card_int = $priceInUSD - $cost_b4_markup_int;
                        $gross_per_card_us = $priceInUSD - $cost_b4_markup_us;

                        if($debug) echo "priceInUSD: ".$priceInUSD."<br>";
                        if($debug) echo "actual price: ".$actual_price."<br>";
                        if($debug) echo "pp_tx_fee_int_1: ".$pp_tx_fee_int_1."<br>";
                        if($debug) echo "pp_tx_fee_int_2: ".$pp_tx_fee_2."<br>";
                        if($debug) echo "cost_per_sms: ".$cost_per_sms."<br>";
                        if($debug) echo "gross_per_card_int: ".$gross_per_card_int."<br><br>";


                        $profit_per_item_int = $gross_per_card_int * $numberSold;
                        $profit_per_item_us = $gross_per_card_us * $numberSold;

                        $totalProfitInt+= $profit_per_item_int;
                        $totalProfitUS+= $profit_per_item_us;
                        $totalRevenue+= ($priceInUSD * $numberSold);

                        if($return_type == "return_msg")
                        {
                            $row_msg.= "<tr>";
                            $row_msg.= "<td>$thisCard</td> ";
                            $row_msg.= "<td>$numberSold</td> ";
                            $row_msg.= "<td>$priceInUSD</td> ";
                            $row_msg.= "<td>".round($gross_per_card_int,2)."</td> ";
                            $row_msg.= "<td>".round($profit_per_item_int,2)."</td> ";
                            $row_msg.= "<td>".round($profit_per_item_us,2)."</td> ";
                            $row_msg.= "</tr>";	
                        }
                        else
                        {
                            //ItemID,Quantity,Price,Card_Profit,Profit_Int,Profit_US
                            $revenue_array[$index]["item_id"]         = $thisCard;
                            $revenue_array[$index]["quantity"]        = $numberSold;
                            $revenue_array[$index]["price_in_usd"]    = $priceInUSD;
                            $revenue_array[$index]["card_profit_int"] = round($gross_per_card_int,2);
                            $revenue_array[$index]["profit_int"]      = round($profit_per_item_int,2);
                            $revenue_array[$index]["profit_us"]       = round($profit_per_item_us,2);
                            $index++;
                        }
                        mysql_free_result($soldResult);
                    }

                }

        disconnectDB($connection);

        //compile email and send it to stakeholders
        if($return_type == "return_msg")
        {
            $gross_profit = round($totalProfitInt,2);
            $partner_revenue = $gross_profit/2;
            $our_revenue = $totalRevenue - $partner_revenue;
            $txt_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."\n";
            $txt_msg.=  substr(DOMAIN_PREFIX,0,-1)." \n";
            $txt_msg.= "Revenue: ".SALE_CURRENCY."".number_format($totalRevenue,2)."\n";
            $txt_msg.= "Gross Profit: ".SALE_CURRENCY."".number_format($gross_profit,2)."\n";
            $txt_msg.= "Our Revenue: ".SALE_CURRENCY."".number_format($our_revenue,2)."\n";
            $txt_msg.= "Number of Transactions: ".$totalSold."\n";
            $txt_msg.= "Ex. Rate: ".$exchange_rate."\n";

            if($debug) echo "txt_msg: ".$txt_msg." <br>";
            if($debug) echo "txt_msg.len: ".strlen($txt_msg)."<br>";

            $email_msg = substr($start_date,0,10)." to ".substr($end_date,0,10)."<br>";
            $email_msg.=  substr(DOMAIN_PREFIX,0,-1)." <br>";
            $email_msg.= "Revenue: ".SALE_CURRENCY."".number_format($totalRevenue,2)."<br>";
            $email_msg.= "Gross Profit: ".SALE_CURRENCY."".number_format($gross_profit,2)."<br>";
            $email_msg.= "TeleBM Revenue: ".SALE_CURRENCY."".number_format($partner_revenue,2)."<br>";
            $email_msg.= "Our Revenue: ".SALE_CURRENCY."".number_format($our_revenue,2)."<br>";
            $email_msg.= "Number of Transactions: ".$totalSold."<br>";
            $email_msg.= "Ex. Rate: ".$exchange_rate."<br><br>";
            $email_msg.= "\n";

            $email_msg.= "<table width=\"65%\"  border=\"1\" >";

            $email_msg.= "<tr>";
            $email_msg.= "<td>Item ID</td><td>Quantity</td><td>Price in USD</td> ";
            $email_msg.= "<td>Card Profit</td><td>Profit(Int)</td><td>Profit(USD)</td> ";
            $email_msg.= "</tr>";

            $email_msg.= $row_msg;

            $email_msg.= "<tr>";
            $email_msg.= "<td>TOTALS</td>";
            $email_msg.= "<td>$totalSold</td>";
            $email_msg.= "<td></td>";
            $email_msg.= "<td></td>";
            $email_msg.= "<td>".round($totalProfitInt,2)."</td>";
            $email_msg.= "<td>".round($totalProfitUS,2)."</td>";
            $email_msg.= "</tr>";
            
            $email_msg.= "</table>";
            
            $email_msg.= "\n---Copyright 3nitylabs Software Inc";
            
            return array("email"=>$email_msg,"txt"=>$txt_msg);
        }
        else
        {
            //return array containing report elements
            //ItemID,Quantity,Price,Card_Profit,Profit_Int,Profit_US
            $revenue_array[$index]["item_id"]         = "TOTAL";
            $revenue_array[$index]["quantity"]        = $totalSold;
            $revenue_array[$index]["price_in_usd"]    = $totalRevenue;
            $revenue_array[$index]["card_profit_int"] = "N/A";
            $revenue_array[$index]["profit_int"]      = round($totalProfitInt,2);
            $revenue_array[$index]["profit_us"]       = round($totalProfitUS,2);

            if($debug) echo "Revenue Array returned <br>";
            return $revenue_array;

        }
    }
?>