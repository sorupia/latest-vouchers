<?
    function displayReport($purchase_array)
	{
		$numOfColumns=count($purchase_array[0]);
		$numOfRows=count($purchase_array);
        
        $titles = array_keys($purchase_array[0]);
		
		// Print the Client data in a HTML table 
		echo"<thead> ";
		echo"<tr> ";
        for($i = 0 ; $i <count($titles) ; $i++)
        {
			echo"<th>".$titles[$i]."</th> ";
        }
		echo"</tr>";
        echo"</thead> ";
		
		for($index =0 ; $index<$numOfRows ; $index++)
		{
			echo "<tbody><tr> ";

            for($j = 0; $j<count($titles) ; $j++)
            {
                $title = $titles[$j];
                echo "<td>".$purchase_array[$index]["$title"]."</td> ";
            }

			echo "</tr></tbody> ";
		}
	}
?>