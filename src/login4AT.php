<?php
		include 'GeneralFunctions.php';
		include 'SessionFunctions.php';
	if($_POST['login4AT']){
		$clientEmail=$_POST['clientEmail'];
		$clientPassword=$_POST['clientPassword'];
		$countryAbbreviation=$_POST['countryAbbreviation'];
		
		global $countryInfo;
		$countryHomePage = $countryInfo[$countryAbbreviation]["homePage"];
		
		//echo"$clientEmail $clientPassword <br>";

		if(authenticateATSubscriber($clientEmail, $clientPassword, $countryAbbreviation)){
			// we must never forget to start the session
			session_start();
			
			//user is authentic
			//echo"User Authentic<br>";
			//what kind of subscriber is this
			$majorSubscriberQuery="SELECT dateOfExpiry FROM Subscribers WHERE clientEmail='$clientEmail'";
			$majorSubscriberData=executeQuery($majorSubscriberQuery,$countryAbbreviation);
			$_SESSION['ATSubscriberDOE']=$majorSubscriberData['dateOfExpiry'];//get major subscribers date of expiry
			
			//obtain subscribers details
			$subscriberDetailsQuery ="SELECT clientFirstName, clientLastName, addressCountry ";
			$subscriberDetailsQuery.="FROM Clients WHERE clientEmail='$clientEmail'";
			$subscriberDetailsData=executeQuery($subscriberDetailsQuery,$countryAbbreviation);

			
			$_SESSION['clientFirstName']=$subscriberDetailsData['clientFirstName'];
			$_SESSION['clientLastName']=$subscriberDetailsData['clientLastName'];
			$_SESSION['clientCountry']=$subscriberDetailsData['addressCountry'];
			$_SESSION['clientEmail']=$clientEmail;
			$_SESSION['loginMessage']="Successfully Logged in";
			
			//login subscriber
			$_SESSION['ATSubscriberLoggedIn']=true;
			

			
			//redirect to confirmation page
			header("Location: ".$countryHomePage);
			
		}
		else{
			// we must never forget to start the session
			session_start();
			//user not authentic
			//echo"User not Authentic<br>";
			//passback message regarding login
			$_SESSION['loginMessage']="Bad Username and Password";
			//redirect to confirmation page
			header("Location: ".$countryHomePage);
		}
	}
	else if($_GET['cmd']=="logout"){

		//$cmd=$_POST['cmd'];
		//echo"$cmd <br>";
		
		// we must never forget to start the session
		session_start();
		
		$countryAbbreviation=$_GET['country'];
		global $countryInfo;
		$countryHomePage = $countryInfo[$countryAbbreviation]["homePage"];
		
		$_SESSION['clientFirstName']="";
		$_SESSION['clientLastName']="";
		$_SESSION['clientCountry']="";
		$_SESSION['clientEmail']="";
		$_SESSION['loginMessage']="Successfully Logged Out";
		//login subscriber
		$_SESSION['ATSubscriberLoggedIn']=false;
		$_SESSION['ATSubscriberDOE']=0;
		
		//redirect to confirmation page
		header("Location: ".$countryHomePage);
	}
	
?>