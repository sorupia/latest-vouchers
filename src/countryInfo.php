<?php
	//an array with all the country information i.e. country code and more
	//mobilePhoneLength is length of +256782726612 includes the '+'
	$countryInfo=array("ca"=>array("country"=>"Canada","phoneCode"=>1,"mobilePhoneLength"=>12,"currency"=>"CDN", "currencyName"=>"Dollars", "homePage"=>"www.sendairtime.com"),
						"us"=>array("country"=>"USA","phoneCode"=>1,"mobilePhoneLength"=>12, "currency"=>"USD", "currencyName"=>"Dollars", "homePage"=>"www.sendairtime.com"),
						"gh"=>array("country"=>"Ghana","phoneCode"=>233,"mobilePhoneLength"=>13, "currency"=>"Cedis", "currencyName"=>"Cedi", "homePage"=>"http://www.ghanaairtime.com/"),
						"rw"=>array("country"=>"Rwanda","phoneCode"=>250,"mobilePhoneLength"=>13,"currency"=>"RWF", "currencyName"=>"Francs", "homePage"=>"www.rwandaairtime.com"),
						"ke"=>array("country"=>"Kenya","phoneCode"=>254,"mobilePhoneLength"=>13,"currency"=>"KES", "currencyName"=>"Shillings", "homePage"=>"www.sendairtime.com"),
						"tz"=>array("country"=>"Tanzania","phoneCode"=>255,"mobilePhoneLength"=>13,"currency"=>"TZS", "currencyName"=>"Shillings", "homePage"=>"www.sendairtime.com"),
						"ug"=>array("country"=>"Uganda","phoneCode"=>256,"mobilePhoneLength"=>13,"currency"=>"UGX", "currencyName"=>"Shillings", "homePage"=>"http://www.sendairtime.com/"),
						"bu"=>array("country"=>"Burundi","phoneCode"=>257,"mobilePhoneLength"=>13,"currency"=>"BIF", "currencyName"=>"Francs", "homePage"=>"www.sendairtime.com"),
						);
	$countryCodes=array(233=>"gh",254=>"ke",255=>"tz",256=>"ug",250=>"rw");
	$countryReferralPointsArray=array("ug"=>array("points"=>5,"reward"=>"10,000UGX Card"),
									  "gh"=>array("points"=>5,"reward"=>"50,000GHC Card"),
									  "default"=>array("points"=>10,"reward"=>"10,000UGX Card"));
	$countryRewardPointsArray=array("ug"=>array("points"=>300,"reward"=>"10,000UGX Card"),
									"gh"=>array("points"=>300,"reward"=>"50,000UGX Card"),
									"default"=>array("points"=>300,"reward"=>"10,000UGX Card"));
	//stores the cellphone numbers used for notification.
	$countryNotifyPhoneArray=array("ug"=>"+15877832000",
								   "gh"=>"+233245867777",//+233245867777
								   "default"=>"+15149631699");
								   
	//stores the amount required to obtain the maximum subscriber discounts
	$subscriptionAmountArray=array("ug"=>100,"gh"=>100,"default"=>100);
	
	//Email Signatures
	$ugEmailSignature="--\n";
	$ugEmailSignature.="www.sendairtime.com \n";
    $ugEmailSignature.="Instant Airtime in Notime \n";
	$ugEmailSignature.="3nitylabs Inc. \n";
	$ugEmailSignature.="Edmonton, AB, Canada \n";
	$ugEmailSignature.="+1.617.340.2408\n";
	//Ghana Signature
	$ghEmailSignature="--\nwww.ghanaairtime.com \n";
	$countryEmailSignatureArray=array("ug"=>$ugEmailSignature,"gh"=>$ghEmailSignature);
?>