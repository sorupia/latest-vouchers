<?php
/**
**  NAME: deliveryFunctions.php
**  
**  PURPOSE: Implements API & messaging between RVP vending platform and delivery manager
**
**  WRITTEN BY: Philip KM
**  
**  DATE: 27th/November/2015
**/

include_once $_SERVER['DOCUMENT_ROOT']."/src/GeneralFunctions.php"; 
include_once $_SERVER['DOCUMENT_ROOT']."/src/General/GeneralFunctions.php";
include_once $_SERVER['DOCUMENT_ROOT']."/src/constants.php";
include_once $_SERVER['DOCUMENT_ROOT']."/src/SecurityFunctions.php";
include_once $_SERVER['DOCUMENT_ROOT']."/src/Objects/ObjectFunctions.php";
include_once $_SERVER['DOCUMENT_ROOT']."/src/Order/OrderFunctions.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/src/Security/SecurityFunctions.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/src/Messaging/Messaging.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/src/Yo/yo_functions.php';

// Delivey log file handle
$dlvryFh = NULL;   

function delivery_send_request($transactionArray){

	$logFileName = "rvp2delivery." . date('Y-m-d_His') . ".txn." . $transactionArray['txn_id']. "_fn.log";
	delivey_log_open($logFileName, "w");

    delivery_log_write("Sending XML Req to: \n" . DELIVERY_URL . "\n");           
   
	//create delivery request xml and insert node values
	$xmlstr = "<?xml version='1.0' encoding='utf-8'?>
	<Delivery>
		<method>vendorToDeliveryReq</method>
		<cardPIN>".$transactionArray['cardPIN'] ."</cardPIN>
		<itemID>".$transactionArray['itemIDNumber']."</itemID>
		<clientEmail>".$transactionArray['payer_email']."</clientEmail>
		<firstName>".$transactionArray['order_data']['receiver_first_name']."</firstName>
		<lastName>".$transactionArray['order_data']['receiver_last_name']."</lastName>
		<receiverPhone>".$transactionArray['order_data']['receiver_phone']."</receiverPhone>
		<dateWeSoldIt>".$transactionArray['timeCardSent']."</dateWeSoldIt>
	</Delivery>
	";
        
	//checking xml
	delivery_log_write("Writing XML \n$xmlstr \n ");
	
	//curl initialization and content compilation
	$ch = curl_init();
	curl_setopt( $ch, CURLOPT_URL, DELIVERY_URL );
	curl_setopt( $ch, CURLOPT_POST, true );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $xmlstr );        

	$data = curl_exec($ch); 

	$network_transaction_id = 'ERROR';
	if(curl_errno($ch)) {
		$network_transaction_id .= curl_error($ch);
		
		delivery_log_write("Error Communicating with Delivery Server\n");

		// Send Error Notification by SMS/Email to Admin
		// todo.
		
	} else {
		delivery_log_write(" XML sent and response received  \n$data\n");

		// Parse the xml and extract status
		$xml = simplexml_load_string($data);
		
		// matching cardPIN, non empty net id, and matching method assumes successfull delivery req
		if ($xml->method == 'vendorToDeliveryResp' && 
			$xml->cardPIN == $transactionArray['cardPIN'] && 
			isset($xml->network_transaction_id_pending)) { 
			$network_transaction_id = $xml->network_transaction_id_pending;
		} else {
			
			$network_transaction_id .= '.' .$logFileName;
			
			// Send Error Notification by SMS/Email to Admin
			// send, xml string of debugging
			// todo.
		}
	}
	curl_close($ch);		
	
	$connection = connect2DB();
		
	// We must update the db with a 'network_transaction_id' that reflects
	// the delivery status with a PENDING Flag
	$query_cards = "UPDATE UsedCards ";
	$query_cards .= "SET network_transaction_id = '$network_transaction_id' ";
	$query_cards .= "WHERE transactionID ='".$transactionArray['txn_id']."'";
	
    $card_result = mysql_query($query_cards,$connection) or handleDatabaseError(''.mysql_error(),$query_cards);					
    
	$affectedRows = mysql_affected_rows($connection);
	
	if ($affectedRows == 0) {
		delivery_log_write("Error -- DB update to UsedCards found no matching transaction_id " . $transactionArray['txn_id'] . "\n");
		
		// Send Error Notification by SMS/Email to Admin
		// todo.
		
	} elseif ($affectedRows > 0) {
		delivery_log_write("DB update to UsedCards found matching transaction_id " . $transactionArray['txn_id'] . "\n");		
	} 
	
	delivery_log_write("Done\n");
	
	delivey_log_close();
	
	return $transactionArray;
}

function delivery_process_notification($xml_in){
	
	$logFileName = "delivery2rvp." . date('Y-m-d_His') . ".txn." . $xml_in->network_transaction_id_pending . "_fn.log";
	delivey_log_open($logFileName, "w");

    delivery_log_write("Received XML values: \n" . $xml_in->network_transaction_id_pending . "\n".$xml_in->network_transaction_id_delivered. "\n". $xml_in->cardPIN . "\n");   
   
	$status  = '';
    
    delivery_log_write("Before if statement");
		
        
    if (!isset($xml_in->network_transaction_id_pending )) {	
		
		delivery_log_write("Error --XML Missing transaction_id \n");
		$status = 'ERROR - transaction_id missing from XML';
			
	} else {	
		
        $connection = connect2DB();
        
		//
		// update the corresponding record with delivered status 
		$query_cards = "UPDATE UsedCards ";
		$query_cards .= "SET network_transaction_id = '" . $xml_in->network_transaction_id_delivered ."', serialNumber = '" . $xml_in->serialNumber  ."' ";
		$query_cards .= "WHERE network_transaction_id ='".$xml_in->network_transaction_id_pending ."' AND cardPIN = '" . $xml_in->cardPIN ."' ";
		$card_result = mysql_query($query_cards,$connection) or handleDatabaseError(''.mysql_error(),$query_cards);					
		$affectedRows = mysql_affected_rows($connection);
		
        delivery_log_write("DB update to UsedCards found $affectedRows matching transaction_id " . $xml_in->network_transaction_id_pending . "\n");	
			
		if ($affectedRows == 0) {
			delivery_log_write("Error -- DB update to UsedCards found no matching transaction_id " . $xml_in->network_transaction_id_pending . "\n");
			
			// Send Error Notification by SMS/Email to Admin
			// todo.
			$status = 'ERROR - Transaction mismatch at vendor';
			
		} elseif ($affectedRows > 0) {
			$status = 'COMPLETED';
		} 
	}
	
	delivery_log_write("Done\n");
	
	delivey_log_close();
	
	//create delivery request xml and insert node values
	$xml_out  = "<?xml version='1.0' encoding='UTF-8' ?>";
	$xml_out .= "<deliveryRequest>";
	$xml_out .= "<method>delivery2VendorResp</method>";
	$xml_out .= "<status>$status</status>";
	$xml_out .= "<cardPIN>". $xml_in->cardPIN . "</cardPIN>";
	$xml_out .= "<serialNumber>". $xml_in->serialNumber."</serialNumber>";
	$xml_out .= "<network_transaction_id_delivered>". $xml_in->network_transaction_id_delivered . "</network_transaction_id_delivered>";
	$xml_out .= "</deliveryRequest>";
	
	return $xml_out;
}


function delivey_log_open($fname){
	global $dlvryFh;
	
	$dlvryFh = fopen($_SERVER['DOCUMENT_ROOT'].'/src/Delivery/'. DELIVERY_LOG_PATH .'/'.$fname, 'w');
}

function delivery_log_write($txt){
	global $dlvryFh;
	
	if ($dlvryFh) {
		// Add timestamp
		fwrite($dlvryFh, date('Y-m-d H:i:s') . ' > '. $txt);
	}
}

function delivey_log_close(){
	global $dlvryFh;
	
	if($dlvryFh){
		fclose($dlvryFh);
	}
}
?>