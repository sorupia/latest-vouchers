<?php
	//SessionFunctions.php
	include_once'GeneralFunctions.php';
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/accessControl.php');

	function authenticateSMS($emailAddress,$password){//,$clientFirstName){
		$authSuccessful=false;
		$debug=false;
		$subscriberStatus='VERIFIED';
		//check username and password to see if they match
		//future:later on we may have to use md5 rehashing to provide extra security
		//this user also needs to be a verified user
		$connection=connect2DB();
		//search SMSUsers Table DB
		$SMSUsersSearchQuery = "SELECT * FROM SMSUsers ";
		$SMSUsersSearchQuery.= "WHERE emailAddress='$emailAddress' AND passwd=PASSWORD('$password') AND subscriberStatus='$subscriberStatus'";
		
		//lock database table
		$SMSUsersSearchResult = mysql_query($SMSUsersSearchQuery) or handleDatabaseError(''. mysql_error(),$SMSUsersSearchQuery);
		$SMSUsersSearchData = mysql_fetch_array($SMSUsersSearchResult, MYSQL_ASSOC);
		//unlock database table
		
		if (mysql_num_rows($SMSUsersSearchResult) == 1) {//VERIFIED user exists and passwd is correct
			
			//return true if user is authenticated
			$authSuccessful=true;
			if($debug){echo "Authentication Successful<br>";}
			//log user into session
			$_SESSION['smsLoggedIn']=true;
			
			//obtain clients name
			$clientFirstName=$SMSUsersSearchData['firstName'];
			if($debug){echo "Logged in User is: $clientFirstName <br>";}
			$_SESSION['clientFirstName']=$clientFirstName;
			
			//obtain Client Email Address
			$_SESSION['emailAddress']=$emailAddress;
			
			//obtain number of credits left
			$creditsLeft=$SMSUsersSearchData['creditsLeft'];
			if($debug){echo "Number Of Credits Left: $creditsLeft";}
			$_SESSION['smsCreditsLeft']=$creditsLeft;//store the number of credits left in the users session container
		}
		else{
			$authSuccessful=false;
			if($debug){echo "Authentication Failure, email:$emailAddress, passwd: $password";}
		}
		disconnectDB($connection);
		return $authSuccessful;
	}
    
    /*
	function authenticateAPIUser($emailAddress,$password){//,$clientFirstName){
		$result['authenticated']=false;
		$debug=true;
		$subscriberStatus='VERIFIED';
		//check username and password to see if they match
		//future:later on we may have to use md5 rehashing to provide extra security
		//this user also needs to be a verified user
		$connection=connect2DB();
		//search SMSUsers Table DB
		$SMSUsersSearchQuery = "SELECT * FROM SMSUsers ";
		$SMSUsersSearchQuery.= "WHERE emailAddress='$emailAddress' AND passwd=PASSWORD('$password') AND subscriberStatus='$subscriberStatus'";
		
		if($debug) {echo "authentication query: $SMSUsersSearchQuery <br>";}
		//lock database table
		$SMSUsersSearchResult = mysql_query($SMSUsersSearchQuery) or handleDatabaseError(''. mysql_error(),$SMSUsersSearchQuery);
		$SMSUsersSearchData = mysql_fetch_array($SMSUsersSearchResult, MYSQL_ASSOC);
		//unlock database table
		
		if (mysql_num_rows($SMSUsersSearchResult) == 1) {//VERIFIED user exists and passwd is correct
			
			//return true if user is authenticated
			$result['authenticated']=true;
			if($debug){echo "Authentication Successful<br>";}
			//log user into session
			//$_SESSION['smsLoggedIn']=true;
			
			//obtain clients name
			$clientFirstName=$SMSUsersSearchData['firstName'];
			if($debug){echo "Logged in User is: $clientFirstName <br>";}
			$result['clientFirstName']=$clientFirstName;
			
			//obtain Client Email Address
			$result['emailAddress']=$emailAddress;
			
			//obtain number of credits left
			$creditsLeft=$SMSUsersSearchData['creditsLeft'];
			if($debug){echo "Number Of SMS Credits Left: $creditsLeft <br>";}
			$result['smsCreditsLeft']=$creditsLeft;//store the number of credits left in the users session container
			$airtimeCreditsLeft = $SMSUsersSearchData['airtimeCreditsLeft'];
			$result['airtimeCreditsLeft'] = $airtimeCreditsLeft;			
			if($debug){echo "Number Of Airtime Credits Left: $airtimeCreditsLeft <br>";}
		}
		else{
			$result['authenticated']=false;
			if($debug){echo "Authentication Failure, email:$emailAddress, passwd: $password";}
		}
		disconnectDB($connection);
		return $result;
	}
    */
    
	/*authenticates employees
	**returns true if employee username and password match
	*/
	function authenticateEmployee($username, $password, $countryAbbreviation){
		$authSuccessful=false;
		//search Employee Table DB
		$connection=connect2DB2($countryAbbreviation);
			$EmployeesSearchQuery = "SELECT * FROM Employees ";
			$EmployeesSearchQuery.= "WHERE username='$username' AND password=PASSWORD('$password')";
			
			$EmployeesSearchResult = mysql_query($EmployeesSearchQuery) or handleDatabaseError(''. mysql_error(),$EmployeesSearchQuery);
			$EmployeesSearchData = mysql_fetch_array($EmployeesSearchResult, MYSQL_ASSOC);
		
			if (mysql_num_rows($EmployeesSearchResult) == 1){
				$authSuccessful=true;
			}
		disconnectDB($connection);
		return $authSuccessful;
	}
	
	//function to authenticate users that are airtime subscribers
	function authenticateATSubscriber($clientEmail, $clientPassword, $countryAbbreviation){
		$authSuccessful=false;
		//search subscriber table DB
		$connection=connect2DB2($countryAbbreviation);
			$subscriberSearchQuery = "SELECT clientEmail FROM Subscribers ";
			$subscriberSearchQuery.= "WHERE clientEmail='$clientEmail' AND clientPassword=PASSWORD('$clientPassword')";
			$subscriberSearchResult = mysql_query($subscriberSearchQuery) or handleDatabaseError(''. mysql_error(),$subscriberSearchQuery);
			if (mysql_num_rows($subscriberSearchResult) == 1){
				$authSuccessful=true;
			}
		disconnectDB($connection);
		return $authSuccessful;
	}

	function checkAccessLevel($maskRequired)
	{

        if( $_SESSION['user_access_level'] & $maskRequired )
        {
            //allow
        }
		else
		{
			//if user is not allowed, go to dashboard
			$_SESSION['message'] = "SORRY NO ACCESS TO PAGE";
			header("Location: dashboard.php");
			exit();
		}
	}
    
    function checkPageAccess($page)
    {
		//get page mask required from global map
		global $pageMaskMap;
        $pageMask = $pageMaskMap[$page];
		
        checkAccessLevel($pageMask);
    }

	function setSession(){
	}
	function clearSession(){
	}
	//function setCookie(){
	//}
	//function clearCookie(){
	//}

?>
