<?
    include_once $_SERVER['DOCUMENT_ROOT']."/src/country_codes.php";

    /* 
    March 21st 2009
    Functionality to compare country code from Paypal information with that 
    returned from the GEOIP query.
    The Geo IP country is returned as a 2 letter ISO code e.g. US 
    and the paypal information is a full country name e.g. United States 
    */

    function compare_ISO_to_cname($country_code,$country_name)
    {
        global $iso_country_codes;
        $geo_ip_country = $iso_country_codes[$country_code];
      
        $paypal_country = $country_name;
        
        if( strcmp(strtolower($geo_ip_country),
                   strtolower($paypal_country)
                   ) == 0 )
        {
            return true;
        }
        
        return false;
    }
   
?>