<?php

/*******************************************************************************
*   FILE: sendATSimple.php
*   PURPOSE: Provides an access function for internal scripts to sendairtime
*            adapted from sendAT.php form handling script.
*   WRITTEN BY: Arthur Ntozi (3nitylabs, Montreal)   DATE: 27.Nov.2009
*
*   MODIFIED BY: Arthur Ntozi (3nitylabs, New York)   DATE: 22.Sep.2012
*                Added dateWePurchased to outgoing email to client
*
*********************************************************************************/
    
	require_once($_SERVER['DOCUMENT_ROOT'].'/src/SessionFunctions.php');
	
    function sendATSimple($countryAbbreviation,
                          $airtimeDetails)
    {
        $employeeUsername=$airtimeDetails['employeeUsername'];
        $employeePassword=$airtimeDetails['employeePassword']; 
        $transID=$airtimeDetails['transID']; 
        $clientEmail=$airtimeDetails['clientEmail']; 
        $clientFirstName=$airtimeDetails['clientFirstName']; 
        $receiverFirstName=$airtimeDetails['receiverFirstName']; 
        $receiverPhone=$airtimeDetails['receiverPhone']; 
        $receiverEmail=$airtimeDetails['receiverEmail']; 
        $itemIDNumber=$airtimeDetails['itemIDNumber'];
        
        $resultArray['message'] = "";
        $resultArray['card'] = 0;
        $resultArray['status'] = "FAILURE";
		$resultArray['result'] = false;

		//authenticate employees username and password
		if(authenticateEmployee($employeeUsername, $employeePassword, $countryAbbreviation)){
            if($submissionFromGlitch){
            //if submission is from paypal glitch
                //check SALES
                //if transaction id is in IPN db and email is a match
                    //this will require us to activate the IPN code to store all the sales prior
                    //lock NewCards Table
                    //extract card
                    //insert into usedCards
                    //unlock NewCards Table
                    //email cx, rx SMS rx
                //else transaction id is not in sales
                    //do nothing
            }
			else{//else submission is not from paypal glitch
            
                $connection=connect2DB2($countryAbbreviation);
					//check for duplicate transactionID
					$query = "SELECT transactionID FROM Sales WHERE (Sales.transactionID LIKE '%$transID%')";
					$result = mysql_query($query) or handleDatabaseError(''. mysql_error(),$query);
					$num_rows = mysql_num_rows($result);
				disconnectDB($connection);
            
				if($clientEmail==""||$clientFirstName==""||$receiverPhone==""||$transID==""){
					$errorMessage.= 'The transaction ID, client email, client first name and receiver phone cannot be blank <br>';
                    $resultArray['message'] = $errorMessage;
				}
                else if( $num_rows != 0 )
				{
					$errorMessage.= "The transaction id you entered is a duplicate <br>";
                    $resultArray['message'] = $errorMessage;
				}
				else{
					//for now we will go with this option until we have created the IPN code
					
					$connection=connect2DB2($countryAbbreviation);
					
					//This is a CRITICAL SECTION, we need a lock from this point till when we delete the card
					//1. lock NewCards Table 	---BEGIN CRITICAL SECTION 
					$lockQuery="LOCK TABLES NewCards WRITE";
					$lockResult=mysql_query($lockQuery) or handleDatabaseError(''. mysql_error(),$lockQuery);
					//2. extractCard
					$newcards_query = "SELECT * FROM NewCards WHERE cardStatus <>'USED' AND itemID='$itemIDNumber' ORDER BY `dateWePurchased` ASC LIMIT 1 FOR UPDATE";
					$newcardsresult = mysql_query($newcards_query) or handleDatabaseError(''. mysql_error(),$newcards_query);
					$newcards_data = mysql_fetch_array($newcardsresult);
					$numberOfCardsLeft = mysql_num_rows($newcardsresult);
		
					$cardPIN = -1;
					if($numberOfCardsLeft>0){
						$cardPIN = $newcards_data['cardPIN'];
						if(debug=='true'){echo"cardPIN: $cardPIN";}
					}
					else{
						$cardPIN = -1;
						//echo "There are no more cards left. ";
					}
						
					$updatecards_query = "UPDATE NewCards SET cardStatus='USED' WHERE cardPIN='$cardPIN'";
					$updatecardsresult = mysql_query($updatecards_query) or handleDatabaseError(''. mysql_error(),$updatecards_query);
					
					$newcards_query = "DELETE FROM NewCards WHERE cardPIN = '$cardPIN' AND cardStatus='USED'";
					$deleteresult = mysql_query($newcards_query) or handleDatabaseError(''. mysql_error(),$newcards_query);
		
					//--END CRITICAL SECTION  UNLOCK DATABASE ACCESS 7
					//unlock NewCards Table
					$lockQuery="UNLOCK TABLES";
					$lockResult=mysql_query($lockQuery) or handleDatabaseError(''. mysql_error(),$lockQuery);
					
					//if there are no new cards matching the one requested, then log to OutGoingQueue table
					//--BEGIN CASE WHERE THERE ARE CARDS PRESENT
					
					if($numberOfCardsLeft>0){//there are cards that match the users' requirements
						$cardsArePresent="true";
						$timeCardSent = date("Y-m-d H:i:s");
						$networkName = $newcards_data['networkName'];
						$valueInUGSh = $newcards_data['valueInUGSh'];
						$ourPriceInUSD = $newcards_data['ourPriceInUSD'];
						$dateWePurchased = $newcards_data['dateWePurchased'];
						$itemIDNumber = $newcards_data['itemID'];
						$loadedBy = $newcards_data['loadedBy'];
						$loadersIP= $newcards_data['loadersIP'];
                        $serialNumber = $newcards_data['serialNumber'];
						if($debug=="true"){
							echo "cardPIN: $cardPIN <br> valueInUGSh: $valueInUGSh <br> networkName: $networkName <br> ourPriceInUSD: $ourPriceInUSD <br>dateWePurchased: $dateWePurchased <br>itemID: $itemID <br>";		
						}
		
						if($debug=="true"){
							print "The PIN $cardPIN has been deleted <br>";
						}
						
						if($debug=="true"){
							print "CardPIN: $cardPIN <br>";
							print "NetworkName: $networkName <br>";
							print "ValueInUgSh: $valueInUGSh <br>";
							print "OurPriceInUSD: $ourPriceInUSD <br>";
							print "dateWePurchased: $dateWePurchased <br>";
							print "Time: $timeCardSent <br>";
							print "transID: $transID <br>";
							print "itemIDNumber: $itemIDNumber <br>";
							print "clientEmail: $clientEmail <br>";
							print "loadersIP: $loadersIP<br>";
						}
		
						//SEND AIRTIME TO CLIENT EMAIL, RECEIVER MOBILE PHONE AND RECEIVER EMAIL							
						//since there is a card, send the PIN to the client: Mail the PIN to $clientEmail
						//sendEmail2Client($clientEmail,$receiverPhone,$clientFirstName,$cardPIN,$itemIDNumber,$transID);
                        //$inputArray['toAddress'] = $clientEmail;
                        $inputArray['payer_email'] = $clientEmail;
                        $inputArray['receiverFirstName'] = $receiverFirstName;
                        $inputArray['firstName'] = $clientFirstName;
                        $inputArray['pin2Send'] = $cardPIN;
                        $inputArray['itemID'] = $itemIDNumber;
                        $inputArray['transid'] = $transID;
                        $inputArray['serialNumber'] = $serialNumber;
                        $inputArray['dateWePurchased'] = $dateWePurchased;
                        emailAirtime($inputArray);
                        
                        
						//SMS the card to receiver
						$clickMessageID=sendEmail2Phone($receiverPhone,$receiverFirstName,$clientFirstName,$cardPIN,$valueInUGSh,$itemIDNumber);
						//Email the card to the receiver if any
						if($receiverEmail!=''){
							sendEmail2Receiver($receiverEmail,$receiverFirstName,$clientFirstName,$cardPIN,$itemIDNumber,$transID);
						}
						//-------------------------------------------------------------------------------------------------------
						
						//BEGIN old code
						//fill in the missing spaces in the Sales DB
						//$updatecards_query = "UPDATE Sales SET receiverEmail='$receiverEmail' WHERE transactionID='$transID'";
						//$updatecardsresult = mysql_query($updatecards_query) or handleDatabaseError(''. mysql_error(),$updatecards_query);
						//END old code
						
						//Insert this transaction into the Sales table-------------------------------------------------------------------
						$updatecards_query = "INSERT INTO Sales (transactionID,clientEmail,amount,clientIP,receiverEmail) ";
						$updatecards_query.= "VALUES('$transID','$clientEmail',$ourPriceInUSD,'$clientIP','$receiverEmail')";
						if($debug=="true") echo "Sales Update Query: $updatecards_query <br>";
						$updatecardsresult = mysql_query($updatecards_query) or handleDatabaseError(''. mysql_error(),$updatecards_query);
						
						//3. transfer Card: Add the extracted PIN to the UsedCards table
						//4. insert into usedCards Table													
						$newcards_query = "INSERT IGNORE INTO UsedCards (cardPIN, networkName, valueInUGSh, ourPriceInUSD, dateWePurchased, dateWeSoldIt, transactionID, itemID, clientEmail, loadedBy, loadersIP, receiverEmail,receiverPhone, smsGWID) 
						VALUES ('$cardPIN', '$networkName', $valueInUGSh, $ourPriceInUSD, '$dateWePurchased', '$timeCardSent', '$transID', '$itemIDNumber', '$clientEmail', '$loadedBy','$loadersIP','$receiverEmail','$receiverPhone','$clickMessageID')";						
						$insertresult = mysql_query($newcards_query) or handleDatabaseError(''. mysql_error(),$newcards_query);
	
						//UPDATE RECEIVERS----------------------------------------------------------------------------------------------
						//check to see if receiver is already in table
						$isReceiverInTable="SELECT receiverPhone FROM Receivers WHERE receiverPhone='$receiverPhone'";
						$isReceiverInResult=mysql_query($isReceiverInTable) or handleDatabaseError(''. mysql_error(),$isReceiverInTable);
						$isReceiverInNumber=mysql_num_rows($isReceiverInResult);//number of results returned		
						//If receiver does not exist
						if($isReceiverInNumber==0){
							$storeReceiverQuery="INSERT INTO Receivers(receiverPhone";
							$add1="";
							$add2="";
							if($receiverEmail!=''){
								$storeReceiverQuery.=" ,receiverEmail";
								$add1=",'$receiverEmail'";
							}
							else{
								//there is no receiverEmail provided so we will not update this information
							}
							if($receiverFirstName!=''){
								$storeReceiverQuery.=" ,receiverFirstName";
								$add2=",'$receiverFirstName'";
							}
							else{
								//there is no receiverFirstName provided so we will not update this information
							}
							$storeReceiverQuery.=") VALUES('$receiverPhone'";
							$storeReceiverQuery.=$add1;
							$storeReceiverQuery.=$add2;
							$storeReceiverQuery.=")";
							
							$storeReceiverResult=mysql_query($storeReceiverQuery) or handleDatabaseError(''. mysql_error(),$storeReceiverQuery);
							
						}
						else{//this Receiver already exists
							//update the receiver table
							$updateReceiverQuery="UPDATE Receivers SET";
							if($receiverEmail!=''){
								$updateReceiverQuery.=" receiverEmail='$receiverEmail' ";
							}
							if(	$receiverFirstName!=''){
								if($receiverEmail!=''){
									$updateReceiverQuery.=",";
								}
								$updateReceiverQuery.=" receiverFirstName='$receiverFirstName' ";
							}
							
							if($receiverEmail!=''||$receiverFirstName!=''){
								$updateReceiverQuery.="WHERE receiverPhone='$receiverPhone'";
								if($debug){echo"updateReceiverQuery = $updateReceiverQuery<br>";}
								$updateReceiverResult=mysql_query($updateReceiverQuery) or handleDatabaseError(''. mysql_error(),$updateReceiverQuery);
							}
						}
						//UPDATE RECEIVERS----------------------------------------------------------------------------------------------
						
						$message.= "Airtime Card with ItemID: $itemIDNumber Sent <br>";
						$message.= "Access No.: $cardPIN <br>";
						$message.= "Receiver Phone: $receiverPhone <br>";
						$message.= "Receiver Email: $receiverEmail <br>";
						$message.= "Client Email: $clientEmail <br>";
                        
                        $resultArray['status'] = "SUCCESS";
                        $resultArray['card'] = $cardPIN;
						$resultArray['result'] = true;
						
					}//end ($numberOfCardsToUpdate>0)
					else{//there are NO MORE CARDS left and hence we have to inform the admin
						$message.= "There are no more cards left";
                        $resultArray['message'] = $message;
					}//--END CASE WHERE THERE ARE CARDS OR NO CARDS PRESENT
					disconnectDB($connection);
					
				}//end else if($clientsEmail==""||$clientsFirstName==""||$receiverPhone=="")
			}//end else if($submissionFromGlitch)
		}
		else{//else employee is invalid
			$errorMessage.= 'The username and password are incorrect <br>';
            $resultArray['message'] = $errorMessage;
		}
        
        return $resultArray;
	}
?>