<?php
/*******************************************************************************
**   FILE: DeliveryNotificationSuccessHandler.php
**
**   FUNCTION: Receives successful delivery xml from Delivery platform,
**   passes xml to delivery_process_notification and echos output
**
**   WRITTEN BY: Daniel Agaba DATE: 1st/Nov/2015
*********************************************************************************/

include_once $_SERVER['DOCUMENT_ROOT']."/src/Delivery/DeliveryFunctions.php"; 

$xml_location = trim(file_get_contents('php://input'));
$xml_in = simplexml_load_string($xml_location);

$xml_out = delivery_process_notification($xml_in);

echo $xml_out;

?>