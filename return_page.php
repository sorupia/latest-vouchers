<?php
/*******************************************************************************
**   FILE: return_page.php
**
**   FUNCTION: N/A
**
**   PURPOSE: Handles the display of the page retrieving return messages from paypal and header/footer files
**
**   WRITTEN BY: Arthur Ntozi (3nitylabs, Kampala)   
**
**   MODIFIED BY: Arthur Ntozi (3nitylabs, Kampala)   DATE: 2015.12.01
**   JavaScript may be causing return_page to be called twice
**
*********************************************************************************/
?>
<? include "wrapper/return_page_header.php" ?>
    <? include "src/Order/return_page_vchr_code.php" ;?>
<? include "wrapper/return_page_footer.php" ?>
